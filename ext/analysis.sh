#!/bin/sh

export CPPCHECK_INCLUDES="-Ibuild/src/libpampa -Isrc/libpampa -Isrc/pampa-remesh -Isrc/samples/C/include -Isrc/remeshers/gmsh -Isrc/remeshers/tetgen -Isrc/remeshers/common -Isrc/remeshers/mmg3d/libpampa-mmg3d4 -Isrc/remeshers/mmg3d/libpampa-mmg3d"
export SOURCES_TO_ANALYZE="src"

cppcheck -v -f --max-configs=1 --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${CPPCHECK_INCLUDES} ${SOURCES_TO_ANALYZE} 2> pampa-cppcheck.xml

rats -w 3 --xml ${SOURCES_TO_ANALYZE} > pampa-rats.xml

cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.login=$SONARQUBE_LOGIN
sonar.links.homepage=https://gitlab.inria.fr/PaMPA/PaMPA
sonar.links.scm=https://gitlab.inria.fr/PaMPA/PaMPA.git
sonar.links.ci=https://gitlab.inria.fr/PaMPA/PaMPA/pipelines
sonar.links.issue=https://gitlab.inria.fr/PaMPA/PaMPA/issues
sonar.projectKey=tadaam:pampa:gitlab:master
sonar.projectDescription=a parallel library for handling, redistributing and remeshing unstructured meshes on distributed-memory architectures, using any sequential remesher
sonar.projectVersion=1.1
sonar.language=c
sonar.lang.patterns.c++=**/*.cpp,**/*.hpp
sonar.sourceEncoding=UTF-8
sonar.sources=src
sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n' ',')build/src/libpampa,src/libpampa,src/pampa-remesh,src/samples/C/include,src/remeshers/gmsh,src/remeshers/tetgen,src/remeshers/common,src/remeshers/mmg3d/libpampa-mmg3d4,src/remeshers/mmg3d/libpampa-mmg3d
sonar.c.errorRecoveryEnabled=true
sonar.c.gcc.charset=UTF-8
sonar.c.gcc.regex=(?<file>.*):(?<line>[0-9]+):[0-9]+:\\\x20warning:\\\x20(?<message>.*)\\\x20\\\[(?<id>.*)\\\]
sonar.c.gcc.reportPath=pampa-build.log
sonar.c.coverage.reportPath=pampa-coverage.xml
sonar.c.cppcheck.reportPath=pampa-cppcheck.xml
sonar.c.rats.reportPath=pampa-rats.xml
sonar.c.clangsa.reportPath=analyzer_reports/*/*.plist
EOF
