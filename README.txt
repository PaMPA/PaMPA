The terms under which this copy of the PaMPA 2.0 distribution is provided to you
are described in file "LICENSE_en.txt", located in the same directory as this
file.

If you accept them, please:
 - look at PaMPA-Doc project which contains all the documentation
 - or build the documentation (with CMake and PAMPA_BUILD_DOC advanced option)
   and refer to file "doc/user_2.0/pampa_v2.0.pdf" or
   "doc/user_2.0/html/index.html", also located in this directory, for the
   installation instructions.


********************************************************************
*                                                                  *
*      We are very eager to get feed-back from our users!          *
*                                                                  *
*   Please send us an e-mail (pampaATinria.fr) to tell us how      *
*   PaMPA works for you.                                           *
*   It will help us update our roadmap and provide new features.   *
*                                                                  *
********************************************************************
