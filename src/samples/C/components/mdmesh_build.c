/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mdmesh_build.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 16 Sep 2015
//!                             to:    4 Jan 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <mpi.h>
#include <common.h>
#include <mdmesh_iterator.h>

//! \example build_check.c
//! This file must be combined with:
//! - a file, which doesn't begin by seq_,  in "samples/src/C/tools" directory
//! - a mesh file, which doesn't match with "*1-proc*", in "samples/src/C/meshes" directory
//!
//! For example, it is coupled with tools/it.c and meshes/11-vert_2-proc.c, the
//! output will be :
//! \verbinclude tests/build_check_11-vert_2-proc_it_c
int main(int argc, char** argv){

  int rank, retval;
  PAMPA_Num vertlocnbr, vertlocmax, edgelocnbr, edgelocsiz, enttglbnbr, mgrid_edge;
  PAMPA_Num *vendloctab, *ventloctab, *esubloctab, *edloloctab, *enloglbtab;
  PAMPA_Num baseval, valuglbmax, mdmhglbnbr, velolocsiz, enttglbnum, elvlglbsiz, loadglbval, flagval, mdmhglbnum, edlolocsiz;
  PAMPA_Dmesh m;
  PAMPA_Mdmesh mm;
  PAMPA_Num *vertloctab, *edgeloctab, *veloloctab, *levledloloctab, *enttglbtab, *elvlglbtab, *lnumglbtab, *etagglbtab;
  FILE *out;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &retval);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank) ;

  if (argc > 1) {
    char s[500];
    sprintf(s, "%s-%d", argv[1], rank);
    out = fopen(s, "w");
  }
  else {
    out = stdout;
  }

  // Initialisation of PaMPA dmesh structure
  CHECK_PAMPA(PAMPA_dmeshInit(&m, MPI_COMM_WORLD), "pampa_dmeshinit", EXIT_FAILURE);

  // Build distributed mesh
  vertlocmax = 0;
  structureBuild(rank, &baseval, &vertlocnbr, &vertlocmax, &edgelocnbr, &edgelocsiz, &enttglbnbr, &valuglbmax, &ventloctab, &esubloctab, &vertloctab, &vendloctab, &edgeloctab, &edloloctab, &enloglbtab);

  if (enttglbnbr == 7) // XXX pas terrible ce test, à cause de common{,2}.h
    mgrid_edge = 3;
  else
    mgrid_edge = 1;
  // Build PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshBuild(&m, 0, vertlocnbr, vertlocmax, vertloctab, vendloctab, NULL, edgelocnbr, edgelocsiz, edgeloctab, edloloctab, enttglbnbr, ventloctab, esubloctab, enloglbtab, valuglbmax, 0), "pampa_dmeshbuild", EXIT_FAILURE);

  // Check PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshCheck(&m), "pampa_dmeshcheck", EXIT_FAILURE);

  mdmhglbnbr = 3; // XXX
  CHECK_PAMPA(PAMPA_mdmeshInit(&mm, 0, 3, MPI_COMM_WORLD), "pampa_mdmeshinit", EXIT_FAILURE);
  CHECK_PAMPA(PAMPA_mdmeshLevelInit(&mm, &m), "pampa_mdmeshLevelinit", EXIT_FAILURE);

  // Finalisation of PaMPA dmesh structure
  //PAMPA_dmeshExit(&m);


  velolocsiz = 0;
  veloloctab = NULL;
  edlolocsiz = 0;
  levledloloctab = NULL;
  //enttglbtab = NULL;
  enttglbtab = (PAMPA_Num *) malloc (enttglbnbr * sizeof (PAMPA_Num));
  for (enttglbnum = 0; enttglbnum < enttglbnbr; enttglbnum ++)
    enttglbtab[enttglbnum] = enttglbnum;

  elvlglbsiz = 1;
  elvlglbtab = (PAMPA_Num *) malloc (elvlglbsiz * sizeof (PAMPA_Num));
  lnumglbtab = (PAMPA_Num *) malloc (elvlglbsiz * sizeof (PAMPA_Num));
  elvlglbtab[0] = mgrid_edge;
  lnumglbtab[0] = 1;
  //elvlglbtab[1] = BND_mgrid_edge;
  //lnumglbtab[1] = 1;
  //elvlglbtab[1] = NODE;
  //lnumglbtab[1] = 2;
  etagglbtab = NULL;

  loadglbval = 2;
  flagval = PAMPA_LVL_PART_LOCAL;

  for (mdmhglbnum = 1; mdmhglbnum < mdmhglbnbr; mdmhglbnum ++) {
    PAMPA_Num tmpval;
    CHECK_PAMPA(PAMPA_mdmeshCoarLevelBuild(
          &mm,           //    PAMPA_Mdmesh * const         meshptr,
          &tmpval,       //    PAMPA_Num * const            meshglbnum,
          velolocsiz,    //    const PAMPA_Num              velolocsiz,
          veloloctab,    //    const PAMPA_Num * const      veloloctab,
          edlolocsiz,    //    const PAMPA_Num              edlolocsiz,
          levledloloctab,//    const PAMPA_Num * const      edloloctab,
          enttglbtab,    //    const PAMPA_Num * const      enttglbtab,
          elvlglbsiz,    //    const PAMPA_Num              elvlglbsiz,
          elvlglbtab,    //    const PAMPA_Num * const      elvlglbtab,
          lnumglbtab,    //    const PAMPA_Num * const      lnumglbtab,
          etagglbtab,    //    const PAMPA_Num * const      etagglbtab,
          loadglbval,    //    const PAMPA_Num              loadglbval,
          flagval),      //    const PAMPA_Num              flagval);
      "pampa_mdmeshlevelbuild", EXIT_FAILURE);
    if (tmpval != mdmhglbnum) {
      printf ("The level is not correct");
      return (1);
    }
  }

  free (elvlglbtab);
  free (enttglbtab);
  free (lnumglbtab);

  //CHECK_PAMPA(PAMPA_mdmeshCheck(&mm), "pampa_dmeshcheck", EXIT_FAILURE);
  
  iterator(out, rank, &mm, 0, ELEM, 1, ELEM);

  iterator(out, rank, &mm, 0, mgrid_edge, 1, mgrid_edge);

  // Finalisation of PaMPA dmesh structure
  PAMPA_mdmeshExit(&mm);

  structureFree( &ventloctab, &esubloctab, &vertloctab, &vendloctab, &edgeloctab, &edloloctab, &enloglbtab);

  if (rank == 0)
    fprintf(out, "fin du prog\n");

  if (argc > 1) {
    fclose(out);
  }

  MPI_Finalize() ;

  return EXIT_SUCCESS;
}
