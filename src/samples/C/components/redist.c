/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        redist.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 14 May 2012
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <mpi.h>
#include <common.h>

//! \example redist.c
//! This file must be combined with:
//! - a file, which doesn't begin by seq_,  in "samples/src/C/tools" directory
//! - a mesh file, which doesn't match with "*1-proc*", in "samples/src/C/meshes" directory
int redist_test(int argc, char** argv){

  int size, rank, retval;
  PAMPA_Num vertlocnbr, vertlocmax, edgelocnbr, edgelocsiz, enttglbnbr;
  PAMPA_Num *vendloctab, *ventloctab, *esubloctab, *edloloctab, *enloglbtab;
  PAMPA_Num baseval, valuglbmax, ovlpglbval;
  PAMPA_Dmesh m, m2;
  PAMPA_Num *vertloctab, *edgeloctab, *partloctab;
  PAMPA_Strat strat;
  FILE *out;
  //MPI_Group group_world, group2, group3, group_union;
  //MPI_Comm comm2, comm_union;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &retval);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank) ;
  MPI_Comm_size(MPI_COMM_WORLD, &size) ;

  ovlpglbval = 0;


  if (argc > 1) {
    char s[500];
    sprintf(s, "%s-%d", argv[1], rank);
    out = fopen(s, "w");
  }
  else {
    out = stdout;
  }

  // Initialisation of PaMPA dmesh structure
  CHECK_PAMPA(PAMPA_dmeshInit(&m, MPI_COMM_WORLD), "pampa_dmeshinit", EXIT_FAILURE);
  CHECK_PAMPA(PAMPA_dmeshInit(&m2, MPI_COMM_WORLD), "pampa_dmeshinit", EXIT_FAILURE);

  // Build distributed mesh
  vertlocmax = 0;
  structureBuild(rank, &baseval, &vertlocnbr, &vertlocmax, &edgelocnbr, &edgelocsiz, &enttglbnbr, &valuglbmax, &ventloctab, &esubloctab, &vertloctab, &vendloctab, &edgeloctab, &edloloctab, &enloglbtab);

  // Build PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshBuild(&m, 0, vertlocnbr, vertlocmax, vertloctab, vendloctab, NULL, edgelocnbr, edgelocsiz, edgeloctab, edloloctab, enttglbnbr, ventloctab, esubloctab, enloglbtab, valuglbmax, 0), "pampa_dmeshbuild", EXIT_FAILURE);

  // Check PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshCheck(&m), "pampa_dmeshcheck", EXIT_FAILURE);

  if ((partloctab = malloc (sizeof (PAMPA_Num) * vertlocnbr)) == NULL)
	return EXIT_FAILURE;
  
  // Initialisation of PaMPA strat structure
  CHECK_PAMPA(PAMPA_stratInit(&strat), "pampa_stratinit", EXIT_FAILURE);

  // Partitioning PaMPA Dmesh
  CHECK_PAMPA(PAMPA_dmeshPart(&m, (PAMPA_Num) (size - 1), &strat, partloctab), "pampa_dmeshpart", EXIT_FAILURE);

  // Redistribute PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshRedist(&m, partloctab, NULL, -1, -1, ovlpglbval, &m2), "pampa_dmeshredist", EXIT_FAILURE);

  // Check PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshCheck(&m2), "pampa_dmeshcheck", EXIT_FAILURE);

  // Finalisation of PaMPA dmesh structure
  PAMPA_dmeshExit(&m);
  PAMPA_dmeshExit(&m2);
  free (partloctab);

  structureFree( &ventloctab, &esubloctab, &vertloctab, &vendloctab, &edgeloctab, &edloloctab, &enloglbtab);

  if (rank == 0)
    fprintf(out, "fin du prog\n");

  if (argc > 1) {
    fclose(out);
  }

  MPI_Finalize() ;

  return EXIT_SUCCESS;
}

int main(int argc, char ** argv) {

  return redist_test (argc, argv);
}

