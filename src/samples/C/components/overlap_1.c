/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        overlap_1.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <mpi.h>
#include <common.h>
#include <iterator.h>


//! \example overlap_1.c
//! This file must be combined with:
//! - a file, which doesn't begin by seq_,  in "samples/src/C/tools" directory
//! - a mesh file, which doesn't match with "*1-proc*", in "samples/src/C/meshes" directory
int overlap_1_test(int argc, char** argv){

  int rank, retval;
  PAMPA_Num vertlocnbr, vertlocmax, edgelocnbr, edgelocsiz, enttglbnbr;
  PAMPA_Num *vendloctab, *ventloctab, *esubloctab, *edloloctab, *enloglbtab;
  PAMPA_Num edgegstnbr, edgenum;
  PAMPA_Num baseval, valuglbmax;
  PAMPA_Dmesh m;
  PAMPA_Num *vertloctab, *edgeloctab;
  int *data;
  FILE *out;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &retval);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank) ;

  if (argc > 1) {
    char s[500];
    sprintf(s, "%s-%d", argv[1], rank);
    out = fopen(s, "w");
  }
  else {
    out = stdout;
  }

  // Initialisation of PaMPA dmesh structure
  CHECK_PAMPA(PAMPA_dmeshInit(&m, MPI_COMM_WORLD), "pampa_dmeshinit", EXIT_FAILURE);

  // Build distributed mesh
  vertlocmax = 0;
  structureBuild(rank, &baseval, &vertlocnbr, &vertlocmax, &edgelocnbr, &edgelocsiz, &enttglbnbr, &valuglbmax, &ventloctab, &esubloctab, &vertloctab, &vendloctab, &edgeloctab, &edloloctab, &enloglbtab);

  // Build PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshBuild(&m, 0, vertlocnbr, vertlocmax, vertloctab, vendloctab, NULL, edgelocnbr, edgelocsiz, edgeloctab, edloloctab, enttglbnbr, ventloctab, esubloctab, enloglbtab, valuglbmax, 1), "pampa_dmeshbuild", EXIT_FAILURE);

  // Check PaMPA dmesh
  CHECK_PAMPA(PAMPA_dmeshCheck(&m), "pampa_dmeshcheck", EXIT_FAILURE);

  iterator(out, rank, &m, NODE, EDGE);
  iterator(out, rank, &m, INT_EDGE, NODE);
  
  iterator(out, rank, &m, ELEM, ELEM);
  
  PAMPA_dmeshEnttSize(&m, BND_EDGE, &edgelocnbr, PAMPA_VERT_LOCAL, &edgegstnbr, PAMPA_VERT_ANY, NULL);


  CHECK_PAMPA(PAMPA_dmeshValueLink(&m, (void **) &data, PAMPA_VALUE_PUBLIC, MPI_INT, BND_EDGE, 1), "pampa_dmeshvaluelink", EXIT_FAILURE);

  for (edgenum = 0 ; edgenum < edgelocnbr ; edgenum ++) {
    data[edgenum] = (edgenum + 1) * 10 + rank + 1;
  }

  CHECK_PAMPA(PAMPA_dmeshHaloValue(&m, BND_EDGE, (PAMPA_Num) 1), "pampa_dmeshhalovalue", EXIT_FAILURE);
  
  for (edgenum = 0 ; edgenum < edgegstnbr ; edgenum ++) {
    fprintf(out, "[%d] data[%d] = %d\n", rank, (int) edgenum, data[edgenum]);
  }

  
  PAMPA_dmeshEnttSize(&m, EDGE, &edgelocnbr, PAMPA_VERT_LOCAL, &edgegstnbr, PAMPA_VERT_ANY, NULL);


  CHECK_PAMPA(PAMPA_dmeshValueLink(&m, (void **) &data, PAMPA_VALUE_PUBLIC, MPI_INT, EDGE, 1), "pampa_dmeshvaluelink", EXIT_FAILURE);

  for (edgenum = 0 ; edgenum < edgelocnbr ; edgenum ++) {
    data[edgenum] = (edgenum + 1) * 10 + rank + 1;
  }

  CHECK_PAMPA(PAMPA_dmeshHaloValue(&m, EDGE, 1), "pampa_dmeshhalovalue", EXIT_FAILURE);
  
  for (edgenum = 0 ; edgenum < edgegstnbr ; edgenum ++) {
    fprintf(out, "[%d] data[%d] = %d\n", rank, (int) edgenum, data[edgenum]);
  }

  // Finalisation of PaMPA dmesh structure
  PAMPA_dmeshExit(&m);

  structureFree( &ventloctab, &esubloctab, &vertloctab, &vendloctab, &edgeloctab, &edloloctab, &enloglbtab);

  if (rank == 0)
    fprintf(out, "fin du prog\n");

  if (argc > 1) {
    fclose(out);
  }

  MPI_Finalize() ;

  return EXIT_SUCCESS;
}

int main(int argc, char ** argv) {

  return overlap_1_test (argc, argv);
}

