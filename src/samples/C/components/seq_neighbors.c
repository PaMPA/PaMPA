/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        seq_neighbors.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <seq_common.h>
#include <seq_iterator.h>


extern int ret_dmesh;

//! \example seq_neighbors.c
//! This file must be combined with:
//! - a file, which begins with seq_,  in "samples/src/C/tools" directory
//! - a mesh file, which matches with "*1-proc*", in "samples/src/C/meshes" directory
int seq_neighbors_test(int argc, char** argv){

  PAMPA_Num vertnbr, edgenbr, enttnbr, *venttab;
  PAMPA_Num baseval, valumax;
  PAMPA_Mesh m;
  PAMPA_Num *verttab, *edgetab, *esubtab;
  PAMPA_Num nodenbr;
  int retval;
  FILE *out;

  if (argc > 1) {
    out = fopen(argv[1], "w");
  }
  else {
    out = stdout;
  }


  // Initialisation of PaMPA mesh structure
  CHECK_PAMPA(PAMPA_meshInit(&m), "pampa_meshinit", EXIT_FAILURE);

  // Build centralized mesh
  structureBuild(&baseval, &vertnbr, &edgenbr, &enttnbr, &valumax, &venttab, &esubtab, &verttab, &edgetab);

  // Build PaMPA mesh
  CHECK_PAMPA(PAMPA_meshBuild(&m, 0, vertnbr, verttab, verttab+1, edgenbr, edgenbr, edgetab, NULL, NULL, enttnbr, venttab, esubtab, NULL, valumax), "pampa_meshbuild", EXIT_FAILURE);

  // Check PaMPA mesh
  if ((retval = PAMPA_meshCheck(&m)) != 0) {
    PAMPA_meshExit(&m);
    structureFree( &venttab, &esubtab, &verttab, &edgetab);
    if (retval == ret_dmesh)
      fprintf(out, "fin du prog\n");
    else
      fprintf(out, "PAMPA_meshCheck: error %d\n", retval);
    return EXIT_SUCCESS;
  }


  PAMPA_meshEnttSize(&m, EDGE, &edgenbr);

  iterator(out, &m, EDGE, NODE);

  iterator(out, &m, INT_EDGE, NODE);

  iterator(out, &m, BND_EDGE, NODE);

  iterator(out, &m, NODE, EDGE);

  iterator(out, &m, NODE, INT_EDGE);

  iterator(out, &m, NODE, BND_EDGE);

  
  // Finalisation of PaMPA mesh structure
  PAMPA_meshExit(&m);

  structureFree( &venttab, &esubtab, &verttab, &edgetab);

  fprintf(out, "fin du prog\n");

  if (argc > 1) {
    fclose(out);
  }

  return EXIT_SUCCESS;
}

int main(int argc, char ** argv) {

  return seq_neighbors_test (argc, argv);
}

