/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        seq_saveall_load.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 21 Jul 2015
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <seq_common.h>
#include <time.h>

extern int ret_dmesh;

//! \example seq_saveall_load.c
//! This file must be combined with:
//! - a file, which doesn't begin by seq_,  in "samples/src/C/tools" directory
//! - a mesh file, which doesn't match with "*1-proc*", in "samples/src/C/meshes" directory
//!
//! For example, it is coupled with tools/it.c and meshes/11-vert_2-proc.c, the
//! output will be :
//! \verbinclude tests/build_check_11-vert_2-proc_it_c
int seq_saveall_load_test(int argc, char** argv){

  int rank, retval;
  PAMPA_Num vertnbr, vertmax, edgenbr, edgesiz, enttnbr;
  PAMPA_Num *vendtab, *venttab, *esubtab, *edlotab, *enlotab;
  PAMPA_Num baseval, valumax;
  PAMPA_Mesh m;
  PAMPA_Num *verttab, *edgetab;
  int seednum;
  FILE *out, *stream;
  char s[100];

  if (argc > 1) {
    out = fopen(argv[1], "w");
  }
  else {
    out = stdout;
  }


  // Initialisation of PaMPA mesh structure
  CHECK_PAMPA(PAMPA_meshInit(&m), "pampa_meshinit", EXIT_FAILURE);

  // Build distributed mesh
  structureBuild(&baseval, &vertnbr, &edgenbr, &enttnbr, &valumax, &venttab, &esubtab, &verttab, &edgetab);

  // Build PaMPA mesh
  CHECK_PAMPA(PAMPA_meshBuild(&m, 0, vertnbr, verttab, NULL, NULL, edgenbr, edgenbr, edgetab, NULL, enttnbr, venttab, esubtab, NULL, valumax), "pampa_meshbuild", EXIT_FAILURE);

  // Check PaMPA mesh
  if ((retval = PAMPA_meshCheck(&m)) != 0) {
    PAMPA_meshExit(&m);
    structureFree( &venttab, &esubtab, &verttab, &edgetab);
    if (retval == ret_dmesh)
      fprintf(out, "fin du prog\n");
    else
      fprintf(out, "PAMPA_meshCheck: error %d\n", retval);
    return EXIT_SUCCESS;
  }


  srand (time(NULL));
  seednum = rand() * 10000 + rand();
  sprintf (s, "/tmp/out-%d", seednum);
  stream  = fopen (s, "w");
  CHECK_PAMPA(PAMPA_meshSaveAll(&m, stream), "pampa_meshcheck", EXIT_FAILURE);
  fclose (stream);

  structureFree( &venttab, &esubtab, &verttab, &edgetab);

  // Finalisation of PaMPA mesh structure
  PAMPA_meshExit(&m);

  CHECK_PAMPA(PAMPA_meshInit(&m), "pampa_meshinit", EXIT_FAILURE);
  stream  = fopen (s, "r");
  CHECK_PAMPA(PAMPA_meshLoad(&m, stream, 0, 0), "pampa_meshload", EXIT_FAILURE);
  fclose (stream);
  remove (s);

  // Check PaMPA mesh
  CHECK_PAMPA(PAMPA_meshCheck(&m), "pampa_meshcheck", EXIT_FAILURE);

  PAMPA_meshEnttSize(&m, EDGE, &edgenbr);

  iterator(out, &m, EDGE, NODE);

  iterator(out, &m, INT_EDGE, NODE);

  iterator(out, &m, BND_EDGE, NODE);

  iterator(out, &m, NODE, EDGE);

  iterator(out, &m, NODE, INT_EDGE);

  iterator(out, &m, NODE, BND_EDGE);

  // Finalisation of PaMPA mesh structure
  PAMPA_meshExit(&m);

  fprintf(out, "fin du prog\n");

  if (argc > 1) {
    fclose(out);
  }

  return EXIT_SUCCESS;
}

int main(int argc, char ** argv) {

  return seq_saveall_load_test (argc, argv);
}

