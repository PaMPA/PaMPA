/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        11-vert_2-proc_many-entt.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 24 Feb 2012
//!                             to:   26 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <common.h>

int ret_dmesh = 0;


//! \example 11-vert_2-proc_many-entt.c
//! \image latex meshes/null.pdf
//! \image html meshes/null.png
int structureBuild(
    int          rank,
    PAMPA_Num *  baseval,
    PAMPA_Num *  vertlocnbr,
    PAMPA_Num *  vertlocmax,
    PAMPA_Num *  edgelocnbr,
    PAMPA_Num *  edgelocsiz,
    PAMPA_Num *  enttglbnbr,
    PAMPA_Num *  valuglbmax,
    PAMPA_Num ** ventloctab,
    PAMPA_Num ** esubloctab,
    PAMPA_Num ** vertloctab,
    PAMPA_Num ** vendloctab,
    PAMPA_Num ** edgeloctab,
    PAMPA_Num ** edloloctab,
    PAMPA_Num ** enloglbtab) {
  PAMPA_Num enttnum;
  PAMPA_Num enttnnd;

  *baseval = 0;
  *enttglbnbr = 100;
  *valuglbmax = 50;
  
  (*vendloctab) = NULL;
  (*edloloctab) = NULL;
  (*enloglbtab) = NULL;
  (*esubloctab) = malloc(sizeof(PAMPA_Num) * (*enttglbnbr));

  for (enttnum = *baseval, enttnnd = *enttglbnbr + *baseval ; enttnum < enttnnd ; enttnum ++)
    (*esubloctab)[enttnum] = PAMPA_ENTT_SINGLE;
  
  (*esubloctab)[ELEM]     = PAMPA_ENTT_SINGLE;
  (*esubloctab)[EDGE]     = PAMPA_ENTT_STABLE;
  (*esubloctab)[BND_EDGE] = EDGE;
  (*esubloctab)[INT_EDGE] = EDGE;
  (*esubloctab)[NODE]     = PAMPA_ENTT_SINGLE;

  if (rank == 0){
    *vertlocnbr = 6;
    *vertlocmax = 6;
    *edgelocnbr = 25;
    *edgelocsiz = 25;
    (*ventloctab) = malloc(sizeof(PAMPA_Num)* (*vertlocmax));
    (*ventloctab)[0] = NODE;
    (*ventloctab)[1] = BND_EDGE;
    (*ventloctab)[2] = ELEM;
    (*ventloctab)[3] = INT_EDGE;
    (*ventloctab)[4] = NODE;
    (*ventloctab)[5] = BND_EDGE;
    (*vertloctab) = malloc(sizeof(PAMPA_Num)* (*vertlocmax + 1));
    (*vertloctab)[0] = 0;
    (*vertloctab)[1] = 5;
    (*vertloctab)[2] = 8;
    (*vertloctab)[3] = 15;
    (*vertloctab)[4] = 19;
    (*vertloctab)[5] = 22;
    (*vertloctab)[6] = 25;
    (*edgeloctab) = malloc(sizeof(PAMPA_Num)* (*edgelocsiz));
    // 0
    (*edgeloctab)[0] = 1;
    (*edgeloctab)[1] = 2;
    (*edgeloctab)[2] = 3;
    (*edgeloctab)[3] = 7;
    (*edgeloctab)[4] = 9;

    // 1
    (*edgeloctab)[5] = 0;
    (*edgeloctab)[6] = 2;
    (*edgeloctab)[7] = 4;

    // 2
    (*edgeloctab)[8] = 0;
    (*edgeloctab)[9] = 1;
    (*edgeloctab)[10] = 3;
    (*edgeloctab)[11] = 4;
    (*edgeloctab)[12] = 5;
    (*edgeloctab)[13] = 6;
    (*edgeloctab)[14] = 9;

    // 3
    (*edgeloctab)[15] = 0;
    (*edgeloctab)[16] = 2;
    (*edgeloctab)[17] = 9;
    (*edgeloctab)[18] = 6;

    // 4
    (*edgeloctab)[19] = 1;
    (*edgeloctab)[20] = 2;
    (*edgeloctab)[21] = 5;

    // 5
    (*edgeloctab)[22] = 4;
    (*edgeloctab)[23] = 2;
    (*edgeloctab)[24] = 6;
  }
  else{
    *vertlocnbr = 5;
    *vertlocmax = 5;
    *edgelocnbr = 21;
    *edgelocsiz = 21;
    (*ventloctab) = malloc(sizeof(PAMPA_Num)* (*vertlocmax));
    (*ventloctab)[0] = NODE;
    (*ventloctab)[1] = BND_EDGE;
    (*ventloctab)[2] = NODE;
    (*ventloctab)[3] = ELEM;
    (*ventloctab)[4] = BND_EDGE;
    (*vertloctab) = malloc(sizeof(PAMPA_Num)* (*vertlocmax + 1));
    (*vertloctab)[0] = 0;
    (*vertloctab)[1] = 5;
    (*vertloctab)[2] = 8;
    (*vertloctab)[3] = 11;
    (*vertloctab)[4] = 18;
    (*vertloctab)[5] = 21;
    (*edgeloctab) = malloc(sizeof(PAMPA_Num)* (*edgelocsiz));
    // 6
    (*edgeloctab)[0] = 5;
    (*edgeloctab)[1] = 2;
    (*edgeloctab)[2] = 3;
    (*edgeloctab)[3] = 9;
    (*edgeloctab)[4] = 10;

    // 7
    (*edgeloctab)[5] = 0;
    (*edgeloctab)[6] = 9;
    (*edgeloctab)[7] = 8;

    // 8
    (*edgeloctab)[8] = 7;
    (*edgeloctab)[9] = 9;
    (*edgeloctab)[10] = 10;

    // 9
    (*edgeloctab)[11] = 0;
    (*edgeloctab)[12] = 7;
    (*edgeloctab)[13] = 8;
    (*edgeloctab)[14] = 3;
    (*edgeloctab)[15] = 10;
    (*edgeloctab)[16] = 6;
    (*edgeloctab)[17] = 2;

    // 10
    (*edgeloctab)[18] = 8;
    (*edgeloctab)[19] = 9;
    (*edgeloctab)[20] = 6;
  }
  return (0);
}

