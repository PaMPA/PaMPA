/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        11-vert_1-proc_wrong.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 24 Feb 2012
//!                             to:   22 Dec 2015
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <seq_common.h>

int ret_dmesh = PAMPA_ERR_LNK_ELEMENT;


//! \example 11-vert_1-proc_wrong.c
//! \image latex meshes/null.pdf
//! \image html meshes/null.png
int structureBuild(
    PAMPA_Num *  baseval,
    PAMPA_Num *  vertnbr,
    PAMPA_Num *  edgenbr,
    PAMPA_Num *  enttnbr,
    PAMPA_Num *  valumax,
    PAMPA_Num ** venttab,
    PAMPA_Num ** esubtab,
    PAMPA_Num ** verttab,
    PAMPA_Num ** edgetab) {

  *baseval = 0;
  *enttnbr = 5;
  *valumax = 0;

  (*esubtab) = malloc(sizeof(PAMPA_Num) * (*enttnbr));
  (*esubtab)[ELEM]     = PAMPA_ENTT_SINGLE;
  (*esubtab)[EDGE]     = PAMPA_ENTT_STABLE;
  (*esubtab)[BND_EDGE] = EDGE;
  (*esubtab)[INT_EDGE] = EDGE;
  (*esubtab)[NODE]     = PAMPA_ENTT_SINGLE;

  *vertnbr = 11;
  *edgenbr = 44;
  (*venttab) = malloc(sizeof(PAMPA_Num)* (*vertnbr));
  (*venttab)[0] = NODE;
  (*venttab)[1] = BND_EDGE;
  (*venttab)[2] = ELEM;
  (*venttab)[3] = INT_EDGE;
  (*venttab)[4] = NODE;
  (*venttab)[5] = BND_EDGE;
  (*venttab)[6] = NODE;
  (*venttab)[7] = BND_EDGE;
  (*venttab)[8] = NODE;
  (*venttab)[9] = ELEM;
  (*venttab)[10] = BND_EDGE;

  (*verttab) = malloc(sizeof(PAMPA_Num)* (*vertnbr + 1));
  (*verttab)[0] = 0;
  (*verttab)[1] = 5;
  (*verttab)[2] = 8;
  (*verttab)[3] = 14;
  (*verttab)[4] = 18;
  (*verttab)[5] = 21;
  (*verttab)[6] = 24;
  (*verttab)[7] = 29;
  (*verttab)[8] = 32;
  (*verttab)[9] = 35;
  (*verttab)[10] = 41;
  (*verttab)[11] = 44;
  (*edgetab) = malloc(sizeof(PAMPA_Num)* (*edgenbr));
  // 0
  (*edgetab)[0] = 1;
  (*edgetab)[1] = 2;
  (*edgetab)[2] = 3;
  (*edgetab)[3] = 7;
  (*edgetab)[4] = 9;

  // 1
  (*edgetab)[5] = 0;
  (*edgetab)[6] = 2;
  (*edgetab)[7] = 4;

  // 2
  (*edgetab)[8] = 0;
  (*edgetab)[9] = 1;
  (*edgetab)[10] = 3;
  (*edgetab)[11] = 4;
  (*edgetab)[12] = 5;
  (*edgetab)[13] = 6;

  // 3
  (*edgetab)[14] = 0;
  (*edgetab)[15] = 2;
  (*edgetab)[16] = 9;
  (*edgetab)[17] = 6;

  // 4
  (*edgetab)[18] = 1;
  (*edgetab)[19] = 2;
  (*edgetab)[20] = 5;

  // 5
  (*edgetab)[21] = 4;
  (*edgetab)[22] = 2;
  (*edgetab)[23] = 6;

  // 6
  (*edgetab)[24] = 5;
  (*edgetab)[25] = 2;
  (*edgetab)[26] = 3;
  (*edgetab)[27] = 9;
  (*edgetab)[28] = 10;

  // 7
  (*edgetab)[29] = 0;
  (*edgetab)[30] = 9;
  (*edgetab)[31] = 8;

  // 8
  (*edgetab)[32] = 7;
  (*edgetab)[33] = 9;
  (*edgetab)[34] = 10;

  // 9
  (*edgetab)[35] = 0;
  (*edgetab)[36] = 7;
  (*edgetab)[37] = 8;
  (*edgetab)[38] = 3;
  (*edgetab)[39] = 10;
  (*edgetab)[40] = 6;

  // 10
  (*edgetab)[41] = 8;
  (*edgetab)[42] = 9;
  (*edgetab)[43] = 6;

  return (0);
}

int structureFree(
    PAMPA_Num ** venttab,
    PAMPA_Num ** esubtab,
    PAMPA_Num ** verttab,
    PAMPA_Num ** edgetab) {

  free( *venttab);
  free( *esubtab);
  free( *verttab);
  free( *edgetab);

  return (0);
}
