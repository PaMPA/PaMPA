/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        17-vert.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 23 Dec 2015
//!                             to:   29 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <common2.h>


int ret_dmesh = 0;


//! \example 17-vert2_2-proc.c
//! \image latex meshes/null.pdf
//! \image html meshes/null.png
int structureBuild(
    int          rank,
    PAMPA_Num *  baseval,
    PAMPA_Num *  vertlocnbr,
    PAMPA_Num *  vertlocmax,
    PAMPA_Num *  edgelocnbr,
    PAMPA_Num *  edgelocsiz,
    PAMPA_Num *  enttglbnbr,
    PAMPA_Num *  valuglbmax,
    PAMPA_Num ** ventloctab,
    PAMPA_Num ** esubloctab,
    PAMPA_Num ** vertloctab,
    PAMPA_Num ** vendloctab,
    PAMPA_Num ** edgeloctab,
    PAMPA_Num ** edloloctab,
    PAMPA_Num ** enloglbtab) {
  PAMPA_Num vertlocbas, vertlocidx, vertlocnnd, edgelocidx, vertglbnbr, edgeglbnbr;
  int procnum, size, *proccnttab, *procdsptab;

  *baseval = 0;
  *enttglbnbr = 7;
  *valuglbmax = 50;

  (*vendloctab) = NULL;
  (*edloloctab) = NULL;
  (*enloglbtab) = NULL;
  (*esubloctab) = malloc(sizeof(PAMPA_Num) * (*enttglbnbr));
  (*esubloctab)[ELEM]     = PAMPA_ENTT_STABLE;
  (*esubloctab)[QUAD]     = ELEM;
  (*esubloctab)[TRIA]     = ELEM;
  (*esubloctab)[EDGE]     = PAMPA_ENTT_STABLE;
  (*esubloctab)[BND_EDGE] = EDGE;
  (*esubloctab)[INT_EDGE] = EDGE;
  (*esubloctab)[NODE]     = PAMPA_ENTT_SINGLE;


  vertglbnbr = 17;
  edgeglbnbr = 76;

  MPI_Comm_size (MPI_COMM_WORLD, &size);
    *vertlocnbr = DATASIZE (vertglbnbr, size, rank);
  *vertlocmax += *vertlocnbr;
  *edgelocnbr = 
    *edgelocsiz = edgeglbnbr;

  /* First use of vertlocbas according to vertlocmax */
  MPI_Scan (vertlocmax, &vertlocbas, 1, PAMPA_MPI_NUM, MPI_SUM, MPI_COMM_WORLD);
  vertlocbas += (*baseval) - (*vertlocmax);

  (*ventloctab) = malloc(sizeof(PAMPA_Num) * (*vertlocnbr));
  (*vertloctab) = malloc(sizeof(PAMPA_Num) * (*vertlocnbr + 1));
  (*edgeloctab) = malloc(sizeof(PAMPA_Num) * (*edgelocsiz));
  proccnttab    = malloc(sizeof(int)       * size);
  procdsptab    = malloc(sizeof(int)       * (size + 1));

  edgelocidx = *baseval;

  vertlocidx = *baseval;

  procdsptab[0] = 0;
  for (vertlocbas = *baseval, procdsptab[0] = procnum = 0; procnum < size; procnum ++) {
    proccnttab[procnum] = DATASIZE (vertglbnbr, size, procnum);
    procdsptab[procnum + 1] = procdsptab[procnum] + proccnttab[procnum];
    /* Second use of vertlocbas according to vertlocnbr */
    if (procnum < rank)
      vertlocbas += proccnttab[procnum];
  }
  vertlocnnd = vertlocbas + (*vertlocnbr);

  if (((vertlocbas + vertlocidx) == 0) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] = edgelocidx;
    (*ventloctab)[vertlocidx ++] = NODE;
    // 0
    (*edgeloctab)[edgelocidx ++] = 1;
    (*edgeloctab)[edgelocidx ++] = 3;
    (*edgeloctab)[edgelocidx ++] = 4;
  }

  if (((vertlocbas + vertlocidx) == 1) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = BND_EDGE;
    // 1
    (*edgeloctab)[edgelocidx ++] = 0;
    (*edgeloctab)[edgelocidx ++] = 2;
    (*edgeloctab)[edgelocidx ++] = 4;
  }

  if (((vertlocbas + vertlocidx) == 2) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = NODE;
    // 2
    (*edgeloctab)[edgelocidx ++] = 1;
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 5;
  }

  if (((vertlocbas + vertlocidx) == 3) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = BND_EDGE;
    // 3
    (*edgeloctab)[edgelocidx ++] = 0;
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 6;
  }

  if (((vertlocbas + vertlocidx) == 4) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = QUAD;
    // 4
    (*edgeloctab)[edgelocidx ++] = 0;
    (*edgeloctab)[edgelocidx ++] = 3;
    (*edgeloctab)[edgelocidx ++] = 6;
    (*edgeloctab)[edgelocidx ++] = 1;
    (*edgeloctab)[edgelocidx ++] = 7;
    (*edgeloctab)[edgelocidx ++] = 2;
    (*edgeloctab)[edgelocidx ++] = 5;
    (*edgeloctab)[edgelocidx ++] = 8;
    (*edgeloctab)[edgelocidx ++] = 9;
  }

  if (((vertlocbas + vertlocidx) == 5) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = BND_EDGE;
    // 5
    (*edgeloctab)[edgelocidx ++] = 2;
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 8;
  }


  if (((vertlocbas + vertlocidx) == 6) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = NODE;
    // 6
    (*edgeloctab)[edgelocidx ++] = 3;
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 7;
    (*edgeloctab)[edgelocidx ++] = 9;
    (*edgeloctab)[edgelocidx ++] = 10;
  }

  if (((vertlocbas + vertlocidx) == 7) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = INT_EDGE;
    // 7
    (*edgeloctab)[edgelocidx ++] = 6;
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 8;
    (*edgeloctab)[edgelocidx ++] = 9;
  }

  if (((vertlocbas + vertlocidx) == 8) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = NODE;
    // 8
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 5;
    (*edgeloctab)[edgelocidx ++] = 7;
    (*edgeloctab)[edgelocidx ++] = 9;
    (*edgeloctab)[edgelocidx ++] = 11;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 12;
  }

  if (((vertlocbas + vertlocidx) == 9) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = TRIA;
    // 9
    (*edgeloctab)[edgelocidx ++] = 4;
    (*edgeloctab)[edgelocidx ++] = 7;
    (*edgeloctab)[edgelocidx ++] = 8;
    (*edgeloctab)[edgelocidx ++] = 6;
    (*edgeloctab)[edgelocidx ++] = 10;
    (*edgeloctab)[edgelocidx ++] = 11;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 14;
  }

  if (((vertlocbas + vertlocidx) == 10) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = BND_EDGE;
    // 10
    (*edgeloctab)[edgelocidx ++] = 9;
    (*edgeloctab)[edgelocidx ++] = 6;
    (*edgeloctab)[edgelocidx ++] = 14;
  }

  if (((vertlocbas + vertlocidx) == 11) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = INT_EDGE;
    // 11
    (*edgeloctab)[edgelocidx ++] = 8;
    (*edgeloctab)[edgelocidx ++] = 9;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 14;
  }

  if (((vertlocbas + vertlocidx) == 12) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = BND_EDGE;
    // 12
    (*edgeloctab)[edgelocidx ++] = 8;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 16;
  }

  if (((vertlocbas + vertlocidx) == 13) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = TRIA;
    // 13
    (*edgeloctab)[edgelocidx ++] = 8;
    (*edgeloctab)[edgelocidx ++] = 9;
    (*edgeloctab)[edgelocidx ++] = 11;
    (*edgeloctab)[edgelocidx ++] = 12;
    (*edgeloctab)[edgelocidx ++] = 14;
    (*edgeloctab)[edgelocidx ++] = 15;
    (*edgeloctab)[edgelocidx ++] = 16;
  }

  if (((vertlocbas + vertlocidx) == 14) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = NODE;
    // 14
    (*edgeloctab)[edgelocidx ++] = 9;
    (*edgeloctab)[edgelocidx ++] = 10;
    (*edgeloctab)[edgelocidx ++] = 11;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 15;
  }

  if (((vertlocbas + vertlocidx) == 15) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = BND_EDGE;
    // 15
    (*edgeloctab)[edgelocidx ++] = 14;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 16;
  }

  if (((vertlocbas + vertlocidx) == 16) && ((vertlocbas + vertlocidx) < vertlocnnd)) {
    (*vertloctab)[vertlocidx] =  edgelocidx;
    (*ventloctab)[vertlocidx ++] = NODE;
    // 16
    (*edgeloctab)[edgelocidx ++] = 12;
    (*edgeloctab)[edgelocidx ++] = 13;
    (*edgeloctab)[edgelocidx ++] = 15;
  }

  (*vertloctab)[vertlocidx] =  edgelocidx;
  *edgelocnbr =
  *edgelocsiz = edgelocidx;
  *edgeloctab = realloc (*edgeloctab, (*edgelocsiz) * sizeof (PAMPA_Num));

  free (proccnttab);
  free (procdsptab);
  return (0);
}

