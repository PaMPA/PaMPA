/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        common.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 31 May 2011
//!                             to:   22 Dec 2015
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <pampa.h>
#include <string.h>

#define DATASIZE(n,p,i)             ((PAMPA_Num) (((n) + ((p) - 1 - (i))) / (p)))

#ifdef SAMPLE_TIME_CHECK
#define CHECK_PAMPA(t,m,e) { \
  int cheklocvl2; \
  time_t begin = time (NULL); \
  cheklocvl2 = (t); \
  PAMPA_infoPrint("%s: %s Elapsed time %lu secondes, line %d", __FILE__, (m), (time(NULL) - begin), (__LINE__)); \
  if(0!=(cheklocvl2)){ \
    fprintf(stderr, "%s: %s KO at line %d\n", __FILE__, m, __LINE__);exit(e); \
  } \
}
#else /* SAMPLE_TIME_CHECK */
#define CHECK_PAMPA(t,m,e) { \
  int cheklocvl2; \
  cheklocvl2 = (t); \
  if(0!=(cheklocvl2)){ \
    fprintf(stderr, "%s: %s KO at line %d\n", __FILE__, m, __LINE__);exit(e); \
  } \
}
#endif /* SAMPLE_TIME_CHECK */

#define ELEM 0
#define EDGE 1
#define BND_EDGE 2
#define INT_EDGE 3
#define NODE 4


int structureBuild(
    int          rank,
    PAMPA_Num *  baseval,
    PAMPA_Num *  vertlocnbr,
    PAMPA_Num *  vertlocmax,
    PAMPA_Num *  edgelocnbr,
    PAMPA_Num *  edgelocsiz,
    PAMPA_Num *  enttglbnbr,
    PAMPA_Num *  valuglbmax,
    PAMPA_Num ** ventloctab,
    PAMPA_Num ** esubloctab,
    PAMPA_Num ** vertloctab,
    PAMPA_Num ** vendloctab,
    PAMPA_Num ** edgeloctab,
    PAMPA_Num ** edloloctab,
    PAMPA_Num ** enloglbtab);

int structureFree(
    PAMPA_Num ** ventloctab,
    PAMPA_Num ** esubloctab,
    PAMPA_Num ** vertloctab,
    PAMPA_Num ** vendloctab,
    PAMPA_Num ** edgeloctab,
    PAMPA_Num ** edloloctab,
    PAMPA_Num ** enloglbtab);
