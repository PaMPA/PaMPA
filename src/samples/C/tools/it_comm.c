/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        it_comm.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 17 Feb 2012
//!                             to:   21 Jan 2015
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <common.h>

//! \example it_comm.c
void iterator(FILE *out, int rank, PAMPA_Dmesh *mesh, PAMPA_Num ventlocnum, PAMPA_Num nentlocnum) {
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it_vert;
  int *data;
  PAMPA_DmeshHaloReqs requests;
  static int tag = 2;

  tag ++;

  PAMPA_dmeshItInit(mesh, ventlocnum, nentlocnum, &it_nghb) ;
  CHECK_PAMPA(PAMPA_dmeshValueLink(mesh, (void **) &data, PAMPA_VALUE_PUBLIC, MPI_INT, nentlocnum, tag), "pampa_dmeshvaluelink", EXIT_FAILURE);
  
  PAMPA_dmeshItInitStart(mesh, nentlocnum, PAMPA_VERT_LOCAL, &it_vert) ;
  while (PAMPA_itHasMore(&it_vert)) {
    PAMPA_Num vertlocnum;

	if (PAMPA_itIsSubEntt(&it_vert))
      vertlocnum = PAMPA_itCurSubEnttVertNum(&it_vert);
    else
      vertlocnum = PAMPA_itCurEnttVertNum(&it_vert);

    data[vertlocnum] = (rank + 1) * 100 + vertlocnum;
    PAMPA_itNext(&it_vert);
  }
  PAMPA_dmeshHaloValueAsync(mesh, nentlocnum, tag, &requests);

  PAMPA_dmeshItInitStart(mesh, ventlocnum, PAMPA_VERT_INTERNAL, &it_vert) ;
  while (PAMPA_itHasMore(&it_vert)) {
    int sum;
    PAMPA_Num vertlocnum;

    sum = 0;

	if (PAMPA_itIsSubEntt(&it_vert))
      vertlocnum = PAMPA_itCurSubEnttVertNum(&it_vert);
    else
      vertlocnum = PAMPA_itCurEnttVertNum(&it_vert);

    PAMPA_itStart(&it_nghb, vertlocnum) ;

    while (PAMPA_itHasMore(&it_nghb)) {

      PAMPA_Num nghblocnum;

	  if (PAMPA_itIsSubEntt(&it_nghb))
        nghblocnum = PAMPA_itCurSubEnttVertNum(&it_nghb);
      else
        nghblocnum = PAMPA_itCurEnttVertNum(&it_nghb);

      sum += data[nghblocnum];
      PAMPA_itNext(&it_nghb);
    }
    vertlocnum = PAMPA_itCurMeshVertNum(&it_vert);
    fprintf(out, "sum of %d = %d\n", vertlocnum, sum);
    PAMPA_itNext(&it_vert);
  }

  PAMPA_dmeshHaloWait (&requests);

  PAMPA_dmeshItInitStart(mesh, ventlocnum, PAMPA_VERT_BOUNDARY, &it_vert) ;
  while (PAMPA_itHasMore(&it_vert)) {
    int sum;

    sum = 0;

    PAMPA_Num vertlocnum;

	if (PAMPA_itIsSubEntt(&it_vert))
      vertlocnum = PAMPA_itCurSubEnttVertNum(&it_vert);
    else
      vertlocnum = PAMPA_itCurEnttVertNum(&it_vert);

    PAMPA_itStart(&it_nghb, vertlocnum) ;

    while (PAMPA_itHasMore(&it_nghb)) {
      PAMPA_Num nghblocnum;

	  if (PAMPA_itIsSubEntt(&it_nghb))
        nghblocnum = PAMPA_itCurSubEnttVertNum(&it_nghb);
      else
        nghblocnum = PAMPA_itCurEnttVertNum(&it_nghb);

      sum += data[nghblocnum];
      PAMPA_itNext(&it_nghb);
    }
    vertlocnum = PAMPA_itCurMeshVertNum(&it_vert);
    fprintf(out, "sum of %d = %d\n", vertlocnum, sum);
    PAMPA_itNext(&it_vert);
  }
}

