/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        it.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file is a part of a C example
//!
//!   \date        Version 1.0: from: 17 Feb 2012
//!                             to:   21 Jan 2015
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
#include <common.h>

//! \example it.c
void iterator(FILE *out, int rank, PAMPA_Dmesh *mesh, PAMPA_Num ventlocnum, PAMPA_Num nesblocnum) {
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it_vert;

  
  PAMPA_dmeshItInitStart(mesh, ventlocnum, PAMPA_VERT_ANY, &it_vert) ;
  PAMPA_dmeshItInit(mesh, ventlocnum, nesblocnum, &it_nghb) ;
  while (PAMPA_itHasMore(&it_vert)) {
    PAMPA_Num vertnum;

	if (PAMPA_itIsSubEntt(&it_vert))
      vertnum = PAMPA_itCurSubEnttVertNum(&it_vert);
    else
      vertnum = PAMPA_itCurEnttVertNum(&it_vert);

    fprintf(out, "vertnum : %d\n", (int) vertnum);
    PAMPA_itStart(&it_nghb, vertnum) ;

    while (PAMPA_itHasMore(&it_nghb)) {
      PAMPA_Num sngblocnum, engblocnum, mngblocnum, nsublocnum, nentlocnum;

      engblocnum = PAMPA_itCurEnttVertNum(&it_nghb);
      mngblocnum = PAMPA_itCurMeshVertNum(&it_nghb);
	  if (PAMPA_itHasSubEntt(&it_nghb)) {
        sngblocnum = PAMPA_itCurSubEnttVertNum(&it_nghb);
        nsublocnum = PAMPA_itCurSubEnttNum(&it_nghb);
        nentlocnum = PAMPA_itCurEnttNum(&it_nghb);
        fprintf(out, "[%d] neighbor of %d with entity %d : %d(sub: %d) %d(ent: %d) %d(glb)\n", rank, (int) vertnum, (int) ventlocnum, (int) sngblocnum, (int) nsublocnum, (int) engblocnum, (int) nentlocnum, (int) mngblocnum);
	  }
	  else {
        fprintf(out, "[%d] neighbor of %d with entity %d : %d(ent: %d) %d(glb)\n", rank, (int) vertnum, (int) ventlocnum, (int) engblocnum, (int) nesblocnum, (int) mngblocnum);
      }
      PAMPA_itNext(&it_nghb);
    }
    PAMPA_itNext(&it_vert);
  }
}

