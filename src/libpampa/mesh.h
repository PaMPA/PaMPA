/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the centralized source mesh structure. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define MESH_H

/*
** The defines.
*/

/* mesh flags. */

#define MESHNONE                  0x0000        /* No options set                             */

#define MESHFREEPRIV              0x0001        /* Set if private arrays freed on exit        */
#define MESHFREECOMM              0x0002        /* MPI communicator has to be freed           */
#define MESHFREETABS              0x0004        /* Set if venttax freed on exit               */
#define MESHFREEENTT              0x0008        /* Local entity private data have to be freed */
#define MESHFREEVALU              0x0010        /* Set if valutab freed on exit               */
#define MESHFREEALL               (MESHFREEPRIV | MESHFREECOMM | MESHFREETABS | MESHFREEENTT | MESHFREEVALU)

/*
** The type and structure definitions.
*/

/* The mesh basic types, which must be signed. */

#ifndef GNUMMAX                                   
typedef INT                 Gnum;                 /* Vertex or edge number   */
#define GNUMMAX                     (INTVALMAX)   /* Maximum Gnum value      */
#endif /* GNUMMAX */

#define GNUM_MPI                    COMM_INT      /* MPI type for Gnum is MPI type for INT */



typedef int MeshFlag;                           /*+ Mesh property flags +*/




/* The centralized mesh structure. */


typedef struct MeshVertBounds_      //!  XXX attention mettre commentaire adéquate par rapport aux itérateurs (<< 1 [ 1])
{
  Gnum                 vertidx;     //!< Vertex beginning index
  Gnum                 vendidx;     //!< Vertex end index
} MeshVertBounds;

typedef struct MeshEnttNghb_        //!  XXX
{
  MeshVertBounds *    vindtax;      //!< Vertex index array [based]
} MeshEnttNghb;

typedef struct MeshEntity_          //!  Partial mesh structure which are used for iterator
{
  Gnum                 vertnbr;     //!< Number of vertices with this type value
  MeshEnttNghb **      nghbtax;     //!< XXX
  Gnum *               mvrttax;     //!< XXX Array of vertex numbers for group or entity
  Gnum *               venttax;     //!< Array of entity values for each vertex, only for groups
  Gnum *               vprmtax;     //!< XXX tableau de permutation pour les itérateurs de niveau 1
  Gnum *               svrttax;     //!< Array of vertex numbers for entity or sub-entity
} MeshEntity;
  
typedef struct Mesh_   
{
  Gnum                 idnum;       //!< Id
  MeshFlag             flagval;     //!< Mesh properties
  Gnum                 baseval;     //!< Base index for edge/vertex/entity arrays
  Gnum                 enttnbr;     //!< Number of types which make up the mesh
  Gnum                 vertnbr;     //!< Number of vertices
  Gnum *               venttax;     //!< Array of entity values for each vertex XXX à supprimer ???
  Gnum *               esubbax;     //!< XXX
  Gnum                 esubmsk;     //!< XXX
  MeshEntity **        entttax;     //!< Array of entity private data
  Gnum                 edgenbr;     //!< XXX
  Gnum                 edgesiz;     //!< XXX
  Gnum *               edgetax;     //!< XXX
  Gnum *			   edlotax;     //!<
  Gnum                 degrmax;
  Values *             valsptr;     //!< Pointer on associated values
  Gnum                 typemax;     //!< Maximum number of types
  MPI_Comm             proccomm;    //!< Mesh communicator XXX comment fait-on sur les I/O ???
  int                  procglbnbr;  //!< Number of processes sharing mesh data
  int                  proclocnum;  //!< Number of this process
} Mesh;

/*
** The functions prototypes.
*/

int meshInit (
    Mesh * const           meshptr);

void meshExit (
    Mesh * const           meshptr);

void meshFree (
    Mesh * const           meshptr);

int meshBuild (
    Mesh * const           meshptr,
    const Gnum             idnum,
    const Gnum             baseval,
    const Gnum             vertnbr,
    Gnum * const           verttax,
    Gnum * const           vendtax,
    const Gnum             edgenbr,
    const Gnum             edgesiz,
    Gnum * const           edgetax,
    Gnum * const           edlotax,
    Gnum                   enttnbr,
    Gnum *                 venttax,
    Gnum *                 esubtab,
    Gnum                   valumax,
    Values *               valsptr,
    Gnum                   degrmax,
    Gnum                   procglbnbr);

int meshBuild2 (
    Mesh * restrict const  meshptr,
    const Gnum             vertnbr,
    const Gnum             edgesiz,
    Gnum *                 venttax,
    Gnum **                vnbrtax);

int meshCheck (
    Mesh * const           meshptr);

int meshCheck2 (
    Mesh * const           meshptr,
    int                    flagval);

int meshCreate (
    Mesh * restrict const  meshptr,
    const Gnum             vertnbr,
    Gnum * const           verttab,
    Gnum * const           vendtab,
    const Gnum             edgenbr,
    Gnum * restrict const  edgetab,
    Gnum                   enttnbr,
    Gnum *                 venttab,
    const Gnum             baseval,
    Gnum                   degnghb);

int meshItBuild (
    const Mesh * const     meshptr,
    Gnum const             ventnum);

int meshLoad (
    Mesh * restrict const  meshptr, 
    FILE * const           stream, 
    const Gnum             baseval,
    const Gnum             flagval);

#if (defined STRAT_H) && (defined ARCH_H)
int meshMapElem (
    Mesh * const           meshptr,
    const Gnum             partnbr,
    Arch * const           archptr,
    Strat * const          stratptr,
    Gnum * const           maptax);
#endif /* STRAT_H && ARCH_H */

int meshMeshSave (
    Mesh * const           meshptr,
    FILE * const           meshstm,
    FILE * const           solstm);

int meshSave (
    Mesh * restrict const  meshptr,
    FILE * const           stream);

int meshSaveAll (
    Mesh * restrict const  meshptr,
    FILE * const           stream);

int meshSurfaceSave (
    Mesh * const           meshptr,
    FILE * const           meshstm,
    FILE * const           solstm);

int meshValueLink (
    Mesh * const           meshptr,
    void **                valutab,
    Gnum                   flagval,
    Gnum *                 sizeval,
    Gnum *                 typesiz,
    MPI_Datatype           typeval,
    Gnum                   enttnum,
    Gnum                   tagnum);

int meshValueUnlink (
    Mesh * const           meshptr,
    Gnum                   enttnum,
    Gnum                   tagnum);

int meshValueData (
    const Mesh * const     meshptr,
    Gnum const             enttnum,
    Gnum                   tagnum,
    Gnum *                 sizeval,
    Gnum *		   typesiz,
    void **                valutab);
