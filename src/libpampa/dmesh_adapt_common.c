/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt_common.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 Oct 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_ADAPT

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "pampa.h"
#include "pampa.h"

#include <ptscotch.h>
#include "dmesh_adapt.h"
#include "dmesh_adapt_common.h"
#include "dmesh_dgraph.h"
#include "dmesh_gather_induce_multiple.h"
#include "dmesh_rebuild.h"

/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshAdaptBase (
    Dmesh * const    srcmeshptr,              /**< mesh */
	const Gnum                bandval,              /**< Band value */
	const Gnum                ballsiz,
        int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
        int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
        int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
        int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
        int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
	void * const              dataptr,
	Dmesh * const    dstmeshptr)
{
  Gnum baseval;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  double * restrict veloloctax;
  Gnum * restrict rmshloctax;
  double taulocval;
  double tauglbval;
  MPI_Comm proccomm;
  int cheklocval;
  Gnum loadmin;

  cheklocval = 0;
  loadmin = 4;
  baseval = srcmeshptr->baseval;
  proccomm = srcmeshptr->proccomm;

  printf("*******************\n\nAdaptBase\n*******************\nLoad min : %d\n*******************\nFirst iteration\n", loadmin);

  do {
    double reduloctab[2];
    double reduglbtab[2];
    Gnum vertlocnum;
    Gnum vertlocnnd;

    //CHECK_FERR (dmeshAdaptGrow (srcmeshptr, bandval, ballsiz, PAMPA_remesh, dataptr, dstmeshptr), proccomm);
    //XXX à corriger
    //CHECK_FERR (dmeshAdaptPart (srcmeshptr, bandval, ballsiz, EXT_meshAdapt, EXT_dmeshCheck, EXT_dmeshBand, EXT_dmeshMetCalc, EXT_meshSave, dataptr, dstmeshptr), proccomm);
    //XXX fin à corriger

    if ((veloloctax = (double *) memAlloc (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (double))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    veloloctax -= baseval;

    CHECK_FERR (EXT_dmeshMetCalc ((PAMPA_Dmesh *) srcmeshptr, dataptr, srcmeshptr->enttloctax[baseval]->vertlocnbr, NULL, veloloctax), proccomm);

    CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
    rmshloctax -= baseval;

    for (taulocval = rmshlocnbr = 0, vertlocnum = baseval, vertlocnnd = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
      taulocval += veloloctax[vertlocnum];

      if (veloloctax[vertlocnum] >= loadmin) {
        rmshlocnbr ++;
        rmshloctax[vertlocnum] = PAMPA_TAG_VERT_REMESH;
      }
      else
        rmshloctax[vertlocnum] = PAMPA_TAG_VERT_NOREMESH;
    }


    memFree (veloloctax + baseval);

    reduloctab[0] = (double) rmshlocnbr;
    reduloctab[1] = taulocval;

    CHECK_VDBG (cheklocval, proccomm);
    if (commAllreduce (reduloctab, reduglbtab, 2, MPI_DOUBLE, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    rmshglbnbr = (Gnum) reduglbtab[0];
    tauglbval = reduglbtab[1];
    tauglbval /= (6 * srcmeshptr->vertglbnbr);
    printf("*******************\n\nAdaptBase\n*******************\nElement to remesh : %d\n", rmshlocnbr);
    errorPrint ("ratio rmshglb / vertglb : %.10lf, tau : %.10lf", 1.0 * rmshglbnbr / srcmeshptr->vertglbnbr, tauglbval);
  } while (rmshglbnbr > 0);

  CHECK_VDBG (cheklocval, proccomm);
  return (0);
}


/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

int dmeshAdapt2 (
	Dmesh * restrict const meshptr,
	SCOTCH_Dgraph * fgrfptr,
	Gnum *          fvrtloctax,
	SCOTCH_Dgraph * cgrfptr,
	Gnum *          cprcvrttab,
	const Gnum      coarval,
	const Gnum      coarnbr,
	const double    coarrat)
{
  Gnum baseval;
  Gnum cvrtlocbas;
  Gnum cvrtlocnbr;
  Gnum tvrtlocnbr;
  Gnum fvrtlocbas;
  Gnum fvrtlocnbr;
  Gnum fvrtglbnbr;
  Gnum fvrtlocmx2;
  int procngbnum;
  int procngbnnd;
  Gnum datasndnbr;
  Gnum datarcvnbr;
  Gnum datarcvidx;
  Gnum * restrict multloctax;
  Gnum * restrict tvrtloctax;
  int * restrict dsndcnttab;
  int * restrict dsnddsptab;
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  Gnum * restrict fprcngbtab;
  Gnum * restrict fprcvrttab;
  Gnum * restrict datasndtab;
  Gnum * restrict datarcvtab;
  MPI_Comm proccomm;
  int        cheklocval;

  cheklocval = 0;
  baseval = meshptr->baseval;
  proccomm = meshptr->proccomm;

  SCOTCH_dgraphData (fgrfptr, NULL, &fvrtglbnbr, &fvrtlocnbr, &fvrtlocmx2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 


  if (memAllocGroup ((void **) (void *)
		&fprcngbtab,  (size_t) (meshptr->procglbnbr       * sizeof (Gnum)),
		&fprcvrttab,  (size_t) ((meshptr->procglbnbr + 1) * sizeof (Gnum)),
        &multloctax,  (size_t) (2 * fvrtlocnbr            * sizeof (Gnum)),
        &dsndcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
        &dsnddsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
        &drcvcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
        &drcvdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
		NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  multloctax -= 2 * baseval;
  memSet (dsndcnttab, 0, meshptr->procglbnbr * sizeof (Gnum));

  CHECK_VDBG (cheklocval, proccomm);
  if (commAllgather (&fvrtlocmx2, 1, GNUM_MPI,
        fprcngbtab, 1, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  fprcvrttab[0] = baseval;
  for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) 
    fprcvrttab[procngbnum + 1] = fprcvrttab[procngbnum] + fprcngbtab[procngbnum];

  fvrtlocbas = fprcvrttab[meshptr->proclocnum] - baseval;
  const Gnum fvrtlocmin = fprcvrttab[meshptr->proclocnum];
  const Gnum fvrtlocmax = fprcvrttab[meshptr->proclocnum + 1] - 1;

  // arrivée à la fin de la récursion
  if ((coarval == 0) || (fvrtglbnbr == 1)) {
	Gnum cvrtlocmax;
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;
	Gnum * restrict cprcngbtab;

	//CHECK_FERR (SCOTCH_dgraphCoarsen (fgrfptr, coarnbr, coarrat, SCOTCH_COARSENNOMERGE, cgrfptr, multloctax + 2 * baseval), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (cgrfptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

  	SCOTCH_dgraphData (cgrfptr, NULL, NULL, &cvrtlocnbr, &cvrtlocmax, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 

  	if ((cprcngbtab = (Gnum *) memAlloc (meshptr->procglbnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllgather (&cvrtlocmax, 1, GNUM_MPI,
          cprcngbtab, 1, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

  	cprcvrttab[0] = baseval;
  	for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) 
      cprcvrttab[procngbnum + 1] = cprcvrttab[procngbnum] + cprcngbtab[procngbnum];

	memFree (cprcngbtab);
	cvrtlocbas = cprcvrttab[meshptr->proclocnum] - baseval;
	for (cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval;
		cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
        fvrtlocnum = multloctax[2 * cvrtlocnum + multdsp];

		if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
		  }
		  dsndcnttab[procngbnum] ++;
		}
		else 
		  fvrtloctax[fvrtlocnum - fvrtlocbas] = cvrtlocnum + cvrtlocbas;
	  }
	}
  }
  // n-ième étape de la récursion
  else {
  	SCOTCH_Dgraph tgrfdat; /* Temporary graph */
	Gnum tvrtlocnum;
	Gnum tvrtlocnnd;

	CHECK_FERR (SCOTCH_dgraphInit (&tgrfdat, meshptr->proccomm), proccomm);

	//CHECK_FERR (SCOTCH_dgraphCoarsen (fgrfptr, coarnbr, coarrat, SCOTCH_COARSENNOMERGE, &tgrfdat, multloctax + 2 * baseval), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (&tgrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

	SCOTCH_dgraphSize (&tgrfdat, NULL, &tvrtlocnbr, NULL, NULL);

  	if ((tvrtloctax = (Gnum *) memAlloc (tvrtlocnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	tvrtloctax -= baseval;

  	cheklocval = dmeshAdapt2 (meshptr, &tgrfdat, tvrtloctax, cgrfptr, cprcvrttab, coarval - 1, coarnbr, coarrat);
  	CHECK_VERR (cheklocval, proccomm);
	SCOTCH_dgraphExit (&tgrfdat);

	SCOTCH_dgraphSize (cgrfptr, NULL, &cvrtlocnbr, NULL, NULL);

	for (tvrtlocnum = baseval, tvrtlocnnd = tvrtlocnbr + baseval;
		tvrtlocnum < tvrtlocnnd; tvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
        fvrtlocnum = multloctax[2 * tvrtlocnum + multdsp];

		if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
		  }
		  dsndcnttab[procngbnum] ++;
		}
		else 
		  fvrtloctax[fvrtlocnum - fvrtlocbas] = tvrtloctax[tvrtlocnum];
	  }
	}
  }

  /* Compute displacement sending array */
  dsnddsptab[0] = 0;
  for (procngbnum = 1, procngbnnd = meshptr->procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) {
	dsndcnttab[procngbnum - 1] <<= 1;
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  }

  datasndnbr = dsnddsptab[meshptr->procglbnbr];

  //* envoyer dsndcnttab et recevoir drcvcnttab
  CHECK_VDBG (cheklocval, proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  //* en déduire ddsp{snd,rcv}tab
  /* Compute displacement receiving array */
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  drcvdsptab[meshptr->procglbnbr] = drcvdsptab[meshptr->procglbnbr - 1] + drcvcnttab[meshptr->procglbnbr - 1];
  datarcvnbr = drcvdsptab[meshptr->procglbnbr];
  dsndcnttab[0] = dsnddsptab[0];

  //* allouer datasndtab et datarcvtab
  //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
  if (memAllocGroup ((void **) (void *)
        &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
        &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  if ((coarval == 0) || (fvrtglbnbr == 1)) {
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;

  	for (procngbnum = meshptr->proclocnum, cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval;
	  	cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
		Gnum fvrtlocbs2;
      	fvrtlocnum = multloctax[2 * cvrtlocnum + multdsp];

	  	if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
			fvrtlocbs2 = fprcvrttab[procngbnum] - baseval;
		  }
		  datasndtab[dsndcnttab[procngbnum] ++] = fvrtlocnum - fvrtlocbs2;
		  datasndtab[dsndcnttab[procngbnum] ++] = cvrtlocnum + cvrtlocbas;
	  	}
	  }
  	}
  }
  else {
	Gnum tvrtlocnum;
	Gnum tvrtlocnnd;
	for (procngbnum = meshptr->proclocnum, tvrtlocnum = baseval, tvrtlocnnd = tvrtlocnbr + baseval;
		tvrtlocnum < tvrtlocnnd; tvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
		Gnum fvrtlocbs2;
        fvrtlocnum = multloctax[2 * tvrtlocnum + multdsp];

		if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
			fvrtlocbs2 = fprcvrttab[procngbnum] - baseval;
		  }

		  datasndtab[dsndcnttab[procngbnum] ++] = fvrtlocnum - fvrtlocbs2;
		  datasndtab[dsndcnttab[procngbnum] ++] = tvrtloctax[tvrtlocnum];
		}
	  }
	}

	memFree (tvrtloctax + baseval);
  }

  for (procngbnum = 0 ; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
  }

  //* envoyer datasndtab et recevoir datarcvtab
  CHECK_VDBG (cheklocval, proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  intSort2asc1 (datarcvtab, datarcvnbr >> 1);

  for (datarcvidx = 0; datarcvidx < datarcvnbr; datarcvidx += 2)
	fvrtloctax[datarcvtab[datarcvidx]] = datarcvtab[datarcvidx + 1];

  memFree (fprcngbtab);
  memFree (datasndtab);

  return (0);
}

  int
dmeshAdaptVertResize (
  	Dmesh * restrict const    meshptr,              /**< mesh */
	Gnum * restrict *               hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_DMESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx];

    if (vertlocnum == ~0) // If empty slot
      continue;

  	for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if ((*hashtabptr)[hashnum] == vertlocnum)
      	errorPrint ("vertex number is already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_DMESH */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (Gnum *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (Gnum)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  if (meshptr != NULL)
    CHECK_VERR (cheklocval, meshptr->proccomm);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (Gnum)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx] != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx];

    if (vertlocnum == ~0) // If empty slot
      continue;

    for (hashnum = (vertlocnum * DMESHADAPTHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum] != ~0 && (*hashtabptr)[hashnum] != vertlocnum; hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx] = ~0;
  }

#ifdef PAMPA_DEBUG_DMESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx];

    if (vertlocnum == ~0) // If empty slot
      continue;

  	for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if ((*hashtabptr)[hashnum] == vertlocnum)
      	errorPrint ("vertex number is already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_DMESH */
  return (0);
}

int dmeshAdaptGatherSave3(
    Dmesh const * const meshptr,
    Gnum                vertlocnbr,
    Gnum  const * const dataloctax,
    Gnum        * const dataglbtax)
{
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  int cheklocval;
  int vertnbr;

  cheklocval = 0;
  if (memAllocGroup ((void **) (void *)
        &drcvcnttab, (size_t) (meshptr->procglbnbr * sizeof (int)),
        &drcvdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  vertnbr = (int) vertlocnbr;
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commGather (&vertnbr, 1, MPI_INT,
        drcvcnttab, 1, MPI_INT, 0, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if (meshptr->proclocnum == 0) {
    Gnum procngbnum;

    drcvdsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1; procngbnum ++) {
      drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    }
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commGatherv ((void *) dataloctax + meshptr->baseval, vertlocnbr, GNUM_MPI,
          dataglbtax + meshptr->baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
  else {
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commGatherv ((void *) dataloctax + meshptr->baseval, vertlocnbr, GNUM_MPI,
          NULL, NULL, NULL, GNUM_MPI, 0, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
  memFreeGroup (drcvcnttab);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

int dmeshAdaptGatherSave2(
    Dmesh const * const meshptr,
    Gnum  const * const dataloctax,
    int (*meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void        * const dataptr,
    char        * const fileptr)
{
    Gnum  * dataglbtax;
    int cheklocval;

    cheklocval = 0;
    if ((dataglbtax = (Gnum *) memAlloc (meshptr->enttloctax[meshptr->baseval]->vertglbnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
    dataglbtax -= meshptr->baseval;

  CHECK_FERR (dmeshAdaptGatherSave3 (meshptr, meshptr->enttloctax[meshptr->baseval]->vertlocnbr, dataloctax, dataglbtax), meshptr->proccomm);
  cheklocval = dmeshAdaptGatherSave (meshptr, dataglbtax, meshSave, dataptr, fileptr);
  memFree (dataglbtax);
  return (cheklocval);
}

int dmeshAdaptGatherSave(
    Dmesh const * const meshptr,
    Gnum        * const dataglbtax,
    int (*meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void        * const dataptr,
    char        * const fileptr)
{
  Mesh wmshdat;
  int cheklocval;

  cheklocval = 0;
  if (meshptr->proclocnum == 0) {
    meshInit (&wmshdat);
    CHECK_FERR (PAMPA_dmeshGather (meshptr, &wmshdat), meshptr->proccomm);
    CHECK_FERR (meshSave((PAMPA_Mesh *) &wmshdat, 1, dataptr, dataglbtax + meshptr->baseval, fileptr), meshptr->proccomm);
    meshExit (&wmshdat);
  }
  else
    CHECK_FERR (PAMPA_dmeshGather (meshptr, NULL), meshptr->proccomm);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}
