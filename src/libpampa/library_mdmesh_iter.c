
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* mesh iterator handling routines.     */
/*                                      */
/****************************************/

//! \brief This routine fill in the PAMPA_Iterator structure in order to have
//! the neighbors, with a given entity, of a vertex.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_mdmeshItInitStart (
PAMPA_Mdmesh * const        meshptr,              //!< Multi-distributed mesh
PAMPA_Num const             levllocnum,           //!< Level number in multigrid
PAMPA_Num const             ventlocnum,           //!< Entity number of vertex
PAMPA_Num const             flagval,              //!< Flag number
PAMPA_Iterator *            iterptr)              //!< Iterator to fill in
{
  int cheklocval;
  PAMPA_Num dummptr;
  PAMPA_IteratorConst * itecptr;

  cheklocval = 0;
  itecptr = (PAMPA_IteratorConst *) &iterptr->c;
#ifdef PAMPA_DEBUG_MDMESH
  if ((levllocnum < ((const Mdmesh *) meshptr)->baseval) || (levllocnum >= ((const Mdmesh *) meshptr)->dmshglbnbr)) {
    errorPrint ("Invalid level number");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, ((const Mdmesh *) meshptr)->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */


#ifdef PAMPA_DEBUG_ITER
  sprintf (itecptr->s, "mdmeshItInitStart, levllocnum: %d, ventlocnum: %d, flagval: %d", levllocnum, ventlocnum, flagval);
#endif /* PAMPA_DEBUG_ITER */

  cheklocval = dmeshItData(((const Mdmesh *) meshptr)->dmshglbtax + levllocnum, ventlocnum, flagval, &itecptr->nentnum, &dummptr, &itecptr->vindtab, &itecptr->edgetab, &dummptr, &itecptr->svrttab, &itecptr->mngbtab, &itecptr->nentbas, &itecptr->nentmsk, &itecptr->nenttab, &itecptr->sngbtab, &iterptr->vertnum, &itecptr->vertnnd, 0);
  CHECK_VERR (cheklocval, ((const Mdmesh *) meshptr)->proccomm);

  itecptr->nesbnum = ventlocnum;
  if (itecptr->nentbas == NULL)
    itecptr->nentbas = (PAMPA_Num *) &itecptr->nesbnum;

#ifdef PAMPA_DEBUG_ITER
  itecptr->vertmax = itecptr->vertnnd;
#endif /* PAMPA_DEBUG_ITER */
  return cheklocval;
}




//! \brief This routine fill in the PAMPA_Iterator structure in order to have
//! the neighbors, with a given entity, of a vertex.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_mdmeshItInit (
PAMPA_Mdmesh * const        meshptr,              //!< Multi-distributed mesh
PAMPA_Num const             vlvllocnum,           //!< Level number in multigrid for vertex
PAMPA_Num const             ventnum,              //!< Entity number of vertex
PAMPA_Num const             nlvllocnum,           //!< Level number in multigrid for neighbors
PAMPA_Num const             nesbnum,              //!< Entity number of neighbors or flag number
PAMPA_Iterator *            iterptr)              //!< Iterator to fill in
{
  int cheklocval;
#ifdef PAMPA_DEBUG_REDUCE
  int chekglbval;
#endif /* PAMPA_DEBUG_REDUCE */
  PAMPA_Num dummval;
  PAMPA_IteratorConst * itecptr;

  cheklocval = 0;
  itecptr = (PAMPA_IteratorConst *) &iterptr->c;
#ifdef PAMPA_DEBUG_MDMESH
  if ((nlvllocnum < ((const Mdmesh *) meshptr)->baseval) || (nlvllocnum >= ((const Mdmesh *) meshptr)->dmshglbnbr) || ((vlvllocnum < ((const Mdmesh *) meshptr)->baseval) || (vlvllocnum >= ((const Mdmesh *) meshptr)->dmshglbnbr))) {
    errorPrint ("Invalid level number for vertex or neighbor");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, ((const Mdmesh *) meshptr)->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

#ifdef PAMPA_DEBUG_ITER
  sprintf (itecptr->s, "mdmeshItInit: %p, vlvllocnum: %d, ventnum: %d, nlvllocnum: %d, nesbnum: %d", meshptr, vlvllocnum, ventnum, nlvllocnum, nesbnum);
#endif /* PAMPA_DEBUG_ITER */

  if (nlvllocnum == vlvllocnum) { /* Same entity for vertex and neighbor */
    cheklocval = dmeshItData(((const Mdmesh *) meshptr)->dmshglbtax + vlvllocnum, ventnum, nesbnum, &itecptr->nentnum, &dummval, &itecptr->vindtab, &itecptr->edgetab, &dummval, &itecptr->svrttab, &itecptr->mngbtab, &itecptr->nentbas, &itecptr->nentmsk, &itecptr->nenttab, &itecptr->sngbtab, &iterptr->vertnum, &itecptr->vertnnd, 0);
    CHECK_VERR (cheklocval, ((const Mdmesh *) meshptr)->proccomm);

    itecptr->nesbnum = nesbnum;
    itecptr->ventnum = ventnum;
    if (itecptr->nentbas == NULL)
      itecptr->nentbas = (PAMPA_Num *) &itecptr->nesbnum;
  }
  else { /* Iterator between levels on multigrid */
    cheklocval = mdmeshItData((const Mdmesh *) meshptr, vlvllocnum, ventnum, nlvllocnum, nesbnum, &itecptr->nentnum, &dummval, &itecptr->vindtab, &itecptr->edgetab, &dummval, &itecptr->svrttab, &itecptr->mngbtab, &itecptr->nentbas, &itecptr->nentmsk, &itecptr->nenttab, &itecptr->sngbtab, &iterptr->vertnum, &itecptr->vertnnd, 0);
    CHECK_VERR (cheklocval, ((const Mdmesh *) meshptr)->proccomm);

    itecptr->nesbnum = nesbnum;
    itecptr->ventnum = ventnum;
    if (itecptr->nentbas == NULL)
      itecptr->nentbas = (PAMPA_Num *) &itecptr->nesbnum;
  }

#ifdef PAMPA_DEBUG_ITER
    itecptr->vertmax = itecptr->vertnnd;
#endif /* PAMPA_DEBUG_ITER */
  return cheklocval;
}



