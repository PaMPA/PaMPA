/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_mat.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the distributed mesh
//!                part functions to fill matrix.
//!
//!   \date        Version 1.0: from: 16 Sep 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/


#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* XXX                                  */
/*                                      */
/****************************************/

int
dmeshMatInit (
const Dmesh * const    meshptr,                                    //!< Distributed mesh
Gnum const             enttlocnum)                                 //!< Entity number
{

  DmeshEntity * enttlocptr;
  Gnum          enttlocsiz;
  Gnum          vertlocnum;
  Gnum          vertlocnnd;
  Gnum          edgelocnbr;

  enttlocptr = meshptr->enttloctax[enttlocnum];

  if (meshptr->ovlpglbval > 0)
    enttlocsiz = enttlocptr->vgstlocnbr;
  else
    enttlocsiz = enttlocptr->vertlocnbr;




  // \attention If PAMPA_ENTT_UNSTABLE exists, be careful with the following
  // test
  switch (meshptr->esublocbax[enttlocnum & meshptr->esublocmsk]) {

    case PAMPA_ENTT_STABLE :
#ifdef PAMPA_DEBUG_MEM
        enttlocptr->nghbloctax += meshptr->baseval; 
        enttlocptr->mvrtloctax += meshptr->baseval;
        enttlocptr->ventloctax += meshptr->baseval; 
        enttlocptr->svrtloctax += meshptr->baseval;
#endif /* PAMPA_DEBUG_MEM */

      /* Allocate private entity neighbors data*/
      if (memReallocGroup ((void *) enttlocptr->commlocdat.procrcvtab,
            &enttlocptr->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
            &enttlocptr->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
            &enttlocptr->nghbloctax,    (size_t) (meshptr->enttglbnbr * sizeof (DmeshEnttNghb)),
            &enttlocptr->mvrtloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
            &enttlocptr->ventloctax,    (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
            &enttlocptr->svrtloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
            &enttlocptr->vmatloctax, (size_t) ((enttlocsiz + 1)       * sizeof (Gnum)),
            (void *) NULL) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
      }

      enttlocptr->nghbloctax -= meshptr->baseval;
      enttlocptr->mvrtloctax -= meshptr->baseval;
      enttlocptr->ventloctax -= meshptr->baseval;
      enttlocptr->svrtloctax -= meshptr->baseval;
      enttlocptr->vmatloctax -= meshptr->baseval;
      break;

    case PAMPA_ENTT_SINGLE :

#ifdef PAMPA_DEBUG_MEM
        enttlocptr->nghbloctax += meshptr->baseval; 
        enttlocptr->mvrtloctax += meshptr->baseval;
#endif /* PAMPA_DEBUG_MEM */
      /* Allocate private entity neighbors data*/
      if (memReallocGroup ((void *) enttlocptr->commlocdat.procrcvtab,
            &enttlocptr->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
            &enttlocptr->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
            &enttlocptr->nghbloctax, (size_t) (meshptr->enttglbnbr * sizeof (DmeshEnttNghb)),
            &enttlocptr->mvrtloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
            &enttlocptr->vmatloctax, (size_t) ((enttlocsiz + 1)       * sizeof (Gnum)),
            (void *) NULL) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
      }

      enttlocptr->nghbloctax -= meshptr->baseval;
      enttlocptr->mvrtloctax -= meshptr->baseval;
      enttlocptr->ventloctax = NULL;
      enttlocptr->svrtloctax = NULL;
      enttlocptr->vmatloctax -= meshptr->baseval;
      break;

    default :
#ifdef PAMPA_DEBUG_MEM
        enttlocptr->ventloctax += meshptr->baseval; 
        enttlocptr->svrtloctax += meshptr->baseval;
#endif /* PAMPA_DEBUG_MEM */
      /* Allocate private entity neighbors data*/
      if (memReallocGroup ((void *) enttlocptr->commlocdat.procrcvtab,
            &enttlocptr->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
            &enttlocptr->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
            &enttlocptr->ventloctax,    (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
            &enttlocptr->svrtloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
            &enttlocptr->vmatloctax, (size_t) ((enttlocsiz + 1)       * sizeof (Gnum)),
            (void *) NULL) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
      }

      enttlocptr->mvrtloctax = NULL;
      enttlocptr->ventloctax -= meshptr->baseval;
      enttlocptr->svrtloctax -= meshptr->baseval;
      enttlocptr->vmatloctax -= meshptr->baseval;

  }



















  for (vertlocnum = meshptr->baseval, vertlocnnd = enttlocsiz + meshptr->baseval, edgelocnbr = meshptr->baseval;
      vertlocnum < vertlocnnd ;
      vertlocnum ++) {
    enttlocptr->vmatloctax[vertlocnum] = edgelocnbr;
    edgelocnbr += enttlocptr->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vendlocidx - enttlocptr->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vertlocidx;
  }
  enttlocptr->vmatloctax[vertlocnnd] = edgelocnbr;


  return (0);
}

