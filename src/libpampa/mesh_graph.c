
/*
 ** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
 */

#define MESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"
#include <ptscotch.h>
#include "mesh_graph.h"

  int
meshGraphInit (
    SCOTCH_Graph *      grafptr)
{
  return SCOTCH_graphInit(grafptr);
}


//! This routine computes the distributed graph corresponding to the distributed
//! mesh
//! It returns:
//! - 0   : if the computation succeeded.
//! - !0  : on error.

  int
meshGraphBuild (
    Mesh * const        meshptr,
    Gnum                 flagval,
    Gnum const           dvrtnbr,
    Gnum * const         dvrttax,
    Gnum * const         velotax,
    Gnum * const         edlotax, // XXX à prendre en compte et modifier les appels de meshgraphbuild
    SCOTCH_Graph *      grafptr,
    Gnum *               vertbas) // XXX en a-t-on besoin ? Il provient d'un copier-coller de dmesh_dgraph
{

  Gnum baseval;
  Gnum chekval;
  Gnum edgenbr;
  Gnum vertnum;
  Gnum vertnm2;
  Gnum vertnnd;
  Gnum ventnum;
  Gnum nentnum;
  Gnum enttnnd;
  Gnum vertnbr;
  Gnum vlblnbr;
  Gnum velonbr;
  Gnum evrtsiz;
  Gnum * verttax;
  Gnum * vendtax;
  Gnum * vlbltax;
  Gnum * velotx2;
  Gnum * edgetax;
  Gnum * evrttax;
  MeshEntity ** entttax;

  baseval = meshptr->baseval;
  chekval = 0;
  entttax = meshptr->entttax;

  if (edlotax != NULL) {
    errorPrint ("edlotax is not yet taken into account");
    return (1);
  }

  switch (flagval) {
    case MESH_ENTT_MAIN_PART :

      enttnnd = baseval + 1;
      vertnbr = dvrtnbr;

      evrtsiz = entttax[baseval]->vertnbr;
      break;
    case MESH_ENTT_MAIN :
      enttnnd = baseval + 1;
      vertnbr = entttax[baseval]->vertnbr;

      evrtsiz = entttax[baseval]->vertnbr;
      break;
    case MESH_ENTT_ANY :
      enttnnd = meshptr->enttnbr + baseval;
      vertnbr = meshptr->vertnbr; // XXX n'a-ton pas vtrunbr pour Mesh ? (on l'a pour Dmesh)

      evrtsiz = 0;
      break;
    default :
      errorPrint("unknown flag");
      return (1);
  }


  // XXX peut-être à simplifier si pas de recouvrement et toutes entités
  for (edgenbr = 0, ventnum = baseval ; ventnum < enttnnd ; ventnum ++) {
    if ((entttax[ventnum] == NULL) || (meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE))
      continue;

    for (nentnum = baseval ; nentnum < enttnnd ; nentnum ++) {
      if ((entttax[ventnum]->nghbtax[nentnum] == NULL) || (meshptr->esubbax[nentnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE))
        continue;

      for (vertnum = baseval, vertnnd = entttax[ventnum]->vertnbr + baseval;
          vertnum < vertnnd; vertnum ++)  {
        edgenbr += entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx
          - entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx;
      }
    }
  }


  if (memAllocGroup ((void **) (void *)
        &evrttax, (size_t) (evrtsiz * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint  ("out of memory");
    chekval = 1;
  }
  CHECK_MAX_REDUCE(chekval, chekval, meshptr->proccomm, "meshGraphBuild", __LINE__);

  evrttax -= baseval;

  //if (vertnbr != 0) {

  velonbr = (velotax == NULL) ?  vertnbr : 0;
  vlblnbr = (flagval == MESH_ENTT_ANY) ? meshptr->vertnbr : 0;

  if (memAllocGroup ((void **) (void *)
        &verttax, (size_t) (MAX(vertnbr, 2) * sizeof (Gnum)),// TRICK: MAX(,2) for Scotch if vertnbr = 0
        &vendtax, (size_t) (MAX(vertnbr, 2) * sizeof (Gnum)),
        &vlbltax, (size_t) (vlblnbr       * sizeof (Gnum)),
        &velotx2, (size_t) (velonbr * sizeof (Gnum)), // FIXME à remplir correctement
        &edgetax, (size_t) (MAX(edgenbr, 2) * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint  ("out of memory");
    chekval = 1;
  }
  CHECK_MAX_REDUCE(chekval, chekval, meshptr->proccomm, "meshGraphBuild", __LINE__);
  verttax -= baseval;
  vendtax -= baseval;
  velotx2 = (velotax == NULL) ? velotx2 - baseval : velotax;
  vlbltax = (vlblnbr == 0) ? NULL : vlbltax - baseval;
  edgetax -= baseval;

  memset (verttax + baseval, 0, MAX(vertnbr, 2) * sizeof (Gnum));
  memset (vendtax + baseval, 0, MAX(vertnbr, 2) * sizeof (Gnum));
  if (velotax == NULL) 
    memset (velotx2 + baseval, 0, vertnbr * sizeof (Gnum));
  //}
  //else {
  //CHECK_MAX_REDUCE(chekval, chekval, meshptr->proccomm, "meshGraphBuild", __LINE__);
  //verttax = 
  //      vendtax =
  //      edgetax = NULL;
  //}


  switch (flagval) {
    case MESH_ENTT_MAIN_PART :
    case MESH_ENTT_MAIN :

      for (vertnum = baseval, vertnnd = entttax[baseval]->vertnbr + baseval; vertnum < vertnnd ; vertnum ++)
        if (flagval == MESH_ENTT_MAIN)
          evrttax[vertnum] = vertnum;
        else if (dvrttax[vertnum] != ~0)
          evrttax[vertnum] = dvrttax[vertnum];
        else
          evrttax[vertnum] = ~0;

      if (vertbas != NULL)
        *vertbas = baseval;

      break;
    case MESH_ENTT_ANY :

	  if (vertbas != NULL)
		*vertbas = baseval;

      break;
    default :
      errorPrint("unknown flag");
      return (1);
  }

  if (flagval == MESH_ENTT_ANY) {
    for (vertnum = baseval, vertnnd = baseval + meshptr->vertnbr ; vertnum < vertnnd ; vertnum ++)
      vlbltax[vertnum] = meshptr->vertnbr + 1;

    for (ventnum = baseval ; ventnum < enttnnd ; ventnum ++) {
      MeshEntity * ventptr;

      ventptr = entttax[ventnum];

      if ((entttax[ventnum] == NULL) || (meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE))
        continue;

      for (vertnum = baseval, vertnnd = ventptr->vertnbr + baseval;
          vertnum < vertnnd; vertnum ++) {
        vertnm2 = ventptr->mvrttax[vertnum];
        vlbltax[vertnm2] = vertnm2;
      }
    }
    intSort1asc1 (vlbltax + baseval, meshptr->vertnbr);
  }



  for (vertnm2 = baseval - 1, edgenbr = ventnum = baseval ; ventnum < enttnnd ; ventnum ++) {
    MeshEntity * ventptr;

    ventptr = entttax[ventnum];

    if ((entttax[ventnum] == NULL) || (meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE))
      continue;

    for (vertnum = baseval, vertnnd = ventptr->vertnbr + baseval;
        vertnum < vertnnd; vertnum ++)  {
      Gnum edgenum;
      Gnum edgennd;

      if (flagval == MESH_ENTT_ANY) {
        Gnum vertnm3;
        Gnum vertmx2;

        vertnm2 = ventptr->mvrttax[vertnum];
        if (vlbltax[vertnm2] != vertnm2) {
          for (vertnm3 = baseval, vertmx2 = meshptr->vertnbr + baseval;
              vertmx2 - vertnm3 > 1; ) {
            Gnum                 vertmed;

            vertmed = (vertmx2 + vertnm3) / 2;
            if (vlbltax[vertmed] <= vertnm2)
              vertnm3 = vertmed;
            else
              vertmx2 = vertmed;
          }
#ifdef PAMPA_MESH2
          if (vlbltax[vertnm3] != vertnm2) {
            errorPrint ("%d not found", vertnm2);
            return (1);
          }
#endif /* PAMPA_MESH2 */
          vertnm2 = vertnm3;
        }
      }
      else if (flagval == MESH_ENTT_MAIN)
        vertnm2 ++;
      else if (evrttax[vertnum] != ~0) // XXX ne peut-on pas faire mieux pour le test ?
        vertnm2 ++;
      else
        continue;

      verttax[vertnm2] = edgenbr;
      if (velotax == NULL)
        velotx2[vertnm2] = 1;

      for (nentnum = baseval ; nentnum < enttnnd ; nentnum ++) {
        MeshEntity * nentptr;

        nentptr = entttax[nentnum];

        if ((ventptr->nghbtax[nentnum] == NULL) || (meshptr->esubbax[nentnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE))
          continue;

        if ((flagval == MESH_ENTT_ANY) && (nentptr != NULL))
          evrttax = nentptr->mvrttax;



        for (edgenum = ventptr->nghbtax[nentnum]->vindtax[vertnum].vertidx,
            edgennd = ventptr->nghbtax[nentnum]->vindtax[vertnum].vendidx;
            edgenum < edgennd ; edgenum ++)
          if ((flagval != MESH_ENTT_MAIN_PART) || ((flagval == MESH_ENTT_MAIN_PART) && (evrttax[meshptr->edgetax[edgenum]] != ~0)))
            edgetax[edgenbr ++] = evrttax[meshptr->edgetax[edgenum]];

      }
      vendtax[vertnm2] = edgenbr;
    }
  }
  edgenbr -= baseval;


  if (verttax != NULL)
    verttax += baseval;
  if (vlbltax != NULL)
    vlbltax += baseval;

  chekval = SCOTCH_graphBuild(grafptr,
      meshptr->baseval,
      vertnbr,
      verttax, // TRICK: verttax already unbased due to allocation group
      vendtax + baseval,
      velotx2 + baseval,
      vlbltax,
      edgenbr,
      edgetax + baseval,
      NULL);
  if (chekval != 0)
    return chekval;

//#define PAMPA_DEBUG_GRAPH
#ifdef PAMPA_DEBUG_GRAPH
  {
    static int i = 0;
    char s[30];
    sprintf(s, "mesh-graph-%d-" GNUMSTRING ".grf", i ++, meshptr->procnum);
    FILE *graph_file;
    graph_file = fopen(s, "w");
    SCOTCH_graphSave (grafptr, graph_file);
    fclose(graph_file);
  }
#endif /* PAMPA_DEBUG_GRAPH */

#ifdef PAMPA_DEBUG_MESH_GRAPH
  CHECK_FERR (SCOTCH_graphCheck(grafptr), meshptr->proccomm);
#endif /* PAMPA_DEBUG_MESH_GRAPH */

  memFreeGroup (evrttax + baseval);
  return (chekval);
}

// XXX
  int
meshGraphFree (
    SCOTCH_Graph *      grafptr)
{

  Gnum * verttab;
  SCOTCH_graphData(grafptr, NULL, NULL, &verttab, NULL, NULL, NULL, NULL, NULL, NULL); 

  if (verttab != NULL)
    memFreeGroup (verttab);

  SCOTCH_graphFree (grafptr);

  return (0);
}

// XXX
  int
meshGraphExit (
    SCOTCH_Graph *      grafptr)
{

  Gnum * verttab;
  SCOTCH_graphData(grafptr, NULL, NULL, &verttab, NULL, NULL, NULL, NULL, NULL, NULL); 

  if (verttab != NULL)
    memFreeGroup (verttab);

  SCOTCH_graphExit(grafptr);

  return (0);
}
