/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dsmesh_dmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the converting
//!                routines between distributed mesh and
//!                distributed simplified mesh.
//!
//!   \date        Version 1.0: from: 21 Jul 2015
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */


#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dsmesh.h"
#include "dsmesh_dmesh.h"
#include "dmesh_ghst.h"
#include "dsmesh_halo.h"
#include "pampa.h"


int dsmesh2dmesh (
	Dsmesh * const smshptr,
	Dmesh * const meshptr)
{
  if (smshptr->procvrttab != NULL)
	meshptr->procloccnt = smshptr->procloccnt;

  // FIXME XXX et flags de smshptr ?
  return dmeshBuild (meshptr,
	  smshptr->baseval,
	  smshptr->vertlocnbr,
	  smshptr->vertlocmax,
	  smshptr->vertloctax,
	  smshptr->vendloctax,
	  smshptr->edgelocnbr,
	  smshptr->edgelocsiz,
	  smshptr->edgeloctax,
	  smshptr->edloloctax,
	  smshptr->enttglbnbr,
	  smshptr->ventloctax,
	  smshptr->esubloctax,
	  -1,
	  smshptr->ovlpglbval,
	  smshptr->valslocptr,
	  smshptr->degrlocmax,
	  smshptr->procvrttab,
	  smshptr->proccnttab,
	  smshptr->procdsptab,
	  smshptr->procngbnbr,
	  smshptr->procngbmax,
	  smshptr->procngbtab);
}

int dmesh2dsmesh (
	Dmesh * const meshptr,
	Dsmesh * const smshptr)
{
  Gnum * vertloctax;
  Gnum * vendloctax;
  Gnum * ventloctax;
  Gnum * edgeloctax;
  Gnum * edloloctax;
  Gnum * esubloctax;
  Gnum   edlolocnbr;
  Gnum   esublocnbr;
  Gnum   ventlocnum;
  Gnum   ventlocnnd;
  Gnum   edgelocidx;
  Gnum   vertlocnbr;
  Gnum   evrtlocnbr;
  Gnum   baseval;
  Gnum   vertlocbas;
  Gnum procsidnbr;
  Gnum enttlocnum;
  Gnum enttlocnnd;
  Gnum procsidcur;
  Gnum vertsidnum;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum vertlocmin;
  Gnum vertlocmax;
  Gnum edgelocnum;
  Gnum edgelocnnd;
  int    cheklocval;
  SidEntt * restrict vertsidtax;
  DsmeshEnttComm * restrict dstcommlocptr;
  int * restrict procsidtab;
  SidEntt * restrict vertgsttax;
  Gnum * restrict vertgsttx2;
  Gnum * restrict edgegsttax;

  cheklocval = 0;
  baseval = meshptr->baseval;

  vertlocnbr = meshptr->vertlocnbr;
  edlolocnbr = (meshptr->edloloctax == NULL) ? 0 : meshptr->edgelocnbr;
  esublocnbr = (meshptr->esublocmsk == 0) ? 0 : meshptr->enttglbnbr;

  if (memAllocGroup ((void **) (void *)
        &ventloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &vertloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &vendloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &edgeloctax, (size_t) (meshptr->edgelocnbr * sizeof (Gnum)),
        &edloloctax, (size_t) (edlolocnbr * sizeof (Gnum)),
        &esubloctax, (size_t) (esublocnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1; 
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  ventloctax -= baseval;
  vertloctax -= baseval;
  vendloctax -= baseval;
  edgeloctax -= baseval;
  edloloctax = (edlolocnbr == 0) ? NULL : edloloctax - baseval;
  esubloctax = (esublocnbr == 0) ? NULL : esubloctax - baseval;

#ifdef PAMPA_DEBUG_DSMESH
  if (PAMPA_ENTT_DUMMY != ~0) {
    errorPrint ("Internal error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  memSet (edgeloctax + baseval, ~0, meshptr->edgelocnbr * sizeof (Gnum));
#endif /* PAMPA_DEBUG_DSMESH */
  memSet (ventloctax + baseval, ~0, vertlocnbr * sizeof (Gnum));
  memSet (vertloctax + baseval, 0, vertlocnbr * sizeof (Gnum));
  memSet (vendloctax + baseval, 0, vertlocnbr * sizeof (Gnum));

  if (esubloctax != NULL)
    memCpy (esubloctax + baseval, meshptr->esublocbax + baseval, meshptr->enttglbnbr * sizeof (Gnum));

  vertlocbas = meshptr->procvrttab[meshptr->proclocnum] - baseval;
  for (edgelocidx = ventlocnum = baseval, ventlocnnd = meshptr->enttglbnbr + baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
    DmeshEntity * restrict ventlocptr;
    Gnum vertlocnum;
    Gnum vertlocnnd;

    if ((meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshptr->enttloctax[ventlocnum] == NULL))
      continue;

    ventlocptr = meshptr->enttloctax[ventlocnum];

    for (vertlocnum = baseval, vertlocnnd = meshptr->enttloctax[ventlocnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)  {
      Gnum bvrtlocnum;
      Gnum nentlocnum;
      Gnum nentlocnnd;

      bvrtlocnum = ventlocptr->mvrtloctax[vertlocnum] - vertlocbas;
      ventloctax[bvrtlocnum] = (ventlocptr->ventloctax == NULL) ? ventlocnum : ventlocptr->ventloctax[vertlocnum];
      vertloctax[bvrtlocnum] = edgelocidx;

      for (nentlocnum = baseval, nentlocnnd = meshptr->enttglbnbr + baseval; nentlocnum < nentlocnnd; nentlocnum ++) {
        DmeshEntity * restrict nentlocptr;
        Gnum edgelocnum;
        Gnum edgelocnnd;

        if ((meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshptr->enttloctax[nentlocnum] == NULL))
          continue;

        nentlocptr = meshptr->enttloctax[nentlocnum];

        for (edgelocnum = ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx,
            edgelocnnd = ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx;
            edgelocnum < edgelocnnd ; edgelocnum ++) {
          if (edloloctax != NULL)
            edloloctax[edgelocidx] = meshptr->edloloctax[edgelocnum];
          edgeloctax[edgelocidx ++] = nentlocptr->mvrtloctax[meshptr->edgeloctax[edgelocnum]];
        }
      }

      vendloctax[bvrtlocnum] = edgelocidx;
    }
  }

  if (meshptr->procvrttab != NULL)
	smshptr->procloccnt = meshptr->procloccnt;

  smshptr->flagval |= DSMESHFREETABS;
  // XXX bogue à corriger si on décommente cette partie
  if (meshptr->flagval & DMESHCOMMPTOP != 0)
    smshptr->flagval |= DSMESHCOMMPTOP;
  // XXX et les autres flags ?

  CHECK_FERR (dsmeshBuild (smshptr,
	  	baseval,
	  	vertlocnbr,
	  	meshptr->vertlocmax,
	  	vertloctax,
	  	vendloctax,
	  	edgelocidx - baseval,
	  	meshptr->edgelocsiz,
	  	edgeloctax,
	  	edloloctax,
	  	meshptr->enttglbnbr,
	  	ventloctax,
	  	esubloctax,
	  	0,
	  	meshptr->ovlpglbval,
	  	meshptr->valslocptr,
	  	meshptr->degrlocmax,
	  	meshptr->procvrttab,
	  	meshptr->proccnttab,
	  	meshptr->procdsptab,
	  	meshptr->procngbnbr,
	  	meshptr->procngbmax,
	  	meshptr->procngbtab), meshptr->proccomm);

  if ((dstcommlocptr = (DsmeshEnttComm *) memAlloc (sizeof (DsmeshEnttComm))) == NULL) {
   	errorPrint  ("out of memory");
   	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  smshptr->commlocptr = dstcommlocptr;
  memSet (dstcommlocptr, 0, sizeof (DsmeshEnttComm));

  for (evrtlocnbr = smshptr->vgstlocnbr = procsidnbr = 0, enttlocnum = baseval, enttlocnnd = meshptr->enttglbnbr + baseval; enttlocnum < enttlocnnd; enttlocnum ++) {
   	DmeshEntity * restrict enttlocptr;

   	if ((meshptr->esublocbax[enttlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
	  continue;

   	enttlocptr = meshptr->enttloctax[enttlocnum];
   	procsidnbr += enttlocptr->commlocdat.procsidnbr;
	smshptr->vgstlocnbr += enttlocptr->vgstlocnbr;
        evrtlocnbr += enttlocptr->vertlocnbr;
  }
  procsidnbr += meshptr->enttglbnbr;
  smshptr->vgstlocnbr += vertlocnbr - evrtlocnbr;
 
  if (memAllocGroup ((void **) (void *)
       	&dstcommlocptr->procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
       	&dstcommlocptr->procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
       	&dstcommlocptr->procsidtab, (size_t) (procsidnbr          * sizeof (int)), // XXX on devrait plutot faire une alloc séparée pour le redimensionner s'il est plus petit que prévu ;)
       	&smshptr->edgegsttax, (size_t) (meshptr->edgelocnbr          * sizeof (Gnum)), // XXX on devrait plutot faire une alloc séparée pour le redimensionner s'il est plus petit que prévu ;)
       	(void *) NULL) == NULL) {
   	errorPrint  ("out of memory");
   	cheklocval = 1; 
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  memSet (dstcommlocptr->procrcvtab, 0, meshptr->procglbnbr * sizeof (int));
  memSet (dstcommlocptr->procsndtab, 0, meshptr->procglbnbr * sizeof (int));
  smshptr->edgegsttax -= baseval;
  edgegsttax = smshptr->edgegsttax;
  procsidtab = dstcommlocptr->procsidtab;

  if (memAllocGroup ((void **) (void *)
       	&vertsidtax, (size_t) (meshptr->enttglbnbr * sizeof (SidEntt)),
       	&vertgsttax, (size_t) (smshptr->vgstlocnbr * sizeof (SidEntt)),
       	(void *) NULL) == NULL) {
   	errorPrint  ("out of memory");
   	cheklocval = 1; 
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  vertsidtax -= baseval;
  vertgsttx2 = (Gnum *) vertgsttax - baseval;
  vertgsttax -= baseval;

  memSet (vertsidtax + baseval, 0, meshptr->enttglbnbr * sizeof (SidEntt));
  memSet (vertgsttax + baseval, ~0, smshptr->vgstlocnbr * sizeof (SidEntt));

  for (enttlocnum = baseval, enttlocnnd = meshptr->enttglbnbr + baseval; enttlocnum < enttlocnnd; enttlocnum ++) {
   	DmeshEntity * restrict enttlocptr;
	Gnum procngbnum;

   	if ((meshptr->esublocbax[enttlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
	  continue;

   	enttlocptr = meshptr->enttloctax[enttlocnum];
   	if ((enttlocptr->commlocdat.procsidnbr > 0) && (enttlocptr->commlocdat.procsidtab[0] < 0)) { // FIXME Il faut une boucle au cas où nous aurions DMESHGHSTSIDMAX
          if (enttlocptr->commlocdat.procsidtab[0] == DMESHGHSTSIDMAX) {
            errorPrint ("internal error, case not implemented");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);
	  vertsidtax[enttlocnum].vertnum = baseval - enttlocptr->commlocdat.procsidtab[0];
	  vertsidtax[enttlocnum].indxnum = 1;
   	} 
   	else
	  vertsidtax[enttlocnum].vertnum = baseval;

   	for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) {
	  dstcommlocptr->procrcvtab[procngbnum] += enttlocptr->commlocdat.procrcvtab[procngbnum];
	  dstcommlocptr->procsndtab[procngbnum] += enttlocptr->commlocdat.procsndtab[procngbnum];
   	}
   	dstcommlocptr->procsndnbr += enttlocptr->commlocdat.procsndnbr;
  }

  //vertlocbas -= baseval;
  for (procsidcur = 0, vertsidnum = vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
	Gnum enttlocnm2;
	Gnum bvrtlocnum;
   	DmeshEntity * restrict enttlocptr;
   	DmeshEnttComm * restrict commlocptr;

   	enttlocnum = ventloctax[vertlocnum];
        if (enttlocnum == PAMPA_ENTT_DUMMY)
          continue;
	vertgsttx2[vertlocnum] = vertlocnum + vertlocbas;

   	if (meshptr->enttloctax[enttlocnum] == NULL) /* If empty entity */
	  continue;

   	if (meshptr->esublocbax[enttlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
	  enttlocnm2 = meshptr->esublocbax[enttlocnum & meshptr->esublocmsk];
	else
	  enttlocnm2 = enttlocnum;

   	enttlocptr = meshptr->enttloctax[enttlocnm2];
   	commlocptr = &enttlocptr->commlocdat;

	bvrtlocnum = enttlocptr->mvrtloctax[vertsidtax[enttlocnm2].vertnum] - vertlocbas;
   	if (bvrtlocnum == vertlocnum) {
	  Gnum procsididx;
	  Gnum procsidnnd;

      while ((vertlocnum - vertsidnum) >= DMESHGHSTSIDMAX) { /* If Gnum range too long for int */
       	procsidtab[procsidcur ++]  = -DMESHGHSTSIDMAX; /* Decrease by maximum int distance      */
       	vertsidnum                += DMESHGHSTSIDMAX;
      }
	  if (vertsidnum != vertlocnum) {
      	procsidtab[procsidcur ++] = - (vertlocnum - vertsidnum); /* Encode jump in procsidtab */
      	vertsidnum = vertlocnum;              /* Current local vertex is last send vertex     */
	  }
   	  for (procsididx = vertsidtax[enttlocnm2].indxnum, procsidnnd = commlocptr->procsidnbr;
	   	  procsididx < procsidnnd && commlocptr->procsidtab[procsididx] >= 0; procsididx ++, procsidcur ++)
	  	procsidtab[procsidcur] = commlocptr->procsidtab[procsididx];

   	  for (; /* we assume that procsidnnd and procsididx are unchanged between the 2 loops */
	   	  procsididx < procsidnnd && commlocptr->procsidtab[procsididx] < 0; procsididx ++)
		vertsidtax[enttlocnm2].vertnum -= commlocptr->procsidtab[procsididx];

	  vertsidtax[enttlocnm2].indxnum = procsididx;
   	}
  }
  smshptr->commlocptr->procsidnbr = procsidcur;

  CHECK_FERR (dsmeshHaloSyncComm (smshptr, dstcommlocptr, vertlocnbr, vertgsttx2 + baseval, GNUM_MPI), meshptr->proccomm);

  //for (vertlocnum = smshptr->vgstlocnbr + baseval - 1; vertlocnum > 0 && vertgsttx2[vertlocnum] == ~0; vertlocnum --);

  //smshptr->vgstlocnbr = vertlocnum - baseval + 1;

  for (vertlocnum = baseval + smshptr->vgstlocnbr - 1, vertlocnnd = baseval - 1; vertlocnum > vertlocnnd; vertlocnum --) {
    vertgsttax[vertlocnum].vertnum = vertgsttx2[vertlocnum];
    vertgsttax[vertlocnum].indxnum = vertlocnum;
  }

  intSort2asc1 (vertgsttax + baseval, smshptr->vgstlocnbr);

  vertlocmin = meshptr->procvrttab[meshptr->proclocnum];
  vertlocmax = meshptr->procvrttab[meshptr->proclocnum + 1] - 1;
  for (edgelocnum = baseval, edgelocnnd = smshptr->edgelocnbr + baseval; edgelocnum < edgelocnnd; edgelocnum ++) {
    Gnum nghblocnum;
    Gnum nghblocidx;

    nghblocnum = edgeloctax[edgelocnum];
#ifdef PAMPA_DEBUG_DSMESH
    if (nghblocnum == ~0) {
      errorPrint ("internal error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DSMESH */
    if ((nghblocnum < vertlocmin) || (nghblocnum > vertlocmax)) { /* If distant neighbor */
      Gnum nghblocmax;

      /* Search the index of nghbex which have current number */
      for (nghblocidx = baseval, nghblocmax = smshptr->vgstlocnbr + baseval;
          nghblocmax - nghblocidx > 1; ) {
        Gnum                 nghblocmed;

        nghblocmed = (nghblocmax + nghblocidx) / 2;
        if (vertgsttax[nghblocmed].vertnum <= nghblocnum)
          nghblocidx = nghblocmed;
        else
          nghblocmax = nghblocmed;
      }
#ifdef PAMPA_DEBUG_DSMESH
	  if (vertgsttax[nghblocidx].vertnum != nghblocnum) {
		errorPrint ("Neighbor " GNUMSTRING " not found", nghblocnum);
		cheklocval = 1;
	  }
	  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DSMESH */
	  edgegsttax[edgelocnum] = vertgsttax[nghblocidx].indxnum;
    }
    else /* If local neighbor */
      edgegsttax[edgelocnum] = nghblocnum - vertlocbas;
  }

  memFreeGroup (vertsidtax + baseval);



  // XXX pour construire la edgegsttax, il faut 
  //  - créer un tableau du nb de sommets fantômes (sera peut-être plus grand que nécessaire si recouvrement > 1)
  //  - le mettre par défaut à -1
  //  - le remplir pour chaque sommet local avec le numéro global
  //  - faire une comm en utilisant notre fameuse struct de comm
  //  - rechercher le premier -1 qui nous déterminera le nb réel de sommets fantômes
  //  - allouer edgegsttax
  //  - parcourir les sommets locaux
  //    - pour chaque voisin local, mettre dans edgegsttax le vertlocnum
  //    - pour chaque voisin distant, le rechercher dans le tableau qu'on a échangé et mettre l'indice dans edgegsttax


  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

