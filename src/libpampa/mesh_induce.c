/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_induce.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the centralized source
//!                induced mesh building routines. 
//!
//!   \date        Version 2.1: from: 25 Aug 2017
//!
/************************************************************/

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

//! This routine XXX
//! On suppose que le tableau d'éléments elemtab est trié
//! XXX BUG il manque les sous-entités à gérer
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshInduce (
Mesh * const         orgmeshptr,        //!< Original mesh structure
const Gnum           elemnbr,           //!< Gnumber of elements
const Gnum *         elemtab,           //!< Array of elements
Mesh * const         indmeshptr)        //!< Induced mesh structure
{
  Gnum baseval;
  Gnum * restrict fvrttax;
  Gnum vertnum;
  Gnum vertnnd;
  Gnum vertnbr;
  Gnum indvertnum;
  Gnum indvertnnd;
  Gnum indvertnbr;
  Gnum indedgenum;
  Gnum indedgenbr;
  Gnum indedgesiz;
  Gnum indedlosiz;
  Gnum ventnum;
  Gnum ventnnd;
  Gnum elemidx;
  Gnum elemnnd;
  Gnum * restrict indverttax;
  Gnum * restrict indvendtax;
  Gnum * restrict indedgetax;
  Gnum * restrict indedlotax;
  Gnum * restrict permtax;
  Values * restrict valsptr;

  baseval = orgmeshptr->baseval;
  
  vertnbr = orgmeshptr->vertnbr;

  if (memAllocGroup ((void **) (void *)
        &fvrttax, (size_t) (vertnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  fvrttax -= baseval;

  memSet (fvrttax + baseval, ~0, vertnbr * sizeof (Gnum));
  // on marque les sommets à garder
  for (elemidx = baseval, elemnnd = baseval + elemnbr; elemidx < elemnnd; elemidx ++) {
    Gnum elemnum;
    Gnum nentnum;
    Gnum nentnnd;

    elemnum = elemtab[elemidx];

    for (nentnum = baseval, nentnnd = baseval + orgmeshptr->enttnbr; nentnum < nentnnd; nentnum ++) {
      Gnum edgenum;
      Gnum edgennd;
      MeshEnttNghb * restrict nghbptr = orgmeshptr->entttax[baseval]->nghbtax[nentnum];

      for (edgenum = nghbptr->vindtax[elemnum].vertidx, edgennd = nghbptr->vindtax[elemnum].vendidx;
          edgenum < edgennd; edgenum ++) {
        Gnum engbnum;
        Gnum mngbnum;

        engbnum = orgmeshptr->edgetax[edgenum];
        // XXX attention aux sous-entités !
        mngbnum = orgmeshptr->entttax[nentnum]->mvrttax[engbnum];
        fvrttax[mngbnum] = 0;
      }
    }
  }

  // on renumérote les sommets
  for (indvertnum = vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++)
    if (fvrttax[vertnum] == 0)
      fvrttax[vertnum] = indvertnum ++;
  indvertnbr = indvertnum - baseval;

  indedlosiz = (orgmeshptr->edlotax == NULL) ? 0 : orgmeshptr->edgesiz;
  if (memAllocGroup ((void **) (void *)
        &indverttax, (size_t) (indvertnbr          * sizeof (Gnum)),
        &indvendtax, (size_t) (indvertnbr          * sizeof (Gnum)),
        &indedgetax, (size_t) (orgmeshptr->edgesiz * sizeof (Gnum)), // XXX est-ce pertinent de mettre aussi grand ?
        &indedlotax, (size_t) (indedlosiz          * sizeof (Gnum)),
        &permtax,    (size_t) (vertnbr             * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  indverttax -= baseval;
  indvendtax -= baseval;
  indedgetax -= baseval;


  // on reparcourt le maillage pour construire tous les tableaux nécessaires au meshBuild
  for (indedgenum = ventnum = baseval, ventnnd = baseval + orgmeshptr->enttnbr; ventnum < ventnnd; ventnum ++) {
    for (vertnum = baseval, vertnnd = baseval + orgmeshptr->entttax[ventnum]->vertnbr;
        vertnum < vertnnd; vertnum ++) {
      Gnum mvrtnum;
      Gnum nentnum;
      Gnum nentnnd;

      mvrtnum = orgmeshptr->entttax[ventnum]->mvrttax[vertnum];

      indverttax[fvrttax[mvrtnum]] = indedgenum;

      for (nentnum = baseval, nentnnd = baseval + orgmeshptr->enttnbr; nentnum < nentnnd; nentnum ++) {
        Gnum edgenum;
        Gnum edgennd;

        MeshEnttNghb * restrict nghbptr = orgmeshptr->entttax[baseval]->nghbtax[nentnum];

        for (edgenum = nghbptr->vindtax[vertnum].vertidx, edgennd = nghbptr->vindtax[vertnum].vendidx;
            edgenum < edgennd; edgenum ++) {
          Gnum engbnum;
          Gnum mngbnum;

          engbnum = orgmeshptr->edgetax[edgenum];
          // XXX attention aux sous-entités !
          mngbnum = orgmeshptr->entttax[nentnum]->mvrttax[engbnum];
          if (fvrttax[mngbnum] != ~0) {
            if (indedlotax != NULL)
              indedlotax[indedgenum] = orgmeshptr->edlotax[edgenum];
            indedgetax[indedgenum ++] = fvrttax[mngbnum];
          }
        }
      }

      indvendtax[fvrttax[mvrtnum]] = indedgenum;
    }
  }
  indedgesiz =
    indedgenbr = indedgenum - baseval;

  CHECK_FDBG2 (meshBuild (indmeshptr, ~0, baseval, indvertnbr, indverttax, indvendtax, indedgenbr, indedgesiz, indedgetax, indedlotax, orgmeshptr->enttnbr, orgmeshptr->venttax, (orgmeshptr->esubmsk == 0) ? NULL : orgmeshptr->esubbax, orgmeshptr->valsptr->valumax, NULL, ~0, ~0));

  // on s'occupe des valeurs associées
  // il manquera les sous-entités
  for (ventnum = baseval, ventnnd = baseval + orgmeshptr->enttnbr;
      ventnum < ventnnd; ventnum ++) 
    for (indvertnum = baseval, indvertnnd = indmeshptr->entttax[ventnum]->vertnbr;
        indvertnum < indvertnnd; indvertnum ++) {
      Gnum indmvrtnum;

      indmvrtnum = indmeshptr->entttax[ventnum]->mvrttax[vertnum];
      permtax[indmvrtnum] = indvertnum;
    }

  for (ventnum = baseval, ventnnd = baseval + orgmeshptr->enttnbr;
      ventnum < ventnnd; ventnum ++) 
    for (vertnum = baseval, vertnnd = orgmeshptr->entttax[ventnum]->vertnbr;
        vertnum < vertnnd; vertnum ++) {
      Gnum orgmvrtnum;

      orgmvrtnum = orgmeshptr->entttax[ventnum]->mvrttax[vertnum];

      fvrttax[orgmvrtnum] = permtax[fvrttax[orgmvrtnum]];
    }

  for (valsptr = orgmeshptr->valsptr, ventnum = baseval, ventnnd = baseval + orgmeshptr->enttnbr;
      ventnum < ventnnd; ventnum ++) {
    Value * valuptr;

    if ((orgmeshptr->entttax[ventnum] == NULL) || (valsptr->evaltak[ventnum] == NULL) /* If empty entity or no value */
        || (orgmeshptr->esubbax[ventnum & orgmeshptr->esubmsk] > PAMPA_ENTT_SINGLE)) /* If sub-entity */
      continue;


    for (valuptr = valsptr->evaltak[ventnum]; valuptr != NULL; valuptr = valuptr->next) {
      MPI_Aint            valusiz;                 /* Extent of attribute datatype */
      MPI_Aint            dummy;
      byte *              valutax;
      Gnum                orgvertnum;
      Gnum                orgvertnnd;

      MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */

      CHECK_FDBG2 (meshValueLink(indmeshptr, (void **) &valutax, PAMPA_VALUE_NONE, NULL, NULL, valuptr->typeval, ventnum, valuptr->tagnum));
      valutax -= baseval * valusiz;

      for (orgvertnum = baseval, orgvertnnd = baseval + orgmeshptr->entttax[ventnum]->vertnbr;
          orgvertnum < orgvertnnd; orgvertnum ++) {
        Gnum orgmvrtnum;
        
        orgmvrtnum = orgmeshptr->entttax[ventnum]->mvrttax[vertnum];
        indvertnum = fvrttax[orgmvrtnum];

        memCpy((byte *) valutax + indvertnum * valusiz, (byte *) valuptr->valutab + orgvertnum * valusiz, valusiz * sizeof (byte));
      }
    }
  }


  return (0);
}
