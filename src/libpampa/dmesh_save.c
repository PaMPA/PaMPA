/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_save.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the distributed mesh
//!                general purpose routines. 
//!
//!   \date        Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/


/*
 ** The defines and includes.
 */

#define DMESH_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"
#include "dmesh_gather_induce_multiple.h"

//! XXX
//! It returns:
//! - 0 XXX
//! XXX

int
dmeshSave2 (
    Dmesh * restrict const meshptr,
    char * const fileval,
    int (*EXT_meshSave) (PAMPA_Mesh * const imshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void * const              dataptr)
{
  Gnum * restrict vnumgsttax;
  Gnum * restrict parttax;
  Mesh * imshloctab;
  Gnum meshlocnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum baseval;
  int procngbnum;
  int cheklocval;
  // XXX toutes les parties avec PAMPA_NOT_REF_ONE sont temporaires, pour
  // enregistrer la partie du maillage remaillé
#ifdef PAMPA_NOT_REF_ONE
  PAMPA_Num * ereftax;
#endif /* PAMPA_NOT_REF_ONE */

  cheklocval = 0;
  baseval = meshptr->baseval;

  if (memAllocGroup ((void **) (void *)
                          &vnumgsttax, (size_t) (meshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
                          &parttax,    (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
                          NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  vnumgsttax -= baseval;
  parttax -= baseval;

#ifdef PAMPA_NOT_REF_ONE
  CHECK_FERR (dmeshValueData (meshptr, baseval, PAMPA_TAG_REF, NULL, NULL, (void **) &ereftax), meshptr->proccomm);
  ereftax -= baseval;
#endif /* PAMPA_NOT_REF_ONE */

  for (vertlocnum = baseval, vertlocnnd = meshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
#ifdef PAMPA_NOT_REF_ONE
    if ((!strcmp (fileval, "part_remesh.mesh")) && (ereftax[vertlocnum] != PAMPA_REF_IS))
      vnumgsttax[vertlocnum] = 0;
    else
      vnumgsttax[vertlocnum] = ~0;
#else /* PAMPA_NOT_REF_ONE */
    vnumgsttax[vertlocnum] = meshptr->proclocnum;
#endif /* PAMPA_NOT_REF_ONE */
  for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) 
          parttax[procngbnum + baseval] = procngbnum;

  imshloctab = NULL;
  meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
  CHECK_FERR (dmeshGatherInduceMultiple (meshptr, 0, meshptr->procglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), meshptr->proccomm);

  if (meshlocnbr == 1) {
    //for (procngbnum = 0; procngbnum < 24; procngbnum ++) {
    //  if ((meshptr->proclocnum % 24) == procngbnum)
#ifdef PAMPA_NOT_REF_ONE
    CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, dataptr, NULL, fileval), meshptr->proccomm);
#else /* PAMPA_NOT_REF_ONE */
    CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, dataptr, vnumgsttax + baseval, fileval), meshptr->proccomm);
#endif /* PAMPA_NOT_REF_ONE */
    //  commBarrier (meshptr->proccomm);
    //}

    meshExit (imshloctab);
  }
  if (imshloctab != NULL)
    memFree (imshloctab);

  memFreeGroup (vnumgsttax + baseval);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}
