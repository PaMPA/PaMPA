/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_overlap.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the distributed source
//!                mesh overlap building routines. 
//!
//!   \date        Version 1.0: from: 22 Aug 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define DMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_ghst.h"
#include "pampa.h"
#include "dmesh_overlap.h"

//! This routine builds the overlap of a distributed mesh XXX
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
dmeshOverlap (
    Dmesh * restrict const      meshptr,              //!< mesh
    const Gnum                  vertlocnbr,           //!< Number of local vertices
    Gnum * const                vertloctax,           //!< Local vertex begin array
    Gnum * const                vendloctax,           //!< Local vertex end array
    const Gnum                  edgelocsiz,           //!< XXX
    Gnum * restrict const       edgeloctax,           //!< Local edge array
    Gnum *                      ventloctax,           //!< Local vertex entity array // XXX Suggestion: remove ventloctax, put sub-entities instead of entities in ventgsttax (see commit 216ffd8ec485b3babae)
    Gnum * restrict *           ventgsttax,           //!< XXX
    Gnum *                      vertovpnbr,           //!< XXX
    Gnum * restrict *           vertovptax,           //!< XXX
    Gnum * restrict *           vendovptax,           //!< XXX
    Gnum * restrict *           vnumovptax,           //!< XXX
    Gnum *                      edgeovpnbr,           //!< XXX
    Gnum * restrict *           edgegsttax,           //!< XXX
    const Gnum                  ovlpglbval)              //!< Size of the overlap
{

  Gnum                  baseval;
  Gnum                  elemnum;
  Gnum                  ovlpglbvl2;
  Gnum                  vertlocmin;
  Gnum                  vertlocmax;
  Gnum *                flagtax;
  int *                esndcnttab; // esnd means elemsnd and esnd
  int *                esnddsptab;
  int *                ercvcnttab;
  int *                ercvdsptab;
  Gnum                  mainentnum;
  Gnum                  vertlocend;
  int                   procngbnum;
  Gnum                  esndnbr;
  Gnum *                esndtab;
  Gnum *                ercvtab;
  Gnum                  vertlocbas;
  Gnum                  ercvnbr;
  Gnum *                proctab;
  Gnum                  vnbrlocval;
  Gnum                  enttnum;
  Gnum                  vertlocnum;
  Gnum                  vertlocnnd;
  Gnum                  flagnum;
  Gnum                  flagnnd;
  Gnum                  enttnnd;
  DmeshOvlpProcVert * restrict hashtab;
  Gnum                          hashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          hashmax;
  Gnum                          hashnbr;
  Gnum                          hashmsk;
  Gnum ovlptmp;
  Gnum * elemloctab;
  Gnum vertflag;
  Gnum elemflag;
  int cheklocval;

  cheklocval = 0;
  ovlpglbvl2 = ~PAMPA_OVLP_FLAGS & ovlpglbval;

  if ((ovlpglbval & PAMPA_OVLP_ELEM) != 0) {
	vertflag = FLAG_HALO;
	elemflag = FLAG_HALO | FLAG_OVLP;
  }
  else {
	vertflag = FLAG_OVLP;
	elemflag = FLAG_HALO;
  }

  hashnbr = MAX (vertlocnbr / 10, 10);
  for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */
  hashnbr = 0;
  //hashnbr = 1;
  //for (hashsiz = 4; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */

  hashmsk = hashsiz - 1;
  hashmax = hashsiz >> 2;


  //Gnum                  vertlocnum;
  //Gnum                  vertlocnbr;
  //Gnum                  edgelocnum;
  //Gnum                  baseval;
  //NghbProcHash        * hashtab;
  //Gnum                  hashmsk;
  //Gnum                  hashsiz;
  //Gnum                  hashnbr;



  baseval = meshptr->baseval;
  vertlocmin = meshptr->procvrttab[meshptr->proclocnum];
  vertlocmax = meshptr->procvrttab[meshptr->proclocnum + 1];
  vertlocbas = vertlocmin - meshptr->baseval;

  if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
        &proctab,       (size_t) (meshptr->procglbnbr           * sizeof (Gnum)),
        &esndcnttab, (size_t) (meshptr->procglbnbr           * sizeof (int)),
        &esnddsptab, (size_t) ((meshptr->procglbnbr + 1)     * sizeof (int)),
        &ercvcnttab, (size_t) (meshptr->procglbnbr           * sizeof (int)),
        &ercvdsptab, (size_t) ((meshptr->procglbnbr + 1)     * sizeof (int)),
        (void *) NULL) == NULL) {
    Gnum                dummyval[2];
    Gnum                dummytab[meshptr->procglbnbr * 2];

    errorPrint ("out of memory");
    dummyval[0] =
      dummyval[1] = -1;
	CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commAllgather (dummyval, 2, MPI_INT,    /* Use dummy receive array (if can be allocated too) */
          dummytab, 2, MPI_INT, meshptr->proccomm) != MPI_SUCCESS)
      errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  mainentnum = baseval; // XXX 0 pour l'entité principale, à changer avec les groupes et peut-être utiliser plutôt vsubloctax

  if ((flagtax = (Gnum *) memAlloc (vertlocnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  flagtax -= baseval;


  vnbrlocval = baseval;

  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {

    enttnum = (*ventgsttax)[vertlocnum]; // XXX ventgsttax contient de manière temporaire le numéro d'entité et nom de sous-entité

    if (enttnum == PAMPA_ENTT_DUMMY)
      continue;
    if (enttnum == mainentnum)
      flagtax[vnbrlocval ++] = vertlocnum;
  }

  if ((flagtax = (Gnum *) memRealloc (flagtax + baseval, vnbrlocval * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  flagtax -= baseval;


  procngbnum = 0;


  if ((hashtab = (DmeshOvlpProcVert *) memAlloc (hashsiz * sizeof (DmeshOvlpProcVert))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  memSet (hashtab, ~0, hashsiz * sizeof (DmeshOvlpProcVert));

  if ((elemloctab = (Gnum *) memAlloc (vnbrlocval * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  memSet (elemloctab, 0, vnbrlocval * sizeof (Gnum));



  for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {
    Gnum                edgelocnum;

    vertlocnum = flagtax[flagnum];

    for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {

      vertlocend = edgeloctax[edgelocnum];


      if (!((vertlocend >= vertlocmin) && (vertlocend < vertlocmax))) { /* If edge is not local */




        if (!((vertlocend >= meshptr->procvrttab[procngbnum]) && (vertlocend < meshptr->procvrttab[procngbnum + 1]))) {
          int procngbmax;
          for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              procngbmax - procngbnum > 1; ) {
            int                 procngbmed;

            procngbmed = (procngbmax + procngbnum) / 2;
            if (meshptr->procvrttab[procngbmed] <= vertlocend)
              procngbnum = procngbmed;
            else
              procngbmax = procngbmed;
          }
        }

        if ((*ventgsttax)[(*edgegsttax)[edgelocnum]] != mainentnum) { // it is not an element
          Gnum bvrtlocnum;

          bvrtlocnum = vertlocnum + vertlocbas;

          for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != bvrtlocnum || hashtab[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

          if ((hashtab[hashnum].vertnum != bvrtlocnum) || (hashtab[hashnum].procnum != procngbnum)) { /* Vertex not already added */
            elemloctab[flagnum] ++;
            hashtab[hashnum].vertnum = bvrtlocnum;
            hashtab[hashnum].procnum = procngbnum;
            hashtab[hashnum].flagval = vertflag;
            hashtab[hashnum].ovlpval = 1;
            hashnbr ++;

            if (hashnbr >= hashmax)  /* If hashtab is too much filled */
			  CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
          }
          else /* Vertex already added */
            hashtab[hashnum].flagval |= vertflag;
        }
        else { // it is an element
          Gnum bvrtlocnum;

          bvrtlocnum = vertlocnum + vertlocbas;

          for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != bvrtlocnum || hashtab[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

          if ((hashtab[hashnum].vertnum != bvrtlocnum) || (hashtab[hashnum].procnum != procngbnum)) { /* Vertex not already added */
            elemloctab[flagnum] ++;
            hashtab[hashnum].vertnum = bvrtlocnum;
            hashtab[hashnum].procnum = procngbnum;
            hashtab[hashnum].flagval = elemflag;
            hashtab[hashnum].ovlpval = 1;
            hashnbr ++;

            if (hashnbr >= hashmax)  /* If hashtab is too much filled */
			  CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
          }
          else /* Vertex already added */
            hashtab[hashnum].flagval |= elemflag;
        }
      }
    }
  }

  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
    Gnum                edgelocnum;
    Gnum bvrtlocend;

    if ((*ventgsttax)[vertlocnum] != mainentnum) { // if is not an element
      for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {

        bvrtlocend = (*edgegsttax)[edgelocnum];
        if ((bvrtlocend >= vertlocnbr + meshptr->baseval) && ((*ventgsttax)[bvrtlocend] == mainentnum)) { /* If edge is not local and neighbor is an element */

          vertlocend = edgeloctax[edgelocnum];
          if (!((vertlocend >= meshptr->procvrttab[procngbnum]) && (vertlocend < meshptr->procvrttab[procngbnum + 1]))) {
            int procngbmax;
            for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
                procngbmax - procngbnum > 1; ) {
              int                 procngbmed;

              procngbmed = (procngbmax + procngbnum) / 2;
              if (meshptr->procvrttab[procngbmed] <= vertlocend)
                procngbnum = procngbmed;
              else
                procngbmax = procngbmed;
            }
          }

          for (hashnum = (vertlocend * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != vertlocend || hashtab[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

          if ((hashtab[hashnum].vertnum != vertlocend) || (hashtab[hashnum].procnum != procngbnum)) { /* Vertex not already added */
            hashtab[hashnum].vertnum = vertlocend;
            hashtab[hashnum].procnum = procngbnum;
        	hashtab[hashnum].flagval = FLAG_OVLP;
            hashtab[hashnum].ovlpval = 1;
            hashnbr ++;

            if (hashnbr >= hashmax)  /* If hashtab is too much filled */
			  CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
          }
          else /* Vertex already added */
            hashtab[hashnum].flagval |= FLAG_OVLP;
        }
      }
    }
  }








  for (ovlptmp = 2; ovlptmp <= ovlpglbvl2; ovlptmp ++) {
    Gnum ercvidx;


    memSet (esndcnttab, 0, meshptr->procglbnbr * sizeof (int));

    for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {

      if (elemloctab[flagnum] > 0) {
        Gnum hashtmp;
        Gnum bvrtlocnum;

        vertlocnum = flagtax[flagnum];
        bvrtlocnum = vertlocnum + vertlocbas;

        for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

        hashtmp = hashnum;
        for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
          if ((hashtab[hashnum].vertnum == bvrtlocnum) && ((hashtab[hashnum].flagval & FLAG_HALO) != 0)) {
            Gnum hashnm2;

            procngbnum = hashtab[hashnum].procnum;
            esndcnttab[procngbnum] ++;

            for (hashnm2 = hashtmp; hashtab[hashnm2].vertnum != ~0; hashnm2 = (hashnm2 + 1) & hashmsk)
              if ((hashtab[hashnm2].vertnum == bvrtlocnum) && (hashtab[hashnm2].procnum != procngbnum))
                esndcnttab[procngbnum] ++;
          }
	  }
    }

    esnddsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
      esnddsptab[procngbnum] = esnddsptab[procngbnum - 1] + esndcnttab[procngbnum - 1];
    }
    esndnbr = esnddsptab[meshptr->procglbnbr] ;
    if ((esndtab = (Gnum *) memAlloc (esndnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);

    for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {

      if (elemloctab[flagnum] > 0) {
        Gnum bvrtlocnum;
        Gnum hashtmp;

        vertlocnum = flagtax[flagnum];
        bvrtlocnum = vertlocnum + vertlocbas;

        for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

        hashtmp = hashnum;

        for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
          if ((hashtab[hashnum].vertnum == bvrtlocnum) && ((hashtab[hashnum].flagval & FLAG_HALO) != 0)) {
            Gnum hashnm2;

            procngbnum = hashtab[hashnum].procnum;
            esndtab[esnddsptab[procngbnum] ++] = -hashtab[hashnum].vertnum - 1;

            for (hashnm2 = hashtmp; hashtab[hashnm2].vertnum != ~0; hashnm2 = (hashnm2 + 1) & hashmsk)
              if ((hashtab[hashnm2].vertnum == bvrtlocnum) && (hashtab[hashnm2].procnum != procngbnum))
                esndtab[esnddsptab[procngbnum] ++] = hashtab[hashnm2].procnum;
          }
	  }
    }

    for (procngbnum = meshptr->procglbnbr ; procngbnum > 0 ; procngbnum --) {
      esnddsptab[procngbnum] = esnddsptab[procngbnum - 1];
    }
    esnddsptab[0] = 0;

	CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commAlltoall(esndcnttab, 1, MPI_INT, ercvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);

    // Computing displacement arrays
    ercvdsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
      ercvdsptab[procngbnum] = ercvdsptab[procngbnum - 1] + ercvcnttab[procngbnum - 1];
    }
    ercvnbr = ercvdsptab[meshptr->procglbnbr];

    if ((ercvtab = (Gnum *) memAlloc (ercvnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);

    // Sending elements and processors
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commAlltoallv(esndtab, esndcnttab, esnddsptab, GNUM_MPI, ercvtab, ercvcnttab, ercvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);

    for (ercvidx = 0; ercvidx < ercvnbr; ercvidx ++) {

      if (ercvtab[ercvidx] < 0) { // if element number (-(vertlocnum + vertlocbas) where vertlocbas += 1 
        elemnum = - ercvtab[ercvidx] - 1;
      }
      else { // if processor number
        procngbnum = ercvtab[ercvidx];
        for (hashnum = (elemnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != elemnum || hashtab[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

        if ((hashtab[hashnum].vertnum != elemnum) || (hashtab[hashnum].procnum != procngbnum)) { /* Vertex not already added */
          hashtab[hashnum].vertnum = elemnum;
          hashtab[hashnum].procnum = procngbnum;
          hashtab[hashnum].flagval = FLAG_OVLP; // XXX peut-être mettre un autre drapeau, non ??
          hashtab[hashnum].ovlpval = ovlptmp;
          hashnbr ++;

          if (hashnbr >= hashmax) /* If hashtab is too much filled */
			CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
        }
		else /* Vertex already added */
          hashtab[hashnum].flagval |= FLAG_OVLP; // XXX peut-être mettre un autre drapeau, non ??
      }
    }

    for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {

      if (elemloctab[flagnum] != 0) {
        Gnum                edgelocnum;
        Gnum bvrtlocnum;

        vertlocnum = flagtax[flagnum];
        bvrtlocnum = vertlocnum + vertlocbas;

    	memSet (proctab,     0, meshptr->procglbnbr * sizeof (Gnum)); // XXX utiliser un procnbr et avoir les numéros de procs dans le tableau

        for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

        for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
          if ((hashtab[hashnum].vertnum == bvrtlocnum) && (hashtab[hashnum].ovlpval < ovlptmp)) {
            if ((hashtab[hashnum].flagval & FLAG_OVLP) != 0)
			  proctab[hashtab[hashnum].procnum] = FLAG_OVLP;
		  	else
			  hashtab[hashnum].flagval |= FLAG_OVLP;
		  }

        for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {
          Gnum vertlocend;

          vertlocend = (*edgegsttax)[edgelocnum];



          if ((vertlocend < vertlocnbr + meshptr->baseval) && ((*ventgsttax)[vertlocend] == mainentnum)) { /* If edge is local and neighbor is an element */
            Gnum bvrtlocend;
            Gnum hashnm2;

            bvrtlocend = edgeloctax[edgelocnum];

  			for (procngbnum = 0 ; procngbnum < meshptr->procglbnbr; procngbnum ++) 
			  if (proctab[procngbnum] != 0) {

                for (hashnm2 = (bvrtlocend * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnm2].vertnum != ~0 && (hashtab[hashnm2].vertnum != bvrtlocend || hashtab[hashnm2].procnum != procngbnum); hashnm2 = (hashnm2 + 1) & hashmsk) ;

                if ((hashtab[hashnm2].vertnum != bvrtlocend) || (hashtab[hashnm2].procnum != procngbnum)) { /* Vertex not already added */
                  Gnum flagmax;
                  Gnum flagnm2;

                  for (flagnm2 = 0, flagmax = vnbrlocval;
                      flagmax - flagnm2 > 1; ) {
                    Gnum                 flagmed;

                    flagmed = (flagmax + flagnm2) / 2;
                    if (flagtax[flagmed] <= vertlocend)
                      flagnm2 = flagmed;
                    else
                      flagmax = flagmed;
                  }

                  elemloctab[flagnm2] ++;
                  hashtab[hashnm2].vertnum = bvrtlocend;
                  hashtab[hashnm2].procnum = procngbnum;
          		  hashtab[hashnm2].flagval = FLAG_OVLP;
          		  hashtab[hashnm2].ovlpval = ovlptmp;
                  hashnbr ++;

                  if (hashnbr >= hashmax) /* If hashtab is too much filled */
					CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
                }
				else /* Vertex already added */
          		  hashtab[hashnm2].flagval |= FLAG_OVLP; // XXX peut-être mettre un autre drapeau, non ??
              }
          }
        }
      }
    }

    // Propagate distant element overlap on local elements
    for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {
      Gnum                edgelocnum;

      vertlocnum = flagtax[flagnum];

      for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {
        Gnum vertlocend;

        vertlocend = (*edgegsttax)[edgelocnum];



        if ((vertlocend >= vertlocnbr + meshptr->baseval) && ((*ventgsttax)[vertlocend] == mainentnum)) { /* If edge is not local and neighbor is an element */
          Gnum bvrtlocnum;
          Gnum bvrtlocend;

          bvrtlocnum = vertlocnum + vertlocbas;
          bvrtlocend = edgeloctax[edgelocnum];
          for (hashnum = (bvrtlocend * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocend; hashnum = (hashnum + 1) & hashmsk) ;

          for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
            if (hashtab[hashnum].vertnum == bvrtlocend) {
              Gnum hashnm2;

              procngbnum = hashtab[hashnum].procnum;

              for (hashnm2 = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnm2].vertnum != ~0 && (hashtab[hashnm2].vertnum != bvrtlocnum || hashtab[hashnm2].procnum != procngbnum); hashnm2 = (hashnm2 + 1) & hashmsk) ;

              if ((hashtab[hashnm2].vertnum != bvrtlocnum) || (hashtab[hashnm2].procnum != procngbnum)) { /* Vertex not already added */
                elemloctab[flagnum] ++;
                hashtab[hashnm2].vertnum = bvrtlocnum;
                hashtab[hashnm2].procnum = procngbnum;
          		hashtab[hashnm2].flagval = FLAG_OVLP;
          		hashtab[hashnm2].ovlpval = ovlptmp;
                hashnbr ++;

                if (hashnbr >= hashmax) /* If hashtab is too much filled */
				  CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
              }
			  else /* Vertex already added */
          		hashtab[hashnum].flagval |= FLAG_OVLP; // XXX peut-être mettre un autre drapeau, non ??
            }
        }
      }
    }
    memFree (esndtab);
    memFree (ercvtab);
  }






  Gnum ercvidx;

  memSet (esndcnttab, 0, meshptr->procglbnbr * sizeof (int));

  for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {

    if (elemloctab[flagnum] > 1) {
      Gnum hashtmp;
      Gnum bvrtlocnum;

      vertlocnum = flagtax[flagnum];
      bvrtlocnum = vertlocnum + vertlocbas;

      for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

      hashtmp = hashnum;
      for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
        if (hashtab[hashnum].vertnum == bvrtlocnum) {
          Gnum hashnm2;

          procngbnum = hashtab[hashnum].procnum;
          esndcnttab[procngbnum] ++;

          for (hashnm2 = hashtmp; hashtab[hashnm2].vertnum != ~0; hashnm2 = (hashnm2 + 1) & hashmsk)
            if ((hashtab[hashnm2].vertnum == bvrtlocnum) && (hashtab[hashnm2].procnum != procngbnum) && ((hashtab[hashnm2].flagval & FLAG_OVLP) != 0))
              esndcnttab[procngbnum] ++;
        }
    }
  }

  esnddsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
    esnddsptab[procngbnum] = esnddsptab[procngbnum - 1] + esndcnttab[procngbnum - 1];
  }
  esndnbr = esnddsptab[meshptr->procglbnbr] ;
  if ((esndtab = (Gnum *) memAlloc (esndnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  for (flagnum = baseval, flagnnd = vnbrlocval ; flagnum < flagnnd ; flagnum ++) {

    if (elemloctab[flagnum] > 1) {
      Gnum bvrtlocnum;
      Gnum hashtmp;

      vertlocnum = flagtax[flagnum];
      bvrtlocnum = vertlocnum + vertlocbas;

      for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

      hashtmp = hashnum;

      for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
        if (hashtab[hashnum].vertnum == bvrtlocnum) {
          Gnum hashnm2;

          procngbnum = hashtab[hashnum].procnum;
          esndtab[esnddsptab[procngbnum] ++] = -hashtab[hashnum].vertnum - 1;

          for (hashnm2 = hashtmp; hashtab[hashnm2].vertnum != ~0; hashnm2 = (hashnm2 + 1) & hashmsk)
            if ((hashtab[hashnm2].vertnum == bvrtlocnum) && (hashtab[hashnm2].procnum != procngbnum) && ((hashtab[hashnm2].flagval & FLAG_OVLP) != 0))
              esndtab[esnddsptab[procngbnum] ++] = hashtab[hashnm2].procnum;
        }
    }
  }
  memFree (flagtax + baseval);

  for (procngbnum = meshptr->procglbnbr ; procngbnum > 0 ; procngbnum --) {
    esnddsptab[procngbnum] = esnddsptab[procngbnum - 1];
  }
  esnddsptab[0] = 0;

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoall(esndcnttab, 1, MPI_INT, ercvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  // Computing displacement arrays
  ercvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
    ercvdsptab[procngbnum] = ercvdsptab[procngbnum - 1] + ercvcnttab[procngbnum - 1];
  }
  ercvnbr = ercvdsptab[meshptr->procglbnbr];

  if ((ercvtab = (Gnum *) memAlloc (ercvnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  // Sending elements and processors
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoallv(esndtab, esndcnttab, esnddsptab, GNUM_MPI, ercvtab, ercvcnttab, ercvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  for (ercvidx = 0; ercvidx < ercvnbr; ercvidx ++) {
    if (ercvtab[ercvidx] < 0) { // if element number (-(vertlocnum + vertlocbas) where vertlocbas += 1 
      elemnum = - ercvtab[ercvidx] - 1;
    }
    else { // if processor number
      procngbnum = ercvtab[ercvidx];
      for (hashnum = (elemnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != elemnum || hashtab[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

      if ((hashtab[hashnum].vertnum != elemnum) || (hashtab[hashnum].procnum != procngbnum)) { /* Vertex not already added */
        hashtab[hashnum].vertnum = elemnum;
        hashtab[hashnum].procnum = procngbnum;
        hashtab[hashnum].flagval = FLAG_OVLP;
        hashnbr ++;

        if (hashnbr >= hashmax) /* If hashtab is too much filled */
		  CHECK_FERR (dmeshOvlpProcVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
      }
    }
  }

  memFree (esndtab);
  memFree (ercvtab);
  memFreeGroup (proctab);
  memFree (elemloctab);



  // XXX ici pour couper en 2
  CHECK_VERR (cheklocval, meshptr->proccomm);
  return dmeshOverlap2 (meshptr, vertlocnbr, vertloctax, vendloctax, edgelocsiz, edgeloctax, ventloctax, ventgsttax, vertovpnbr, vertovptax, vendovptax, vnumovptax, edgeovpnbr, edgegsttax, ovlpglbval, hashtab, hashsiz, hashnbr, hashmsk);
}


// hashtab contains only elements (enttnum 0), local as well as distant elements

int
dmeshOverlap2 (
    Dmesh * restrict const      meshptr,              //!< mesh
    const Gnum                  vertlocnbr,           //!< Number of local vertices
    Gnum * const                vertloctax,           //!< Local vertex begin array
    Gnum * const                vendloctax,           //!< Local vertex end array
    const Gnum                  edgelocsiz,           //!< XXX
    Gnum * restrict const       edgeloctax,           //!< Local edge array
    Gnum *                      ventloctax,           //!< Local vertex entity array // XXX Suggestion: remove ventloctax, put sub-entities instead of entities in ventgsttax (see commit 216ffd8ec485b3babae)
    Gnum * restrict *           ventgsttax,           //!< XXX
    Gnum *                      vertovpnbr,           //!< XXX
    Gnum * restrict *           vertovptax,           //!< XXX
    Gnum * restrict *           vendovptax,           //!< XXX
    Gnum * restrict *           vnumovptax,           //!< XXX
    Gnum *                      edgeovpnbr,           //!< XXX
    Gnum * restrict *           edgegsttax,           //!< XXX
    const Gnum                  ovlpglbval,              //!< Size of the overlap
  DmeshOvlpProcVert * restrict  hashtab,         
  Gnum                          hashsiz,          /* Size of hash table    */
  Gnum                          hashnbr,
  Gnum                          hashmsk)
{

  // XXX contertir le const hashtab en restrict ;)
  Gnum                  baseval;
  Gnum                  ovlpglbvl2;
  Gnum                  vertlocmin;
  Gnum                  vertlocmax;
  Gnum *                vertsidtab;
  Gnum * restrict       vnbrloctax;           //!< XXX
  int *                esndcnttab; // esnd means elemsnd and esnd
  int *                esnddsptab;
  int *                ercvcnttab;
  int *                ercvdsptab;
  int *                 procsidnbr;
  int                   procsidind;
  Gnum                  vertsidnum;
  Gnum                  mainentnum;
  Gnum                  vertlocend;
  int                   procngbnum;
  Gnum                  esndnbr;
  Gnum *                esndtab;
  Gnum *                ercvtab;
  Gnum                  vertlocbas;
  Gnum                  vertovpbas;
  Gnum                  delta;
  Gnum                  ercvnbr;
  Gnum *                sorttab;
  Gnum                  sortnum;
  Gnum                  edgenum;
  Gnum                  edgeovpnum;
  Gnum *                proctab;
  int                   procsidnum;
  Gnum *                eidxgsttax;
  Gnum *                edgeloctmx;
  Gnum                  enttnum;
  Gnum                  vertlocnum;
  Gnum                  vertlocnnd;
  DmeshEntity **        enttloctax;
  int * restrict        procsidtab;           /* Send index array                              */
  Gnum                  vertnum;
  Gnum                  vertovpnum;
  Gnum                  vertglbnum;
  Gnum *                vertnumtax;
  Gnum                  sortnnd;
  Gnum                  enttnnd;
  Gnum                          hashnum;          /* Hash value            */
  int cheklocval;

  cheklocval = 0;


  baseval = meshptr->baseval;
  vertlocmin = meshptr->procvrttab[meshptr->proclocnum];
  vertlocmax = meshptr->procvrttab[meshptr->proclocnum + 1];
  vertlocbas = vertlocmin - meshptr->baseval;
  mainentnum = baseval; // XXX 0 pour l'entité principale, à changer avec les groupes et peut-être utiliser plutôt vsubloctax
  enttloctax = meshptr->enttloctax;
  ovlpglbvl2 = ~PAMPA_OVLP_FLAGS & ovlpglbval;


  if (ovlpglbvl2 > 1) // first of all, all vertices will be in overlap because of ovlpglbval > 1
    for (hashnum = 0; hashnum < hashsiz; hashnum ++) 
	  if (hashtab[hashnum].vertnum != ~0)
		hashtab[hashnum].flagval = FLAG_OVLP;


  if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
        &vertsidtab,    (size_t) (meshptr->procglbnbr           * sizeof (Gnum)),
        &edgeloctmx,    (size_t) (meshptr->enttglbnbr           * sizeof (Gnum)),
        &proctab,       (size_t) (meshptr->procglbnbr           * sizeof (Gnum)),
        &eidxgsttax,    (size_t) (vertlocnbr                    * sizeof (Gnum)),
        &vertnumtax, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
        &esndcnttab, (size_t) (meshptr->procglbnbr           * sizeof (int)),
        &esnddsptab, (size_t) ((meshptr->procglbnbr + 1)     * sizeof (int)),
        &ercvcnttab, (size_t) (meshptr->procglbnbr           * sizeof (int)),
        &ercvdsptab, (size_t) ((meshptr->procglbnbr + 1)     * sizeof (int)),
        (void *) NULL) == NULL) {
    errorPrint ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  edgeloctmx -= baseval;
  eidxgsttax -= baseval;
  vertnumtax -= baseval;

  memSet (vertsidtab, ~0, meshptr->procglbnbr * sizeof (Gnum));
  memSet (edgeloctmx + baseval, 0, meshptr->enttglbnbr * sizeof (Gnum)); /* Array which contains edge numbers */

  if ((vnbrloctax = (Gnum *) memAlloc (meshptr->enttglbnbr * sizeof (Gnum))) == NULL) {
    memFree     (vnbrloctax);
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  vnbrloctax -= baseval;

  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    vnbrloctax[enttnum] = baseval; // XXX vnbrloctax est déja calculé dans dmeshBuild4, peut-on mutualiser ce calcul ??

  }


  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    Gnum esubnum;

    esubnum = ventloctax[vertlocnum];
    enttnum = (*ventgsttax)[vertlocnum]; // XXX ventgsttax contient de manière temporaire le numéro d'entité et nom de sous-entité

    if (enttnum == PAMPA_ENTT_DUMMY)
      continue;
    if (enttnum != esubnum)
      vnbrloctax[enttnum] ++;

    eidxgsttax[vertlocnum] = vnbrloctax[esubnum]++;
    enttloctax[esubnum]->vertlocnbr ++;

    edgeloctmx[esubnum] += vendloctax[vertlocnum] - vertloctax[vertlocnum];

  }
  memFree (vnbrloctax + baseval);



  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    if (enttloctax[enttnum] == NULL)
      continue;

    /* Allocate private entity neighbors data*/
    if (memAllocGroup ((void **) (void *)
          &enttloctax[enttnum]->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
          &enttloctax[enttnum]->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
          (void *) NULL) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);

    // XXX pour l'allocation, voir si on ne surestime pas trop
    edgeloctmx[enttnum] += enttloctax[enttnum]->vertlocnbr + hashnbr * meshptr->degrlocmax; /* edgeloctmx used to store the size of procsidtab for each entity */
    if ((meshptr->esublocbax[enttnum & meshptr->esublocmsk] >= PAMPA_ENTT_SINGLE) &&
        ((enttloctax[enttnum]->commlocdat.procsidtab = (int *) memAlloc (edgeloctmx[enttnum] * sizeof (int))) == NULL)) { /* TRICK: procsidtab for entities, which have sub-entities, will be allocated later */
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    enttloctax[enttnum]->commlocdat.procsndnbr = 0;
    enttloctax[enttnum]->commlocdat.procsidnbr = 0;

    memSet (enttloctax[enttnum]->commlocdat.procrcvtab, 0, meshptr->procglbnbr * sizeof (int));
    memSet (enttloctax[enttnum]->commlocdat.procsndtab, 0, meshptr->procglbnbr * sizeof (int));

  }

  if (meshptr->esublocmsk == 0) {
    procsidtab = enttloctax[mainentnum]->commlocdat.procsidtab;
    procsidnbr = &enttloctax[mainentnum]->commlocdat.procsidnbr;
  }



  memSet (esndcnttab,  0, meshptr->procglbnbr * sizeof (int));

  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    vertnumtax[enttnum] = baseval;
  }

  if ((sorttab = (Gnum *) memAlloc ((2 * vertlocnbr) * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);


  for (procngbnum = 0,sortnum = 0, vertlocnum = vertsidnum = baseval, vertlocnnd = vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
    Gnum                edgelocnum;

    memSet (proctab,     0, meshptr->procglbnbr * sizeof (Gnum)); // XXX utiliser un procnbr et avoir les numéros de procs dans le tableau


    if ((*ventgsttax)[vertlocnum] == PAMPA_ENTT_DUMMY)
      continue;

    if ((*ventgsttax)[vertlocnum] == mainentnum) {
      Gnum bvrtlocnum;

      bvrtlocnum = vertlocnum + vertlocbas;
      for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != bvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

      for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk) // TRICK: one vertex but many processors
        if (hashtab[hashnum].vertnum == bvrtlocnum)
          proctab[hashtab[hashnum].procnum] = hashtab[hashnum].flagval;
    }
    else /* Local vertex is not an element */
      for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {

        vertlocend = edgeloctax[edgelocnum];

        if ((vertlocend >= vertlocmin) && (vertlocend < vertlocmax)) { /* If edge is local */

          vertlocend -= vertlocbas;
          if ((*ventgsttax)[vertlocend] != mainentnum) /* Not an element */
            continue;

          vertlocend += vertlocbas;
          for (hashnum = (vertlocend * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != vertlocend; hashnum = (hashnum + 1) & hashmsk) ;

          for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
            if ((hashtab[hashnum].vertnum == vertlocend) && ((hashtab[hashnum].flagval & FLAG_OVLP) != 0))
              proctab[hashtab[hashnum].procnum] = FLAG_OVLP;
        }
        else { /* If edge is not local */
          int                 procngbmax;

          if (!((vertlocend >= meshptr->procvrttab[procngbnum]) && (vertlocend < meshptr->procvrttab[procngbnum + 1]))) {
            for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
                procngbmax - procngbnum > 1; ) {
              int                 procngbmed;

              procngbmed = (procngbmax + procngbnum) / 2;
              if (meshptr->procvrttab[procngbmed] <= vertlocend)
                procngbnum = procngbmed;
              else
                procngbmax = procngbmed;
            }
          }
          proctab[procngbnum] = FLAG_OVLP;

          if ((*ventgsttax)[(*edgegsttax)[edgelocnum]] == mainentnum) {
            for (hashnum = (vertlocend * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && hashtab[hashnum].vertnum != vertlocend; hashnum = (hashnum + 1) & hashmsk) ;

            for (; hashtab[hashnum].vertnum != ~0; hashnum = (hashnum + 1) & hashmsk)
              if (hashtab[hashnum].vertnum == vertlocend)
                proctab[hashtab[hashnum].procnum] = FLAG_OVLP;
          }
        }
      }

    vertnum = eidxgsttax[vertlocnum];
    enttnum = ventloctax[vertlocnum];
    procsidtab = enttloctax[enttnum]->commlocdat.procsidtab;
    procsidnbr = &enttloctax[enttnum]->commlocdat.procsidnbr;
    procsidind = *procsidnbr;

    for (procngbnum = 0 ; procngbnum < meshptr->procglbnbr ; procngbnum++) {
      if (proctab[procngbnum]) {
        if (vertsidtab[procngbnum] != vertlocnum) { /* If vertex not already sent to process  */


          vertsidtab[procngbnum] = vertlocnum;    /* Neighbor process will receive vertex     */
          esndcnttab[procngbnum] += 2;
          if (((*ventgsttax)[vertlocnum] != mainentnum) || (((*ventgsttax)[vertlocnum] == mainentnum) && ((proctab[procngbnum] & FLAG_OVLP) != 0))) /* Not an halo element */
            esndcnttab[procngbnum] += vendloctax[vertlocnum] - vertloctax[vertlocnum]; // neighbors sent only for overlap vertices
          enttloctax[enttnum]->commlocdat.procsndtab[procngbnum] ++;     /* One more vertex to send to this neighbor */

          while ((vertnum - vertnumtax[enttnum]) >= DMESHGHSTSIDMAX) { /* If Gnum range too long for int */
#ifdef PAMPA_DEBUG_DMESH
            if ((*procsidnbr) >= edgeloctmx[enttnum]) {
              errorPrint ("internal error");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
            procsidtab[(*procsidnbr) ++] = -DMESHGHSTSIDMAX; /* Decrease by maximum int distance      */
            vertnumtax[enttnum]               += DMESHGHSTSIDMAX;
          }
          if (vertnumtax[enttnum] != vertnum) {         /* If communication concerns new local vertex   */
#ifdef PAMPA_DEBUG_DMESH
            if ((*procsidnbr) >= edgeloctmx[enttnum]) {
              errorPrint ("internal error");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
            procsidtab[(*procsidnbr) ++] = - (vertnum - vertnumtax[enttnum]); /* Encode jump in procsidtab */

            vertnumtax[enttnum] = vertnum;              /* Current local vertex is last send vertex     */
          }
#ifdef PAMPA_DEBUG_DMESH
          if ((*procsidnbr) >= edgeloctmx[enttnum]) {
            errorPrint ("internal error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
          procsidtab[(*procsidnbr) ++] = procngbnum; /* Send this vertex data to this processor */
        }
      }
    }

    if (procsidind != *procsidnbr) {
      sorttab[sortnum ++] = enttnum;
      sorttab[sortnum ++] = vertlocnum;
    }
  }

  intSort2asc2(sorttab, sortnum / 2);


  // 2)
  esnddsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
    esnddsptab[procngbnum] = esnddsptab[procngbnum - 1] + esndcnttab[procngbnum - 1];
  }
  esndnbr = esnddsptab[meshptr->procglbnbr] ;
  if ((esndtab = (Gnum *) memAlloc (esndnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  // 3)
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    vertnumtax[enttnum] = baseval;
  }

  vertlocbas += 1; /* TRICK: vertlocbas will be reajusted */
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval, sortnum = 1;
      enttnum < enttnnd ; enttnum ++) {
    if (enttloctax[enttnum] == NULL)
      continue;

    procsidnbr = &enttloctax[enttnum]->commlocdat.procsidnbr;
    procsidtab = enttloctax[enttnum]->commlocdat.procsidtab;

    if ((*procsidnbr > 0) && (procsidtab[0] >= 0)) { /* If first element of procsidtab is a processor */
      vertlocnum = sorttab[sortnum];
      sortnum += 2;
    }


    for (procsidnum = 0 ; procsidnum < *procsidnbr ; procsidnum ++) {
      Gnum haloval;

      if (procsidtab[procsidnum] < 0) { /* If current value is a vertex or the maximum displacement */
        if (procsidtab[procsidnum] != DMESHGHSTSIDMAX) {
          vertlocnum = sorttab[sortnum];
          sortnum += 2;
        }

        continue;
      }

      procngbnum = procsidtab[procsidnum];

      haloval = 0;
      if ((enttnum == mainentnum) || (meshptr->esublocbax[enttnum & meshptr->esublocmsk] == mainentnum)) { /* If subentity of main entity */
        Gnum bvrtlocnum;

        bvrtlocnum = vertlocnum + vertlocbas - 1; //TRICK: vertlocbas is incremented
        for (hashnum = (bvrtlocnum * DMESHOVLPHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != bvrtlocnum || hashtab[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

        if ((hashtab[hashnum].vertnum != bvrtlocnum) || (hashtab[hashnum].procnum != procngbnum) || ((hashtab[hashnum].flagval & FLAG_OVLP) == 0))
          haloval = 1;
      }

      esndtab[esnddsptab[procngbnum] ++] = -(vertlocbas + vertlocnum);
      esndtab[esnddsptab[procngbnum] ++] = ventloctax[vertlocnum];
      if (haloval == 0) {
        delta = vendloctax[vertlocnum] - vertloctax[vertlocnum];
        memCpy(esndtab + esnddsptab[procngbnum], edgeloctax + vertloctax[vertlocnum], delta * sizeof (Gnum));
        esnddsptab[procngbnum] += delta;
      }
    }
  }
  vertlocbas -= 1;

  memFree (hashtab);


  for (procngbnum = meshptr->procglbnbr ; procngbnum > 0 ; procngbnum --) {
    esnddsptab[procngbnum] = esnddsptab[procngbnum - 1];
  }
  esnddsptab[0] = 0;

  // 4)
  // Sending number of vertices and neighbors, which will be in the overlap or
  // the halo
  // XXX attention on communique des entiers :(
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoall(esndcnttab, 1, MPI_INT, ercvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  // Computing displacement arrays
  ercvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
    ercvdsptab[procngbnum] = ercvdsptab[procngbnum - 1] + ercvcnttab[procngbnum - 1];
  }
  ercvnbr = ercvdsptab[meshptr->procglbnbr];

  if ((ercvtab = (Gnum *) memAlloc (ercvnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  // Sending vertices and neighbors, which will be in the overlap or the halo
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoallv(esndtab, esndcnttab, esnddsptab, GNUM_MPI, ercvtab, ercvcnttab, ercvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  // 4bis)
  if (ercvnbr > (2 * vertlocnbr)) {
    memFree (sorttab);
    if ((sorttab = (Gnum *) memAlloc (ercvnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);
  }

  sortnum = 0;

  // 5)
  // Searching distant vertices and their entity
  *vertovpnbr = 0;
  for (edgenum = 0 ; edgenum < ercvnbr ; edgenum ++) {
    if (ercvtab[edgenum] < 0) {
      sorttab[sortnum ++] = - ercvtab[edgenum ++] - 1; // vertex number
      if ((edgenum + 1 == ercvnbr) || (ercvtab[edgenum + 1] < 0)) // If halo vertex
        sorttab[sortnum ++] = ENTT_MAIN_INV; // main entity
      else 
        sorttab[sortnum ++] = ercvtab[edgenum];    // entity number

    }
  }

  sortnnd = sortnum;
  *vertovpnbr = sortnum / 2;

  *edgeovpnbr = ercvnbr - 2 * (*vertovpnbr);

  if ((sorttab = (Gnum *) memRealloc (sorttab, ((*vertovpnbr * 2) * sizeof (Gnum)))) == NULL) {
    memFree     (sorttab);
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);




  // 6)
  intSort2asc1 (sorttab, *vertovpnbr);

  // 7)

  memFreeGroup (*ventgsttax + baseval);
  if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
        vertovptax, (size_t) (*vertovpnbr                       * sizeof (Gnum)),
        vendovptax, (size_t) (*vertovpnbr                       * sizeof (Gnum)),
        ventgsttax, (size_t) ((vertlocnbr + *vertovpnbr)        * sizeof (Gnum)),
        vnumovptax, (size_t) (*vertovpnbr                       * sizeof (Gnum)),
        edgegsttax, (size_t) ((edgelocsiz + *edgeovpnbr)        * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  *vertovptax -= baseval;
  *vendovptax -= baseval;
  *ventgsttax -= baseval;
  *edgegsttax -= baseval;

  for (sortnum = 0 ; sortnum < sortnnd ; sortnum += 2) {
    (*vnumovptax)[sortnum >> 1] = sorttab[sortnum];
  }

  *vnumovptax -= baseval;

  memCpy(*ventgsttax + baseval, ventloctax + baseval, vertlocnbr * sizeof (Gnum));

  vertovpbas = vertlocnbr + baseval;
  for (procngbnum = edgenum = 0, vertovpnum = baseval, edgeovpnum = edgelocsiz + baseval;
      edgenum < ercvnbr ;
      edgenum ++) {

    if (ercvtab[edgenum] < 0) { // If vertex number (alias -v -1)
      Gnum vertovpend;
      Gnum vertovpmax;

      (*vendovptax)[vertovpnum] = edgeovpnum;
      for (vertovpnum = 0, vertovpmax = *vertovpnbr, vertglbnum = vertovpend = - ercvtab[edgenum ++] - 1;
          vertovpmax - vertovpnum > 1; ) {
        int                 vertovpmed;

        vertovpmed = (vertovpmax + vertovpnum) / 2;
        if (sorttab[2 * vertovpmed] <= vertovpend)
          vertovpnum = vertovpmed;
        else
          vertovpmax = vertovpmed;
      }
      vertovpnum += baseval;
      (*vertovptax)[vertovpnum] = edgeovpnum;
      enttnum = ercvtab[edgenum];
      (*ventgsttax)[vertovpnum + vertlocnbr] = enttnum;
      if (!((meshptr->procvrttab[procngbnum] <= vertglbnum) && (meshptr->procvrttab[procngbnum+1] > vertglbnum))) {
        int procngbmax;

        for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
            procngbmax - procngbnum > 1; ) {
          int                 procngbmed;

          procngbmed = (procngbmax + procngbnum) / 2;
          if (meshptr->procvrttab[procngbmed] <= vertglbnum)
            procngbnum = procngbmed;
          else
            procngbmax = procngbmed;
        }
      }

      enttloctax[enttnum]->commlocdat.procrcvtab[procngbnum]++;
      continue;
    }

    vertlocend = ercvtab[edgenum];
    if ((vertlocend >= vertlocmin) && (vertlocend < vertlocmax)) { /* If edge is local */
      vertlocend -= vertlocbas;
    }
    else { /* If edge is not local */
      Gnum sortmax;

      for (sortnum = 0, sortmax = *vertovpnbr;
          sortmax - sortnum > 1; ) {
        Gnum                 sortmed;

        sortmed = (sortmax + sortnum) / 2;
        if (sorttab[2 * sortmed] <= vertlocend)
          sortnum = sortmed;
        else
          sortmax = sortmed;
      }

      if ((sorttab[2 * sortnum] != vertlocend) || (!(ovlpglbval & PAMPA_OVLP_ELEM) && (sorttab[2 * sortnum + 1] == ENTT_MAIN_INV))) { // XXX à modifier pour les élément halo ???
        (*edgeovpnbr) --;
        continue;
      }

      vertlocend = sortnum + vertovpbas;
    }

    (*edgegsttax)[edgeovpnum ++] = vertlocend;

  }
  if (*vertovpnbr > 0)
    (*vendovptax)[vertovpnum] = edgeovpnum;







  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    Gnum edgelocnum;

    for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {
      vertlocend = edgeloctax[edgelocnum];
      if ((vertlocend >= vertlocmin) && (vertlocend < vertlocmax)) { /* If edge is local */
        vertlocend -= vertlocbas;
      }
      else {
        Gnum sortmax;

        for (sortnum = 0, sortmax = *vertovpnbr;
            sortmax - sortnum > 1; ) {
          Gnum                 sortmed;

          sortmed = (sortmax + sortnum) / 2;
          if (sorttab[2 * sortmed] <= vertlocend)
            sortnum = sortmed;
          else
            sortmax = sortmed;
        }

#ifdef PAMPA_DEBUG_DMESH
		if (vertlocend != sorttab[2 * sortnum]) {
		  errorPrint ("Distant neighbor is unknown");
    	  cheklocval = 1;
		}
  		CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

        vertlocend = sortnum + vertovpbas;
      }

      (*edgegsttax)[edgelocnum] = vertlocend;
    }
  }
  // 8)
  // inutile ici
  memFree (sorttab);

  CHECK_FERR (dmeshGhst3(meshptr, vertnumtax), meshptr->proccomm);

  memFree (esndtab);
  memFree (ercvtab);
  memFreeGroup (vertsidtab);

  meshptr->flagval   |= DMESHFREEPSID; /* Mesh now have valid ghost private data */

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

//static
int
dmeshOvlpProcVertResize (
	DmeshOvlpProcVert * restrict * hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_DMESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    Gnum vertlocnum;
    Gnum procngbnum;

    vertlocnum = (*hashtabptr)[hashidx].vertnum;
    procngbnum = (*hashtabptr)[hashidx].procnum;

    if (vertlocnum == ~0) // If empty slot
      continue;

  	for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].vertnum == vertlocnum) && ((*hashtabptr)[hashnum].procnum == procngbnum)) {
      	errorPrint ("vertex and processor numbers are already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
		return (1);
	  }
  }
#endif /* PAMPA_DEBUG_DMESH */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (DmeshOvlpProcVert *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (DmeshOvlpProcVert)))) == NULL) {
    errorPrint ("Out of memory");
    return (1);
  }

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (DmeshOvlpProcVert)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].vertnum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum vertlocnum;
    Gnum procngbnum;

    vertlocnum = (*hashtabptr)[hashidx].vertnum;
    procngbnum = (*hashtabptr)[hashidx].procnum;

    if (vertlocnum == ~0) // If empty slot
      continue;

    for (hashnum = (vertlocnum * DMESHOVLPHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].vertnum != ~0 && ((*hashtabptr)[hashnum].vertnum != vertlocnum || (*hashtabptr)[hashnum].procnum != procngbnum); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].vertnum = ~0;
    (*hashtabptr)[hashidx].procnum = ~0;
  }

#ifdef PAMPA_DEBUG_DMESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    Gnum vertlocnum;
    Gnum procngbnum;

    vertlocnum = (*hashtabptr)[hashidx].vertnum;
    procngbnum = (*hashtabptr)[hashidx].procnum;

    if (vertlocnum == ~0) // If empty slot
      continue;

  	for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].vertnum == vertlocnum) && ((*hashtabptr)[hashnum].procnum == procngbnum)) {
      	errorPrint ("vertex and processor numbers are already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
		return (1);
	  }
  }
#endif /* PAMPA_DEBUG_DMESH */
  return (0);
}

