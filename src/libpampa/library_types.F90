!*  Copyright 2009-2016 Inria
!*
!* This file is part of the PaMPA software package for parallel
!* mesh partitioning and adaptation.
!*
!* PaMPA is free software: you can redistribute it and/or modify
!* it under the terms of the GNU General Public License as published by
!* the Free Software Foundation, either version 3 of the License, or
!* any later version.
!* 
!* PaMPA is distributed in the hope that it will be useful,
!* but WITHOUT ANY WARRANTY; without even the implied warranty of
!* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!* GNU General Public License for more details.
!* 
!* In this respect, the user's attention is drawn to the risks associated
!* with loading, using, modifying and/or developing or reproducing the
!* software by the user in light of its specific status of free software,
!* that may mean that it is complicated to manipulate, and that also
!* therefore means that it is reserved for developers and experienced
!* professionals having in-depth computer knowledge. Users are therefore
!* encouraged to load and test the software's suitability as regards
!* their requirements in conditions enabling the security of their
!* systems and/or data to be ensured and, more generally, to use and
!* operate it in the same conditions as regards security.
!* 
!* The fact that you are presently reading this means that you have had
!* knowledge of the GPLv3 license and that you accept its terms.
!*
!************************************************************
!>
!>    \file        library_types.F90
!>
!>    \authors     Cedric Lachat
!>
!>    \brief       This module is the API for the libPampa
!>                 library.
!>
!>    \date        Version 1.0: from: 30 Sep 2016
!>                              to:   30 Sep 2016
!>                 Version 2.0: from:  7 Oct 2016
!>                              to:   12 May 2017
!>
!************************************************************


#include "commonf.h"
#include "iterf.h"
#define PAMPAF_BUILD_ITER
#ifdef PAMPAF_DEBUG_ITER
#undef PAMPAF_DEBUG_ITER
#include "libraryf.h"
#define PAMPAF_DEBUG_ITER
#else /* PAMPAF_DEBUG_ITER */
#include "libraryf.h"
#endif /* PAMPAF_DEBUG_ITER */
#undef PAMPAF_BUILD_ITER

MODULE LIBRARY_TYPES

USE ISO_C_BINDING

IMPLICIT NONE

INCLUDE "pampa_commonf.h"

!> \brief This type is used by routines and macros on iterators
TYPE :: PAMPAF_Iterator
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: VERTNND            ! vertnnd
  INTEGER(KIND=PAMPA_C_INT)                       :: NENTMSK = 1       ! nentmsk
  INTEGER(KIND=PAMPA_C_INT)                       :: NESBNUM            ! nesbnum 
  INTEGER(KIND=PAMPA_C_INT)                       :: NENTNUM         ! nentnum 
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: NENTBAS           ! nentbas
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: EDGETAB           ! edgetab
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: SVRTTAB           ! svrttab
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: MNGBTAB           ! mngbtab
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: SNGBTAB           ! sngbtab
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: VINDTAB           ! vindtab
  INTEGER(KIND=PAMPA_C_INT)                       :: VENTNUM            ! ventnum 
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: NENTTAB           ! nenttab
  INTEGER(KIND=PAMPA_C_INT), DIMENSION(:),POINTER :: VERTNUM            ! vertnum
#ifdef PAMPAF_DEBUG_ITER
  INTEGER(KIND=PAMPA_C_INT)                       :: VINDVAL           ! vindval
#endif /* PAMPAF_DEBUG_ITER */
END TYPE PAMPAF_iterator

TYPE :: PAMPAF_Dmesh
DOUBLE PRECISION :: dummy(PAMPAF_DMESHDIM)
END TYPE PAMPAF_Dmesh

TYPE :: PAMPAF_DmeshHaloReqs
DOUBLE PRECISION :: dummy(PAMPAF_DMESHHALOREQSDIM)
END TYPE PAMPAF_DmeshHaloReqs

TYPE :: PAMPAF_Mesh
DOUBLE PRECISION :: dummy(PAMPAF_MESHDIM)
END TYPE PAMPAF_Mesh

TYPE :: PAMPAF_Strat
DOUBLE PRECISION :: dummy(PAMPAF_STRATDIM)
END TYPE PAMPAF_Strat

END MODULE
