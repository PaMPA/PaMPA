
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine accesses mesh data from a mdmesh.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_mdmeshDmeshData (
const PAMPA_Mdmesh * const mdmhptr,              //!< Multi-distributed mesh structure to read
PAMPA_Num      const       mdmhglbnum,           //!< Level number
PAMPA_Dmesh ** const       meshptr)              //!< Pointer on the distributed mesh
{
  int cheklocval = 0;

#ifdef PAMPA_DEBUG_MDMESH
  if (((Mdmesh *) mdmhptr)->dmshglbnxt < mdmhglbnum) {
    errorPrint ("No corresponding level");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, ((Mdmesh *) mdmhptr)->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

  *meshptr = (PAMPA_Dmesh *) (((Mdmesh *) mdmhptr)->dmshglbtax + mdmhglbnum);

  return (0);
}
