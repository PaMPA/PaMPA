
/*
 ** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
 */

#define MDMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_overlap.h"
#include "dmesh_redist.h"
#include "dsmesh.h"
#include "dsmesh_dmesh.h"
#include "mdmesh.h"
#include "mdmesh_redist.h"
#include "mdmesh_mesh_levels.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* mesh meshs.                      */
/*                                   */
/*************************************/

int
mdmeshRedistRenum (
    Mdmesh * const       srcmeshptr,                        //!< XXX
    Gnum const           srclevllocnum,
    Gnum const           vertlocnum,
    Gnum const           dstlevllocnum,
    Gnum * const         newpermgsttax,
    Gnum * const         vertlocidx)
{
  DmeshVertBounds * restrict vindloctax;
  Gnum edgelocnum;
  Gnum edgelocnnd;
  Gnum vertlocbas;
  int cheklocval;

  Gnum baseval = srcmeshptr->baseval;
  const Gnum vertlocmin = srcmeshptr->dmshglbtax[baseval].procvrttab[srcmeshptr->dmshglbtax[baseval].proclocnum];
  const Gnum vertlocmax = srcmeshptr->dmshglbtax[baseval].procvrttab[srcmeshptr->dmshglbtax[baseval].proclocnum + 1] - 1;
  Dmesh * restrict meshcurptr = srcmeshptr->dmshglbtax + srclevllocnum - 1;
  Dmesh * restrict mesholdptr = srcmeshptr->dmshglbtax + srclevllocnum;
  cheklocval = 0;
  vindloctax = mesholdptr->enttloctax[baseval]->ngblloctax[baseval]->vindloctax;
  vertlocbas = meshcurptr->procvrttab[meshcurptr->proclocnum] - baseval;

  /* Renumbering aggregated elements */
  // XXX le problème a l'air de venir de newpermgsttax qui n'est pas bon :
  // entre les niveaux 0 et 1 (mdmhnum = 0), newpermgsttax = {4, 1, 0, 2, 5, 3, 6, 8, 10, 7, 9}
  // il correspond à une permutation au sein d'un même niveau
  // on voit que l'élément 9 est renuméroté après l'arête 1 => bas bien !! :)
  // peut-être décomposer la boucle où on est en deux : 
  //  - renuméroter les éléments
  //  - puis pour chaque élément, renuméroter les voisins
  for (edgelocnum = vindloctax[vertlocnum].vertlocidx, edgelocnnd = vindloctax[vertlocnum].vendlocidx;
      edgelocnum < edgelocnnd; edgelocnum ++) {
    Gnum vertlocnm2;
    Gnum bvrtlocnm2;

    vertlocnm2 = mesholdptr->medgloctax[edgelocnum]; // XXX cela fonctionne si les éléments agrégés sont sur le même processeur que le macro élément
    bvrtlocnm2 = meshcurptr->enttloctax[baseval]->mvrtloctax[vertlocnm2] - vertlocbas; // XXX cela fonctionne si les éléments agrégés sont sur le même processeur que le macro élément
    if (srclevllocnum != (dstlevllocnum + 1)) { // XXX est-ce qu'on ne compte pas plusieurs fois la même chose, vu que l'appel à mdmeshRedistRenum (récursif) est fait pour chaque niveau du multigrille ?
      CHECK_FERR (mdmeshRedistRenum (srcmeshptr, srclevllocnum - 1, vertlocnm2, dstlevllocnum, newpermgsttax, vertlocidx), srcmeshptr->proccomm);
    }
    else {
#ifdef PAMPA_DEBUG_MDMESH
      if (newpermgsttax[bvrtlocnm2] != ~0) {
        errorPrint ("internal error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, srcmeshptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */
      newpermgsttax[bvrtlocnm2] = (*vertlocidx) ++;
    }
  }

  if (srclevllocnum == (dstlevllocnum + 1)) {
    Gnum vertlocnum;
    Gnum vertlocnnd;

    for (vertlocnum = baseval, vertlocnnd = mesholdptr->enttloctax[baseval]->vertlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++) {
      Gnum edgelocnum;
      Gnum edgelocnnd;

      for (edgelocnum = vindloctax[vertlocnum].vertlocidx, edgelocnnd = vindloctax[vertlocnum].vendlocidx;
          edgelocnum < edgelocnnd; edgelocnum ++) {
        Gnum vertlocnm2;
        Gnum enttnum;

        vertlocnm2 = mesholdptr->medgloctax[edgelocnum]; // XXX cela fonctionne si les éléments agrégés sont sur le même processeur que le macro élément

        /* Renumbering neighbors of aggregated elements */
        for (enttnum = baseval + 1; enttnum < meshcurptr->enttglbnbr; enttnum ++) {
          Gnum edgelocnm2;
          Gnum edgelocnn2;

          DmeshVertBounds * restrict vindloctx2;

          if ((meshcurptr->enttloctax[enttnum] == NULL) || (meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)) // dummy entity or sub-entity
            continue;

          vindloctx2 = meshcurptr->enttloctax[baseval]->nghbloctax[enttnum]->vindloctax;

          for (edgelocnm2 = vindloctx2[vertlocnm2].vertlocidx, edgelocnn2 = vindloctx2[vertlocnm2].vendlocidx;
              edgelocnm2 < edgelocnn2; edgelocnm2 ++) {
            Gnum nghblocnum;

            nghblocnum = meshcurptr->enttloctax[enttnum]->mvrtloctax[meshcurptr->edgeloctax[edgelocnm2]];
            if ((nghblocnum >= vertlocmin) && (nghblocnum <= vertlocmax)) {
              nghblocnum -=  vertlocbas;
              if (newpermgsttax[nghblocnum] == ~0)
                newpermgsttax[nghblocnum] = (*vertlocidx) ++;
            }
          }
        }
      }
    }
  }

  return (cheklocval);
}

int
mdmeshRedistOvlp (
    Mdmesh * const       srcmeshptr,                        //!< XXX
    Mdmesh * const       dstmeshptr,                        //!< XXX
    Gnum const           dstlevllocnum,
    DmeshOvlpProcVert * * const ovphashtab,
    Gnum * const                ovphashsiz,          /* Size of hash table    */
    Gnum * const                ovphashnbr,
    Gnum * const                ovphashmsk)
{
  Gnum                  vertlocnum;
  Gnum                  vertlocnnd;
  int                   procngbnum;
  Gnum                  baseval;
  int                   cheklocval;
  Gnum * restrict       idenloctax;
  int *  restrict       dsndcnttab;
  int *  restrict       dsnddsptab;
  Gnum * restrict       datasndtab;
  int *  restrict       drcvcnttab;
  int *  restrict       drcvdsptab;
  Gnum * restrict       datarcvtab;
  Gnum                  datasndidx;
  Gnum                  datasndnbr;
  Gnum                  datarcvidx;
  Gnum                  datarcvnnd;
  Gnum                  datarcvnbr;
  MdmeshVertProc * restrict vertproctab;
  PAMPA_Iterator        it;
  DmeshEntity * restrict enttlocptr;
  Gnum                  voldlocbas;
  Gnum                  vertprocidx;
  Gnum                  ovphashmax;
  Gnum                  ovphashnum;


  Dmesh * restrict meshcurptr = dstmeshptr->dmshglbtax + dstlevllocnum + 1;
  Dmesh * restrict mesholdptr = srcmeshptr->dmshglbtax + dstlevllocnum + 1;

  baseval =  meshcurptr->baseval;
  cheklocval = 0;

  CHECK_FERR (dmeshItBuild (meshcurptr, baseval), meshcurptr->proccomm);
  datasndnbr = meshcurptr->enttloctax[baseval]->vovplocnbr - meshcurptr->enttloctax[baseval]->vertlocnbr;

  //* allouer dsndcnttab, drcvcnttab de taille |proc|
  if (memAllocGroup ((void **) (void *)
        &dsndcnttab, (size_t) (meshcurptr->procglbnbr       * sizeof (int)),
        &dsnddsptab, (size_t) ((meshcurptr->procglbnbr + 1) * sizeof (int)),
        &drcvcnttab, (size_t) (meshcurptr->procglbnbr       * sizeof (int)),
        &drcvdsptab, (size_t) ((meshcurptr->procglbnbr + 1) * sizeof (int)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshcurptr->proccomm);

  memSet (dsndcnttab, 0, meshcurptr->procglbnbr * sizeof (int));

  if ((datasndtab = (Gnum *) memAlloc (datasndnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  CHECK_FERR (dmeshValueData (meshcurptr, baseval, -20, NULL, NULL, (void **) &idenloctax), dstmeshptr->proccomm); // XXX remplacer -20 par une macro définie
  idenloctax -= baseval;

  CHECK_FERR (dmeshHaloSync (meshcurptr, meshcurptr->enttloctax[baseval], idenloctax + baseval, GNUM_MPI), dstmeshptr->proccomm);
  for (datasndidx = procngbnum = 0, vertlocnum = meshcurptr->enttloctax[baseval]->vfrnlocmin,
      vertlocnnd = meshcurptr->enttloctax[baseval]->vovplocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++) {
    Gnum mnewlocnum;
    Gnum moldlocnum;

    mnewlocnum = meshcurptr->enttloctax[baseval]->mvrtloctax[vertlocnum];
    moldlocnum = idenloctax[vertlocnum];

    if (!((moldlocnum >= mesholdptr->procvrttab[procngbnum]) && (moldlocnum < mesholdptr->procvrttab[procngbnum + 1]))) {
      int procngbmax;
      for (procngbnum = 0, procngbmax = mesholdptr->procglbnbr;
          procngbmax - procngbnum > 1; ) {
        int                 procngbmed;

        procngbmed = (procngbmax + procngbnum) / 2;
        if (mesholdptr->procvrttab[procngbmed] <= moldlocnum)
          procngbnum = procngbmed;
        else
          procngbmax = procngbmed;
      }
    }

    dsndcnttab[procngbnum] ++;
  }

  CHECK_VDBG (cheklocval, dstmeshptr->proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, dstmeshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  // Computing displacement arrays
  dsnddsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshcurptr->procglbnbr ; procngbnum ++) {
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  dsndcnttab[0] = dsnddsptab[0];

  for (datasndidx = procngbnum = 0, vertlocnum = meshcurptr->enttloctax[baseval]->vfrnlocmin,
      vertlocnnd = meshcurptr->enttloctax[baseval]->vovplocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++) {
    Gnum mnewlocnum;
    Gnum moldlocnum;

    mnewlocnum = meshcurptr->enttloctax[baseval]->mvrtloctax[vertlocnum];
    moldlocnum = idenloctax[vertlocnum];

    if (!((moldlocnum >= mesholdptr->procvrttab[procngbnum]) && (moldlocnum < mesholdptr->procvrttab[procngbnum + 1]))) {
      int procngbmax;
      for (procngbnum = 0, procngbmax = mesholdptr->procglbnbr;
          procngbmax - procngbnum > 1; ) {
        int                 procngbmed;

        procngbmed = (procngbmax + procngbnum) / 2;
        if (mesholdptr->procvrttab[procngbmed] <= moldlocnum)
          procngbnum = procngbmed;
        else
          procngbmax = procngbmed;
      }
    }

    datasndtab[dsndcnttab[procngbnum] ++] = mnewlocnum;
  }

  for (procngbnum = 0 ; procngbnum < meshcurptr->procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
  }

  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshcurptr->procglbnbr + 1 ; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
  }
  datarcvnbr = drcvdsptab[meshcurptr->procglbnbr];

  if ((datarcvtab = (Gnum *) memAlloc (datarcvnbr * 2 * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  CHECK_VDBG (cheklocval, dstmeshptr->proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, dstmeshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  for (procngbnum = meshcurptr->procglbnbr - 1; procngbnum >= 0; procngbnum --)
    for (datarcvidx = drcvdsptab[procngbnum + 1] - 1, datarcvnnd = drcvdsptab[procngbnum] - 1;
        datarcvidx > datarcvnnd; datarcvidx --) {
      datarcvtab[datarcvidx << 1] = datarcvtab[datarcvidx];
      datarcvtab[(datarcvidx << 1) + 1] = procngbnum;
    }

  vertproctab = (MdmeshVertProc *) datarcvtab;

  /* Sort received vertex numbers with their corresponding processor numbers */
  intSort2asc1 (vertproctab, datarcvnbr);

  PAMPA_mdmeshItInit((PAMPA_Mdmesh *) srcmeshptr, dstlevllocnum + 1, baseval, dstlevllocnum, baseval, &it) ;
  enttlocptr = meshcurptr->enttloctax[baseval];
  voldlocbas = srcmeshptr->dmshglbtax[dstlevllocnum + 1].procvrttab[meshcurptr->proclocnum] - baseval;
  
  *ovphashnbr = MAX ((enttlocptr->vovplocnbr - enttlocptr->vertlocnbr) / 10, 10); // XXX N'est-ce pas trop grand pour ovphashnbr ?
  for (*ovphashsiz = 256; *ovphashsiz < *ovphashnbr; *ovphashsiz <<= 1) ; /* Get upper power of two */
  *ovphashnbr = 0;

  *ovphashmsk = (*ovphashsiz) - 1;
  ovphashmax = (*ovphashsiz) >> 2;

  if (((*ovphashtab) = (DmeshOvlpProcVert *) memAlloc ((*ovphashsiz) * sizeof (DmeshOvlpProcVert))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshcurptr->proccomm);

  memSet (*ovphashtab, ~0, (*ovphashsiz) * sizeof (DmeshOvlpProcVert));

  for (vertprocidx = 0, vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr; vertlocnum < vertlocnnd && vertprocidx < datarcvnbr; vertlocnum ++) {
    Gnum mnewlocnum;

    mnewlocnum = enttlocptr->mvrtloctax[vertlocnum];

    if (mnewlocnum < vertproctab[vertprocidx].vertnum)
      continue;
    
    for (; vertprocidx < datarcvnbr && vertproctab[vertprocidx].vertnum == mnewlocnum; vertprocidx ++) {
      Gnum vertnum;
      Gnum moldlocnum;

      moldlocnum = idenloctax[vertlocnum];
      vertnum = moldlocnum - voldlocbas;

      procngbnum = (int) vertproctab[vertprocidx].procnum;


      PAMPA_itStart(&it, vertnum) ;

      while (PAMPA_itHasMore(&it)) {
        Gnum mcurlocnum;



        mcurlocnum = PAMPA_itCurMeshVertNum(&it);

        for (ovphashnum = (mcurlocnum * DMESHOVLPHASHPRIME) & (*ovphashmsk);
            (*ovphashtab)[ovphashnum].vertnum != ~0
            && ((*ovphashtab)[ovphashnum].vertnum != mcurlocnum || ((int) (*ovphashtab)[ovphashnum].procnum) != procngbnum);
            ovphashnum = (ovphashnum + 1) & (*ovphashmsk)) ;

#ifdef PAMPA_DEBUG_MDMESH
        if (((*ovphashtab)[ovphashnum].vertnum == mcurlocnum) && (((int) (*ovphashtab)[ovphashnum].procnum) == procngbnum)) { /* Vertex already added */
          errorPrint ("internal error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dstmeshptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

        (*ovphashtab)[ovphashnum].vertnum = mcurlocnum;
        (*ovphashtab)[ovphashnum].procnum = (Gnum) procngbnum;
        //ovphashtab[ovphashnum].flagval = FLAG_OVLP;
        //ovphashtab[ovphashnum].ovlpval = 1; // XXX n'est pas utilisé dans dmeshOverlap2 
        (*ovphashnbr) ++;

        if (*ovphashnbr >= ovphashmax)  /* If ovphashtab is too much filled */
          CHECK_FERR (dmeshOvlpProcVertResize (ovphashtab, ovphashsiz, &ovphashmax, ovphashmsk), dstmeshptr->proccomm);
        PAMPA_itNext(&it);
      }
    }
  }

  memSet (dsndcnttab, 0, meshcurptr->procglbnbr * sizeof (int));

  /* Count number of vertices to be sent for each processor */
  for (datasndidx = ovphashnum = 0; ovphashnum < *ovphashsiz; ovphashnum ++)
    if ((*ovphashtab)[ovphashnum].vertnum != ~0)
      dsndcnttab[(*ovphashtab)[ovphashnum].procnum] ++;

  // Computing displacement arrays
  dsnddsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshcurptr->procglbnbr + 1 ; procngbnum ++) {
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  }
  datasndnbr = dsnddsptab[meshcurptr->procglbnbr];

  if ((datasndtab = (Gnum *) memRealloc (datasndtab, datasndnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  /* Fill datasndtab with the elements needed in the overlap. Processor numbers
   * are not needed (it will be the sender) */
  for (ovphashnum = 0; ovphashnum < *ovphashsiz; ovphashnum ++)
    if ((*ovphashtab)[ovphashnum].vertnum != ~0)
      datasndtab[dsnddsptab[(*ovphashtab)[ovphashnum].procnum] ++] = (*ovphashtab)[ovphashnum].vertnum;

  for (procngbnum = 0 ; procngbnum < meshcurptr->procglbnbr ; procngbnum ++) 
    dsnddsptab[procngbnum] -= dsndcnttab[procngbnum];

  CHECK_VDBG (cheklocval, dstmeshptr->proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, dstmeshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  // Computing displacement arrays
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshcurptr->procglbnbr + 1 ; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
  }
  datarcvnbr = drcvdsptab[meshcurptr->procglbnbr];

  if ((datarcvtab = (Gnum *) memRealloc (datarcvtab, datarcvnbr * 2 * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  CHECK_VDBG (cheklocval, dstmeshptr->proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, dstmeshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dstmeshptr->proccomm);

  memFree (datasndtab);

  /* Add received vertices into overlap, processor numbers are the senders */
  for (procngbnum = 0; procngbnum < meshcurptr->procglbnbr; procngbnum ++) {
    for (datarcvidx = drcvdsptab[procngbnum], datarcvnnd = drcvdsptab[procngbnum + 1];
        datarcvidx < datarcvnnd; datarcvidx ++) {
      Gnum              mcurlocnum;

      mcurlocnum = datarcvtab[datarcvidx];

      for (ovphashnum = (mcurlocnum * DMESHOVLPHASHPRIME) & (*ovphashmsk);
          (*ovphashtab)[ovphashnum].vertnum != ~0
          && ((*ovphashtab)[ovphashnum].vertnum != mcurlocnum || ((int) (*ovphashtab)[ovphashnum].procnum) != procngbnum);
          ovphashnum = (ovphashnum + 1) & (*ovphashmsk)) ;

#ifdef PAMPA_DEBUG_MDMESH
      if (((*ovphashtab)[ovphashnum].vertnum == mcurlocnum) && (((int) (*ovphashtab)[ovphashnum].procnum) == procngbnum)) { /* Vertex already added */
        errorPrint ("internal error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dstmeshptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

      (*ovphashtab)[ovphashnum].vertnum = mcurlocnum;
      (*ovphashtab)[ovphashnum].procnum = (Gnum) procngbnum;
      //(*ovphashtab)[ovphashnum].flagval = FLAG_OVLP;
      //(*ovphashtab)[ovphashnum].ovlpval = 1; // XXX n'est pas utilisé dans dmeshOverlap2 
      (*ovphashnbr) ++;

      if (*ovphashnbr >= ovphashmax)  /* If ovphashtab is too much filled */
        CHECK_FERR (dmeshOvlpProcVertResize (ovphashtab, ovphashsiz, &ovphashmax, ovphashmsk), dstmeshptr->proccomm);
    }
  }
      
  memFreeGroup (dsndcnttab);
  memFree (datarcvtab);
  return (cheklocval);
}


    

//! XXX Pré-requis un sommet ne peut pas être seul (sans un élément) sur un proc, sinon il faut refaire mdmeshRedistRenum
//! C'est pour cela qu'il y a un test après l'appel à cette fonction avec un
//! «internal error»
//! This routine XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
mdmeshRedist (
    Mdmesh * const       srcmeshptr,                        //!< XXX
    Gnum * const         partloctax,                        //!< XXX
    Gnum   const         vertlocdlt,                        //!< XXX
    Gnum   const         edgelocdlt,                        //!< XXX
    Gnum const           ovlpglbval,
    Mdmesh * const       dstmeshptr)                        //!< XXX
{
  Dsmesh dsmshdat; // XXX en attendant d'avoir implanté dmeshCopy
  int    cheklocval = 0;
  Gnum baseval;
  Gnum mdmhnum;
  Gnum mdmhnnd;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum vertlocmx2;
  Gnum vgstlocmx2;
  Gnum * restrict srcpartloctax; // XXX tableau temporaire, à terme ce sera directement partloctax
  Gnum * restrict oldpermgsttax;
  Gnum * restrict newpermgsttax;
  Gnum * restrict vnumgsttax;
  Gnum * restrict ventloctax;
  Gnum * restrict enttglbtax;
  DmeshOvlpProcVert * ovphashtab;
  Gnum                ovphashsiz;
  Gnum                ovphashnbr;
  Gnum                ovphashmsk;

  baseval = srcmeshptr->baseval;

#ifdef PAMPA_DEBUG_MDMESH
  {
    if (srcmeshptr->dmshglbnxt <= srcmeshptr->baseval) {
      errorPrint ("No level in source multigrid");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, srcmeshptr->proccomm);
  }
#endif /* PAMPA_DEBUG_MDMESH */

 // XXX est-ce que les deux lignes suivantes sont toujours vraies suivant les maillages ? Il faudrait que cela soit clairement dit !!
  const Gnum vertlocmin = srcmeshptr->dmshglbtax[baseval].procvrttab[srcmeshptr->dmshglbtax[baseval].proclocnum];
  const Gnum vertlocmax = srcmeshptr->dmshglbtax[baseval].procvrttab[srcmeshptr->dmshglbtax[baseval].proclocnum + 1] - 1;
  vertlocmx2 = MAX(srcmeshptr->dmshglbtax[baseval].vertlocmax, srcmeshptr->dmshglbtax[baseval].vertlocnbr) + baseval; // temporaire tant que vtrulocnbr existe, sinon vertlocmax uniquement
  // XXX est-ce que le test suivant est intéressant, puisqu'il ne sert pas si
  // baseval = 0
#ifdef PAMPA_DEBUG_MDMESH
  if (srcmeshptr->dmshglbnbr != dstmeshptr->dmshglbnbr) {
    errorPrint ("Destination distributed multigrid has not the same number of levels");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

  mdmhnum = srcmeshptr->dmshglbnxt - 1;
  dstmeshptr->flagval |= MDMESHFREEDMESH;

  /* Copy last level of source mesh in destination mesh */
  dsmeshInit (&dsmshdat, srcmeshptr->dmshglbtax[mdmhnum].proccomm);
  dmesh2dsmesh (srcmeshptr->dmshglbtax + mdmhnum, &dsmshdat);
  dsmesh2dmesh (&dsmshdat, dstmeshptr->dmshglbtax + mdmhnum);
  dsmeshExit (&dsmshdat);

  dstmeshptr->dmshglbtax[mdmhnum].flagval |= DMESHFREELVL;
  dstmeshptr->dmshglbnxt = mdmhnum + 1;

  for (vgstlocmx2 = 0, mdmhnnd = baseval - 1; mdmhnum > mdmhnnd; mdmhnum --) {
    Gnum vgstloctmp;
    Dmesh * meshcurptr = srcmeshptr->dmshglbtax + mdmhnum;

    for (vgstloctmp = 0, enttnum = baseval; enttnum < meshcurptr->enttglbnbr; enttnum ++) {
      if ((meshcurptr->enttloctax[enttnum] == NULL) || (meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)) // dummy entity or sub-entity
        continue;

      vgstloctmp += srcmeshptr->dmshglbtax[mdmhnum].enttloctax[enttnum]->vgstlocnbr;
    }

    if (vgstlocmx2 < vgstloctmp)
      vgstlocmx2 = vgstloctmp;
  }
  mdmhnum = srcmeshptr->dmshglbnxt - 1;

  if (memAllocGroup ((void **) (void *)
        &srcpartloctax, (size_t) (vertlocmx2                           * sizeof (Gnum)),
        &newpermgsttax, (size_t) (vgstlocmx2                           * sizeof (Gnum)),
        &oldpermgsttax, (size_t) (vgstlocmx2                           * sizeof (Gnum)), // XXX (vgst ou vert ??)
        &ventloctax,    (size_t) (vertlocmx2                           * sizeof (Gnum)),
        &vnumgsttax,    (size_t) (vertlocmx2                           * sizeof (Gnum)),
        &enttglbtax,    (size_t) (srcmeshptr->dmshglbtax[0].enttglbnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);

  srcpartloctax -= baseval;
  newpermgsttax -= baseval;
  ventloctax    -= baseval;
  vnumgsttax    -= baseval;
  enttglbtax    -= baseval;

  for (vertlocnum = baseval, vertlocnnd = srcmeshptr->dmshglbtax[baseval].vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) { // XXX et si la numérotation est à trou ??
    srcpartloctax[vertlocnum] = dstmeshptr->dmshglbtax[baseval].proclocnum;
    oldpermgsttax[vertlocnum] = vertlocnum; // XXX à changer si repartitionnement sur le niveau le plus grossier
  }

  for (mdmhnnd = baseval - 1, mdmhnum --; mdmhnum > mdmhnnd; mdmhnum --) {
    Gnum vertlocbas;
    Gnum vertlocidx;

    DmeshVertBounds * restrict vindloctax;
    Dmesh * restrict meshcurptr = srcmeshptr->dmshglbtax + mdmhnum;
    Dmesh * restrict mesholdptr = srcmeshptr->dmshglbtax + mdmhnum + 1;
    vertlocbas = meshcurptr->procvrttab[meshcurptr->proclocnum] - baseval;

    memSet (newpermgsttax + baseval, ~0, vertlocmx2 * sizeof (Gnum));

    vindloctax = mesholdptr->enttloctax[baseval]->ngblloctax[baseval]->vindloctax;

    for (vertlocidx = vertlocbas + baseval, vertlocnum = baseval, vertlocnnd = srcmeshptr->dmshglbtax[srcmeshptr->dmshglbnxt - 1].enttloctax[baseval]->vertlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++) {

      CHECK_FERR (mdmeshRedistRenum (srcmeshptr, srcmeshptr->dmshglbnxt - 1, vertlocnum, mdmhnum, newpermgsttax, &vertlocidx), dstmeshptr->proccomm);
    }

#ifdef PAMPA_DEBUG_MDMESH2
    {
      /* Check if all vertices have been numbered */
      for (enttnum = baseval; enttnum < meshcurptr->enttglbnbr; enttnum ++) {
        if ((meshcurptr->enttloctax[enttnum] == NULL) || (meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)) // dummy entity or sub-entity
          continue;

        for (vertlocnum = baseval, vertlocnnd = meshcurptr->enttloctax[enttnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
          Gnum bvrtlocnum;

          bvrtlocnum = meshcurptr->enttloctax[enttnum]->mvrtloctax[vertlocnum] - vertlocbas;

          if (newpermgsttax[bvrtlocnum] == ~0) {
            errorPrint ("internal error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, srcmeshptr->proccomm);
        }
      }
      CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
    }
#endif /* PAMPA_DEBUG_MDMESH2 */


    if ((ovlpglbval == 0) || (mdmhnum == srcmeshptr->dmshglbnxt - 2)) {
      Gnum * idenloctax;
      Gnum   vertlocbas;

      if (ovlpglbval != 0) {
        CHECK_FERR (dmeshValueLink (meshcurptr, (void **) &idenloctax, 0, NULL, NULL, GNUM_MPI, baseval, -20), dstmeshptr->proccomm); // XXX remplacer -20 par une macro définie
        idenloctax -= baseval;

        vertlocbas = meshcurptr->procvrttab[meshcurptr->proclocnum] - baseval;
        for (vertlocnum = baseval, vertlocnnd = meshcurptr->enttloctax[baseval]->vertlocnbr + baseval;
            vertlocnum < vertlocnnd; vertlocnum ++)
          idenloctax[vertlocnum] = vertlocnum + vertlocbas;
      }

      CHECK_FERR (dmeshRedist (meshcurptr, srcpartloctax, newpermgsttax, (MAX (meshcurptr->vertlocmax, meshcurptr->vertlocnbr)) - meshcurptr->vtrulocnbr, 0, ovlpglbval, dstmeshptr->dmshglbtax + mdmhnum), dstmeshptr->proccomm); // XXX à changer quand il n'y aura plus vtrulocnbr
    }
    else {
      CHECK_FERR (mdmeshRedistOvlp (srcmeshptr, dstmeshptr, mdmhnum, &ovphashtab, &ovphashsiz, &ovphashnbr, &ovphashmsk), dstmeshptr->proccomm);
      CHECK_FERR (dmeshRedistFull (meshcurptr, srcpartloctax, newpermgsttax, (MAX (meshcurptr->vertlocmax, meshcurptr->vertlocnbr)) - meshcurptr->vtrulocnbr, 0, ovlpglbval, dstmeshptr->dmshglbtax + mdmhnum, ovphashtab, ovphashsiz, ovphashnbr, ovphashmsk), dstmeshptr->proccomm); // XXX à changer quand il n'y aura plus vtrulocnbr
      memFree (ovphashtab);
    }
    memSet (vnumgsttax + baseval, ~0, vertlocmx2 * sizeof (Gnum));
    memSet (ventloctax + baseval, ~0, vertlocmx2 * sizeof (Gnum));
    memSet (enttglbtax + baseval, ~0, srcmeshptr->dmshglbtax[0].enttglbnbr * sizeof (Gnum));
    for (enttnum = baseval; enttnum < meshcurptr->enttglbnbr + baseval; enttnum ++) {
      Gnum enttngb;

      if ((meshcurptr->enttloctax[enttnum] == NULL) || (meshcurptr->enttloctax[enttnum]->ngbuloctax == NULL)) // dummy entity
        continue;

      for (enttngb = baseval; enttngb < meshcurptr->enttglbnbr + baseval; enttngb ++) {

        if ((mesholdptr->enttloctax[enttngb] == NULL) || (mesholdptr->enttloctax[enttngb]->ngblloctax == NULL))
          continue;

        //if (((meshcurptr->enttloctax[enttnum]->ngbuloctax[enttngb] != NULL) || ((meshcurptr->enttloctax[enttnum]->ngblloctax != NULL) && (meshcurptr->enttloctax[enttnum]->ngblloctax[enttngb] != NULL))) && (meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] == meshcurptr->esublocbax[enttngb & meshcurptr->esublocmsk]) && ((meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] < PAMPA_ENTT_SINGLE) || (enttnum == enttngb))) // XXX ce test est très bizarre et pas sur qu'il soit correct
        if (((meshcurptr->enttloctax[enttnum]->ngbuloctax[enttngb] != NULL)
              || 
              (mesholdptr->enttloctax[enttngb]->ngblloctax[enttnum] != NULL))
            && (
              (((meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
               && (meshcurptr->esublocbax[enttngb & meshcurptr->esublocmsk] <= PAMPA_ENTT_SINGLE)))
              ||
              (((meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)
               && (meshcurptr->esublocbax[enttngb & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)))
              ))
          enttglbtax[enttnum] = enttngb;

        if ((meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)
            || (meshcurptr->esublocbax[enttngb & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE)
            || (meshcurptr->enttloctax[enttnum]->ngbuloctax[enttngb] == NULL)) // dummy entity or sub-entities (enttnum and enttngb) or not in level
          continue;

        vindloctax = meshcurptr->enttloctax[enttnum]->ngbuloctax[enttngb]->vindloctax;

        for (vertlocnum = baseval, vertlocnnd = baseval + meshcurptr->enttloctax[enttnum]->vertlocnbr;
            vertlocnum < vertlocnnd; vertlocnum ++) {

          if (vindloctax[vertlocnum].vertlocidx != ~0) {
            Gnum nghblocnum;

            nghblocnum = mesholdptr->enttloctax[enttnum]->mvrtloctax[meshcurptr->medgloctax[vindloctax[vertlocnum].vertlocidx]];
            vnumgsttax[newpermgsttax[meshcurptr->enttloctax[enttnum]->mvrtloctax[vertlocnum] - vertlocbas] - vertlocbas] = oldpermgsttax[nghblocnum - vertlocbas];
            ventloctax[oldpermgsttax[nghblocnum - vertlocbas]] = enttngb;
          }
        }
      }
    }
    CHECK_FERR (mdmeshMeshLevels (dstmeshptr->dmshglbtax + mdmhnum + 1, dstmeshptr->dmshglbtax + mdmhnum, ventloctax, vnumgsttax, enttglbtax), dstmeshptr->proccomm);

    //memCpy (oldpermgsttax + baseval, newpermgsttax + baseval, meshcurptr->vertlocnbr * sizeof (Gnum));
    for (vertlocnum = baseval, vertlocnnd = baseval + meshcurptr->vertlocnbr;
        vertlocnum < vertlocnnd; vertlocnum ++)
      oldpermgsttax[vertlocnum] = newpermgsttax[vertlocnum] - vertlocbas;
  }

  memFreeGroup (srcpartloctax + baseval);
  return (0);
}
