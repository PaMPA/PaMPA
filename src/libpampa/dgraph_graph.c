/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dgraph_graph.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the centralized graph
//!                extracting routines from a distributed
//!                graph. Each centralized graph corresponds
//!                to a subdomain.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   10 Aug 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 **  The defines and includes.
 */

#define DMESH

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"
#include <ptscotch.h>
#include "dmesh_graph.h"

//! \brief This routine initializes a centralized graph.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dgraphGraphInit (
    SCOTCH_Graph *      grafptr)
{
  return SCOTCH_graphInit(grafptr);
}


//! \brief This routine builds a centralized graph. Each subdomain will be a
//! centralized graph.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dgraphGraphBuild (
    SCOTCH_Dgraph * const        dgrfptr,
    MPI_Comm            proccomm,
    SCOTCH_Graph *      grafptr)
{

  Gnum baseval;
  Gnum cheklocval;
  Gnum edgelocnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum vertlocnbr;
  Gnum vertlocmax;
  Gnum edgenbr;
  Gnum * vertloctax;
  Gnum * vendloctax;
  Gnum * veloloctax;
  Gnum * vlblloctax;
  Gnum * edgegsttax;
  Gnum * edloloctax;
  Gnum * verttax;
  Gnum * edgetax;

  cheklocval = 0;
  SCOTCH_dgraphGhst(dgrfptr);
  SCOTCH_dgraphData(dgrfptr, &baseval, NULL, &vertlocnbr, NULL, NULL, &vertloctax,
      &vendloctax, &veloloctax, &vlblloctax, NULL, &edgelocnbr, NULL, NULL,
      &edgegsttax, &edloloctax, NULL); 
  vertlocmax = vertlocnbr + baseval - 1;
  vertloctax -= baseval;
  vendloctax = (vendloctax != NULL) ? vendloctax - baseval : NULL;
  veloloctax = (veloloctax != NULL) ? veloloctax - baseval : NULL;
  vlblloctax = (vlblloctax != NULL) ? vlblloctax - baseval : NULL;
  edgegsttax -= baseval;
  if (edloloctax != NULL) {
    errorPrint ("edloloctax is not yet implemented");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  if (memAllocGroup ((void **) (void *)
		&verttax, (size_t) ((vertlocnbr + 1) * sizeof (Gnum)),
		&edgetax, (size_t) (edgelocnbr       * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  verttax -= baseval;
  edgetax -= baseval;

  /* Adjacency list is computed, only local edges are copied for local vertices */
  for (edgenbr = vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    Gnum edgelocnum;
    Gnum edgelocnnd;

    verttax[vertlocnum] = edgenbr;

    for (edgelocnum = vertloctax[vertlocnum], edgelocnnd = vendloctax[vertlocnum];
        edgelocnum < edgelocnnd ; edgelocnum ++) 
      if (edgegsttax[edgelocnum] <= vertlocmax)
        edgetax[edgenbr ++] = edgegsttax[edgelocnum];
  }
  verttax[vertlocnum] = edgenbr;

  edgenbr -= baseval;

  if (verttax != NULL)
    verttax += baseval;

  cheklocval = SCOTCH_graphBuild(grafptr,
      baseval,
      vertlocnbr,
      verttax, // TRICK: verttax already unbased due to allocation group
      verttax + 1, // TRICK: verttax already unbased due to allocation group
      veloloctax + baseval,
      vlblloctax + baseval,
      edgenbr,
      edgetax + baseval,
      NULL);
  if (cheklocval != 0)
    return cheklocval;

#ifdef PAMPA_DEBUG_DGRAPH_GRAPH
  cheklocval = SCOTCH_graphCheck(grafptr);
#endif /* PAMPA_DEBUG_DGRAPH_GRAPH */

  return (cheklocval);
}

//! \brief This routine frees the public data of the given
//! centralized graph, but not its private data.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dgraphGraphFree (
    SCOTCH_Graph *      grafptr)
{

  Gnum * vertloctab;
  /* Before freeing Graph, allocated memory used to build graph must be freed */
  SCOTCH_graphData(grafptr, NULL, NULL, &vertloctab, NULL, NULL, NULL, NULL, NULL,
      NULL); 

  if (vertloctab != NULL)
    memFreeGroup (vertloctab);

  SCOTCH_graphFree (grafptr);

  return (0);
}

//! \brief This routine destroys a centralized graph structure.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! Private data are always destroyed. If this is not
//! wanted, use dgraphGraphFree() instead.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dgraphGraphExit (
    SCOTCH_Graph *      grafptr)
{

  Gnum * vertloctab;
  /* Before freeing Graph, allocated memory used to build graph must be freed */
  SCOTCH_graphData(grafptr, NULL, NULL, &vertloctab, NULL, NULL, NULL, NULL, NULL,
      NULL); 

  if (vertloctab != NULL)
    memFreeGroup (vertloctab);

  SCOTCH_graphExit(grafptr);

  return (0);
}
