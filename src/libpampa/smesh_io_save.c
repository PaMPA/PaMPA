
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define SMESH_IO_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "smesh.h"
#include <pampa.h>


static inline int valuePrint (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  if (!strcmp (format, "%d\t")) 
    fprintf (stream, format, *((int *) value));
  else if (!strcmp (format, "%.16e\t")) 
    fprintf (stream, "%.16e\t", *((double *) value)); 
    //fprintf (stream, format, *((double *) value));
  else if (!strcmp (format, "%lld\t")) 
    fprintf (stream, format, *((int64_t *) value));
  else {
    errorPrint ("Unrecognized format");
    return (1);
  }

  return (0);
}
    
/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
smeshSaveValues (
Smesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream,               /* One single centralized stream or distributed ones */
Gnum const                 procnum,
Gnum * const               procvrttab)
{
  Values * restrict valsptr;
  Gnum * restrict vnbrtax;
  Gnum * restrict vdsptax;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum vertnum;
  Gnum vertnnd;
  int chekval;

  chekval = 0;
  valsptr = meshptr->valsptr;

  if ((meshptr->esubtax != NULL) || (procnum == ~0) || (procvrttab == NULL)) {
    errorPrint ("Not yet implemented");
    return (1);
  }
  if (memAllocGroup ((void **) (void *)
        &vnbrtax , (size_t) (meshptr->enttnbr * sizeof (Gnum)),
        &vdsptax , (size_t) (meshptr->enttnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  vnbrtax -= meshptr->baseval;
  vdsptax -= meshptr->baseval;

  memSet (vdsptax + meshptr->baseval, 0, meshptr->enttnbr * sizeof (Gnum));
  memSet (vnbrtax + meshptr->baseval, 0, meshptr->enttnbr * sizeof (Gnum));
  for (vertnum = meshptr->baseval, vertnnd = procvrttab[procnum]; vertnum < vertnnd; vertnum ++)
    vdsptax[meshptr->venttax[vertnum]] ++;
  for (vertnnd = procvrttab[procnum + 1]; vertnum < vertnnd; vertnum ++)
    vnbrtax[meshptr->venttax[vertnum]] ++;

  for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = meshptr->enttnbr + meshptr->baseval; enttnum < enttnnd; enttnum ++) {
    Value * restrict valuptr;
    Gnum valunbr;

    // XXX modifier la partie suivante (et pour le dmesh) :
    // - ne mettre que les entités pour lesquelles il y a des valeurs
    // - ne pas compter le nb de valeurs, mais le mettre à la fin avec ftell et fseek
    for (valunbr = 0, valuptr = valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next, valunbr ++);
    fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", enttnum, valunbr);

    for (valuptr = valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next) {
      int addnbr, intnbr, typenbr, combdat;
      MPI_Aint            valusiz;                 /* Extent of attribute datatype */
      MPI_Aint            dummy;
      Gnum countnbr;
      Gnum countnum;
      Gnum proclocnum;
      Gnum vertnum;
      Gnum vertnnd;
      Gnum flagval;
      size_t elemsiz;
      char format[9];
      int intdat;
      byte * valutab;
      MPI_Datatype typedat;

      valutab = (byte *) valuptr->valutab;
      MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */
      MPI_Type_get_envelope( valuptr->typeval, &intnbr, &addnbr, &typenbr, &combdat);

      fprintf (stream, GNUMSTRING "\t", valuptr->tagnum);
      flagval = 0; // XXX temporaire
      intSave (stream, flagval);
      putc ('\t', stream);


      switch (combdat) {
        case MPI_COMBINER_NAMED:
          countnbr = 1;
          fprintf (stream, "1\t");
          if (valuptr->typeval == MPI_INT) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 1);
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (valuptr->typeval == MPI_DOUBLE) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 2);
            strcpy (format, "%.16e\t");
            elemsiz = sizeof (double);
          }
          else if (valuptr->typeval == MPI_LONG_LONG) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 3);
            strcpy (format, "%lld\t");
            elemsiz = sizeof (long long int);
          }
          else {
            errorPrint ("Type not implemented");
            chekval = 1;
          }
          break;
        case MPI_COMBINER_CONTIGUOUS:
          fprintf (stream, "2\t");
          MPI_Type_get_contents( valuptr->typeval, 1, 0, 1, &intdat, NULL, &typedat); 
          countnbr = (Gnum) intdat;
          if (typedat == MPI_INT) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 1);
            strcpy (format, "%d\t");
            elemsiz = sizeof (int);
          }
          else if (typedat == MPI_DOUBLE) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 2);
            strcpy (format, "%.16e\t");
            elemsiz = sizeof (double);
          }
          else if (valuptr->typeval == MPI_LONG_LONG) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 3);
            strcpy (format, "%lld\t");
            elemsiz = sizeof (long long int);
          }
          else {
            errorPrint ("Type not implemented");
            chekval = 1;
          }
          break;
        default:
          errorPrint ("case not implemented");
          chekval = 1;
      }
      CHECK_VDBG2 (chekval);

      switch (enttnum) {
        case PAMPA_ENTT_VIRT_PROC:
          for (proclocnum = 0; proclocnum < meshptr->procglbnbr; proclocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valutab + proclocnum * valusiz + countnum * elemsiz);
          fprintf (stream, "\n");
          break;
        case PAMPA_ENTT_VIRT_VERT:
          for (vertnum = procvrttab[procnum], vertnnd = procvrttab[procnum + 1];
              vertnum < vertnnd; vertnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valutab + vertnum * valusiz + countnum * elemsiz);
          fprintf (stream, "\n");
          break;
        default: /* real entity */

          for (vertnum = vdsptax[enttnum] + meshptr->baseval,
              vertnnd = vdsptax[enttnum] + vnbrtax[enttnum] + meshptr->baseval;
              vertnum < vertnnd; vertnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valutab + vertnum * valusiz + countnum * elemsiz);
          fprintf (stream, "\n");
      }
    }
  }
  CHECK_VDBG2 (chekval);
  return (0);
}

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
smeshSaveByName (
Smesh * restrict const     meshptr,              /* Not const since halo may update structure         */
char * const               nameval,
Gnum const                 procnbr,
Gnum * const               procvrttab)
{
  PAMPA_Num           procnum;
  char *              nametmp;

  for (procnum = 0; procnum < procnbr; procnum ++) {
    FILE *              ostream;
    File                filedat;

    ostream = NULL;
    nametmp = nameval;
    filedat.name = nametmp;
    filedat.mode = "w+";
    filedat.pntr = stdout;
    if (fileBlockOpenDist (&filedat, 1, procnbr, procnum, -1) == 0) {
      ostream = fileBlockFile (&filedat, 0);
      //memFree (nametmp);                            /* Expanded name no longer needed anyway */
    }
    if (ostream == NULL) {
      errorPrint ("cannot open file");
      return     (1);
    }
    CHECK_FDBG2 (smeshSave (meshptr, ostream, procnum, procnbr, procvrttab));
    fileBlockClose (&filedat, 1);
  }

  return (0);
}

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
smeshSave (
Smesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream,               /* One single centralized stream or distributed ones */
Gnum const                 procnum,
Gnum const                 procnbr,
Gnum * const               procvrttab)
{
  Values * restrict valsptr;
  Gnum vertnum;
  Gnum vertnnd;
  Gnum edgenum;
  Gnum edgennd;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum edgelocnbr;
  Gnum ovlpval;
  int chekval;

  chekval = 0;
  valsptr = meshptr->valsptr;

  if ((procnum == ~0) || (procnbr == ~0) || (procvrttab == NULL)) {
    errorPrint ("Not yet implemented");
    return     (1);
  }
  // XXX et proccomm ? que fait-on ?
  ovlpval = 0; // XXX à changer
  /* Compute edgelocnbr, rewind is not possible with file compress on the fly */
  for (edgelocnbr = 0, vertnum = procvrttab[procnum], vertnnd = procvrttab[procnum + 1];
		  vertnum < vertnnd; vertnum ++) 
	  edgelocnbr += meshptr->vendtax[vertnum] - meshptr->verttax[vertnum];

  if (fprintf(stream,
        "%d\t%d\t%d\n" /* version, parallel, PAMPA_Num size  */
        "%d\t%d\n" /* proclocnum, procglbnbr */
        PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* enttnbr, vertnbr, edgenbr */
        PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* vertnbr, edgenbr */
        PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* valunbr, valumax */
        PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n", /* baseval, ovlpval, esubtab, flagval    */
        0, /* Version */
        2, /* Parallel XXX à changer */
        1, /* PAMPA_Numval XXX à changer */
        (int) procnum, (int) procnbr,
        meshptr->enttnbr, meshptr->vertnbr, meshptr->edgenbr, /* enttnbr, vertnbr, edgenbr */
        (procvrttab[procnum + 1] - procvrttab[procnum]),
        edgelocnbr,
        meshptr->valsptr->valunbr, meshptr->valsptr->valumax,
        meshptr->baseval, ovlpval, (PAMPA_Num) (meshptr->esubtax == NULL ? 0 : 1), (PAMPA_Num) (meshptr->edlotax == NULL ? 0 : 1)) == EOF) {
    errorPrint ("bad output");
    return     (1);
  }

  if (meshptr->esubtax != NULL) {
    for (enttnum = meshptr->baseval, enttnnd = meshptr->baseval + meshptr->enttnbr; enttnum < enttnnd; enttnum ++)
      chekval |= (fprintf (stream, GNUMSTRING "\t", meshptr->esubtax[enttnum]) == EOF);
    chekval |= (fprintf (stream, "\n") == EOF);
  }

  for (edgelocnbr = 0, vertnum = procvrttab[procnum], vertnnd = procvrttab[procnum + 1];
      vertnum < vertnnd; vertnum ++) {
    fprintf (stream, GNUMSTRING "\t" GNUMSTRING, meshptr->venttax[vertnum], meshptr->vendtax[vertnum] - meshptr->verttax[vertnum]);
    for (edgenum = meshptr->verttax[vertnum], edgennd = meshptr->vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
      if (meshptr->edlotax != NULL)
        fprintf (stream, "\t" GNUMSTRING, meshptr->edlotax[edgenum]);
      fprintf (stream, "\t" GNUMSTRING, meshptr->edgetax[edgenum]);
      edgelocnbr ++;
    }
    fprintf (stream, "\n");
  }
  CHECK_VDBG2 (chekval);


  chekval |= smeshSaveValues (meshptr, stream, procnum, procvrttab);
  CHECK_VDBG2 (chekval);

  //if (fseek(stream, 0L, SEEK_SET) != 0) {
  //        errorPrint ("rewind impossible");
  //        return     (1);
  //}

  //if (fprintf(stream,
  //      "%d\t%d\t%d\n" /* version, parallel, PAMPA_Num size  */
  //      "%d\t%d\n" /* proclocnum, procglbnbr */
  //      PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* enttglbnbr, vertglbnbr, edgeglbnbr */
  //      PAMPA_NUMSTRING "\t%015d\n",  /* vertlocnbr, edgelocnbr */
  //      0, /* Version */
  //      2, /* Parallel XXX à changer */
  //      1, /* PAMPA_Numval XXX à changer */
  //      (int) procnum, (int) procnbr,
  //      meshptr->enttnbr, meshptr->vertnbr, meshptr->edgenbr, /* enttnbr, vertnbr, edgenbr */
  //      (procvrttab[procnum + 1] - procvrttab[procnum]),
  //      (long long) edgelocnbr) == EOF) { /* Now we know the exact number of edges */
  //  errorPrint ("bad output");
  //  return     (1);
  //}
  return (chekval);
}


