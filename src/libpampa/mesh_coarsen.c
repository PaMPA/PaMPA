/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_coarsen.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the source mesh
//!                coarsening functions.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"
#include "mesh_coarsen.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* mesh meshs.                      */
/*                                   */
/*************************************/

//! XXX
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshCoarsen2 (
    Mesh * restrict const       meshptr,              //!< Mesh structure
    Gnum const                  venttag,              //!< Tag used to compute vertex load
    Gnum const                  nentnum,
    Gnum * restrict const       coartab,
    Gnum * const                coarnbr)
{
    Gnum * restrict ellotax; /* Element load array */
    Gnum * restrict elemtab;
    MeshVertLoad * restrict velotax; /* (Vertex load, vertex number) array */
    Gnum                    baseval;
    Gnum                    elemnbr;
    Gnum                    vertidx;
    Gnum                    vertnum;
    Gnum                    vertnnd;
    PAMPA_Iterator it_nghb;
    PAMPA_Iterator it_vert;

    Gnum                    nvrtnbr; /* Number of vertex in nentnum */

    baseval = meshptr->baseval;
    elemnbr = meshptr->entttax[baseval]->vertnbr;
    nvrtnbr = meshptr->entttax[nentnum]->vertnbr;

    // algo de contraction de maillage : 1) identification des éléments à contracter
    //      * en entrée : maillage centralisé
    //                    entité « nent » par laquelle va se faire la contraction
    //
    // Avoir un poids en chaque élément
    if (memAllocGroup ((void **) (void *)
                elemtab, (size_t) (elemnbr * sizeof (Gnum)),
                velotax, (size_t) (nvrtnbr * sizeof (MeshVertLoad)),
                (void *) NULL) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
    }
    velotax -= baseval;


    for (vertnum = baseval, vertnnd = baseval + nvrtnbr; vertnum < vertnnd; vertnum ++)
        velotax[vertnum].vertnum = vertnum;

    // Mettre à 0 le poids en chaque sommet « nent » dans un tableau de paires (poids, num sommet)
    memSet (velotax, 0, nvrtnbr * sizeof (MeshVertLoad));
    CHECK_FDBG2 (meshValueData( meshptr, baseval, venttag, NULL, NULL, (void *) &ellotax));
    ellotax -= baseval;

    // Pour chaque élément
    //      ajouter le poids de l'élément à chaque sommet voisin « v », d'entité « nent »
    PAMPA_meshItInitStart((PAMPA_Mesh *) meshptr, baseval, &it_vert) ;
    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, baseval, nentnum, &it_nghb) ;
    while (PAMPA_itHasMore(&it_vert)) {
        Gnum vertnum;

        vertnum = PAMPA_itCurEnttVertNum(&it_vert);
        PAMPA_itStart(&it_nghb, vertnum) ;

        while (PAMPA_itHasMore(&it_nghb)) {
            Gnum nghbnum;

            nghbnum = PAMPA_itCurEnttVertNum(&it_nghb);
            velotax[nghbnum].loadval += ellotax[vertnum];

            PAMPA_itNext(&it_nghb);
        }
        PAMPA_itNext(&it_vert);
    }

    // Trier le cadeau de paires (poids, num sommet) : « sorttab », de poids ascendant
    intSort2asc1 (velotax + baseval, nvrtnbr);

    // Initialiser à ~0 : « elemtab » (de la taille du nombre d'éléments)
    memSet (elemtab, ~0, elemnbr * sizeof (Gnum));

    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, nentnum, baseval, &it_nghb) ;
    // Parcourir le tableau « sorttab »
    for (*coarnbr = 0, vertidx = baseval, vertnnd = baseval + nvrtnbr; vertidx < vertnnd; vertidx ++) {
        Gnum vertflg = 0;
    //      Pour chaque sommet « s » dans « sorttab »
        vertnum = velotax[vertidx].vertnum;

    //          Pour chaque élément voisin « v » de « s »
        PAMPA_itStart(&it_nghb, vertnum) ;

        while ((PAMPA_itHasMore(&it_nghb) && (vertflg == 0)) {
            Gnum elemnum;

            elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
    //              Si elemtab[v] != ~0
    //                  Prendre le sommet suivant
            if (elemtab[elemnum] != ~0)
                vertflg = 1;

            PAMPA_itNext(&it_nghb);
        }

        if (vertflg == 0) {
            coartab[(*coarnbr) ++] = vertnum;
            PAMPA_itStart(&it_nghb, vertnum) ;

            while (PAMPA_itHasMore(&it_nghb)) {
                Gnum elemnum;

                elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
    //          Pour chaque élément voisin « v » de « s »
    //              elemtab[v] = s
                elemtab[elemnum] = vertnum;

                PAMPA_itNext(&it_nghb);
            }
        }
    }

    return (0);
}

//! XXX
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshCoarsen (
    Mesh * restrict const       srcmeshptr,              //!< Mesh structure
    Gnum const                  venttag,              //!< Tag used to compute vertex load
    Gnum const                  nentnum,
    Mesh * restrict             dstmeshptr)
{
    Smesh * restrict smshptr;
    Values * restrict valsptr;
    //Values * restrict nvlsptr;
    Gnum * restrict  vnumtax;
    Gnum * restrict  vinvtax;
    Gnum * restrict  vinvtx2;
    Gnum * restrict  hashtab;
    Gnum * restrict  coartab;
    Gnum             coarnbr;
    Gnum             nvrtnbr;
    Gnum             baseval;
    Gnum             vertnbr;

    baseval = srcmeshptr->baseval;
    vertnbr = srcmeshtpr->vertnbr;

    // algo de contraction de maillage : 2) après identification
    //      * en entrée : maillage centralisé
    //                    entité sur laquelle est fait la contraction
    //
    // num <- baseval
    nvrtnbr = baseval;
    edlosiz = (srcmeshptr->edlotax == NULL) ? 0 : srcmeshptr->edgesiz;

    if (memAllocGroup ((void **) (void *)
                coartab, (size_t) (srcmeshptr->entttax[nentnum]->vertnbr * sizeof (Gnum)),
                vnumtax, (size_t) (vertnbr                               * sizeof (Gnum)),
                vinvtax, (size_t) (vertnbr                               * sizeof (Gnum)),
                vinvtx2, (size_t) (vertnbr                               * sizeof (Gnum)),
                verttax, (size_t) (vertnbr                               * sizeof (Gnum)),
                vendtax, (size_t) (vertnbr                               * sizeof (Gnum)),
                venttax, (size_t) (vertnbr                               * sizeof (Gnum)),
                edgetax, (size_t) (srcmeshptr->edgesiz                   * sizeof (Gnum)),
                edlotax, (size_t) (edlosiz                               * sizeof (Gnum)),
                (void *) NULL) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
    }
    vnumtax -= baseval;
    vinvtax -= baseval;
    vinvtx2 -= baseval;
    verttax -= baseval;
    vendtax -= baseval;
    venttax -= baseval;
    edgetax -= baseval;
    if (edlosiz != 0)
      edlotax -= baseval;

    //memSet (verttax + baseval, ~0, vertnbr * sizeof (Gnum));
    //memSet (vendtax + baseval, ~0, vertnbr * sizeof (Gnum));
    //memSet (venttax + baseval, ~0, vertnbr * sizeof (Gnum));

    CHECK_FDBG2 (meshCoarsen2(srcmeshptr, venttag, nentnum, coartab, &coarnbr));

    // vnumtax <- ~0 (vnumtax: de taille le nombre de sommets)
    memSet (vnumtax, ~0, vertnbr * sizeof(Gnum));

    // Convertir le maillage en un maillage simple
    CHECK_FDBG2 (mesh2smesh (srcmeshptr, smshptr));

    for (vertnum = 0; vertnum < coarnbr; vertnum ++)
        vnumtax[coartab[vertnum]] = 0;

    // pour chaque sommet du maillage
    for (nvrtnbr = vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnbr; vertnum ++)
    //      Calculer la nouvelle numérotation
    //      Si on garde le sommet
    //      vnumtax[s] <- nvrtnbr++
      if (vnumtax[vertnum] == ~0) {
        vinvtax[nvrtnbr] = vertnum;
        vnumtax[vertnum] = nvrtnbr ++;
      }
    nvrtnbr -= baseval;


    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, nentnum, baseval, &it_nghb) ;
    // pour chaque sommet « s » à contracter du maillage
    for (coarnum = 0; coarnum < coarnbr; coarnum ++) {
      Gnum melmmin /* minimum element by mesh number */
    //      On prend le plus petit élément voisin « v' »
      vertnum = coartab[coarnum];
      mvrtnum = meshptr->entttax[nentnum]->mvrttax[vertnum];
      elemmin = baseval + vertnbr;

      PAMPA_itStart(&it_nghb, vertnum) ;

      while (PAMPA_itHasMore(&it_nghb)) {
        Gnum elemnum;

        elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
        if (elemnum < elemmin)
          elemmin = elemnum;
        PAMPA_itNext(&it_nghb);
      }
      melmmin = meshptr->entttax[baseval]->mvrttax[elemmin];

      vnumtax[mvrtnum] = - vnumtax[melmmin] - 1;

    //      On met pour chaque élément v != v'
    //      vnumtax[v] <- -(v' + 1)
      PAMPA_itStart(&it_nghb, vertnum) ;

      while (PAMPA_itHasMore(&it_nghb)) {
        Gnum elemnum;
        Gnum melmnum;

        elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
        melmnum = PAMPA_itCurMeshVertNum(&it_nghb);
        if (elemnum != elemmin) 
          vnumtax[melmnum] = - vnumtax[melmmin] - 1;
        PAMPA_itNext(&it_nghb);
      }
    }

    hashnbr = meshptr->degrmax * 4;
    for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */

    hashmsk = hashsiz - 1;
    hashmax = hashsiz >> 2;

    if ((hashtab = (Gnum *) memAlloc (hashsiz * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      return (1);
    }

    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, nentnum, baseval, &it_nghb) ;

    for (coarnum = 0; coarnum < coarnbr; coarnum ++) {
      memSet (hashtab, ~0, hashsiz * sizeof (Gnum));
      hashnbr = 0;

      vertnum = coartab[coarnum];
      melmmin = - vnumtax[meshptr->entttax[nentnum]->mvrttax[vertnum]] - 1;
      PAMPA_itStart(&it_nghb, vertnum) ;

      // pour chaque sommet « s » du maillage, à contracter
      //          En utilisant vnumtax[v] et vnumtax[s], on recopie chaque voisin « v » de « s » qui n'y est pas déjà (table de hachage pour éviter les doublons)

      while (PAMPA_itHasMore(&it_nghb)) {
        Gnum melmnum;
        Gnum elemnum;

        elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
        melmnum = PAMPA_itCurMeshVertNum(&it_nghb);
        for (edgenum = smshptr->verttab[melmnum], edgennd = smshptr->vendtab[melmnum]; edgenum < edgennd; edgenum ++) {
          Gnum nngbnum; /* new neighbor number */
          nngbnum = vnumtax[smshptr->edgetab[edgenum]];
          if (nngbnum < 0)
            nngbnum = - vnumtax[melmnum] - 1;
          if (nngbnum != melmmin) {
            for (hashnum = (nngbnum * MESHCOARHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum].nngbnum != nngbnum; hashnum = (hashnum + 1) & hashmsk) ;

            if (hashtab[hashnum] == ~0) { /* Vertex not already added */
              hashtab[hashnum] = nngbnum;
              hashnbr ++;
              if (hashnbr >= hashmax)  /* If hashtab is too much filled */
                CHECK_FDBG2 (meshCoarVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk));
            }
          }
        }
        PAMPA_itNext(&it_nghb);
      }

      verttax[melmmin] = edgeidx;
      venttax[melmmin] = smshptr->venttax[melmmin];
      for (hashnum = 0; hashnum < hashsiz; hashnum ++)
        if (hashtab[hashnum] != ~0) {
            if (edlotax[edgeidx] != NULL)
              edlotax[edgeidx] = smshptr->edlotax[hashtab[hashnum]];
          edgetax[edgeidx ++] = hashtab[hashnum];
        }
      vendtax[melmmin] = edgeidx;
    }

    // pour chaque sommet « s » du maillage, qui n'est pas à contracter
    for (edgeidx = vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
      memSet (hashtab, ~0, hashsiz * sizeof (Gnum));
      hashnbr = 0;

      //      Si vnumtax[s] >= baseval
      if (vnumtax[vertnum] >= baseval) {
        nvrtnum = vnumtax[vertnum];

        //          En utilisant vnumtax[v] et vnumtax[s], on recopie chaque voisin « v » de « s » qui n'y est pas déjà (table de hachage pour éviter les doublons)
        //      Sinon
        //          Ajouter à - (vnumtax[s] + 1) les voisins de « s » s'ils n'y sont pas déjà
        for (edgenum = smshptr->verttab[vertnum], edgennd = smshptr->vendtab[vertnum]; edgenum < edgennd; edgenum ++) {
          Gnum nngbnum; /* new neighbor number */
          nngbnum = vnumtax[smshptr->edgetab[edgenum]];
          if (nngbnum < 0)
            nngbnum = - vnumtax[melmnum] - 1;
          for (hashnum = (nngbnum * MESHCOARHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum].nngbnum != nngbnum; hashnum = (hashnum + 1) & hashmsk) ;

          if (hashtab[hashnum] == ~0) { /* Vertex not already added */
            hashtab[hashnum] = nngbnum;
            hashnbr ++;
            if (hashnbr >= hashmax)  /* If hashtab is too much filled */
              CHECK_FDBG2 (meshCoarVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk));
          }
        }

        verttax[nvrtnum] = edgeidx;
        venttax[nvrtnum] = smshptr->venttax[nvrtnum];
        for (hashnum = 0; hashnum < hashsiz; hashnum ++)
          if (hashtab[hashnum] != ~0) {
            if (edlotax[edgeidx] != NULL)
              edlotax[edgeidx] = smshptr->edlotax[hashtab[hashnum]];
            edgetax[edgeidx ++] = hashtab[hashnum];
          }
        vendtax[nvrtnum] = edgeidx;
      }
    }

    // XXX FIXME
    //      ATTENTION : s'il y a plus que deux entités dans le maillage, il faut
    //      vérifier qu'un sommet n'ait pas qu'un seul sommet voisin d'une autre
    //      entité (ex : arête qui a plus qu'un seul nœud, est-ce généralisable
    //      ?)
    
    CHECK_FDBG2 (meshBuild(dstmeshptr, ~0, baseval, nvrtnbr, verttax, vendtax, edgeidx - baseval, smshptr->edgesiz, edgetax, edlotax, smshptr->enttnbr, smshptr->venttax, smshptr->esubtab, smshptr->valumax, NULL, ~0, ~0));
    smeshExit (smshptr);

#ifdef PAMPA_DEBUG_MESH
    CHECK_FDBG2 (meshCheck (dstmeshptr));
#endif /* PAMPA_DEBUG_MESH */

    // il reste les valeurs à propager...
    valsptr = srcmeshptr->valsptr;
    //nvlsptr = dstmeshptr->valsptr;
    {
      enttnum = PAMPA_ENTT_VIRT_PROC;
      MeshEntity * restrict enttptr; /* new entity pointer */
      MeshEntity * restrict nentptr; /* new entity pointer */
      Value * restrict valuptr;

      enttptr = srcmeshptr->entttax[enttnum];
      nentptr = dstmeshptr->entttax[enttnum];

      valuptr = valsptr->evaltak[enttnum];
      while (valuptr != NULL) {
        byte * restrict * restrict valutax;
        MPI_Datatype typeval;
        Gnum typesiz;
        Gnum sizeval;

        CHECK_FDBG2 (meshValueLink (dstmeshptr, valutax, &sizeval, &typesiz, typeval, enttnum, valuptr->tagnum));
        valutax -= typesiz;

        memCpy ((byte *) valutax + typesiz, valuptr->valutab + typesiz, sizeval * typesiz * sizeof (byte));

        valuptr = valuptr->next;
      }
    }

    {
      enttnum = PAMPA_ENTT_VIRT_VERT;
      MeshEntity * restrict enttptr; /* new entity pointer */
      MeshEntity * restrict nentptr; /* new entity pointer */
      Value * restrict valuptr;

      enttptr = srcmeshptr->entttax[enttnum];
      nentptr = dstmeshptr->entttax[enttnum];

      valuptr = valsptr->evaltak[enttnum];
      while (valuptr != NULL) {
        byte * restrict * restrict valutax;
        MPI_Datatype typeval;
        Gnum typesiz;

        CHECK_FDBG2 (meshValueLink (dstmeshptr, valutax, NULL, &typesiz, typeval, enttnum, valuptr->tagnum));
        valutax -= typesiz;

        for (nvrtnum = baseval, nvrtnnd = baseval + nentptr->vertnbr; nvrtnum < nvrtnnd; nvrtnum ++) 
          memCpy ((byte *) valutax + nvrtnum * typesiz, valuptr->valutab + vinvtax[nvrtnum] * typesiz, typesiz * sizeof (byte));

        valuptr = valuptr->next;
      }
    }

    for (enttnum = baseval, enttnnd = dstmeshptr->enttnbr + baseval; enttnum < enttnnd; enttnum ++) {
      MeshEntity * restrict enttptr; /* new entity pointer */
      MeshEntity * restrict nentptr; /* new entity pointer */
      Value * restrict valuptr;

      enttptr = srcmeshptr->entttax[enttnum];
      nentptr = dstmeshptr->entttax[enttnum];

      //for (vertnum = baseval, 
      for (vertnum = baseval, vertnnd = baseval + enttptr->vertnbr; vertnum < vertnnd; vertnum ++) 
        vnumtax[enttptr->mvrttax[vertnum]] = vertnum;

      for (nvrtnum = baseval, nvrtnnd = baseval + nentptr->vertnbr; nvrtnum < nvrtnnd; nvrtnum ++)
        vinvtx2[vertnum] = vnumtax[vinvtax[nentptr->mvrttax[vertnum]]] - baseval;

      valuptr = valsptr->evaltak[enttnum];
      while (valuptr != NULL) {
        byte * restrict * restrict valutax;
        MPI_Datatype typeval;
        Gnum typesiz;

        CHECK_FDBG2 (meshValueLink (dstmeshptr, valutax, NULL, &typesiz, typeval, enttnum, valuptr->tagnum));
        valutax -= typesiz;

        for (nvrtnum = baseval, nvrtnnd = baseval + nentptr->vertnbr; nvrtnum < nvrtnnd; nvrtnum ++) 
          memCpy ((byte *) valutax + nvrtnum * typesiz, valuptr->valutab + vinvtx2[nvrtnum] * typesiz, typesiz * sizeof (byte));

        valuptr = valuptr->next;
      }
    }

    return (0);
}
