/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_halo.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module is the API for the
//!                distributed mesh halo routines of the
//!                libpampa library. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_halo.h"
#include "pampa.h"

/************************************/
/*                                  */
/* These routines are the C API for */
/* the mesh handling routines.      */
/*                                  */
/************************************/

//! \brief This routine requests the computation
//! of the ghost edge array.
//!
//! \returns 0   : if the computation succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshGhst (
PAMPA_Dmesh * const         meshptr)                                      //!< Distributed mesh for which the computation will be done
{
  //return (dmeshGhst ((Dmesh *) meshptr));
  // XXX appeler une routine intermédiaire pour calculer les tableaux manquants
  // (peut-être dmeshBuild3 en l'adaptant un peu
  // Cela peut peut-être fait maintenant en utilisant un Dsmesh et en modifiant
  // un peu les paramètres d'entrée du dmeshGhst is ovlglbval = 0
  errorPrint ("This routine couldn't be called yet\n");
  return (1);
}




//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for all associated
//! values and all entities.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHalo (
PAMPA_Dmesh * const         meshptr)                                      //!< Distributed mesh on which exchange will be done
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues *                    valslocptr;
  Dvalue                   *valulocptr;
  int                          valulocnum;

  srcmeshptr = (Dmesh *) meshptr;

  valslocptr = srcmeshptr->valslocptr;

  // XXX il faut faire un test dans un define de débogage pour savoir si enttnum est >= baseval et < enttglbnbr + baseval
  // rapporter le test dans les autres routines du fichier
  for( valulocnum = 0, valulocptr = valslocptr->valuloctab ;
      valulocnum < valslocptr->valuglbmax ;
      valulocnum++, valulocptr++) {
    if ((valulocptr->tagnum == PAMPA_TAG_NIL) || ((valulocptr->flagval & PAMPA_VALUE_PRIVATE) != 0))
      continue;

    CHECK_FERR (dmeshHaloSync (srcmeshptr, srcmeshptr->enttloctax[valulocptr->enttnum], (byte *) valulocptr->valuloctab, valulocptr->typeval), srcmeshptr->proccomm);
  }

  return (0);
}





//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for all associated
//! values and all entities, in an asynchronous way.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHaloAsync (
PAMPA_Dmesh * const         meshptr,                                      //!< Distributed mesh on which exchange will be done
PAMPA_DmeshHaloReqs * const  requptr)                                      //!< Request pointer
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues *                    valslocptr;
  Dvalue                   *valulocptr;
  PAMPA_Num                    valulocnum;
  PAMPA_Num *                  entttab;
  PAMPA_Num *                  tagtab;
  PAMPA_Num                    valsglbnbr;
  int                          cheklocval;

  cheklocval = 0;
  srcmeshptr = (Dmesh *) meshptr;

  valslocptr = srcmeshptr->valslocptr;

  if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
        &entttab, (size_t) (valslocptr->valuglbnbr * sizeof (Gnum)),
        &tagtab, (size_t) (valslocptr->valuglbnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);

  // XXX il faut faire un test dans un define de débogage pour savoir si enttnum est >= baseval et < enttglbnbr + baseval
  // rapporter le test dans les autres routines du fichier
  for(valsglbnbr =  valulocnum = 0, valulocptr = valslocptr->valuloctab ;
      valulocnum < valslocptr->valuglbmax ;
      valulocnum++, valulocptr++) {
    if ((valulocptr->tagnum == PAMPA_TAG_NIL) || ((valulocptr->flagval & PAMPA_VALUE_PRIVATE) != 0))
      continue;
    

    entttab[valsglbnbr] = valulocptr->enttnum;
    tagtab[valsglbnbr ++] = valulocptr->tagnum;
  }

  dmeshHaloMultAsync (srcmeshptr, valsglbnbr, entttab, tagtab, (DmeshHaloRequests * const) requptr);
  return (0);
}



//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for all associated
//! values and all entities, in an asynchronous way.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHaloTagAsync (
PAMPA_Dmesh * const         meshptr,                                      //!< Distributed mesh on which exchange will be done
PAMPA_Num     const         tagval,                                       //!< Tag of values
PAMPA_DmeshHaloReqs * const  requptr)                                     //!< Request pointer
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues *                    valslocptr;
  Dvalue                   *valulocptr;
  PAMPA_Num                    valulocnum;
  PAMPA_Num *                  entttab;
  PAMPA_Num *                  tagtab;
  PAMPA_Num                    valsglbnbr;
  int                          cheklocval;

  cheklocval = 0;
  srcmeshptr = (Dmesh *) meshptr;

  valslocptr = srcmeshptr->valslocptr;

  // XXX valuglbnbr est trop grand, à modifier ! TODO
  if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
        &entttab, (size_t) (valslocptr->valuglbnbr * sizeof (Gnum)),
        &tagtab, (size_t) (valslocptr->valuglbnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);

  // XXX il faut faire un test dans un define de débogage pour savoir si enttnum est >= baseval et < enttglbnbr + baseval
  // rapporter le test dans les autres routines du fichier
  for(valsglbnbr =  valulocnum = 0, valulocptr = valslocptr->valuloctab ;
      valulocnum < valslocptr->valuglbmax ;
      valulocnum++, valulocptr++) {
    if ((valulocptr->tagnum == PAMPA_TAG_NIL) || ((valulocptr->flagval & PAMPA_VALUE_PRIVATE) != 0) || (valulocptr->tagnum != tagval))
      continue;
    

    entttab[valsglbnbr] = valulocptr->enttnum;
    tagtab[valsglbnbr ++] = valulocptr->tagnum;
  }

  dmeshHaloMultAsync (srcmeshptr, valsglbnbr, entttab, tagtab, (DmeshHaloRequests * const) requptr);

  memFreeGroup (entttab);
  return (0);
}






//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for
//! associated values which have a given tag number with a given entity number.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHaloValue (
PAMPA_Dmesh * const         meshptr,                                      //!< Distributed mesh on which partial exchange will be done
PAMPA_Num                   enttnum,                                      //!< Entity number
PAMPA_Num                   tagnum)                                       //!< Tag number
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues * valslocptr;
  Dvalue                   *valulocptr;
  int cheklocval;

  cheklocval = 0;

  srcmeshptr = (Dmesh *) meshptr;
  valslocptr = srcmeshptr->valslocptr;
  valulocptr = valslocptr->evalloctak[enttnum];

  while (valulocptr !=  NULL) {
    if (valulocptr->tagnum == tagnum)
      break;
    valulocptr = valulocptr->next;
  }

  if (valulocptr == NULL) { // XXX peut-être mettre ce test dans un define de débogage
    errorPrint ("enttnum or tagnum are not correct");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);
  if ((valulocptr->flagval & PAMPA_VALUE_PRIVATE) != 0) { // XXX peut-être mettre ce test dans un define de débogage
    errorPrint ("private value could not be exchanged");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);


  CHECK_FERR (dmeshHaloSync (srcmeshptr, srcmeshptr->enttloctax[enttnum], (byte *) valulocptr->valuloctab, valulocptr->typeval), srcmeshptr->proccomm);

  CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
  return (0);
}







//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for
//! associated values which have a given tag number with a given entity number,
//! in an asynchronous way.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHaloValueAsync (
PAMPA_Dmesh * const         meshptr,                                      //!< Distributed mesh on which partial exchange will be done
PAMPA_Num                   enttnum,                                      //!< Entity number
PAMPA_Num                   tagnum,                                       //!< Tag number
PAMPA_DmeshHaloReqs * const  requptr)                                      //!< Request pointer
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues * valslocptr;
  Dvalue                   *valulocptr;
  int cheklocval;

  cheklocval = 0;

  srcmeshptr = (Dmesh *) meshptr;
  valslocptr = srcmeshptr->valslocptr;
  valulocptr = valslocptr->evalloctak[enttnum];

  while (valulocptr !=  NULL) {
    if (valulocptr->tagnum == tagnum)
      break;
    valulocptr = valulocptr->next;
  }

  if (valulocptr == NULL) { // XXX peut-être mettre ce test dans un define de débogage
    errorPrint ("enttnum or tagnum are not correct");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);
  if ((valulocptr->flagval & PAMPA_VALUE_PRIVATE) != 0) { // XXX peut-être mettre ce test dans un define de débogage
    errorPrint ("private value could not be exchanged");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);


  dmeshHaloAsync (srcmeshptr, srcmeshptr->enttloctax[enttnum], (byte *) valulocptr->valuloctab, valulocptr->typeval, (DmeshHaloRequests * const) requptr);

  CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
  return (0);
}







//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for
//! associated values which have a given entity number.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHaloEntt (
PAMPA_Dmesh * const         meshptr,                                      //!< Distributed mesh on which partial exchange will be done
PAMPA_Num                   enttnum)                                      //!< Entity number
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues * valslocptr;
  Dvalue                   *valulocptr;
  int cheklocval;

  cheklocval = 0;

  srcmeshptr = (Dmesh *) meshptr;
  valslocptr = srcmeshptr->valslocptr;
  valulocptr = valslocptr->evalloctak[enttnum];

  while (valulocptr !=  NULL) {
    if (valulocptr->tagnum != PAMPA_TAG_NIL) {
      if ((valulocptr->flagval & PAMPA_VALUE_PUBLIC) != 0)
      	CHECK_FERR (dmeshHaloSync (srcmeshptr, srcmeshptr->enttloctax[valulocptr->enttnum], (byte *) valulocptr->valuloctab, valulocptr->typeval), srcmeshptr->proccomm);
    }
    valulocptr = valulocptr->next;
  }

  CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
  return 0;
}







//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for
//! associated values which have a given entity number, in an asynchronous way.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.
//! \warning This interface is not yet implemented

int
PAMPA_dmeshHaloEnttAsync (
PAMPA_Dmesh * const         meshptr,                                      //!< Distributed mesh on which partial exchange will be done
PAMPA_Num                   enttnum,                                      //!< Entity number
PAMPA_DmeshHaloReqs * const  requptr)                                      //!< Request pointer
{
  Dmesh *                      srcmeshptr;         /* Pointer to source mesh structure          */
  Dvalues *                    valslocptr;
  Dvalue                   *valulocptr;

  errorPrint ("Not yet implemented"); // XXX il faut modifier la structure de requptr et dmeshHaloAsync 
  return (1);

  srcmeshptr = (Dmesh *) meshptr;
  valslocptr = srcmeshptr->valslocptr;
  valulocptr = valslocptr->evalloctak[enttnum];

  while (valulocptr !=  NULL) {
    if (valulocptr->tagnum != PAMPA_TAG_NIL) {
      dmeshHaloAsync (srcmeshptr, srcmeshptr->enttloctax[valulocptr->enttnum], (byte *) valulocptr->valuloctab, valulocptr->typeval, (DmeshHaloRequests * const) requptr);
    }
    valulocptr = valulocptr->next;
  }

  return (0);
}



//! \brief This routine spreads local information
//! borne by local vertices across the ghost
//! vertices of the neighboring processes.
//! The process will be for
//! given multiple associated values, in an asynchronous way.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int PAMPA_dmeshHaloValueMultAsync (
     PAMPA_Dmesh * const               meshptr,
     PAMPA_Num                               enttnbr,
     PAMPA_Num  *                           entttab,
     PAMPA_Num  *                           tagtab,
     PAMPA_DmeshHaloReqs * const  reqsptr)
{
  dmeshHaloMultAsync ((Dmesh *) meshptr, enttnbr, entttab, tagtab, (DmeshHaloRequests *) reqsptr);
  return (0);
}

int PAMPA_dmeshHaloValueMult (
     PAMPA_Dmesh * const               meshptr,
     PAMPA_Num                               enttnbr,
     PAMPA_Num  *                           entttab,
     PAMPA_Num  *                           tagtab)
{
  return dmeshHaloMultSync ((Dmesh *) meshptr, enttnbr, entttab, tagtab, NULL);
}





//! \brief This routine waits for the termination of
//! an asynchronous halo
//! request.
//!
//! \returns 0   : if the exchange succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshHaloWait (
PAMPA_DmeshHaloReqs * const  requptr)                                      //!< Request pointer
{
  return dmeshHaloWait ((DmeshHaloRequests *) requptr);
}
