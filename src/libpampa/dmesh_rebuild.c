/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_rebuild.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 10 Jul 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_REBUILD

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "smesh_mesh.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_overlap.h"
#include "dmesh_build.h"
#include "dsmesh.h"
#include "pampa.h"
#include "pampa.h"
#include "dmesh_rebuild.h"
#include "dsmesh_dmesh.h"
#include <ptscotch.h>
#include "dmesh_adapt.h"

/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshRebuild (
    Dmesh * restrict const    dmshptr,              /**< mesh */
    const Gnum                meshnbr,
    Mesh *                    meshloctab)
{
  Smesh * smshloctab;
  Gnum meshlocnum;
  int cheklocval;

  cheklocval = 0;

  if ((smshloctab = (Smesh *) memAlloc (meshnbr * sizeof (Smesh))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  for (meshlocnum = 0; meshlocnum < meshnbr; meshlocnum ++) {
//#ifdef PAMPA_DEBUG_DMESH2
//    // XXX temporaire
//    PAMPA_Iterator it, it_nghb;
//    PAMPA_meshItInitStart((PAMPA_Mesh *) meshloctab + meshlocnum, 0, &it);
//    PAMPA_meshItInit((PAMPA_Mesh *) meshloctab + meshlocnum, 0, 1, &it_nghb);
//    PAMPA_Num tempval;
//    while (PAMPA_itHasMore(&it)) {
//      PAMPA_Num tetnum;
//
//      tetnum = PAMPA_itCurEnttVertNum(&it);
//      tempval = 0;
//
//      PAMPA_itStart(&it_nghb, tetnum);
//
//      while (PAMPA_itHasMore(&it_nghb)) {
//        tempval ++;
//        PAMPA_itNext(&it_nghb);
//      }
//
//      if (tempval != 4) {
//        errorPrint ("element %d doesn't have 4 nodes", tetnum);
//        cheklocval = 1;
//      }
//      CHECK_VERR (cheklocval, dmshptr->proccomm);
//      PAMPA_itNext(&it);
//    }
//#endif /* PAMPA_DEBUG_DMESH2 */
    smeshInit (smshloctab + meshlocnum);
    CHECK_FERR (mesh2smesh (meshloctab + meshlocnum, smshloctab + meshlocnum), dmshptr->proccomm);
    // meshExit (meshloctab + meshlocnum); // XXX doit-on le supprimer ?
  }
  CHECK_VDBG (cheklocval, dmshptr->proccomm);

  CHECK_FERR (dsmeshRebuild (dmshptr, meshnbr, smshloctab), dmshptr->proccomm);

  for (meshlocnum = 0; meshlocnum < meshnbr; meshlocnum ++)
    smeshExit (smshloctab + meshlocnum);

  memFree (smshloctab);
  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  return (0);
}


/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dsmeshRebuild (
    Dmesh * restrict const    dmshptr,              /**< mesh */
    const Gnum                meshnbr,
    Smesh *                   smshloctab)
{
  Dsmesh dsmshdat;
  Gnum * restrict vflgloctax;
  Gnum * restrict orgvprmloctax;
  Gnum * restrict vflggsttax;
  Gnum baseval;
  Gnum procdsp;
  Gnum enttglbmax;
  Gnum * restrict smshsndtab;
  Gnum proclocnbr;
  int * restrict procloctab;
  Gnum * restrict pnbrloctab;
  Gnum * restrict * restrict orgprocvrtptr;
  Gnum * restrict orgprocvrttab;
  Gnum * restrict * restrict procmshtab;
      // XXX tmp
//#define PAMPA_DEBUG_DMESH2
      // XXX fin tmp
#ifdef PAMPA_DEBUG_DMESH2
  Gnum * partloctmp;
#endif /* PAMPA_DEBUG_DMESH2 */
  int cheklocval;
  Gnum vertmax;
  Gnum procmax;
  Gnum passval;
  Gnum enttlocnum;
  Gnum smshlocnum;
  Gnum smshrcvnum;
  Gnum ndstlocinc;
  Gnum nghblocinc;
  Gnum smshsndidx;
  Gnum smshrcvidx;
  Gnum smshrcvnbr;
  Gnum datasndidx;
  Gnum datasndcur;
  Gnum datasndsiz;
  Gnum datasndid2;
  Gnum datasndnbr;
  Gnum datarcvsiz;
  Gnum datarcvidx;
  Gnum datarcvid2;
  Gnum datarcvcur;
  Gnum datarcvnnd;
  Gnum datarcvnbr;
  int procngbnum;
  int procngbnnd;
  int * restrict dsndcnttab;
  int * restrict dsnddsptab;
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  Gnum * vertsidtab;
  Gnum * restrict datasndtab;
  Gnum * restrict datarcvtab;
  int * restrict dsndcnttb2;
  int * restrict dsnddsptb2;
  int * restrict drcvcnttb2;
  int * restrict drcvdsptb2;
  Gnum vertloccur;
  Gnum dstvertlocnbr;
  Gnum vextlocnbr;
  Gnum vintlocnbr;
  Gnum vbndlocnbr;
  Gnum dstedgelocnbr;
  Gnum dstedlolocnbr;
  Gnum dstedgelocsiz;
  Gnum edgelocidx;
  Gnum srcvertlocnum;
  Gnum srcvertlocnnd;
  Gnum datasndnb2;
  Gnum datarcvnb2;
  Gnum vertcntnbr;
  Gnum vswploccur;
  Gnum vswplocnbr;
  Gnum * restrict evrttax;
  Gnum * restrict svrttax;
  Gnum * restrict * restrict vflgtab;
  SmeshRcv * restrict smshrcvtab;
  NdstAdj * restrict smshrcvtb2;
  Gnum * restrict vintloctab;
  Gnum * restrict datasndtb2;
  Gnum * datarcvtb2;
  ProcNdst ** ndstsndtab; /* contains distant neighbors */
  Gnum * ndstsndtb2; /* contains distant neighbors number for each processor */
  PermNdst * ndstrcvtab;
  Gnum * restrict datarcvtb3;
  Gnum * restrict datasndtb3;
  DmeshRebldSmeshVert * restrict hashtab;
  Gnum                          hashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          hashmax;
  Gnum                          hashnbr;
  Gnum                          hashmsk;
  Gnum * restrict evrtloctax;
  Gnum * restrict mvrtloctax;
  Gnum * restrict vprmgsttax;
  Gnum * restrict vswploctab;
  Gnum * restrict vmshcnttax;
  Gnum * restrict vmshdsptax;
  Gnum * restrict vmshloctab;
  Gnum * restrict * restrict vprmmshtab;
  Gnum * restrict enbrloctax;
  Gnum * restrict dstvertloctax;
  Gnum * restrict dstvendloctax;
  Gnum * restrict dstventloctax;
  Gnum * restrict dstedgeloctax;
  Gnum * restrict dstedloloctax;
  Dvalues * restrict valslocptr;
  DataSndEntt * restrict dsndloctab;
  DataRcvEntt * restrict drcvloctab;
  int * tmpptr;
  Gnum srcvertlocbas;
  Gnum degrmax;
  SCOTCH_Strat  strtdat; 
  SCOTCH_Arch   archdat; 
  SCOTCH_Graph  grafdat;
  INIT_TIME(cnt1);

  START_TIME(cnt1);
  cheklocval = 0;
  baseval = dmshptr->baseval;
  enttglbmax = dmshptr->enttglbnbr + baseval - 1;
  CHECK_FERR (dmeshValueData (dmshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, NULL, NULL, (void **) &vflgloctax), dmshptr->proccomm);
  vflgloctax -= baseval;
  cheklocval = dmeshValueData (dmshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_PERM, NULL, NULL, (void **) &orgvprmloctax);
  if (cheklocval == 0)  /* If there is a value with this tag */
    orgvprmloctax -= baseval;
  else if (cheklocval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    CHECK_VERR (cheklocval, dmshptr->proccomm);
  }
  else { /* If there no value with this tag */
    orgvprmloctax = NULL;
    cheklocval = 0;
  }

  cheklocval = dmeshValueData (dmshptr, PAMPA_ENTT_VIRT_PROC, TAG_INT_PROCVRT, NULL, NULL, (void **) &orgprocvrttab);
  if (cheklocval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    CHECK_VERR (cheklocval, dmshptr->proccomm);
    orgprocvrtptr = &orgprocvrttab;
  }
  else { /* If there no value with this tag */
    orgprocvrtptr = &dsmshdat.procvrttab;
    cheklocval = 0;
  }


  if ((evrtloctax = (Gnum *) memAlloc (dmshptr->enttglbnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);
  evrtloctax -= baseval;
  memSet (evrtloctax + baseval, 0, dmshptr->enttglbnbr * sizeof (Gnum));

  valslocptr = dmshptr->valslocptr;
  for (enttlocnum = baseval; enttlocnum <= enttglbmax; enttlocnum ++) 
    if (valslocptr->evalloctak[enttlocnum] != NULL) {
      CHECK_FERR (dmeshValueLink (dmshptr, (void **) &mvrtloctax, PAMPA_VALUE_PRIVATE, NULL, NULL, GNUM_MPI, enttlocnum, TAG_INT_GLOBAL), dmshptr->proccomm);
      mvrtloctax -= baseval;
      memCpy (mvrtloctax + baseval, dmshptr->enttloctax[enttlocnum]->mvrtloctax + baseval, dmshptr->enttloctax[enttlocnum]->vertlocnbr * sizeof (Gnum));
      evrtloctax[enttlocnum] = dmshptr->enttloctax[enttlocnum]->vertlocnbr;
    }

  // - convertir dmshptr en Dsmesh
  dsmeshInit(&dsmshdat, dmshptr->proccomm);
  CHECK_FERR (dmesh2dsmesh(dmshptr, &dsmshdat), dmshptr->proccomm);
  dmeshFree (dmshptr);

  const Gnum vertlocmin = dsmshdat.procvrttab[dsmshdat.proclocnum];
  const Gnum vertlocmax = dsmshdat.procvrttab[dsmshdat.proclocnum + 1] - 1;
  srcvertlocbas = dsmshdat.procvrttab[dsmshdat.proclocnum] - baseval;

  degrmax = dsmshdat.degrlocmax;
  procmax =
    vertmax = 0;
  for (smshlocnum = 0; smshlocnum < meshnbr; smshlocnum ++) {
    Gnum proctmp;
    Gnum * restrict vnbrtab;

    CHECK_FERR (smeshValueData (&smshloctab[smshlocnum], PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART, NULL, NULL, (void **) &vnbrtab), dsmshdat.proccomm);

    for (proctmp = procngbnum = 0; procngbnum < dsmshdat.procglbnbr; procngbnum ++)
      if (vnbrtab[procngbnum] > 0)
        proctmp ++;

    if (procmax < proctmp)
      procmax = proctmp;

    if (smshloctab[smshlocnum].vertnbr > vertmax)
      vertmax = smshloctab[smshlocnum].vertnbr;
    if (smshloctab[smshlocnum].degrmax > degrmax)
      degrmax = smshloctab[smshlocnum].degrmax;
  }
  END_TIME(cnt1);

  START_TIME(cnt1);
  //- pour chaque maillage local «meshlocnum» :
  //  - chercher le plus petit numéro global de sommet
  //  - en déduire le proc qui l'a par défaut sauf frontière
  //  - pour chaque sommet «s»
  //    - si sommet de frontière
  //      - proc est déterminé par rapport au num global du sommet
  // - vnumsidtab alloue de taille procglbnbr et initialisé à baseval
  if (memAllocGroup ((void **) (void *)
        &smshsndtab, (size_t) (meshnbr * sizeof (Gnum)),
        &ndstsndtb2, (size_t) (meshnbr * sizeof (Gnum *)),
        &ndstsndtab, (size_t) (meshnbr * sizeof (ProcNdst *)),
        &vflggsttax, (size_t) (dsmshdat.vgstlocnbr * sizeof (Gnum)),
        &vflgtab,    (size_t) (meshnbr * sizeof (Gnum *)),
        &procloctab, (size_t) (procmax * sizeof (int)),
        &pnbrloctab, (size_t) (procmax * sizeof (Gnum)), // FIXME : pnbr n'est pas le bon nom vu qu'il s'agit du nb de sommets par proc
        &procmshtab, (size_t) (meshnbr * sizeof (Gnum *)), 
#ifdef PAMPA_DEBUG_DMESH2
        &partloctmp, (size_t) (vertmax * sizeof (Gnum)),
#endif /* PAMPA_DEBUG_DMESH2 */
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);
  vflggsttax -= baseval;
#ifdef PAMPA_DEBUG_DMESH2
  partloctmp -= baseval;
#endif /* PAMPA_DEBUG_DMESH2 */

  memSet (pnbrloctab, 0, procmax * sizeof (int));
  memSet (ndstsndtb2, 0, meshnbr * sizeof (Gnum));
  memCpy (vflggsttax + baseval, vflgloctax + baseval, dsmshdat.vertlocnbr * sizeof (Gnum));
  CHECK_FERR (dsmeshHaloSyncComm (&dsmshdat, dsmshdat.commlocptr, dsmshdat.vertlocnbr, vflggsttax, GNUM_MPI), dsmshdat.proccomm);
  END_TIME(cnt1);

  START_TIME(cnt1);
  CHECK_FERR (SCOTCH_graphInit (&grafdat), dsmshdat.proccomm);
  CHECK_FERR (SCOTCH_stratInit (&strtdat), dsmshdat.proccomm);
  CHECK_FERR (SCOTCH_archInit (&archdat), dsmshdat.proccomm);
  for (smshlocnum = 0; smshlocnum < meshnbr; smshlocnum ++) {
    Smesh * smshptr;
    Gnum * restrict vnbrtab;
    Gnum proclocidx;
    Gnum vertnum;
    Gnum vertnnd;
    Gnum vertnbr;
    Gnum vertdif;
    Gnum tmpflg;

    smshptr = smshloctab + smshlocnum;

    CHECK_FERR (smeshValueData (smshptr, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART, NULL, NULL, (void **) &vnbrtab), dsmshdat.proccomm);
    vnbrtab -= baseval;

    CHECK_FERR (smeshValueData (smshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, NULL, NULL, (void **) vflgtab + smshlocnum), dsmshdat.proccomm);
    vflgtab[smshlocnum] -= baseval;

    if ((procmshtab[smshlocnum] = (Gnum *) memAlloc (smshptr->vertnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);
    procmshtab[smshlocnum] -= baseval;

    memSet (procmshtab[smshlocnum] + baseval, ~0, smshptr->vertnbr * sizeof (Gnum));

    for (vertnbr = proclocnbr = procngbnum = 0; procngbnum < dsmshdat.procglbnbr; procngbnum ++)
      if (vnbrtab[procngbnum] > 0) {
        pnbrloctab[proclocnbr] = vnbrtab[procngbnum];
        vertnbr += vnbrtab[procngbnum];
        procloctab[proclocnbr ++] = procngbnum;
      }

    // ajout de la diff du nb de sommets en les répartissant sur tous les procs
    if ((smshptr->vertnbr - vertnbr) > 0) {
      vertdif = smshptr->vertnbr - vertnbr;

      for (proclocidx = 0; proclocidx < proclocnbr; proclocidx ++)
        pnbrloctab[proclocidx] += DATASIZE (vertdif, proclocnbr, proclocidx);
    }

    for (proclocidx = tmpflg = 0, procngbnum = procloctab[0], vertnum = baseval, vertnnd = smshptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++) 
      if (vflgtab[smshlocnum][vertnum] != PAMPA_TAG_VERT_INTERNAL) {
        Gnum mvrtnum;

        mvrtnum = vflgtab[smshlocnum][vertnum];

        if (!((mvrtnum >= (*orgprocvrtptr)[procngbnum]) && (mvrtnum < (*orgprocvrtptr)[procngbnum + 1]))) {
          int proclocmax;

          for (proclocidx = 0, proclocmax = proclocnbr;
              proclocmax - proclocidx > 1; ) {
            int                 proclocmed;

            proclocmed = (proclocmax + proclocidx) / 2;
            if ((*orgprocvrtptr)[procloctab[proclocmed]] <= mvrtnum)
              proclocidx = proclocmed;
            else
              proclocmax = proclocmed;
          }
          procngbnum = procloctab[proclocidx];
        }
//#ifdef PAMPA_DEBUG_DMESH2
//        if (!((mvrtnum >= (*orgprocvrtptr)[procngbnum]) && (mvrtnum < (*orgprocvrtptr)[procngbnum + 1]))) {
//          errorPrint ("processor %d does not contain vertex %d", procngbnum, mvrtnum);
//          cheklocval = 1;
//        }
//        CHECK_VERR (cheklocval, dsmshdat.proccomm);
//#endif /* PAMPA_DEBUG_DMESH2 */

        procmshtab[smshlocnum][vertnum] = proclocidx; 
      }
      else 
        tmpflg = 1;


    // FIXME en attendant d'avoir la correction dans Scotch
    if ((proclocnbr != 0) && (tmpflg == 1)) {
      //if (proclocnbr != 0) {
      // XXX FIXME et pour le poids des sommets ???
      CHECK_FERR (SCOTCH_graphBuild (&grafdat, baseval, smshptr->vertnbr, smshptr->verttax + baseval, smshptr->vendtax + baseval, NULL, NULL, smshptr->edgenbr, smshptr->edgetax + baseval, smshptr->edlotax + baseval), dsmshdat.proccomm); /* FIXME est-ce que le partitionnement est comme on le veut réellement : la même proportion qu'avant remaillage puis une répartition équilibrée des sommets restants */
//#define PAMPA_DEBUG_GRAPH
#ifdef PAMPA_DEBUG_GRAPH
      {
        SCOTCH_graphCheck (&grafdat);

        PAMPA_Num i, j;
        char s[30];
        sprintf(s, "graph-rebuild-%d.grf", dsmshdat.proclocnum);
        FILE *graph_file;
        graph_file = fopen(s, "w");
        SCOTCH_graphSave (&grafdat, graph_file);
        fclose(graph_file);

        sprintf(s, "graph-rebuild2-%d.grf", dsmshdat.proclocnum);
        graph_file = fopen(s, "w");
        fprintf (graph_file, "%d\n", proclocnbr);
        for (i = 0; i < proclocnbr; i ++)
          fprintf (graph_file, "%d ", pnbrloctab[i]);
        fprintf (graph_file, "\n%d\n", smshptr->vertnbr);
        for (i = baseval, j = smshptr->vertnbr + baseval; i < j; i ++)
          fprintf (graph_file, "%d ", procmshtab[smshlocnum][i]);
        fprintf (graph_file, "\n", smshptr->vertnbr);
        fclose(graph_file);

//
//        Gnum tetentt, nodentt;
//        double * coortax;
//        int i, j;
//
//        tetentt = 0;
//        nodentt = 1;
//        smeshValueData(smshptr, nodentt, PAMPA_TAG_GEOM, NULL, NULL, (void **) &coortax);
//        coortax -= 3 * baseval;
//        sprintf(s, "graph-rebuild-%d.xyz", dsmshdat.proclocnum);
//        graph_file = fopen("graph-map.xyz", "w");
//        graph_file2 = fopen("graph-map.map", "w");
//        fprintf (graph_file, "3\n%d\n", meshptr->entttax[baseval]->vertnbr);
//        fprintf (graph_file2, "%d\n", meshptr->entttax[baseval]->vertnbr);
//
//        PAMPA_meshItInitStart((PAMPA_Mesh *) meshptr, tetentt, &it);
//        PAMPA_meshItInit((PAMPA_Mesh *) meshptr, tetentt, nodentt, &it_nghb);
//        while (PAMPA_itHasMore(&it)) {
//          PAMPA_Num tetnum;
//          double coorval[3];
//
//          tetnum = PAMPA_itCurEnttVertNum(&it);
//          coorval[0] =
//            coorval[1] =
//            coorval[2] = 0.0;
//
//
//          PAMPA_itStart(&it_nghb, tetnum);
//
//          while (PAMPA_itHasMore(&it_nghb)) {
//            PAMPA_Num nodenum;
//
//            nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
//            for (i = 0; i < 3; i++)
//              coorval[i] += coortax[nodenum * 3 + i];
//            PAMPA_itNext(&it_nghb);
//          }
//
//          fprintf(graph_file, "%d", tetnum);
//          fprintf(graph_file2, "%d %d\n", tetnum, gmaptax[tetnum]);
//          for (i = 0; i < 3; i++)
//            fprintf(graph_file, " %lf", coorval[i] / 4);
//          fprintf(graph_file, "\n");
//
//          PAMPA_itNext(&it);
//        }
//        fclose (graph_file); 
//        fclose (graph_file2); 
      }
#endif /* PAMPA_DEBUG_GRAPH */
//#undef PAMPA_DEBUG_GRAPH
#ifdef PAMPA_DEBUG_DMESH2
      //Gnum tmpval;
      //SCOTCH_graphSize (&grafdat, &tmpval, NULL);
      //if (tmpval == 0) {
      //  errorPrint ("Invalid size of graph");
      //  cheklocval = 1;
      //}
      //CHECK_VERR (cheklocval, dsmshdat.proccomm);

      memCpy (partloctmp + baseval, procmshtab[smshlocnum] + baseval, smshptr->vertnbr * sizeof (Gnum));
#endif /* PAMPA_DEBUG_DMESH2 */
      CHECK_FERR (SCOTCH_archCmpltw (&archdat, proclocnbr, pnbrloctab), dsmshdat.proccomm);
      CHECK_FERR (SCOTCH_stratGraphMap (&strtdat, "r{sep=h}"), dsmshdat.proccomm);
      CHECK_FERR (SCOTCH_graphMapFixed (&grafdat, &archdat, &strtdat, procmshtab[smshlocnum] + baseval), dsmshdat.proccomm);
      SCOTCH_archExit (&archdat);
#ifdef PAMPA_DEBUG_DMESH2
      for (vertnum = baseval, vertnnd = baseval + smshptr->vertnbr; vertnum < vertnnd; vertnum ++)
        if (((partloctmp[vertnum] != ~0) && (procmshtab[smshlocnum][vertnum] != partloctmp[vertnum])) 
            || (procmshtab[smshlocnum][vertnum] > proclocnbr)) {
          //errorPrint ("Invalid part number for vertex %d on Smesh %d", vertnum, smshlocnum);
          procmshtab[smshlocnum][vertnum] = partloctmp[vertnum];
          //cheklocval = 1;
        }
      //CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
      // XXX tmp
//#undef PAMPA_DEBUG_DMESH2
      // XXX fin tmp
      SCOTCH_graphFree (&grafdat);
    }
    else if (tmpflg == 1) {
      memSet (procmshtab[smshlocnum] + baseval, 0, smshptr->vertnbr * sizeof (Gnum));
      procloctab[0] = dsmshdat.proclocnum;
    }


    for (vertnum = baseval, vertnnd = smshptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++) 
        procmshtab[smshlocnum][vertnum] = procloctab[procmshtab[smshlocnum][vertnum]];
  // XXX il faut l'enregistrement par PaMPA
//#ifdef PAMPA_DEBUG_DMESH_SAVE2
//    {
//      static int cntnbr = 0;
//      char s1[100];
//      Mesh mshdat;
//
//      sprintf(s1, "rebuild_part_%d_%d_%d.mesh", dsmshdat.proclocnum, cntnbr, smshlocnum);
//      if (smshlocnum == meshnbr - 1)
//        cntnbr ++;
//      Gnum datatab[2];
//      datatab[0] = 0; // XXX 0 pour éléments
//      datatab[1] = 1; // XXX 1 pour nœuds
//      meshInit (&mshdat);
//      smesh2mesh (smshptr, &mshdat);
//      CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (&mshdat), 1, datatab, procmshtab[smshlocnum] + baseval, s1), dsmshdat.proccomm);
//    }
//#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
  }
  SCOTCH_graphExit (&grafdat);
  SCOTCH_stratExit (&strtdat);
  SCOTCH_archExit (&archdat);
  END_TIME(cnt1);


  //
  // - pour chaque sommet «s»
  //   - ajouter à datasndtab
  //     - pour chaque maillage centralisé
  //       - « - smshlocnum»
  //       - pour chaque sommet
  //         - «s»
  //         - mvrttax[s]
  //         - entt
  //         - poids sur le sommet ?
  //         - nb voisins qui sont locaux
  //            - «v»
  //            - poids sur l'arête
  //         - nb voisins qui sont sur un autre proc
  //            - «v»
  //            - poids sur l'arête
  //            - proc

  START_TIME(cnt1);
  const Gnum vertdsp = 0;
  const Gnum mvrtdsp = 1;
  const Gnum enttdsp = 2;
  const Gnum vertlocinc = 3;
  const Gnum nghbdsp = 0; /* TRICK: same value if neighbor is local or distant */
  const Gnum edlodsp = 1; /* TRICK: same value if neighbor is local or distant */

  ndstlocinc = sizeof (ProcNdst) / sizeof (Gnum); /* distant neighbor */
  procdsp = 1;
  if (dsmshdat.edloloctax == NULL) {
    nghblocinc = 1; /* local neighbor */
  }
  else {
    nghblocinc = 2; /* local neighbor */
  }

  hashnbr = MAX (dsmshdat.vertlocnbr / 10, 10); // XXX N'est-ce pas trop grand pour hashnbr ?
  for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */
  hashnbr = 0;

  hashmsk = hashsiz - 1;
  hashmax = hashsiz >> 2;

  if ((hashtab = (DmeshRebldSmeshVert *) memAlloc (hashsiz * sizeof (DmeshRebldSmeshVert))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  //* allouer dsndcnttab, drcvcnttab de taille |proc|
  if (memAllocGroup ((void **) (void *)
        &dsndcnttab, (size_t) (dsmshdat.procglbnbr       * sizeof (int)),
        &dsnddsptab, (size_t) ((dsmshdat.procglbnbr + 1) * sizeof (int)),
        &drcvcnttab, (size_t) (dsmshdat.procglbnbr       * sizeof (int)),
        &drcvdsptab, (size_t) ((dsmshdat.procglbnbr + 1) * sizeof (int)),
        &vertsidtab, (size_t) (dsmshdat.procglbnbr       * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  memSet (dsndcnttab, 0, dsmshdat.procglbnbr * sizeof (int));
  memSet (vertsidtab, ~0, dsmshdat.procglbnbr * sizeof (Gnum));

  for (smshsndidx = 0; smshsndidx < meshnbr; smshsndidx ++) {
    Smesh * smshptr;
    Gnum vertnum;
    Gnum vertnnd;
    Gnum ndstmshidx;

    smshptr = smshloctab + smshsndidx;

    memSet (hashtab, ~0, hashsiz * sizeof (DmeshRebldSmeshVert));

    for (vertnum = baseval, vertnnd = smshptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
      Gnum ndstlocnbr;
      Gnum nghblocnbr;
      int                 procngbnum;
      Gnum edgenum;
      Gnum edgennd;

      procngbnum = procmshtab[smshsndidx][vertnum];

      if (vertsidtab[procngbnum] != smshsndidx) {
        vertsidtab[procngbnum] = smshsndidx;
        dsndcnttab[procngbnum] += 2; // one for Smesh id and one for ndstrcvnbr
      }

      dsndcnttab[procngbnum] += vertlocinc;

      for (ndstlocnbr = nghblocnbr = 0, edgenum = smshptr->verttax[vertnum], edgennd = smshptr->vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
        Gnum nghblocnum;

        nghblocnum = smshptr->edgetax[edgenum];

        nghblocnbr ++;
        if (procmshtab[smshsndidx][nghblocnum] != procngbnum) {
          for (hashnum = (nghblocnum * DMESHREBLDHASHPRIME) & hashmsk; hashtab[hashnum].vertnum != ~0 && (hashtab[hashnum].vertnum != nghblocnum || hashtab[hashnum].prshval.procnum != procngbnum); hashnum = (hashnum + 1) & hashmsk) ;

          if ((hashtab[hashnum].vertnum != nghblocnum) || (hashtab[hashnum].prshval.procnum != procngbnum)) { /* Vertex not already added */

            ndstlocnbr ++;
            hashtab[hashnum].vertnum = nghblocnum;
            hashtab[hashnum].prshval.procnum = procngbnum;
            hashnbr ++;

            if (hashnbr >= hashmax) /* If hashtab is too much filled */
              CHECK_FERR (dmeshRebldSmeshVertResize (&dsmshdat, &hashtab, &hashsiz, &hashmax, &hashmsk), dsmshdat.proccomm);
          }
        }
      }

      dsndcnttab[procngbnum] += nghblocnbr * nghblocinc + 1 + ndstlocnbr * ndstlocinc;
      ndstsndtb2[smshsndidx] += ndstlocnbr;
    }

    if ((ndstsndtab[smshsndidx] = (ProcNdst *) memAlloc (ndstsndtb2[smshsndidx] * sizeof (ProcNdst))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);

    for (ndstmshidx = hashnum = 0; hashnum < hashsiz; hashnum ++)
      if (hashtab[hashnum].vertnum != ~0) {
        ndstsndtab[smshsndidx][ndstmshidx].nghbnum = hashtab[hashnum].vertnum;
        ndstsndtab[smshsndidx][ndstmshidx ++].procnum = (Gnum) hashtab[hashnum].prshval.procnum;
      }

    intSort2asc2(ndstsndtab[smshsndidx], ndstsndtb2[smshsndidx]);
  }
  memFree (hashtab);

  /* Compute displacement sending array */
  dsnddsptab[0] = 0;
  for (procngbnum = 1, procngbnnd = dsmshdat.procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) {
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  }

  datasndnbr = dsnddsptab[dsmshdat.procglbnbr];

  //* envoyer dsndcnttab et recevoir drcvcnttab
  CHECK_FMPI (cheklocval, commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, dsmshdat.proccomm), dsmshdat.proccomm);

  //* en déduire ddsp{snd,rcv}tab
  /* Compute displacement receiving array */
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  drcvdsptab[dsmshdat.procglbnbr] = drcvdsptab[dsmshdat.procglbnbr - 1] + drcvcnttab[dsmshdat.procglbnbr - 1];
  datarcvnbr = drcvdsptab[dsmshdat.procglbnbr];
  dsndcnttab[0] = dsnddsptab[0];

  //* allouer datasndtab et datarcvtab
  //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
  if (memAllocGroup ((void **) (void *)
        &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
        &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);


  /* Fill in datasndtab */
  // - pour chaque sommet «s»
  //   - ajouter à datasndtab
  //     - pour chaque maillage centralisé
  //       - « - smshlocnum»
  //       - pour chaque sommet
  //         - «s»
  //         - mvrttax[s]
  //         - entt
  //         - poids sur le sommet ?
  //         - nb voisins qui sont locaux ou qui sont distants mais dont le num de proc a déjà été envoyé (utiliser un tableau de drapeaux ndsttax de taille max(smshptr->vertnbr)
  //            - «v» si local et - «v» si distant (-v doit prendre en compte baseval)
  //            - poids sur l'arête
  //         - nb voisins qui sont sur un autre proc
  //            - «v»
  //            - poids sur l'arête
  //            - proc
  memSet (vertsidtab, ~0, dsmshdat.procglbnbr * sizeof (Gnum));

  for (smshsndidx = 0; smshsndidx < meshnbr; smshsndidx ++) {
    Smesh * smshptr;
    Gnum vertnum;
    Gnum vertnnd;

    smshptr = smshloctab + smshsndidx;

    for (vertnum = baseval, vertnnd = smshptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
      Gnum ndstlocidx;
      Gnum ndstlocid2;
      Gnum * tmpptr;
      int                 procngbnum;
      Gnum edgenum;
      Gnum edgennd;

      procngbnum = procmshtab[smshsndidx][vertnum];

      if (vertsidtab[procngbnum] != smshsndidx) {
        Gnum ndstlocnnd;

        vertsidtab[procngbnum] = smshsndidx;
        smshsndtab[smshsndidx] = smshptr->idnum;
        datasndtab[dsndcnttab[procngbnum] ++] = - smshptr->idnum - 1;
        tmpptr = &datasndtab[dsndcnttab[procngbnum] ++];
        for (ndstlocidx = 0, ndstlocnnd = ndstsndtb2[smshsndidx]; ndstlocidx < ndstlocnnd && ndstsndtab[smshsndidx][ndstlocidx].procnum < procngbnum; ndstlocidx ++);

        for (ndstlocid2 = ndstlocidx, ndstlocnnd = ndstsndtb2[smshsndidx]; ndstlocidx < ndstlocnnd && ndstsndtab[smshsndidx][ndstlocidx].procnum == procngbnum; ndstlocidx ++) {
          datasndtab[dsndcnttab[procngbnum] ++] = ndstsndtab[smshsndidx][ndstlocidx].nghbnum;
          datasndtab[dsndcnttab[procngbnum] ++] = procmshtab[smshsndidx][ndstsndtab[smshsndidx][ndstlocidx].nghbnum];
        }
        *tmpptr = ndstlocidx - ndstlocid2;
      }

      datasndtab[dsndcnttab[procngbnum] + vertdsp] = vertnum;
      datasndtab[dsndcnttab[procngbnum] + mvrtdsp] = vflgtab[smshsndidx][vertnum];
      datasndtab[dsndcnttab[procngbnum] + enttdsp] = smshptr->venttax[vertnum];
      dsndcnttab[procngbnum] += vertlocinc;
      datasndtab[dsndcnttab[procngbnum] ++] = smshptr->vendtax[vertnum] - smshptr->verttax[vertnum];

      for (edgenum = smshptr->verttax[vertnum], edgennd = smshptr->vendtax[vertnum]; edgenum < edgennd; edgenum ++) { 
        Gnum nghblocnum;

        nghblocnum = smshptr->edgetax[edgenum];

        if (procmshtab[smshsndidx][nghblocnum] == procngbnum) 
          datasndtab[dsndcnttab[procngbnum] + nghbdsp] = smshptr->edgetax[edgenum];
        else {
          datasndtab[dsndcnttab[procngbnum] + nghbdsp] = - smshptr->edgetax[edgenum] - 1;
        }
        if (smshptr->edlotax != NULL)
          datasndtab[dsndcnttab[procngbnum] + edlodsp] = smshptr->edlotax[edgenum];
        dsndcnttab[procngbnum] += nghblocinc;
      }
    }
  }

  for(smshlocnum = 0; smshlocnum < meshnbr; smshlocnum ++) {
    memFree (ndstsndtab[smshlocnum]);
    memFree (procmshtab[smshlocnum]);
  }

  for (procngbnum = 0 ; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
  }
  END_TIME(cnt1);

  //* envoyer datasndtab et recevoir datarcvtab
  CHECK_FMPI (cheklocval, commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, dsmshdat.proccomm), dsmshdat.proccomm);

  //
  vextlocnbr = 0;
  vintlocnbr = 0;
  vbndlocnbr = 0;
  dstedgelocnbr = dsmshdat.edgelocnbr;
  // - Pour chaque sommet «s» du maillage distribué
  //   - si sommet interne à une CC (utiliser le tableau attaché au maillage distribué pour chaque sommet contenant -1 (PAMPA_TAG_VERT_EXTERNAL) si pas dans CC, et 2 autres valeurs interne à une CC, bord d'une CC)
  //     - vintlocnbr ++
  //   - si sommet bord à une CC
  //     - vbndlocnbr ++

  START_TIME(cnt1);
  for (srcvertlocnum = dsmshdat.baseval, srcvertlocnnd = dsmshdat.vertlocnbr + baseval; srcvertlocnum < srcvertlocnnd; srcvertlocnum ++)
    if (vflgloctax[srcvertlocnum] == PAMPA_TAG_VERT_INTERNAL) // XXX peut-être changer le drapeau
      vintlocnbr ++;
    else if (vflgloctax[srcvertlocnum] == PAMPA_TAG_VERT_FRONTIER)
      vbndlocnbr ++;

  // - allouer dsndcnttb2, dsnddsptb2, drcvcnttb2 et drcvdsptb2 (tb2 car tab va être réutilisé pour les VA)
  if (memAllocGroup ((void **) (void *)
        &dsndcnttb2, (size_t) (dsmshdat.procglbnbr       * sizeof (int)),
        &dsnddsptb2, (size_t) ((dsmshdat.procglbnbr + 1) * sizeof (int)),
        &drcvcnttb2, (size_t) (dsmshdat.procglbnbr       * sizeof (int)),
        &drcvdsptb2, (size_t) ((dsmshdat.procglbnbr + 1) * sizeof (int)),
        &vmshcnttax, (size_t) (dsmshdat.vertlocnbr       * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);
  vmshcnttax -= baseval;

  memset (dsndcnttb2, 0, dsmshdat.procglbnbr * sizeof (int));
  memset (vmshcnttax + baseval, 0, dsmshdat.vertlocnbr * sizeof (Gnum));

  vextlocnbr = dsmshdat.vertlocnbr - vbndlocnbr - vintlocnbr;


  // - Parcourir datarcvtab
  //   - si valeur négative (changement de maillage centralisé)
  //     - smshrcvnbr ++     // Autre manière pour avoir le nombre de maillage centralisé serait d'ajouter une valeur soit lors du alltoall soit lors du alltoallv
  //   - pour chaque sommet «s»
  //     - ajouter à dsndcnttb2[procngbnum] 2 * nb voisins distants (procngbnum étant donné pour chaque voisin distant)
  memSet (vertsidtab, ~0, dsmshdat.procglbnbr * sizeof (Gnum));

#ifdef PAMPA_DEBUG_DMESH2
  if (datarcvnbr > 0)
    if (datarcvtab[0] > 0) {
      errorPrint ("Not negative value at the beginning");
      cheklocval = 1;
    }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
  for (smshrcvnbr = datarcvidx = 0; datarcvidx < datarcvnbr; )
    if (datarcvtab[datarcvidx] < 0) {
      datarcvidx ++;
      smshrcvnbr ++;

      for (datarcvnnd = datarcvidx + datarcvtab[datarcvidx] * ndstlocinc, datarcvidx ++;
          datarcvidx <= datarcvnnd; datarcvidx += ndstlocinc) {
        Gnum procngbnum;

        procngbnum = datarcvtab[datarcvidx + procdsp]; 
        if (vertsidtab[procngbnum] != smshrcvnbr) {
          vertsidtab[procngbnum] = smshrcvnbr;
          dsndcnttb2[procngbnum] ++; /* Number of Smesh must be sent */
        }
        dsndcnttb2[procngbnum] ++;
      }
    }
    else {
      Gnum mvrtnum;

      mvrtnum = datarcvtab[datarcvidx + mvrtdsp];

      if (mvrtnum != PAMPA_TAG_VERT_INTERNAL) {
        if (orgvprmloctax != NULL) {
          Gnum vertlocidx;
          Gnum vertlocmax;

          /* Search the vertex number which have this old global number */
          for (vertlocidx = baseval, vertlocmax = dsmshdat.vertlocnbr + baseval;
              vertlocmax - vertlocidx > 1; ) {
            Gnum                 vertlocmed;

            vertlocmed = (vertlocmax + vertlocidx) / 2;
            if (orgvprmloctax[vertlocmed] <= mvrtnum)
              vertlocidx = vertlocmed;
            else
              vertlocmax = vertlocmed;
          }
#ifdef PAMPA_DEBUG_DMESH2
          if (orgvprmloctax[vertlocidx] != mvrtnum) {
            errorPrint ("Vertex number %d not found", mvrtnum);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

          mvrtnum =
            datarcvtab[datarcvidx + mvrtdsp] =
            vertlocidx + srcvertlocbas;
          vmshcnttax[vertlocidx] += 2;
        }
        else {
#ifdef PAMPA_DEBUG_DMESH2
          if (((mvrtnum - srcvertlocbas) < baseval) || ((mvrtnum - srcvertlocbas) >= (dsmshdat.vertlocnbr + baseval))) {
            errorPrint ("Vertex number %d not correct", mvrtnum);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
          vmshcnttax[mvrtnum - srcvertlocbas] += 2; // invalid read of size 4
        }
      }

      datarcvidx += vertlocinc;
      datarcvidx += datarcvtab[datarcvidx] * nghblocinc + 1; /* Displacement for neighbors */

    }

  /* Compute displacement sending array */
  dsnddsptb2[0] = 0;
  for (procngbnum = 1 ; procngbnum < dsmshdat.procglbnbr + 1; procngbnum ++) {
    dsnddsptb2[procngbnum] = dsnddsptb2[procngbnum - 1] + dsndcnttb2[procngbnum - 1];
  }

  datasndnb2 = dsnddsptb2[dsmshdat.procglbnbr];

  //* envoyer dsndcnttb2 et recevoir drcvcnttb2
  CHECK_FMPI (cheklocval, commAlltoall(dsndcnttb2, 1, MPI_INT, drcvcnttb2, 1, MPI_INT, dsmshdat.proccomm), dsmshdat.proccomm);

  //* en déduire ddsp{snd,rcv}tb2
  /* Compute displacement receiving array */
  drcvdsptb2[0] = 0;
  for (procngbnum = 1 ; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    drcvdsptb2[procngbnum] = drcvdsptb2[procngbnum - 1] + drcvcnttb2[procngbnum - 1];
    dsndcnttb2[procngbnum] = dsnddsptb2[procngbnum];
  }
  drcvdsptb2[dsmshdat.procglbnbr] = drcvdsptb2[dsmshdat.procglbnbr - 1] + drcvcnttb2[dsmshdat.procglbnbr - 1];
  datarcvnb2 = drcvdsptb2[dsmshdat.procglbnbr];
  dsndcnttb2[0] = dsnddsptb2[0];

  // - allouer smshrcvtab de taille smshrcvnbr qui contient l'identifiant du Smesh et l'indice de début dans datarcvtab
  // - allouer smshrcvtb2 de taille smshrcvnbr qui contient l'indice de début du Smesh dans datarctb2
  // - allouer vintloctab de taille smshrcvnbr qui contient le nombre de sommets internes pour chaque Smesh
  // - allouer datasndtb2, datarcvtb2, datasndtb3 et datarcvtb3 (ces 2 derniers n'auront pas les numéros des Smesh)
  for (vertcntnbr = 0, srcvertlocnum = baseval, srcvertlocnnd = dsmshdat.vertlocnbr + baseval;
      srcvertlocnum < srcvertlocnnd; srcvertlocnum ++)
    vertcntnbr += vmshcnttax[srcvertlocnum];

  if (memAllocGroup ((void **) (void *)
        &smshrcvtab, (size_t) (smshrcvnbr                 * sizeof (SmeshRcv)),
        &smshrcvtb2, (size_t) (smshrcvnbr                 * sizeof (NdstAdj)),
        &vintloctab, (size_t) (smshrcvnbr                 * sizeof (Gnum)),
        &datasndtb2, (size_t) (datasndnb2                 * sizeof (Gnum)),
        // FIXME dsndloctab pointe sur datarcvtb2, doit-on allouer deux fois la
        // taille pour datarcvtb2 obligatoirement ???
        &datarcvtb2, (size_t) ((datarcvnb2 + datasndnb2) * sizeof (Gnum)), // XXX ne peut-on pas faire le max ?
        &datarcvtb3, (size_t) (datasndnb2                 * sizeof (Gnum)), // TRICK: Second send, so size is the same as datasndtb2
        &ndstrcvtab, (size_t) (datasndnb2                 * sizeof (PermNdst)), // XXX la taille est trop grande, il faut diminuer les autres avant 
        /* dans le commit c3896a3eaf5 il y avait une optim qui ne marchait pas à tout les coups en reprenant les tableaux ci-dessus */
        &vmshdsptax, (size_t) ((dsmshdat.vertlocnbr + 1) * sizeof (Gnum)),
        &vmshloctab, (size_t) (vertcntnbr                * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);
  vmshdsptax -= baseval;


  datasndtb3 = datarcvtb2;
  memset (vintloctab, 0, smshrcvnbr * sizeof (Gnum));
  memset (smshrcvtb2, 0, smshrcvnbr * sizeof (NdstAdj));

  vmshdsptax[baseval] = 0;
  for (srcvertlocnum = baseval, srcvertlocnnd = dsmshdat.vertlocnbr + baseval;
      srcvertlocnum < srcvertlocnnd; srcvertlocnum ++) {
    vmshdsptax[srcvertlocnum + 1] = vmshdsptax[srcvertlocnum] + vmshcnttax[srcvertlocnum];
    vmshcnttax[srcvertlocnum] = vmshdsptax[srcvertlocnum];
  }

  // - Parcourir datarcvtab
  //   - si valeur négative
  //     - mettre l'id du Smesh dans smshrcvtab
  //     - mettre l'indice de datarcvtab dans smshrcvtab
  for (smshrcvidx = datarcvidx = 0; datarcvidx < datarcvnbr; )
    if (datarcvtab[datarcvidx] < 0) {
      smshrcvtab[smshrcvidx].vertmin = INTVALMAX;
      smshrcvtab[smshrcvidx].vertmax = 0;
      smshrcvtab[smshrcvidx].smshnum = - datarcvtab[datarcvidx] - 1;
      datarcvidx ++;
      datarcvidx += datarcvtab[datarcvidx] * ndstlocinc + 1;
      smshrcvtab[smshrcvidx ++].dataidx = datarcvidx;
    }
    else {
      smshrcvtab[smshrcvidx - 1].vertmin = MIN(datarcvtab[datarcvidx + vertdsp], smshrcvtab[smshrcvidx - 1].vertmin);
      smshrcvtab[smshrcvidx - 1].vertmax = MAX(datarcvtab[datarcvidx + vertdsp], smshrcvtab[smshrcvidx - 1].vertmax);
      datarcvidx += vertlocinc;
      datarcvidx += datarcvtab[datarcvidx] * nghblocinc + 1; /* Displacement for neighbors */
    }

  // - trier smshrcvtab
  intSort4asc1 (smshrcvtab, smshrcvnbr);

  // - parcourir datarcvtab
  //   - si sommet «s»
  //     - si mvrttax[s] == -1 (mvrttax[s] : valeur suivante dans datarcvtab)
  //       - vintloctab[smshrcvnum] ++
  //     - sinon : sommet de bord
  //       - ajouter à hashtab la paire (mvrttax[s], smshlocnum)
  //   - pour chaque voisin «v» étant dans la catégorie distant
  //     - s'il n'y est pas déjà
  //       - ajouter à datasndtb2 l'id du Smesh (avec smshrcvtab) et le numéro du sommet pour le bon proc (celui-ci étant mis dans datarcvtab)
  //     - ajouter le numéro du sommet pour le bon proc (celui-ci étant mis dans datarcvtab)
  //   - ajouter à edgelocnbr le nb de sommets distants et locaux  // XXX voir si cela n'est pas trop grand

  memSet (vertsidtab, ~0, dsmshdat.procglbnbr * sizeof (Gnum));

  for (smshrcvidx = -1, datarcvidx = 0; datarcvidx < datarcvnbr; )
    if (datarcvtab[datarcvidx] < 0) {
      Gnum smshrcvmax;

      smshrcvnum = - datarcvtab[datarcvidx] - 1;

      /* Search the index of Smesh which have current number */
      for (smshrcvidx = 0, smshrcvmax = smshrcvnbr;
          smshrcvmax - smshrcvidx > 1; ) {
        Gnum                 smshrcvmed;

        smshrcvmed = (smshrcvmax + smshrcvidx) / 2;
        if (smshrcvtab[smshrcvmed].smshnum <= smshrcvnum)
          smshrcvidx = smshrcvmed;
        else
          smshrcvmax = smshrcvmed;
      }
#ifdef PAMPA_DEBUG_DMESH2
      if (smshrcvtab[smshrcvidx].smshnum != smshrcvnum) {
        errorPrint ("Smesh number %d not found", smshrcvnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

      datarcvidx ++;
      for (datarcvnnd = datarcvidx + datarcvtab[datarcvidx] * ndstlocinc, datarcvidx ++;
          datarcvidx <= datarcvnnd; datarcvidx += ndstlocinc) {
        Gnum procngbnum;

        procngbnum = datarcvtab[datarcvidx + procdsp]; 
        if (vertsidtab[procngbnum] != smshrcvidx) {
          vertsidtab[procngbnum] = smshrcvidx;
          datasndtb2[dsndcnttb2[procngbnum] ++] = - smshrcvnum - 1; /* Number of Smesh must be sent */
        }
        datasndtb2[dsndcnttb2[procngbnum] ++] = datarcvtab[datarcvidx + nghbdsp];
        smshrcvtb2[smshrcvidx].indxnnd ++;
      }
    }
    else {
      Gnum mvrtnum;

      mvrtnum = datarcvtab[datarcvidx + mvrtdsp];
      if (mvrtnum != PAMPA_TAG_VERT_INTERNAL) {
        vmshloctab[vmshcnttax[mvrtnum - srcvertlocbas] ++] = smshrcvidx;
        vmshloctab[vmshcnttax[mvrtnum - srcvertlocbas] ++] = datarcvidx;
      }


      if (mvrtnum == PAMPA_TAG_VERT_INTERNAL)
        vintloctab[smshrcvidx] ++;

      datarcvidx += vertlocinc;
      dstedgelocnbr += datarcvtab[datarcvidx]; /* Adding neighbors */
      datarcvidx += datarcvtab[datarcvidx] * nghblocinc + 1; /* Displacement for neighbors */
    }

  for (procngbnum = 0 ; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    dsndcnttb2[procngbnum] -= dsnddsptb2[procngbnum];
  }


  // - échanger datasndtb2 et datarcvtb2
  CHECK_FMPI (cheklocval, commAlltoallv(datasndtb2, dsndcnttb2, dsnddsptb2, GNUM_MPI, datarcvtb2, drcvcnttb2, drcvdsptb2, GNUM_MPI, dsmshdat.proccomm), dsmshdat.proccomm);
  CHECK_VDBG (cheklocval, dsmshdat.proccomm);

  // XXX ajouter les «ref» sur chaque super-entité et après voir pourquoir le
  // vertlocnbr est diminué de 1 sur chaque proc (cela est peut-être lié…)

  // - vertlocnbr = vextlocnbr + vbndlocnbr + somme pr ts les maillages centralisés de (vintloctab)
  dstvertlocnbr = vextlocnbr + vbndlocnbr;
  for (smshrcvidx = 0; smshrcvidx < smshrcvnbr; smshrcvidx ++)
    dstvertlocnbr += vintloctab[smshrcvidx];
  dstvertlocnbr = MAX(dsmshdat.vertlocnbr, dstvertlocnbr);

  dstedlolocnbr = (dsmshdat.edloloctax == NULL) ? 0 : dsmshdat.edgelocsiz + dstedgelocnbr - dsmshdat.edgelocnbr; // XXX est-ce bon ?? edgelocnbr n'est-il pas suffisant dans ce cas ?

  // - allouer vprmgsttax de taille vertlocnbr
  // - allouer vprmmshtab de taille :
  //   - 1) smshrcvnbr
  // - allouer vertloctax de taille vertlocnbr
  // - allouer edgeloctax de taille edgelocsiz + edgelocnbr - dsmshdat.edgelocnbr   // XXX est-ce correct et suffisant ??
  dstedgelocsiz = dsmshdat.edgelocsiz - dsmshdat.edgelocnbr + dstedgelocnbr;
  if (memAllocGroup ((void **) (void *)
        &vprmgsttax, (size_t) (dsmshdat.vgstlocnbr * sizeof (Gnum)),
        &vswploctab, (size_t) (dsmshdat.vertlocnbr * sizeof (Gnum)),
        &vprmmshtab, (size_t) (smshrcvnbr * sizeof (PermVert *)),
        &evrttax,    (size_t) (vertmax * sizeof (Gnum)),
        &svrttax,    (size_t) (vertmax * sizeof (Gnum)),
        &enbrloctax, (size_t) (dsmshdat.enttglbnbr * sizeof (Gnum)),
        &dstvertloctax, (size_t) (dstvertlocnbr * sizeof (Gnum)),
        &dstvendloctax, (size_t) (dstvertlocnbr * sizeof (Gnum)),
        &dstventloctax, (size_t) (dstvertlocnbr * sizeof (Gnum)),
        &dstedgeloctax, (size_t) (dstedgelocsiz * sizeof (Gnum)), // XXX est-ce suffisant ??
        &dstedloloctax, (size_t) (dstedlolocnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  evrttax -= baseval;
  svrttax -= baseval;
  vprmgsttax -= baseval;
  dstvertloctax -= baseval;
  dstvendloctax -= baseval;
  dstventloctax -= baseval;
  dstedgeloctax -= baseval;
  enbrloctax -= baseval;
  dstedloloctax = (dstedlolocnbr == 0) ? NULL : dstedloloctax - baseval;

  memSet (dstventloctax + baseval, ~0, dstvertlocnbr * sizeof (Gnum));
  memSet (dstvertloctax + baseval, 0, dstvertlocnbr * sizeof (Gnum));
  memSet (dstvendloctax + baseval, 0, dstvertlocnbr * sizeof (Gnum));
  memSet (dstedgeloctax + baseval, ~0, dstedgelocsiz * sizeof (Gnum));
  memSet (vprmgsttax, ~0, dsmshdat.vertlocnbr * sizeof (Gnum));


  // - initialiser vprmmshtab pour chaque smshlocnum à -1
  for (smshrcvidx = 0; smshrcvidx < smshrcvnbr; smshrcvidx ++) {
    if ((vprmmshtab[smshrcvidx] = (Gnum *) memAlloc ((smshrcvtab[smshrcvidx].vertmax - smshrcvtab[smshrcvidx].vertmin + 1) * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);

    vprmmshtab[smshrcvidx] -= smshrcvtab[smshrcvidx].vertmin;

    memSet (vprmmshtab[smshrcvidx] + smshrcvtab[smshrcvidx].vertmin, ~0, (smshrcvtab[smshrcvidx].vertmax - smshrcvtab[smshrcvidx].vertmin + 1) * sizeof (Gnum));
  }

  // vertlocidx = baseval
  //
  // - pour chaque sommet local du maillage distribué
  //   - si «s» n'appartient pas à une CC ou «s» est sur le bord d'une CC
  //     - vprmloctab[s] = vertlocidx ++

  //memFreeGroup (dmshptr->procdsptab);
  (*dmshptr->procloccnt) --;
  dmshptr->procdsptab = NULL;
  CHECK_FERR (dmeshBuild2 (dmshptr, baseval, dstvertlocnbr, dstvertlocnbr), dsmshdat.proccomm); // XXX vertlocnbr à la place de vertlocmax ??
  // XXX il faudrait que dmeshBuild2 ne soit pas rappelé dans dmeshBuild du coup

  const Gnum dstvertlocbas = dmshptr->procvrttab[dmshptr->proclocnum] - baseval;
  vswploccur =
  vswplocnbr = 0;

  for (srcvertlocnum = baseval, srcvertlocnnd = dsmshdat.vertlocnbr + baseval; srcvertlocnum < srcvertlocnnd; srcvertlocnum ++)
    if ((vflgloctax[srcvertlocnum] == PAMPA_TAG_VERT_EXTERNAL) || (vflgloctax[srcvertlocnum] == PAMPA_TAG_VERT_FRONTIER))
      vprmgsttax[srcvertlocnum] = srcvertlocnum + dstvertlocbas;
    else
      vswploctab[vswplocnbr ++] = srcvertlocnum + dstvertlocbas;
#ifdef PAMPA_DEBUG_DMESH2
  if (dstvertlocnbr + vswplocnbr - baseval < dsmshdat.vertlocnbr) {
    errorPrint ("internal error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
  END_TIME(cnt1);

  START_TIME(cnt1);
  for (srcvertlocnum = dsmshdat.vertlocnbr + baseval - 1, srcvertlocnnd = baseval - 1; srcvertlocnum > srcvertlocnnd && vswploccur + dstvertlocnbr < dsmshdat.vertlocnbr; srcvertlocnum --) 
    if (vprmgsttax[srcvertlocnum] != ~0)
      vprmgsttax[srcvertlocnum] = vswploctab[vswploccur ++];

#ifdef PAMPA_DEBUG_DMESH2
  for (srcvertlocnum = baseval, srcvertlocnnd = dsmshdat.vertlocnbr + baseval; srcvertlocnum < srcvertlocnnd; srcvertlocnum ++)
      if ((vprmgsttax[srcvertlocnum] != ~0) && ((vprmgsttax[srcvertlocnum] < dmshptr->procvrttab[dmshptr->proclocnum])
          || (vprmgsttax[srcvertlocnum] >= dmshptr->procvrttab[dmshptr->proclocnum + 1]))) {
        errorPrint ("Global vertex number is incorrect for Smesh %d and vertex %d", smshrcvidx, srcvertlocnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

  CHECK_FERR (dsmeshHaloSyncComm (&dsmshdat, dsmshdat.commlocptr, dsmshdat.vertlocnbr, vprmgsttax, GNUM_MPI), dsmshdat.proccomm);

  vertloccur = dmshptr->procvrttab[dmshptr->proclocnum] + dsmshdat.vertlocnbr;

  // - parcourir datarcvtab
  //   - pour chaque sommet «s»
  //     - si mvrttax[s] == -1
  //       - vprmmshtab[smshlocnum][s] = vertloccur ++
  //     - sinon
  //       - vprmmshtab[smshlocnum][s] = vprmgsttax[mvrttax[s]]
  for (smshrcvidx = -1, datarcvidx = 0; datarcvidx < datarcvnbr; )
    if (datarcvtab[datarcvidx] < 0) {
      Gnum smshrcvmax;

      smshrcvnum = - datarcvtab[datarcvidx] - 1;
      datarcvidx ++;
      datarcvidx += datarcvtab[datarcvidx] * ndstlocinc + 1;

      /* Search the index of Smesh which have current number */
      for (smshrcvidx = 0, smshrcvmax = smshrcvnbr;
          smshrcvmax - smshrcvidx > 1; ) {
        Gnum                 smshrcvmed;

        smshrcvmed = (smshrcvmax + smshrcvidx) / 2;
        if (smshrcvtab[smshrcvmed].smshnum <= smshrcvnum)
          smshrcvidx = smshrcvmed;
        else
          smshrcvmax = smshrcvmed;
      }
#ifdef PAMPA_DEBUG_DMESH2
      if (smshrcvtab[smshrcvidx].smshnum != smshrcvnum) {
        errorPrint ("Smesh number %d not found", smshrcvnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
    }
    else {
      Gnum vertlocnum;
      Gnum mvrtlocnum;

      vertlocnum = datarcvtab[datarcvidx + vertdsp];
#ifdef PAMPA_DEBUG_DMESH2
      if ((vertlocnum < smshrcvtab[smshrcvidx].vertmin)
          || (vertlocnum > smshrcvtab[smshrcvidx].vertmax)) {
        errorPrint ("vertlocnum %d not in [%d,%d] for Smesh %d", vertlocnum, smshrcvtab[smshrcvidx].vertmin, smshrcvtab[smshrcvidx].vertmax, smshrcvidx);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
      mvrtlocnum = datarcvtab[datarcvidx + mvrtdsp];
      if ((mvrtlocnum == PAMPA_TAG_VERT_INTERNAL) || (mvrtlocnum < vertlocmin) || (mvrtlocnum > vertlocmax))
        if (vswploccur < vswplocnbr)
          vprmmshtab[smshrcvidx][vertlocnum] = vswploctab[vswploccur ++];
        else
          vprmmshtab[smshrcvidx][vertlocnum] = vertloccur ++;
      else 
        vprmmshtab[smshrcvidx][vertlocnum] = DSTVERTLOCNUM(mvrtlocnum - srcvertlocbas);
#ifdef PAMPA_DEBUG_DMESH2
      if ((vprmmshtab[smshrcvidx][vertlocnum] < dmshptr->procvrttab[dmshptr->proclocnum])
          || (vprmmshtab[smshrcvidx][vertlocnum] >= dmshptr->procvrttab[dmshptr->proclocnum + 1])) {
        errorPrint ("Global vertex number is incorrect for Smesh %d and vertex %d", smshrcvidx, vertlocnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

      datarcvidx += vertlocinc;
      datarcvidx += datarcvtab[datarcvidx] * nghblocinc + 1; /* Displacement for neighbors */
    }

  // - parcourir datarcvtb2
  //   - pour chaque sommet s
  //     - remplir datasndtb3 avec vprmmshtab[smshlocnum][s] (smshlocnum et s étant tous deux dans datarcvtb2)
  //   - supprimer les id des Smesh en décalant les valeurs dans datasndtb2

  for (procngbnum =0; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    Gnum datasndnnd;
#ifdef PAMPA_DEBUG_DMESH2
    if (dsndcnttb2[procngbnum] > 0)
      if (datasndtb2[dsnddsptb2[procngbnum]] > 0) {
        errorPrint ("Not negative value at the beginning for processor %d", procngbnum);
        cheklocval = 1;
      }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);
    if (drcvcnttb2[procngbnum] > 0)
      if (datarcvtb2[drcvdsptb2[procngbnum]] > 0) {
        errorPrint ("Not negative value at the beginning for processor %d", procngbnum);
        cheklocval = 1;
      }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

    for (datasndidx = dsnddsptb2[procngbnum], datasndnnd = dsnddsptb2[procngbnum + 1]; datasndidx < datasndnnd; datasndidx ++)
      if (datasndtb2[datasndidx] < 0)
        dsndcnttb2[procngbnum] --;
  }

  tmpptr = dsndcnttb2; /* Swapping sending and receiving count arrays */
  dsndcnttb2 = drcvcnttb2;
  drcvcnttb2 = tmpptr;

  tmpptr = dsnddsptb2; /* Swapping sending and receiving displacement arrays */
  dsnddsptb2 = drcvdsptb2;
  drcvdsptb2 = tmpptr;

  for (datasndid2 = procngbnum =0; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    Gnum datasndnnd;


    for (datasndidx = dsnddsptb2[procngbnum], datasndnnd = dsnddsptb2[procngbnum + 1]; datasndidx < datasndnnd; datasndidx ++)
      if (datarcvtb2[datasndidx] < 0) {
        Gnum smshsndmax;
        Gnum smshsndnum;

        dsndcnttb2[procngbnum] --;
        smshsndnum = - datarcvtb2[datasndidx] - 1;

        /* Search the index of Smesh which have current number */
        for (smshsndidx = 0, smshsndmax = smshrcvnbr;
            smshsndmax - smshsndidx > 1; ) {
          Gnum                 smshsndmed;

          smshsndmed = (smshsndmax + smshsndidx) / 2;
          if (smshrcvtab[smshsndmed].smshnum <= smshsndnum)
            smshsndidx = smshsndmed;
          else
            smshsndmax = smshsndmed;
        }
#ifdef PAMPA_DEBUG_DMESH2
        if (smshrcvtab[smshsndidx].smshnum != smshsndnum) {
          errorPrint ("Smesh number %d not found", smshsndnum);
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
      }
      else {
        Gnum vertsndnum;
        Gnum vertsndidx;
        Gnum vertsndmax;

        vertsndnum = datarcvtb2[datasndidx];
#ifdef PAMPA_DEBUG_DMESH2
      if ((vertsndnum < smshrcvtab[smshsndidx].vertmin)
          || (vertsndnum > smshrcvtab[smshsndidx].vertmax)) {
        errorPrint ("vertsndnum %d not in [%d,%d] for Smesh %d", vertsndnum, smshrcvtab[smshsndidx].vertmin, smshrcvtab[smshsndidx].vertmax, smshsndidx);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

        datasndtb3[datasndid2 ++] = vprmmshtab[smshsndidx][vertsndnum];
      }
  }

  /* Compute displacement sending array */
  /* Compute displacement receiving array */
  dsnddsptb2[0] = 0;
  drcvdsptb2[0] = 0;
  for (procngbnum = 1 ; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
    dsnddsptb2[procngbnum] = dsnddsptb2[procngbnum - 1] + dsndcnttb2[procngbnum - 1];
    drcvdsptb2[procngbnum] = drcvdsptb2[procngbnum - 1] + drcvcnttb2[procngbnum - 1];
  }
  dsnddsptb2[procngbnum] = dsnddsptb2[procngbnum - 1] + dsndcnttb2[procngbnum - 1];
  drcvdsptb2[procngbnum] = drcvdsptb2[procngbnum - 1] + drcvcnttb2[procngbnum - 1];

  CHECK_FMPI (cheklocval, commAlltoallv(datasndtb3, dsndcnttb2, dsnddsptb2, GNUM_MPI, datarcvtb3, drcvcnttb2, drcvdsptb2, GNUM_MPI, dsmshdat.proccomm), dsmshdat.proccomm);
  CHECK_VDBG (cheklocval, dsmshdat.proccomm);

  memFreeGroup (drcvcnttb2); /* Not dsndcnttb2 because dsndcnttb2 and drcvcnttb2 are swapped */

  // il faut parcourir datasndtb2 pour qu'on puisse faire la recherche entre datasndtb2 et datarcvtb3

  for (smshrcvidx = 1; smshrcvidx < smshrcvnbr; smshrcvidx ++) {
    smshrcvtb2[smshrcvidx].indxnum = smshrcvtb2[smshrcvidx - 1].indxnnd;
    smshrcvtb2[smshrcvidx].indxnnd += smshrcvtb2[smshrcvidx].indxnum;
    smshrcvtb2[smshrcvidx - 1].indxnnd = smshrcvtb2[smshrcvidx - 1].indxnum;
  }
  if (smshrcvnbr > 0)
    smshrcvtb2[smshrcvnbr - 1].indxnnd = smshrcvtb2[smshrcvnbr - 1].indxnum;

  for (smshrcvidx = -1, datarcvidx = datasndidx = 0; datasndidx < datasndnb2; datasndidx ++) {
    if (datasndtb2[datasndidx] < 0) {
      Gnum smshrcvmax;

      smshrcvnum = - datasndtb2[datasndidx] - 1;

      /* Search the index of Smesh which have current number */
      for (smshrcvidx = 0, smshrcvmax = smshrcvnbr;
          smshrcvmax - smshrcvidx > 1; ) {
        Gnum                 smshrcvmed;

        smshrcvmed = (smshrcvmax + smshrcvidx) / 2;
        if (smshrcvtab[smshrcvmed].smshnum <= smshrcvnum)
          smshrcvidx = smshrcvmed;
        else
          smshrcvmax = smshrcvmed;
      }
#ifdef PAMPA_DEBUG_DMESH2
      if (smshrcvtab[smshrcvidx].smshnum != smshrcvnum) {
        errorPrint ("Smesh number %d not found", smshrcvnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
    }
    else {
      ndstrcvtab[smshrcvtb2[smshrcvidx].indxnnd].nghbnum = datasndtb2[datasndidx];
      ndstrcvtab[smshrcvtb2[smshrcvidx].indxnnd ++].nprmnum = datarcvtb3[datarcvidx ++];
    }
  }

  for (smshrcvidx = 0; smshrcvidx < smshrcvnbr; smshrcvidx ++)
    intSort2asc1 (ndstrcvtab + smshrcvtb2[smshrcvidx].indxnum, (smshrcvtb2[smshrcvidx].indxnnd - smshrcvtb2[smshrcvidx].indxnum));
  END_TIME(cnt1);


  // - pour chaque sommet «s» du maillage distribué
  //   - si «s» n'appartient pas à une CC
  //     - ajouter tous les voisins «v» de «s» dans la edgeloctax
  //   - si «s» est de bord
  //     - ajouter tous les voisins «v» qui n'appartiennent pas à une CC ou qui sont de bord d'une CC (utiliser le tableau de drapeaux qui est sur le dsmshdat)
  //       - en utilisant vprmgsttax
  //     - parcourir la section de la hashtab susceptible de contenir «s»
  //       - pour chaque id de Smesh présent avec «s»
  //         - trouver le smshlocnum avec smshloctab
  //         - pour tous les voisins internes (qui ne sont pas de bord) «v» de «s» dans smshlocnum
  //           - ajouter «v» s'il n'est pas déjà présent dans la liste d'adjacence (utiliser un pointeur temporaire sur le début d'adjacence de «s»)
  //             - en utilisant vprmmshtab si ≠ -1
  //             - sinon utiliser datasndtb2 pour chercher l'indice «i» et prendre la
  //             valeur dans datarcvtb3 à l'indice «i» (utiliser dsndcnttb2)
  srcvertlocbas -= baseval;
  START_TIME(cnt1);;
  for (edgelocidx = srcvertlocnum = baseval, srcvertlocnnd = dsmshdat.vertlocnbr + baseval; srcvertlocnum < srcvertlocnnd; srcvertlocnum ++) {
    // XXX peut-être utiliser des tableaux avec restrict pour dsmshdat.{vert,vend,edge}loctax
    Gnum dstmvrtlocnum;
    Gnum dstbvrtlocnum;
    Gnum srcmvrtlocnum;
    Gnum edgelocnum;
    Gnum edgelocnnd;

    srcmvrtlocnum = srcvertlocnum + srcvertlocbas;
    dstmvrtlocnum = srcvertlocnum + dstvertlocbas;
    dstbvrtlocnum = dstmvrtlocnum - dstvertlocbas;
    if (vflgloctax[srcvertlocnum] == PAMPA_TAG_VERT_EXTERNAL) { /* If external vertex */
      Gnum edgelocid2;
      Gnum edgelocnn2;

      dstventloctax[dstbvrtlocnum] = dsmshdat.ventloctax[srcvertlocnum];
      dstvertloctax[dstbvrtlocnum] = edgelocidx;
      if (dstedloloctax != NULL)
        memCpy (dstedloloctax + edgelocidx, dsmshdat.edloloctax + dsmshdat.vertloctax[srcvertlocnum], (dsmshdat.vendloctax[srcvertlocnum] - dsmshdat.vertloctax[srcvertlocnum]) * sizeof (Gnum));
      for (edgelocid2 = dsmshdat.vertloctax[srcvertlocnum], edgelocnn2 = dsmshdat.vendloctax[srcvertlocnum]; edgelocid2 < edgelocnn2; edgelocid2 ++)
        dstedgeloctax[edgelocidx ++] = DSTVERTLOCNUM(dsmshdat.edgegsttax[edgelocid2]);
      dstvendloctax[dstbvrtlocnum] = edgelocidx;
    }
    else if (vflgloctax[srcvertlocnum] == PAMPA_TAG_VERT_FRONTIER) { /* If frontier vertex */
      Gnum vmshlocidx;
      Gnum vmshlocnnd;

      dstventloctax[dstbvrtlocnum] = dsmshdat.ventloctax[srcvertlocnum];
      dstvertloctax[dstbvrtlocnum] = edgelocidx;

      for (edgelocnum = dsmshdat.vertloctax[srcvertlocnum], edgelocnnd = dsmshdat.vendloctax[srcvertlocnum]; edgelocnum < edgelocnnd; edgelocnum ++) {
        Gnum nghblocnum;

        nghblocnum = dsmshdat.edgegsttax[edgelocnum];
        if ((vflggsttax[nghblocnum] == PAMPA_TAG_VERT_EXTERNAL) || (vflggsttax[nghblocnum] == PAMPA_TAG_VERT_FRONTIER)) {
          //if (((vflggsttax[nghblocnum] == PAMPA_TAG_VERT_EXTERNAL) || (vflggsttax[nghblocnum] == PAMPA_TAG_VERT_FRONTIER)) || ((dsmshdat.ventloctax[nghblocnum] != baseval) && (dsmshdat.ventloctax[srcvertlocnum] == baseval))) { // FIXME : prendre en compte s'il y a des sous-entt pour baseval
          if (dstedloloctax != NULL)
            dstedloloctax[edgelocidx] = dsmshdat.edloloctax[edgelocnum];
          dstedgeloctax[edgelocidx ++] = DSTVERTLOCNUM(nghblocnum);
        }
        }

        for (vmshlocidx = vmshdsptax[srcvertlocnum], vmshlocnnd = vmshdsptax[srcvertlocnum + 1];
            vmshlocidx < vmshlocnnd; vmshlocidx += 2) {
          smshrcvidx = vmshloctab[vmshlocidx];
          datarcvidx = vmshloctab[vmshlocidx + 1] +  vertlocinc;
          for (datarcvnnd = datarcvidx + datarcvtab[datarcvidx] * nghblocinc, datarcvidx ++;
              datarcvidx <= datarcvnnd; datarcvidx += nghblocinc) {
            Gnum nghblocnum;
            Gnum nprmlocnum;

            nghblocnum = datarcvtab[datarcvidx + nghbdsp];
            if (nghblocnum < 0) {
              Gnum nghblocidx;
              Gnum nghblocmax;
              Gnum edgelocid2;

              nghblocnum = - nghblocnum - 1;

              /* Search the index of distant neighbor which have current number */
              for (nghblocidx = smshrcvtb2[smshrcvidx].indxnum, nghblocmax = smshrcvtb2[smshrcvidx].indxnnd;
                  nghblocmax - nghblocidx > 1; ) {
                Gnum                 nghblocmed;

                nghblocmed = (nghblocmax + nghblocidx) / 2;
                if (ndstrcvtab[nghblocmed].nghbnum <= nghblocnum)
                  nghblocidx = nghblocmed;
                else
                  nghblocmax = nghblocmed;
              }
              nprmlocnum = ndstrcvtab[nghblocidx].nprmnum;

#ifdef PAMPA_DEBUG_DMESH2
              if (ndstrcvtab[nghblocidx].nghbnum != nghblocnum) {
                errorPrint ("Neighbor number %d not found", nghblocnum);
                cheklocval = 1;
              }
              CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

              // faire un test pour savoir si le sommet n'est pas de bord
              for (edgelocid2 = dstvertloctax[dstbvrtlocnum]; edgelocid2 < edgelocidx && dstedgeloctax[edgelocid2] != nprmlocnum;
                  edgelocid2 ++); // XXX on pourrait utiliser une table de hachage à la place de la recherche linéaire

              if (dstedgeloctax[edgelocid2] != nprmlocnum) {
                if (dstedloloctax != NULL)
                  dstedloloctax[edgelocidx] = datarcvtab[datarcvidx + edlodsp];
                dstedgeloctax[edgelocidx ++] = nprmlocnum;
              }
            }
            else {
              Gnum nghblocidx;
              Gnum nghblocmax;
              Gnum edgelocid2;

#ifdef PAMPA_DEBUG_DMESH2
              if ((nghblocnum < smshrcvtab[smshrcvidx].vertmin)
                  || (nghblocnum > smshrcvtab[smshrcvidx].vertmax)) {
                errorPrint ("nghblocnum %d not in [%d,%d] for Smesh %d", nghblocnum, smshrcvtab[smshrcvidx].vertmin, smshrcvtab[smshrcvidx].vertmax, smshrcvidx);
                cheklocval = 1;
              }
              CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
              nprmlocnum = vprmmshtab[smshrcvidx][nghblocnum];

              // faire un test pour savoir si le sommet n'est pas de bord
              for (edgelocid2 = dstvertloctax[dstbvrtlocnum]; edgelocid2 < edgelocidx && dstedgeloctax[edgelocid2] != nprmlocnum;
                  edgelocid2 ++); // XXX on pourrait utiliser une table de hachage à la place de la recherche linéaire

              if (dstedgeloctax[edgelocid2] != nprmlocnum) {
                if (dstedloloctax != NULL)
                  dstedloloctax[edgelocidx] = datarcvtab[datarcvidx + edlodsp];
                dstedgeloctax[edgelocidx ++] = nprmlocnum;
              }
            }
          }

        }
        dstvendloctax[dstbvrtlocnum] = edgelocidx;
      }
    }
    srcvertlocbas += baseval;
    END_TIME(cnt1);;


    // - parcourir datarcvtab
    //   - pour chaque sommet «s» internes à une CC (utiliser mvrttax, valeur qui suit le numéro du sommet)
    //     - ajouter les voisins «v» de «s»
    //       - en utilisant vprmmshtab si ≠ -1
    //       - sinon utiliser datasndtb2 pour chercher l'indice «i» et prendre la
    //       valeur dans datarcvtb3 à l'indice «i» (utiliser dsndcnttb2)
    START_TIME(cnt1);
    for (datarcvidx = 0; datarcvidx < datarcvnbr; ) {

      if (datarcvtab[datarcvidx] < 0) {
        Gnum smshrcvmax;

        smshrcvnum = - datarcvtab[datarcvidx ++] - 1;
        datarcvidx += datarcvtab[datarcvidx] * ndstlocinc + 1;

        /* Search the index of Smesh which have current number */
        for (smshrcvidx = 0, smshrcvmax = smshrcvnbr;
            smshrcvmax - smshrcvidx > 1; ) {
          Gnum                 smshrcvmed;

          smshrcvmed = (smshrcvmax + smshrcvidx) / 2;
          if (smshrcvtab[smshrcvmed].smshnum <= smshrcvnum)
            smshrcvidx = smshrcvmed;
          else
            smshrcvmax = smshrcvmed;
        }
#ifdef PAMPA_DEBUG_DMESH2
        if (smshrcvtab[smshrcvidx].smshnum != smshrcvnum) {
          errorPrint ("Smesh number %d not found", smshrcvnum);
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

      }
      else if (datarcvtab[datarcvidx + mvrtdsp] == PAMPA_TAG_VERT_INTERNAL) {
        Gnum dstmvrtlocnum;
        Gnum dstbvrtlocnum;

        srcvertlocnum = datarcvtab[datarcvidx + vertdsp];
#ifdef PAMPA_DEBUG_DMESH2
      if ((srcvertlocnum < smshrcvtab[smshrcvidx].vertmin)
          || (srcvertlocnum > smshrcvtab[smshrcvidx].vertmax)) {
        errorPrint ("srcvertlocnum %d not in [%d,%d] for Smesh %d", srcvertlocnum, smshrcvtab[smshrcvidx].vertmin, smshrcvtab[smshrcvidx].vertmax, smshrcvidx);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
        dstmvrtlocnum = vprmmshtab[smshrcvidx][srcvertlocnum];
        dstbvrtlocnum = dstmvrtlocnum - dstvertlocbas;
        dstventloctax[dstbvrtlocnum] = datarcvtab[datarcvidx + enttdsp];
        dstvertloctax[dstbvrtlocnum] = edgelocidx;

        datarcvidx += vertlocinc;
        for (datarcvnnd = datarcvidx + datarcvtab[datarcvidx] * nghblocinc, datarcvidx ++;
            datarcvidx <= datarcvnnd; datarcvidx += nghblocinc) {
          Gnum nghblocnum;

          nghblocnum = datarcvtab[datarcvidx + nghbdsp];
          if (nghblocnum < 0) {
            Gnum nghblocidx;
            Gnum nghblocmax;

            nghblocnum = - nghblocnum - 1;

            /* Search the index of distant neighbor which have current number */
            for (nghblocidx = smshrcvtb2[smshrcvidx].indxnum, nghblocmax = smshrcvtb2[smshrcvidx].indxnnd;
                nghblocmax - nghblocidx > 1; ) {
              Gnum                 nghblocmed;

              nghblocmed = (nghblocmax + nghblocidx) / 2;
              if (ndstrcvtab[nghblocmed].nghbnum <= nghblocnum)
                nghblocidx = nghblocmed;
              else
                nghblocmax = nghblocmed;
            }

#ifdef PAMPA_DEBUG_DMESH2
            if (ndstrcvtab[nghblocidx].nghbnum != nghblocnum) {
              errorPrint ("Neighbor number %d not found", nghblocnum);
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

            if (dstedloloctax != NULL)
              dstedloloctax[edgelocidx] = datarcvtab[datarcvidx + edlodsp];
            dstedgeloctax[edgelocidx ++] = ndstrcvtab[nghblocidx].nprmnum;
          }
          else {
#ifdef PAMPA_DEBUG_DMESH2
            if ((nghblocnum < smshrcvtab[smshrcvidx].vertmin)
                || (nghblocnum > smshrcvtab[smshrcvidx].vertmax)) {
              errorPrint ("nghblocnum %d not in [%d,%d] for Smesh %d", nghblocnum, smshrcvtab[smshrcvidx].vertmin, smshrcvtab[smshrcvidx].vertmax, smshrcvidx);
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

            if (dstedloloctax != NULL)
              dstedloloctax[edgelocidx] = datarcvtab[datarcvidx + edlodsp];
            dstedgeloctax[edgelocidx ++] = vprmmshtab[smshrcvidx][nghblocnum];
          }
        }

        dstvendloctax[dstbvrtlocnum] = edgelocidx;
      }
      else {
        datarcvidx += vertlocinc;
        datarcvidx += datarcvtab[datarcvidx] * nghblocinc + 1; /* Displacement for neighbors */
      }
    }
    dstedgelocnbr = edgelocidx - baseval;
    END_TIME(cnt1);



    // - libérer le Dsmesh temporaire utilisé et pourquoi pas le Dmesh en prenant soin au préalable de garder les valeurs associées
    // - faire le dmeshBuild
    CHECK_FERR (dmeshBuild (dmshptr,
          baseval,
          dstvertlocnbr,
          dstvertlocnbr, // XXX dstvertlocnbr pour vertlocsiz ?
          dstvertloctax,
          dstvendloctax,
          dstedgelocnbr,
          dstedgelocsiz, // XXX ne devrait-on pas mettre dstedgelocnbr ??? Ou modifier la valeur
          dstedgeloctax,
          dstedloloctax,
          dsmshdat.enttglbnbr,
          dstventloctax,
          dsmshdat.esubloctax,
          dsmshdat.valslocptr->valuglbmax,
          dsmshdat.ovlpglbval,
          NULL,
          -1,
          NULL,
          NULL,
          NULL,
          -1,
          -1,
          NULL), dsmshdat.proccomm);

#ifdef PAMPA_DEBUG_DMESH2
    CHECK_FERR (dmeshCheck (dmshptr), dsmshdat.proccomm); // XXX à décommenter
#endif /* PAMPA_DEBUG_DMESH2 */

    valslocptr->cuntlocnbr ++;
    dsmeshExit (&dsmshdat);

    // - Parcourir datasndtab et datarcvtab
    //   - remplir ces deux tableaux en ne gardant uniquement les numéros de
    //   sommets
    //     - pour datasndtab : numéro du sommet dans Smesh
    //     - pour datarcvtab : le numéro du sommet le nouveau maillage distribué en utilisant vprmmshtab

    dsndloctab = (DataSndEntt *) datasndtab;
    drcvloctab = (DataRcvEntt *) datarcvtab;

    // FIXME il faut un parcours d'abord sur les Smesh pour que nous calculions
    // qu'une seule fois evrttax et evrttx2
    START_TIME(cnt1);
    for (datasndid2 = procngbnum = 0; procngbnum < dmshptr->procglbnbr; procngbnum ++)
      for (datasndidx = dsnddsptab[procngbnum]; datasndidx < dsnddsptab[procngbnum + 1]; )
        if (datasndtab[datasndidx] < 0) {
          Smesh * smshptr;
          Gnum smshsndnum;
          Gnum smshsndmax;
          Gnum enttnum;
          Gnum enttnnd;
          Gnum vertnum;
          Gnum vertnnd;

          dsndcnttab[procngbnum] --;
          smshsndnum = - datasndtab[datasndidx ++] - 1;
          datasndidx += datasndtab[datasndidx] * ndstlocinc + 1;

          /* Search the index of Smesh which have current number */
          for (smshsndidx = 0, smshsndmax = meshnbr;
              smshsndmax - smshsndidx > 1; ) {
            Gnum                 smshsndmed;

            smshsndmed = (smshsndmax + smshsndidx) / 2;
            if (smshsndtab[smshsndmed] <= smshsndnum)
              smshsndidx = smshsndmed;
            else
              smshsndmax = smshsndmed;
          }
#ifdef PAMPA_DEBUG_DMESH2
          if (smshsndtab[smshsndidx] != smshsndnum) {
            errorPrint ("Smesh number %d not found", smshsndnum);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

          smshptr = smshloctab + smshsndidx;
          for (enttnum = baseval, enttnnd = smshptr->enttnbr + baseval; enttnum < enttnnd; enttnum ++)
            enbrloctax[enttnum] = baseval;
          memSet (evrttax + baseval, 0, vertmax * sizeof (Gnum));
          for (vertnum = baseval, vertnnd = smshptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
            Gnum esubnum;

            esubnum = smshptr->venttax[vertnum];
            enttnum = dmshptr->esublocbax[esubnum & dmshptr->esublocmsk];
            svrttax[vertnum] = enbrloctax[esubnum] ++;
            if (enttnum > PAMPA_ENTT_SINGLE)
              evrttax[vertnum] = enbrloctax[enttnum] ++;


          }
        }
        else {
          Gnum enttnum;
          Gnum vertnum;

          enttnum = datasndtab[datasndidx + enttdsp];
          vertnum = datasndtab[datasndidx + vertdsp];
          datasndidx += vertlocinc;
          datasndidx += datasndtab[datasndidx] * nghblocinc + 1; /* Displacement for neighbors */

          dsndloctab[datasndid2].enttnum = enttnum;
          dsndloctab[datasndid2].smshidx = smshsndidx;
          dsndloctab[datasndid2].procnum = procngbnum;
          dsndloctab[datasndid2].vertnum = svrttax[vertnum];
          dsndloctab[datasndid2 ++].vertnm2 = evrttax[vertnum];
        }
    for (procngbnum = 0; procngbnum < dmshptr->procglbnbr; procngbnum ++)
      dsnddsptab[procngbnum + 1] = dsnddsptab[procngbnum] + dsndcnttab[procngbnum];
    datasndsiz = datasndid2;
    memFreeGroup (smshsndtab);

    memset (enbrloctax + baseval, 0, dmshptr->enttglbnbr * sizeof (Gnum));
    
    for (datarcvid2 = procngbnum = 0; procngbnum < dmshptr->procglbnbr; procngbnum ++)
      for (datarcvidx = drcvdsptab[procngbnum]; datarcvidx < drcvdsptab[procngbnum + 1]; ) {
        Gnum vertlocidx;

        if (datarcvtab[datarcvidx] < 0) {
          Gnum smshrcvmax;

          drcvcnttab[procngbnum] --;
          vertlocidx = 0; // XXX 0 ou baseval ??
          smshrcvnum = - datarcvtab[datarcvidx ++] - 1;
          datarcvidx += datarcvtab[datarcvidx] * ndstlocinc + 1;

          /* Search the index of Smesh which have current number */
          for (smshrcvidx = 0, smshrcvmax = smshrcvnbr;
              smshrcvmax - smshrcvidx > 1; ) {
            Gnum                 smshrcvmed;

            smshrcvmed = (smshrcvmax + smshrcvidx) / 2;
            if (smshrcvtab[smshrcvmed].smshnum <= smshrcvnum)
              smshrcvidx = smshrcvmed;
            else
              smshrcvmax = smshrcvmed;
          }
#ifdef PAMPA_DEBUG_DMESH2
          if (smshrcvtab[smshrcvidx].smshnum != smshrcvnum) {
            errorPrint ("Smesh number %d not found", smshrcvnum);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
        }
        else {
          Gnum enttnum;

          enttnum = datarcvtab[datarcvidx + enttdsp];
          srcvertlocnum = datarcvtab[datarcvidx + vertdsp];
#ifdef PAMPA_DEBUG_DMESH2
          if ((srcvertlocnum < smshrcvtab[smshrcvidx].vertmin)
              || (srcvertlocnum > smshrcvtab[smshrcvidx].vertmax)) {
            errorPrint ("srcvertlocnum %d not in [%d,%d] for Smesh %d", srcvertlocnum, smshrcvtab[smshrcvidx].vertmin, smshrcvtab[smshrcvidx].vertmax, smshrcvidx);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
          datarcvidx += vertlocinc;
          datarcvidx += datarcvtab[datarcvidx] * nghblocinc + 1; /* Displacement for neighbors */

          drcvloctab[datarcvid2].enttnum = enttnum;
          drcvloctab[datarcvid2].procnum = procngbnum;
          drcvloctab[datarcvid2].smshnum = smshrcvnum;
          drcvloctab[datarcvid2].vertnum = srcvertlocnum;
          drcvloctab[datarcvid2 ++].vprmnum = vprmmshtab[smshrcvidx][srcvertlocnum];
        }
      }
    for (procngbnum = 0; procngbnum < dmshptr->procglbnbr; procngbnum ++)
      drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
    datarcvsiz = datarcvid2;
    for (smshrcvidx = 0; smshrcvidx < smshrcvnbr; smshrcvidx ++)
      memFree (vprmmshtab[smshrcvidx] + smshrcvtab[smshrcvidx].vertmin);
    memFreeGroup (smshrcvtab);
    END_TIME(cnt1);

    START_TIME(cnt1);
    for (passval = 0; passval < 2; passval ++) { /* 0 for sub-entities and 1 for entities */

      intSort5asc4 (dsndloctab, datasndsiz);

      intSort5asc4 (drcvloctab, datarcvsiz);
      // - pour chaque valeur associée de l'utilisateur (mettre un drapeau pour
      // celles qu'on a ajoutées lors de l'adaptation) FIXME
      //   - allouer un tableau temporaire pour envoyer
      //   - associer la nouvelle valeur associée
      //   - recopier les valeurs des sommets «externes» en utilisant vprmgsttax
      //   - supprimer la valeur associée sur l'ancien maillage
      //
      //   - remplir le tableau temporaire pour envoyer en utilisant datasndtab
      //
      //   - échanger datasndtab et datarcvtab
      //
      //   - remplir le tableau de valeurs associées avec les valeurs reçues en utilisant smshrcvtab et datarcvtab
      for (datasndcur = datarcvcur = 0, enttlocnum = baseval; enttlocnum <= enttglbmax; enttlocnum ++) { // XXX et les entités virtuelles, ne sont-elles pas traitées FIXME
        Dvalue * valulocptr;
        DmeshEntity * enttlocptr;

        enttlocptr = dmshptr->enttloctax[enttlocnum];

        if ((enttlocptr == NULL) || (valslocptr->evalloctak[enttlocnum] == NULL) /* If empty entity or no value */
            || ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] >= PAMPA_ENTT_SINGLE) && (passval == 1)) /* If sub-entity or single entity */
            || ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] < PAMPA_ENTT_SINGLE) && (passval == 0))) /* If super-entity */
          continue;

        CHECK_FERR (dvalueData(valslocptr, enttlocnum, TAG_INT_GLOBAL, NULL, NULL, (void **) &mvrtloctax), dmshptr->proccomm);
        mvrtloctax -= baseval;

        for (; datasndcur < datasndsiz && dsndloctab[datasndcur].enttnum < enttlocnum; datasndcur ++);
        memset (dsndcnttab, 0, dmshptr->procglbnbr * sizeof (int));
        for (datasndidx = datasndcur; datasndidx < datasndsiz && dsndloctab[datasndidx].enttnum == enttlocnum; datasndidx ++)
          dsndcnttab[dsndloctab[datasndidx].procnum] ++;

        //
        // * en déduire drcvdsptab
        /* Compute displacement sending array */
        dsnddsptab[0] = 0;
        for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr + 1; procngbnum ++) {
          dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
        }

        datasndnbr = dsnddsptab[dmshptr->procglbnbr];

        for (; datarcvcur < datarcvsiz && drcvloctab[datarcvcur].enttnum < enttlocnum; datarcvcur ++);
        memset (drcvcnttab, 0, dmshptr->procglbnbr * sizeof (int));
        for (datarcvidx = datarcvcur; datarcvidx < datarcvsiz && drcvloctab[datarcvidx].enttnum == enttlocnum; datarcvidx ++)
          drcvcnttab[drcvloctab[datarcvidx].procnum] ++;

        //
        // * en déduire dsnddsptab
        /* Compute displacement receiving array */
        drcvdsptab[0] = 0;
        for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr + 1; procngbnum ++) {
          drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
        }

        datarcvnbr = drcvdsptab[dmshptr->procglbnbr];

        // il faut :
        //   - faire une boucle pour chaque Smesh
        //   - faire l'échange des valeurs
        //   - attacher la nouvelle valeur
        //   - recopier et permuter les valeurs reçues sur la nouvelle valeur attachée
        //   - recopier les valeurs de l'ancien valslocptr à la nouvelle valeur attachée
        for (valulocptr = valslocptr->evalloctak[enttlocnum]; valulocptr != NULL; valulocptr = valulocptr->next) {
          MPI_Aint            valuglbsiz;                 /* Extent of attribute datatype */
          MPI_Aint            dummy;
          Gnum                tagnum;
          byte *              valusndtab;
          byte *              valurcvtab;
          byte *              valuloctax;
	  Value * valuptr;

          tagnum = valulocptr->tagnum;

          if (tagnum == TAG_INT_GLOBAL)
            continue;

          MPI_Type_get_extent (valulocptr->typeval, &dummy, &valuglbsiz);     /* Get type extent */

          CHECK_FERR (dmeshValueLink(dmshptr, (void **) &valuloctax, valulocptr->flagval, NULL, NULL, valulocptr->typeval, enttlocnum, valulocptr->tagnum), dmshptr->proccomm);
          valuloctax -= baseval * valuglbsiz;

          for (srcvertlocnum = baseval, srcvertlocnnd = evrtloctax[enttlocnum] + baseval; srcvertlocnum < srcvertlocnnd; srcvertlocnum ++) {
            Gnum srcbvrtlocnum;

            srcbvrtlocnum = mvrtloctax[srcvertlocnum] - srcvertlocbas;
            if (vprmgsttax[srcbvrtlocnum] != ~0) {
              Gnum dstmvrtlocnum;
              Gnum dstvertlocidx;
              Gnum dstvertlocmax;

              dstmvrtlocnum = srcbvrtlocnum + dstvertlocbas;

              /* Search the index of local vertex which have current number */
              for (dstvertlocidx = baseval, dstvertlocmax = enttlocptr->vertlocnbr + baseval;
                  dstvertlocmax - dstvertlocidx > 1; ) {
                Gnum                 dstvertlocmed;

                dstvertlocmed = (dstvertlocmax + dstvertlocidx) / 2;
                if (enttlocptr->mvrtloctax[dstvertlocmed] <= dstmvrtlocnum)
                  dstvertlocidx = dstvertlocmed;
                else
                  dstvertlocmax = dstvertlocmed;
              }
#ifdef PAMPA_DEBUG_DMESH2
              if (enttlocptr->mvrtloctax[dstvertlocidx] != dstmvrtlocnum) {
                errorPrint ("vertex %d not found", dstmvrtlocnum);
                cheklocval = 1;
              }
              CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

              memCpy ((byte *) valuloctax + dstvertlocidx * valuglbsiz, (byte *) valulocptr->valuloctab + (srcvertlocnum - baseval) * valuglbsiz, valuglbsiz * sizeof (byte));
            }
          }



          if (memAllocGroup ((void **) (void *)
                &valusndtab, (size_t) (datasndnbr * valuglbsiz),
                &valurcvtab, (size_t) (datarcvnbr * valuglbsiz),
                NULL) == NULL) {
            errorPrint ("Out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, dmshptr->proccomm);

          for (procngbnum = 0 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
            dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
          }

	  //errorPrint("datasndnbr:%d, datasndsiz:%d", datasndnbr, datasndsiz);
	  //exit (1);
	  smshsndidx = -1;
          valuptr = NULL;
          for (datasndidx = datasndcur; datasndidx < datasndsiz && dsndloctab[datasndidx].enttnum == enttlocnum; datasndidx ++) {
            Gnum procnum;
	    Values * valsptr;
    	    int t;
t = 0;

//errorPrint ("la0");
            if ((smshsndidx != dsndloctab[datasndidx].smshidx) || ((valuptr != NULL) && (valuptr->tagnum != tagnum))) {
t = 1;
//errorPrint ("la1");
              smshsndidx = dsndloctab[datasndidx].smshidx;
              valsptr = smshloctab[smshsndidx].valsptr;

              for (valuptr = valsptr->evaltak[enttlocnum]; valuptr != NULL && valuptr->tagnum != tagnum; valuptr = valuptr->next);

            }

#ifdef PAMPA_DEBUG_DMESH2
            if ((valuptr == NULL) || (valuptr->tagnum != tagnum)) {
              errorPrint ("Tag number %d not found", tagnum);
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
            procnum = dsndloctab[datasndidx].procnum;
            memCpy ((byte *) valusndtab + dsndcnttab[procnum] * valuglbsiz, (byte *) valuptr->valutab + dsndloctab[datasndidx].vertnum * valuglbsiz, valuglbsiz * sizeof (byte));
            dsndcnttab[procnum] ++;
          }

          for (procngbnum = 0 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
            dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
          }

          CHECK_FMPI (cheklocval, commAlltoallv(valusndtab, dsndcnttab, dsnddsptab, valulocptr->typeval, valurcvtab, drcvcnttab, drcvdsptab, valulocptr->typeval, dmshptr->proccomm), dmshptr->proccomm);


          // FIXME il faut recopier les valeurs de l'ancien maillage sur le
          // nouveau avant les valeurs des boules
          //for (srcvertlocnum = baseval, srcvertlocnnd = enttlocptr->vertlocnbr + baseval; srcvertlocnum < srcvertlocnnd; srcvertlocnum ++) {
          //  srcbvrtlocnum = 

          for (datarcvidx = datarcvcur; datarcvidx < datarcvsiz && drcvloctab[datarcvidx].enttnum == enttlocnum; datarcvidx ++) {
            Gnum mvrtlocnum;
            Gnum evrtlocnum;
            Gnum evrtlocmax;
            mvrtlocnum = drcvloctab[datarcvidx].vprmnum;
            /* Search the index of vertex which have current number */
            for (evrtlocnum = baseval, evrtlocmax = enttlocptr->vertlocnbr + baseval;
                evrtlocmax - evrtlocnum > 1; ) {
              Gnum                 evrtlocmed;

              evrtlocmed = (evrtlocmax + evrtlocnum) / 2;
              if (enttlocptr->mvrtloctax[evrtlocmed] <= mvrtlocnum) // XXX mettre dans un pointeur local sortvrttab[compnum][evrtlocnum]
                evrtlocnum = evrtlocmed;
              else
                evrtlocmax = evrtlocmed;
            }

            memCpy((byte *) valuloctax + evrtlocnum * valuglbsiz, (byte *) valurcvtab + (datarcvidx - datarcvcur) * valuglbsiz, valuglbsiz * sizeof (byte));
          }
          memFreeGroup (valusndtab);
        }
        }




        for (enttlocnum = -1, datasndidx = 0; datasndidx < datasndsiz; datasndidx ++) {
          Gnum enttlocnm2;

          if (dsndloctab[datasndidx].enttnum != enttlocnum) {
            enttlocnum = dsndloctab[datasndidx].enttnum;
            if (dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
              enttlocnm2 = dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk];
              dsndloctab[datasndidx].enttnum = enttlocnm2;
              dsndloctab[datasndidx].vertnum = dsndloctab[datasndidx].vertnm2;
            }
            else {
              for (; datasndidx < datasndsiz && enttlocnum == dsndloctab[datasndidx].enttnum; datasndidx ++);
              datasndidx --;
            }
          }
          else {
            dsndloctab[datasndidx].enttnum = enttlocnm2;
            dsndloctab[datasndidx].vertnum = dsndloctab[datasndidx].vertnm2;
          }
        }

        for (enttlocnum = -1, datarcvidx = 0; datarcvidx < datarcvsiz; datarcvidx ++) {
          Gnum enttlocnm2;

          if (drcvloctab[datarcvidx].enttnum != enttlocnum) {
            enttlocnum = drcvloctab[datarcvidx].enttnum;
            if (dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
              enttlocnm2 = dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk];
              drcvloctab[datarcvidx].enttnum = enttlocnm2;
            }
            else {
              for (; datarcvidx < datarcvsiz && enttlocnum == drcvloctab[datarcvidx].enttnum; datarcvidx ++);
              datarcvidx --;
            }
          }
          else {
            drcvloctab[datarcvidx].enttnum = enttlocnm2;
          }
        }

      }
      memFreeGroup (vprmgsttax + baseval);
      memFree (evrtloctax + baseval);
      END_TIME(cnt1);


//#define PAMPA_DEBUG_DMESH2
//#ifdef PAMPA_DEBUG_DMESH2
//      // XXX temporaire
//      double * coorloctax;
//      double abx, aby, abz, acx, acy, acz, adx, ady, adz, v1, v2, v3, vol;
//      CHECK_FERR (dmeshValueData (dmshptr, 1, PAMPA_TAG_GEOM, NULL, NULL, (void **) &coorloctax), dmshptr->proccomm);
//      coorloctax -= 3 * baseval;
//      PAMPA_Iterator it, it_nghb;
//      PAMPA_dmeshItInitStart((PAMPA_Dmesh *) dmshptr, 0, PAMPA_VERT_LOCAL, &it);
//      PAMPA_dmeshItInit((PAMPA_Dmesh *) dmshptr, 0, 1, &it_nghb);
//      while (PAMPA_itHasMore(&it)) {
//        PAMPA_Num tetnum;
//        PAMPA_Num mtetnum;
//        PAMPA_Num nodeidx;
//        double coortab[4][3];
//
//        tetnum = PAMPA_itCurEnttVertNum(&it);
//        mtetnum = PAMPA_itCurMeshVertNum(&it);
//        nodeidx = 0;
//
//        PAMPA_itStart(&it_nghb, tetnum);
//
//        while (PAMPA_itHasMore(&it_nghb)) {
//          PAMPA_Num nodenum;
//          nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
//          memCpy (coortab[nodeidx], coorloctax + 3 * nodenum, 3 * sizeof (double));
//          nodeidx ++;
//          PAMPA_itNext(&it_nghb);
//        }
//
//        if (nodeidx != 4) {
//          errorPrint ("element %d (%d) doesn't have 4 nodes, but %d\n", tetnum, mtetnum, nodeidx);
//          cheklocval = 1;
//        }
//        CHECK_VERR (cheklocval, dmshptr->proccomm);
//        // FIXME : partie reprise de MMG3d pour calculer les volumes
//        /* volume */
//        abx = coortab[1][0] - coortab[0][0];
//        aby = coortab[1][1] - coortab[0][1];
//        abz = coortab[1][2] - coortab[0][2];
//
//        acx = coortab[2][0] - coortab[0][0];
//        acy = coortab[2][1] - coortab[0][1];
//        acz = coortab[2][2] - coortab[0][2];
//
//        adx = coortab[3][0] - coortab[0][0];
//        ady = coortab[3][1] - coortab[0][1];
//        adz = coortab[3][2] - coortab[0][2];
//
//        v1  = acy*adz - acz*ady;
//        v2  = acz*adx - acx*adz;
//        v3  = acx*ady - acy*adx;
//        vol = abx * v1 + aby * v2 + abz * v3;
//        //if (vol < 0) {
//        //  errorPrint ("element %d (%d) has negative volume %lf", tetnum, mtetnum, vol);
//        //  cheklocval = 1;
//        //}
//        //CHECK_VERR (cheklocval, dmshptr->proccomm);
//        // FIXME : fin de partie reprise de MMG3d
//        PAMPA_itNext(&it);
//      }
//#endif /* PAMPA_DEBUG_DMESH2 */
//#undef PAMPA_DEBUG_DMESH2 





      dvaluesFree (valslocptr);
      memFreeGroup (dsndcnttab);
      memFreeGroup (datasndtab);

      CHECK_VDBG (cheklocval, dmshptr->proccomm);
      return (0);
    }

    static
      int
      dmeshRebldSmeshVertResize (
          Dsmesh * restrict const    dmshptr,              /**< mesh */
          DmeshRebldSmeshVert * restrict * hashtabptr,
          Gnum * restrict const           hashsizptr,
          Gnum * restrict const           hashmaxptr,
          Gnum * restrict const           hashmskptr)
      {
        Gnum                          oldhashsiz;          /* Size of hash table    */
        Gnum                          hashnum;          /* Hash value            */
        Gnum                          oldhashmsk;
        int                           cheklocval;
        Gnum hashidx;
        Gnum hashtmp;

        cheklocval = 0;
#ifdef PAMPA_DEBUG_DMESH
        for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
          Gnum vertlocnum;
          Gnum smshrcvidx;

          vertlocnum = (*hashtabptr)[hashidx].vertnum;
          smshrcvidx = (*hashtabptr)[hashidx].prshval.smshidx;

          if (vertlocnum == ~0) // If empty slot
            continue;

          for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
            if (((*hashtabptr)[hashnum].vertnum == vertlocnum) && ((*hashtabptr)[hashnum].prshval.smshidx == smshrcvidx))
              errorPrint ("vertex and processor numbers are already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
        }
#endif /* PAMPA_DEBUG_DMESH */

        oldhashmsk = *hashmskptr;
        oldhashsiz = *hashsizptr;
        *hashmaxptr <<= 1;
        *hashsizptr <<= 1;
        *hashmskptr = *hashsizptr - 1;

        if ((*hashtabptr = (DmeshRebldSmeshVert *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (DmeshRebldSmeshVert)))) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dmshptr->proccomm);

        memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (DmeshRebldSmeshVert)); // TRICK: *hashsizptr = oldhashsiz * 2

        for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].vertnum != ~0; hashidx --); // Stop at last empty slot

        hashtmp = hashidx;

        for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
          Gnum vertlocnum;
          Gnum smshrcvidx;

          vertlocnum = (*hashtabptr)[hashidx].vertnum;
          smshrcvidx = (*hashtabptr)[hashidx].prshval.smshidx;

          if (vertlocnum == ~0) // If empty slot
            continue;

          for (hashnum = (vertlocnum * DMESHREBLDHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].vertnum != ~0 && ((*hashtabptr)[hashnum].vertnum != vertlocnum || (*hashtabptr)[hashnum].prshval.smshidx != smshrcvidx); hashnum = (hashnum + 1) & (*hashmskptr)) ;

          if (hashnum == hashidx) // already at the good slot 
            continue;
          (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
          (*hashtabptr)[hashidx].vertnum = ~0;
          (*hashtabptr)[hashidx].prshval.smshidx = ~0;
        }

#ifdef PAMPA_DEBUG_DMESH
        for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
          Gnum vertlocnum;
          Gnum smshrcvidx;

          vertlocnum = (*hashtabptr)[hashidx].vertnum;
          smshrcvidx = (*hashtabptr)[hashidx].prshval.smshidx;

          if (vertlocnum == ~0) // If empty slot
            continue;

          for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
            if (((*hashtabptr)[hashnum].vertnum == vertlocnum) && ((*hashtabptr)[hashnum].prshval.smshidx == smshrcvidx))
              errorPrint ("vertex and processor numbers are already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
        }
#endif /* PAMPA_DEBUG_DMESH */
        return (0);
      }

