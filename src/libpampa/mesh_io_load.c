/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_io_load.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the input/output routines for centralized
//!                meshes.
//!
//!   \date        Version 1.0: from:  6 Feb 2015
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define MESH_IO_LOAD

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "mesh_io_load.h"
#include "pampa.h"

/* This routine loads a centralized source
** mesh from the given stream(s). Either
** one processor holds a non-NULL stream
** of a centralized mesh, or all of them
** hold valid streams to either a centralized
** or a centralized mesh.
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
meshLoad (
Mesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream,               /* One single centralized stream or centralized ones */
const Gnum                 baseval,              /* Base value (-1 means keep file base)              */
const Gnum                 flagval)              /* Mesh loading flags                               */
{
  Gnum                cenval;
  int                 versvl2;
  int                 cenvl2;
  int                 gnumvl2;
  int                 chekval;

  chekval = 0;

  chekval |= (fscanf (stream, "%d\t%d\t%d\n", /* version, parallel, Gnum size  */
        &versvl2, &cenvl2, &gnumvl2) == EOF);
  CHECK_VDBG2 (chekval);
  cenval = (Gnum) cenvl2;
   // XXX versval et cenval ne sont pas utilisés, à faire... TODO 
    //if (intLoad (stream, &cenval) != 1) {        /* Read version number */
    //  errorPrint ("bad input");
    //  cenval       = 0;
    //  redutab[6] = 1;
    //}
    //else if ((cenval != 0) && (cenval != 2)) {  /* If not a mesh format */
    //  errorPrint ("meshLoad: not a mesh format");
    //  redutab[6] = 1;
    //}

  if (cenval == 2)                         /* If centralized mesh format             */
    return (meshLoadCent (meshptr, stream, baseval, flagval)); /* Read centralized mesh */
  else if (cenval == 3)                        /* If centralized mesh format with all data */
    return (meshLoadCentAll (meshptr, stream, baseval, flagval)); /* Read centralized mesh */

  errorPrint ("meshLoad: invalid number of input streams");
  return     (1);
}

static inline int valueScan (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  int chekval;

  chekval = 0;

  if (!strcmp (format, "%d\t")) 
    chekval |= (fscanf (stream, format, ((int *) value)) == EOF);
  else if (!strcmp (format, "%lf\t")) // XXX bug voir mesh_io_save
    chekval |= (fscanf (stream, format, ((double *) value)) == EOF);
  else if (!strcmp (format, "%lld\t")) 
    chekval |= (fscanf (stream, format, ((int64_t *) value)) == EOF);
  else {
    errorPrint ("Unrecognized format");
    return (1);
  }

  return (chekval);
}
 
/** XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
meshLoadValues (
Mesh * restrict const     meshptr,              /* centralized mesh to load            */
FILE * const                stream)               /* */
{
  Gnum enttnum;
  Gnum enttnnd;
  int chekval;

  chekval = 0;

  for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = meshptr->enttnbr + meshptr->baseval; enttnum < enttnnd; enttnum ++) {
    MeshEntity * restrict enttptr;
    Gnum valunum;
    Gnum valunbr;
    Gnum vertnum;
    Gnum vertnnd;

    chekval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\n", &enttnum, &valunbr) == EOF);

    if (enttnum >= meshptr->baseval)
      enttptr = meshptr->entttax[enttnum];
    for (valunum = 0; valunum < valunbr; valunum ++) {
      int combdat;
      MPI_Aint            valusiz;                 /* Extent of attribute datatype */
      MPI_Aint            dummy;
      Gnum typeval;
      Gnum countnbr;
      Gnum countnum;
      Gnum tagnum;
      size_t elemsiz;
      char format[9];
      byte * valutab;
      MPI_Datatype typedat;
      MPI_Datatype typedt2;
      int proclocnum;

      chekval |= (fscanf (stream, GNUMSTRING "\t%d\t", &tagnum, &combdat) == EOF);
      switch (combdat) {
        case 1: /* MPI_COMBINER_NAMED */
          countnbr = 1;
          chekval |= (fscanf (stream, GNUMSTRING "\n", &typeval) == EOF);

          if (typeval == 1) { /* MPI_INT */
            typedat = MPI_INT;
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (typeval == 2) { /* MPI_DOUBLE */
            typedat = MPI_DOUBLE;
            strcpy (format, "%lf\t");
            elemsiz = sizeof (double);
          }
          else if (typeval == 3) { /* MPI_LONG_LONG */
            typedat = MPI_LONG_LONG;
            strcpy (format, "%lf\t");
            elemsiz = sizeof (int64_t);
          }
          else {
            errorPrint ("Type not implemented");
            chekval = 1;
          }
          break;
        case 2: /* MPI_COMBINER_CONTIGUOUS */
          chekval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\n", &countnbr, &typeval) == EOF);

          if (typeval == 1) { /* MPI_INT */
            typedt2 = MPI_INT;
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (typeval == 2) { /* MPI_DOUBLE */
            typedt2 = MPI_DOUBLE;
            strcpy (format, "%lf\t");
            elemsiz = sizeof (double);
          }
          else if (typeval == 3) { /* MPI_LONG_LONG */
            typedt2 = MPI_LONG_LONG;
            strcpy (format, "%lf\t");
            elemsiz = sizeof (int64_t);
          }
          else {
            errorPrint ("Type not implemented");
            chekval = 1;
          }


          MPI_Type_contiguous(countnbr, typedt2, &typedat);
          MPI_Type_commit(&typedat);

          break;
        default:
          errorPrint ("case not implemented");
          chekval = 1;
      }
      CHECK_VDBG2 (chekval);

      CHECK_FDBG2 (meshValueLink (meshptr, (void **) &valutab, PAMPA_VALUE_NONE, NULL, NULL, typedat, enttnum, tagnum));
      MPI_Type_get_extent (typedat, &dummy, &valusiz);     /* Get type extent */

      switch (enttnum) {
        case PAMPA_ENTT_VIRT_PROC:
          for (proclocnum = 0; proclocnum < meshptr->procglbnbr; proclocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valueScan (stream, format, valutab + proclocnum * valusiz + countnum * elemsiz);
          chekval |= (fscanf (stream, "\n") == EOF);
          break;
        case PAMPA_ENTT_VIRT_VERT:
          for (vertnum = 0; vertnum < meshptr->vertnbr; vertnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valueScan (stream, format, valutab + vertnum * valusiz + countnum * elemsiz);
          chekval |= (fscanf (stream, "\n") == EOF);
          break;
        default: /* real entity */

          for (vertnum = meshptr->baseval, vertnnd = enttptr->vertnbr + meshptr->baseval; vertnum < vertnnd; vertnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valueScan (stream, format, valutab + vertnum * valusiz + countnum * elemsiz);
          chekval |= (fscanf (stream, "\n") == EOF);
      }
    }
  }   
  CHECK_VDBG2 (chekval);
  return (0);
}

/* This routine loads a centralized source
** mesh from a centralized source mesh
** file spread across all of the streams.
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
meshLoadCent (
Mesh * restrict const     meshptr,              /* centralized mesh to load            */
FILE * const                stream,               /* */
Gnum                        baseval,              /* Base value (-1 means keep file base) */
const MeshFlag            flagval)              /* Mesh loading flags                  */
{
  int                 proclocnum;
  int                 procglbnbr;
  Gnum                enttnbr;
  Gnum                enttnum;
  Gnum                enttnnd;
  Gnum                edgenbr;
  Gnum                edgesiz;
  Gnum                valunbr;
  Gnum                valumax;
  Gnum                esubsiz;
  Gnum                vlblsiz;
  Gnum                edlosiz;
  Gnum                vertnbr;
  Gnum                vertnnd;
  Gnum                vertnum;
  Gnum * restrict     verttax;
  Gnum * restrict     venttax;
  Gnum * restrict     esubtax;
  Gnum                edgennd;
  Gnum                edgenum;
  Gnum * restrict     edgetax;
  Gnum * restrict     edlotax;
  int                 chekval;

  chekval = 0;
#ifdef PAMPA_DEBUG_MESH2
  if (stream == NULL) {
    errorPrint ("invalid parameter");
    return     (1);
  }
#endif /* PAMPA_DEBUG_MESH2 */

  chekval |= (fscanf (stream,
      "%d\t%d\n"  /* proclocnum, procglbnbr */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n"  /* enttnbr, vertnbr, edgenbr */
      GNUMSTRING "\t" GNUMSTRING "\n"  /* valunbr, valumax */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n" GNUMSTRING "\n", /* baseval, esubtab, vlblsiz, edlosiz */
      &proclocnum, &procglbnbr,
      &enttnbr, &vertnbr, &edgenbr, /* enttnbr, vertnbr, edgenbr */
      &valunbr, &valumax,
      &baseval, &esubsiz, &vlblsiz, &edlosiz) == EOF);

  if (vlblsiz == 1) { /* vlbltax not yet implemented" */
    errorPrint ("vlbltab not yet used, please wait for v1.1.0");
    return (1);
  }

  edlosiz *= edgenbr;
  esubsiz *= enttnbr;
  edgesiz = edgenbr;

  if (memAllocGroup ((void **) (void *)
        &verttax, (size_t) ((vertnbr + 1) * sizeof (Gnum)),
        &venttax, (size_t) (vertnbr       * sizeof (Gnum)),
        &edgetax, (size_t) (edgesiz       * sizeof (Gnum)),
        &edlotax, (size_t) (edgesiz       * sizeof (Gnum)),
        &esubtax, (size_t) (esubsiz       * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
    chekval = 1;
  }
  CHECK_VDBG2 (chekval);
  verttax -= baseval;
  edgetax -= baseval;
  if (esubsiz != 0)
    esubtax -= baseval;
  else
    esubtax = NULL;
  if (edlosiz != 0)
    edlotax -= baseval;
  else
    edlotax = NULL;

  if (esubtax != NULL) {
    for (enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
      chekval |= (fscanf (stream, GNUMSTRING "\t", &esubtax[enttnum + baseval]) == EOF);
    }
    chekval |= (fscanf (stream, "\n") == EOF);
  }

  verttax[baseval] = baseval;
  for (vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++) {
    chekval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\t", &venttax[vertnum], &verttax[vertnum + 1]) == EOF);
    verttax[vertnum + 1] += verttax[vertnum];

    for (edgenum = verttax[vertnum], edgennd = verttax[vertnum + 1]; edgenum < edgennd; edgenum ++) {
      if (edlosiz != 0)
        chekval |= (fscanf (stream, GNUMSTRING "\t", &edlotax[edgenum]) == EOF);
      chekval |= (fscanf (stream, GNUMSTRING "\t", &edgetax[edgenum]) == EOF);
    }
  }
  chekval |= (fscanf (stream, "\n") == EOF);

  // XXX idnum = ~0 ?
 CHECK_FDBG2 (meshBuild ( meshptr, ~0, baseval, vertnbr, verttax, verttax + 1, edgenbr, edgesiz, edgetax, edlotax, enttnbr, venttax, esubtax, valumax, NULL, -1, procglbnbr));

 memFreeGroup (verttax + baseval);
  CHECK_VDBG2 (chekval);
  return (meshLoadValues (meshptr, stream));
}

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
meshLoadCentAll (
Mesh * restrict const     meshptr,              /* centralized mesh to load            */
FILE * const                stream,               /* */
Gnum                        baseval,              /* Base value (-1 means keep file base) */
const MeshFlag            flagval)              /* Mesh loading flags                  */
{
  static Gnum     enttsingle = -1;
  Gnum ventnum;
  Gnum ventnnd;
  Gnum edgenum;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum edlosiz;
  Gnum valumax;
  int tmp;
  int chekval;

  chekval = 0;

  // XXX attention si baseval et meshptr->baseval diffèrent TODO
  // XXX et proccomm ??
  chekval |= (fscanf (stream,
      "%d\t" /* meshptr->flagval    */
      GNUMSTRING "\t" /* meshptr->baseval    */
      GNUMSTRING "\t" /* meshptr->vertnbr */
      GNUMSTRING "\t" /* meshptr->enttnbr */
      GNUMSTRING "\t" /* meshptr->esubmsk */
      GNUMSTRING "\t" /* meshptr->degrmax */
      GNUMSTRING "\t" /* meshptr->edgenbr */
      GNUMSTRING "\t" /* meshptr->edgesiz */
      "%d\t"          /* meshptr->procglbnbr */
      "%d\t"          /* meshptr->proclocnum */
      GNUMSTRING "\t", /* valumax          */
      &meshptr->flagval,   
      &meshptr->baseval,   
      &meshptr->vertnbr,
      &meshptr->enttnbr,
      &meshptr->esubmsk,
      &meshptr->degrmax,
      &meshptr->edgenbr,
      &meshptr->edgesiz,
      &meshptr->procglbnbr,
      &meshptr->proclocnum,
      &valumax) == EOF);
  baseval = meshptr->baseval;

  if (meshptr->esubmsk != 0) {
    if ((meshptr->esubbax = (Gnum *) memAlloc (meshptr->enttnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      chekval = 1;
    }
    CHECK_VDBG2 (chekval);

    for (enttnum = meshptr->baseval, enttnnd = meshptr->baseval + meshptr->enttnbr; enttnum < enttnnd; enttnum ++)
      chekval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->esubbax[enttnum]) == EOF);
    chekval |= (fscanf (stream, "\n") == EOF);
  }
  else 
    meshptr->esubbax = &enttsingle;

  chekval |= (fscanf (stream, "%d\n", &tmp) == EOF); // meshptr->edlotax
  if (tmp == 1)
    edlosiz = meshptr->edgesiz;
  else
    edlosiz = 0;

  /* Alate private entity arrays and adjacency list */
  if (memAllocGroup ((void **) (void *)
        &meshptr->entttax, (size_t) (meshptr->enttnbr * sizeof (MeshEntity *)),
        &meshptr->edgetax, (size_t) (meshptr->edgesiz * sizeof (Gnum)),
        &meshptr->edlotax, (size_t) (edlosiz          * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    chekval = 1;
  }
  CHECK_VDBG2 (chekval);

  if (tmp == 1) { //if (meshptr->edlotax != NULL) 
    Gnum edlonum;
    for (edlonum = baseval; edlonum < meshptr->edgesiz; edlonum ++)
      chekval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->edlotax[edlonum]) == EOF);
    chekval |= (fscanf (stream, "\n") == EOF);
  }
  else
    meshptr->edlotax = NULL;

  for (edgenum = baseval; edgenum < meshptr->edgesiz; edgenum ++)
    chekval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->edgetax[edgenum]) == EOF);
  chekval |= (fscanf (stream, "\n") == EOF);

  for (ventnum = baseval, ventnnd = meshptr->enttnbr + baseval; ventnum < ventnnd; ventnum ++) {
    MeshEntity * restrict ventptr;
    Gnum vertnum;
    Gnum vertnnd;

    chekval |= (fscanf (stream, "%d\n", &tmp) == EOF); 

    if (tmp == 1) { //if (meshptr->entttax[ventnum] != NULL) 
      if ((meshptr->entttax[ventnum] = (MeshEntity *) memAlloc (sizeof (MeshEntity))) == NULL) {
        errorPrint  ("out of memory");
        chekval = 1;
      }
      CHECK_VDBG2 (chekval);

      ventptr = meshptr->entttax[ventnum];
      chekval |= (fscanf (stream, GNUMSTRING "\n", &ventptr->vertnbr) == EOF);
      switch (meshptr->esubbax[ventnum & meshptr->esubmsk]) {

        case PAMPA_ENTT_STABLE : /* Entity with sub-entities and sorting is stable inside the entity */

          /* Alate private entity neighbors data*/
          if (memAllocGroup ((void **) (void *)
                &ventptr->nghbtax,    (size_t) (meshptr->enttnbr * sizeof (MeshEnttNghb *)),
                &ventptr->mvrttax, (size_t) (ventptr->vertnbr * sizeof (Gnum)),
                &ventptr->venttax,    (size_t) (ventptr->vertnbr * sizeof (Gnum)),
                &ventptr->svrttax, (size_t) (ventptr->vertnbr * sizeof (Gnum)),
                (void *) NULL) == NULL) {
            errorPrint  ("out of memory");
            chekval = 1;
          }
          CHECK_VDBG2 (chekval);

          ventptr->nghbtax -= baseval;
          ventptr->mvrttax -= baseval;
          ventptr->venttax -= baseval;
          ventptr->svrttax -= baseval;
          break;

        case PAMPA_ENTT_SINGLE : /* Entity without sub-entities */

          /* Alate private entity neighbors data*/
          if (memAllocGroup ((void **) (void *)
                &ventptr->nghbtax, (size_t) (meshptr->enttnbr * sizeof (MeshEnttNghb *)),
                &ventptr->mvrttax, (size_t) (ventptr->vertnbr * sizeof (Gnum)),
                (void *) NULL) == NULL) {
            errorPrint  ("out of memory");
            chekval = 1;
          }
          CHECK_VDBG2 (chekval);

          ventptr->nghbtax -= baseval;
          ventptr->mvrttax -= baseval;
          ventptr->venttax = NULL;
          ventptr->svrttax = NULL;
          break;

        default :

          /* Alate private entity neighbors data*/
          if (memAllocGroup ((void **) (void *)
                &ventptr->venttax,    (size_t) (ventptr->vertnbr * sizeof (Gnum)),
                &ventptr->svrttax, (size_t) (ventptr->vertnbr * sizeof (Gnum)),
                (void *) NULL) == NULL) {
            errorPrint  ("out of memory");
            chekval = 1;
          }
          CHECK_VDBG2 (chekval);

          ventptr->mvrttax = NULL;
          ventptr->venttax -= baseval;
          ventptr->svrttax -= baseval;
      }

      if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { /* If ventnum is not a sub-entity */
        for (vertnum = baseval, vertnnd = ventptr->vertnbr; vertnum < vertnnd; vertnum ++)
          chekval |= (fscanf (stream, GNUMSTRING "\t", &ventptr->mvrttax[vertnum]) == EOF);
        chekval |= (fscanf (stream, "\n") == EOF);
      }

      if (meshptr->esubbax[ventnum & meshptr->esubmsk] != PAMPA_ENTT_SINGLE) { /* If ventnum is not a single entity */
        for (vertnum = baseval, vertnnd = ventptr->vertnbr; vertnum < vertnnd; vertnum ++)
          chekval |= (fscanf (stream, GNUMSTRING "\t", &ventptr->svrttax[vertnum]) == EOF);
        chekval |= (fscanf (stream, "\n") == EOF);
        for (vertnum = baseval, vertnnd = ventptr->vertnbr; vertnum < vertnnd; vertnum ++)
          chekval |= (fscanf (stream, GNUMSTRING "\t", &ventptr->venttax[vertnum]) == EOF);
        chekval |= (fscanf (stream, "\n") == EOF);
      }

      chekval |= (fscanf (stream, "%d\n", &tmp) == EOF); 
      if (tmp == 1) { //if (meshptr->vprmtax != NULL) 
        if ((ventptr->vprmtax = (Gnum *) memAlloc (sizeof (Gnum) * ventptr->vertnbr)) == NULL) {
          errorPrint  ("out of memory");
          return      (1);
        }
        ventptr->vprmtax -= meshptr->baseval;

        for (vertnum = baseval; vertnum < ventptr->vertnbr; vertnum ++)
          chekval |= (fscanf (stream, GNUMSTRING "\t", &ventptr->vprmtax[vertnum]) == EOF);
        chekval |= (fscanf (stream, "\n") == EOF);
      }
      else
        ventptr->vprmtax = NULL;

    }
    else
      meshptr->entttax[ventnum] = NULL;
  }

  for (ventnum = baseval, ventnnd = meshptr->enttnbr + baseval; ventnum < ventnnd; ventnum ++) {
    MeshEntity * restrict ventptr;
    Gnum vertnum;
    Gnum nentnum;
    Gnum nentnnd;

    if (meshptr->entttax[ventnum] != NULL) {
      ventptr = meshptr->entttax[ventnum];
      if (meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) { /* If ventnum is a sub-entity */
        ventptr->mvrttax = meshptr->entttax[meshptr->esubbax[ventnum & meshptr->esubmsk]]->mvrttax;
        ventptr->nghbtax = meshptr->entttax[meshptr->esubbax[ventnum & meshptr->esubmsk]]->nghbtax;
      }
      else {

        for (nentnum = baseval, nentnnd = meshptr->enttnbr + baseval; nentnum < nentnnd; nentnum ++) {
          MeshEnttNghb * restrict nghbptr;

          if ((meshptr->entttax[nentnum] != NULL) || (nentnum == baseval) || (nentnum == nentnnd - 1)) {
            chekval |= (fscanf (stream, "%d\n", &tmp) == EOF); 

            if (tmp == 1) { //if (ventptr->nghbtax[nentnum] != NULL)
              if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { /* If ventnum is not a sub-entity */
                if ((ventptr->nghbtax[nentnum] = (MeshEnttNghb *) memAlloc (sizeof (MeshEnttNghb))) == NULL) {
                  errorPrint  ("out of memory");
                  chekval = 1;
                }
                CHECK_VDBG2 (chekval);

                nghbptr = ventptr->nghbtax[nentnum];

                if (meshptr->esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { /* If nentnum is not a sub-entity */
                  /* Alate centralized entity private data */
                  if (memAllocGroup ((void **) (void *)
                        &nghbptr->vindtax, (size_t) (ventptr->vertnbr * sizeof (MeshVertBounds)),
                        (void *) NULL) == NULL) {
                    errorPrint  ("out of memory");
                    chekval = 1;
                  }
                  CHECK_VDBG2 (chekval);

                  nghbptr->vindtax -= baseval;

                  for (vertnum = baseval; vertnum < ventptr->vertnbr; vertnum ++)
                    fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\t", 
                        &nghbptr->vindtax[vertnum].vertidx,
                        &nghbptr->vindtax[vertnum].vendidx);
                  chekval |= (fscanf (stream, "\n") == EOF);
                }
              }
            }
          }
          else
            ventptr->nghbtax[nentnum] = NULL;
        }
      }
    }
  }

  for (ventnum = baseval, ventnnd = meshptr->enttnbr + baseval; ventnum < ventnnd; ventnum ++) {
    Gnum nentnum;
    Gnum nentnnd;

    if ((meshptr->entttax[ventnum] != NULL) && (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE)) /* If ventnum is not a sub-entity */
      for (nentnum = baseval, nentnnd = meshptr->enttnbr + baseval; nentnum < nentnnd; nentnum ++) 
        if ((meshptr->entttax[nentnum] != NULL) && (meshptr->esubbax[nentnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE)) /* If nentnum is a sub-entity */
          meshptr->entttax[ventnum]->nghbtax[nentnum]->vindtax =
            meshptr->entttax[ventnum]->nghbtax[meshptr->esubbax[nentnum & meshptr->esubmsk]]->vindtax;
  }

  CHECK_VDBG2 (chekval);
  CHECK_FDBG2 (valuesInit(&meshptr->valsptr, meshptr->baseval, valumax, meshptr->enttnbr));
  return (meshLoadValues (meshptr, stream));
}

