!*  Copyright 2009-2016 Inria
!*
!* This file is part of the PaMPA software package for parallel
!* mesh partitioning and adaptation.
!*
!* PaMPA is free software: you can redistribute it and/or modify
!* it under the terms of the GNU General Public License as published by
!* the Free Software Foundation, either version 3 of the License, or
!* any later version.
!* 
!* PaMPA is distributed in the hope that it will be useful,
!* but WITHOUT ANY WARRANTY; without even the implied warranty of
!* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!* GNU General Public License for more details.
!* 
!* In this respect, the user's attention is drawn to the risks associated
!* with loading, using, modifying and/or developing or reproducing the
!* software by the user in light of its specific status of free software,
!* that may mean that it is complicated to manipulate, and that also
!* therefore means that it is reserved for developers and experienced
!* professionals having in-depth computer knowledge. Users are therefore
!* encouraged to load and test the software's suitability as regards
!* their requirements in conditions enabling the security of their
!* systems and/or data to be ensured and, more generally, to use and
!* operate it in the same conditions as regards security.
!* 
!* The fact that you are presently reading this means that you have had
!* knowledge of the GPLv3 license and that you accept its terms.
!*
!************************************************************
!>
!>    \file        library_iter.F90
!>
!>    \authors     Cedric Lachat
!>
!>    \brief       This module is the API for the libPampa
!>                 library.
!>
!>    \date        Version 1.0: from: 21 Mar 2011
!>                              to:   30 Sep 2016
!>                 Version 2.0: from:  7 Oct 2016
!>                              to:   12 May 2017
!>
!************************************************************

#include "commonf.h"
#include "iterf.h"
#define PAMPAF_BUILD_ITER
#ifdef PAMPAF_DEBUG_ITER
#undef PAMPAF_DEBUG_ITER
#include "libraryf.h"
#define PAMPAF_DEBUG_ITER
#else /* PAMPAF_DEBUG_ITER */
#include "libraryf.h"
#endif /* PAMPAF_DEBUG_ITER */
#undef PAMPAF_BUILD_ITER

MODULE LIBRARY_ITER

USE ISO_C_BINDING
USE LIBRARY_TYPES

IMPLICIT NONE



CONTAINS

!> \brief This routine initialize the iterator by using vertex number.
SUBROUTINE PAMPAF_itStart(&
	iterptr,&                                       !< Iterator to fill in
	vertnum,&                                       !< Vertex number
	retval&                                         !< Return value : 0 on success and !0 on error
	)


TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

INTEGER(KIND=PAMPAF_NUM), INTENT(IN) :: vertnum
INTEGER, INTENT(OUT) :: retval

! XXX TODO et si vertnum est plus grand que la taille svrttab ?
IF (ASSOCIATED(iterptr%SVRTTAB)) THEN
  iterptr%VERTNUM(1) = iterptr%VINDTAB(iterptr%SVRTTAB(vertnum) * 2 - 1)
  iterptr%VERTNND(1) = iterptr%VINDTAB(iterptr%SVRTTAB(vertnum) * 2)
#ifdef PAMPAF_DEBUG_ITER
  iterptr%VINDVAL = iterptr%SVRTTAB(vertnum) * 2 - 1
#endif /* PAMPAF_DEBUG_ITER */
ELSE
  iterptr%VERTNUM(1) = iterptr%VINDTAB(vertnum * 2 - 1)
  iterptr%VERTNND(1) = iterptr%VINDTAB(vertnum * 2)
#ifdef PAMPAF_DEBUG_ITER
  iterptr%VINDVAL = vertnum * 2 - 1
#endif /* PAMPAF_DEBUG_ITER */
END IF

IF (ASSOCIATED(iterptr%NENTBAS, iterptr%NENTTAB)) THEN
  DO WHILE (((iterptr%VERTNND(1) - iterptr%VERTNUM(1)) > 0) .AND. &
  (iterptr%NENTTAB(iterptr%EDGETAB(iterptr%VERTNUM(1))) /= iterptr%NESBNUM))
  iterptr%VERTNUM(1) = iterptr%VERTNUM(1) + 1
  END DO
  DO WHILE (((iterptr%VERTNND(1) - iterptr%VERTNUM(1)) > 1) .AND. &
  (iterptr%NENTTAB(iterptr%EDGETAB(iterptr%VERTNND(1) - 1)) /= iterptr%NESBNUM))
  iterptr%VERTNND(1) = iterptr%VERTNND(1) - 1
  END DO
END IF

retval = 0

END SUBROUTINE

!> \brief This routine initialize the iterator by using vertex number.
SUBROUTINE PAMPAF_itThreadsStart(&
	iterptr,&                                       !< Iterator to fill in
	vertnum,&                                       !< Vertex number
	thrdnum,&                                       !< Thread number
	retval&                                         !< Return value : 0 on success and !0 on error
	)


TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

INTEGER(KIND=PAMPAF_NUM), INTENT(IN) :: vertnum, thrdnum
INTEGER, INTENT(OUT) :: retval

IF (ASSOCIATED(iterptr%SVRTTAB)) THEN
  iterptr%VERTNUM(thrdnum) = iterptr%VINDTAB(iterptr%SVRTTAB(vertnum) * 2 - 1)
  iterptr%VERTNND(thrdnum) = iterptr%VINDTAB(iterptr%SVRTTAB(vertnum) * 2)
#ifdef PAMPAF_DEBUG_ITER
  iterptr%VINDVAL = iterptr%SVRTTAB(vertnum) * 2 - 1
#endif /* PAMPAF_DEBUG_ITER */
ELSE
  iterptr%VERTNUM(thrdnum) = iterptr%VINDTAB(vertnum * 2 - 1)
  iterptr%VERTNND(thrdnum) = iterptr%VINDTAB(vertnum * 2)
#ifdef PAMPAF_DEBUG_ITER
  iterptr%VINDVAL = vertnum * 2 - 1
#endif /* PAMPAF_DEBUG_ITER */
END IF

IF (ASSOCIATED(iterptr%NENTBAS, iterptr%NENTTAB)) THEN
  DO WHILE (((iterptr%VERTNND(thrdnum) - iterptr%VERTNUM(thrdnum)) > 0) .AND. &
  (iterptr%NENTTAB(iterptr%EDGETAB(iterptr%VERTNUM(thrdnum))) /= iterptr%NESBNUM))
  iterptr%VERTNUM(thrdnum) = iterptr%VERTNUM(thrdnum) + 1
  END DO
  DO WHILE (((iterptr%VERTNND(thrdnum) - iterptr%VERTNUM(thrdnum)) > 1) .AND. &
  (iterptr%NENTTAB(iterptr%EDGETAB(iterptr%VERTNND(thrdnum) - 1)) /= iterptr%NESBNUM))
  iterptr%VERTNND(thrdnum) = iterptr%VERTNND(thrdnum) - 1
  END DO
END IF

retval = 0

END SUBROUTINE



!> \brief This routine test if the vertex in the iterator has more neighbors.
LOGICAL FUNCTION PAMPAF_itHasMoreDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

#ifdef PAMPAF_DEBUG_ITER
  IF ((iterptr%VENTNUM /= iterptr%NESBNUM)&
  .AND. ((iterptr%VERTNND(1) .GT. iterptr%VINDTAB(iterptr%VINDVAL+1))&
  .OR. (iterptr%VERTNUM(1) .LT. iterptr%VINDTAB(iterptr%VINDVAL)))) THEN
    WRITE(0,*) "PAMPAF_itHasMore: invalid index of vertnum or vertnnd"
    write(0,*) iterptr%VERTNND, iterptr%VERTNUM, iterptr%VINDTAB(iterptr%VINDVAL), iterptr%VINDTAB(iterptr%VINDVAL+1)
    CALL EXIT(1)
  ENDIF
#endif /* PAMPAF_DEBUG_ITER */
        PAMPAF_itHasMoreDebug = PAMPAF_itHasMore(iterptr)

END FUNCTION

!> \brief This routine returns the next vertex or neighbor in the iterator.
SUBROUTINE PAMPAF_itNextDebug(&
	itptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: itptr

#ifdef PAMPAF_DEBUG_ITER
  IF ((itptr%VENTNUM /= itptr%NESBNUM)&
  .AND.  ((itptr%VERTNND(1) .GT. itptr%VINDTAB(itptr%VINDVAL+1))&
  .OR. (itptr%VERTNUM(1) .GE.  itptr%VINDTAB(itptr%VINDVAL+1))&
  .OR. (itptr%VERTNUM(1) .LT. itptr%VINDTAB(itptr%VINDVAL)))) THEN
    WRITE(0,*) "PAMPAF_itNext: invalid index of vertnum or vertnnd"
    CALL EXIT(1)
  ENDIF
#endif /* PAMPAF_DEBUG_ITER */
        PAMPAF_itNext(itptr)

END SUBROUTINE

!> \brief This routine gives the current number of vertex by entity.
INTEGER(KIND=PAMPA_C_INT) FUNCTION PAMPAF_itCurEnttVertNumDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

        PAMPAF_itCurEnttVertNumDebug = PAMPAF_itCurEnttVertNum(iterptr)

END FUNCTION

!> \brief This routine gives the current number of vertex by mesh.
INTEGER(KIND=PAMPA_C_INT) FUNCTION PAMPAF_itCurMeshVertNumDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

#ifdef PAMPAF_DEBUG_ITER
		! The following test may use unitialized values, maybe iterptr%MNGBTAB
        !IF ((iterptr%VERTNUM(1) .GT. size(iterptr%EDGETAB)) .OR. (iterptr%EDGETAB(iterptr%VERTNUM) .GT. size(iterptr%MNGBTAB))) THEN
        !  WRITE(0,*) "PAMPAF_itCurMeshVertNum: internal error"
        !  CALL EXIT(1)
        !ENDIF
#endif /* PAMPAF_DEBUG_ITER */
        PAMPAF_itCurMeshVertNumDebug = PAMPAF_itCurMeshVertNum(iterptr)

END FUNCTION

!> \brief This routine gives the current number of vertex by sub-entity.
INTEGER(KIND=PAMPA_C_INT) FUNCTION PAMPAF_itCurSubEnttVertNumDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

        PAMPAF_itCurSubEnttVertNumDebug = PAMPAF_itCurSubEnttVertNum(iterptr)

END FUNCTION

!> \brief This routine gives the number of entity of the current vertex
INTEGER(KIND=PAMPA_C_INT) FUNCTION PAMPAF_itCurEnttNumDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

        PAMPAF_itCurEnttNumDebug = PAMPAF_itCurEnttNum(iterptr)

END FUNCTION

!> \brief This routine gives the number of sub-entity of the current vertex
INTEGER(KIND=PAMPA_C_INT) FUNCTION PAMPAF_itCurSubEnttNumDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

        PAMPAF_itCurSubEnttNumDebug = PAMPAF_itCurSubEnttNum(iterptr)

END FUNCTION

!> \brief This routine gives if the vertex belongs to a sub-entity
LOGICAL FUNCTION PAMPAF_itHasSubEnttDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

        PAMPAF_itHasSubEnttDebug = PAMPAF_itHasSubEntt(iterptr)

END FUNCTION

!> \brief This routine gives if the vertex belongs to a sub-entity and iterator
!> created with this sub-entity
LOGICAL FUNCTION PAMPAF_itIsSubEnttDebug(&
	iterptr&                                        !< Iterator
	)

        TYPE(PAMPAF_ITERATOR), INTENT(INOUT) :: iterptr

        PAMPAF_itIsSubEnttDebug = PAMPAF_itIsSubEntt(iterptr)

END FUNCTION

END MODULE
