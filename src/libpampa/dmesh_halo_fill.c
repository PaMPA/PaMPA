/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_halo_fill.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the halo update
//!                routines. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

//! This function fills the send array used by
//! all of the halo routines.
//! It returns:
//! - void  : in all cases.

static
void
DMESHHALOFILLNAME (
const DmeshEnttComm * restrict const commlocptr,
const void * restrict const   attrgsttab,         //!< Attribute array to diffuse
#ifdef DMESHHALOFILLFLAG
const int                     attrglbsiz,         //!< Type extent of attribute
#endif /* DMESHHALOFILLFLAG */
byte ** restrict const        attrdsptab)         //!< Temporary address displacement array
{
  byte * restrict       attrgstptr;
  const int * restrict  procsidptr;
  const int * restrict  procsidnnd;

  for (procsidptr = commlocptr->procsidtab, procsidnnd = procsidptr + commlocptr->procsidnbr, attrgstptr = (byte *) attrgsttab;
       procsidptr < procsidnnd; procsidptr ++) {
    int                 procsidval;

    procsidval = *procsidptr;
    if (procsidval < 0)
      attrgstptr -= procsidval * DMESHHALOFILLSIZE;
    else {
      byte *              attrdspptr;

      attrdspptr = attrdsptab[procsidval];
      attrdsptab[procsidval] = attrdspptr + DMESHHALOFILLSIZE; /* Skip to next position in send buffer */
      DMESHHALOFILLCOPY (attrdspptr, attrgstptr, DMESHHALOFILLSIZE);
    }
  }
}
