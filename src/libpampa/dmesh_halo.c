/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_halo.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the halo update
//!                routines. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define DMESH_HALO

#include "module.h"
#include "common.h"
#include "comm.h"
#include "pampa.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_halo.h"
#include <pthread.h>


//! These routines fill the send arrays used by
//! all of the halo routines.
//! They return:
//! - void  : in all cases.

#define DMESHHALOFILLNAME          dmeshHaloFillGeneric
#define DMESHHALOFILLFLAG
#define DMESHHALOFILLSIZE          attrglbsiz
#define DMESHHALOFILLCOPY(d,s,n)   memCpy ((d), (s), (n))
#include "dmesh_halo_fill.c"
#undef DMESHHALOFILLNAME
#undef DMESHHALOFILLFLAG
#undef DMESHHALOFILLSIZE
#undef DMESHHALOFILLCOPY

#define DMESHHALOFILLNAME          dmeshHaloFillGnum
#define DMESHHALOFILLSIZE          sizeof (Gnum)
#define DMESHHALOFILLCOPY(d,s,n)   *((Gnum *) (d)) = *((Gnum *) (s))
#include "dmesh_halo_fill.c"
#undef DMESHHALOFILLNAME
#undef DMESHHALOFILLSIZE
#undef DMESHHALOFILLCOPY

#define DMESHHALOFILLNAME          dmeshHaloFillInt //!< In case Gnum is not int
#define DMESHHALOFILLSIZE          sizeof (int)
#define DMESHHALOFILLCOPY(d,s,n)   *((int *) (d)) = *((int *) (s))
#include "dmesh_halo_fill.c"
#undef DMESHHALOFILLNAME
#undef DMESHHALOFILLSIZE
#undef DMESHHALOFILLCOPY

//! \brief This function, used internally, fills send data
//! \returns VOID : in any case
static
void
dmeshHaloFill (
const Dmesh * restrict const meshptr,
const DmeshEnttComm * restrict const commlocptr,
const void * restrict const   attrgsttab,         //!< [inout] Attribute array to diffuse
int                           attrglbsiz,         //!< [in] Type extent of attribute
byte * restrict const         attrsndtab,         //!< [in] Array for packing data to send
int * const                   senddsptab,         //!< [out] Temporary displacement array
const int * restrict const    sendcnttab)         //!< [in] Count array
{
  int                 procnum;
  byte **             attrdsptab;

  attrdsptab = (byte **) senddsptab;              /* TRICK: use senddsptab (int *) as attrdsptab (byte **) */
  attrdsptab[0] = attrsndtab;                     /* Pre-set send arrays for send buffer filling routines */
  for (procnum = 1; procnum < meshptr->procglbnbr; procnum ++)
    attrdsptab[procnum] = attrdsptab[procnum - 1] + sendcnttab[procnum - 1] * attrglbsiz;

  if (attrglbsiz == sizeof (Gnum))
    dmeshHaloFillGnum (commlocptr, attrgsttab, attrdsptab);
  else if (attrglbsiz == sizeof (int))            /* In case Gnum is not int */
    dmeshHaloFillInt (commlocptr, attrgsttab, attrdsptab);
  else                                            /* Generic but slower fallback routine */
    dmeshHaloFillGeneric (commlocptr, attrgsttab, attrglbsiz, attrdsptab);

  senddsptab[0] = 0;                              /* Pre-set send arrays for data sending routines */
  for (procnum = 1; procnum < meshptr->procglbnbr; procnum ++)
    senddsptab[procnum] = senddsptab[procnum - 1] + sendcnttab[procnum - 1];
}

//! \brief This function checks that the data of proc{snd,rcv}tab
//! are consistent.
//! \returns  0 : on success
//! \returns !0 : on error

#ifdef PAMPA_DEBUG_DMESH2
int
dmeshHaloCheck (
const Dmesh * restrict const meshptr, 			 //!< [in] Mesh pointer
const DmeshEnttComm * restrict const commlocptr)	 //!< [in] Communicator data pointer
{
  int *               proctab;                    /* Array to collect data */
  int                 procnum;
  int                 o;

  if ((proctab = memAlloc (meshptr->procglbnbr * sizeof (int))) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }

  if (commAlltoall (commlocptr->procsndtab, 1, MPI_INT, proctab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    memFree    (proctab);                         /* Free group leader */
    return     (1);
  }

  o = 0;
  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    if (proctab[procnum] != commlocptr->procrcvtab[procnum]) {
      errorPrint ("data error");
      o = 1;
      break;
    }
  }

  memFree (proctab);                              /* Free group leader */

  return  (o);
}
#endif /* PAMPA_DEBUG_DMESH2 */

//! \brief These functions perform a synchronous collective
//! halo diffusion operation on the ghost or overlap array given
//! on input.
//!
//! \returns 0   : if the halo has been successfully propagated.
//! \returns !0  : on error.

static
int
dmeshHaloSync3 (
Dmesh * restrict const       meshptr,             //!< [in] Mesh pointer
const DmeshEnttComm * restrict const commlocptr,  //!< [in] Communicator data pointer
const Gnum                    vertlocnbr,         //!< [in] Local number of vertices
void * restrict const         attrgsttab,         //!< [out] Attribute array to share
const MPI_Datatype            attrglbtype,        //!< [in] Attribute datatype
byte ** const                 attrsndptr,         //!< [out] Pointer to array for packing data to send
int ** const                  senddspptr,         //!< [out] Pointer to send displacement arrays
int ** const                  recvdspptr,         //!< [out] Pointer to receive displacement array 
MPI_Request ** const          requptr)            //!< [out] Pointer to local request array for point-to-point
{
  MPI_Aint            attrglbsiz;                 /* Extent of attribute datatype */
  MPI_Aint            dummy;
  MPI_Aint            lb;
  int                 procngbsiz;                 /* Size of request array for point-to-point communications */
  int                 procngbnum;

  // XXX mettre un autre test à la place…
  //if (dmeshGhst (meshptr) != 0) {                /* Compute ghost edge array if not already present */
  //  errorPrint ("cannot compute ghost edge array");
  //  return     (1);
  //}

  procngbsiz = ((meshptr->flagval & DMESHCOMMPTOP) != 0) ? commlocptr->procngbnbr : 0;

  MPI_Type_get_extent (attrglbtype, &dummy, &attrglbsiz);     /* Get type extent */
  if ((*attrsndptr = memAlloc (commlocptr->procsndnbr * attrglbsiz)) == NULL) {
    errorPrint ("out of memory");
    return (1);
  }

  if (memAllocGroup ((void **) (void *)
                     senddspptr, (size_t) (meshptr->procglbnbr * MAX (sizeof (int), sizeof (byte *))), /* TRICK: use senddsptab (int *) as attrdsptab (byte **) */
                     recvdspptr, (size_t) (meshptr->procglbnbr * sizeof (int)),
                     (void *) NULL) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }
  if ((*requptr = memAlloc (procngbsiz * 2 * sizeof (MPI_Request))) == NULL) {
    errorPrint ("out of memory");
    return (1);
  }


  dmeshHaloFill (meshptr, commlocptr, attrgsttab, attrglbsiz, *attrsndptr, *senddspptr, commlocptr->procsndtab); /* Fill data arrays */

  (*recvdspptr)[0] = (int) vertlocnbr;
  for (procngbnum = 1; procngbnum < meshptr->procglbnbr; procngbnum ++)
    (*recvdspptr)[procngbnum] = (*recvdspptr)[procngbnum - 1] + commlocptr->procrcvtab[procngbnum - 1];

#ifdef PAMPA_DEBUG_DMESH2
  if (dmeshHaloCheck (meshptr, commlocptr) != 0) {
    errorPrint ("invalid communication data");
    return     (1);
  }
#endif /* PAMPA_DEBUG_DMESH2 */

  return (0);
}

//! \brief Perform multiple synchronous halo
//! \returns  0 : on success.
//! \returns !0 : on error.
static
int
dmeshHaloMultSync3 (
DmeshHaloRequests * const reqsptr, 
int ** const   senddspptr,                        //!< Pointer to sending displacement arrays
int ** const   recvdspptr)		          //!< Pointer to receiving displacement arrays
{
  MPI_Aint            attrglbsiz;                 /* Extent of attribute datatype */
  MPI_Aint            dummy;
  Gnum                preqnum;
  Gnum                baseval;
  int                 procngbsiz;                 /* Size of request array for point-to-point communications */
  int                 procngbnum;
  int                 ptopflgval;

  baseval = reqsptr->meshptr->baseval;
  ptopflgval = reqsptr->meshptr->flagval & DMESHCOMMPTOP;
  for (preqnum = 0, procngbsiz = 0; preqnum < reqsptr->preqnbr; preqnum ++) {
    const DmeshEnttComm * restrict commlocptr;
    const DmeshEntity * restrict   enttlocptr;

    enttlocptr = reqsptr->preqtab[preqnum].enttlocptr;
    commlocptr = &enttlocptr->commlocdat;

    procngbsiz += (ptopflgval != 0) ? commlocptr->procngbnbr : 0;

    MPI_Type_get_extent (reqsptr->preqtab[preqnum].attrglbtype, &dummy, &attrglbsiz);     /* Get type extent */
    if ((reqsptr->preqtab[preqnum].attrsndtab = memAlloc (commlocptr->procsndnbr * attrglbsiz)) == NULL) {
      errorPrint ("out of memory");
      return (1);
    }

    if (memAllocGroup ((void **) (void *)
          &senddspptr[preqnum], (size_t) (reqsptr->meshptr->procglbnbr * MAX (sizeof (int), sizeof (byte *))), /* TRICK: use senddsptab (int *) as attrdsptab (byte **) */
          &recvdspptr[preqnum], (size_t) (reqsptr->meshptr->procglbnbr * sizeof (int)),
          (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      return     (1);
    }

    dmeshHaloFill (reqsptr->meshptr, commlocptr, reqsptr->preqtab[preqnum].attrgsttab, attrglbsiz, reqsptr->preqtab[preqnum].attrsndtab, senddspptr[preqnum], commlocptr->procsndtab); /* Fill data arrays */

    recvdspptr[preqnum][0] = (int) enttlocptr->vertlocnbr;
    for (procngbnum = 1; procngbnum < reqsptr->meshptr->procglbnbr; procngbnum ++)
      recvdspptr[preqnum][procngbnum] = recvdspptr[preqnum][procngbnum - 1] + commlocptr->procrcvtab[procngbnum - 1];

#ifdef PAMPA_DEBUG_DMESH2
    if (dmeshHaloCheck (reqsptr->meshptr, commlocptr) != 0) {
      errorPrint ("invalid communication data");
      return     (1);
    }
#endif /* PAMPA_DEBUG_DMESH2 */
  }
  reqsptr->mreqnbr = procngbsiz * 2;
  if ((reqsptr->mreqtab = memAlloc (reqsptr->mreqnbr * sizeof (MPI_Request))) == NULL) {
    errorPrint ("out of memory");
    reqsptr->mreqnbr = 0;
    return (1);
  }

  return (0);
}

//! \brief Exchange data on specific entity
//! \returns  0 : on success.
//! \returns !0 : on error.
//! \todo Merge this function with dmeshHaloMultSync() whenever multiple
//! communications will be done with MPI_Alltoall()

int
dmeshHaloSync (
Dmesh * restrict const       meshptr,             //!< [in] Source mesh pointer 
DmeshEntity const * restrict const enttlocptr,	  //!< [in] Entity pointer 
void * restrict const         attrgsttab,         //!< [inout] Data to exchange
const MPI_Datatype            attrglbtype)        //!< [in] Data type 
{
  int o;
  byte * attrsndtab;
  o = dmeshHaloSync2 (meshptr, enttlocptr, &attrsndtab, attrgsttab, attrglbtype, NULL);
  if (attrsndtab != NULL)
    memFree (attrsndtab);
  return (o);
}

//! \brief Common function tu synchronous and asynchronous communication
//! \returns 0 : on success.
//! \returns !0 : on error.
int
dmeshHaloSync2 (
Dmesh * restrict const       meshptr,             //!< [in] Source mesh pointer
DmeshEntity const * restrict const enttlocptr,	  //!< [in] Entity pointer
byte **                       attrsndptr,         //!< [in] Array for packing data to send
void * restrict const         attrgsttab,         //!< [inout] Attribute array to share
const MPI_Datatype            attrglbtype,        //!< [in] Attribute datatype
MPI_Request **                requptr)            //!< [inout] Request pointer
{
  int *               senddsptab;
  int *               recvdsptab;
  MPI_Request *       requtab;
  MPI_Request **      lreqptr;
  int                 o;

  if (requptr == NULL)
    lreqptr = &requtab;
  else
    lreqptr = requptr;
  if (dmeshHaloSync3 (meshptr, &enttlocptr->commlocdat, enttlocptr->vertlocnbr, attrgsttab, attrglbtype, attrsndptr, &senddsptab, &recvdsptab, lreqptr) != 0) /* Prepare communication arrays */
    return (1);

  o = 0;                                          /* Assume success */
  if ((meshptr->flagval & DMESHCOMMPTOP) != 0) { /* If point-to-point exchange   */
    MPI_Aint              attrglbsiz;             /* Extent of attribute datatype */
    MPI_Aint              dummy;
    const int * restrict  procrcvtab;
    const int * restrict  procsndtab;
    const int * restrict  procngbtab;
    int                   procngbnbr;
    int                   procngbnum;
    MPI_Comm              proccomm;
    int                   requnbr;

    proccomm   = meshptr->proccomm;
    procngbtab = enttlocptr->commlocdat.procngbtab;
    procngbnbr = enttlocptr->commlocdat.procngbnbr;
    procrcvtab = enttlocptr->commlocdat.procrcvtab;
    MPI_Type_get_extent (attrglbtype,&dummy, &attrglbsiz);   /* Get type extent */
    for (procngbnum = procngbnbr - 1, requnbr = 0; procngbnum >= 0; procngbnum --, requnbr ++) { /* Post receives first */
      int                 procglbnum;

      procglbnum = procngbtab[procngbnum];
      if (commIrecv ((byte *) attrgsttab + recvdsptab[procglbnum] * attrglbsiz, procrcvtab[procglbnum],
                     attrglbtype, procglbnum, TAGHALO, proccomm, (*lreqptr) + requnbr) != MPI_SUCCESS) {
        errorPrint ("communication error");
        o = 1;
        break;
      }
    }

    procsndtab = enttlocptr->commlocdat.procsndtab;
    for (procngbnum = 0; procngbnum < procngbnbr; procngbnum ++, requnbr ++) { /* Post sends afterwards */
      int                 procglbnum;

      procglbnum = procngbtab[procngbnum];
      if (commIsend ((*attrsndptr) + senddsptab[procglbnum] * attrglbsiz, procsndtab[procglbnum],
                     attrglbtype, procglbnum, TAGHALO, proccomm, (*lreqptr) + requnbr) != MPI_SUCCESS) {
        errorPrint ("communication error");
        o = 1;
        break;
      }
    }
    if ((requptr == NULL) && (commWaitall (requnbr, (*lreqptr), MPI_STATUSES_IGNORE) != MPI_SUCCESS)) {
      errorPrint ("communication error");
      o = 1;
    }
  }
  else {                                          /* Collective communication */
    if (commAlltoallv (*attrsndptr, enttlocptr->commlocdat.procsndtab, senddsptab, attrglbtype, /* Perform diffusion */
                            attrgsttab, enttlocptr->commlocdat.procrcvtab, recvdsptab, attrglbtype,
                            meshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            o = 1;
    }
    memFree (*attrsndptr);                           /* Free group leader */
    *attrsndptr = NULL;
  }

  memFreeGroup (senddsptab);                           /* Free group leader */
  if (requptr == NULL)
    memFree (*lreqptr);

  return (o);
}

//! \brief Common function used by synchronous and asynchronous multiple
//! communications
//! \returns  0 : on success.
//! \returns !0 : on error.
int
dmeshHaloMultSync2 (
DmeshHaloRequests * const reqsptr)                //!< [inout] Request pointer
{
  Gnum                  preqnum;
  int **              senddspptr;
  int **              recvdspptr;
  int                 o;

  if (memAllocGroup ((void **) (void *)
                     &senddspptr, (size_t) (reqsptr->preqnbr * sizeof (int *)),
                     &recvdspptr, (size_t) (reqsptr->preqnbr * sizeof (int *)),
                     (void *) NULL) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }

  if (dmeshHaloMultSync3 (reqsptr, senddspptr, recvdspptr) != 0) /* Prepare communication arrays */
    return (1);

  o = 0;                                          /* Assume success */
  if ((reqsptr->meshptr->flagval & DMESHCOMMPTOP) != 0) { /* If point-to-point exchange   */
    MPI_Comm              proccomm;
    int                   mreqnbr;

    proccomm   = reqsptr->meshptr->proccomm;
    for (preqnum = 0, mreqnbr = 0; preqnum < reqsptr->preqnbr; preqnum ++) {
      DmeshHaloRequest * restrict requptr;
      DmeshEntity const * restrict enttlocptr;
      MPI_Aint              attrglbsiz;             /* Extent of attribute datatype */
      MPI_Aint              dummy;
      const int * restrict  procrcvtab;
      const int * restrict  procsndtab;
      const int * restrict  procngbtab;
      int                   procngbnbr;
      int                   procngbnum;
      int                   tagnum;

      requptr = &reqsptr->preqtab[preqnum];
      tagnum = reqsptr->meshptr->enttglbnbr * requptr->tagnum + requptr->enttnum;
      enttlocptr = requptr->enttlocptr;
      procngbtab = enttlocptr->commlocdat.procngbtab;
      procngbnbr = enttlocptr->commlocdat.procngbnbr;
      procrcvtab = enttlocptr->commlocdat.procrcvtab;
      MPI_Type_get_extent (requptr->attrglbtype,&dummy, &attrglbsiz);   /* Get type extent */
      for (procngbnum = procngbnbr - 1; procngbnum >= 0; procngbnum --, mreqnbr ++) { /* Post receives first */
        int                 procglbnum;

        procglbnum = procngbtab[procngbnum];
        if (commIrecv ((byte *) requptr->attrgsttab + recvdspptr[preqnum][procglbnum] * attrglbsiz, procrcvtab[procglbnum],
              requptr->attrglbtype, procglbnum, tagnum, proccomm, reqsptr->mreqtab + mreqnbr) != MPI_SUCCESS) {
          errorPrint ("communication error");
          o = 1;
          break;
        }
      }

      procsndtab = enttlocptr->commlocdat.procsndtab;
      for (procngbnum = 0; procngbnum < procngbnbr; procngbnum ++, mreqnbr ++) { /* Post sends afterwards */
        int                 procglbnum;

        procglbnum = procngbtab[procngbnum];
        if (commIsend (requptr->attrsndtab + senddspptr[preqnum][procglbnum] * attrglbsiz, procsndtab[procglbnum],
              requptr->attrglbtype, procglbnum, tagnum, proccomm, reqsptr->mreqtab + mreqnbr) != MPI_SUCCESS) {
          errorPrint ("communication error");
          o = 1;
          break;
        }
      }
    }
  }
  else {                                          /* Collective communication */
    errorPrint ("not yet implemented, use PAMPA_COMM_PTOP flag");
    o = 1;
  }

  for (preqnum = 0; preqnum < reqsptr->preqnbr; preqnum ++) 
    memFreeGroup (senddspptr[preqnum]);
  memFreeGroup (senddspptr);                           /* Free group leader */

  return (o);
}

//! \brief Fill the request pointer. Used by synchronous and asynchronous
//! multiple communications.
//! \returns  0 : on success.
//! \returns !0 : on error.
int
dmeshHaloMultCommon (
Dmesh * restrict const       meshptr,			  //!< XXX
Gnum const                   valunbr,
Gnum const * restrict const  entttab,
Gnum const * restrict const  tagtab,
DmeshHaloRequests * const    reqsptr)
{
  Dvalues *                    valslocptr;
  Gnum                         valunum;
  Gnum                         valunnd;
  Gnum                         baseval;
  int                          cheklocval;

  cheklocval = 0;
  reqsptr->flagval = 1;               /* Assuming error */
  reqsptr->mreqtab = NULL;
  reqsptr->preqtab = NULL;
  reqsptr->preqnbr = valunbr;
  reqsptr->meshptr = meshptr;
  baseval = meshptr->baseval;
  valslocptr = meshptr->valslocptr;
  if ((reqsptr->preqtab = memAlloc (valunbr * sizeof (DmeshHaloRequest))) == NULL) {
    errorPrint ("out of memory");
    return (0);
  }
  
#ifdef PAMPA_DEBUG_DMESH3
    infoPrint ("valunbr: %d", valunbr);
#endif /* PAMPA_DEBUG_DMESH3 */
  for (valunum = reqsptr->meshptr->baseval, valunnd = valunbr + reqsptr->meshptr->baseval; valunum < valunnd; valunum ++) {
    DmeshHaloRequest * restrict requptr;
    Dvalue                   *valulocptr;

    valulocptr = valslocptr->evalloctak[entttab[valunum]];

    while (valulocptr !=  NULL) {
      if (valulocptr->tagnum == tagtab[valunum])
        break;
      valulocptr = valulocptr->next;
    }

    if (valulocptr == NULL) { // XXX peut-être mettre ce test dans un define de débogage
      errorPrint ("enttnum or tagnum are not correct");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
    if (valulocptr->flagval == PAMPA_VALUE_PRIVATE) { // XXX peut-être mettre ce test dans un define de débogage
      errorPrint ("private value could not be exchanged");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);



    requptr = &reqsptr->preqtab[valunum - baseval];
    requptr->enttnum = entttab[valunum];
    requptr->tagnum = tagtab[valunum];
    requptr->enttlocptr = meshptr->enttloctax[entttab[valunum]];
    requptr->attrgsttab = (byte *) valulocptr->valuloctab;
    requptr->attrglbtype = valulocptr->typeval;
#ifdef PAMPA_DEBUG_DMESH3
    infoPrint ("enttnum: %d, tagnum: %d, vertlocnbr: %d, vgstlocnbr: %d", requptr->enttnum, requptr->tagnum, requptr->enttlocptr->vertlocnbr, requptr->enttlocptr->vgstlocnbr);
#endif /* PAMPA_DEBUG_DMESH3 */
  }
  reqsptr->flagval = -1;               /* Assuming no error */

  return (0);
}

//! \brief This function exchange synchronously multiple data 
//! \returns  0 : on success.
//! \returns !0 : on error.
//! \todo Redundant functions betwen dmeshHalo* and dmeshHaloMult* for
//synchronous and asynchronous communications. 
int
dmeshHaloMultSync (
Dmesh * restrict const       meshptr,             //!< [in] Source mesh pointer
Gnum const                   valunbr,             //!< [in] Number of values
Gnum const * restrict const  entttab,             //!< [in] Entity array
Gnum const * restrict const  tagtab,              //!< [in] Tag array
DmeshHaloRequests * const    reqsptr)             //!< [inout] Requests pointer
{
  DmeshHaloRequests reqsdat;
  DmeshHaloRequests * lreqptr;
  Gnum preqnum;
  int o = 0;

  if (reqsptr == NULL)
    lreqptr = &reqsdat;
  else
    lreqptr = reqsptr;
  CHECK_FERR (dmeshHaloMultCommon (meshptr, valunbr, entttab, tagtab, lreqptr), meshptr->proccomm);

  CHECK_FERR (dmeshHaloMultSync2 (lreqptr), meshptr->proccomm);

  if (reqsptr == NULL) {
    if (commWaitall (lreqptr->mreqnbr, lreqptr->mreqtab, MPI_STATUSES_IGNORE) != MPI_SUCCESS) {
      errorPrint ("communication error");
      o = 1;
    }
    memFree (lreqptr->mreqtab);
  }

  if (reqsptr == NULL) {
    if (lreqptr->preqtab != NULL) {
      Gnum preqnum;

      for (preqnum = 0; preqnum < lreqptr->preqnbr; preqnum ++) {
        if (lreqptr->preqtab[preqnum].attrsndtab != NULL) {                /* Free group leader if it was successfully allocated before */
          memFree (lreqptr->preqtab[preqnum].attrsndtab);
          lreqptr->preqtab[preqnum].attrsndtab = NULL;
        }
      }
      memFree (lreqptr->preqtab);
      lreqptr->preqtab = NULL;
    }
  }

  return (o);
}

//! \brief This function performs an asynchronous collective
//! halo diffusion operation on the ghost array given
//! on input. It fills the given request structure with
//! the relevant data.
//!
//! \returns 0   : if the halo has been successfully propagated.
//! \returns !0  : on error.

#ifdef PAMPA_PTHREAD
static
void *
dmeshHaloAsync2 (
DmeshHaloRequests * restrict  requptr)            //!< [inout] Request pointer
{
  return ((void *) (intptr_t) dmeshHaloSync (requptr->meshptr, requptr->preqtab->enttlocptr, requptr->preqtab->attrgsttab, requptr->preqtab->attrglbtype));
}

//! \brief This function performs multiple asynchronous collective
//! halo diffusion operation on the ghost array given
//! on input. It fills the given request structure with
//! the relevant data.
//!
//! \returns 0   : if the halo has been successfully propagated.
//! \returns !0  : on error.
static
void *
dmeshHaloMultAsync2 (
DmeshHaloRequests * const reqsptr)                //!< [inout] Requests pointer
{
  int o = 0;
  o = dmeshHaloMultSync2 (reqsptr);
  if (commWaitall (reqsptr->mreqnbr, reqsptr->mreqtab, MPI_STATUSES_IGNORE) != MPI_SUCCESS) {
    errorPrint ("communication error");
    o = 1;
  }

  return ((void *) (intptr_t) o);
}
#endif /* PAMPA_PTHREAD */

void
dmeshHaloAsync (
Dmesh * restrict const      meshptr,			  //!< XXX
DmeshEntity * restrict const enttlocptr,    //!< XXX
void * restrict const       attrgsttab,         //!< Attribute array to share
const MPI_Datatype          attrglbtype,        //!< Attribute datatype
DmeshHaloRequests * restrict requptr)		  	  //!< XXX
{
#ifndef PAMPA_PTHREAD
#ifdef PAMPA_MPI_ASYNC_COLL
  int *               senddsptab;
  int *               recvdsptab;
#endif /* PAMPA_MPI_ASYNC_COLL */
#endif /* PAMPA_PTHREAD */

  requptr->flagval     = -1;                      /* Assume thread will be successfully launched */
  requptr->meshptr     = meshptr;
  requptr->mreqtab     = NULL;
  requptr->preqnbr     = 1;

  if ((requptr->preqtab = memAlloc (sizeof (DmeshHaloRequest))) == NULL) {
    errorPrint ("out of memory");
    return ;
  }

#ifdef PAMPA_PTHREAD
  requptr->preqtab->attrsndtab  = NULL;
  requptr->preqtab->attrgsttab  = attrgsttab;
  requptr->preqtab->attrglbtype = attrglbtype;
  requptr->preqtab->enttlocptr  = enttlocptr;

  if (pthread_create (&requptr->thrdval, NULL, (void * (*) (void *)) dmeshHaloAsync2, (void *) requptr) != 0) /* If could not create thread */
    requptr->flagval = (int) (intptr_t) dmeshHaloAsync2 (requptr); /* Call function synchronously */
#else /* PAMPA_PTHREAD */
#ifdef PAMPA_MPI_ASYNC_COLL
  requptr->flagval    = 1;                        /* Assume error */
  requptr->attrsndtab = NULL;                     /* No memory    */

  if (dmeshHaloSync3 (meshptr, enttlocptr, attrgsttab, attrglbtype, &requptr->preqtab->attrsndtab, &senddsptab, &recvdsptab) != 0) /* Prepare communication arrays */
    return;

  if (MPE_Ialltoallv (requptr->attrsndtab, enttlocptr->procsndtab, senddsptab, attrglbtype, /* Perform asynchronous collective communication */
                      attrgsttab, enttlocptr->procrcvtab, recvdsptab, attrglbtype,
                      meshptr->proccomm, &requptr->requval) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return;
  }
  requptr->flagval = -1;                          /* Communication successfully launched */
#else /* PAMPA_MPI_ASYNC_COLL */
  requptr->mreqnbr = ((meshptr->flagval & DMESHCOMMPTOP) != 0) ? enttlocptr->commlocdat.procngbnbr : 0; // XXX attention l'allocation de mreqtab se fait dans dmeshHaloSync3, il faudrait faire en sorte que la taille soit modifiée au même endroit
  requptr->flagval = dmeshHaloSync2 (meshptr, enttlocptr, &requptr->preqtab->attrsndtab, attrgsttab, attrglbtype, &requptr->mreqtab); /* Last resort is synchronous communication */
#endif /* PAMPA_MPI_ASYNC_COLL */
#endif /* PAMPA_PTHREAD */
}



void
dmeshHaloMultAsync (
Dmesh * restrict const       meshptr,			  //!< XXX
Gnum const                   valunbr,
Gnum const * restrict const  entttab,
Gnum const * restrict const  tagtab,
DmeshHaloRequests * restrict  reqsptr)		  	  //!< XXX
{
#ifndef PAMPA_PTHREAD
#ifdef PAMPA_MPI_ASYNC_COLL
  int *               senddsptab;
  int *               recvdsptab;
#endif /* PAMPA_MPI_ASYNC_COLL */
#endif /* PAMPA_PTHREAD */

#ifdef PAMPA_PTHREAD
  reqsptr->flagval     = -1;                      /* Assume thread will be successfully launched */
  if (dmeshHaloMultCommon (meshptr, valunbr, entttab, tagtab, reqsptr) != 0)
      return;
  if (pthread_create (&reqsptr->thrdval, NULL, (void * (*) (void *)) dmeshHaloMultAsync2, (void *) reqsptr) != 0) /* If could not create thread */
    reqsptr->flagval = (int) (intptr_t) dmeshHaloMultAsync2 (reqsptr); /* Call function synchronously */
#else /* PAMPA_PTHREAD */
#ifdef PAMPA_MPI_ASYNC_COLL
  reqsptr->flagval    = 1;                        /* Assume error */
  reqsptr->attrsndtab = NULL;                     /* No memory    */

  if (dmeshHaloMultSync2 (reqsptr) != 0) /* Prepare communication arrays */
    return;

  errorPrint ("not yet implemented");
  if (MPE_Ialltoallv (reqsptr->attrsndtab, enttlocptr->procsndtab, senddsptab, attrglbtype, /* Perform asynchronous collective communication */
                      attrgsttab, enttlocptr->procrcvtab, recvdsptab, attrglbtype,
                      meshptr->proccomm, &reqsptr->requval) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return;
  }
  reqsptr->flagval = -1;                          /* Communication successfully launched */
#else /* PAMPA_MPI_ASYNC_COLL */
  reqsptr->flagval = dmeshHaloMultSync (meshptr, valunbr, entttab, tagtab, reqsptr); /* Last resort is synchronous communication */
#endif /* PAMPA_MPI_ASYNC_COLL */
#endif /* PAMPA_PTHREAD */
}


//! This function performs an asynchronous collective
//! halo diffusion operation on the ghost array given
//! on input. It fills the given request structure with
//! the relevant data.
//! It returns:
//! - 0   : if the halo has been successfully propagated.
//! - !0  : on error.

int
dmeshHaloWait (
DmeshHaloRequests * restrict  reqsptr)			  //!< XXX
{
  Gnum                preqnum;
#ifdef PAMPA_PTHREAD
  void *                    o;

  if (reqsptr->flagval == -1) {                   /* If thread launched              */
    pthread_join (reqsptr->thrdval, &o);          /* Wait for its completion         */
    reqsptr->flagval = (int) (intptr_t) o;        /* Get thread return value         */
  }                                               /* Else return value already known */
  if (reqsptr->mreqtab != NULL)
    memFree (reqsptr->mreqtab);
#else /* PAMPA_PTHREAD */
#ifdef PAMPA_MPI_ASYNC_COLL
  MPI_Status                statval;

  if (reqsptr->flagval == -1) {                   /* If communication launched                                    */
    MPI_Wait (&reqsptr->requval, &statval);       /* Wait for completion of asynchronous collective communication */
    reqsptr->flagval = (statval.MPI_ERROR == MPI_SUCCESS) ? 0 : 1; /* Set return value accordingly                */
  }
#else /* PAMPA_MPI_ASYNC_COLL */
  int o = 0;
  if (reqsptr->mreqtab != NULL) {
    if (commWaitall (reqsptr->mreqnbr, reqsptr->mreqtab, MPI_STATUSES_IGNORE) != MPI_SUCCESS) {
      errorPrint ("communication error");
      o = 1;
    }
    memFree (reqsptr->mreqtab);
  }
  reqsptr->flagval |= o;

#endif /* PAMPA_MPI_ASYNC_COLL */
#endif /* PAMPA_PTHREAD */
  if (reqsptr->preqtab != NULL) {
    for (preqnum = 0; preqnum < reqsptr->preqnbr; preqnum ++) {
      if (reqsptr->preqtab[preqnum].attrsndtab != NULL) {                /* Free group leader if it was successfully allocated before */
        memFree (reqsptr->preqtab[preqnum].attrsndtab);
        reqsptr->preqtab[preqnum].attrsndtab = NULL;
      }
    }
    memFree (reqsptr->preqtab);
    reqsptr->preqtab = NULL;
  }


  return (reqsptr->flagval);                      /* Return asynchronous or synchronous error code */
}
