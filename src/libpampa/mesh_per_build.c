/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_rebuild.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 28 Mar 2017
//!                             to:   28 Sep 2017
//!
/************************************************************/

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "smesh_mesh.h"
#include "mesh_per_build.h"
#include "pampa.h"

//! This routine rebuilds a mesh XXX
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshPerBuild (
    Mesh * restrict const       imshptr,           //!< Internal mesh
    const Gnum                  pvrtnbr,
    PerNum * restrict const     pvrttab,
    Mesh * restrict const       omshptr)           //!< Output mesh
{
  Smesh           smshdat;
  Gnum            baseval;
  Gnum            pvrtidx;
  Gnum            outvertnum;
  Gnum            outvertnnd;
  Gnum            outvertnbr;
  Gnum            outedgenbr;
  Gnum            invertnum;
  Gnum            invertnnd;
  Gnum            invertnbr;
  Gnum            edgenbr;
  Gnum            edgesiz;
  Gnum            enttnum;
  Gnum            enttnnd;
  Gnum * restrict flagtax;
  Gnum *          vm2mtax;
  Gnum * restrict permtax;
  Gnum * restrict outverttax;
  Gnum * restrict outvendtax;
  Gnum * restrict outventtax;
  Gnum * restrict outedgetax;
  Gnum * restrict outedlotax;
  Gnum            ventnum;
  Gnum            ventnnd;

  baseval = imshptr->baseval;
  CHECK_FDBG2 (smeshInit (&smshdat));
  CHECK_FDBG2 (mesh2smesh (imshptr, &smshdat));

  ///* Identity array */
  //for (invertnum = baseval, invertnnd = invertnbr + baseval; invertnum < invertnnd; invertnum ++)
  //  permtax[invertnum] = invertnum;

  memSet (flagtax + baseval, 0, invertnbr * sizeof (Gnum));

  /* Compute permutation array */
  /* For each vertex (v) with a corresponding neighbor (n), (n) is flagged */
  for (pvrtidx = 0; pvrtidx < pvrtnbr; pvrtidx ++) 
    flagtax[pvrttab[pvrtidx].nghbnum] = 1;

  /* Only used vertices will have a out number (compact graph) */
  for (outvertnum = invertnum = baseval, invertnnd = invertnbr + baseval; invertnum < invertnnd; invertnum ++)
    if (flagtax[invertnum] == 0)
      permtax[invertnum] = outvertnum ++;
  outvertnbr -= baseval;

  /* For each vertex (v) with a corresponding neighbor (n), if (n) is flagged, permtax[n] = permtax[v] */
  for (pvrtidx = 0; pvrtidx < pvrtnbr; pvrtidx ++) 
    permtax[pvrttab[pvrtidx].nghbnum] = pvrttab[pvrtidx].vertnum;

  /* The array which contains pair, is sorted according to the first value */
  intSort2asc1 (pvrttab, pvrtnbr);

  /* Compute adjacency list on out graph */
  for (pvrtidx = 0, outedgenbr = outvertnum = invertnum = baseval, invertnnd = invertnbr + baseval;
      invertnum < invertnnd; invertnum ++) {
    Gnum inedgenum;
    Gnum inedgennd;

    if (flagtax[invertnum] != 0) /* If not used vertex */
      continue;

    for (; pvrttab[pvrtidx].vertnum < invertnum; pvrtidx ++);

    outverttax[outvertnum] = smshdat.verttax[invertnum];
    outventtax[outvertnum] = smshdat.venttax[invertnum];
    for (inedgenum = smshdat.verttax[invertnum], inedgennd = smshdat.vendtax[invertnum];
        inedgenum < inedgennd; inedgenum ++) {
      if (smshdat.edlotax != NULL)
        outedlotax[outedgenbr] = smshdat.edlotax[inedgenum];
      outedgetax[outedgenbr ++] = permtax[smshdat.edgetax[inedgenum]];
    }

    /* If invertnum vertex is combined with another vertex */
    if (pvrttab[pvrtidx].vertnum == invertnum) {
      Gnum outnghbnum;

      outnghbnum = pvrttab[pvrtidx].nghbnum;
      /* for each neighbor of combined vertex (of invertnum) */
      for (inedgenum = smshdat.verttax[outnghbnum], inedgennd = smshdat.vendtax[outnghbnum];
          inedgenum < inedgennd; inedgenum ++) {
        Gnum innghbnum;
        Gnum outedgenum;
        Gnum outedgennd;

        innghbnum = smshdat.edgetax[inedgenum];
        /* Check if vertex is already in the adjacency list */
        for (outedgenum = outverttax[permtax[innghbnum]], outedgennd = outvendtax[permtax[innghbnum]];
            outedgenum < outedgennd; outedgenum ++)
          if (outnghbnum == outedgetax[outedgenum])
            break;
        /* Vertex is not inserted */
        if (outedgenum == outedgennd) {
          if (smshdat.edlotax != NULL)
            outedlotax[outedgenbr] = smshdat.edlotax[inedgenum];
          outedgetax[outedgenbr ++] = permtax[smshdat.edgetax[inedgenum]];
        }
      }
    }
  }
  outedgenbr -= baseval;

  CHECK_FDBG2 (meshBuild (omshptr, imshptr->idnum, baseval, outvertnbr, outverttax, outvendtax, outedgenbr, outedgenbr, outedgetax, outedlotax, imshptr->enttnbr, outventtax, smshdat.esubtax, smshdat.valsptr->valumax, NULL, ~0, smshdat.procglbnbr));
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (meshCheck (omshptr));
#endif /* PAMPA_DEBUG_MESH */

  /* Change values */
  vm2mtax = flagtax;

  /* Keep permutation array */
  for (invertnum = baseval, invertnnd = invertnbr + baseval; invertnum < invertnnd; invertnum ++)
    vm2mtax[invertnum] = (vm2mtax[invertnum] == 0) ? (permtax[invertnum]) : (~0);

  for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = baseval + smshdat.enttnbr; enttnum < enttnnd; enttnum ++) {
    Gnum           vertmax;
    PAMPA_Iterator it;

    if (enttnum >= baseval)
      memSet (permtax + baseval, ~0, invertnbr * sizeof (Gnum));
    switch (enttnum) {
      case PAMPA_ENTT_DUMMY:
        break;
      case PAMPA_ENTT_VIRT_PROC:
        break;
      case PAMPA_ENTT_VIRT_VERT:
        memCpy (permtax + baseval, vm2mtax + baseval, invertnbr * sizeof (Gnum));
        vertmax = smshdat.vertnbr + baseval;
        break;
      default:
        if (omshptr->entttax[enttnum] == NULL)
          continue;

        PAMPA_meshItInitStart((PAMPA_Mesh *) omshptr, enttnum, &it);
        while (PAMPA_itHasMore(&it)) {
          Gnum vertnum;
          Gnum mvrtnum;

          vertnum = PAMPA_itCurEnttVertNum(&it);
          mvrtnum = PAMPA_itCurMeshVertNum(&it);            

          permtax[mvrtnum] = vertnum;
          PAMPA_itNext(&it);
        }
        vertmax = imshptr->entttax[enttnum]->vertnbr;
    }
    CHECK_FDBG2 (meshPerBldValues (imshptr, omshptr, (const Gnum *) vm2mtax, enttnum, permtax, baseval, vertmax));
  }

  return (0);
}

int meshPerBldValues (
    Mesh  * const imshptr,
    Mesh  * const omshptr,
    const Gnum *  vm2mtax,
    const Gnum    enttnum,
    const Gnum *  vm2etax,
    const Gnum    vertmin,
    const Gnum    vertmax)
{
  Value * valuptr;

  for (valuptr = imshptr->valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next) {
    MPI_Aint            valusiz;                 /* Extent of attribute datatype */
    MPI_Aint            dummy;
    byte *              valutax;
    Gnum                invertnum;
    Gnum                invertnnd;
    Gnum                sizeval;
    int                 chekval;

    MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */

    chekval = meshValueData (omshptr, enttnum, valuptr->tagnum, &sizeval, NULL, (void **) &valutax);
    if (chekval == 2) {
      CHECK_FDBG2 (meshValueLink(omshptr, (void **) &valutax, valuptr->flagval & (!PAMPA_VALUE_ALLOCATED), &sizeval, NULL, valuptr->typeval, enttnum, valuptr->tagnum));
      memSet ((byte *) valutax, ~0, sizeval * valusiz * sizeof (byte));
    }
    else if (chekval != 0) {
      errorPrint ("internal error");
      return (1);
    }

    if (enttnum != PAMPA_ENTT_VIRT_PROC)
      valutax -= omshptr->baseval * valusiz;

    if (enttnum == PAMPA_ENTT_VIRT_PROC)
      memCpy ((byte *) valutax, (byte *) valuptr->valutab, valusiz * sizeval * sizeof (byte));
    else 
      for (invertnum = vertmin, invertnnd = vertmax; invertnum < invertnnd; invertnum ++) {
        Gnum outvertnum;
        Gnum inmvrtnum;

        if (enttnum >= omshptr->baseval)
          inmvrtnum = imshptr->entttax[enttnum]->mvrttax[invertnum];

        if ((enttnum >= omshptr->baseval) && (vm2etax[inmvrtnum] == ~0))
          continue;

        outvertnum = (enttnum == PAMPA_ENTT_VIRT_VERT)
          ? (vm2mtax[inmvrtnum])
          : (vm2etax[vm2mtax[inmvrtnum]]);
        if (outvertnum != ~0)
          memCpy ((byte *) valutax + outvertnum * valusiz, (byte *) valuptr->valutab + (invertnum - imshptr->baseval) * valusiz, valusiz * sizeof (byte));
      }
  }
  return (0);
}

