!*  Copyright 2009-2016 Inria
!*
!* This file is part of the PaMPA software package for parallel
!* mesh partitioning and adaptation.
!*
!* PaMPA is free software: you can redistribute it and/or modify
!* it under the terms of the GNU General Public License as published by
!* the Free Software Foundation, either version 3 of the License, or
!* any later version.
!* 
!* PaMPA is distributed in the hope that it will be useful,
!* but WITHOUT ANY WARRANTY; without even the implied warranty of
!* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!* GNU General Public License for more details.
!* 
!* In this respect, the user's attention is drawn to the risks associated
!* with loading, using, modifying and/or developing or reproducing the
!* software by the user in light of its specific status of free software,
!* that may mean that it is complicated to manipulate, and that also
!* therefore means that it is reserved for developers and experienced
!* professionals having in-depth computer knowledge. Users are therefore
!* encouraged to load and test the software's suitability as regards
!* their requirements in conditions enabling the security of their
!* systems and/or data to be ensured and, more generally, to use and
!* operate it in the same conditions as regards security.
!* 
!* The fact that you are presently reading this means that you have had
!* knowledge of the GPLv3 license and that you accept its terms.
!*
!************************************************************
!> 
!>    \file        library_commonf.h
!> 
!>    \authors     Cedric Lachat
!> 
!>    \brief       This module is the API for the libPampa
!>                 library.
!> 
!>    \date        Version 1.0: from:  7 Jun 2011
!>                              to:   30 Sep 2016
!>                 Version 2.0: from:  7 Oct 2016
!>                              to:   12 May 2017
!> 
!************************************************************

!// Size definitions for the PaMPA opaque
!// structures. These structures must be
!// allocated as arrays of DOUBLEPRECISION
!// values for proper padding. The dummy
!// sizes are computed at compile-time by
!// program "dummysizes".

INTEGER PAMPAF_DMESHDIM
INTEGER PAMPAF_DMESHHALOREQSDIM
INTEGER PAMPAF_MESHDIM
INTEGER PAMPAF_STRATDIM
INTEGER PAMPAF_ITERATORDIM
INTEGER PAMPAF_NUM
INTEGER PAMPAF_DOUBLE
INTEGER PAMPAF_MPI_NUM
INTEGER PAMPAF_VERSION
INTEGER PAMPAF_RELEASE
INTEGER PAMPAF_PATCHLEVEL

PARAMETER (PAMPAF_VERSION           = DUMMYVERSION)
PARAMETER (PAMPAF_RELEASE           = DUMMYRELEASE)
PARAMETER (PAMPAF_PATCHLEVEL        = DUMMYPATCHLEVEL)

PARAMETER (PAMPAF_DMESHDIM          = DUMMYSIZEDMESH)
PARAMETER (PAMPAF_DMESHHALOREQSDIM   = DUMMYSIZEDMESHHALOREQS)
PARAMETER (PAMPAF_MESHDIM           = DUMMYSIZEMESH)
PARAMETER (PAMPAF_STRATDIM          = DUMMYSIZESTRAT)
PARAMETER (PAMPAF_ITERATORDIM       = DUMMYSIZEITERATOR)
PARAMETER (PAMPAF_NUM               = DUMMYSIZENUM)
PARAMETER (PAMPAF_DOUBLE            = DUMMYSIZEDOUBLE)
!//PARAMETER (PAMPAF_MPI_NUM           = DUMMYMPIINT) !< \todo is it correct?


!/*+ Flags for entities and sub-entities +*/
!// TRICK: PAMPAF_ENTT_SINGLE must be the max value of entity tags
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ENTT_SINGLE
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ENTT_STABLE

PARAMETER (PAMPAF_ENTT_SINGLE       = -1)
PARAMETER (PAMPAF_ENTT_STABLE       = -2)

!/*+ Flags for overlap +*/
INTEGER(KIND = PAMPAF_NUM) PAMPAF_OVLP_ELEM

PARAMETER (PAMPAF_OVLP_ELEM           = ishft(int(1, PAMPAF_NUM), 5))



!/*+ Flags for vertices +*/
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_ANY
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_LOCAL
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_INTERNAL
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_BOUNDARY
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_OVERLAP
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_GHOST
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_FRONTIER
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VERT_NOT_FRONTIER

PARAMETER (PAMPAF_VERT_ANY           = DUMMYVERTANY)
PARAMETER (PAMPAF_VERT_LOCAL        = DUMMYVERTLOCAL)
PARAMETER (PAMPAF_VERT_INTERNAL     = DUMMYVERTINTERNAL)
PARAMETER (PAMPAF_VERT_BOUNDARY     = DUMMYVERTBOUNDARY)
PARAMETER (PAMPAF_VERT_OVERLAP      = DUMMYVERTOVERLAP)
PARAMETER (PAMPAF_VERT_GHOST        = DUMMYVERTGHOST)
PARAMETER (PAMPAF_VERT_FRONTIER     = DUMMYVERTFRONTIER)
PARAMETER (PAMPAF_VERT_NOT_FRONTIER = DUMMYVERTNOTFRONTIER)



!/*+ Flags for associated values. +*/
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_NIL
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_GEOM
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_REMESH
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_REF
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_SOL_2DI
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_SOL_2DAI
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_SOL_3DI
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_SOL_3DAI
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_STATUS
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_PART
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_PERM
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_WEIGHT
INTEGER(KIND = PAMPAF_NUM) PAMPAF_TAG_LOAD

PARAMETER (PAMPAF_TAG_NIL          = -42) !/* XXX temporaire */
PARAMETER (PAMPAF_TAG_GEOM         = -1)
PARAMETER (PAMPAF_TAG_REMESH       = -2)
PARAMETER (PAMPAF_TAG_REF          = -3)
PARAMETER (PAMPAF_TAG_SOL_2DI      = -4)
PARAMETER (PAMPAF_TAG_SOL_2DAI     = -5)
PARAMETER (PAMPAF_TAG_SOL_3DI      = -6)
PARAMETER (PAMPAF_TAG_SOL_3DAI     = -7)
PARAMETER (PAMPAF_TAG_STATUS       = -8)
PARAMETER (PAMPAF_TAG_PART         = -9)
PARAMETER (PAMPAF_TAG_PERM         = -10)
PARAMETER (PAMPAF_TAG_WEIGHT       = -11)
PARAMETER (PAMPAF_TAG_LOAD         = -12)
!/*+ Some parameters are missing +*/



!/*+ Flags for matrix +*/
INTEGER(KIND = PAMPAF_NUM) PAMPAF_MAT_NGHB_FIRST

PARAMETER (PAMPAF_MAT_NGHB_FIRST          = -1)

!/*+ Flags for errors +*/
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_MEM
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_COMM
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_DATA
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LOC_DATA
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_GLB_DATA
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_VRT_ARRAY
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LNK_ARRAY
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LNK_NBR
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LNK_ELEMENT
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_INTERNAL
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LNK_MISSING
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LNK_REDEF
INTEGER(KIND = PAMPAF_NUM) PAMPAF_ERR_LNK_DUP

PARAMETER (PAMPAF_ERR_MEM 			= 1)
PARAMETER (PAMPAF_ERR_COMM 			= 2)
PARAMETER (PAMPAF_ERR_DATA 			= 3)
PARAMETER (PAMPAF_ERR_LOC_DATA 			= 4)
PARAMETER (PAMPAF_ERR_GLB_DATA 			= 5)
PARAMETER (PAMPAF_ERR_VRT_ARRAY 		= 6)
PARAMETER (PAMPAF_ERR_LNK_ARRAY 		= 7)
PARAMETER (PAMPAF_ERR_LNK_NBR 			= 8)
PARAMETER (PAMPAF_ERR_LNK_ELEMENT 		= 9)
PARAMETER (PAMPAF_ERR_INTERNAL 			= 10)
PARAMETER (PAMPAF_ERR_LNK_MISSING 		= 11)
PARAMETER (PAMPAF_ERR_LNK_REDEF   		= 12)
PARAMETER (PAMPAF_ERR_LNK_DUP     		= 13)



!/*+ Flags for values +*/
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VALUE_PRIVATE
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VALUE_PUBLIC
INTEGER(KIND = PAMPAF_NUM) PAMPAF_VALUE_ALLOCATED

PARAMETER (PAMPAF_VALUE_PRIVATE			= 1) !/* XXX Not good */
PARAMETER (PAMPAF_VALUE_PUBLIC 			= 2) !/* XXX Not good */
PARAMETER (PAMPAF_VALUE_ALLOCATED		= 4) !/* XXX Not good */
