/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the distributed mesh
//!                general purpose routines. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define DMESH

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* mesh meshs.                      */
/*                                   */
/*************************************/

//! \brief This routine initializes a distributed mesh
//! structure.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dmeshInit (
    Dmesh * restrict const      meshptr,          //!< Distributed mesh structure
    MPI_Comm                    proccomm)         //!< Communicator to be used for all communications
{
  memSet (meshptr, 0, sizeof (Dmesh));            /* Clear public and private mesh fields */

  meshptr->proccomm = proccomm;                   /* Set private fields    */
  MPI_Comm_size (proccomm, &meshptr->procglbnbr); /* Get communicator data */
  MPI_Comm_rank (proccomm, &meshptr->proclocnum);


  return (0);
}

//! \brief This routine is internally used and frees user fields
//!
//! \returns VOID : in all cases.

static void
dmeshFree2 (
    Dmesh * restrict const     meshptr)           //!< Distributed mesh structure
{
  DmeshEntity ** enttloctax;
  Gnum enttnum;
  Gnum nentlocnum;
  Gnum baseval;
  Gnum enttnnd;

  baseval = meshptr->baseval;

  /* If process send arrays must be freed */
  if ((meshptr->flagval & DMESHFREEPSID) != 0) { 
    if (meshptr->enttloctax != NULL) {
      enttloctax = meshptr->enttloctax;
    for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ;
        enttnum < enttnnd ; enttnum ++) {
        if ((enttloctax[enttnum] != NULL)
            && (enttloctax[enttnum]->commlocdat.procsidtab != NULL))
          memFree (enttloctax[enttnum]->commlocdat.procsidtab);
        if ((enttloctax[enttnum] != NULL)
            && (enttloctax[enttnum]->commlocdat.procngbtab != NULL))
          memFree (enttloctax[enttnum]->commlocdat.procngbtab);
      }
    }
  }

  /* If local entity private data must be freed */
  if ((meshptr->flagval & DMESHFREEENTT) != 0) { 
    if (meshptr->enttloctax != NULL) {
      enttloctax = meshptr->enttloctax;
      for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ;
          enttnum < enttnnd ; enttnum ++) {
        DmeshEnttNghb ** nghbloctax;
        Gnum ngbenttnnd;

        if (enttloctax[enttnum] != NULL) {        /* If entity is not empty */

        nghbloctax = enttloctax[enttnum]->nghbloctax;

        if ((nghbloctax != NULL)                  /* If entity has neighbors */
            && (meshptr->esublocbax[enttnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE))
          for (nentlocnum = baseval, ngbenttnnd = meshptr->enttglbnbr + baseval ;
              nentlocnum < ngbenttnnd ; nentlocnum ++) {
            if (nghbloctax[nentlocnum] != NULL) {
              if ((meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
                  && (nghbloctax[nentlocnum]->vindloctax != NULL)) { 
                // TRICK: verttax and vendtax are allocated together
                memFreeGroup (nghbloctax[nentlocnum]->vindloctax+baseval);
                nghbloctax[nentlocnum]->vindloctax = NULL;
              }
              memFree (nghbloctax[nentlocnum]);
            }
            
          }

        if ((meshptr->flagval & DMESHFREELVL) != 0) { /* If local data between levels must be freed */
          DmeshEnttNghb ** nghbloctax;
          Gnum ngbenttnnd;

          if (enttloctax[enttnum] != NULL) {

            nghbloctax = enttloctax[enttnum]->ngblloctax;

            if ((nghbloctax != NULL) && (meshptr->esublocbax[enttnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)) {
              for (nentlocnum = baseval, ngbenttnnd = meshptr->enttglbnbr + baseval ; nentlocnum < ngbenttnnd ; nentlocnum ++) {
                if (nghbloctax[nentlocnum] != NULL) {
                  if ((meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) && (nghbloctax[nentlocnum]->vindloctax != NULL)) { // TRICK: verttax and vendtax are allocated together
                    memFree (nghbloctax[nentlocnum]->vindloctax+baseval);
                    nghbloctax[nentlocnum]->vindloctax = NULL;
                  }
                  memFree (nghbloctax[nentlocnum]);
                  nghbloctax[nentlocnum] = NULL;
                }
              }
              memFree (nghbloctax + baseval);
              nghbloctax = NULL;
            }

            nghbloctax = enttloctax[enttnum]->ngbuloctax;

            if ((nghbloctax != NULL) && (meshptr->esublocbax[enttnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)) {
              for (nentlocnum = baseval, ngbenttnnd = meshptr->enttglbnbr + baseval ; nentlocnum < ngbenttnnd ; nentlocnum ++) {
                if (nghbloctax[nentlocnum] != NULL) {
                  if ((meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) && (nghbloctax[nentlocnum]->vindloctax != NULL)) { // TRICK: verttax and vendtax are allocated together
                    memFree (nghbloctax[nentlocnum]->vindloctax+baseval);
                    nghbloctax[nentlocnum]->vindloctax = NULL;
                  }
                  memFree (nghbloctax[nentlocnum]);
                  nghbloctax[nentlocnum] = NULL;
                }
              }
              memFree (nghbloctax + baseval);
              nghbloctax = NULL;
            }
          }
           
          if (meshptr->medgloctax != NULL) {
            memFree (meshptr->medgloctax + baseval);
            meshptr->medgloctax = NULL;
          }
        }

        if (enttloctax[enttnum]->commlocdat.procrcvtab != NULL)
          memFreeGroup (enttloctax[enttnum]->commlocdat.procrcvtab);

        if (enttloctax[enttnum]->vprmloctax != NULL)
          memFree (enttloctax[enttnum]->vprmloctax + baseval);

        memFree (enttloctax[enttnum]);
        }
      }
      memFreeGroup (meshptr->enttloctax + baseval);
    }

    if ((meshptr->esublocmsk != 0) && (meshptr->esublocbax != NULL))
      memFree (meshptr->esublocbax + baseval);
  }


  if (meshptr->valslocptr != NULL)                /* Free attached values */
  	dvaluesFree(meshptr->valslocptr);



}

//! \brief This routine frees the public data of the given
//! distributed mesh, but not its private data.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! 
//! \returns VOID  : in all cases.

void
dmeshFree (
    Dmesh * restrict const     meshptr)	          //!< Distributed mesh structure
{
  DmeshFlag          flagval;
  MPI_Comm            proccomm;                   /* Data for temporarily saving private data */
  int                 procglbnbr;
  int                 proclocnum;
  Gnum * restrict     proccnttab;                 
  Gnum * restrict     procdsptab;
  Gnum * restrict     procvrttab;
  int * restrict      procngbtab;
  Gnum *              procloccnt;

  dmeshFree2 (meshptr);                           /* Free all user fields */

  flagval    = meshptr->flagval & (DMESHFREEPRIV | DMESHFREECOMM);
  proccomm   = meshptr->proccomm;                 /* Save private fields only */
  procglbnbr = meshptr->procglbnbr;
  proclocnum = meshptr->proclocnum;
  proccnttab = meshptr->proccnttab;
  procdsptab = meshptr->procdsptab;
  procvrttab = meshptr->procvrttab;
  procngbtab = meshptr->procngbtab;
  procloccnt = meshptr->procloccnt;

  memSet (meshptr, 0, sizeof (Dmesh));            /* Reset all of the mesh structure */

  meshptr->flagval    = flagval;                  /* Restore private fields */
  meshptr->proccomm   = proccomm;
  meshptr->procglbnbr = procglbnbr;
  meshptr->proclocnum = proclocnum;
  meshptr->proccnttab = proccnttab;
  meshptr->procdsptab = procdsptab;
  meshptr->procvrttab = procvrttab;
  meshptr->procngbtab = procngbtab;
  meshptr->procloccnt = procloccnt;


  return;
}

//! \brief This routine destroys a distributed mesh structure.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! Private data are always destroyed. If this is not
//! wanted, use dmeshFree() instead.
//!
//! \returns VOID  : in all cases.

void
dmeshExit (
    Dmesh * restrict const     meshptr)					//!< Distributed mesh structure
{
  if ((meshptr->flagval & DMESHFREEPRIV) != 0) { /* If private data has to be freed */
    if (*meshptr->procloccnt == 1) {   /* If private data has to be freed */
      if (meshptr->procdsptab != NULL)
        memFreeGroup (meshptr->procdsptab);              /* Free group leader of mesh private data */
    }
    else  if (*meshptr->procloccnt > 0)
      (*meshptr->procloccnt) --;
  }
  if ((meshptr->flagval & DMESHFREECOMM) != 0)   /* If communicator has to be freed         */
    MPI_Comm_free (&meshptr->proccomm);           /* Free it                                 */

  dmeshFree2 (meshptr);


#ifdef PAMPA_DEBUG_DMESH1
  memSet (meshptr, 0, sizeof (Dmesh));
#endif /* PAMPA_DEBUG_DMESH1 */
}
