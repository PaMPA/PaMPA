/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_iter.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations
//!                for the mesh iterator purpose routines.
//!
//!   \date        Version 1.0: from: 31 May 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "mesh_iter.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* mesh iterator handling routines.     */
/*                                      */
/****************************************/

//! \brief This routine fill in the PAMPA_Iterator structure in order to have
//! the neighbors, with a given entity, of a vertex.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_meshItInitStart (
PAMPA_Mesh * const          meshptr,                                    //!< Centralized mesh
PAMPA_Num const             ventnum,                                    //!< Entity number of vertex
PAMPA_Iterator *            iterptr)                                    //!< Iterator to fill in
{

  PAMPA_Num chekval;
  PAMPA_Num dummval;
  PAMPA_IteratorConst * itecptr;

  itecptr = (PAMPA_IteratorConst *) &iterptr->c;

  chekval = meshItData((const Mesh *) meshptr, ventnum, -1, &itecptr->nentnum, &dummval, &itecptr->vindtab, &itecptr->edgetab, &dummval, &itecptr->svrttab, &itecptr->mngbtab, &itecptr->nentbas, &itecptr->nentmsk, &itecptr->nenttab, &itecptr->sngbtab, &iterptr->vertnum, &itecptr->vertnnd); /* TRICK: -1 means that any vertex */

  itecptr->nesbnum = ventnum;
  if (itecptr->nentbas == NULL)
    itecptr->nentbas = (PAMPA_Num *) &itecptr->nesbnum;

#ifdef PAMPA_DEBUG_ITER
  itecptr->vertmax = itecptr->vertnnd;
#endif /* PAMPA_DEBUG_ITER */
  return chekval;
}


//! \brief This routine fill in the PAMPA_Iterator structure in order to have
//! the neighbors, with a given entity, of a vertex.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_meshItInit (
PAMPA_Mesh * const          meshptr,                                    //!< Centralized mesh
PAMPA_Num const             ventnum,                                    //!< Entity number of vertex
PAMPA_Num const             nesbnum,                                    //!< Entity number of neighbors
PAMPA_Iterator *            iterptr)                                    //!< Iterator to fill in
{

  PAMPA_Num chekval;
  PAMPA_Num dummval;
  PAMPA_IteratorConst * itecptr;

  itecptr = (PAMPA_IteratorConst *) &iterptr->c;

  chekval = meshItData((const Mesh *) meshptr, ventnum,nesbnum, &itecptr->nentnum, &dummval, &itecptr->vindtab, &itecptr->edgetab, &dummval, &itecptr->svrttab, &itecptr->mngbtab, &itecptr->nentbas, &itecptr->nentmsk, &itecptr->nenttab, &itecptr->sngbtab, &iterptr->vertnum, &itecptr->vertnnd);

  itecptr->nesbnum = nesbnum;
  itecptr->ventnum = ventnum;
  if (itecptr->nentbas == NULL)
    itecptr->nentbas = (PAMPA_Num *) &itecptr->nesbnum;

#ifdef PAMPA_DEBUG_ITER
  itecptr->vertmax = itecptr->vertnnd;
#endif /* PAMPA_DEBUG_ITER */
  return chekval;
}


