
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* multi level distributed mesh         */
/* handling routines.                   */
/*                                      */
/****************************************/


//! \brief This routine fills the contents of the given
//! opaque multi level distributed mesh structure with
//! the data provided by the user. The base value
//! allows the user to set the mesh base to 0 or 1.
//! 
//! \returns  0  : on success.
//! \returns !0  : on error.

int
PAMPA_mdmeshCoarLevelBuild (
PAMPA_Mdmesh * const        meshptr,            //!< Multi level distributed mesh structure to fill        
PAMPA_Num * const           meshglbptr,
const PAMPA_Num             velolocsiz,         //!< Size of veloloctax 
const PAMPA_Num * const     veloloctab,         //!< Weight on vertices which are only elements 
const PAMPA_Num             edlolocsiz,         //!< Size of edloloctab 
const PAMPA_Num * const     edloloctab,         //!< Weight on edges between edges 
const PAMPA_Num * const     enttglbtab,         //!< Concordance table on entities between two levels 
const PAMPA_Num             elvlglbsiz,         //!< Size of elvlglbtab 
const PAMPA_Num * const     elvlglbtab,         //!< Array which indicate the order to treat the entities 
const PAMPA_Num * const     lnumglbtab,         //!< Array which indicate the number of the levels 
const PAMPA_Num * const     etagglbtab,         //!< Tag array for each entity, tag is compared before merging vertices with the same entity 
const PAMPA_Num             loadglbval,         //!< Weight for coarsened element */
const PAMPA_Num             flagval)
{
  Mdmesh *                  srcmeshptr;         /* Pointer to source mesh structure           */
  Gnum *                    veloloctax;
  Gnum *                    edloloctax;
  Gnum *                    enttglbtax;
  Gnum *                    elvlglbtax;
  Gnum *                    lnumglbtax;
  Gnum *                    etagglbtax;
  Gnum                      baseval;

#ifdef PAMPA_DEBUG_LIBRARY1
  if (sizeof (PAMPA_Mdmesh) < sizeof (Mdmesh)) {
    errorPrint ("internal error");
    return     (1);
  }
#endif /* PAMPA_DEBUG_LIBRARY1 */

  srcmeshptr = (Mdmesh *) meshptr;                /* Use structure as source mesh */
  baseval    = srcmeshptr->baseval;



  
  veloloctax = (veloloctab == NULL) ? NULL : (Gnum *) veloloctab - baseval;
  edloloctax = (edloloctab == NULL) ? NULL : (Gnum *) edloloctab - baseval;
  enttglbtax = (enttglbtab == NULL) ? NULL : (Gnum *) enttglbtab - baseval;
  elvlglbtax = (Gnum *) elvlglbtab - baseval;
  lnumglbtax = (Gnum *) lnumglbtab - baseval;
  etagglbtax = (etagglbtab == NULL) ? NULL : (Gnum *) etagglbtab - baseval;

  return (mdmeshCoarLevelBuild (srcmeshptr, meshglbptr, velolocsiz, veloloctax, edlolocsiz, edloloctax, enttglbtax, elvlglbsiz, elvlglbtax, lnumglbtax, etagglbtax, loadglbval, flagval));

}
