/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_check.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the distributed mesh
//!                consistency checking routine.
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define DMESH_CHECK

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_check.h"
#include "pampa.h"

//! \brief This function checks the consistency
//! of the given distributed mesh.
//! \todo This function does not work if distributed mesh is built with
//! PAMPA_OVLP_NOGT
//!
//! \returns 0   : if mesh data are consistent.
//! \returns <0  : on error.
//! \returns >0  : code corresponding to the invalid data of the mesh.

int
dmeshCheck (
    Dmesh * restrict const meshptr)
{

  MPI_Comm            proccomm;                   //!< Mesh communicator
  int                 procglbnbr;                 //!< Number of processes sharing mesh data
  int                 proclocnum;                 //!< Number of this process
  int                 procrcvnum;                 //!< Number of process from which to receive
  int                 procsndnum;                 //!< Number of process to which to send
  int                 procngbnum;                 //!< Number of current neighbor process
  Gnum *              procngbtab;                 //!< Array of neighbor vertex ranges
  Gnum                baseval;
  Gnum                ventlocnum;
  Gnum                enttnnd;
  Gnum                enttglbnbr;
  Gnum                vertlocnum;
  Gnum                vertlocnbr;
  Gnum                edgelocnbr;                 //!< Local number of edges
  int                 cheklocval;                 //!< Local consistency flag
  int                 chekglbval;                 //!< Global consistency flag
  Gnum                reduloctab[11];             //!< Arrays for reductions
  Gnum                reduglbtab[11];
  MPI_Status          statloctab[8];
  DmeshEntity **      enttloctax;

  cheklocval = 0;

  if ((meshptr->ovlpglbval & PAMPA_OVLP_NOGT) != 0) {
    errorPrint ("no check could be done with flag PAMPA_OVLP_NOGT enabled");
    return (0);
  }

  // \bug a test must be added on nghbloctax between enttglbmax and baseval, because it is used in dmeshBuild

  proccomm = meshptr->proccomm;                   /* Simplify */
  baseval  = meshptr->baseval;
  enttglbnbr = meshptr->enttglbnbr;
  enttloctax = meshptr->enttloctax;

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commBarrier (proccomm) != MPI_SUCCESS) {    /* Synchronize */
    errorPrint ("communication error");
    cheklocval = -1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

  cheklocval =                                    /* Assume everything is all right */
    chekglbval = 0;
  MPI_Comm_size (proccomm, &procglbnbr);          /* Get communicator data */
  MPI_Comm_rank (proccomm, &proclocnum);

  for (vertlocnbr = 0, ventlocnum = baseval, enttnnd = enttglbnbr + baseval ; ventlocnum < enttnnd ; ventlocnum ++) {
    if (enttloctax[ventlocnum] == NULL)
      continue;

    if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)  // ce n'est pas une sous-entité
      vertlocnbr += enttloctax[ventlocnum]->vertlocnbr;
  }

  /* If communicator data don't correspond to those stored */
  if ((meshptr->procglbnbr != procglbnbr) ||
      (meshptr->proclocnum != proclocnum) ||
      ((meshptr->procdsptab == NULL) && (vertlocnbr != 0))) {
    errorPrint ("inconsistent communication data");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

  reduloctab[0] = (meshptr->procdsptab == NULL) ? 0 : 1; /* If private data not initialized */
CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce (reduloctab, reduglbtab, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return     (1);
  }
  if (reduglbtab[0] == 0)                         /* If distributed mesh is empty         */
    return (0);                                   /* Do not go any further                 */
  if (reduglbtab[0] != procglbnbr) {              /* If private data not consistently here */
    errorPrint ("inconsistent communication data");
    return     (1);
  }

  for (procrcvnum = 0; procrcvnum < meshptr->procglbnbr; procrcvnum ++) {
    if ((meshptr->proccnttab[procrcvnum] < 0)                                                                        ||
        (meshptr->proccnttab[procrcvnum] != (meshptr->procdsptab[procrcvnum + 1] - meshptr->procdsptab[procrcvnum])) ||
        (meshptr->proccnttab[procrcvnum] >  (meshptr->procvrttab[procrcvnum + 1] - meshptr->procvrttab[procrcvnum]))) {
      errorPrint ("inconsistent communication data");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
  }
  //! \todo why "<" is used?
  if (meshptr->proccnttab[proclocnum] < vertlocnbr) {
    errorPrint ("inconsistent communication data");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

  procrcvnum = (proclocnum + 1) % procglbnbr;     /* Compute indices of neighbors */
  procsndnum = (proclocnum - 1 + procglbnbr) % procglbnbr;

  if ((procngbtab = (Gnum *) memAlloc ((procglbnbr + 1) * sizeof (Gnum))) == NULL) {
    errorPrint ("out of memory");
    cheklocval = PAMPA_ERR_MEM;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce (&cheklocval, &chekglbval, 1, MPI_INT, MPI_MAX, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return     (-1);
  }
  /* If there is one propagated error */
  if (chekglbval != 0) {
    if (procngbtab != NULL)
      memFree (procngbtab);
    return (chekglbval);
  }

  /* Exchange procdsptab with neighbors (±1) and compare it to stored procdsptab */
  commSendrecv (meshptr->procdsptab, procglbnbr + 1, GNUM_MPI, procsndnum, TAGPROCVRTTAB, /* Check vertex range array */
      procngbtab,          procglbnbr + 1, GNUM_MPI, procrcvnum, TAGPROCVRTTAB,
      proccomm, &statloctab[0]);
  for (procngbnum = 0; procngbnum <= procglbnbr; procngbnum ++) {
    if (meshptr->procdsptab[procngbnum] != procngbtab[procngbnum]) {
      errorPrint ("inconsistent communication data");
      cheklocval = PAMPA_ERR_COMM;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }

  /* Exchange procvrttab with neighbors (±1) and compare it to stored procvrttab */
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  commSendrecv (meshptr->procvrttab, procglbnbr + 1, GNUM_MPI, procsndnum, TAGPROCVRTTAB, /* Check vertex range array */
      procngbtab,          procglbnbr + 1, GNUM_MPI, procrcvnum, TAGPROCVRTTAB,
      proccomm, &statloctab[0]);
  for (procngbnum = 0; procngbnum <= procglbnbr; procngbnum ++) {
    if (meshptr->procvrttab[procngbnum] != procngbtab[procngbnum]) {
      errorPrint ("inconsistent communication data");
      cheklocval = PAMPA_ERR_COMM;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
  memFree (procngbtab);

  /* Check some values, like baseval, edgelocnbr... */
  if ((meshptr->baseval < 0) ||                   /* Elementary constraints on mesh fields */
      (meshptr->baseval > 1) ||                   /* Strong limitation on base value        */
      (vertlocnbr < 0) ||
      (meshptr->edgelocnbr < 0) ||
      (meshptr->edgelocsiz < meshptr->edgelocnbr) ||
      (meshptr->valslocptr == NULL) ||
      (meshptr->valslocptr->valuglbmax < meshptr->valslocptr->valuglbnbr)) {
    errorPrint ("inconsistent local mesh data");
    cheklocval = PAMPA_ERR_LOC_DATA;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  /* Check if all processes have the same common values */
  reduloctab[ 0] =   meshptr->flagval;
  reduloctab[ 1] = - meshptr->flagval;
  reduloctab[ 2] =   meshptr->baseval;
  reduloctab[ 3] = - meshptr->baseval;
  reduloctab[ 4] =   meshptr->ovlpglbval;
  reduloctab[ 5] = - meshptr->ovlpglbval;
  reduloctab[ 6] =   meshptr->enttglbnbr;
  reduloctab[ 7] = - meshptr->enttglbnbr;
  reduloctab[ 8] =   meshptr->valslocptr->valuglbmax;
  reduloctab[ 9] = - meshptr->valslocptr->valuglbmax;
  reduloctab[10] = (Gnum) cheklocval;

CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce (reduloctab, reduglbtab, 11, GNUM_MPI, MPI_MAX, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return     (-1);
  }
  if (reduglbtab[10] != 0)
    return (reduglbtab[10]);
  if ((reduglbtab[ 1] != - reduglbtab[ 0]) ||       /* Check if global mesh data match */
      (reduglbtab[ 3] != - reduglbtab[ 2]) ||
      (reduglbtab[ 5] != - reduglbtab[ 4]) ||
      (reduglbtab[ 7] != - reduloctab[ 6]) ||
      (reduglbtab[ 9] != - reduglbtab[ 8])) {
    errorPrint ("inconsistent global mesh data");
    cheklocval = PAMPA_ERR_GLB_DATA;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  reduloctab[0] = (meshptr->esublocmsk != 0)    ? 1 : 0; /* Check consistency */
  reduloctab[1] = (Gnum) cheklocval;
CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce (reduloctab, reduglbtab, 2, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return     (-1);
  }
  if (reduglbtab[1] != 0)
    return (1);
  if (((reduglbtab[0] != 0) && (reduglbtab[0] != procglbnbr))) {
    errorPrint ("inconsistent global mesh data");
    cheklocval = PAMPA_ERR_GLB_DATA;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);




  enttglbnbr = meshptr->enttglbnbr;

  /* For each entity */
  for (ventlocnum = baseval, enttnnd = enttglbnbr + baseval, edgelocnbr = 0 ;
      ventlocnum < enttnnd && cheklocval == 0 ; ventlocnum ++) {
    DmeshEnttNghb ** nghbloctax;
    Gnum            vertlocnnd;
    Gnum            nentlocnum;
    Gnum            nentlocnnd;

    if (enttloctax[ventlocnum] == NULL)
      continue;

    if (meshptr->ovlpglbval > 0)
      vertlocnnd = enttloctax[ventlocnum]->vgstlocnbr + baseval;
    else
      vertlocnnd = enttloctax[ventlocnum]->vertlocnbr + baseval;

    /* If it is a sub-entity and if no local vertex, then next entity */
    if ((meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
        || (vertlocnnd == 0))
      continue;

    nghbloctax = enttloctax[ventlocnum]->nghbloctax;

    /* For each entity on neighbors */
    for (nentlocnum = baseval, nentlocnnd = enttglbnbr + baseval ;
        nentlocnum < nentlocnnd && cheklocval == 0 ; nentlocnum ++) {
      DmeshVertBounds *   vindloctax;
      Gnum                nghblocnnd;

      if (enttloctax[nentlocnum] == NULL)
        continue;

      /* If it is not a sub-entity */
      if (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {

        vindloctax = nghbloctax[nentlocnum]->vindloctax;
        nghblocnnd = enttloctax[nentlocnum]->vgstlocnbr + baseval;

        /* For each vertex */
        for (vertlocnum = baseval; vertlocnum < vertlocnnd  && cheklocval == 0 ; vertlocnum ++) {
          Gnum                edgelocnum;
          Gnum                mvrtlocnum;

          mvrtlocnum = enttloctax[ventlocnum]->mvrtloctax[vertlocnum];

          /* Check inconsistency on vindloctax */
          if ((vindloctax[vertlocnum].vendlocidx < vindloctax[vertlocnum].vertlocidx) ||
              (vindloctax[vertlocnum].vendlocidx > (meshptr->edgelocsiz + baseval))) {
            errorPrint ("inconsistent local vertex arrays");
            edgelocnbr = meshptr->edgelocnbr;     /* Avoid unwanted cascaded error messages */
            cheklocval = PAMPA_ERR_VRT_ARRAY;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);
          edgelocnbr += vindloctax[vertlocnum].vendlocidx - vindloctax[vertlocnum].vertlocidx;

          /* For each neighbor */
          for (edgelocnum = vindloctax[vertlocnum].vertlocidx;
              edgelocnum < vindloctax[vertlocnum].vendlocidx; edgelocnum ++) {
            /* If neighbor is less than baseval or greater than vgstlocnbr */
            if ((meshptr->edgeloctax[edgelocnum] < meshptr->baseval) ||
                (meshptr->edgeloctax[edgelocnum] > nghblocnnd)) {
              errorPrint ("inconsistent ghost edge array");
              edgelocnbr = meshptr->edgelocnbr;   /* Avoid unwanted cascaded error messages */
              cheklocval = PAMPA_ERR_LNK_ARRAY;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
            /* Check loop link */
            if (enttloctax[nentlocnum]->mvrtloctax[meshptr->edgeloctax[edgelocnum]] == mvrtlocnum) {
              errorPrint ("The vertex %d is linked to itself", mvrtlocnum);
              cheklocval = PAMPA_ERR_LNK_REDEF;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
          }
        }
      }
    }
  }


  if (edgelocnbr != meshptr->edgelocnbr) {
    errorPrint ("invalid local number of edges");
    cheklocval = PAMPA_ERR_LNK_NBR;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  /* If overlap */
  if (meshptr->ovlpglbval > 0) {
    for (ventlocnum = baseval, enttnnd = enttglbnbr + baseval ; ventlocnum < enttnnd ; ventlocnum ++) {
      if (enttloctax[ventlocnum] == NULL)
        continue;

      /* If it is not a sub-entity */
      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {
        Gnum vertlocnnd;
        Gnum edgelocidx;
        Gnum vertlocmax;

        /* Determine maximum of local vertex number */
        vertlocmax = enttloctax[ventlocnum]->vertlocnbr + baseval;

        /* For all vertices (local+overlap+ghost) */
        for (vertlocnum = baseval, vertlocnnd = enttloctax[ventlocnum]->vgstlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
          Gnum vmshlocnum;
          Gnum nentlocnum;
          Gnum nentlocnnd;

          vmshlocnum = enttloctax[ventlocnum]->mvrtloctax[vertlocnum];
          edgelocidx = baseval;

          /* Following test to check if vertex is linked to at least one element */
          /* If vertex number is greater than maximum number */
          if ((vertlocnum > vertlocmax)
              /* and it is a ghost element (neighbor entity is baseval and
               * vendlocidx == vertlocidx) */
              && (enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx == enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx)
              /* and vertex entity is not an element */
              && (ventlocnum != baseval)
              /* and vertex entity is not a sub-entity of element entity
               * (baseval) */
              && (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] != baseval)) {
            errorPrint ("The vertex %d is not linked to an element", vmshlocnum);
            cheklocval = PAMPA_ERR_LNK_ELEMENT;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          /* For each entity for neighbors */
          for (nentlocnum = baseval, nentlocnnd = enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++) {
            if (enttloctax[nentlocnum] == NULL)
              continue;

            /* If it is not a sub-entity */
            if (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { 
              DmeshVertBounds *vindloctax;
              Gnum edgelocnum;
              Gnum edgelocnnd;

              /* Check if vindloctax is not correctly filled */
              if (((enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx < edgelocidx)
                    && (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx != 0)
                    && (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx != 0))
                  || (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx < enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx)) {
                errorPrint("Internal error (1)\n");
                cheklocval = PAMPA_ERR_INTERNAL;
              }
              CHECK_VERR (cheklocval, meshptr->proccomm);
              edgelocidx = (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx != 0) ? enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx != 0 : edgelocidx;

              vindloctax = enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax;

              /* For each neighbor */
              for (edgelocnum = vindloctax[vertlocnum].vertlocidx, edgelocnnd = vindloctax[vertlocnum].vendlocidx;
                  edgelocnum < edgelocnnd ; edgelocnum ++) {
                Gnum nmshlocnum;
                Gnum nvrtlocnum;
                Gnum found;
                Gnum edgelocnm2;
                Gnum edgelocnd2;

                nvrtlocnum = meshptr->edgeloctax[edgelocnum];
                nmshlocnum = enttloctax[nentlocnum]->mvrtloctax[nvrtlocnum];

                /* Check if one link is duplicated */
                for (found = 0, edgelocnm2 = edgelocnum + 1, edgelocnd2 = edgelocnnd;
                    edgelocnm2 < edgelocnd2 && found == 0; edgelocnm2 ++)
                  if (meshptr->edgeloctax[edgelocnm2] == nvrtlocnum) {
                    found = 1;
                    errorPrint ("Link %d -> %d is duplicated", vmshlocnum, nmshlocnum);
                    cheklocval = PAMPA_ERR_LNK_DUP;
                  }
                CHECK_VERR (cheklocval, meshptr->proccomm);

                /* We know that v->n, check if n->v exists */
                vindloctax = enttloctax[nentlocnum]->nghbloctax[ventlocnum]->vindloctax;

                for (found = 0, edgelocnm2 = vindloctax[nvrtlocnum].vertlocidx, edgelocnd2 = vindloctax[nvrtlocnum].vendlocidx;
                    edgelocnm2 < edgelocnd2 ; edgelocnm2 ++) {
                  if (enttloctax[ventlocnum]->mvrtloctax[meshptr->edgeloctax[edgelocnm2]] == vmshlocnum) {
                    found = 1;
                    break;
                  }
                }

                if ((!found) && (vindloctax[nvrtlocnum].vertlocidx != vindloctax[nvrtlocnum].vendlocidx) && (ventlocnum != baseval) && (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] != baseval)) {
                  errorPrint ("Link %d -> %d is missing", nmshlocnum, vmshlocnum);
                  cheklocval = PAMPA_ERR_LNK_MISSING;
                }
              }
            }
          }
        }
      }
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
  else { // ovlpglbval == 0
    DmeshCheckVert * triploctab;
    int * psnddsptab;
    int * prcvdsptab;
    int * prcvcnttab;
    Gnum * prcvdattab;
    int * pnbrloctab;
    Gnum * psizloctab;
    Gnum ** pdatloctab;




    if (memAllocGroup ((void **) (void *)
          &triploctab, (size_t) (vertlocnbr * sizeof (DmeshCheckVert)),
          &psnddsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &prcvdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &prcvcnttab, (size_t) (meshptr->procglbnbr * sizeof (int)),
          &prcvdattab, (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
          &pnbrloctab, (size_t) (meshptr->procglbnbr * sizeof (int)),
          &psizloctab, (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
          &pdatloctab, (size_t) (meshptr->procglbnbr * sizeof (Gnum *)),
          (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      cheklocval = PAMPA_ERR_MEM;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

    memSet (pnbrloctab, 0, meshptr->procglbnbr * sizeof (int));
    memSet (psizloctab, 0, meshptr->procglbnbr * sizeof (Gnum));
    memSet (pdatloctab, 0, meshptr->procglbnbr * sizeof (Gnum *)); // TRICK: nul pointer


    Gnum triplocnum;
    /* For each entity */
    for (ventlocnum = baseval, enttnnd = enttglbnbr + baseval, triplocnum = 0 ; ventlocnum < enttnnd ; ventlocnum ++) {
      /* If no vertex (globally) in this entity */
      if (enttloctax[ventlocnum] == NULL)
        continue;

      /* If it is not a sub-entity */
      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {
        Gnum vertlocnnd;

        /* Store for each vertex its global number, local number and entity
         * number in triploctab */
        for (vertlocnum = baseval, vertlocnnd = enttloctax[ventlocnum]->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
          triploctab[triplocnum].vmshlocnum = enttloctax[ventlocnum]->mvrtloctax[vertlocnum];
          triploctab[triplocnum].vertlocnum = vertlocnum;
          triploctab[triplocnum++].enttlocnum = ventlocnum;
        }
      }
    }

    /* Sort triploctab according to the global vertex number */
    intSort3asc1(triploctab, vertlocnbr);

    Gnum vertlocmin;
    Gnum vertlocmax;

    /* Determine the interval of local vertex numbering */
    vertlocmin = meshptr->procvrttab[meshptr->proclocnum];
    vertlocmax = meshptr->procvrttab[meshptr->proclocnum + 1];

    //! \todo how could we test sub-entities? Also entity (baseval) could not be a sub-entity
    /* For each entity */
    for (ventlocnum = baseval, enttnnd = enttglbnbr + baseval ; ventlocnum < enttnnd ; ventlocnum ++) {
      /* If no vertex in entity */
      if (enttloctax[ventlocnum] == NULL)
        continue;

      /* If it is not a sub-entity */
      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {
        Gnum vertlocnnd;
        Gnum edgelocidx;

        /* For each vertex */
        for (vertlocnum = baseval, vertlocnnd = enttloctax[ventlocnum]->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
          Gnum vmshlocnum;
          Gnum nentlocnum;
          Gnum nentlocnnd;

          vmshlocnum = enttloctax[ventlocnum]->mvrtloctax[vertlocnum];
          edgelocidx = baseval;

          /* Check if the vertex is linked to an element */
          if ((enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx == enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx)
              && ((ventlocnum != baseval) || (enttloctax[baseval]->vgstlocnbr != 1))) { /* To avoid following error if there is only one element */
            errorPrint ("The vertex " GNUMSTRING " (entt " GNUMSTRING ") is not linked to an element", vmshlocnum, ventlocnum);
            cheklocval = PAMPA_ERR_LNK_MISSING;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          /* For each entity on neighbors */
          for (nentlocnum = baseval, nentlocnnd = enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++) {
            /* If no vertex in this entity */
            if (enttloctax[nentlocnum] == NULL)
              continue;

            /* If it is not a sub-entity */
            if (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {
              DmeshVertBounds *vindloctax; //!< \attention vindloctax is modified if neighbor is local
              Gnum edgelocnum;
              Gnum edgelocnnd;

              /* Check consistency of vindloctax */
              if (((enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx < edgelocidx)
                    && (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx != 0)
                    && (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx != 0))
                  || (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx < enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx)) {
                errorPrint("Internal error (1)\n");
                cheklocval = PAMPA_ERR_INTERNAL;
              }
              CHECK_VERR (cheklocval, meshptr->proccomm);
              edgelocidx = (enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx != 0) ? enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx != 0 : edgelocidx;

              vindloctax = enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax;
              /* For each neighbor */
              for (edgelocnum = vindloctax[vertlocnum].vertlocidx, edgelocnnd = vindloctax[vertlocnum].vendlocidx;
                  edgelocnum < edgelocnnd ; edgelocnum ++) {
                Gnum nmshlocnum;
                Gnum nvrtlocnum;
                Gnum edgelocnm2;
                Gnum edgelocnd2;
                Gnum found;

                nvrtlocnum = meshptr->edgeloctax[edgelocnum];
                nmshlocnum = enttloctax[nentlocnum]->mvrtloctax[nvrtlocnum];

                /* Check if v->n is duplicated */
                for (found = 0, edgelocnm2 = edgelocnum + 1, edgelocnd2 = edgelocnnd;
                    edgelocnm2 < edgelocnd2 && found == 0; edgelocnm2 ++)
                  if (meshptr->edgeloctax[edgelocnm2] == nvrtlocnum) {
                    found = 1;
                    errorPrint ("Link %d -> %d is duplicated", vmshlocnum, nmshlocnum);
                    cheklocval = PAMPA_ERR_LNK_DUP;
                  }
                CHECK_VERR (cheklocval, meshptr->proccomm);

                if ((nmshlocnum < vertlocmin) || (nmshlocnum >= vertlocmax)) { /* If neighbor is not local */
                  int                 procngbnum;
                  int                 procngbmax;

                  /* Determine processor number which have the neighbor */
                  for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
                      procngbmax - procngbnum > 1; ) {
                    int                 procngbmed;

                    procngbmed = (procngbmax + procngbnum) / 2;
                    if (meshptr->procvrttab[procngbmed] <= nmshlocnum)
                      procngbnum = procngbmed;
                    else
                      procngbmax = procngbmed;
                  }

                  /* If pnbrloctab has not enough space */
                  if (pnbrloctab[procngbnum] + 2 >= psizloctab[procngbnum]) {
                    psizloctab[procngbnum] = (psizloctab[procngbnum] == 0)
                      ?  MAX(20, enttloctax[nentlocnum]->vertglbnbr / (10 * meshptr->procglbnbr))
                      : (psizloctab[procngbnum] << 1); //!< \todo replace hardcoded value 20
                    if ((pdatloctab[procngbnum] = (Gnum *) memRealloc(pdatloctab[procngbnum], psizloctab[procngbnum] * sizeof (Gnum))) == NULL) {
                      errorPrint ("out of memory");
                      cheklocval = PAMPA_ERR_MEM;
                    }
                    CHECK_VERR (cheklocval, meshptr->proccomm);
                  }
                  /* store link in order to send it to corresponding processor
                   * */
                  pdatloctab[procngbnum][pnbrloctab[procngbnum] ++] = nmshlocnum;
                  pdatloctab[procngbnum][pnbrloctab[procngbnum] ++] = ventlocnum;
                  pdatloctab[procngbnum][pnbrloctab[procngbnum] ++] = vmshlocnum;

                }
                else { // neighbor is local
                  vindloctax = enttloctax[nentlocnum]->nghbloctax[ventlocnum]->vindloctax;
                  Gnum found;

                  /* We found v->n, check if n->v exists */
                  for (found = 0, edgelocnm2 = vindloctax[nvrtlocnum].vertlocidx, edgelocnd2 = vindloctax[nvrtlocnum].vendlocidx;
                      edgelocnm2 < edgelocnd2 ; edgelocnm2 ++) {
                    if (enttloctax[ventlocnum]->mvrtloctax[meshptr->edgeloctax[edgelocnm2]] == vmshlocnum) {
                      found = 1;
                      break;
                    }
                  }

                  if (!found) {
                    errorPrint ("Link %d -> %d is missing", nmshlocnum, vmshlocnum);
                    cheklocval = PAMPA_ERR_LNK_MISSING;
                  }
                  CHECK_VERR (cheklocval, meshptr->proccomm);
                }
              }
            }
          }
        }
      }
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);


    /* Determine psnddsptab with pnbrloctab */
    psnddsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
      psnddsptab[procngbnum] = psnddsptab[procngbnum - 1] + pnbrloctab[procngbnum - 1];
    }
    Gnum psndnbr = psnddsptab[meshptr->procglbnbr];

    /* Exchange pnbrloctab to determine prcvcnttab */
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commAlltoall(pnbrloctab, 1, MPI_INT, prcvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      return (-1);
    }

    /* Determine prcvdsptab with prcvcnttab */
    prcvdsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
      prcvdsptab[procngbnum] = prcvdsptab[procngbnum - 1] + prcvcnttab[procngbnum - 1];
    }
    Gnum prcvnbr = prcvdsptab[meshptr->procglbnbr];

    Gnum * psnddattab;

    if (memAllocGroup ((void **) (void *)
          &psnddattab, (size_t) (psndnbr * sizeof (Gnum)),
          &prcvdattab, (size_t) (prcvnbr * sizeof (Gnum)),
          (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      cheklocval = PAMPA_ERR_MEM;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    /* Fill in psnddattab */
    Gnum idx;
    for (idx = 0, procngbnum = 0 ; procngbnum < meshptr->procglbnbr ; procngbnum ++) {
      Gnum psndnum;
      Gnum psndnnd;

      for (psndnum = 0, psndnnd = pnbrloctab[procngbnum] ; psndnum < psndnnd ; psndnum ++)
        psnddattab[idx ++] = pdatloctab[procngbnum][psndnum];
    }

    /* Send psnddattab and receive prcvdattab */
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commAlltoallv(psnddattab, pnbrloctab, psnddsptab, GNUM_MPI, prcvdattab, prcvcnttab, prcvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      return (-1);
    }

    Gnum vertrcvnum;
    Gnum vertrcvnnd;
    /* For each received triplet */
    for (vertrcvnum = 0, vertrcvnnd = prcvnbr ; vertrcvnum < vertrcvnnd ; vertrcvnum += 3) {
      Gnum vmshlocnum;
      Gnum nentlocnum;
      Gnum nmshlocnum;
      Gnum                 vmshlocnm2;
      Gnum                 vmshlocmax;
      DmeshVertBounds *vindloctax;

      vmshlocnum = prcvdattab[vertrcvnum];
      nentlocnum = prcvdattab[vertrcvnum + 1];
      nmshlocnum = prcvdattab[vertrcvnum + 2];


      /* Dichotomy search to find vmshlocnum */
      for (vmshlocnm2 = 0, vmshlocmax = vertlocnbr;
          vmshlocmax - vmshlocnm2 > 1; ) {
        Gnum                 vmshlocmed;

        vmshlocmed = (vmshlocmax + vmshlocnm2) / 2;
        if (triploctab[vmshlocmed].vmshlocnum <= vmshlocnum)
          vmshlocnm2 = vmshlocmed;
        else
          vmshlocmax = vmshlocmed;
      }
#ifdef PAMPA_DEBUG_DMESH2
      if (triploctab[vmshlocnm2].vmshlocnum != vmshlocnum) {
        errorPrint ("vmshlocnum %d not found", vmshlocnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

      vertlocnum = triploctab[vmshlocnm2].vertlocnum;
      ventlocnum = triploctab[vmshlocnm2].enttlocnum;

      vindloctax = enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax;

      Gnum edgelocnum;
      Gnum edgelocnnd;
      Gnum found;

      /* Check if n->v exists */
      for (found = 0, edgelocnum = vindloctax[vertlocnum].vertlocidx, edgelocnnd = vindloctax[vertlocnum].vendlocidx;
          edgelocnum < edgelocnnd ; edgelocnum ++) {

        if (enttloctax[nentlocnum]->mvrtloctax[meshptr->edgeloctax[edgelocnum]] == nmshlocnum) {
          found = 1;
          break;
        }
      }

      /* No n->v found */
      if (!found) {
        errorPrint ("Link %d -> %d is missing", vmshlocnum, nmshlocnum);
        cheklocval = PAMPA_ERR_LNK_MISSING;
      }
    }
    for (procngbnum = 0 ; procngbnum < meshptr->procglbnbr ; procngbnum ++)
      memFree (pdatloctab[procngbnum]);
    memFreeGroup (triploctab);
    memFreeGroup (psnddattab);
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);


  // \todo temporary disabled, bug reported by Vincent
  //for (ventlocnum = baseval, enttnnd = enttglbnbr + baseval ; ventlocnum < enttnnd ; ventlocnum ++) 
  //  CHECK_FERR (dmeshCheck2 (meshptr, baseval, ventlocnum), meshptr->proccomm);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

//! \brief This function checks if two elements have the same neighbors 
//!
//! \returns  0 : on success.
//! \returns !0 : on error.
int
dmeshCheck2 (
    Dmesh * restrict const meshptr,
    Gnum const             ventlocnum,
    Gnum const             nentlocnum)
{
  int cheklocval = 0;
  DmeshEntity * restrict ventlocptr;
  DmeshEntity * restrict nentlocptr;
  Gnum * restrict        vertloctb1;
  Gnum * restrict        vertloctb2;
  DmeshCheckElem * restrict hashtab;
  Gnum                      hashsiz;          /* Size of hash table    */
  Gnum                      hashnum;          /* Hash value            */
  Gnum                      hashmax;
  Gnum                      hashnbr;
  Gnum                      hashmsk;
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it_vert;

  ventlocptr = meshptr->enttloctax[ventlocnum];
  nentlocptr = meshptr->enttloctax[nentlocnum];

  hashnbr = ventlocptr->vovplocnbr * 4;
  for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */
  hashmsk = hashsiz - 1;
  hashmax = hashsiz >> 2;

  if (memAllocGroup ((void **) (void *)
              &vertloctb1, (size_t) (meshptr->degrlocmax * sizeof (Gnum)),
              &vertloctb2, (size_t) (meshptr->degrlocmax * sizeof (Gnum)),
              (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      cheklocval = PAMPA_ERR_MEM;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if ((hashtab = (DmeshCheckElem *) memAlloc (hashsiz * sizeof (DmeshCheckElem))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  memSet (hashtab, ~0, hashsiz * sizeof (DmeshCheckElem));


  PAMPA_dmeshItInitStart((PAMPA_Dmesh *) meshptr, ventlocnum, PAMPA_VERT_ANY, &it_vert) ;
  PAMPA_dmeshItInit((PAMPA_Dmesh *) meshptr, ventlocnum, nentlocnum, &it_nghb) ;
  while (PAMPA_itHasMore(&it_vert)) {
    Gnum vertnum;
    Gnum nghblocsum = 0;

    vertnum = PAMPA_itCurEnttVertNum(&it_vert);

    PAMPA_itStart(&it_nghb, vertnum) ;
    while (PAMPA_itHasMore(&it_nghb)) {
      nghblocsum += PAMPA_itCurEnttVertNum(&it_nghb);

      PAMPA_itNext(&it_nghb);
    }

    for (hashnum = (nghblocsum * DMESHCHECKHASHPRIME) & hashmsk; hashtab[hashnum].vertsum != ~0 && hashtab[hashnum].vertsum != nghblocsum; hashnum = (hashnum + 1) & hashmsk) ;

    if (hashtab[hashnum].vertsum != nghblocsum) { /* Vertex not already added */
        hashtab[hashnum].vertsum = nghblocsum;
        hashtab[hashnum].vertnum = vertnum;
        hashnbr ++;

        if (hashnbr >= hashmax)  /* If hashtab is too much filled */
          CHECK_FERR (dmeshCheckElemResize (&hashtab, &hashsiz, &hashmax, &hashmsk), meshptr->proccomm);
    }
    else { /* Vertex with same sum already added */
      Gnum vertid1 = 0;
      Gnum vertid2 = 0;

      if (((ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertnum].vendlocidx)
            - (ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertnum].vertlocidx)) ==
          ((ventlocptr->nghbloctax[nentlocnum]->vindloctax[hashtab[hashnum].vertnum].vendlocidx)
           - (ventlocptr->nghbloctax[nentlocnum]->vindloctax[hashtab[hashnum].vertnum].vertlocidx))) {
        /* Search neighbors of new vertex */
        PAMPA_itStart(&it_nghb, vertnum) ;
        while (PAMPA_itHasMore(&it_nghb)) {
          vertloctb1[vertid1 ++] = PAMPA_itCurEnttVertNum(&it_nghb);

          PAMPA_itNext(&it_nghb);
        }
        intSort1asc1(vertloctb1, vertid1);

        /* Search neighbors of added vertex */
        PAMPA_itStart(&it_nghb, hashtab[hashnum].vertnum) ;
        while (PAMPA_itHasMore(&it_nghb)) {
          vertloctb2[vertid2 ++] = PAMPA_itCurEnttVertNum(&it_nghb);

          PAMPA_itNext(&it_nghb);
        }
        intSort1asc1(vertloctb2, vertid2);

        for (vertnum = 0; vertnum < vertid1 && (vertloctb1[vertnum] == vertloctb2[vertnum]); vertnum ++);

        if (vertnum == vertid1) {
          errorPrint ("Element %d is the same as %d with\n\t", vertnum, hashtab[hashnum].vertnum);
          for (vertnum = 0; vertnum < vertid1; vertnum ++)
            printf ("%d ", vertloctb1[vertnum]);
          printf ("\n");
        }
      }

    }

    PAMPA_itNext(&it_vert);
  }

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

//! \brief This function reallocate hash table used to store elements in
//! dmeshCheck2
//!
//! \returns  0 : on success.
//! \returns !0 : on error.
static
  int
dmeshCheckElemResize (
	DmeshCheckElem * restrict * hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (DmeshCheckElem *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (DmeshCheckElem)))) == NULL) {
    errorPrint ("Out of memory");
    return (1);
  }

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (DmeshCheckElem)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].vertnum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  // \todo temporary disabled, bug reported by Vincent: blocked here in this loop
  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum vertnum;
    Gnum vertsum;

    vertnum = (*hashtabptr)[hashidx].vertnum;
    vertsum = (*hashtabptr)[hashidx].vertsum;

    if (vertnum == ~0) // If empty slot
      continue;

    for (hashnum = (vertnum * DMESHCHECKHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].vertnum != ~0 && ((*hashtabptr)[hashnum].vertnum != vertnum || (*hashtabptr)[hashnum].vertsum != vertsum); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].vertnum = ~0;
    (*hashtabptr)[hashidx].vertsum = ~0;
  }

  return (0);
}

