
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "smesh.h"
#include <pampa.h>

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed smesh handling routines. */
/*                                      */
/****************************************/

//! \brief This routine fills the contents of the given
//! opaque centralized smesh structure with the
//! data provided by the user. The base value
//! allows the user to set the smesh base to 0 or 1.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_smeshBuild (
PAMPA_Smesh * const        meshptr,           //!< Distributed smesh structure to fill
const PAMPA_Num            baseval,           //!< Base for indexing                 
const PAMPA_Num            vertnbr,           //!< Number of local vertices         
PAMPA_Num * const          verttab,           //!< Local vertex begin array          
PAMPA_Num * const          vendtab,           //!< Local vertex end array            
const PAMPA_Num            edgenbr,           //!< Number of local edges             
const PAMPA_Num            edgesiz,           //!< Size of local edges             
PAMPA_Num * const          edgetab,           //!< Local edge array                  
PAMPA_Num * const          edlotab,           //!< Local edge load array (if any)    
PAMPA_Num                  enttnbr,           //!< Number of entities
PAMPA_Num *                venttab,           //!< Vertex entity array
PAMPA_Num *                esubtab,           //!< Entity group array
PAMPA_Num *                enlotab,           //!< Global entity load array (if any) (triplet : [entity, entity, load] and first value is the number of triplets)
PAMPA_Num                  valumax)           //!< Maximum number of linked values
{
  Smesh *                  srcmeshptr;         /* Pointer to source mesh structure          */
  Gnum *                   venttax;
  Gnum *                   verttax;
  Gnum *                   vendtax;
  Gnum *                   edgetax;
  Gnum *                   edlotax;
  Gnum *                   esubtax;

  // XXX mettre à jour smeshBuild par rapport aux arguments de PAMPA_smeshBuild

#ifdef PAMPA_DEBUG_LIBRARY1
  if (sizeof (PAMPA_Smesh) < sizeof (Smesh)) {
    errorPrint ("internal error");
    return     (1);
  }
#endif /* PAMPA_DEBUG_LIBRARY1 */

  // XXX : faire l'appel à graphdata pour avoir le degrmax !
  
  srcmeshptr = (Smesh *) meshptr;                /* Use structure as source smesh */

  verttax = (Gnum *) verttab - baseval;
  vendtax = ((vendtab == NULL) || (vendtab == verttab + 1)) ? verttax + 1 : (Gnum *) vendtab - baseval;
  edgetax = (Gnum *) edgetab - baseval;
  edlotax = (edlotab == NULL) ? NULL : edlotab - baseval;
  venttax = (Gnum *) venttab - baseval;
  esubtax = (esubtab == NULL) ? NULL : (Gnum *) esubtab - baseval;

  return (smeshBuild (srcmeshptr, -1, baseval, vertnbr, verttax, vendtax, edgenbr, edgesiz, edgetax, edlotax, enttnbr, venttax, esubtax, valumax, NULL, -1, -1));
}
