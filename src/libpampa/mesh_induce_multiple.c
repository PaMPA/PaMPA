/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_induce_multiple.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 10 Oct 2017
//!                             to:   17 Oct 2017
//!
/************************************************************/

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "smesh_mesh.h"
#include "mesh_induce_multiple.h"
#include "pampa.h"

//! This routine rebuilds a mesh XXX
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshInduceMultiple (
    Mesh * restrict const       imshptr,           //!< Input mesh
    const Gnum * restrict const vindtax,           //!< Vertex array
    const Gnum                  omshnbr,           //!< Number of output meshes
    Mesh * restrict const       omshtab)           //!< Output meshes
{
  Gnum * restrict * restrict vprmtab; // permutation array (output to input)
  Gnum * restrict            vnbrtab;
  Gnum * restrict            enbrtab;
  Gnum                       omshnum;
  Smesh                      smshdat;
  Gnum                       vertnum;
  Gnum                       vertnnd;
  Gnum                       baseval;
  Gnum *                     verttax;
  Gnum *                     vendtax;
  Gnum *                     venttax;
  Gnum * restrict            edgetax;
  Gnum * restrict            edlotax;
  Gnum *                     mtoeinptax;
  Gnum *                     itoomshtax;
  Gnum *                     itooenttax;
  Gnum                       edlosiz;
  PAMPA_Iterator             it;

  baseval = imshptr->baseval;

  edlosiz = (imshptr->edlotax != NULL) ? imshptr->edgesiz : 0;
  if (memAllocGroup ((void **) (void *)
        &vprmtab, (size_t) (omshnbr          * sizeof (Gnum *)),
        &vnbrtab, (size_t) (omshnbr          * sizeof (Gnum)),
        &enbrtab, (size_t) (omshnbr          * sizeof (Gnum)),
        &verttax, (size_t) (imshptr->vertnbr * sizeof (Gnum)),
        &vendtax, (size_t) (imshptr->vertnbr * sizeof (Gnum)),
        &venttax, (size_t) (imshptr->vertnbr * sizeof (Gnum)),
        &edgetax, (size_t) (imshptr->edgesiz * sizeof (Gnum)),
        &edlotax, (size_t) (edlosiz          * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }
  verttax -= baseval;
  vendtax -= baseval;
  venttax -= baseval;
  edgetax -= baseval;
  edlotax = (imshptr->edlotax != NULL) ? (edlotax - baseval) : NULL;

  for (omshnum = 0; omshnum < omshnbr; omshnum ++) {
    if ((vprmtab[omshnum] = (Gnum *) memAlloc (imshptr->vertnbr * sizeof (Gnum))) == NULL) {
      errorPrint("out of memory");
      return (1);
    }

    memSet (vprmtab[omshnum], ~0, imshptr->vertnbr * sizeof (Gnum));
  }

  CHECK_FDBG2 (smeshInit (&smshdat));
  CHECK_FDBG2 (mesh2smesh (imshptr, &smshdat));


  PAMPA_meshItInitStart((PAMPA_Mesh *) imshptr, baseval, &it);
  while (PAMPA_itHasMore(&it)) {
    Gnum tetrnum;
    Gnum mtetnum;
    Gnum edgenum;
    Gnum edgennd;

    tetrnum = PAMPA_itCurEnttVertNum(&it);
    mtetnum = PAMPA_itCurMeshVertNum(&it);

    if ((omshnum = vindtax[tetrnum]) != ~0) {
      vprmtab[omshnum][mtetnum] = 1;
      for (edgenum = smshdat.verttax[mtetnum], edgennd = smshdat.vendtax[mtetnum]; edgenum < edgennd; edgenum ++) {
        Gnum nghbnum;

        nghbnum = smshdat.edgetax[edgenum];
        if (smshdat.venttax[nghbnum] != baseval)
          vprmtab[omshnum][nghbnum] = 1;
      }
    }

    PAMPA_itNext(&it);
  }

  /* Compute permutation array */
  for (omshnum = 0; omshnum < omshnbr; omshnum ++) {
    Gnum vertidx;

    for (vertidx = vertnum = baseval, vertnnd = baseval + imshptr->vertnbr; vertnum < vertnnd; vertnum ++)
      if (vprmtab[omshnum][vertnum] != ~0)
        vprmtab[omshnum][vertnum] = vertidx ++;
    vnbrtab[omshnum] = vertidx - baseval;
  }

  for (omshnum = 0; omshnum < omshnbr; omshnum ++) {
    Gnum vertidx;
    Gnum edgeidx;

    for (edgeidx = vertidx = vertnum = baseval, vertnnd = baseval + smshdat.vertnbr; vertnum < vertnnd; vertnum ++) {
      if ((vertidx = vprmtab[omshnum][vertnum]) != ~0) {
        Gnum edgenum;
        Gnum edgennd;

        verttax[vertidx] = edgeidx;
        venttax[vertidx] = smshdat.venttax[vertnum];

        for (edgenum = smshdat.verttax[vertnum], edgennd = smshdat.vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
          Gnum nghbnum;
          Gnum nghbidx;

          nghbnum = smshdat.edgetax[edgenum];
          if ((nghbidx = vprmtab[omshnum][nghbnum]) != ~0) {
            if (smshdat.edlotax != NULL)
              edlotax[edgeidx] = smshdat.edlotax[edgenum];
            edgetax[edgeidx ++] = nghbidx;
          }
        }
        vendtax[vertidx] = edgeidx;
      }
    }

    enbrtab[omshnum] = edgeidx - baseval;
    CHECK_FDBG2 (meshBuild (omshtab + omshnum, (Gnum) ~0, baseval, vnbrtab[omshnum], verttax, vendtax, enbrtab[omshnum], enbrtab[omshnum], edgetax, edlotax, smshdat.enttnbr, venttax, smshdat.esubtax, smshdat.valsptr->valumax, NULL, ~0, smshdat.procglbnbr));
    // XXX si on décommente la partie ci-dessous, ça ne compile plus
    // alors que la fonction est bien définie dans la bib…
//#ifdef PAMPA_DEBUG_MESH
//    CHECK_FDBG2 (meshCheck2 (omshtab + omshnum, PAMPA_CHECK_ALONE));
//#endif /* PAMPA_DEBUG_MESH */
  }

  /* Induce values */
  { 
    Value * valuptr;
    Gnum    enttnum;
    Gnum    enttnnd;

    /* Copy values on virtual entity: processors */
    for (omshnum = 0; omshnum < omshnbr; omshnum ++) {
      for (valuptr = imshptr->valsptr->evaltak[PAMPA_ENTT_VIRT_PROC]; valuptr != NULL; valuptr = valuptr->next) {
        MPI_Aint            valusiz;                 /* Extent of attribute datatype */
        MPI_Aint            dummy;
        byte *              valutax;
        Gnum                sizeval;

        MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */


        CHECK_FDBG2 (meshValueLink(omshtab + omshnum, (void **) &valutax, valuptr->flagval & (!PAMPA_VALUE_ALLOCATED), &sizeval, NULL, valuptr->typeval, PAMPA_ENTT_VIRT_PROC, valuptr->tagnum));
        memCpy (valutax, valuptr->valutab, sizeval * valusiz * sizeof (byte));
      }
    }

    mtoeinptax = verttax; /* vertex numbering array (mesh numbering to entity numbering, for imshptr) */
    itoomshtax = vendtax; /* vertex numbering array (output mesh numbering to input mesh numbering) */
    //etomouttax = vendtax; /* vertex numbering array (entity numbering to mesh numbering, for omshptr) */
    itooenttax = venttax; /* vertex numbering array (output mesh to input mesh) */

    for (enttnum = baseval, enttnnd = baseval + imshptr->enttnbr; enttnum < enttnnd; enttnum ++) {
      if (imshptr->entttax[enttnum] == NULL)
        continue;

      for (vertnum = baseval, vertnnd = baseval + imshptr->entttax[enttnum]->vertnbr;
          vertnum < vertnnd; vertnum ++) {
        Gnum mvrtnum;

        mvrtnum = imshptr->entttax[enttnum]->mvrttax[vertnum];
        mtoeinptax[mvrtnum] = vertnum;
      }
    }

    for (int rounval = 0; rounval < 2; rounval ++) { /* rounval 0 for entities and 1 for sub-entities */

      for (omshnum = 0; omshnum < omshnbr; omshnum ++) {
        /* Update itoomshtax */
        for (vertnum = baseval, vertnnd = baseval + smshdat.vertnbr; vertnum < vertnnd; vertnum ++) 
          if (vprmtab[omshnum][vertnum] != ~0)
            itoomshtax[vprmtab[omshnum][vertnum]] = vertnum;

        for (enttnum = PAMPA_ENTT_VIRT_VERT, enttnnd = baseval + smshdat.enttnbr; enttnum < enttnnd; enttnum ++) {
          Gnum    vertmax;

          switch (enttnum) {
            case PAMPA_ENTT_DUMMY:
              break;
            case PAMPA_ENTT_VIRT_VERT:
              if (rounval == 0) /* we copy only once values */
                continue;

              for (vertnum = baseval, vertnnd = baseval + imshptr->vertnbr + baseval;
                  vertnum < vertnnd; vertnum ++)
                if (vprmtab[omshnum][vertnum] != ~0)
                  itooenttax[vprmtab[omshnum][vertnum]] = vertnum;
              vertmax = omshtab[omshnum].vertnbr + baseval;
              break;
            default:
              //if ((imshptr->entttax[enttnum] == NULL) || (omshtab[omshnum].entttax[enttnum] == NULL))
              //if (imshptr->entttax[enttnum] == NULL)
              //  continue;

              /* rounval 0 for entities and 1 for sub-entities */
              if (((rounval == 0) && (imshptr->esubbax[enttnum & imshptr->esubmsk] > PAMPA_ENTT_SINGLE))
                  || ((rounval == 1) && (imshptr->esubbax[enttnum & imshptr->esubmsk] <= PAMPA_ENTT_SINGLE)))
                  continue;

              /* XXX not implemented for sub-entities */
              if ((rounval == 1) && (imshptr->valsptr->evaltak[enttnum] != NULL)) {
                errorPrint ("Not yet implemented");
                return     (1);
              }


              if (omshtab[omshnum].entttax[enttnum] != NULL) {
                for (vertnum = baseval, vertnnd = baseval + omshtab[omshnum].entttax[enttnum]->vertnbr;
                    vertnum < vertnnd; vertnum ++) {
                  Gnum mvrtnum;


                  mvrtnum = omshtab[omshnum].entttax[enttnum]->mvrttax[vertnum];
                  //etomouttax[vertnum] = mvrtnum;
                  itooenttax[vertnum] = mtoeinptax[itoomshtax[mvrtnum]];
                }
                vertmax = omshtab[omshnum].entttax[enttnum]->vertnbr;
              }
              else
                vertmax = baseval;
          }

          for (valuptr = imshptr->valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next) {
            MPI_Aint            valusiz;                 /* Extent of attribute datatype */
            MPI_Aint            dummy;
            byte *              valutax;
            Gnum                dstvertnum;
            Gnum                dstvertnnd;
            Gnum                sizeval;

            MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */

            CHECK_FDBG2 (meshValueLink(omshtab + omshnum, (void **) &valutax, valuptr->flagval & (!PAMPA_VALUE_ALLOCATED), &sizeval, NULL, valuptr->typeval, enttnum, valuptr->tagnum));
            memSet (valutax, ~0, sizeval * valusiz * sizeof (byte)); // XXX à supprimer

            valutax -= baseval * valusiz;

            for (dstvertnum = baseval, dstvertnnd = vertmax; dstvertnum < dstvertnnd; dstvertnum ++) {
              Gnum srcvertnum;

              srcvertnum = itooenttax[dstvertnum];
              memCpy ((byte *) valutax + dstvertnum * valusiz, (byte *) valuptr->valutab + (srcvertnum - baseval) * valusiz, valusiz * sizeof (byte));
            } /* loop on vertices */
          } /* loop on values */ 
        } /* loop on entities */
      } /* loop on omshnum */
    } /* loop on roundval */
  } /* block on values */

  smeshExit (&smshdat);
  for (omshnum = 0; omshnum < omshnbr; omshnum ++) 
    memFree (vprmtab[omshnum]);
  memFreeGroup (vprmtab);

  return (0);
}
