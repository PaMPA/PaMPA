/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        smesh_mesh.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the converting
//!                routines between simplified mesh and
//!                mesh.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */


#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "pampa.h"


int smesh2mesh (
	Smesh * const smshptr,
	Mesh * const meshptr)
{
  return meshBuild (meshptr,
	  smshptr->idnum,
	  smshptr->baseval,
	  smshptr->vertnbr,
	  smshptr->verttax,
	  smshptr->vendtax,
	  smshptr->edgenbr,
	  smshptr->edgesiz,
	  smshptr->edgetax,
	  smshptr->edlotax,
	  smshptr->enttnbr,
	  smshptr->venttax,
	  smshptr->esubtax,
	  0,
	  smshptr->valsptr,
	  smshptr->degrmax,
          smshptr->procglbnbr);
}

int mesh2smesh (
	Mesh * const meshptr,
	Smesh * const smshptr)
{
  Gnum * verttax;
  Gnum * vendtax;
  Gnum * venttax;
  Gnum * edgetax;
  Gnum * edlotax;
  Gnum * esubtax;
  Gnum   edlonbr;
  Gnum   esubnbr;
  Gnum   ventnum;
  Gnum   ventnnd;
  Gnum   edgeidx;
  Gnum   baseval;

  baseval = meshptr->baseval;

  edlonbr = (meshptr->edlotax == NULL) ? 0 : meshptr->edgesiz;
  esubnbr = (meshptr->esubmsk == 0) ? 0 : meshptr->enttnbr;

  if (memAllocGroup ((void **) (void *)
        &venttax, (size_t) (meshptr->vertnbr * sizeof (Gnum)),
        &verttax, (size_t) (meshptr->vertnbr * sizeof (Gnum)),
        &vendtax, (size_t) (meshptr->vertnbr * sizeof (Gnum)),
        &edgetax, (size_t) (meshptr->edgesiz * sizeof (Gnum)),
        &edlotax, (size_t) (edlonbr * sizeof (Gnum)),
        &esubtax, (size_t) (esubnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }

  venttax -= baseval;
  verttax -= baseval;
  vendtax -= baseval;
  edgetax -= baseval;
  edlotax = (edlonbr == 0) ? NULL : edlotax - baseval;
  esubtax = (esubnbr == 0) ? NULL : esubtax - baseval;

  memSet (verttax + baseval, ~0, meshptr->vertnbr * sizeof (Gnum));
  memSet (vendtax + baseval, ~0, meshptr->vertnbr * sizeof (Gnum));

  if (esubtax != NULL)
	memCpy (esubtax + baseval, meshptr->esubbax + baseval, meshptr->enttnbr * sizeof (Gnum));

  for (edgeidx = ventnum = baseval, ventnnd = meshptr->enttnbr + baseval; ventnum < ventnnd; ventnum ++) {
	MeshEntity * restrict ventptr;
	Gnum vertnum;
	Gnum vertnnd;

    if ((meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) || (meshptr->entttax[ventnum] == NULL))
	  continue;

	ventptr = meshptr->entttax[ventnum];

  	for (vertnum = baseval, vertnnd = ventptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++)  {
	  Gnum mvrtnum;
	  Gnum nentnum;
	  Gnum nentnnd;

	  mvrtnum = ventptr->mvrttax[vertnum];
	  venttax[mvrtnum] = (ventptr->venttax == NULL) ? ventnum : ventptr->venttax[vertnum];
	  verttax[mvrtnum] = edgeidx;

  	  for (nentnum = baseval, nentnnd = meshptr->enttnbr + baseval; nentnum < nentnnd; nentnum ++) {
		MeshEntity * restrict nentptr;
		Gnum edgenum;
		Gnum edgennd;

    	if ((meshptr->esubbax[nentnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) || (meshptr->entttax[nentnum] == NULL))
	  	  continue;

		nentptr = meshptr->entttax[nentnum];

      	for (edgenum = ventptr->nghbtax[nentnum]->vindtax[vertnum].vertidx,
		  	edgennd = ventptr->nghbtax[nentnum]->vindtax[vertnum].vendidx;
		  	edgenum < edgennd ; edgenum ++) {
		  if (edlotax != NULL)
			edlotax[edgeidx] = meshptr->edlotax[edgenum];
		  edgetax[edgeidx ++] = nentptr->mvrttax[meshptr->edgetax[edgenum]];
		}
	  }

	  vendtax[mvrtnum] = edgeidx;
	}
  }

  smshptr->flagval |= SMESHFREETABS;

  return smeshBuild (smshptr,
	  meshptr->idnum,
	  baseval,
	  meshptr->vertnbr,
	  verttax,
	  vendtax,
	  meshptr->edgenbr,
	  meshptr->edgesiz,
	  edgetax,
	  edlotax,
	  meshptr->enttnbr,
	  venttax,
	  esubtax,
	  0,
	  meshptr->valsptr,
	  meshptr->degrmax,
          meshptr->procglbnbr);
}

