/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_overlap.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the distributed mesh overlap building
//!                routines.
//!
//!   \date        Version 1.0: from: 17 Feb 2012
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define DMESHOVLPHASHPRIME     17            //!< Prime number for hashing

#define ENTT_MAIN_INV -1                //!< XXX

#define FLAG_OVLP 1
#define FLAG_HALO 2

typedef struct DmeshOvlpProcVert_                    //!  XXX
{
  Gnum          vertnum;                                //!< XXX
  int          procnum;                                //!< XXX
  byte         flagval;
  Gnum         ovlpval;
} DmeshOvlpProcVert;                                 //!< XXX

int
dmeshOverlap (
    Dmesh * restrict const      meshptr,
    const Gnum                  vertlocnbr,
    Gnum * const                vertloctax,
    Gnum * const                vendloctax,
    const Gnum                  edgelocsiz,
    Gnum * restrict const       edgeloctax,
    Gnum *                      ventloctax,
    Gnum * restrict *           ventgsttax,
    Gnum *                      vertovpnbr,
    Gnum * restrict *           vertovptax,
    Gnum * restrict *           vendovptax,
    Gnum * restrict *           vnumovptax,
    Gnum *                      edgeovpnbr,
    Gnum * restrict *           edgegsttax,
    const Gnum                  ovlpglbval);


int
dmeshOverlap2 (
    Dmesh * restrict const      meshptr,
    const Gnum                  vertlocnbr,
    Gnum * const                vertloctax,
    Gnum * const                vendloctax,
    const Gnum                  edgelocsiz,
    Gnum * restrict const       edgeloctax,
    Gnum *                      ventloctax,
    Gnum * restrict *           ventgsttax,
    Gnum *                      vertovpnbr,
    Gnum * restrict *           vertovptax,
    Gnum * restrict *           vendovptax,
    Gnum * restrict *           vnumovptax,
    Gnum *                      edgeovpnbr,
    Gnum * restrict *           edgegsttax,
    const Gnum                  ovlpglbval,
  DmeshOvlpProcVert * restrict  hashtab,
  Gnum                          hashsiz,
  Gnum                          hashnbr,
  Gnum                          hashmsk);

int
dmeshOvlpProcVertResize (
	DmeshOvlpProcVert * restrict * hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr);
