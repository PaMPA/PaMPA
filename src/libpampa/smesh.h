/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        smesh.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the simplified mesh handling routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define SMESH_H

/*
** The defines.
*/

/* smesh flags. */

#define SMESHNONE                  0x0000        /* No options set                             */

#define SMESHFREETABS              0x0004        /* Set if arrays freed on exit               */

/*
** The type and structure definitions.
*/

/* The smesh basic types, which must be signed. */

#ifndef GNUMMAX                                   
typedef INT                 Gnum;                 /* Vertex or edge number   */
#define GNUMMAX                     (INTVALMAX)   /* Maximum Gnum value      */
#endif /* GNUMMAX */

#define GNUM_MPI                    COMM_INT      /* MPI type for Gnum is MPI type for INT */



typedef int smeshFlag;                           /*+ Smesh property flags +*/





typedef struct Smesh_   
{
  Gnum                 idnum;       //!< Id
  smeshFlag            flagval;     //!< Smesh properties
  Gnum                 baseval;     //!< Base index for edge/vertex/entity arrays
  Gnum                 enttnbr;     //!< Number of types which make up the smesh
  Gnum                 vertnbr;     //!< Number of vertices
  Gnum *               venttax;     //!< Array of entity values for each vertex XXX à supprimer ???
  Gnum *               verttax;
  Gnum *               vendtax;
  Gnum *               esubtax;     //!< XXX
  Gnum                 edgenbr;     //!< XXX
  Gnum                 edgesiz;     //!< XXX
  Gnum *               edgetax;     //!< XXX
  Gnum *			   edlotax;     //!<
  Gnum                 degrmax;
  Values *          valsptr;     //!< Pointer on associated values
  MPI_Comm             proccomm;    //!< Smesh communicator
  int                  procglbnbr;  //!< Number of processes sharing smesh data
  int                  proclocnum;  //!< Number of this process
} Smesh;

/*
** The functions prototypes.
*/

int smeshInit (
    Smesh * const           smshptr);

void smeshExit (
    Smesh * const           smshptr);

void smeshFree (
    Smesh * const           smshptr);

int smeshBuild (
    Smesh * const           smshptr,
    const Gnum             idnum,
    const Gnum             baseval,
    const Gnum             vertnbr,
    Gnum * const           verttax,
    Gnum * const           vendtax,
    const Gnum             edgenbr,
    const Gnum             edgesiz,
    Gnum * const           edgetax,
    Gnum * const           edlotax,
    Gnum                   enttnbr,
    Gnum *                 venttax,
    Gnum *                 esubtax,
    Gnum                   valumax,
    Values *               valsptr,
    Gnum                   degrmax,
    Gnum                   procglbnbr);

int smeshSaveByName (
    Smesh * restrict const     meshptr,
    char * const               nameval,
    Gnum const                 procnbr,
    Gnum * const               procvrttab);

int smeshSave (
    Smesh * restrict const     meshptr,
    FILE * const               stream,
    Gnum const                 procnum,
    Gnum const                 procnbr,
    Gnum * const               procvrttab);

int smeshValueLink (
Smesh * restrict const        smeshptr,
void **                       valutab,
Gnum                         flagval,
Gnum *                        sizeval,
Gnum *					     typesiz,
MPI_Datatype                  typeval,
Gnum                         enttnum,
Gnum                         tagnum);

int smeshValueUnlink (
    Smesh * restrict const        smeshptr,
    Gnum                         enttnum,
    Gnum                         tagnum);

int smeshValueData (
const Smesh * const     smshptr,
Gnum const              enttnum,
Gnum                    tagnum,
Gnum *                        sizeval,
Gnum *					     typesiz,
void **                      valutab);
