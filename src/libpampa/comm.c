/* Copyright 2010-2016 ENSEIRB, Inria & CNRS
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        comm.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the large size
//!                communication handling routines.
//!
//!   \date        Version 1.0: from: 21 Mar 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define COMM

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"

/************************************/
/*                                  */
/* These routines handle large size */
/* communications                   */
/*                                  */
/************************************/

/*
**
*/

//! \brief This routine is equivalent to MPI_Allgatherv, but check if indices
//! out of range
int
commGnumAllgatherv (
void * const                senddattab,
const Gnum                  sendcntnbr,
MPI_Datatype                sendtypval,
void * const                recvdattab,
const Gnum * const          recvcnttab,
const Gnum * const          recvdsptab,
MPI_Datatype                recvtypval,
MPI_Comm                    comm)
{
  int * restrict      ircvcnttab;
  int * restrict      ircvdsptab;
  int                 procglbnbr;
  int                 procnum;
  int                 o;

  MPI_Comm_size (comm, &procglbnbr);
  if (memAllocGroup ((void **) (void *)
                     &ircvcnttab, (size_t) (procglbnbr * sizeof (int)),
                     &ircvdsptab, (size_t) (procglbnbr * sizeof (int)),
                     (void *) NULL) == NULL) {
    errorPrint ("out of memory");
    return     (MPI_ERR_OTHER);
  }

  for (procnum = 0; procnum < procglbnbr; procnum ++) {
    ircvcnttab[procnum] = (int) recvcnttab[procnum];
    ircvdsptab[procnum] = (int) recvdsptab[procnum];
    if (((Gnum) ircvcnttab[procnum] != recvcnttab[procnum]) ||
        ((Gnum) ircvdsptab[procnum] != recvdsptab[procnum])) {
      errorPrint ("communication indices out of range");
      memFree    (ircvcnttab);
      return     (MPI_ERR_ARG);
    }
  }

  o = MPI_Allgatherv (senddattab, sendcntnbr, sendtypval,
                      recvdattab, ircvcnttab, ircvdsptab, recvtypval, comm);

  memFree (ircvcnttab);

  return (o);
}

//! \brief This function is equivalent to MPI_Gatherv, but checks if indices are
//! out of range

int
commGnumGatherv (
void * const                senddattab,
const Gnum                  sendcntnbr,
MPI_Datatype                sendtypval,
void * const                recvdattab,
const Gnum * const          recvcnttab,
const Gnum * const          recvdsptab,
MPI_Datatype                recvtypval,
const int                   rootnum,
MPI_Comm                    comm)
{
  int * restrict      ircvcnttab;
  int * restrict      ircvdsptab;
  int                 proclocnum;
  int                 o;

  MPI_Comm_rank (comm, &proclocnum);

  ircvcnttab = NULL;

  if (rootnum == proclocnum) {
    int                 procglbnbr;
    int                 procnum;

    MPI_Comm_size (comm, &procglbnbr);
    if (memAllocGroup ((void **) (void *)
                       &ircvcnttab, (size_t) (procglbnbr * sizeof (int)),
                       &ircvdsptab, (size_t) (procglbnbr * sizeof (int)),
                       (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      return     (MPI_ERR_OTHER);
    }

    for (procnum = 0; procnum < procglbnbr; procnum ++) {
      ircvcnttab[procnum] = (int) recvcnttab[procnum];
      ircvdsptab[procnum] = (int) recvdsptab[procnum];
      if (((Gnum) ircvcnttab[procnum] != recvcnttab[procnum]) ||
          ((Gnum) ircvdsptab[procnum] != recvdsptab[procnum])) {
        errorPrint ("communication indices out of range");
        memFree    (ircvcnttab);
        return     (MPI_ERR_ARG);
      }
    }
  }

  o = MPI_Gatherv (senddattab, sendcntnbr, sendtypval,
                   recvdattab, ircvcnttab, ircvdsptab, recvtypval, rootnum, comm);

  if (ircvcnttab != NULL)
    memFree (ircvcnttab);

  return (o);
}

//! \brief This routine has the same signature as MPI_Scatterv, and uses
//! point-to-point communication

int
commGnumScatterv2 (
void * const                senddattab,
const Gnum * const          sendcnttab,
const Gnum * const          senddsptab,
MPI_Datatype                sendtypval,
void * const                recvdattab,
const Gnum                  recvcntnbr,
MPI_Datatype                recvtypval,
const int                   rootnum,
MPI_Comm                    comm)
{
  int * restrict      isndcnttab;
  MPI_Request *       rsndloctab;
  MPI_Status  *       ssndloctab;
  MPI_Request         rrcvlocval;
  MPI_Status          srcvlocval;
  int                 requlocnbr;
  int                 proclocnum;
  int                 procglbnbr;
  int                 o;

  o = 0;
  MPI_Comm_rank (comm, &proclocnum);
  MPI_Comm_size (comm, &procglbnbr);

  isndcnttab = NULL;

  if (rootnum == proclocnum) {
    int                 procnum;

    if (memAllocGroup ((void **) (void *)
                       &isndcnttab, (size_t) (procglbnbr * sizeof (int)),
                       &rsndloctab, (size_t) (procglbnbr * sizeof (MPI_Request)),
                       &ssndloctab, (size_t) (procglbnbr * sizeof (MPI_Status)),
                       (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      return     (MPI_ERR_OTHER);
    }

    for (procnum = 0; procnum < procglbnbr; procnum ++) {
      isndcnttab[procnum] = (int) sendcnttab[procnum];
      if ((Gnum) isndcnttab[procnum] != sendcnttab[procnum]) {
        errorPrint   ("communication indices out of range");
        memFreeGroup (isndcnttab);
        return       (MPI_ERR_ARG);
      }
    }
  }

  MPI_Irecv (recvdattab, (int) recvcntnbr, recvtypval, rootnum, TAGSCATTERTAB,
      comm, &rrcvlocval);

  if (rootnum == proclocnum) {
    Gnum procngbnum;
    MPI_Aint            typsiz;                 /* Extent of attribute datatype */
    MPI_Aint            dummy;

    MPI_Type_get_extent (sendtypval, &dummy, &typsiz);     /* Get type extent */
    for (procngbnum = 0; procngbnum < procglbnbr; procngbnum ++) {

      MPI_Isend ((byte *) senddattab + senddsptab[procngbnum] * typsiz,
          isndcnttab[procngbnum], sendtypval, procngbnum, TAGSCATTERTAB, comm,
          &rsndloctab[procngbnum]);
    }

    /* Wait for communication completion */
    o |= MPI_Waitall (procglbnbr, rsndloctab, ssndloctab); 
  }

  o |= MPI_Wait (&rrcvlocval, &srcvlocval);

  if (isndcnttab != NULL)
    memFreeGroup (isndcnttab);

  return (o);
}

//! \brief This routine is equivalent to MPI_Scatterv, but checks if indices are
//! out of range
int
commGnumScatterv3 (
void * const                senddattab,
const Gnum * const          sendcnttab,
const Gnum * const          senddsptab,
MPI_Datatype                sendtypval,
void * const                recvdattab,
const Gnum                  recvcntnbr,
MPI_Datatype                recvtypval,
const int                   rootnum,
MPI_Comm                    comm)
{
  int * restrict      isndcnttab;
  int * restrict      isnddsptab;
  int                 proclocnum;
  int                 o;

  MPI_Comm_rank (comm, &proclocnum);

  isndcnttab = NULL;

  if (rootnum == proclocnum) {
    int                 procglbnbr;
    int                 procnum;

    MPI_Comm_size (comm, &procglbnbr);
    if (memAllocGroup ((void **) (void *)
                       &isndcnttab, (size_t) (procglbnbr * sizeof (int)),
                       &isnddsptab, (size_t) (procglbnbr * sizeof (int)),
                       (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      return     (MPI_ERR_OTHER);
    }

    for (procnum = 0; procnum < procglbnbr; procnum ++) {
      isndcnttab[procnum] = (int) sendcnttab[procnum];
      isnddsptab[procnum] = (int) senddsptab[procnum];
      if (((Gnum) isndcnttab[procnum] != sendcnttab[procnum]) ||
          ((Gnum) isnddsptab[procnum] != senddsptab[procnum])) {
        errorPrint ("communication indices out of range");
        memFreeGroup (isndcnttab);
        return     (MPI_ERR_ARG);
      }
    }
  }

  o = MPI_Scatterv (senddattab, isndcnttab, isnddsptab, sendtypval,
                    recvdattab, (int) recvcntnbr, recvtypval, rootnum, comm);

  if (isndcnttab != NULL)
    memFreeGroup (isndcnttab);

  return (o);
}

//! \brief This routine has the same signature as MPI_Scatterv

int
commGnumScatterv (
void * const                senddattab,
const Gnum * const          sendcnttab,
const Gnum * const          senddsptab,
MPI_Datatype                sendtypval,
void * const                recvdattab,
const Gnum                  recvcntnbr,
MPI_Datatype                recvtypval,
const int                   rootnum,
MPI_Comm                    comm)
{
  return (commGnumScatterv3 (senddattab, sendcnttab, senddsptab, sendtypval,
        recvdattab, recvcntnbr, recvtypval, rootnum, comm));
}

