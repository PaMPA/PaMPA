/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_map.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module is the API for the
//!                distributed mesh mapping routines of the
//!                libpampa library. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include <ptscotch.h>
#include "strat.h"
#include "arch.h"
#include "dmesh_map.h"
#include "pampa.h"

/************************************/
/*                                  */
/* These routines are the C API for */
/* the parallel mapping routines.   */
/*                                  */
/************************************/

//! \brief This routine computes a partition of
//! the given mesh structure with respect
//! to the given strategy.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_dmeshPart (
PAMPA_Dmesh * const        meshptr,              //!< mesh to map
const PAMPA_Num            partnbr,              //!< Number of parts 
PAMPA_Strat * const        stratptr,             //!< Mapping strategy
PAMPA_Num * const          termloctab)           //!< Mapping array   
{
  Dmesh * srcmeshptr;

  srcmeshptr = (Dmesh * const) meshptr;

  if ((srcmeshptr->ovlpglbval & PAMPA_OVLP_NOGT) != 0) {
    errorPrint ("Overlap contains PAMPA_OVLP_NOGT and partitioning could not be computed");
    return (1);
  }

  return dmeshMapElem (srcmeshptr, partnbr, NULL, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
  //return dmeshMapElem2 (srcmeshptr, partnbr, NULL, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
}

int
PAMPA_dmeshPart2 (
PAMPA_Dmesh * const        meshptr,              //!< mesh to map
const char *               methval,              //!< method of partitioning
const PAMPA_Num            partnbr,              //!< Number of parts 
PAMPA_Strat * const        stratptr,             //!< Mapping strategy
PAMPA_Num * const          termloctab)           //!< Mapping array   
{
  Dmesh * srcmeshptr;

  srcmeshptr = (Dmesh * const) meshptr;
  if ((srcmeshptr->ovlpglbval & PAMPA_OVLP_NOGT) != 0) {
    errorPrint ("Overlap contains PAMPA_OVLP_NOGT and partitioning could not be computed");
    return (1);
  }


  if (strcmp (methval, "partElem") == 0)
    return dmeshMapElem (srcmeshptr, partnbr, NULL, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
  else if (strcmp (methval, "partVert") == 0)
    return dmeshMapVert (srcmeshptr, partnbr, NULL, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
  else {
    errorPrint ("Undefined method");
    return (1);
  }
}

//! \brief This routine computes a mapping of the
//! given graph structure onto the given
//! target architecture with respect to the
//! given strategy.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_dmeshMap (
PAMPA_Dmesh * const        meshptr,              //!< mesh to map
PAMPA_Arch * const         archptr,              //!< Target architecture
PAMPA_Strat * const        stratptr,             //!< Mapping strategy
PAMPA_Num * const          termloctab)           //!< Mapping array   
{
  Dmesh * srcmeshptr;

  srcmeshptr = (Dmesh * const) meshptr;

  if ((srcmeshptr->ovlpglbval & PAMPA_OVLP_NOGT) != 0) {
    errorPrint ("Overlap contains PAMPA_OVLP_NOGT and partitioning could not be computed");
    return (1);
  }

  return dmeshMapElem (srcmeshptr, ~0, (Arch * const) archptr, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
}

int
PAMPA_dmeshMap2 (
PAMPA_Dmesh * const        meshptr,              //!< mesh to map
const char *               methval,              //!< method of partitioning
PAMPA_Arch * const         archptr,              //!< Target architecture
PAMPA_Strat * const        stratptr,             //!< Mapping strategy
PAMPA_Num * const          termloctab)           //!< Mapping array   
{
  Dmesh * srcmeshptr;

  srcmeshptr = (Dmesh * const) meshptr;
  if ((srcmeshptr->ovlpglbval & PAMPA_OVLP_NOGT) != 0) {
    errorPrint ("Overlap contains PAMPA_OVLP_NOGT and partitioning could not be computed");
    return (1);
  }


  if (strcmp (methval, "partElem") == 0)
    return dmeshMapElem (srcmeshptr, ~0, (Arch * const) archptr, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
  else if (strcmp (methval, "partVert") == 0)
    return dmeshMapVert (srcmeshptr, ~0, (Arch * const) archptr, (Strat * const) stratptr, termloctab - srcmeshptr->baseval);
  else {
    errorPrint ("Undefined method");
    return (1);
  }
}

