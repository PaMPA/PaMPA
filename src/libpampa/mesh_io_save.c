/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_io_save.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the input/output routines for centralized
//!                meshes.
//!
//!   \date        Version 1.0: from:  6 Feb 2015
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define MESH_IO_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "smesh_mesh.h"
#include "pampa.h"


static inline int valuePrint (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  if (!strcmp (format, "%d\t")) 
    fprintf (stream, format, *((int *) value));
  else if (!strcmp (format, "%.16e\t")) 
    fprintf (stream, "%.16e\t", *((double *) value)); 
    //fprintf (stream, format, *((double *) value));
  else if (!strcmp (format, "%lld\t")) 
    fprintf (stream, format, *((int64_t *) value));
  else {
    errorPrint ("Unrecognized format");
    return (1);
  }

  return (0);
}
    
/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
meshSaveValues (
Mesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream)               /* One single centralized stream or distributed ones */
{
  Values * restrict valsptr;
  Gnum enttnum;
  Gnum enttnnd;
  int chekval;

  chekval = 0;
  valsptr = meshptr->valsptr;

  for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = meshptr->enttnbr + meshptr->baseval; enttnum < enttnnd; enttnum ++) {
    MeshEntity * restrict enttptr;
    Value * restrict valuptr;
    Gnum valunbr;

    if (enttnum >= meshptr->baseval)
      enttptr = meshptr->entttax[enttnum];
    for (valunbr = 0, valuptr = valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next, valunbr ++);
    fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", enttnum, valunbr);

    for (valuptr = valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next) {
      int addnbr, intnbr, typenbr, combdat;
      MPI_Aint            valusiz;                 /* Extent of attribute datatype */
      MPI_Aint            dummy;
      Gnum countnbr;
      Gnum countnum;
      Gnum proclocnum;
      Gnum vertnum;
      Gnum vertnnd;
      size_t elemsiz;
      char format[9];
      int intdat;
      byte * valutab;
      MPI_Datatype typedat;

      valutab = (byte *) valuptr->valutab;
      MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */
      MPI_Type_get_envelope( valuptr->typeval, &intnbr, &addnbr, &typenbr, &combdat);

      fprintf (stream, GNUMSTRING "\t", valuptr->tagnum);

      switch (combdat) {
        case MPI_COMBINER_NAMED:
          countnbr = 1;
          fprintf (stream, "1\t");
          if (valuptr->typeval == MPI_INT) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 1);
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (valuptr->typeval == MPI_DOUBLE) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 2);
            strcpy (format, "%.16e\t");
            elemsiz = sizeof (double);
          }
          else if (valuptr->typeval == MPI_LONG_LONG) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 3);
            strcpy (format, "%lld\t");
            elemsiz = sizeof (long long int);
          }
          else {
            errorPrint ("Type not implemented");
            chekval = 1;
          }
          break;
        case MPI_COMBINER_CONTIGUOUS:
          fprintf (stream, "2\t");
          MPI_Type_get_contents( valuptr->typeval, 1, 0, 1, &intdat, NULL, &typedat); 
          countnbr = (Gnum) intdat;
          if (typedat == MPI_INT) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 1);
            strcpy (format, "%d\t");
            elemsiz = sizeof (int);
          }
          else if (typedat == MPI_DOUBLE) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 2);
            strcpy (format, "%.16e\t");
            elemsiz = sizeof (double);
          }
          else if (valuptr->typeval == MPI_LONG_LONG) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 3);
            strcpy (format, "%lld\t");
            elemsiz = sizeof (long long int);
          }
          else {
            errorPrint ("Type not implemented");
            chekval = 1;
          }
          break;
        default:
          errorPrint ("case not implemented");
          chekval = 1;
      }
      CHECK_VDBG2 (chekval);

      switch (enttnum) {
        case PAMPA_ENTT_VIRT_PROC:
          for (proclocnum = 0; proclocnum < meshptr->procglbnbr; proclocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valutab + proclocnum * valusiz + countnum * elemsiz);
          fprintf (stream, "\n");
          break;
        case PAMPA_ENTT_VIRT_VERT:
          for (vertnum = 0; vertnum < meshptr->vertnbr; vertnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valutab + vertnum * valusiz + countnum * elemsiz);
          fprintf (stream, "\n");
          break;
        default: /* real entity */

          for (vertnum = meshptr->baseval, vertnnd = enttptr->vertnbr + meshptr->baseval; vertnum < vertnnd; vertnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valutab + vertnum * valusiz + countnum * elemsiz);
          fprintf (stream, "\n");
      }
    }
  }
  CHECK_VDBG2 (chekval);
  return (0);
}

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
meshSave (
Mesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream)               /* One single centralized stream or distributed ones */
{
  Values * restrict valsptr;
  Gnum vertnum;
  Gnum vertnnd;
  Gnum edgenum;
  Gnum edgennd;
  Gnum enttnum;
  Gnum enttnnd;
  Smesh smshdat;
  int chekval;

  chekval = 0;
  valsptr = meshptr->valsptr;
  smeshInit(&smshdat);
  mesh2smesh(meshptr, &smshdat);

  // XXX et proccomm ? que fait-on ?
  chekval |= (fprintf(stream,
      "%d\t%d\t%d\n" /* version, parallel, Gnum size  */
      "%d\t%d\n" /* proclocnum, procglbnbr */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n"  /* enttnbr, vertnbr, edgenbr */
      GNUMSTRING "\t" GNUMSTRING "\n"  /* valunbr, valumax */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n" GNUMSTRING "\n", /* baseval, esubtab, vlbltax, edlotax    */
      0, /* Version */
      2, /* Centralized */
      1, /* Gnumval XXX à changer */
      smshdat.proclocnum, smshdat.procglbnbr,
      smshdat.enttnbr,
      smshdat.vertnbr, smshdat.edgenbr,
      valsptr->valunbr, valsptr->valumax,
      smshdat.baseval, (Gnum) (smshdat.esubtax == NULL ? 0 : 1),
      (Gnum) 0, /* vlbltax not yet implemented */
      (Gnum) (smshdat.edlotax == NULL ? 0 : 1)) == EOF);
  CHECK_VDBG2 (chekval);

  if (smshdat.esubtax != NULL) {
    for (enttnum = smshdat.baseval, enttnnd = smshdat.baseval + smshdat.enttnbr; enttnum < enttnnd; enttnum ++)
      chekval |= (fprintf (stream, GNUMSTRING "\t", smshdat.esubtax[enttnum]) == EOF);
    chekval |= (fprintf (stream, "\n") == EOF);
  }

  for (vertnum = smshdat.baseval, vertnnd = smshdat.baseval + smshdat.vertnbr; vertnum < vertnnd; vertnum ++) {
    fprintf (stream, GNUMSTRING "\t" GNUMSTRING, smshdat.venttax[vertnum], smshdat.vendtax[vertnum] - smshdat.verttax[vertnum]);
    for (edgenum = smshdat.verttax[vertnum], edgennd = smshdat.vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
      if (smshdat.edlotax != NULL)
        fprintf (stream, "\t" GNUMSTRING, smshdat.edlotax[edgenum]);
      fprintf (stream, "\t" GNUMSTRING, smshdat.edgetax[edgenum]);
    }
    fprintf (stream, "\n");
  }
  CHECK_VDBG2 (chekval);
  smeshExit (&smshdat);
  return (meshSaveValues (meshptr, stream));
}
    

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
meshSaveAll (
Mesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream)               /* One single centralized stream or distributed ones */
{
  Gnum ventnum;
  Gnum ventnnd;
  Gnum edgenum;
  Gnum enttnum;
  Gnum enttnnd;
  int chekval;

  chekval = 0;

  // XXX et proccomm ? que fait-on ?
  chekval |= (fprintf(stream,
      "%d\t%d\t%d\n" /* version, parallel, Gnum size  */
      "%d\t" /* meshptr->flagval    */
      GNUMSTRING "\t" /* meshptr->baseval    */
      GNUMSTRING "\t" /* meshptr->vertnbr */
      GNUMSTRING "\t" /* meshptr->enttnbr */
      GNUMSTRING "\t" /* meshptr->esubmsk */
      GNUMSTRING "\t" /* meshptr->degrmax */
      GNUMSTRING "\t" /* meshptr->edgenbr */
      GNUMSTRING "\t" /* meshptr->edgesiz */
      "%d\t"          /* meshptr->procglbnbr */
      "%d\t"          /* meshptr->proclocnum */
      GNUMSTRING "\t", /* valumax          */
      0, /* Version */
      3, /* All centralized XXX à changer */
      1, /* Gnumval XXX à changer */
      meshptr->flagval,   
      meshptr->baseval,   
      meshptr->vertnbr,
      meshptr->enttnbr,
      meshptr->esubmsk,
      meshptr->degrmax,
      meshptr->edgenbr,
      meshptr->edgesiz,
      meshptr->procglbnbr,
      meshptr->proclocnum,
      meshptr->valsptr->valumax) == EOF);
  if (meshptr->esubmsk != 0) {
    for (enttnum = meshptr->baseval, enttnnd = meshptr->baseval + meshptr->enttnbr; enttnum < enttnnd; enttnum ++)
      chekval |= (fprintf (stream, GNUMSTRING "\t", meshptr->esubbax[enttnum]) == EOF);
    chekval |= (fprintf (stream, "\n") == EOF);
  }

  chekval |= (fprintf (stream, "%d\n", (meshptr->edlotax == NULL ? 0 : 1)) == EOF);
  if (meshptr->edlotax != NULL) {
    Gnum edlonum;
    for (edlonum = meshptr->baseval; edlonum < meshptr->edgesiz; edlonum ++)
      chekval |= (fprintf (stream, GNUMSTRING "\t", meshptr->edlotax[edlonum]) == EOF);
    chekval |= (fprintf (stream, "\n") == EOF);
  }

  for (edgenum = meshptr->baseval; edgenum < meshptr->edgesiz; edgenum ++)
    chekval |= (fprintf (stream, GNUMSTRING "\t", meshptr->edgetax[edgenum]) == EOF);
  chekval |= (fprintf (stream, "\n") == EOF);

  for (ventnum = meshptr->baseval, ventnnd = meshptr->enttnbr + meshptr->baseval; ventnum < ventnnd; ventnum ++) {
    MeshEntity * restrict ventptr;
    Gnum vertnum;
    Gnum vertnnd;

    chekval |= (fprintf (stream, "%d\n", (meshptr->entttax[ventnum] == NULL ? 0 : 1)) == EOF);

    if (meshptr->entttax[ventnum] != NULL) {
      ventptr = meshptr->entttax[ventnum];

      chekval |= (fprintf (stream, GNUMSTRING "\n", ventptr->vertnbr) == EOF);

      if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { /* If ventnum is not a sub-entity */
        for (vertnum = meshptr->baseval, vertnnd = ventptr->vertnbr; vertnum < vertnnd; vertnum ++)
          chekval |= (fprintf (stream, GNUMSTRING "\t", ventptr->mvrttax[vertnum]) == EOF);
        chekval |= (fprintf (stream, "\n") == EOF);
      }

      if (meshptr->esubbax[ventnum & meshptr->esubmsk] != PAMPA_ENTT_SINGLE) { /* If ventnum is not a single entity */
        for (vertnum = meshptr->baseval, vertnnd = ventptr->vertnbr; vertnum < vertnnd; vertnum ++)
          chekval |= (fprintf (stream, GNUMSTRING "\t", ventptr->svrttax[vertnum]) == EOF);
        chekval |= (fprintf (stream, "\n") == EOF);
        for (vertnum = meshptr->baseval, vertnnd = ventptr->vertnbr; vertnum < vertnnd; vertnum ++)
          chekval |= (fprintf (stream, GNUMSTRING "\t", ventptr->venttax[vertnum]) == EOF);
        chekval |= (fprintf (stream, "\n") == EOF);
      }

      chekval |= (fprintf (stream, "%d\n", (ventptr->vprmtax == NULL ? 0 : 1)) == EOF);
      if (ventptr->vprmtax != NULL) {
        for (vertnum = meshptr->baseval; vertnum < ventptr->vertnbr; vertnum ++)
          chekval |= (fprintf (stream, GNUMSTRING "\t", ventptr->vprmtax[vertnum]) == EOF);
        chekval |= (fprintf (stream, "\n") == EOF);
      }

    }
  }

  for (ventnum = meshptr->baseval, ventnnd = meshptr->enttnbr + meshptr->baseval; ventnum < ventnnd; ventnum ++) {
    MeshEntity * restrict ventptr;
    Gnum vertnum;
    Gnum nentnum;
    Gnum nentnnd;

    if (meshptr->entttax[ventnum] != NULL) {
      ventptr = meshptr->entttax[ventnum];


      if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) /* If ventnum is not a sub-entity */
        for (nentnum = meshptr->baseval, nentnnd = meshptr->enttnbr + meshptr->baseval; nentnum < nentnnd; nentnum ++) {
          if ((meshptr->entttax[nentnum] != NULL) || (nentnum == meshptr->baseval) || (nentnum == nentnnd - 1)) {
            MeshEnttNghb * restrict nghbptr;

            chekval |= (fprintf (stream, "%d\n", (ventptr->nghbtax[nentnum] == NULL ? 0 : 1)) == EOF);

            nghbptr = ventptr->nghbtax[nentnum];

            if (meshptr->esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { /* If nentnum is not a sub-entity */
              for (vertnum = meshptr->baseval; vertnum < ventptr->vertnbr; vertnum ++)
                chekval |= (fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\t", 
                      nghbptr->vindtax[vertnum].vertidx,
                      nghbptr->vindtax[vertnum].vendidx) == EOF);
              chekval |= (fprintf (stream, "\n") == EOF);
            }
          }
        }

    }
  }

  CHECK_VDBG2 (chekval);
  return (meshSaveValues (meshptr, stream));
}
