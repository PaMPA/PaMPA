/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_ghst.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the halo building
//!                routine. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define DMESH_GHST

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_allreduce.h"
#include "dmesh_ghst.h"
#include "dmesh_halo.h"
#include "pampa.h"

#define C_FLAGDEBUG                 0x0002        /* Enable easy debugger attachment */



//! \brief Define a reduce function with 2 max and 1 sum

DMESHALLREDUCEMAXSUMOP (2, 1)

//! \brief This routine builds the ghost structures
//! required by the halo routines.
//! 
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dmeshGhst (
    Dmesh * restrict const  meshptr,              //!< Mesh structure
    Gnum                    vertlocnbr,           //!< Number of local vertices
    Gnum                    vgstlocnbr,           //!< Number of local + ghost vertices
    Gnum *                  vertloctax,           //!< Array of vertex beginning index
    Gnum *                  vendloctax,           //!< Array of vertex end index
    Gnum *                  edgeloctax,           //!< Array of edges
    Gnum *                  ventgsttax,           //!< Array of entities
    Gnum *                  vertgsttax)           //!< Ghost array of vertex number
{
  DmeshEntity **            enttloctax;
  Gnum                      enttnum;
  int                       procnum;
  Gnum                      baseval;
  Gnum                      vertnum;
  int * restrict            procsidtab;           /* Send index array                              */
  int *                     procsidnbr;           /* Number of entries in send index array         */
  Gnum *                    vertnumtax;           /* Last vertex index in send index array         */
  Gnum                      vertlocmin;           /* Smallest index of local vertices              */
  Gnum *                    vertnbrtax;
  Gnum *                    eidxgsttax;
  Gnum *                    vertsidtab;
  Gnum *                    edgeloctmx;
  Gnum                      vertlocmax;           /* Largest index of local vertices, + 1          */
  Gnum                      vertlocnum;           /* Current vertex number (based)                 */
  Gnum                      vertlocnnd;           /* Current vertex number (based)                 */
  Gnum                      enttnnd;

  // flagval2 = C_FLAGDEBUG; // \todo what is it for?
  baseval = meshptr->baseval;

  if ((meshptr->flagval & DMESHFREEPSID) != 0) /* If ghost private data already computed, do nothing */
    return (0);


  if (memAllocGroup ((void **) (void *)
        &edgeloctmx, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
        &vertsidtab, (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
        &vertnumtax, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
        &eidxgsttax, (size_t) (vgstlocnbr * sizeof (Gnum)),
        &vertnbrtax, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  eidxgsttax -= baseval;
  vertnbrtax -= baseval;
  edgeloctmx -= baseval;
  vertnumtax -= baseval;


  /* Initializing vertnbrtax */
  //! \todo maybe vertnbrtax is not well known, prefer vnbr{loc,gst}tax
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    vertnbrtax[enttnum] = baseval;
  }

  /* Compute local numbering by entity for each local and ghost vertex */
  for (vertlocnum = baseval, vertlocnnd = vgstlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    if (ventgsttax[vertlocnum] != PAMPA_ENTT_DUMMY)
      eidxgsttax[vertlocnum] = vertnbrtax[ventgsttax[vertlocnum]]++;
  }


  memSet (edgeloctmx + baseval, 0, meshptr->enttglbnbr * sizeof (Gnum)); /* Array which contains edge numbers */
  /* Compute number of neighbors for each local vertex */
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval ;
      vertlocnum < vertlocnnd ; vertlocnum ++) {

    if (ventgsttax[vertlocnum] != PAMPA_ENTT_DUMMY)
      edgeloctmx[ventgsttax[vertlocnum]] += vendloctax[vertlocnum] - vertloctax[vertlocnum];

  }

  enttloctax = meshptr->enttloctax;

  /* Initialize communicator data for each entity */
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    if (enttloctax[enttnum] == NULL)
      continue;

    if ((meshptr->esublocbax[enttnum & meshptr->esublocmsk] >= PAMPA_ENTT_SINGLE) &&
        ((enttloctax[enttnum]->commlocdat.procsidtab = (int *) memAlloc ((enttloctax[enttnum]->vertlocnbr + edgeloctmx[enttnum]) * sizeof (int))) == NULL)) { /* TRICK: procsidtab for entities, which have sub-entities, will be allocated later */
      errorPrint  ("out of memory");
      return      (1);
    }

    enttloctax[enttnum]->commlocdat.procsndnbr = 0;
    enttloctax[enttnum]->commlocdat.procngbnbr = 0;
    enttloctax[enttnum]->commlocdat.procsidnbr = 0;

    memSet (enttloctax[enttnum]->commlocdat.procrcvtab, 0, meshptr->procglbnbr * sizeof (int));
    memSet (enttloctax[enttnum]->commlocdat.procsndtab, 0, meshptr->procglbnbr * sizeof (int));

  }

  /* Determine minimum and maximum global numbering on processor */
  vertlocmin = meshptr->procvrttab[meshptr->proclocnum];
  vertlocmax = meshptr->procvrttab[meshptr->proclocnum + 1];
  memSet (vertsidtab,         ~0, meshptr->procglbnbr * sizeof (Gnum));

  /* Initialize vertnumtax for each entity */
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    vertnumtax[enttnum] = baseval;
  }

  /* Compute vertices to send to processor neighbors */
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval ;
      vertlocnum < vertlocnnd ; vertlocnum ++) {
    Gnum   edgelocnum;

    for (edgelocnum = vertloctax[vertlocnum];
        edgelocnum < vendloctax[vertlocnum]; edgelocnum ++) {
      Gnum   vertlocend;

      vertlocend = edgeloctax[edgelocnum];
#ifdef DMESH_DEBUG_DMESH2
      if ((vertlocend < meshptr->baseval) || (vertlocend >= (meshptr->procvrttab[meshptr->procglbnbr]))) {
        errorPrint  ("invalid edge array");
        if (commAllreduce (reduloctab, reduglbtab, 2, MPI_INT, MPI_MAX, meshptr->proccomm) != MPI_SUCCESS)
          errorPrint ("communication error");
        memFree (ventgsttax);                     /* Free group leader */
        for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
          if (enttloctax[enttnum] == NULL)
            continue;

          memFree (enttloctax[enttnum]->commlocdat.procsidtab);
        }
        return  (1);
      }
#endif /* DMESH_DEBUG_DMESH2 */

      if (!((vertlocend >= vertlocmin) && (vertlocend < vertlocmax))) { /* If edge is not local */
        int                 procngbnum;
        int                 procngbmax;

        for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
            procngbmax - procngbnum > 1; ) {
          int                 procngbmed;

          procngbmed = (procngbmax + procngbnum) / 2;
          if (meshptr->procvrttab[procngbmed] <= vertlocend)
            procngbnum = procngbmed;
          else
            procngbmax = procngbmed;
        }

        if (vertsidtab[procngbnum] != vertlocnum) { /* If vertex not already sent to process  */
          vertnum = eidxgsttax[vertlocnum];
          enttnum = ventgsttax[vertlocnum];
          procsidtab = enttloctax[enttnum]->commlocdat.procsidtab;
          procsidnbr = &enttloctax[enttnum]->commlocdat.procsidnbr;


          vertsidtab[procngbnum] = vertlocnum;    /* Neighbor process will receive vertex     */
          enttloctax[enttnum]->commlocdat.procsndtab[procngbnum] ++;     /* One more vertex to send to this neighbor */

          while ((vertnum - vertnumtax[enttnum]) >= DMESHGHSTSIDMAX) { /* If Gnum range too long for int */
            procsidtab[(*procsidnbr) ++] = -DMESHGHSTSIDMAX; /* Decrease by maximum int distance      */
            vertnumtax[enttnum]               += DMESHGHSTSIDMAX;
          }
          if (vertnumtax[enttnum] != vertnum) {         /* If communication concerns new local vertex   */
            procsidtab[(*procsidnbr) ++] = - (vertnum - vertnumtax[enttnum]); /* Encode jump in procsidtab */
            vertnumtax[enttnum] = vertnum;              /* Current local vertex is last send vertex     */
          }
          procsidtab[(*procsidnbr) ++] = procngbnum; /* Send this vertex data to this processor */
        }
      }
    }
  }


  /* Update procrcvtab on each entity */
  for (vertlocnum = vertlocnbr + baseval, vertlocnnd = vgstlocnbr + baseval ;
      vertlocnum < vertlocnnd ; vertlocnum ++) {
    int procngbnum = 0;
    int procngbmax;

    if (!((meshptr->procvrttab[procngbnum] <= vertgsttax[vertlocnum]) && (meshptr->procvrttab[procngbnum+1] > vertgsttax[vertlocnum])))
      for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
          procngbmax - procngbnum > 1; ) {
        int                 procngbmed;

        procngbmed = (procngbmax + procngbnum) / 2;
        if (meshptr->procvrttab[procngbmed] <= vertgsttax[vertlocnum])
          procngbnum = procngbmed;
        else
          procngbmax = procngbmed;
      }

    enttloctax[ventgsttax[vertlocnum]]->commlocdat.procrcvtab[procngbnum]++;
  }

  /* Update procngbtab on entities */
  CHECK_FERR (dmeshGhst3(meshptr, vertnumtax), meshptr->proccomm);



  meshptr->flagval   |= DMESHFREEPSID; /* Mesh now have valid ghost private data */

#ifdef PAMPA_DEBUG_DMESH2
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    if (enttloctax[enttnum] == NULL)
      continue;

    if (dmeshHaloCheck (meshptr, &meshptr->enttloctax[enttnum]->commlocdat) != 0) {
      errorPrint ("internal error");
      return     (1);
    }
  }
#endif /* PAMPA_DEBUG_DMESH2 */

  memFreeGroup (edgeloctmx + baseval);

  return (0);
}



//! \brief This routine builds the ghost structures,
//! for entities which have sub-entities,
//! required by the halo routines.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//! \pre dmeshGhst or dmeshOverlap must be called before:
//! procsidnbr, procsnd{tab,nbr) and procrcvtab must be initialized

  int
dmeshGhst2 (
    Dmesh * restrict const     meshptr)              //!< mesh structure
{
  Gnum                      enttlocidx;
  Gnum                      enttlocnum;
  Gnum                      enttlocnnd;
  Gnum                      esublocnum;
  Gnum                      esublocnnd;
  Gnum                      esublocmax;
  Gnum                      esublocnbr; // number of entities which have sub-entities
  Gnum *                    snbrloctax; // number of sub-entities for each entity
  Gnum                      baseval;
  DmeshGhstEntt *           enttesubloctab; // array which contains informations of entities with sub-entities
  Gnum                      enttesublocnbr;
  Gnum                      reduloctab[3];        /* Gnum to perform a maxsum operator             */
  Gnum                      reduglbtab[3];

  baseval = meshptr->baseval;

  if ((snbrloctax = (Gnum *) memAlloc (meshptr->enttglbnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  snbrloctax -= baseval;

  memSet (snbrloctax + baseval, 0, meshptr->enttglbnbr * sizeof (Gnum));


  /* Computing the number of sub-entities for each entity */
  for (enttlocnum = baseval, enttlocnnd = meshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
    if (meshptr->esublocbax[enttlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) /* If sub-entity */
      snbrloctax[meshptr->esublocbax[enttlocnum]] ++;
  }

  /* Determining the maximum number of sub-entities for any entity and the number
   * of entities which have sub-entities */
  for (esublocmax = esublocnbr = 0, enttlocnum = baseval, enttlocnnd = meshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
    if (snbrloctax[enttlocnum] > 0) {
      esublocnbr ++;
      if (snbrloctax[enttlocnum] > esublocmax)
        esublocmax = snbrloctax[enttlocnum];
    }
  }

  /* Allocating memory for each entity which contains sub-entities */
  if ((enttesubloctab = (DmeshGhstEntt *) memAlloc (esublocnbr * sizeof (DmeshGhstEntt))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }

  for (esublocnum = 0 ; esublocnum < esublocnbr ; esublocnum ++) {
    if (memAllocGroup ((void **) (void *)
          &enttesubloctab[esublocnum].esubloctab, (size_t) (esublocmax * sizeof (Gnum)),
          &enttesubloctab[esublocnum].vertnumtab, (size_t) (esublocmax * sizeof (Gnum)),
          &enttesubloctab[esublocnum].procsidtab, (size_t) (esublocmax * sizeof (Gnum)),
          (void *) NULL) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }
    enttesubloctab[esublocnum].esublocnbr = 0;
    enttesubloctab[esublocnum].vertlocnum = baseval;
    enttesubloctab[esublocnum].enttlocnum = -1;

    memSet (enttesubloctab[esublocnum].procsidtab, 0, esublocmax * sizeof (Gnum));

  }

  memSet (snbrloctax + baseval, -1, meshptr->enttglbnbr * sizeof (Gnum)); /* snbrloctax will be used to store index of entity in enttesubloctax */

  for (enttesublocnbr = 0, esublocnum = baseval, esublocnnd = meshptr->enttglbnbr + baseval ; esublocnum < esublocnnd ; esublocnum ++) {
    Gnum enttesublocnum;

    if (meshptr->enttloctax[esublocnum] == NULL) /* If empty entity */
      continue;


    if (meshptr->esublocbax[esublocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) { /* If sub-entity */
      DmeshEntity * esublocptr;

      esublocptr = meshptr->enttloctax[esublocnum];

      enttlocnum = meshptr->esublocbax[esublocnum];

      if (snbrloctax[enttlocnum] == -1) { /* Not yet added */
        enttesublocnum = enttesublocnbr ++;
        snbrloctax[enttlocnum] = enttesublocnum;
        enttesubloctab[enttesublocnum].enttlocnum = enttlocnum;
      }
      else /* already added */
        enttesublocnum = snbrloctax[enttlocnum];


      if (esublocptr->commlocdat.procsidnbr != 0) { /* If communication are necessary for this sub-entity */
        int procsidnum;
        int procsidnnd;

        enttesubloctab[enttesublocnum].vertnumtab[enttesubloctab[enttesublocnum].esublocnbr] = baseval;
        for (procsidnum = 0, procsidnnd = esublocptr->commlocdat.procsidnbr ;
            procsidnum < procsidnnd && esublocptr->commlocdat.procsidtab[procsidnum] < 0 ; procsidnum ++) { /* For offset of vertex */
          enttesubloctab[enttesublocnum].vertnumtab[enttesubloctab[enttesublocnum].esublocnbr] -= esublocptr->commlocdat.procsidtab[procsidnum];
          enttesubloctab[enttesublocnum].procsidtab[enttesubloctab[enttesublocnum].esublocnbr] ++;
        }

      }

      enttesubloctab[enttesublocnum].esubloctab[enttesubloctab[enttesublocnum].esublocnbr ++] = esublocnum;

    }
  }
  memFree (snbrloctax + baseval);

  /* For each entity with sub-entities */
  for (enttlocidx = 0 ; enttlocidx < esublocnbr ; enttlocidx ++) {
    Gnum vertlocmax;
    Gnum vnxtlocnum;
    Gnum procsidnbr;
    Gnum esublocidx;
    Gnum esublocnnd;
      int  procnum;

    DmeshGhstEntt * restrict enttesublocptr;
    DmeshEntity * restrict const * enttloctax = meshptr->enttloctax;
    DmeshEntity * restrict enttlocptr;

    enttesublocptr = &enttesubloctab[enttlocidx];
    enttlocnum = enttesublocptr->enttlocnum;

    if (enttlocnum == -1) /* If entity has no comm */
      continue;

    enttlocptr = meshptr->enttloctax[enttlocnum];

    /* for each sub-entity belonging to entity */
    for (esublocidx = 0, esublocnnd = enttesubloctab[enttlocidx].esublocnbr ; esublocidx < esublocnnd ; esublocidx ++) {
      Gnum esublocnum;

      esublocnum = enttesublocptr->esubloctab[esublocidx];

      for (procnum = 0 ; procnum < meshptr->procglbnbr ; procnum ++) {
        enttlocptr->commlocdat.procrcvtab[procnum] += enttloctax[esublocnum]->commlocdat.procrcvtab[procnum];
        enttlocptr->commlocdat.procsndtab[procnum] += enttloctax[esublocnum]->commlocdat.procsndtab[procnum];
        if ((enttlocptr->commlocdat.procsndtab[procnum] > 0) || (enttlocptr->commlocdat.procrcvtab[procnum] > 0))
          enttlocptr->commlocdat.procngbnbr ++;
      }

      if (enttloctax[esublocnum]->commlocdat.procsidnbr == 0) { /* If no communication */
        enttesublocptr->esubloctab[esublocidx] = -1; 
        continue;
      }

      if (enttesublocptr->procsidtab[esublocidx] == 0) /* If first vertex has to send values */
        enttlocptr->commlocdat.procsidnbr ++;

      enttlocptr->commlocdat.procsidnbr += enttloctax[esublocnum]->commlocdat.procsidnbr;
      enttlocptr->commlocdat.procsndnbr += enttloctax[esublocnum]->commlocdat.procsndnbr;
    }

    if ((enttlocptr->commlocdat.procngbtab = (int *) memAlloc (enttlocptr->commlocdat.procngbnbr * sizeof (int))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }

    for (enttlocptr->commlocdat.procngbnbr = procnum = 0; procnum < meshptr->procglbnbr; procnum ++) 
      if ((enttlocptr->commlocdat.procsndtab[procnum] > 0) || (enttlocptr->commlocdat.procrcvtab[procnum] > 0))
        enttlocptr->commlocdat.procngbtab[enttlocptr->commlocdat.procngbnbr ++] = procnum;



    if ((enttlocptr->commlocdat.procsidtab = (int *) memAlloc (enttlocptr->commlocdat.procsidnbr *sizeof (int))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }

    procsidnbr = 0;
    vertlocmax = enttlocptr->vertlocnbr + baseval;

    /* Update procsidtab on sub-entities */
    while (1) {
      Gnum esublocid2;
      int procsidnum;
      int procsidnnd;
      int * restrict procsidtab;


      vnxtlocnum = vertlocmax;

      for (esublocidx = 0, esublocnnd = enttesubloctab[enttlocidx].esublocnbr ; esublocidx < esublocnnd ; esublocidx ++) {
        Gnum esublocnum;

        esublocnum = enttesublocptr->esubloctab[esublocidx];

        if ((esublocnum != -1) && (enttloctax[esublocnum]->svrtloctax[enttesublocptr->vertnumtab[esublocidx]] < vnxtlocnum)) {
          esublocid2 = esublocidx;
          vnxtlocnum = enttloctax[esublocnum]->svrtloctax[enttesublocptr->vertnumtab[esublocidx]];
        }
      }

      if (vnxtlocnum == vertlocmax) /* If no more sub-entity has communication */
        break;

      esublocnum = enttesublocptr->esubloctab[esublocid2];

      while ((vnxtlocnum - enttesublocptr->vertlocnum) >= DMESHGHSTSIDMAX) { /* If Gnum range too long for int */
        enttlocptr->commlocdat.procsidtab[procsidnbr ++] = -DMESHGHSTSIDMAX; /* Decrease by maximum int distance      */
        enttesublocptr->vertlocnum               += DMESHGHSTSIDMAX;
      }

      if (vnxtlocnum != enttesublocptr->vertlocnum) {
        enttlocptr->commlocdat.procsidtab[procsidnbr ++] = -(vnxtlocnum - enttesublocptr->vertlocnum);
        enttesublocptr->vertlocnum = vnxtlocnum;
      }

      procsidtab = enttloctax[esublocnum]->commlocdat.procsidtab;

      for (procsidnum = enttesublocptr->procsidtab[esublocid2], procsidnnd = enttloctax[esublocnum]->commlocdat.procsidnbr ;
          procsidnum < procsidnnd && procsidtab[procsidnum] >= 0 ; procsidnum ++)
        enttlocptr->commlocdat.procsidtab[procsidnbr ++] = procsidtab[procsidnum];

      if (procsidnum == procsidnnd) /* If no more communication for this sub-entity */
        enttesublocptr->esubloctab[esublocid2] = -1;
      else 
        for ( ; /* procsidnum and procsidnnd with current values (refers to previous loop) */
            procsidnum < procsidnnd && procsidtab[procsidnum] < 0 ; procsidnum ++)
          enttesublocptr->vertnumtab[esublocid2] -= procsidtab[procsidnum];

      enttesublocptr->procsidtab[esublocid2] = procsidnum;
    }

    if (procsidnbr + DMESHGHSTSIDEPSILON < enttlocptr->commlocdat.procsidnbr) { /* If size of procsidtab is too big, give or take epsilon */
      if ((enttlocptr->commlocdat.procsidtab = memRealloc(enttlocptr->commlocdat.procsidtab,
              enttlocptr->commlocdat.procsidnbr * sizeof(int))) == NULL) {
        errorPrint ("out of memory");
        return     (1);
      }
    }
    enttlocptr->commlocdat.procsidnbr = procsidnbr;
    }
    for (esublocnum = 0 ; esublocnum < esublocnbr ; esublocnum ++)
      memFreeGroup (enttesubloctab[esublocnum].esubloctab);
    memFree (enttesubloctab);

  reduloctab[0] = 0;                              /* No memory error                 */
  reduloctab[1] =                                 /* Set maximum number of neighbors */
  reduloctab[2] = meshptr->procngbnbr;
  if (dmeshAllreduceMaxSum (reduloctab, reduglbtab, 2, 1, meshptr->proccomm) != 0) {
    errorPrint ("communication error");
    return     (1);
  }
  if (reduglbtab[0] != 0)                         /* If error, propagated by some previous reduction operator */
    return (1);

  meshptr->procngbmax = reduglbtab[1];
#ifndef PAMPA_COMM_COLL
#ifndef PAMPA_COMM_PTOP
  if (((float) reduglbtab[2]) <= ((float) meshptr->procglbnbr * (float) (meshptr->procglbnbr - 1) * (float) PAMPA_COMM_PTOP_RAT))
#endif /* PAMPA_COMM_PTOP */
    meshptr->flagval |= DMESHCOMMPTOP;           /* If too few communications, use point-to-point instead */
#endif /* PAMPA_COMM_COLL */

#ifdef PAMPA_INFO_ADAPT // \todo change flag
  if (meshptr->flagval & DMESHCOMMPTOP)
    infoPrint ("comm ptop");
  else
    infoPrint ("comm coll");
#endif /* PAMPA_INFO_ADAPT */
#ifdef PAMPA_DEBUG_DMESH2
  //! \todo program don't stop if followed part is uncommented
//  for (enttlocnum = baseval, enttlocnnd = meshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
//    if (meshptr->enttloctax[enttlocnum] == NULL)
//      continue;
//
//    if (dmeshHaloCheck (meshptr, &meshptr->enttloctax[enttlocnum]->commlocdat) != 0) {
//      errorPrint ("internal error");
//      return     (1);
//    }
//  }
#endif /* PAMPA_DEBUG_DMESH2 */

    return (0);
  }


//! \brief This function computes procngbtab by entity and update procsidtab on
//! sub-entities.
//! \remark Function used by dmeshGhst and dmeshOverlap
//! 
//! \returns 0   : on success.
//! \returns !0  : on error.
int dmeshGhst3 (
    Dmesh * restrict const  meshptr,              //!< mesh structure
    Gnum * restrict         vertnumtax)           //!< dummy used array
{
  DmeshEntity * restrict * restrict enttloctax;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum baseval;
  int  procnum;

  baseval = meshptr->baseval;
  enttloctax = meshptr->enttloctax;
  meshptr->procngbnbr = 0;
  for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    DmeshEnttComm * commlocptr;

    if ((enttloctax[enttnum] == NULL) || (meshptr->esublocbax[enttnum & meshptr->esublocmsk] < PAMPA_ENTT_SINGLE)) /* If no vertex or super-entity */
      continue;

    commlocptr = &enttloctax[enttnum]->commlocdat;

    for (commlocptr->procngbnbr = procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
      if ((commlocptr->procsndtab[procnum] > 0) || (commlocptr->procrcvtab[procnum] > 0))
        commlocptr->procngbnbr ++;
      if (commlocptr->procsndtab[procnum] > 0) 
        commlocptr->procsndnbr += commlocptr->procsndtab[procnum];
    }
      

    if ((commlocptr->procngbtab = (int *) memAlloc (commlocptr->procngbnbr * sizeof (int))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }

    for (commlocptr->procngbnbr = procnum = 0; procnum < meshptr->procglbnbr; procnum ++) 
      if ((commlocptr->procsndtab[procnum] > 0) || (commlocptr->procrcvtab[procnum] > 0))
        commlocptr->procngbtab[commlocptr->procngbnbr ++] = procnum;

    if ((meshptr->esublocbax[enttnum & meshptr->esublocmsk] >= PAMPA_ENTT_SINGLE) &&
        ((commlocptr->procsidtab = memRealloc(commlocptr->procsidtab,
                                              commlocptr->procsidnbr * sizeof(int))) == NULL)) {
      errorPrint ("out of memory");
      return     (1);
    }
  }

  /* Reuse of vertnumtax to store index in procngbtab for each entity */
  for (meshptr->procngbnbr = 0, enttnum = baseval, enttnnd = meshptr->enttglbnbr +
      baseval; enttnum < enttnnd; enttnum ++) {
    if (enttloctax[enttnum] == NULL)
      continue;

    meshptr->procngbnbr = MAX(meshptr->procngbnbr, enttloctax[enttnum]->commlocdat.procngbnbr);
    vertnumtax[enttnum] = 0;
  }
  
  for (procnum = 0; ; procnum ++) {
    Gnum enttngbnum;
    int  procngbnum;

    for (procngbnum = meshptr->procglbnbr, enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval;
        enttnum < enttnnd; enttnum ++) {
      
      if (enttloctax[enttnum] == NULL)
        continue;

      if ((vertnumtax[enttnum] < enttloctax[enttnum]->commlocdat.procngbnbr)
          && (enttloctax[enttnum]->commlocdat.procngbtab[vertnumtax[enttnum]] < procngbnum)) {
        enttngbnum = enttnum;
        procngbnum = enttloctax[enttnum]->commlocdat.procngbtab[vertnumtax[enttnum]];
      }
    }

    if (procngbnum == meshptr->procglbnbr)
      break;
    meshptr->procngbtab[procnum] = procngbnum;
    vertnumtax[enttngbnum] ++;

    for (enttnum = baseval, enttnnd = meshptr->enttglbnbr + baseval; enttnum < enttnnd; enttnum ++) {
      if (enttloctax[enttnum] == NULL)
        continue;

      for (; (vertnumtax[enttnum] < enttloctax[enttnum]->commlocdat.procngbnbr)
          && enttloctax[enttnum]->commlocdat.procngbtab[vertnumtax[enttnum]] <= procngbnum;
          vertnumtax[enttnum] ++);
    }
  }
  meshptr->procngbnbr = procnum;

  return (0);
}


