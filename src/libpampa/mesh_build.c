/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_build.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the centralized source
//!                mesh building routines. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

//! This routine builds a mesh from
//! the local arrays that are passed to it. If
//! a vertex label array is given, it is assumed
//! that edge ends are given with respect to these
//! labels, and thus they are updated so as to be
//! given with respect to the implicit (based)
//! global numbering.
//! As for all routines that build meshs, the private
//! fields of the mesh structure have to be initialized
//! if they are not already.
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshBuild (
    Mesh * restrict const       meshptr,           //!< mesh
    const Gnum                  idnum,             //!< Id
    const Gnum                  baseval,           //!< Base for indexing
    const Gnum                  vertnbr,           //!< Number of local vertices
    Gnum * const                verttax,           //!< Local vertex begin array
    Gnum * const                vendtax,           //!< Local vertex end array
    const Gnum                  edgenbr,           //!< Number of local edges
    const Gnum                  edgesiz,           //!< Size of local edges
    Gnum * restrict const       edgetax,           //!< Local edge array
    Gnum * restrict const       edlotax,           //!< Local edge load array (if any)
    Gnum                        enttnbr,		   //!< XXX
    Gnum *                      venttax,		   //!< XXX
    Gnum *                      esubtax,		   //!< XXX
    Gnum                        valumax,
	Values *                    valsptr,
	Gnum                        degrmax,		   //!< -1: not defined
        Gnum                        procglbnbr)

{
  Gnum                  vertnum;
  Gnum                  evrtnum;
  Gnum                  edgenum;
  Gnum                  vertnnd;
  Gnum *                sortvrttab;
  Gnum *                eidxtax;
  Gnum *                edgetmp;
  Gnum                  sizetmp;
  Gnum                  enttnum;
  Gnum                  nentnum;
  Gnum                  ngbenttnnd;
  Gnum                  num;
  Gnum                  enttnumnbr;
  Gnum *                svrttax;
  Gnum *                snumtax;
  Gnum                  vertnbr2;
  Gnum                  edgenbr2;
  MeshEntity **         entttax;
  Gnum * restrict esubbax;
  Gnum                  esubnum;
  Gnum                  enttnnd;
  Gnum *                vnbrtax;
  static Gnum     enttsingle = -1; // XXX à changer, peut-être utiliser DUMMY…

  meshptr->idnum = idnum;
  meshptr->baseval = baseval;    /* set global and local data */
  meshptr->procglbnbr = procglbnbr;

  if (esubtax != NULL) {
    vertnbr2 = vertnbr;
    edgenbr2 = edgenbr;
    enttnumnbr = enttnbr;
    if ((esubbax = (Gnum *) memAlloc (enttnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }
    esubbax -= baseval;

    memCpy (esubbax + baseval, esubtax + baseval, enttnbr * sizeof (Gnum));
    meshptr->esubmsk = ~((Gnum) 0);
  }
  else {
    vertnbr2 =
      edgenbr2 =
      enttnumnbr = 0;
    esubbax = &enttsingle;
    meshptr->esubmsk = 0;
  }
  meshptr->esubbax = (Gnum *) esubbax;

  meshptr->enttnbr = enttnbr;
  meshptr->vertnbr = vertnbr;

  // FIXME trop long à mettre avec un autre drapeau
//#ifdef PAMPA_DEBUG_MESH
//  for (vertnum = baseval, vertnnd = vertnbr + baseval;
//      vertnum < vertnnd; vertnum ++) {
//    Gnum vertnm2;
//    Gnum vertnn2;
//    for (vertnm2 = baseval, vertnn2 = vertnbr + baseval;
//        vertnm2 < vertnn2; vertnm2 ++) {
//      if ((vertnum != vertnm2) && (verttax[vertnum] < vendtax[vertnm2]) && (vendtax[vertnum] > verttax[vertnm2])) {
//        errorPrint ("Overlap of adjacency between %d and %d", vertnum, vertnm2);
//        return (1);
//      }
//    }
//  }
//#endif /* PAMPA_DEBUG_MESH */

  if (degrmax == -1) {
  	degrmax = 0;
  	for (vertnum = baseval, vertnnd = vertnbr + baseval;
      	vertnum < vertnnd; vertnum ++)  {

      if (vendtax[vertnum] - verttax[vertnum] > degrmax)
      	degrmax = vendtax[vertnum] - verttax[vertnum];
  	}
  }

  meshptr->degrmax = degrmax;

  if (memAllocGroup ((void **) (void *)
        &sortvrttab , (size_t) ((degrmax * 2) * sizeof (Gnum)),
        &edgetmp    , (size_t) (degrmax * sizeof (Gnum)),
        &eidxtax    , (size_t) (vertnbr * sizeof (Gnum)),
        &svrttax, (size_t) (vertnbr2 * sizeof (Gnum)),
        &snumtax , (size_t) (enttnumnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }

  eidxtax -= baseval;
  svrttax -= baseval;
  snumtax -= baseval;





  // XXX il manque un test sur venttax en debug pour voir si les valeurs sont
  // correctes
  CHECK_FDBG2 (meshBuild2(meshptr, vertnbr, edgesiz, venttax, &vnbrtax));



  entttax = meshptr->entttax;



  if (esubtax != NULL) {
    for (enttnum = baseval, enttnnd = enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
      Gnum      esubnum;

      if (vnbrtax[enttnum] == 0)
        continue;

      entttax[enttnum]->vertnbr = baseval;
      esubnum = esubtax[enttnum];
      snumtax[enttnum] = ((esubnum > PAMPA_ENTT_SINGLE) && (esubtax[esubnum] == PAMPA_ENTT_STABLE)) 
        ? esubnum : enttnum;
    }


    for (vertnum = baseval, vertnnd = vertnbr + baseval ;
        vertnum < vertnnd ;
        vertnum ++) {
      if (venttax[vertnum] != PAMPA_ENTT_DUMMY)
        svrttax[vertnum] = snumtax[venttax[vertnum]];
      else
        svrttax[vertnum] = PAMPA_ENTT_DUMMY;
    }

  }
  else {
    for (enttnum = baseval, enttnnd = enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {

      if (vnbrtax[enttnum] == 0)
        continue;

      entttax[enttnum]->vertnbr = baseval;
    }


    svrttax = venttax;
  }




  for (vertnum = baseval, vertnnd = vertnbr + baseval;
      vertnum < vertnnd; vertnum ++)  {

    if (venttax[vertnum] == PAMPA_ENTT_DUMMY)
      continue;

    eidxtax[vertnum] = entttax[venttax[vertnum]]->vertnbr++;
    /* The following test could be put in an entity loop instead of this vertex loop */
    if ((venttax[vertnum] != PAMPA_ENTT_DUMMY) && (esubbax[venttax[vertnum] & meshptr->esubmsk] > PAMPA_ENTT_SINGLE))
      eidxtax[vertnum] = entttax[svrttax[vertnum]]->vertnbr ++;

    for (edgenum = verttax[vertnum], num = 0 ; edgenum < vendtax[vertnum] ; edgenum ++, num ++) {
      sortvrttab[2 * num] = svrttax[edgetax[edgenum]];
      sortvrttab[2 * num + 1] = num;
    }


    sizetmp = vendtax[vertnum] - verttax[vertnum];
    intSort2asc2(sortvrttab, sizetmp);


    memCpy(edgetmp, edgetax+verttax[vertnum], sizetmp * sizeof(Gnum));
    for (edgenum = verttax[vertnum], num = 1 ;
        edgenum < vendtax[vertnum] ;
        edgenum ++, num += 2) {
      edgetax[edgenum] = edgetmp[sortvrttab[num]];
    }

    if (edlotax != NULL) {
      memCpy(edgetmp, edlotax+verttax[vertnum], sizetmp * sizeof(Gnum));
      for (edgenum = verttax[vertnum], num = 1 ;
          edgenum < vendtax[vertnum] ;
          edgenum ++, num += 2) {
        edlotax[edgenum] = edgetmp[sortvrttab[num]];
      }
    }
  }






  for (enttnum = baseval, enttnnd = enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {

    if (vnbrtax[enttnum] == 0)
      continue;

    entttax[enttnum]->vertnbr -= baseval;

    // If PAMPA_GRP_UNSTABLE exists, be careful with the following test
    switch (esubbax[enttnum & meshptr->esubmsk]) {
      case PAMPA_ENTT_STABLE :
        /* Allocate private entity neighbors data*/
        if (memAllocGroup ((void **) (void *)
              &entttax[enttnum]->nghbtax,    (size_t) (enttnbr * sizeof (MeshEnttNghb)),
              &entttax[enttnum]->mvrttax,    (size_t) (entttax[enttnum]->vertnbr * sizeof (Gnum)),
              &entttax[enttnum]->venttax,    (size_t) (entttax[enttnum]->vertnbr * sizeof (Gnum)),
              &entttax[enttnum]->svrttax, (size_t) (entttax[enttnum]->vertnbr * sizeof (Gnum)),
              (void *) NULL) == NULL) {
          errorPrint  ("out of memory");
          return      (1);
        }

        entttax[enttnum]->nghbtax -= baseval;
        entttax[enttnum]->mvrttax -= baseval;
        entttax[enttnum]->venttax -= baseval;
        entttax[enttnum]->svrttax -= baseval;
        break;

      case PAMPA_ENTT_SINGLE :
        /* Allocate private entity neighbors data*/
        if (memAllocGroup ((void **) (void *)
              &entttax[enttnum]->nghbtax, (size_t) (enttnbr * sizeof (MeshEnttNghb *)),
              &entttax[enttnum]->mvrttax,    (size_t) (entttax[enttnum]->vertnbr * sizeof (Gnum)),
              (void *) NULL) == NULL) {
          errorPrint  ("out of memory");
          return      (1);
        }

        entttax[enttnum]->nghbtax -= baseval;
        entttax[enttnum]->mvrttax -= baseval;
        entttax[enttnum]->venttax = NULL;
        entttax[enttnum]->svrttax = NULL;
        break;

      default :
        /* Allocate private entity neighbors data*/
        if (memAllocGroup ((void **) (void *)
              &entttax[enttnum]->venttax,    (size_t) (entttax[enttnum]->vertnbr * sizeof (Gnum)),
              &entttax[enttnum]->svrttax, (size_t) (entttax[enttnum]->vertnbr * sizeof (Gnum)),
              (void *) NULL) == NULL) {
          errorPrint  ("out of memory");
          return      (1);
        }

        entttax[enttnum]->mvrttax = NULL;
        entttax[enttnum]->venttax -= baseval;
        entttax[enttnum]->svrttax -= baseval;

    }

    // \attention If PAMPA_GRP_UNSTABLE exists, be careful with the following
    // test
    if (esubbax[enttnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) {
      MeshEnttNghb ** nghbtax;

      nghbtax = entttax[enttnum]->nghbtax;

      for (nentnum = baseval, ngbenttnnd = enttnbr + baseval ; nentnum < ngbenttnnd ; nentnum ++) {

        if ((vnbrtax[nentnum] == 0) && (nentnum != baseval) && (nentnum != enttnbr - 1 + baseval))
          nghbtax[nentnum] = NULL;
        else {

          if ((nghbtax[nentnum] = (MeshEnttNghb *) memAlloc (sizeof (MeshEnttNghb))) == NULL) {
            errorPrint  ("out of memory");
            return      (1);
          }
          memSet (nghbtax[nentnum], 0, sizeof (MeshEnttNghb));

          if (esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) {
            /* Allocate centralized entity private data */
            if (memAllocGroup ((void **) (void *)
                  &nghbtax[nentnum]->vindtax, (size_t) (entttax[enttnum]->vertnbr * sizeof (MeshVertBounds)),
                  (void *) NULL) == NULL) {
              errorPrint  ("out of memory");
              return      (1);
            }

            nghbtax[nentnum]->vindtax -= baseval;

            memSet (nghbtax[nentnum]->vindtax + baseval, 0, entttax[enttnum]->vertnbr * sizeof (MeshVertBounds));

          }
        }
      }
    }

    entttax[enttnum]->vertnbr = baseval; // TRICK: baseval will be removed later
  }

  if (esubtax != NULL) {
    for (enttnum = baseval, enttnnd = enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
      if (vnbrtax[enttnum] == 0)
        continue;

      if (esubbax[enttnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE)
        entttax[enttnum]->nghbtax = entttax[snumtax[enttnum]]->nghbtax;
        entttax[enttnum]->mvrttax = entttax[snumtax[enttnum]]->mvrttax;

      MeshEnttNghb ** nghbtax;

      nghbtax = entttax[enttnum]->nghbtax;

      for (nentnum = baseval, ngbenttnnd = enttnbr + baseval ; nentnum < ngbenttnnd ; nentnum ++) {
        if (vnbrtax[nentnum] == 0)
          continue;

        if (esubbax[nentnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) {
          nghbtax[nentnum]->vindtax = nghbtax[snumtax[nentnum]]->vindtax;
        }
      }
    }
  }


  for (vertnum = baseval, vertnnd = vertnbr + baseval;
      vertnum < vertnnd; vertnum ++)  {

    MeshEnttNghb ** nghbtax;

    enttnum = svrttax[vertnum];

    if (enttnum == PAMPA_ENTT_DUMMY)
      continue;
    nentnum = svrttax[edgetax[verttax[vertnum]]];

#ifdef PAMPA_DEBUG_MESH
    if ((enttnum < baseval) || (enttnum > enttnbr + baseval)
        || (nentnum < baseval) || (nentnum > enttnbr + baseval)) {
      errorPrint("invalid vertex entity number");
      return (1);
    }
#endif /* PAMPA_DEBUG_MESH */

    nghbtax = entttax[enttnum]->nghbtax;
    evrtnum = entttax[enttnum]->vertnbr;

    nghbtax[baseval]->vindtax[evrtnum].vertidx = verttax[vertnum];
    if (nentnum != baseval)
      nghbtax[baseval]->vindtax[evrtnum].vendidx = verttax[vertnum];

    nghbtax[nentnum]->vindtax[evrtnum].vertidx = verttax[vertnum];

    for (edgenum = verttax[vertnum] ; edgenum < vendtax[vertnum] ; edgenum ++) {
      if (nentnum != svrttax[edgetax[edgenum]]) {
        nghbtax[nentnum]->vindtax[evrtnum].vendidx = edgenum;
        nentnum = svrttax[edgetax[edgenum]];
#ifdef PAMPA_DEBUG_MESH
        if ((nentnum < baseval) || (nentnum > enttnbr + baseval)) {
          errorPrint("invalid vertex entity number");
          return (1);
        }
#endif /* PAMPA_DEBUG_MESH */

        nghbtax[nentnum]->vindtax[evrtnum].vertidx = edgenum;
      }

      meshptr->edgetax[edgenum] = eidxtax[edgetax[edgenum]];

    }

    nghbtax[nentnum]->vindtax[evrtnum].vendidx = edgenum;
    nghbtax[enttnbr - 1 + baseval]->vindtax[evrtnum].vendidx = edgenum;
    if (nentnum != (enttnbr - 1 + baseval))
      nghbtax[enttnbr - 1 + baseval]->vindtax[evrtnum].vertidx = vendtax[vertnum];


    entttax[enttnum]->mvrttax[entttax[enttnum]->vertnbr] = vertnum;
    if (esubbax[enttnum & meshptr->esubmsk] != PAMPA_ENTT_SINGLE) {
      enttnum = venttax[vertnum];
      esubnum = svrttax[vertnum];

      entttax[enttnum]->svrttax[entttax[enttnum]->vertnbr] = entttax[esubnum]->vertnbr;
      entttax[enttnum]->venttax[entttax[enttnum]->vertnbr] = esubnum;

      entttax[esubnum]->venttax[entttax[esubnum]->vertnbr] = enttnum;
      entttax[esubnum]->svrttax[entttax[esubnum]->vertnbr ++] = entttax[enttnum]->vertnbr ++;


    }
    else {
      entttax[enttnum]->vertnbr ++;
    }

  }



  for (enttnum = baseval, enttnnd = enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    if (vnbrtax[enttnum] == 0)
      continue;

    entttax[enttnum]->vertnbr -= baseval;
  }


  meshptr->edgenbr = edgenbr;
  meshptr->edgesiz = edgesiz;
  meshptr->venttax = venttax;
  meshptr->edlotax = edlotax;


  memFreeGroup(sortvrttab);
  memFreeGroup(vnbrtax + baseval);

  meshptr->flagval |= MESHFREEENTT;

  if (valsptr == NULL)
  	return valuesInit(&meshptr->valsptr, baseval, valumax, enttnbr);
  else {
	valsptr->cuntnbr ++;
	meshptr->valsptr = valsptr;
	return (0);
  }
}



//! This routine builds a distributed mesh from
//! XXX
//! \pre meshptr->enttglbnbr defined
//! \pre meshptr->esubbax defined
//! \pre meshptr->esubmsk defined
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshBuild2 (
    Mesh * restrict const     meshptr,              //!< mesh
    const Gnum                  vertnbr,           //!< Number of local vertices
    const Gnum                  edgesiz,           //!< XXX
    Gnum *                      venttax,           //!< Local vertex entity array
    Gnum **                     vnbrtax)           //!< XXX
{

  Gnum baseval;
  Gnum vertnum;
  Gnum vertnnd;
  Gnum esubnum;
  Gnum esubnnd;
  Gnum enttnum;
  Gnum enttnnd;

  baseval = meshptr->baseval;

  if (memAllocGroup ((void **) (void *)
        vnbrtax, (size_t) (meshptr->enttnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  *vnbrtax -= baseval;

  memSet (*vnbrtax + baseval, 0, meshptr->enttnbr * sizeof (Gnum));

  for (vertnum = baseval, vertnnd = vertnbr + baseval;
      vertnum < vertnnd; vertnum ++) 
    if (venttax[vertnum] != PAMPA_ENTT_DUMMY)
      (*vnbrtax)[venttax[vertnum]] ++;

  for (esubnum = baseval, esubnnd = meshptr->enttnbr + baseval ; esubnum < esubnnd ; esubnum ++) {
    Gnum enttnum;

    enttnum = meshptr->esubbax[esubnum & meshptr->esubmsk];

    if (enttnum > PAMPA_ENTT_SINGLE)
      (*vnbrtax)[enttnum] += (*vnbrtax)[esubnum];
  }

  if (memAllocGroup ((void **) (void *)
        &meshptr->entttax, (size_t) (meshptr->enttnbr * sizeof (MeshEntity *)),
        &meshptr->edgetax, (size_t) (edgesiz * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  meshptr->entttax -= baseval;
  meshptr->edgetax -= baseval;

  for (enttnum = baseval, enttnnd = meshptr->enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    if ((*vnbrtax)[enttnum] == 0)
      meshptr->entttax[enttnum] = NULL;
    else {
      if ((meshptr->entttax[enttnum] = (MeshEntity *) memAlloc (sizeof (MeshEntity))) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
      }
      memSet (meshptr->entttax[enttnum], 0, sizeof (MeshEntity));
    }
  }

  return (0);
}

