/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dummysizes.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       Part of the libPampa compilation job.
//!                This small program processes files that
//!                are in fact pattern header files for the
//!                libPampa library, and replaces symbolic
//!                sizes of the opaque libPampa by the
//!                proper integer values according to the
//!                machine on which it is run. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define DUMMYSIZES

#define CHARMAX                     2048          //!< Maximum line size

#define SUBSMAX                     43            //!< Maximum number of substitutions

#define C_FILENBR                   2             //!< Number of files in list
#define C_FILEARGNBR                2             //!< Number of files which can be arguments

#define C_filenamehedinp            C_fileTab[0].name //!< Source graph input file name
#define C_filenamehedout            C_fileTab[1].name //!< Statistics output file name

#define C_filepntrhedinp            C_fileTab[0].pntr //!< Source graph input file
#define C_filepntrhedout            C_fileTab[1].pntr //!< Statistics output file

#define EXPAND(s) EXPANDTWO(s)
#define EXPANDTWO(s) #s

#include "module.h"
#include "common.h"
#include "comm.h"
#ifdef SCOTCH
#include <scotch.h>
#endif /* SCOTCH */
#ifdef PTSCOTCH
#include <ptscotch.h>
#endif /* PTSCOTCH */
#include "arch.h"
#include "strat.h"
#include "values.h"
#include "dvalues.h"
#include "mesh.h"
#include "smesh.h"
#include "dmesh.h"
#include "dmesh_halo.h"
#include "mdmesh.h"

/*
**  The static definitions.
*/

static int                  C_fileNum = 0;        //!< Number of file in arg list
static File                 C_fileTab[C_FILENBR] =   //!< The file array
                            {
                              { "r" },
                              { "w" } };

/******************************/
/*                            */
/* This is the main function. */
/*                            */
/******************************/

void
subsFill (
char *                      substab[2],
char *                      origptr,
int                         subsval)
{
  char *              subsptr;

  subsptr = malloc (32 * sizeof (char));
  sprintf (subsptr, "%d", (int) ((subsval + sizeof (double) - 1) / sizeof (double)));

  substab[0] = origptr;
  substab[1] = subsptr;
}

void
subsFill2 (
char *                      substab[2],
char *                      origptr,
int                         subsval)
{
  char *              subsptr;

  subsptr = malloc (32 * sizeof (char));
  sprintf (subsptr, "%d", (int) subsval);

  substab[0] = origptr;
  substab[1] = subsptr;
}

/******************************/
/*                            */
/* This is the main function. */
/*                            */
/******************************/

int
main (
int                         argc,
char *                      argv[])
{
  char                chartab[CHARMAX];
  char                chartmp[CHARMAX];
  char *              substab[SUBSMAX][2];        /* Substitution array */
  int                 subsnbr;
  int                 i;

  if ((argc >= 2) && (argv[1][0] == '?')) {       /* If need for help */
    printf ("Usage is:\ndummysizes [<input pattern header file> [<output header file>]]\n");
    return (((argv[1][0] == '?') && argv[1][1] == '\0') ? 0 : 1);
  }

  for (i = 0; i < C_FILENBR; i ++)                /* Set default stream pointers */
    C_fileTab[i].pntr = (C_fileTab[i].mode[0] == 'r') ? stdin : stdout;
  for (i = 1; i < argc; i ++) {                   /* Loop for all option codes */
    if ((argv[i][0] != '+') &&                    /* If found a file name      */
        ((argv[i][0] != '-') || (argv[i][1] == '\0'))) {
      if (C_fileNum < C_FILEARGNBR)               /* A file name has been given */
        C_fileTab[C_fileNum ++].name = argv[i];
      else {
        fprintf (stderr, "dummysizes: ERROR: main: too many file names given");
        exit    (1);
      }
    }
    else {                                        /* If found an option name */
      switch (argv[i][1]) {
        case 'H' :                                /* Give the usage message */
        case 'h' :
          printf ("Usage is:\ndummysizes [<input pattern header file> [<output header file>]]\n");
          exit       (0);
        case 'V' :
          fprintf (stderr, "dummysizes, version " PAMPA_VERSION_STRING "\n");
          fprintf (stderr, "Copyright 2004,2007,2008 ENSEIRB, INRIA & CNRS, France\n");
          fprintf (stderr, "This software is libre/free software under CeCILL-C -- see the user's manual for more information\n");
          return  (0);
        default :
          fprintf (stderr, "dummysizes: ERROR: main: unprocessed option (\"%s\")", argv[i]);
          exit    (1);
      }
    }
  }

#ifdef PTSCOTCH
  if (sizeof(SCOTCH_Num) != sizeof(INT)) {
    fprintf(stderr, "dummysizes: ERROR: SCOTCH_Num and PAMPA_Num have different size,\n");
    fprintf(stderr, "\tmaybe INTSIZE64 flag is missing in building SCOTCH or PaMPA.\n");
    exit   (1);
  }
#endif /* PTSCOTCH */

  for (i = 0; i < C_FILENBR; i ++) {              /* For all file names     */
    if ((C_fileTab[i].name[0] != '-') ||          /* If not standard stream */
        (C_fileTab[i].name[1] != '\0')) {
      if ((C_fileTab[i].pntr = fopen (C_fileTab[i].name, C_fileTab[i].mode)) == NULL) { /* Open the file */
          fprintf (stderr, "dummysizes: ERROR: main: cannot open file (%d)", i);
          exit    (1);
      }
    }
  }

  substab[0][0] = "library.h ";
  substab[0][1] = "pampa.h   ";
  substab[1][0] = "libraryf.h ";
  substab[1][1] = "pampaf.h   ";
  substab[2][0] = "library_commonf.h ";
  substab[2][1] = "pampa_commonf.h   ";
  substab[3][0] = "DUMMYINT";
  substab[3][1] = EXPAND(INT);
  substab[4][0] = "DUMMYMAXINT";
  substab[4][1] = EXPAND(INTVALMAX);
  substab[5][0] = "#ifndef PAMPA_BUILD_ITER";
  substab[5][1] = "";
  substab[6][0] = "#endif /* PAMPA_BUILD_ITER */";
  substab[6][1] = "";
  substab[7][0] = "DUMMYVERSION";
  substab[7][1] = EXPAND (PAMPA_VERSION);
  substab[8][0] = "DUMMYRELEASE";
  substab[8][1] = EXPAND (PAMPA_RELEASE);
  substab[9][0] = "DUMMYPATCHLEVEL";
  substab[9][1] = EXPAND (PAMPA_PATCHLEVEL);
  substab[10][0] = "DUMMYMPIINT";
  substab[10][1] = EXPAND (COMM_INT);
  substab[11][0] = "DUMMYPAMPASTRING";
  printf ("gnumstring: %s\n", GNUMSTRING);
  substab[11][1] = EXPAND (GNUMSTRING);

  substab[12][0] = "VERTNUM";
  substab[12][1] = "v";
  substab[13][0] = "VERTNND";
  substab[13][1] = "w";
  substab[14][0] = "NENTMSK";
  substab[14][1] = "m";
  substab[15][0] = "NESBNUM";
  substab[15][1] = "s";
  substab[16][0] = "NENTNUM";
  substab[16][1] = "e";
  substab[17][0] = "NENTBAS";
  substab[17][1] = "b";
  substab[18][0] = "EDGETAB";
  substab[18][1] = "f";
  substab[19][0] = "SVRTTAB";
  substab[19][1] = "t";
  substab[20][0] = "MNGBTAB";
  substab[20][1] = "g";
  substab[21][0] = "VINDTAB";
  substab[21][1] = "i";
  substab[22][0] = "VENTNUM";
  substab[22][1] = "d";
  substab[23][0] = "NENTTAB";
  substab[23][1] = "u";
  substab[24][0] = "VINDVAL";
  substab[24][1] = "j";
  substab[25][0] = "SNGBTAB";
  substab[25][1] = "r";

  subsnbr = 26;
  subsFill (substab[subsnbr ++], "DUMMYSIZEDMESHHALOREQS",           sizeof (DmeshHaloRequests)); // TRICK: before DUMMYSIZEDMESH
  subsFill (substab[subsnbr ++], "DUMMYSIZEDMESH",          sizeof (Dmesh));
  subsFill (substab[subsnbr ++], "DUMMYSIZEMDMESH",          sizeof (Mdmesh));
  subsFill (substab[subsnbr ++], "DUMMYSIZEMESH",           sizeof (Mesh));
  subsFill (substab[subsnbr ++], "DUMMYSIZESMESH",           sizeof (Smesh));
#ifdef PTSCOTCH
  subsFill (substab[subsnbr ++], "DUMMYSIZEARCH",           sizeof (Arch));
  subsFill (substab[subsnbr ++], "DUMMYSIZESTRAT",           sizeof (Strat));
#else /* PTSCOTCH */
  subsFill (substab[subsnbr ++], "DUMMYSIZEARCH",            1);
  subsFill (substab[subsnbr ++], "DUMMYSIZESTRAT",           1); // TRICK: 1 for dummysize 
#endif /* PTSCOTCH */
  subsFill (substab[subsnbr ++], "DUMMYSIZENUM",           8 * sizeof (INT));
  subsFill (substab[subsnbr ++], "DUMMYSIZEDOUBLE",           8 * sizeof (double));
  // XXX trouver une solution plus propre pour ne pas maintenir  les deux versions (c et fortran) (variable CMake par exemple ??)
  subsFill2 (substab[subsnbr ++], "DUMMYVERTLOCAL",           ~ 0x0002);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTINTERNAL",        ~ 0x0004);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTBOUNDARY",        ~ 0x0008);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTOVERLAP",         ~ 0x0010);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTANY",             ~ 0x0020);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTGHOST",           ~ 0x0040);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTFRONTIER",        ~ 0x0100);
  subsFill2 (substab[subsnbr ++], "DUMMYVERTNOTFRONTIER",     ~ 0x0200);
  //subsFill2 (substab[subsnbr ++], "DUMMYOVLPELEM",            ~ 0x0200);

  while (fgets (chartab, CHARMAX, C_filepntrhedinp) != NULL) { /* Infinite loop on file lines */
    int                 charnbr;
    int                 subsnum;

    if (((charnbr = strlen (chartab)) >= (CHARMAX - 1)) && /* If line read is at least as long as maximum size     */
        (chartab[CHARMAX - 1] != '\n')) {         /* And last character is not a newline, that is, some is missing */
      fprintf (stderr, "dummysizes: ERROR: line too long\n");
      exit    (1);
    }

    for (subsnum = 0; subsnum < subsnbr; subsnum ++) { /* Perform substitutions */
      char *              charptr;                /* Place where token found    */

      while ((charptr = strstr (chartab, substab[subsnum][0])) != NULL) { /* As long as substitution can be performed */
        int                 charnbr;

        charnbr = strlen (substab[subsnum][0]);   /* Length of token            */

        strcpy (chartmp, charptr + charnbr);      /* Save end of line */

        sprintf (charptr, "%s%s", substab[subsnum][1], chartmp); /* Replace end of line with substituted token */
      }
    }

    fputs (chartab, C_filepntrhedout);            /* Output possibly updated line */
  }

#ifdef PAMPA_DEBUG_MAIN1
  for (i = 0; i < C_FILENBR; i ++) {              /* For all file names     */
    if ((C_fileTab[i].name[0] != '-') ||          /* If not standard stream */
        (C_fileTab[i].name[1] != '\0')) {
      fclose (C_fileTab[i].pntr);                 /* Close the stream */
    }
  }
#endif /* PAMPA_DEBUG_MAIN1 */

  exit (0);
}
