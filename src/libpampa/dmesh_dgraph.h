/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_dgraph.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the distributed graph building routines
//!                from a distributed mesh.
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define DMESH_ENTT_MAIN      1               //!< Tag for vertices considered as elements
#define DMESH_ENTT_MAIN_PART 2               //!< Tag for vertices considered as elements (a part of them)
#define DMESH_ENTT_ANY       3               //!< Tag for all vertices

int dmeshDgraphInit (
    Dmesh * const        meshptr,
    SCOTCH_Dgraph *      grafptr);

int dmeshDgraphBuild (
    Dmesh * const        meshptr,
    Gnum                 flagval,
    Gnum                 dvrtlocnbr,
    Gnum * const         dvrtgsttax,
    Gnum * const         veloloctax,
    Gnum * const         edloloctax,
    SCOTCH_Dgraph *      grafptr,
    Gnum *               vertlocbas);

int dmeshDgraphExit (
    SCOTCH_Dgraph *      grafptr);

int dmeshDgraphFree (
    SCOTCH_Dgraph *      grafptr);
