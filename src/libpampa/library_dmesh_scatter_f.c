/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_scatter_f.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API of the libpampa
//!                library. 
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine scatters the data of a
//! centralized mesh on a distributed mesh.
//! entttab, tagtab and typetab must be defined on each processor. All of these
//! arrays are of size the number of attached values on the current centralized
//! mesh
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

FORTRAN (                                       \
PAMPAF_DMESHSCATTERF, pampaf_dmeshscatterf, (   \
PAMPA_Dmesh * const       dmshptr,                                      /*!< Distributed mesh         */   \
const PAMPA_Mesh * const  cmshptr,                                      /*!< Centralized mesh         */   \
PAMPA_Num * const         typenbr,                                      /*!< XXX                      */   \
PAMPA_Num * const         entttab,                                      /*!< Array of entity numbers  */   \
PAMPA_Num * const         tagtab,                                       /*!< Array of tags            */   \
MPI_Fint * const          typetab,                                      /*!< Array of types           */   \
PAMPA_Num * const         ovlpglbval,	         		        /*!< Overlap value            */   \
PAMPA_Num * const            revaptr),           \
(dmshptr, cmshptr, typenbr, entttab, tagtab, typetab, ovlpglbval, revaptr))
{
  const Mesh *       srccmshptr;
  MPI_Datatype *     typetb2;
  PAMPA_Num          tmpval;

  *revaptr = 0;

  if ((typetb2 = (MPI_Datatype *) memAlloc (*typenbr * sizeof (MPI_Datatype))) == NULL) {
    errorPrint  ("out of memory");
    *revaptr = -1;
    return;
  }

  for (tmpval = 0; tmpval < (*typenbr); tmpval ++)
    typetb2[tmpval] = MPI_Type_f2c (typetab[tmpval]);

  srccmshptr = (((void *) cmshptr) == ((void *) dmshptr)) ? NULL : (const Mesh *) cmshptr; /* Consider same pointers as flag for non-root process */

  *revaptr = dmeshScatter2 ((Dmesh *) dmshptr, srccmshptr, (Gnum) (*typenbr), (Gnum *) entttab, (Gnum *) tagtab, typetb2, (Gnum) (*ovlpglbval));
  memFree (typetb2); /* FIXME Maybe typetb2 is still used, so be careful!! */
}

