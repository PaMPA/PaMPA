
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mesh.h"
#include <pampa.h>
#include "dmesh_gather_induce_multiple.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine computes induced meshes and gather each induced mesh on
//! a given processor. Each centralized mesh could have one specific processor
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

  int
PAMPA_dmeshGatherInduceMultiple (
    PAMPA_Dmesh * restrict const    meshptr,              /**< mesh */
    const PAMPA_Num                 partglbnbr,
    PAMPA_Num * const               vnumloctab,           // TODO: incorrect used name
    PAMPA_Num * const               partglbtab,
	PAMPA_Num *                     meshlocnbr,
    PAMPA_Mesh **                   meshloctab)
{

  Dmesh *                   srcmeshptr;         /* Pointer to source mesh structure           */

  srcmeshptr = (Dmesh *) meshptr;                /* Use structure as source mesh */

  return dmeshGatherInduceMultiple (srcmeshptr, 0, (Gnum) partglbnbr, (Gnum *) vnumloctab - srcmeshptr->baseval, (Gnum *) partglbtab - srcmeshptr->baseval, (Gnum *) meshlocnbr, (Mesh **) meshloctab);
}
