/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_scatter.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the routine that
//!                builds a distributed mesh by evenly
//!                distributing the pieces of a centralized
//!                mesh across processors. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define DMESH_SCATTER

#include "module.h"
#include "common.h"
#include "values.h"
#include "dvalues.h"
#include "mesh.h"
#include "dmesh.h"
#include "dmesh_build.h"
#include "dmesh_scatter.h"
#include "comm.h"
#include "pampa.h"


//! \pre verttax must be allocated and initialized to -1
//! \pre edgetax must be allocated too
static
int
dmeshScatter3 (
	const Mesh * restrict const cmshptr,
	Gnum * restrict 			verttax,	
	Gnum * restrict 			vendtax,	
	Gnum * restrict 			venttax,	
	Gnum * restrict				edgetax)
{
  Gnum enttnum;
  Gnum enttnnd;
  Gnum baseval;
  Gnum edgenum;
  Gnum mvrtnum;
  Gnum mvrtnnd;
  DmeshScatterPerm * restrict permtax;

  baseval = cmshptr->baseval;

  if ((permtax = (DmeshScatterPerm *) memAlloc (cmshptr->vertnbr * sizeof (DmeshScatterPerm))) == NULL) {
    errorPrint  ("out of memory");
    return (1);
  }

  permtax -= baseval;

#ifdef PAMPA_DEBUG_DMESH2
  memSet (permtax + baseval, ~0, cmshptr->vertnbr * sizeof (DmeshScatterPerm));
#endif /* PAMPA_DEBUG_DMESH2 */
  for (enttnum = baseval, enttnnd = baseval + cmshptr->enttnbr ; enttnum < enttnnd ; enttnum ++) {
    Gnum vertnum;
    Gnum vertnnd;
    MeshEntity * enttptr;

    if ((cmshptr->esubbax[enttnum & cmshptr->esubmsk] > PAMPA_ENTT_SINGLE) || (cmshptr->entttax[enttnum] == NULL))
      continue;

    enttptr = cmshptr->entttax[enttnum];

    for (vertnum = baseval, vertnnd = baseval + enttptr->vertnbr ; vertnum < vertnnd ; vertnum ++) {
	  Gnum mvrtnum;
 	  mvrtnum  = enttptr->mvrttax[vertnum];
      permtax[mvrtnum].vertnum = vertnum;
      permtax[mvrtnum].enttnum = enttnum;
    }
  }

  for (edgenum = mvrtnum = baseval, mvrtnnd = baseval + cmshptr->vertnbr ; mvrtnum < mvrtnnd ; mvrtnum ++) {
    Gnum vertnum;
    Gnum engbnum;
    Gnum engbnnd;
    MeshEntity * enttptr;

#ifdef PAMPA_DEBUG_DMESH2
    if (permtax[mvrtnum].vertnum == ~0) {
      errorPrint ("internal error");
      return (1);
    }
#endif /* PAMPA_DEBUG_DMESH2 */

    vertnum = permtax[mvrtnum].vertnum;
    enttnum = permtax[mvrtnum].enttnum;
    enttptr = cmshptr->entttax[enttnum];

    verttax[mvrtnum] = edgenum;
    venttax[mvrtnum] = enttnum;

    for (engbnum = baseval, engbnnd = baseval + cmshptr->enttnbr ; engbnum < engbnnd ; engbnum ++) {
      MeshEnttNghb	*	nghbptr;
      MeshEntity	*	engbptr;
      Gnum edgeidx;
      Gnum edgennd;

      engbptr = cmshptr->entttax[engbnum];

      if (engbptr == NULL)
        continue;

      nghbptr = enttptr->nghbtax[engbnum];
      for (edgeidx = nghbptr->vindtax[vertnum].vertidx, edgennd = nghbptr->vindtax[vertnum].vendidx ; edgeidx < edgennd ; edgeidx ++)
        edgetax[edgenum ++] = engbptr->mvrttax[cmshptr->edgetax[edgeidx]];

    }
    vendtax[mvrtnum] = edgenum;
  }

  memFree (permtax + baseval);
  return (0);
}



//! This function evenly distributes the pieces
//! of a centralized mesh across processors.
//! It returns:
//! - 0   : if scattering has succeeded.
//! - !0  : on error.

int
dmeshScatter2 (
Dmesh * restrict const       meshptr,            //!< Distributed mesh
const Mesh * restrict const  cmshptr,            //!< Centralized mesh to scatter
Gnum const                   typenbr,            //!< XXX
Gnum * const                 entttab,            //!< Array of entity numbers
Gnum * const                 tagtab,             //!< Array of tags
MPI_Datatype * const         typetab,            //!< Array of types
Gnum const                   ovlpglbval)         //!< Overlap value
{
  Gnum                  baseval;           
  Gnum                  vertlocnbr;           /* Number of local vertices             */
  Gnum *                vertloctax;           /* Local vertex begin array             */
  Gnum *                vendloctax;           /* Local vertex end array               */
  Gnum                  edgelocnbr;           /* Number of local edges                */
  Gnum *                edgeloctax;           /* Local edge array                     */
  Gnum *                edloloctax;           /* Local edge load array (if any)       */
  Gnum *                ventloctax;
  Gnum *                esubglbtax;
  Gnum                  reduloctab[9];        /* Arrays for reductions                */
  Gnum                  reduglbtab[9];
  int                   proclocnum;
  int                   procnum;
  int                   procnnd;
  Gnum edlolocnbr;
  int cheklocval;
  Gnum *				verttax;
  Gnum *				vendtax;
  Gnum *                venttax;
  Gnum *				edgetax;
  Gnum *				edlotax;
  DmeshScatterValue *   vidxloctab;
  Gnum                  enttglbnbr;
  Gnum                  valuglbmax;
  Gnum * ecntsndtab;
  Gnum * edspsndtab;
  Gnum vertlocbas;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum esubglbnbr;
  Gnum valuglbnbr;
  Gnum valuglbnum;

  cheklocval = 0;


  if (cmshptr != NULL) {
    reduloctab[0] = 1; /* root process */
    reduloctab[1] = (Gnum) cmshptr->proclocnum;
    reduloctab[2] = cmshptr->baseval;
    reduloctab[3] = cmshptr->enttnbr;
    reduloctab[4] = cmshptr->vertnbr;
    reduloctab[5] = cmshptr->valsptr->valunbr;
    reduloctab[6] = cmshptr->valsptr->valumax;
    reduloctab[7] = (cmshptr->esubmsk != 0) ? 1 : 0;
    reduloctab[8] = (cmshptr->edlotax != NULL) ? 1 : 0;

    edlotax = cmshptr->edlotax;


    if (memAllocGroup ((void **) (void *)
          &verttax, (size_t) (cmshptr->vertnbr * sizeof (Gnum)),
          &vendtax, (size_t) (cmshptr->vertnbr * sizeof (Gnum)),
          &venttax, (size_t) (cmshptr->vertnbr * sizeof (Gnum)),
          &edgetax, (size_t) (cmshptr->edgenbr * sizeof (Gnum)),
          (void *) NULL) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
    verttax -= cmshptr->baseval;
    vendtax -= cmshptr->baseval;
    venttax -= cmshptr->baseval;
    edgetax -= cmshptr->baseval;

    CHECK_FERR (dmeshScatter3 (cmshptr, verttax, vendtax, venttax, edgetax), meshptr->proccomm);

  }
  else {
  reduloctab[0] =
  reduloctab[1] = 
  reduloctab[2] = 
  reduloctab[3] = 
  reduloctab[4] = 
  reduloctab[5] = 
  reduloctab[6] = 
  reduloctab[7] = 
  reduloctab[8] = 0;

  verttax =
  vendtax =
  edgetax =
  venttax =
  edlotax = NULL;
  }

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce (reduloctab, reduglbtab, 9, GNUM_MPI, MPI_SUM, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if (reduglbtab[0] != 1) {
    errorPrint ("only one root is permitted");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  proclocnum = reduglbtab[1];
  baseval = reduglbtab[2];
  enttglbnbr = reduglbtab[3];
  esubglbnbr = ((cmshptr == NULL) && (reduglbtab[7] != 0)) ? enttglbnbr : 0;
  vertlocnbr = DATASIZE (reduglbtab[4], meshptr->procglbnbr, meshptr->proclocnum);
  edlolocnbr = (reduglbtab[8] != 0) ? vertlocnbr : 0;
  valuglbmax = reduglbtab[6];
  valuglbnbr = reduglbtab[5];


  CHECK_FERR (dmeshBuild2 (meshptr, baseval, vertlocnbr, vertlocnbr), meshptr->proccomm);

  /* Allocate memory of vertex entity arrays */
  if (memAllocGroup ((void **) (void *)
        &vertloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &vendloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &ventloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &esubglbtax, (size_t) (esubglbnbr * sizeof (Gnum)),
        &ecntsndtab, (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
        &edspsndtab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (Gnum)),
        &vidxloctab, (size_t) (typenbr * sizeof (DmeshScatterValue)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  vertloctax -= baseval;
  vendloctax -= baseval;
  ventloctax -= baseval;
  esubglbtax  = (reduglbtab[7] != 0) ? (esubglbtax - baseval) : NULL;

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commGnumScatterv (verttax, meshptr->proccnttab, meshptr->procdsptab, GNUM_MPI,
        vertloctax + baseval, vertlocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    memFreeGroup (vertloctax + baseval);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commGnumScatterv (vendtax, meshptr->proccnttab, meshptr->procdsptab, GNUM_MPI,
        vendloctax + baseval, vertlocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    memFreeGroup (vertloctax + baseval);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commGnumScatterv (venttax, meshptr->proccnttab, meshptr->procdsptab, GNUM_MPI,
        ventloctax + baseval, vertlocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    memFreeGroup (vertloctax + baseval);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  vertlocbas = vertloctax[baseval] - baseval;
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd ; vertlocnum ++) {
    vertloctax[vertlocnum] -= vertlocbas;
    vendloctax[vertlocnum] -= vertlocbas;
  }

  edgelocnbr = vendloctax[vertlocnnd - 1] - vertloctax[baseval];
  edlolocnbr = (reduglbtab[8] != 0) ? edgelocnbr : 0;


  if (memAllocGroup ((void **) (void *)
        &edgeloctax, (size_t) (edgelocnbr * sizeof (Gnum)),
        &edloloctax, (size_t) (edlolocnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  edgeloctax -= baseval;
  edloloctax  = (reduglbtab[8] != 0) ? (edloloctax - baseval) : NULL;


  if (cmshptr != NULL) {
    esubglbtax = (reduglbtab[7] != 0) ? cmshptr->esubbax : NULL;

    for (procnum = 0, procnnd = meshptr->procglbnbr + 0;
        procnum < procnnd ; procnum ++) {
      ecntsndtab[procnum] = vendtax[meshptr->procdsptab[procnum] + meshptr->proccnttab[procnum] - 1] -
        verttax[meshptr->procdsptab[procnum]];
      edspsndtab[procnum] = verttax[meshptr->procdsptab[procnum]] - baseval;
    }

    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commGnumScatterv (edgetax + baseval, ecntsndtab, edspsndtab, GNUM_MPI,
          edgeloctax + baseval, edgelocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      memFreeGroup (vertloctax + baseval);
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    if (reduglbtab[8] != 0) {
      CHECK_VDBG (cheklocval, meshptr->proccomm);
      if (commGnumScatterv (edlotax + baseval, ecntsndtab, edspsndtab, GNUM_MPI,
            edloloctax + baseval, edlolocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        memFreeGroup (vertloctax + baseval);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
    }
  }
  else {
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commGnumScatterv (NULL, NULL, NULL, GNUM_MPI,
          edgeloctax + baseval, edgelocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      memFreeGroup (vertloctax + baseval);
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    if (reduglbtab[8] != 0) {
      CHECK_VDBG (cheklocval, meshptr->proccomm);
      if (commGnumScatterv (NULL, NULL, NULL, GNUM_MPI,
            edloloctax + baseval, edlolocnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        memFreeGroup (vertloctax + baseval);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
    }
  }

  if (reduglbtab[7] != 0) {
    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commBcast (esubglbtax + baseval, enttglbnbr, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      memFreeGroup (vertloctax + baseval);
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }


  meshptr->flagval |= DMESHFREETABS;

  if (dmeshBuild (meshptr, baseval, vertlocnbr, vertlocnbr, vertloctax, vendloctax, edgelocnbr, edgelocnbr, edgeloctax, edloloctax, enttglbnbr, ventloctax, esubglbtax, valuglbmax, ovlpglbval, NULL, -1, NULL, NULL, NULL, -1, -1, NULL) != 0) {
    memFree (vertloctax + baseval);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if (cmshptr != NULL)
    memFreeGroup (verttax + baseval);
  memFreeGroup (edgeloctax + baseval);

  for (valuglbnum = 0; valuglbnum < typenbr; valuglbnum ++) {
    vidxloctab[valuglbnum].enttnum = entttab[valuglbnum];
    vidxloctab[valuglbnum].tagnum = tagtab[valuglbnum];
    vidxloctab[valuglbnum].indxnum = valuglbnum;
  }

  intSort3asc2(vidxloctab, typenbr);

  if (cmshptr != NULL) {
    Gnum enttnum;
    Gnum enttnnd;
    for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = cmshptr->enttnbr + baseval; enttnum < enttnnd; enttnum ++) {
      Values * valsptr = cmshptr->valsptr;
      Value * restrict valuptr;

      if (enttnum >= baseval) {
        MeshEntity * restrict enttptr;

        enttptr = cmshptr->entttax[enttnum];

        if (valsptr->evaltak[enttnum] != NULL) {
          memSet (ecntsndtab, 0, meshptr->procglbnbr * sizeof (Gnum));

          int procngbnum = 0;
          int procngbmax;
          Gnum vertnum;
          Gnum vertnnd;

          for (vertnum = baseval, vertnnd = enttptr->vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
            Gnum mvrtnum;

            mvrtnum = enttptr->mvrttax[vertnum];

            if (!((meshptr->procvrttab[procngbnum] <= mvrtnum) && (meshptr->procvrttab[procngbnum+1] > mvrtnum)))
              for (procngbmax = meshptr->procglbnbr; // TRICK: procngbnum=0 not necessary
                  procngbmax - procngbnum > 1; ) {
                int                 procngbmed;

                procngbmed = (procngbmax + procngbnum) / 2;
                if (meshptr->procvrttab[procngbmed] <= mvrtnum)
                  procngbnum = procngbmed;
                else
                  procngbmax = procngbmed;
              }

            ecntsndtab[procngbnum] ++;
          }

          for (edspsndtab[0] = procnum = 0, procnnd = meshptr->procglbnbr;
              procnum < procnnd ; procnum ++) {
            edspsndtab[procnum + 1] = ecntsndtab[procnum] + edspsndtab[procnum];
          }
        }
      }

      for (valuptr = valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next) {
        MPI_Datatype typeval;
        void * valuloctab;
        Gnum vidxmax;
        Gnum vidxnum;

        for (vidxnum = 0, vidxmax = typenbr;
            vidxmax - vidxnum > 1; ) {
          Gnum                 vidxmed;

          vidxmed = (vidxmax + vidxnum) / 2;
          // XXX utiliser des variables locales pour éviter d'accéder à valuptr->*
          if ((vidxloctab[vidxmed].enttnum < valuptr->enttnum)
              || ((vidxloctab[vidxmed].enttnum == valuptr->enttnum) && (vidxloctab[vidxmed].tagnum <= valuptr->tagnum)))
            vidxnum = vidxmed;
          else
            vidxmax = vidxmed;
        }

#ifdef PAMPA_DEBUG_DMESH
        if ((vidxloctab[vidxnum].enttnum != valuptr->enttnum) || (vidxloctab[vidxnum].tagnum != valuptr->tagnum)) {
          errorPrint ("Type for value on entity %d with tag %d has not be defined", valuptr->enttnum, valuptr->tagnum);
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
        CHECK_VDBG (cheklocval, meshptr->proccomm);
        if (commBcast (vidxloctab + vidxnum, 2, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);

        typeval = typetab[vidxloctab[vidxnum].indxnum];

        if (enttnum < baseval) {
          if (enttnum == PAMPA_ENTT_VIRT_PROC) {
            errorPrint ("Not yet implemented"); /* TODO */
            return (1);
          }
          if (dmeshValueLink(meshptr, &valuloctab, PAMPA_VALUE_PRIVATE, NULL, NULL, typeval, vidxloctab[vidxnum].enttnum, vidxloctab[vidxnum].tagnum) != 0) { // XXX toute valeur est publique pour le moment, à changer en mettant flagval dans Values
            errorPrint ("value link error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          CHECK_VDBG (cheklocval, meshptr->proccomm);
          if (commGnumScatterv (valuptr->valutab, meshptr->proccnttab, meshptr->procdsptab, typeval,
                valuloctab, meshptr->vertlocnbr, typeval, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);
        }
        else {
          if (dmeshValueLink(meshptr, &valuloctab, PAMPA_VALUE_PUBLIC, NULL, NULL, typeval, vidxloctab[vidxnum].enttnum, vidxloctab[vidxnum].tagnum) != 0) { // XXX toute valeur est publique pour le moment, à changer en mettant flagval dans Values
            errorPrint ("value link error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          CHECK_VDBG (cheklocval, meshptr->proccomm);
          if (commGnumScatterv (valuptr->valutab, ecntsndtab, edspsndtab, typeval,
                valuloctab, meshptr->enttloctax[vidxloctab[vidxnum].enttnum]->vertlocnbr, typeval, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);
        }
      }
    }
  }
  else 
    for (valuglbnum = 0; valuglbnum < valuglbnbr ; valuglbnum ++) {
      void * valuloctab;
      DmeshScatterValue valuval; // XXX peut-être trouver un nom plus approprié
      MPI_Datatype typeval;
      Gnum vidxnum;
      Gnum vidxmax;




      CHECK_VDBG (cheklocval, meshptr->proccomm);
      if (commBcast (&valuval, 2, GNUM_MPI, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

      for (vidxnum = 0, vidxmax = typenbr;
          vidxmax - vidxnum > 1; ) {
        Gnum                 vidxmed;

        vidxmed = (vidxmax + vidxnum) / 2;
        // XXX utiliser des variables locales pour éviter d'accéder à valuval.*
        if ((vidxloctab[vidxmed].enttnum < valuval.enttnum)
            || ((vidxloctab[vidxmed].enttnum == valuval.enttnum) && (vidxloctab[vidxmed].tagnum <= valuval.tagnum)))
          vidxnum = vidxmed;
        else
          vidxmax = vidxmed;
      }

#ifdef PAMPA_DEBUG_DMESH
      if ((vidxloctab[vidxnum].enttnum != valuval.enttnum) || (vidxloctab[vidxnum].tagnum != valuval.tagnum)) {
        errorPrint ("Type for value on entity %d with tag %d has not be defined", valuval.enttnum, valuval.tagnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

      typeval = typetab[vidxloctab[vidxnum].indxnum];

      if (valuval.enttnum < baseval) {
        if (valuval.enttnum == PAMPA_ENTT_VIRT_PROC) {
          errorPrint ("Not yet implemented"); /* TODO */
          return (1);
        }
        if (dmeshValueLink(meshptr, &valuloctab, PAMPA_VALUE_PRIVATE, NULL, NULL, typeval, valuval.enttnum, valuval.tagnum) != 0) { // XXX toute valeur est publique pour le moment, à changer en mettant flagval dans Values
          errorPrint ("value link error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);

        CHECK_VDBG (cheklocval, meshptr->proccomm);
        if (commGnumScatterv (NULL, NULL, NULL, typeval,
              valuloctab, meshptr->vertlocnbr, typeval, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);
      }
      else {
        if (dmeshValueLink(meshptr, &valuloctab, PAMPA_VALUE_PUBLIC, NULL, NULL, typeval, valuval.enttnum, valuval.tagnum) != 0) { // XXX toute valeur est publique pour le moment, à changer en mettant flagval dans Values
          errorPrint ("value link error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);

        CHECK_VDBG (cheklocval, meshptr->proccomm);
        if (commGnumScatterv (NULL, NULL, NULL, typeval,
              valuloctab, meshptr->enttloctax[valuval.enttnum]->vertlocnbr, typeval, proclocnum, meshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);
      }
    }

  memFreeGroup (vertloctax + baseval);
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}
