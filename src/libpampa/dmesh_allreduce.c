/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_allreduce.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contains the sum-max reduce
//!                routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define DMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"

//! \brief This routine creates an allreduce operator from
//! the function pointer that is passed to it, and
//! use it to perform an allreduce operation on the
//! given data.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!

int
dmeshAllreduceMaxSum2 (
Gnum *                      reduloctab,           /* Pointer to array of local Gnum data   */
Gnum *                      reduglbtab,           /* Pointer to array of reduced Gnum data */
int                         redumaxsumnbr,        /* Number of max + sum Gnum operations   */
MPI_User_function *         redufuncptr,          /* Pointer to operator function          */
MPI_Comm                    proccomm)             /* Communicator to be used for reduction */
{
  MPI_Datatype      redutypedat;                  /* Data type for finding best separator              */
  MPI_Op            reduoperdat;                  /* Handle of MPI operator for finding best separator */

  if ((MPI_Type_contiguous (redumaxsumnbr, GNUM_MPI, &redutypedat) != MPI_SUCCESS) ||
      (MPI_Type_commit (&redutypedat)                              != MPI_SUCCESS) ||
      (MPI_Op_create (redufuncptr, 1, &reduoperdat)                != MPI_SUCCESS)) {
    errorPrint ("dmeshAllreduceMaxSum: communication error (1)");
    return     (1);
  }

  if (commAllreduce (reduloctab, reduglbtab, 1, redutypedat, reduoperdat, proccomm) != MPI_SUCCESS) {
    errorPrint ("dmeshAllreduceMaxSum: communication error (2)");
    return     (1);
  }

  if ((MPI_Op_free   (&reduoperdat) != MPI_SUCCESS) ||
      (MPI_Type_free (&redutypedat) != MPI_SUCCESS)) {
    errorPrint ("dmeshAllreduceMaxSum: communication error (3)");
    return     (1);
  }

  return (0);
}
