/* Copyright 2004,2007-2012,2014-2016 IPB, Universite de Bordeaux, Inria & CNRS
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        common_integer.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module handles the generic integer
//!                type. 
//!
//!   \date        Version 1.0: from: 26 Mar 2012
//!                             to:   10 Aug 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define COMMON_INTEGER

#ifndef COMMON_NOMODULE
#include "module.h"
#endif /* COMMON_NOMODULE */
#include "common.h"

/********************************/
/*                              */
/* Basic routines for fast I/O. */
/*                              */
/********************************/

/* Fast read for INT values.
** It returns:
** - 1  : on success.
** - 0  : on error.
*/

int
intLoad (
FILE * const                stream,               /*+ Stream to read from     +*/
INT * const                 valptr)               /*+ Area where to put value +*/
{
  int                 sign;                       /* Sign flag      */
  int                 car;                        /* Character read */
  INT                 val;                        /* Value          */

  sign = 0;                                       /* Assume positive constant     */
  for ( ; ; ) {                                   /* Consume whitespaces and sign */
    car = getc (stream);
    if (isspace (car))
      continue;
    if ((car >= '0') && (car <= '9'))
      break;
    if (car == '-') {
      sign = 1;
      car  = getc (stream);
      break;
    }
    if (car == '+') {
      car = getc (stream);
      break;
    }
    return (0);
  }
  if ((car < '0') || (car > '9'))                 /* If first char is non numeric */
    return (0);                                   /* Then it is an error          */
  val = car - '0';                                /* Get first digit              */
  for ( ; ; ) {
    car = getc (stream);
    if ((car < '0') || (car > '9')) {
      ungetc (car, stream);
      break;
    }
    val = val * 10 + (car - '0');                 /* Accumulate digits */
  }
  *valptr = (sign != 0) ? (- val) : val;          /* Set result */

  return (1);
}

/* Write routine for INT values.
** It returns:
** - 1  : on success.
** - 0  : on error.
*/

int
intSave (
FILE * const                stream,               /*+ Stream to write to +*/
const INT                   val)                  /*+ Value to write     +*/
{
  return ((fprintf (stream, INTSTRING, (INT) val) == EOF) ? 0 : 1);
}

/**********************************/
/*                                */
/* Permutation building routines. */
/*                                */
/**********************************/

/* This routine fills an array with
** consecutive INT values, in
** ascending order.
** It returns:
** - VOID  : in all cases.
*/

void
intAscn (
INT * const                 permtab,              /*+ Permutation array to build +*/
const INT                   permnbr,              /*+ Number of entries in array +*/
const INT                   baseval)              /*+ Base value                 +*/
{
  INT *               permtax;
  INT                 permnum;
  INT                 permnnd;

  for (permnum = baseval, permnnd = baseval + permnbr, permtax = permtab - baseval;
       permnum < permnnd; permnum ++)
    permtax[permnum] = permnum;
}

/* This routine computes a random permutation
** of an array of INT values.
** It returns:
** - VOID  : in all cases.
*/

void
intPerm (
INT * const                 permtab,              /*+ Permutation array to build +*/
const INT                   permnbr)              /*+ Number of entries in array +*/
{
  INT *               permptr;
  INT                 permrmn;

  for (permptr = permtab, permrmn = permnbr;      /* Perform random permutation */
       permrmn > 0; permptr ++, permrmn --) {
    INT                 permnum;
    INT                 permtmp;

    permnum          = intRandVal (permrmn);      /* Select index to swap       */
    permtmp          = permptr[0];                /* Swap it with current index */
    permptr[0]       = permptr[permnum];
    permptr[permnum] = permtmp;
  }
}

/********************/
/*                  */
/* Random routines. */
/*                  */
/********************/

static volatile int intrandflag = 0;              /*+ Flag set if generator already initialized +*/
static unsigned int intrandseed = 1;              /*+ Random seed                               +*/

/* This routine initializes the pseudo-random
** generator if necessary. In order for multi-sequential
** programs to have exactly the same behavior on any
** process, the random seed does not depend on process
** rank. This routine is not really thread-safe, so it
** should not be called concurrently when it has never
** been initialized before.
** It returns:
** - VOID  : in all cases.
*/

void
intRandInit (void)
{
  if (intrandflag == 0) {                         /* If generator not yet initialized */
#if ! ((defined COMMON_DEBUG) || (defined COMMON_RANDOM_FIXED_SEED) || (defined SCOTCH_DETERMINISTIC))
    intrandseed = time (NULL);                    /* Set random seed if needed */
#endif /* ((defined COMMON_DEBUG) || (defined COMMON_RANDOM_FIXED_SEED)) */
#ifdef COMMON_RANDOM_RAND
    srand (intrandseed);
#else /* COMMON_RANDOM_RAND */
    srandom (intrandseed);
#endif /* COMMON_RANDOM_RAND */
    intrandflag = 1;                              /* Generator has been initialized */
  }
}

/* This routine reinitializes the pseudo-random
** generator to its initial value. This routine
** is not thread-safe.
** It returns:
** - VOID  : in all cases.
*/

void
intRandReset (void)
{
  if (intrandflag != 0) {                         /* Keep seed computed during first initialization */
#ifdef COMMON_RANDOM_RAND
    srand (intrandseed);
#else /* COMMON_RANDOM_RAND */
    srandom (intrandseed);
#endif /* COMMON_RANDOM_RAND */
  }
  else
    intRandInit ();
}

/*********************/
/*                   */
/* Sorting routines. */
/*                   */
/*********************/

/* This routine sorts an array of
** INT values in ascending order
** by their first value, used as key.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort1asc1
#define INTSORTSIZE                 (sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t; t = *((INT *) (p)); *((INT *) (p)) = *((INT *) (q)); *((INT *) (q)) = t; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

#define INTSORTNAME                 intSort1desc1
#define INTSORTSIZE                 (sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t; t = *((INT *) (p)); *((INT *) (p)) = *((INT *) (q)); *((INT *) (q)) = t; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) > *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of pairs of
** INT values in ascending order by their
** first value, used as key.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort2asc1
#define INTSORTSIZE                 (2 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u; t = *((INT *) (p)); u = *((INT *) (p) + 1); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of pairs of
** INT values in ascending order by both
** of their values, used as primary and
** secondary keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort2asc2
#define INTSORTSIZE                 (2 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u; t = *((INT *) (p)); u = *((INT *) (p) + 1); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 3-uples of
** INT values in ascending order by their
** first value, used as key.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort3asc1
#define INTSORTSIZE                 (3 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 3-uples of
** INT values in ascending order by their
** first and second values, used as primary
** and secondary keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort3asc2
#define INTSORTSIZE                 (3 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 3-uples of
** INT values in ascending order by their
** first, second and third values, used as primary,
** secondary and thirdly keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort3asc3
#define INTSORTSIZE                 (3 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) < *((INT *) (q) + 2))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 4-uples of
** INT values in descending order by their
** first value, used as primary
** and secondary keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort4desc1
#define INTSORTSIZE                 (4 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v;  *((INT *) (q) + 3) = w; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) > *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 4-uples of
** INT values in ascending order by their
** first value, used as primary
** and secondary keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort4asc1
#define INTSORTSIZE                 (4 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v;  *((INT *) (q) + 3) = w; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 4-uples of
** INT values in ascending order by their
** first and second values, used as primary
** and secondary keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort4asc2
#define INTSORTSIZE                 (4 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v;  *((INT *) (q) + 3) = w; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 4-uples of
** INT values in ascending order by their
** first and second values, used as primary,
** secondary and thirdly keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort4asc3
#define INTSORTSIZE                 (4 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v;  *((INT *) (q) + 3) = w; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) < *((INT *) (q) + 2))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 4-uples of
** INT values in ascending order by their
** first and second values, used as primary,
** secondary, thirdly and fourthly keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort4asc4
#define INTSORTSIZE                 (4 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v;  *((INT *) (q) + 3) = w; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) < *((INT *) (q) + 2))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) == *((INT *) (q) + 2)) && (*((INT *) (p) + 3) < *((INT *) (q) + 3))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 5-uples of
** INT values in ascending order by their
** first value, used as primary
** key.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort5asc1
#define INTSORTSIZE                 (5 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w, x; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); x = *((INT *) (p) + 4); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (p) + 4) = *((INT *) (q) + 4); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; *((INT *) (q) + 3) = w; *((INT *) (q) + 4) = x; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 5-uples of
** INT values in ascending order by their
** first and second values, used as primary,
** secondary and thirdly keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort5asc3
#define INTSORTSIZE                 (5 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w, x; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); x = *((INT *) (p) + 4); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (p) + 4) = *((INT *) (q) + 4); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; *((INT *) (q) + 3) = w; *((INT *) (q) + 4) = x; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) < *((INT *) (q) + 2))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 5-uples of
** INT values in ascending order by their
** first and second values, used as primary,
** secondary, thirdly and fourthly keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort5asc4
#define INTSORTSIZE                 (5 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w, x; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); x = *((INT *) (p) + 4); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (p) + 4) = *((INT *) (q) + 4); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; *((INT *) (q) + 3) = w; *((INT *) (q) + 4) = x; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) < *((INT *) (q) + 2))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) == *((INT *) (q) + 2)) && (*((INT *) (p) + 3) < *((INT *) (q) + 3))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 6-uples of
** INT values in ascending order by their
** first values, used as primary
** keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort6asc1
#define INTSORTSIZE                 (6 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w, x, y; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); x = *((INT *) (p) + 4); y = *((INT *) (p) + 5); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (p) + 4) = *((INT *) (q) + 4); *((INT *) (p) + 5) = *((INT *) (q) + 5); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; *((INT *) (q) + 3) = w; *((INT *) (q) + 4) = x; *((INT *) (q) + 5) = y; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 6-uples of
** INT values in ascending order by their
** first and second values, used as primary,
** secondary and thirdly keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort6asc3
#define INTSORTSIZE                 (6 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w, x, y; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); x = *((INT *) (p) + 4); y = *((INT *) (p) + 5); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (p) + 4) = *((INT *) (q) + 4); *((INT *) (p) + 5) = *((INT *) (q) + 5); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; *((INT *) (q) + 3) = w; *((INT *) (q) + 4) = x; *((INT *) (q) + 5) = y; } while (0)
#define INTSORTCMP(p,q)             ((*((INT *) (p)) < *((INT *) (q))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) < *((INT *) (q) + 1))) || ((*((INT *) (p)) == *((INT *) (q))) && (*((INT *) (p) + 1) == *((INT *) (q) + 1)) && (*((INT *) (p) + 2) < *((INT *) (q) + 2))))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

/* This routine sorts an array of 7-uples of
** INT values in ascending order by their
** first values, used as primary
** keys.
** It returns:
** - VOID  : in all cases.
*/

#define INTSORTNAME                 intSort7asc1
#define INTSORTSIZE                 (7 * sizeof (INT))
#define INTSORTSWAP(p,q)            do { INT t, u, v, w, x, y, z; t = *((INT *) (p)); u = *((INT *) (p) + 1); v = *((INT *) (p) + 2); w = *((INT *) (p) + 3); x = *((INT *) (p) + 4); y = *((INT *) (p) + 5); z = *((INT *) (p) + 6); *((INT *) (p)) = *((INT *) (q)); *((INT *) (p) + 1) = *((INT *) (q) + 1); *((INT *) (p) + 2) = *((INT *) (q) + 2); *((INT *) (p) + 3) = *((INT *) (q) + 3); *((INT *) (p) + 4) = *((INT *) (q) + 4); *((INT *) (p) + 5) = *((INT *) (q) + 5); *((INT *) (q)) = t; *((INT *) (q) + 1) = u; *((INT *) (q) + 2) = v; *((INT *) (q) + 3) = w; *((INT *) (q) + 4) = x; *((INT *) (q) + 5) = y; *((INT *) (q) + 6) = z; } while (0)
#define INTSORTCMP(p,q)             (*((INT *) (p)) < *((INT *) (q)))
#include "common_sort.c"
#undef INTSORTNAME
#undef INTSORTSIZE
#undef INTSORTSWAP
#undef INTSORTCMP

