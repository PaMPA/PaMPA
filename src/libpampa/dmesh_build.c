/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_build.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the distributed source
//!                mesh building routines. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define DMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_build.h"
#include "pampa.h"
#include "dmesh_ghst.h"

//! \brief This routine builds a distributed mesh from
//! the local arrays that are passed to it.
//! As for all routines that build meshes, the private
//! fields of the Dmesh structure have to be initialized
//! if they are not already.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.


int
dmeshBuild (
  Dmesh * restrict const    meshptr,              /**< mesh */
  const Gnum                baseval,              /**< Base for indexing */
  const Gnum                vertlocnbr,           /**< Number of local vertices */
  const Gnum                vertlocmax,           /**< Number of local vertices */
  Gnum * const              vertloctax,           /**< Local vertex begin array */
  Gnum * const              vendloctax,           /**< Local vertex end array */
  const Gnum                edgelocnbr,           /**< Number of local edges */
  const Gnum                edgelocsiz,           /**< Size of local edges */
  Gnum * restrict const     edgeloctax,           /**< Local edge array */
  Gnum * restrict const     edloloctax,           /**< Local edge load array (if any) */
  Gnum                      enttglbnbr,           /**< Number of global entities */
  Gnum *                    ventloctax,           /**< Local vertex entity array */
  Gnum *                    esubloctax,           /**< Local sub-entity array */
  Gnum                      valuglbmax,           /**< Maximum number of values over all processes */
  const Gnum                ovlpglbval,           /**< Global overlap value */
  Dvalues *                 valslocptr,           //!< Pointer on attached values (if any)
  Gnum                      degrlocmax,           //!< Maximum degree (if any, else ~0)
  Gnum *                    procvrttab,           //!< Global array of vertex number ranges [1,based] (if any)
  Gnum *                    proccnttab,           //!< Count array for local number of vertices (if any)
  Gnum *                    procdsptab,           //!< Displacement array with respect to proccnttab [1,based] (if any)
  int                       procngbnbr,           //!< Number of neighboring processes (if any, else ~0)
  int                       procngbmax,           //!< Maximum number of neighboring processes (if any, else ~0)
  int *                     procngbtab)           //!< Array of neighbor process numbers [sorted] (if any)
{
  static Gnum           enttsingle = -1;
  Gnum                  ovlpglbvl2;
  Gnum                  esubglbnbr;
  Gnum                  vgstlocnbr;
  Gnum                  vertovpnbr;               // \todo change by vovplocnbr instead of vertovpnbr, same for others
  Gnum                  edgeovpnbr;
  Gnum                  vertlocnum;
  Gnum                  vertlocnnd;
  Gnum                  ventlocnum;
  Gnum                  ventlocnnd;
  Gnum                  vertlocbas;
  Gnum * restrict       esublocbax;
  Gnum * restrict       vnbrloctax;
  Gnum * restrict       vnbrglbtax;
  DmeshBuildGhstVert * restrict hashtab;          /* Extended vertex array */
  DmeshBuildProcVert * restrict procvertloctab;
  Gnum * restrict       ventgsttax;               /* TRICK: ventgsttax is in fact vsubgsttax if sub-entities and overlap */
  Gnum * restrict       vertgsttax;
  Gnum * restrict       edgegsttax;
  Gnum * restrict       vertovptax;
  Gnum * restrict       vendovptax;
  Gnum * restrict       vnumovptax;
  Gnum * restrict       vertsortloctab;
  Gnum * restrict       edgeloctmp;
  Gnum * restrict       edgegsttmp;
  Gnum * restrict       evrtgsttax;
  Gnum * restrict       svrtgsttax;
  Gnum * restrict       snumloctax;
  DmeshEntity * restrict * enttloctax;
  int                   cheklocval;
#ifdef PAMPA_DEBUG_REDUCE
  int                   chekglbval;
#endif /* PAMPA_DEBUG_REDUCE */

  meshptr->baseval    = baseval;    /* set global and local data */
  ovlpglbvl2 = ~PAMPA_OVLP_NOGT & ovlpglbval;
  // \todo test in debug mode if overlap is the same on all processors

  cheklocval = 0;

  /* If there are sub-entities */
  if (esubloctax != NULL) {
#ifdef PAMPA_DEBUG_DMESH
    /* Check if ventloctax is correctly filled by sub-entities instead of
     * super-entities */
    for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
      if ((ventloctax[vertlocnum] != PAMPA_ENTT_DUMMY) && (esubloctax[ventloctax[vertlocnum]] < PAMPA_ENTT_SINGLE)) { // \warn PAMPA_ENTT_DUMMY not tested with meshBuild
        errorPrint ("Entity %d cannot be in ventloctab for vertex number %d. Sub-entity number must be present", ventloctax[vertlocnum], vertlocnum);
        cheklocval = 1;
      }
    CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

    esubglbnbr = enttglbnbr;
    if ((esublocbax = (Gnum *) memAlloc (enttglbnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      memFree     (esublocbax);
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    esublocbax -= baseval;

    memCpy (esublocbax + baseval, esubloctax + baseval, enttglbnbr * sizeof (Gnum));
    meshptr->esublocmsk = ~((Gnum) 0);
  }
  /* There are no sub-entities */
  else {
    esubglbnbr = 0;
    esublocbax = &enttsingle;
    meshptr->esublocmsk = 0;
  }
  meshptr->esublocbax = (Gnum *) esublocbax;


  /* If private arrays are already allocated */
  if ((meshptr->flagval & DMESHFREEPRIV) == 0) {
    if ((procvrttab == NULL) || (ovlpglbvl2 > 0)) { //! \warning why overlap is needed?
      CHECK_FERR(dmeshBuild2 (meshptr, baseval, vertlocnbr, vertlocmax), meshptr->proccomm);
    }
    else {
      (*meshptr->procloccnt) ++;
      meshptr->flagval |= DMESHFREEPRIV;
      meshptr->procvrttab = procvrttab;
      meshptr->proccnttab = proccnttab;
      meshptr->procdsptab = procdsptab;
      meshptr->procngbnbr = procngbnbr;
      meshptr->procngbmax = procngbmax;
      meshptr->procngbtab = procngbtab;
    }
  }


  meshptr->edgelocsiz = edgelocsiz;
  meshptr->edgelocnbr = edgelocnbr;
  meshptr->enttglbnbr = enttglbnbr;
  meshptr->vertlocnbr = vertlocnbr;
  meshptr->vertlocmax = vertlocmax;

//! \todo this following test is too long, another flag must be used
//#ifdef PAMPA_DEBUG_DMESH
//  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
//      vertlocnum < vertlocnnd; vertlocnum ++) {
//    Gnum vertlocnm2;
//    Gnum vertlocnn2;
//    for (vertlocnm2 = baseval, vertlocnn2 = vertlocnbr + baseval;
//        vertlocnm2 < vertlocnn2; vertlocnm2 ++) {
//      if ((vertlocnum != vertlocnm2) && (vertloctax[vertlocnum] < vendloctax[vertlocnm2]) && (vendloctax[vertlocnum] > vertloctax[vertlocnm2])) {
//        errorPrint ("Overlap of adjacency between %d and %d", vertlocnum, vertlocnm2);
//        cheklocval = 1;
//      }
//    }
//  }
//  CHECK_VERR (cheklocval, meshptr->proccomm);
//#endif /* PAMPA_DEBUG_DMESH */

  /* Compute local and global number of vertices for each entity */
  CHECK_FERR (dmeshBuild4 (meshptr, vertlocnbr, ventloctax, &vnbrloctax, &vnbrglbtax), meshptr->proccomm);
  CHECK_FERR (dmeshBuild3 (meshptr, baseval,
                            vertlocnbr, vertlocmax, vertloctax, vendloctax,
                            edgelocsiz, edgeloctax, ventloctax, ovlpglbvl2,
                            &vgstlocnbr, &hashtab, &procvertloctab, &ventgsttax, &vertgsttax, &edgegsttax), meshptr->proccomm);

  if (ovlpglbvl2 > 0) {                           /* If overlap */
    Gnum              vertovpnum;
    Gnum              vertovpnnd;

    degrlocmax = 0;                      /* Determine the local maximum degree */
    for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++) {
      if (vendloctax[vertlocnum] - vertloctax[vertlocnum] > degrlocmax)
        degrlocmax = vendloctax[vertlocnum] - vertloctax[vertlocnum];
    }
    meshptr->degrlocmax = degrlocmax;

    /* Add vertices and edges corresponding to the overlap, compute
     * communication structures and fill in the edgegsttax */
    CHECK_FERR (dmeshOverlap (meshptr, vertlocnbr, vertloctax, vendloctax,
                               edgelocsiz, edgeloctax, ventloctax,
                               &ventgsttax, &vertovpnbr, &vertovptax, &vendovptax, &vnumovptax,
                               &edgeovpnbr, &edgegsttax, ovlpglbval), meshptr->proccomm);

    vgstlocnbr = vertlocnbr + vertovpnbr;

    /* Finish to determine the local maximum degree, including overlap */
    for (vertovpnum = baseval, vertovpnnd = vertovpnbr + baseval;
        vertovpnum < vertovpnnd; vertovpnum ++)  {
      if (vendovptax[vertovpnum] - vertovptax[vertovpnum] > degrlocmax)
        degrlocmax = vendovptax[vertovpnum] - vertovptax[vertovpnum];
    }
    meshptr->degrlocmax = degrlocmax;

 /* Allocate memory for intermediate data */
    if (memAllocGroup ((void **) (void *)
                       &vertsortloctab, (size_t) ((meshptr->degrlocmax * 2) * sizeof (Gnum)),
                       &edgeloctmp,     (size_t) (meshptr->degrlocmax * sizeof (Gnum)),
                       &edgegsttmp,     (size_t) (meshptr->degrlocmax * sizeof (Gnum)),
                       &evrtgsttax,     (size_t) (vgstlocnbr * sizeof (Gnum)),
                       &svrtgsttax,     (size_t) (vgstlocnbr * sizeof (Gnum)),
                       &snumloctax ,    (size_t) (esubglbnbr * sizeof (Gnum)), (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    evrtgsttax -= baseval;
    svrtgsttax -= baseval;
    snumloctax -= baseval;

    meshptr->edgelocsiz += edgeovpnbr;
    meshptr->edgelocnbr += edgeovpnbr;
  }
  else {                                          /* ovlpglbvl2 = 0                        */
    if (memAllocGroup ((void **) (void *)         /* Allocate memory for intermediate data */
                       &vertsortloctab, (size_t) ((meshptr->degrlocmax * 2) * sizeof (Gnum)),
                       &edgeloctmp,     (size_t) (meshptr->degrlocmax * sizeof (Gnum)),
                       &edgegsttmp,     (size_t) (meshptr->degrlocmax * sizeof (Gnum)),
                       &evrtgsttax,     (size_t) (vgstlocnbr * sizeof (Gnum)),
                       &svrtgsttax,     (size_t) (vgstlocnbr * sizeof (Gnum)),
                       &snumloctax,     (size_t) (esubglbnbr * sizeof (Gnum)), (void *) NULL) == NULL) {
      errorPrint ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    evrtgsttax -= baseval;
    svrtgsttax -= baseval;
    snumloctax -= baseval;

  }

  /* Reallocate memory, edgelocsiz is yet known */
#ifndef PAMPA_DEBUG_MEM
  if (memReallocGroup ((void *) (meshptr->enttloctax + baseval),
                       &meshptr->enttloctax, (size_t) (enttglbnbr * sizeof (DmeshEntity *)),
                       &meshptr->edgeloctax, (size_t) (meshptr->edgelocsiz * sizeof (Gnum)), (void *) NULL) == NULL) {
#else /* PAMPA_DEBUG_MEM */
  meshptr->enttloctax += baseval;
  meshptr->edgeloctax += baseval;
  if (memReallocGroup ((void *) meshptr->enttloctax,
                       &meshptr->enttloctax, (size_t) (enttglbnbr * sizeof (DmeshEntity *)),
                       &meshptr->edgeloctax, (size_t) (meshptr->edgelocsiz * sizeof (Gnum)), (void *) NULL) == NULL) {
#endif /* PAMPA_DEBUG_MEM */

    errorPrint ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  meshptr->enttloctax -= baseval;
  meshptr->edgeloctax -= baseval;
  enttloctax = meshptr->enttloctax;

  /* Compute the sub-entity number for each vertex */
  if (esubloctax != NULL) { /* If there are sub-entities */
    for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
      Gnum      esublocnum;

      if (enttloctax[ventlocnum] == NULL)
        continue;

      enttloctax[ventlocnum]->vertlocnbr = baseval;
      esublocnum = esubloctax[ventlocnum];
      snumloctax[ventlocnum] = ((esublocnum > PAMPA_ENTT_SINGLE) && (esubloctax[esublocnum] == PAMPA_ENTT_STABLE))
                               ? esublocnum : ventlocnum;
    }

    for (vertlocnum = baseval, vertlocnnd = vgstlocnbr + baseval;
         vertlocnum < vertlocnnd; vertlocnum ++)
      if (ventgsttax[vertlocnum] != PAMPA_ENTT_DUMMY)
        svrtgsttax[vertlocnum] = snumloctax[ventgsttax[vertlocnum]];
      else
        svrtgsttax[vertlocnum] = PAMPA_ENTT_DUMMY;
  }
  else {                                          /* If there is no sub-entity */
    for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
      if (enttloctax[ventlocnum] == NULL)
        continue;

      enttloctax[ventlocnum]->vertlocnbr = baseval;
    }

    svrtgsttax = ventgsttax;
  }

  /* Sort edge{loc,gst}tax and edloloctax by each entity of neighbors for local
   * vertices */
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    Gnum  edgelocnum;
    Gnum  num;
    Gnum  siz;

    /* If vertex belongs to dummy entity (not used) */
    if (ventgsttax[vertlocnum] == PAMPA_ENTT_DUMMY)
      continue;
    evrtgsttax[vertlocnum] = enttloctax[ventgsttax[vertlocnum]]->vertlocnbr++;

    /* The following test could be put in an entity loop instead of this vertex loop */
    if ((ventloctax[vertlocnum] != PAMPA_ENTT_DUMMY) && (esublocbax[ventloctax[vertlocnum] & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      evrtgsttax[vertlocnum] = enttloctax[svrtgsttax[vertlocnum]]->vertlocnbr ++;

    /* Loop over each neighbors of vertlocnum */
    for (edgelocnum = vertloctax[vertlocnum], num = 0 ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++, num ++) {
#ifdef PAMPA_DEBUG_DMESH
      if ((svrtgsttax[edgegsttax[edgelocnum]] < baseval) || (svrtgsttax[edgegsttax[edgelocnum]] >= (enttglbnbr + baseval))) {
        errorPrint ("invalid neighbor" GNUMSTRING "with invalid entity" GNUMSTRING "for vertex" GNUMSTRING "\n", edgegsttax[edgelocnum], svrtgsttax[edgegsttax[edgelocnum]], vertlocnum);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
      vertsortloctab[2 * num] = svrtgsttax[edgegsttax[edgelocnum]];
      vertsortloctab[2 * num + 1] = num;
    }


    siz = vendloctax[vertlocnum] - vertloctax[vertlocnum];
    /* Neighbors are sorted according to their number and also their entity */
    intSort2asc2(vertsortloctab, siz);


    memCpy (edgeloctmp, edgeloctax + vertloctax[vertlocnum], siz * sizeof (Gnum));
    memCpy (edgegsttmp, edgegsttax + vertloctax[vertlocnum], siz * sizeof (Gnum));

    /* Adjacency list is copied */
    for (edgelocnum = vertloctax[vertlocnum], num = 1;
        edgelocnum < vendloctax[vertlocnum];
        edgelocnum ++, num += 2) {
      edgeloctax[edgelocnum] = edgeloctmp[vertsortloctab[num]];
      edgegsttax[edgelocnum] = edgegsttmp[vertsortloctab[num]];
    }

    if (edloloctax != NULL) {
      memCpy (edgeloctmp, edloloctax+vertloctax[vertlocnum], siz * sizeof (Gnum));
      for (edgelocnum = vertloctax[vertlocnum], num = 1;
          edgelocnum < vendloctax[vertlocnum];
          edgelocnum ++, num += 2) {
        edloloctax[edgelocnum] = edgeloctmp[vertsortloctab[num]];
      }
    }
  }


  /* Sort edgegsttax by each entity of neighbors for overlap vertices */
  if (ovlpglbvl2 > 0) {                           /* If overlap */
    Gnum                vertovpnum;
    Gnum                vertovpnnd;

    for (vertovpnum = baseval, vertovpnnd = vertovpnbr + baseval;
        vertovpnum < vertovpnnd; vertovpnum ++) {
      if (vertovptax[vertovpnum] < vendovptax[vertovpnum]) {
        Gnum              edgeovpnum;
        Gnum              edgeovpnnd;
        Gnum              num;
        Gnum              siz;

        for (edgeovpnum = vertovptax[vertovpnum], edgeovpnnd = vendovptax[vertovpnum], num = 0;
             edgeovpnum < edgeovpnnd; edgeovpnum ++, num ++) {
          vertsortloctab[2 * num] = svrtgsttax[edgegsttax[edgeovpnum]];
          vertsortloctab[2 * num + 1] = num;
        }

        siz = vendovptax[vertovpnum] - vertovptax[vertovpnum];
        intSort2asc2 (vertsortloctab, siz);

        memCpy (edgeloctmp, edgegsttax+vertovptax[vertovpnum], siz * sizeof (Gnum));
        for (edgeovpnum = vertovptax[vertovpnum], num = 1;
             edgeovpnum < vendovptax[vertovpnum]; edgeovpnum ++, num += 2)
          edgegsttax[edgeovpnum] = edgeloctmp[vertsortloctab[num]];
      }
    }

    for (vertlocnum = vertlocnbr + baseval, vertlocnnd = vgstlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++)  {
      ventlocnum = ventgsttax[vertlocnum];
      vnbrloctax[ventlocnum] ++;
      if (esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
        vnbrloctax[esubloctax[ventlocnum]] ++;
    }
  }

  /* Put local vertex counter to baseval for each entity */
  for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
    if (enttloctax[ventlocnum] == NULL)
      continue;


    enttloctax[ventlocnum]->vgstlocnbr = enttloctax[ventlocnum]->vertlocnbr;
    if (esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
      enttloctax[ventlocnum]->vertlocnbr = baseval; // TRICK: vertlocnbr for sub-entities will not be modified

  }





  /* Determine ghost vertex counter for each entity and sub-entity */
  for (vertlocnum = vertlocnbr + baseval, vertlocnnd = vgstlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {

    if (ventgsttax[vertlocnum] == PAMPA_ENTT_DUMMY)
      continue;
    evrtgsttax[vertlocnum] = enttloctax[ventgsttax[vertlocnum]]->vgstlocnbr++;
    /* The following test could be put in an entity loop instead of this vertex loop */
    if (esublocbax[ventgsttax[vertlocnum] & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
      evrtgsttax[vertlocnum] = enttloctax[svrtgsttax[vertlocnum]]->vgstlocnbr ++;

  }

  /* Allocate and initialize neighbor data for each not null entity */
  for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {

    if (enttloctax[ventlocnum] == NULL)
      continue;

    //! \attention If PAMPA_ENTT_UNSTABLE exists, be careful with the following
    //! test
    switch (esublocbax[ventlocnum & meshptr->esublocmsk]) {

      case PAMPA_ENTT_STABLE : /* Entity with sub-entities and sorting is stable inside the entity */

#ifdef PAMPA_DEBUG_MEM
        enttloctax[ventlocnum]->nghbloctax = NULL;
          enttloctax[ventlocnum]->mvrtloctax = 
          enttloctax[ventlocnum]->ventloctax = 
          enttloctax[ventlocnum]->svrtloctax = NULL;
#endif /* PAMPA_DEBUG_MEM */
        /* Allocate private entity neighbors data*/
        if (memReallocGroup ((void *) enttloctax[ventlocnum]->commlocdat.procrcvtab,
              &enttloctax[ventlocnum]->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
              &enttloctax[ventlocnum]->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
              &enttloctax[ventlocnum]->nghbloctax,    (size_t) (enttglbnbr * sizeof (DmeshEnttNghb *)),
              &enttloctax[ventlocnum]->mvrtloctax, (size_t) (enttloctax[ventlocnum]->vgstlocnbr * sizeof (Gnum)),
              &enttloctax[ventlocnum]->ventloctax,    (size_t) (enttloctax[ventlocnum]->vgstlocnbr * sizeof (Gnum)),
              &enttloctax[ventlocnum]->svrtloctax, (size_t) (enttloctax[ventlocnum]->vgstlocnbr * sizeof (Gnum)),
              (void *) NULL) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);

        enttloctax[ventlocnum]->nghbloctax -= baseval;
        enttloctax[ventlocnum]->mvrtloctax -= baseval;
        enttloctax[ventlocnum]->ventloctax -= baseval;
        enttloctax[ventlocnum]->svrtloctax -= baseval;
        break;

      case PAMPA_ENTT_SINGLE : /* Entity without sub-entities */

#ifdef PAMPA_DEBUG_MEM
        enttloctax[ventlocnum]->nghbloctax = NULL;
          enttloctax[ventlocnum]->mvrtloctax = NULL;
#endif /* PAMPA_DEBUG_MEM */
        /* Allocate private entity neighbors data*/
        if (memReallocGroup ((void *) enttloctax[ventlocnum]->commlocdat.procrcvtab,
              &enttloctax[ventlocnum]->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
              &enttloctax[ventlocnum]->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
              &enttloctax[ventlocnum]->nghbloctax, (size_t) (enttglbnbr * sizeof (DmeshEnttNghb *)),
              &enttloctax[ventlocnum]->mvrtloctax, (size_t) (enttloctax[ventlocnum]->vgstlocnbr * sizeof (Gnum)),
              (void *) NULL) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);

        enttloctax[ventlocnum]->nghbloctax -= baseval;
        enttloctax[ventlocnum]->mvrtloctax -= baseval;
        enttloctax[ventlocnum]->ventloctax = NULL;
        enttloctax[ventlocnum]->svrtloctax = NULL;
        break;

      default :

#ifdef PAMPA_DEBUG_MEM
        enttloctax[ventlocnum]->ventloctax = 
          enttloctax[ventlocnum]->svrtloctax = NULL;
#endif /* PAMPA_DEBUG_MEM */
        /* Allocate private entity neighbors data*/
        if (memReallocGroup ((void *) enttloctax[ventlocnum]->commlocdat.procrcvtab,
              &enttloctax[ventlocnum]->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
              &enttloctax[ventlocnum]->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
              &enttloctax[ventlocnum]->ventloctax,    (size_t) (enttloctax[ventlocnum]->vgstlocnbr * sizeof (Gnum)),
              &enttloctax[ventlocnum]->svrtloctax, (size_t) (enttloctax[ventlocnum]->vgstlocnbr * sizeof (Gnum)),
              (void *) NULL) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);

        enttloctax[ventlocnum]->mvrtloctax = NULL;
        enttloctax[ventlocnum]->ventloctax -= baseval;
        enttloctax[ventlocnum]->svrtloctax -= baseval;

    }


    enttloctax[ventlocnum]->vgstlocnbr = baseval; // TRICK: baseval will be removed later

    //! \attention If PAMPA_ENTT_UNSTABLE exists, be careful with the following
    //! test
    if (esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { /* If ventlocnum is not a sub-entity */
      DmeshEnttNghb * restrict * nghbloctax;
   Gnum        nentlocnum;
   Gnum        nentlocnnd;


      nghbloctax = enttloctax[ventlocnum]->nghbloctax;
      for (nentlocnum = baseval, nentlocnnd = enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++) {
        if ((vnbrglbtax[nentlocnum] == 0) && (nentlocnum != baseval) && (nentlocnum != enttglbnbr - 1 + baseval))
          nghbloctax[nentlocnum] = NULL;
        else {
          if ((nghbloctax[nentlocnum] = (DmeshEnttNghb *) memAlloc (sizeof (DmeshEnttNghb))) == NULL) {
            errorPrint  ("out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          if (esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {
            /* Allocate distributed entity private data */
            if (memAllocGroup ((void **) (void *)
                  &nghbloctax[nentlocnum]->vindloctax, (size_t) (vnbrloctax[ventlocnum] * sizeof (DmeshVertBounds)),
                  (void *) NULL) == NULL) {
              errorPrint  ("out of memory");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);

            nghbloctax[nentlocnum]->vindloctax -= baseval;

            memSet (nghbloctax[nentlocnum]->vindloctax + baseval, 0, vnbrloctax[ventlocnum] * sizeof (DmeshVertBounds));

          }
        }
      }
    }
  }

  if (esubloctax != NULL) { /* If there are sub-entities */
    /* Redirect private neighbor data for each sub-entity of verctices or neighbors */
    for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
      Gnum nentlocnum;
      Gnum nentlocnnd;

      if (enttloctax[ventlocnum] == NULL)
        continue;

      if (esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
        enttloctax[ventlocnum]->nghbloctax = enttloctax[snumloctax[ventlocnum]]->nghbloctax;
        enttloctax[ventlocnum]->mvrtloctax = enttloctax[snumloctax[ventlocnum]]->mvrtloctax;
      }

      DmeshEnttNghb * restrict * nghbloctax;

      nghbloctax = enttloctax[ventlocnum]->nghbloctax;

      for (nentlocnum = baseval, nentlocnnd = enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++) {
        if (enttloctax[nentlocnum] == NULL)
          continue;

        if (esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
          nghbloctax[nentlocnum]->vindloctax = nghbloctax[snumloctax[nentlocnum]]->vindloctax;
        }
      }
    }
  }

  /* Compute beginning and ending in the adjacency list for each local vertex and each
   * entity of neighbors */
  vertlocbas = meshptr->procvrttab[meshptr->proclocnum] - baseval;
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    DmeshEnttNghb * restrict * nghbloctax;
 Gnum        evrtlocnum;
 Gnum        edgelocnum;
 Gnum        nentlocnum;

    ventlocnum = svrtgsttax[vertlocnum];
    if (ventlocnum == PAMPA_ENTT_DUMMY)
      continue;
    nentlocnum = svrtgsttax[edgegsttax[vertloctax[vertlocnum]]];

    enttloctax[ventlocnum]->mvrtloctax[enttloctax[ventlocnum]->vertlocnbr ++] = vertlocbas + vertlocnum;
#ifdef PAMPA_DEBUG_DMESH
    if ((ventlocnum < 0) || (ventlocnum > enttglbnbr)
        || (nentlocnum < 0) || (nentlocnum > enttglbnbr)) {
      errorPrint("invalid vertex entity number");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

    nghbloctax = enttloctax[ventlocnum]->nghbloctax;
    evrtlocnum = enttloctax[ventlocnum]->vgstlocnbr;

    nghbloctax[baseval]->vindloctax[evrtlocnum].vertlocidx = vertloctax[vertlocnum];
    if (nentlocnum != baseval)
      nghbloctax[baseval]->vindloctax[evrtlocnum].vendlocidx = vertloctax[vertlocnum];

    nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vertlocidx = vertloctax[vertlocnum];

    for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {
      if (nentlocnum != svrtgsttax[edgegsttax[edgelocnum]]) {
        nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vendlocidx = edgelocnum;
        nentlocnum = svrtgsttax[edgegsttax[edgelocnum]];
#ifdef PAMPA_DEBUG_DMESH
        if ((nentlocnum < 0) || (nentlocnum > enttglbnbr)) {
          errorPrint("invalid vertex entity number");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

        nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vertlocidx = edgelocnum;
      }

      meshptr->edgeloctax[edgelocnum] = evrtgsttax[edgegsttax[edgelocnum]];

    }

    nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vendlocidx = edgelocnum;
    nghbloctax[enttglbnbr - 1 + baseval]->vindloctax[evrtlocnum].vendlocidx = edgelocnum;
    if (nentlocnum != (enttglbnbr - 1 + baseval))
      nghbloctax[enttglbnbr - 1 + baseval]->vindloctax[evrtlocnum].vertlocidx = vendloctax[vertlocnum];


    /* Increment ghost vertex number for entity and sub-entity */
    if (esublocbax[ventlocnum & meshptr->esublocmsk] != PAMPA_ENTT_SINGLE) { /* If stable entity or sub-entity */
   Gnum  vsublocnum;

      ventlocnum = ventgsttax[vertlocnum];
      vsublocnum = svrtgsttax[vertlocnum];

      enttloctax[ventlocnum]->svrtloctax[enttloctax[ventlocnum]->vgstlocnbr] = enttloctax[vsublocnum]->vgstlocnbr;
      enttloctax[ventlocnum]->ventloctax[enttloctax[ventlocnum]->vgstlocnbr] = vsublocnum;

      enttloctax[vsublocnum]->ventloctax[enttloctax[vsublocnum]->vgstlocnbr] = ventlocnum;
      enttloctax[vsublocnum]->svrtloctax[enttloctax[vsublocnum]->vgstlocnbr ++] = enttloctax[ventlocnum]->vgstlocnbr ++;


    }
    else { /* If simple entity */
      enttloctax[ventlocnum]->vgstlocnbr ++;
    }

  }





  if (ovlpglbvl2 > 0) { /* If overlap */
 Gnum  vertovpnum;
 Gnum  vertovpnnd;

  /* Compute beginning and ending in the adjacency list for each overlap vertex and each
   * entity of neighbors */
    for (vertovpnum = baseval, vertovpnnd = vertovpnbr + baseval;
        vertovpnum < vertovpnnd; vertovpnum ++)  {

      DmeshEnttNghb * restrict * nghbloctax;
   Gnum        nentlocnum;
   Gnum        evrtlocnum;
   Gnum        edgelocnum;
   Gnum        edgelocnnd;

      ventlocnum = svrtgsttax[vertlocnbr + vertovpnum];
	  if (vertovptax[vertovpnum] != vendovptax[vertovpnum])
      	nentlocnum = svrtgsttax[edgegsttax[vertovptax[vertovpnum]]];
	  else
		nentlocnum = baseval;
#ifdef PAMPA_DEBUG_DMESH
      if ((ventlocnum < baseval) || (ventlocnum > enttglbnbr + baseval)) {
        errorPrint("invalid vertex entity number %d for vertex %d", ventlocnum, vertovptax[vertovpnum]);
        cheklocval = 1;
      }
      if ((nentlocnum < baseval) || (nentlocnum > enttglbnbr + baseval)) {
        errorPrint("invalid vertex entity number %d for vertex %d", nentlocnum, edgegsttax[vertovptax[vertovpnum]]);
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

      nghbloctax = enttloctax[ventlocnum]->nghbloctax;
      evrtlocnum = enttloctax[ventlocnum]->vgstlocnbr;

      nghbloctax[baseval]->vindloctax[evrtlocnum].vertlocidx = vertovptax[vertovpnum];
      if (nentlocnum != baseval)
        nghbloctax[baseval]->vindloctax[evrtlocnum].vendlocidx = vertovptax[vertovpnum];

      nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vertlocidx = vertovptax[vertovpnum];

      for (edgelocnum = vertovptax[vertovpnum], edgelocnnd = vendovptax[vertovpnum]; edgelocnum < edgelocnnd ; edgelocnum ++) {
        if (nentlocnum != svrtgsttax[edgegsttax[edgelocnum]]) {
          nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vendlocidx = edgelocnum;
          nentlocnum = svrtgsttax[edgegsttax[edgelocnum]];
#ifdef PAMPA_DEBUG_DMESH
          if ((nentlocnum < baseval) || (nentlocnum > enttglbnbr + baseval)) {
            errorPrint("invalid vertex entity number %d for vertex %d", nentlocnum, edgegsttax[vertovptax[vertovpnum]]);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

          nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vertlocidx = edgelocnum;
        }

        meshptr->edgeloctax[edgelocnum] = evrtgsttax[edgegsttax[edgelocnum]];

      }

      nghbloctax[nentlocnum]->vindloctax[evrtlocnum].vendlocidx = edgelocnum;
      nghbloctax[enttglbnbr - 1 + baseval]->vindloctax[evrtlocnum].vendlocidx = edgelocnum;
      if (nentlocnum != (enttglbnbr - 1 + baseval))
        nghbloctax[enttglbnbr - 1 + baseval]->vindloctax[evrtlocnum].vertlocidx = vendovptax[vertovpnum];


      enttloctax[ventlocnum]->mvrtloctax[enttloctax[ventlocnum]->vgstlocnbr] = vnumovptax[vertovpnum];

   /* Increment ghost vertex number for entity and sub-entity */
      if (esublocbax[ventlocnum & meshptr->esublocmsk] != PAMPA_ENTT_SINGLE) { /* If stable entity or sub-entity */
  Gnum  vsublocnum;

        vertlocnum = vertlocnbr + vertovpnum;
        ventlocnum = ventgsttax[vertlocnum];
        vsublocnum = svrtgsttax[vertlocnum];

        enttloctax[ventlocnum]->svrtloctax[enttloctax[ventlocnum]->vgstlocnbr] = enttloctax[vsublocnum]->vgstlocnbr;
        enttloctax[ventlocnum]->ventloctax[enttloctax[ventlocnum]->vgstlocnbr] = vsublocnum;

        enttloctax[vsublocnum]->ventloctax[enttloctax[vsublocnum]->vgstlocnbr] = ventlocnum;
        enttloctax[vsublocnum]->svrtloctax[enttloctax[vsublocnum]->vgstlocnbr ++] = enttloctax[ventlocnum]->vgstlocnbr ++;


      }
      else { /* If simple entity */
        enttloctax[ventlocnum]->vgstlocnbr ++;
      }

    }
  }






  if (ovlpglbvl2 == 0) { /* If no overlap */
    Gnum  procvertlocbas;

    procvertlocbas = vertlocnbr + baseval;
    /* Updating entity number for ghst vertices */
    for (vertlocnum = vertlocnbr + baseval, vertlocnnd = vgstlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++)  {

   Gnum  vsublocnum;

      vsublocnum = svrtgsttax[vertlocnum];
      if (vsublocnum != PAMPA_ENTT_DUMMY) {
        enttloctax[vsublocnum]->mvrtloctax[enttloctax[vsublocnum]->vgstlocnbr] = procvertloctab[vertlocnum - procvertlocbas].mvrtnum;

        if (esublocbax[vsublocnum & meshptr->esublocmsk] != PAMPA_ENTT_SINGLE) { /* If stable entity or sub-entity */
          ventlocnum = ventgsttax[vertlocnum];

          enttloctax[ventlocnum]->svrtloctax[enttloctax[ventlocnum]->vgstlocnbr] = enttloctax[vsublocnum]->vgstlocnbr;
          enttloctax[ventlocnum]->ventloctax[enttloctax[ventlocnum]->vgstlocnbr] = vsublocnum;

          enttloctax[vsublocnum]->ventloctax[enttloctax[vsublocnum]->vgstlocnbr] = ventlocnum;
          enttloctax[vsublocnum]->svrtloctax[enttloctax[vsublocnum]->vgstlocnbr ++] = enttloctax[ventlocnum]->vgstlocnbr ++;


        }
        else { /* If simple entity */
          enttloctax[vsublocnum]->vgstlocnbr ++;
        }
      }

    }
  }
  memFreeGroup (hashtab);

  /* Removing baseval form {vert,vgst}locnbr for each entity */
  meshptr->vtrulocnbr = 0;
  for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
    if (enttloctax[ventlocnum] == NULL)
      continue;

    enttloctax[ventlocnum]->vertlocnbr -= baseval; // TRICK: baseval has been added
    enttloctax[ventlocnum]->vgstlocnbr -= baseval; // TRICK: baseval has been added
    if (esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) /* If not sub-entity */
      meshptr->vtrulocnbr += enttloctax[ventlocnum]->vertlocnbr;
  }


  /* This macro is used when dmeshGhst failed */
#ifndef PAMPA_DEBUG_NO_HALO
  /* Compute commmunication structure if no overlap */
  if (ovlpglbvl2 == 0) {
    CHECK_FERR (dmeshGhst(meshptr, vertlocnbr, vgstlocnbr, vertloctax, vendloctax, edgeloctax, ventgsttax, vertgsttax), meshptr->proccomm);
  }

  /* Compute communication structure for stable entities */
  CHECK_FERR (dmeshGhst2(meshptr), meshptr->proccomm);
#endif /* PAMPA_DEBUG_NO_HALO */

  meshptr->vertlocmax = vertlocmax;
  meshptr->edloloctax = edloloctax; // XXX bug what??
  meshptr->ovlpglbval = ovlpglbval;

  if (ovlpglbvl2 > 0)
    memFreeGroup (vertovptax + baseval);
  else
    memFreeGroup (ventgsttax + baseval);


  memFreeGroup (vnbrloctax + baseval);
  memFreeGroup(vertsortloctab);
  meshptr->flagval |= DMESHFREEENTT;

  if (valslocptr == NULL)
  	cheklocval = dvaluesInit(&meshptr->valslocptr, baseval, valuglbmax, enttglbnbr);
  else {
	valslocptr->cuntlocnbr ++;
	meshptr->valslocptr = valslocptr;
        cheklocval = (0);
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  /* With overlap and if no ghost vertex is wanted. Only elements (vertices with entity
   * baseval) are concerned and will be removed if they are ghost vertices */
  if ((ovlpglbval & PAMPA_OVLP_NOGT) != 0) {
    int * restrict vsndcnttab;
    int * restrict vsnddsptab;
    Gnum * restrict vertsndtab;
    int * restrict vrcvcnttab;
    int * restrict vrcvdsptab;
    Gnum * restrict vertrcvtab;
    int * restrict vsndcnttb2;
    int * restrict vsnddsptb2;
    Gnum * restrict vertsndtb2;
    int * restrict vrcvcnttb2;
    int * restrict vrcvdsptb2;
    Gnum * restrict vertrcvtb2;
    int  * restrict procsidtab;
    Gnum * svrtloctax;
    DmeshBuildNogtVert * restrict mvrtloctax;
    Gnum * restrict mvrtoldloctax;
    Gnum * restrict ventoldloctax;
    Gnum mvrtlocsiz;
    int procsidnbr;
    DmeshEntity * enttlocptr;
    Gnum enttglbmax;
    Gnum vertsndnbr;
    Gnum vertrcvnbr;
    Gnum vertlocnm2;
    Gnum vertlocmax;
    Gnum vertlocidx;
    Gnum enttlocnum;
    int procsidnum;
    int procsidnm2; 
    int procngbnum;
    int procngbmax;

    enttlocptr = meshptr->enttloctax[baseval];

    if (memAllocGroup ((void **) (void *)
          &vsndcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
          &vsnddsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &vrcvcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
          &vrcvdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &vertsndtab, (size_t) ((enttlocptr->vgstlocnbr - enttlocptr->vertlocnbr) * sizeof (Gnum)),
          &vsndcnttb2, (size_t) (meshptr->procglbnbr       * sizeof (int)),
          &vsnddsptb2, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &vrcvcnttb2, (size_t) (meshptr->procglbnbr       * sizeof (int)),
          &vrcvdsptb2, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &vertsndtb2, (size_t) ((enttlocptr->vgstlocnbr - enttlocptr->vertlocnbr) * sizeof (Gnum)),
          &mvrtloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (DmeshBuildNogtVert)),
          &mvrtoldloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
          &ventoldloctax, (size_t) (enttlocptr->vgstlocnbr * sizeof (Gnum)),
          &svrtloctax, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
          (void *) NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    mvrtloctax -= baseval;
    mvrtoldloctax -= baseval;
    svrtloctax -= baseval;

    memSet (mvrtloctax, ~0, enttlocptr->vgstlocnbr * sizeof (DmeshBuildNogtVert));
    memSet (svrtloctax, ~0, meshptr->enttglbnbr * sizeof (Gnum));
    memCpy (mvrtoldloctax + baseval, enttlocptr->mvrtloctax, enttlocptr->vgstlocnbr * sizeof (Gnum));
    if (enttlocptr->ventloctax != NULL)
      memCpy (ventoldloctax + baseval, enttlocptr->ventloctax, enttlocptr->vgstlocnbr * sizeof (Gnum));

    /* Fill the triplet array mvrtloctax with local elements */
    for (vertlocnum = baseval, vertlocnnd = meshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
      mvrtloctax[vertlocnum].mvrtnum = enttloctax[baseval]->mvrtloctax[vertlocnum];
      mvrtloctax[vertlocnum].vertnum = vertlocnum;
      mvrtloctax[vertlocnum].vnewnum = vertlocnum;
    }
    mvrtlocsiz = meshptr->enttloctax[baseval]->vgstlocnbr;

    enttglbmax = meshptr->enttglbnbr + meshptr->baseval - 1;
    for (ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
      if (enttloctax[ventlocnum] != NULL)
        svrtloctax[ventlocnum] = enttloctax[ventlocnum]->vertlocnbr + baseval;
    }

    /* Loop over not local elements */
    for (vertsndnbr = 0, vertlocnum = meshptr->enttloctax[baseval]->vertlocnbr + baseval, vertlocnnd = meshptr->enttloctax[baseval]->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
      mvrtloctax[vertlocnum].mvrtnum = enttloctax[baseval]->mvrtloctax[vertlocnum];
      mvrtloctax[vertlocnum].vertnum = vertlocnum;

      /* If vertex is halo (means no neighbors) */
      if (enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[vertlocnum].vertlocidx ==
          enttlocptr->nghbloctax[enttglbmax]->vindloctax[vertlocnum].vendlocidx) { 
        vertsndtab[vertsndnbr ++] = enttlocptr->mvrtloctax[vertlocnum];
        enttlocptr->vgstlocnbr --;
        //! \warning This following test seems to be out of date, because
        //! vertlocnum has entity baseval
        if ((enttlocptr->ventloctax != NULL) && (enttlocptr->ventloctax[vertlocnum] != baseval)) {
          Gnum entslocnum;

          entslocnum = enttlocptr->ventloctax[vertlocnum];
          enttloctax[entslocnum]->vgstlocnbr --;
        }
      }
      /* Vertex is not halo (has neighbors) */
      else {
        Gnum nentlocnum;
        Gnum nentlocnnd;

        mvrtloctax[vertlocnum].vnewnum = svrtloctax[baseval];

        for (nentlocnum = baseval, nentlocnnd = enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++)
          if ((enttlocptr->nghbloctax[nentlocnum] != NULL) && (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)) /* If ventlocnum is not a sub-entity */
          enttlocptr->nghbloctax[nentlocnum]->vindloctax[svrtloctax[baseval]] = enttlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum];

        enttlocptr->mvrtloctax[svrtloctax[baseval]] = enttlocptr->mvrtloctax[vertlocnum];
        if ((enttlocptr->ventloctax != NULL) && (enttlocptr->ventloctax[vertlocnum] != baseval)) {
          Gnum entslocnum;

          entslocnum = enttlocptr->ventloctax[vertlocnum];
          enttloctax[entslocnum]->svrtloctax[svrtloctax[entslocnum]] = svrtloctax[baseval];
          enttloctax[entslocnum]->ventloctax[svrtloctax[entslocnum]] = baseval;

          enttlocptr->svrtloctax[svrtloctax[baseval]] = svrtloctax[entslocnum];
          enttlocptr->ventloctax[svrtloctax[baseval]] = entslocnum;

          svrtloctax[entslocnum] ++;
        }

        svrtloctax[baseval] ++;
      }
    }

    intSort3asc1 (mvrtloctax + baseval, mvrtlocsiz);

    memSet (vsndcnttab, 0, meshptr->procglbnbr * sizeof (int));
    /* For each vertex in vsndcnttab, processor number will be determined (with
     * procvrttab */
    for (procngbnum = vertlocnum = 0; vertlocnum < vertsndnbr; vertlocnum ++) {

      if (!((meshptr->procvrttab[procngbnum] <= vertsndtab[vertlocnum]) && (meshptr->procvrttab[procngbnum] > vertsndtab[vertlocnum]))) {
        for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
            procngbmax - procngbnum > 1; ) {
          int                 procngbmed;

          procngbmed = (procngbmax + procngbnum) / 2;
          if (meshptr->procvrttab[procngbmed] <= vertsndtab[vertlocnum])
            procngbnum = procngbmed;
          else
            procngbmax = procngbmed;
        }
      }
      vsndcnttab[procngbnum] ++;
    }

    /* Compute displacement sending array */
    vsnddsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1; procngbnum ++) {
      vsnddsptab[procngbnum] = vsnddsptab[procngbnum - 1] + vsndcnttab[procngbnum - 1];
    }

    vertsndnbr = vsnddsptab[meshptr->procglbnbr];

    //* Send vsndcnttab and receive vrcvcnttab
    CHECK_FMPI (cheklocval, commAlltoall(vsndcnttab, 1, MPI_INT, vrcvcnttab, 1, MPI_INT, meshptr->proccomm), meshptr->proccomm);

    /* Compute displacement receiving array */
    vrcvdsptab[0] = 0;
    for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1; procngbnum ++) {
      vrcvdsptab[procngbnum] = vrcvdsptab[procngbnum - 1] + vrcvcnttab[procngbnum - 1];
    }

    vertrcvnbr = vrcvdsptab[meshptr->procglbnbr];

    if (memAllocGroup ((void **) (void *)
          &vertrcvtab, (size_t) (vertrcvnbr * sizeof (Gnum)),
          &vertrcvtb2, (size_t) (vertrcvnbr * sizeof (Gnum)),
          (void *) NULL) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    CHECK_FMPI (cheklocval, commAlltoallv(vertsndtab, vsndcnttab, vsnddsptab, GNUM_MPI, vertrcvtab, vrcvcnttab, vrcvdsptab, GNUM_MPI, meshptr->proccomm), meshptr->proccomm);
    memCpy (vertsndtb2, vertsndtab, vertsndnbr * sizeof (Gnum));
    memCpy (vsndcnttb2, vsndcnttab, meshptr->procglbnbr * sizeof (int));
    memCpy (vsnddsptb2, vsnddsptab, (meshptr->procglbnbr + 1) * sizeof (int));
    memCpy (vertrcvtb2, vertrcvtab, vertrcvnbr * sizeof (Gnum));
    memCpy (vrcvcnttb2, vrcvcnttab, meshptr->procglbnbr * sizeof (int));
    memCpy (vrcvdsptb2, vrcvdsptab, (meshptr->procglbnbr + 1) * sizeof (int));

    if (vertsndnbr > 0) {
      for (vertlocnum = baseval, vertlocidx = 0; vertlocidx < vertsndnbr; vertlocidx ++) {
        while (mvrtloctax[vertlocnum].mvrtnum < vertsndtab[vertlocidx])
          vertlocnum ++;
#ifdef PAMPA_DEBUG_DMESH2
        if (mvrtloctax[vertlocnum].mvrtnum != vertsndtab[vertlocidx]) {
          errorPrint ("invalid number of vertex");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
        vertsndtab[vertlocidx] = mvrtloctax[vertlocnum].vertnum;
      }
      
      /* For each entity */
      for (vertlocmax = enttlocptr->vertlocnbr + baseval - 1,
          ventlocnum = baseval, ventlocnnd = enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
        Gnum edgelocnum;
        Gnum edgelocnnd;
        Gnum * edgelocnm2;
        /* If this entity does not contain any vertex */
        if (enttloctax[ventlocnum] == NULL)
          continue;

        /* If entity is a super-entity */
        if (esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) {
          /* For each vertex, ghost neighbors will be removed using vertsndtab */
          for (vertlocnum = baseval, vertlocnnd = enttloctax[ventlocnum]->vgstlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
            for (edgelocnum = enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
                edgelocnm2 = &enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
                edgelocnnd = enttloctax[ventlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
                edgelocnum < edgelocnnd; edgelocnum ++) {

              /* If neighbor is not local */
              if (meshptr->edgeloctax[edgelocnum] > vertlocmax) {
                Gnum vertngbidx;
                Gnum vertngbmax;

                for (vertngbidx = 0, vertngbmax = vertsndnbr;
                    vertngbmax - vertngbidx > 1; ) {
                  Gnum                 vertngbmed;

                  vertngbmed = (vertngbmax + vertngbidx) / 2;
                  if (vertsndtab[vertngbmed] <= meshptr->edgeloctax[edgelocnum])
                    vertngbidx = vertngbmed;
                  else
                    vertngbmax = vertngbmed;
                }
                /* If vertsndtab contains neighbor */
                if (vertsndtab[vertngbidx] == meshptr->edgeloctax[edgelocnum]) {
                  meshptr->edgeloctax[edgelocnum] = meshptr->edgeloctax[(*edgelocnm2) ++];
                  meshptr->edgelocnbr --;
                }
                /* neighbor is not in vertsndtab */ 
                else {
                  Gnum vertlocnm2;
                  Gnum vertlocmax;
                  Gnum vertlocend;

                  vertlocend = mvrtoldloctax[meshptr->edgeloctax[edgelocnum]];

                  for (vertlocnm2 = baseval, vertlocmax = mvrtlocsiz;
                      vertlocmax - vertlocnm2 > 1; ) {
                    Gnum                 vertlocmed;

                    vertlocmed = (vertlocmax + vertlocnm2) / 2;
                    if (mvrtloctax[vertlocmed].mvrtnum <= vertlocend)
                      vertlocnm2 = vertlocmed;
                    else
                      vertlocmax = vertlocmed;
                  }
#ifdef PAMPA_DEBUG_DMESH2
                  if (mvrtloctax[vertlocnm2].mvrtnum != vertlocend) {
                    errorPrint ("invalid number of vertex");
                    cheklocval = 1;
                  }
                  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
                  meshptr->edgeloctax[edgelocnum] = mvrtloctax[vertlocnm2].vnewnum;
                }

              }
            }
        }
      }
    }

    /* For each entity */
    enttlocnum = baseval;
    do {
      /* If entity does not contain any vertex, or it is a sub-entity of
       * baseval, or it is baseval, then next entity */
      if ((enttloctax[enttlocnum] == NULL) || ((esublocbax[enttlocnum & meshptr->esublocmsk] != baseval) && (enttlocnum != baseval))) {
        enttlocnum ++;
        continue;
      }

      //! \warning This following test seems to be out of date
      if (enttlocnum != baseval) {
        memCpy (vertsndtab, vertsndtb2, vertsndnbr * sizeof (Gnum));
        memCpy (vsndcnttab, vsndcnttb2, meshptr->procglbnbr * sizeof (int));
        memCpy (vsnddsptab, vsnddsptb2, (meshptr->procglbnbr + 1) * sizeof (int));
        memCpy (vertrcvtab, vertrcvtb2, vertrcvnbr * sizeof (Gnum));
        memCpy (vrcvcnttab, vrcvcnttb2, meshptr->procglbnbr * sizeof (int));
        memCpy (vrcvdsptab, vrcvdsptb2, (meshptr->procglbnbr + 1) * sizeof (int));

        /* For each processor number */
        for (vertsndnbr = procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++)
          /* For each vertex sent to this processor */
          for (vertlocidx = vsnddsptab[procngbnum]; vertlocidx < vsnddsptab[procngbnum + 1]; vertlocidx ++) {
            Gnum vertlocmax;

            /* Dichotomy search */
            for (vertlocnum = baseval, vertlocmax = mvrtlocsiz;
                vertlocmax - vertlocnum > 1; ) {
              Gnum                 vertlocmed;

              vertlocmed = (vertlocmax + vertlocnum) / 2;
              if (mvrtloctax[vertlocmed].mvrtnum <= vertsndtab[vertlocidx])
                vertlocnum = vertlocmed;
              else
                vertlocmax = vertlocmed;
            }
#ifdef PAMPA_DEBUG_DMESH2
            if (mvrtloctax[vertlocnum].mvrtnum != vertsndtab[vertlocidx]) {
              errorPrint ("invalid number of vertex");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
            /* If entity of current vertex (vertlocnum) is the same as current
             * entity (enttlocnum) */
            if (ventoldloctax[mvrtloctax[vertlocnum].vertnum] == enttlocnum) 
              vertsndtab[vertsndnbr ++] = vertsndtab[vertlocidx];
            else
              vsndcnttab[procngbnum] --;
          }

        memSet (vrcvcnttab, 0, meshptr->procglbnbr * sizeof (int));
        /* For each processor number */
        for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++)
          /* For each vertex received from this processor */
          for (vertlocidx = vrcvdsptab[procngbnum]; vertlocidx < vrcvdsptab[procngbnum + 1]; vertlocidx ++) {
            Gnum vertlocmax;

            /* Dichotomy search */
            for (vertlocnum = baseval, vertlocmax = mvrtlocsiz;
                vertlocmax - vertlocnum > 1; ) {
              Gnum                 vertlocmed;

              vertlocmed = (vertlocmax + vertlocnum) / 2;
              if (mvrtloctax[vertlocmed].mvrtnum <= vertrcvtab[vertlocidx])
                vertlocnum = vertlocmed;
              else
                vertlocmax = vertlocmed;
            }
#ifdef PAMPA_DEBUG_DMESH2
            if (mvrtloctax[vertlocnum].mvrtnum != vertrcvtab[vertlocidx]) {
              errorPrint ("invalid number of vertex");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
            if (ventoldloctax[mvrtloctax[vertlocnum].vertnum] == enttlocnum) 
              vertrcvtab[vrcvdsptab[procngbnum] + vrcvcnttab[procngbnum] ++] = vertrcvtab[vertlocidx];
          }
      }

      enttlocptr = meshptr->enttloctax[enttlocnum];

      procsidtab = enttlocptr->commlocdat.procsidtab;
      procsidnbr = enttlocptr->commlocdat.procsidnbr;

      if (vertrcvnbr > 0) {
        /* For each processor */
        for (vertlocnum = baseval, procngbnum = 0 ; procngbnum < meshptr->procglbnbr ; procngbnum ++) {
          Gnum vertlocnnd;

          /* For each vertex received from procngbnum */
          for (vertlocidx = vrcvdsptab[procngbnum], vertlocnnd = vrcvdsptab[procngbnum] + vrcvcnttab[procngbnum];
              vertlocidx < vertlocnnd; vertlocidx ++) {
            Gnum vertlocmax;

            /* Dichotomy search */
            for (vertlocnum = baseval, vertlocmax = mvrtlocsiz;
                vertlocmax - vertlocnum > 1; ) {
              Gnum                 vertlocmed;

              vertlocmed = (vertlocmax + vertlocnum) / 2;
              if (mvrtloctax[vertlocmed].mvrtnum <= vertrcvtab[vertlocidx])
                vertlocnum = vertlocmed;
              else
                vertlocmax = vertlocmed;
            }
#ifdef PAMPA_DEBUG_DMESH2
            if (mvrtloctax[vertlocnum].mvrtnum != vertrcvtab[vertlocidx]) {
              errorPrint ("invalid number of vertex");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
            if (enttlocnum == baseval)
              vertrcvtab[vertlocidx] = mvrtloctax[vertlocnum].vertnum;
            else
              vertrcvtab[vertlocidx] = enttloctax[baseval]->svrtloctax[mvrtloctax[vertlocnum].vertnum];
          }
        }

        /* Update entity procsidtab */
        for (vertlocnm2 = vertlocnum = baseval, procsidnm2 = procsidnum = 0 ; procsidnum < enttlocptr->commlocdat.procsidnbr ; procsidnum ++) {
          Gnum vertlocmax;

          if (procsidtab[procsidnum] < 0) {
            vertlocnum -= procsidtab[procsidnum];
            continue;
          }

          procngbnum = procsidtab[procsidnum];
          for (vertlocidx = vrcvdsptab[procngbnum], vertlocmax = vrcvdsptab[procngbnum] + vrcvcnttab[procngbnum];
              vertlocmax - vertlocidx > 1; ) {
            Gnum                 vertlocmed;

            vertlocmed = (vertlocmax + vertlocidx) / 2;
            if (vertrcvtab[vertlocmed] <= vertlocnum)
              vertlocidx = vertlocmed;
            else
              vertlocmax = vertlocmed;
          }
          if ((vrcvcnttab[procngbnum] == 0) || (vertrcvtab[vertlocidx] != vertlocnum)) {
            if (vertlocnm2 < vertlocnum) {
              while ((vertlocnum - vertlocnm2) >= DMESHGHSTSIDMAX) { /* If Gnum range too long for int */
                procsidtab[procsidnm2 ++] = -DMESHGHSTSIDMAX; /* Decrease by maximum int distance      */
                vertlocnm2 += DMESHGHSTSIDMAX;
              }
              procsidtab[procsidnm2 ++] = -(vertlocnum - vertlocnm2);
              vertlocnm2 += (vertlocnum - vertlocnm2);
            }
            procsidtab[procsidnm2 ++] = procngbnum;
          }
        }

        enttlocptr->commlocdat.procsidnbr = procsidnm2;
      }

      for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) {
        enttlocptr->commlocdat.procsndtab[procngbnum] -= vrcvcnttab[procngbnum];
        enttlocptr->commlocdat.procrcvtab[procngbnum] -= vsndcnttab[procngbnum];
      }

      enttlocnum ++;
    } while (enttlocnum <= enttglbmax);

    memFreeGroup (vsndcnttab);
    memFreeGroup (vertrcvtab);
  }

  return (cheklocval);
}

//! \brief This routine fills proc...tab
//! It returns:
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dmeshBuild2 (
    Dmesh * restrict const  meshptr,              /**< mesh */
    const Gnum              baseval,              /**< Base for indexing */
    const Gnum              vertlocnbr,           /**< Number of local vertices */
    const Gnum              vertlocmax)           /**< Maximum number of local vertices */
{
  Gnum                  procnum;
  int                   reduloctab[2];
  int cheklocval;

  cheklocval = 0;

#ifdef PAMPA_DEBUG_DMESH2
  if (vertlocmax < vertlocnbr) {
    errorPrint ("invalid parameters");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

  if (meshptr->procdsptab == NULL) {              /* If private data not yet allocated */
  //if (((meshptr->flagval & DMESHFREEPRIV) == 0) && (meshptr->procdsptab == NULL)) {              /* If private data not yet allocated */
    int                   procglbnbr;

    procglbnbr = meshptr->procglbnbr;
    if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
          &meshptr->procdsptab, (size_t) ((procglbnbr + 1) * sizeof (Gnum)),
          &meshptr->procvrttab, (size_t) ((procglbnbr + 1) * sizeof (Gnum)),
#ifndef PAMPA_DEBUG_MEM
          &meshptr->procngbtab, (size_t) (procglbnbr       * sizeof (int)),
#else /* PAMPA_DEBUG_MEM */
          &meshptr->procngbtab, (size_t) (procglbnbr * 2   * sizeof (int)),
#endif /* PAMPA_DEBUG_MEM */
          &meshptr->proccnttab, (size_t) (procglbnbr       * sizeof (Gnum)),
          &meshptr->procloccnt, (size_t) (                   sizeof (Gnum)),
          (void *) NULL) == NULL) {
      Gnum                dummyval[2];
      Gnum                dummytab[procglbnbr * 2];

      errorPrint ("out of memory");
      dummyval[0] =
        dummyval[1] = -1;
      if (commAllgather (dummyval, 2, MPI_INT,    /* Use dummy receive array (if can be allocated too) */
            dummytab, 2, MPI_INT, meshptr->proccomm) != MPI_SUCCESS)
        errorPrint ("communication error");
	cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, meshptr->proccomm);

	*meshptr->procloccnt = 1;
  }

  reduloctab[0] = (int) vertlocnbr;
  reduloctab[1] = (int) vertlocmax;
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllgather (reduloctab, 2, MPI_INT, /* Use procngbtab and proccnttab as a joint allreduce receive array */
        meshptr->procngbtab, 2, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  meshptr->procdsptab[0] =                        /* Build vertex-to-process array */
    meshptr->procvrttab[0] = baseval;
  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    if (meshptr->procngbtab[procnum] < 0) {       /* If error notified by another process during memory allocation */
      memFree (meshptr->procdsptab);
      meshptr->procdsptab = NULL;                 /* Free memory that has just been allocated */
	cheklocval = 1;
    }
  CHECK_VERR (cheklocval, meshptr->proccomm);

    meshptr->procdsptab[procnum + 1] = meshptr->procdsptab[procnum] + (Gnum) meshptr->procngbtab[2 * procnum];
    meshptr->procvrttab[procnum + 1] = meshptr->procvrttab[procnum] + (Gnum) meshptr->procngbtab[2 * procnum + 1];
  }

  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    meshptr->proccnttab[procnum]     = meshptr->procdsptab[procnum + 1]     - meshptr->procdsptab[procnum];
  }

  meshptr->flagval |= DMESHFREEPRIV;

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

//! \brief This routine increases size of hash table with type
//! DmeshBuildGhstVert
//!
//! \returns  0 : on success.
//! \returns !0 : on error.

static
int
dmeshBuildGhstVertResize (
	DmeshBuildGhstVert * restrict * hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr,
	DmeshBuildProcVert * restrict *               procvertlocptr,
	const Gnum                      hashnbr)
{
  Gnum                          oldhashsiz;       /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  Gnum hashidx;
  Gnum hashtmp;
#ifndef PAMPA_DEBUG_MEM
  Gnum procvertlocidx;
  DmeshBuildGhstVert * restrict oldhashtabptr;
  DmeshBuildProcVert * restrict oldprocvertlocptr;
  DmeshBuildProcVert * restrict savprocvertlocptr;

  oldhashtabptr = *hashtabptr;
  oldprocvertlocptr = *procvertlocptr;
#endif /* PAMPA_DEBUG_MEM */


  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if (memReallocGroup ((void *) (*hashtabptr),
                     hashtabptr,       (size_t) (*hashsizptr * sizeof (DmeshBuildGhstVert)),
                     procvertlocptr, (size_t) (*hashmaxptr * sizeof (DmeshBuildProcVert)),
					 (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
	return (1);
  }

#ifndef PAMPA_DEBUG_MEM
  savprocvertlocptr = (DmeshBuildProcVert *) ((byte *) *hashtabptr + ((byte *) oldprocvertlocptr - (byte *) oldhashtabptr));

  for (procvertlocidx = hashnbr - 1; procvertlocidx >= 0; procvertlocidx --)
	(*procvertlocptr)[procvertlocidx] = savprocvertlocptr[procvertlocidx];
#endif /* PAMPA_DEBUG_MEM */

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (DmeshBuildGhstVert)); // TRICK: *hashsizptr = oldhashsiz << 1

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].mvrtnum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
	Gnum vertlocend;

	vertlocend = (*hashtabptr)[hashidx].mvrtnum;

	if (vertlocend == ~0) // If empty slot
	  continue;

    for (hashnum = (vertlocend * DMESHBUILDGHSTHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].mvrtnum != ~0 && (*hashtabptr)[hashnum].mvrtnum != vertlocend; hashnum = (hashnum + 1) & (*hashmskptr)) ;

	if (hashnum == hashidx) // already at the good slot 
	  continue;
	(*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
	(*hashtabptr)[hashidx].mvrtnum = ~0;
  }

  return (0);
}
  


//! \brief This routine compute the edgegsttax and the ventgsttax (entity of vertex for
//!  ghost vertices)
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dmeshBuild3 (
  Dmesh * restrict const    meshptr,              /**< mesh */
  const Gnum                baseval,              /**< Base for indexing */
  const Gnum                vertlocnbr,           /**< Number of local vertices */
  const Gnum                vertlocmax,           /**< Maximum number of local vertices */
  Gnum * const              vertloctax,           /**< Local vertex begin array */
  Gnum * const              vendloctax,           /**< Local vertex end array */
  const Gnum                edgelocsiz,           /**< Size of local edges */
  Gnum * restrict const     edgeloctax,           /**< Local edge array */
  Gnum *                    ventloctax,           /**< Local vertex entity array */
  const Gnum                ovlpglbval,           /**< Global overlap value */
  Gnum *                    vgstlocnbr,           /**< Number of local + ghost vertices */
  DmeshBuildGhstVert * restrict * hashtab,          /* Extended vertex array */
  DmeshBuildProcVert * restrict *         procvertlocptr,       /**< Local arrray of processor and vertex number */
  Gnum * restrict *         ventgsttax,           /**< Ghost array of vertex entity */
  Gnum * restrict *         vertgsttax,           /**< Ghost array of vertex number */
  Gnum * restrict *         edgegsttax)           /**< Ghost edge array (if any); not const */
{
  Gnum                          vertlocnum;
  Gnum                          vertlocnnd;
  Gnum                          vertlocbas;
  Gnum                          bvrtlocmin;
  Gnum                          bvrtlocmax;
  Gnum                          hashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          vertrcvnbr;
  Gnum                          procvertlocidx;
  Gnum                          hashmax;
  Gnum                          hashnbr;
  Gnum                          hashmsk;
  Gnum                          procvertlocbas;
  int                           procngbnum;
  int * restrict               vsndcnttab;
  int * restrict               vsnddsptab;
  int * restrict               vrcvcnttab;
  int * restrict               vrcvdsptab;
  Gnum * restrict               vrcvdattab;
  int                           cheklocval;
#ifdef PAMPA_DEBUG_REDUCE
  int chekglbval;
#endif /* PAMPA_DEBUG_REDUCE */

  cheklocval = 0;

  hashnbr = vertlocnbr / 10;
  for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */
  //hashnbr = 1;
  //for (hashsiz = 4; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */

  hashmsk = hashsiz - 1;
  hashmax = hashsiz >> 2;


  if (memAllocGroup ((void **) (void *)
                     hashtab,       (size_t) (hashsiz * sizeof (DmeshBuildGhstVert)),
                     procvertlocptr, (size_t) (hashmax * sizeof (DmeshBuildProcVert)),
					 (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  hashnbr = 0;

  memset (*hashtab, ~0, hashsiz * sizeof (DmeshBuildGhstVert)); /* Set all vertex numbers to ~0 */

  vertlocbas = meshptr->procvrttab[meshptr->proclocnum] - baseval;
  bvrtlocmin = meshptr->procvrttab[meshptr->proclocnum];
  bvrtlocmax = meshptr->procvrttab[meshptr->proclocnum + 1];
  meshptr->degrlocmax = 0;

  /* Determine the processor number which have ghost neighbor for each local
   * vertex. Processor number and ghost neighbor are stored in procvertlocptr */
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++)  {
    Gnum                edgelocnum;

 /* Update the degrlocmax */
    if (vendloctax[vertlocnum] - vertloctax[vertlocnum] > meshptr->degrlocmax)
      meshptr->degrlocmax = vendloctax[vertlocnum] - vertloctax[vertlocnum];

    for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {

      Gnum vertlocend;

      vertlocend = edgeloctax[edgelocnum];

      if (!((vertlocend >= bvrtlocmin) && (vertlocend < bvrtlocmax))) { /* If edge is not local */
        for (hashnum = (vertlocend * DMESHBUILDGHSTHASHPRIME) & hashmsk; (*hashtab)[hashnum].mvrtnum != ~0 && (*hashtab)[hashnum].mvrtnum != vertlocend; hashnum = (hashnum + 1) & hashmsk) ;

        if ((*hashtab)[hashnum].mvrtnum == vertlocend) /* If vertex already added */
          continue;

        int procngbmax;
  
  /* Search the processor which have current neighbor */
        for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
            procngbmax - procngbnum > 1; ) {
          int                 procngbmed;

          procngbmed = (procngbmax + procngbnum) / 2;
          if (meshptr->procvrttab[procngbmed] <= vertlocend)
            procngbnum = procngbmed;
          else
            procngbmax = procngbmed;
        }

  /* Fill in the procvertlocptr */
        (*procvertlocptr)[hashnbr].procnum = procngbnum;
        (*procvertlocptr)[hashnbr].mvrtnum = vertlocend;
        (*hashtab)[hashnum].mvrtnum = vertlocend;
        hashnbr ++;

        if (hashnbr >= hashmax) { /* If (*hashtab) is too much filled */
		  CHECK_FERR (dmeshBuildGhstVertResize (hashtab, &hashsiz, &hashmax, &hashmsk, procvertlocptr, hashnbr), meshptr->proccomm);
        }

      }
    }
  }

  intSort2asc2(*procvertlocptr, hashnbr); /* sort processor and neighbor numbers by both */


  /* Allocate memory for communication arrays */
  if (memAllocGroup ((void **) (void *)
        &vsndcnttab, (size_t) (meshptr->procglbnbr           * sizeof (int)),
        &vsnddsptab, (size_t) ((meshptr->procglbnbr + 1)     * sizeof (int)),
        &vrcvcnttab, (size_t) (meshptr->procglbnbr           * sizeof (int)),
        &vrcvdsptab, (size_t) ((meshptr->procglbnbr + 1)     * sizeof (int)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  memSet (vsndcnttab,  0, meshptr->procglbnbr * sizeof (int));

  /* Determine the ghost vertex number and the size of data to send */
  *vgstlocnbr = vertlocnbr + baseval;
  for (procvertlocidx = 0 ; procvertlocidx < hashnbr ; procvertlocidx ++) {
    Gnum vertlocend;

    vsndcnttab[(*procvertlocptr)[procvertlocidx].procnum] ++;
    vertlocend = (*procvertlocptr)[procvertlocidx].mvrtnum;

    for (hashnum = (vertlocend * DMESHBUILDGHSTHASHPRIME) & hashmsk; (*hashtab)[hashnum].mvrtnum != vertlocend; hashnum = (hashnum + 1) & hashmsk) ;

    (*hashtab)[hashnum].vertnum = (*vgstlocnbr) ++;

  }


  *vgstlocnbr -= baseval;

  if (memAllocGroup ((void **) (void *)
        ventgsttax, (size_t) (*vgstlocnbr * sizeof (Gnum)),
        vertgsttax, (size_t) ((*vgstlocnbr - vertlocnbr) * sizeof (Gnum)),
        edgegsttax,    (size_t) (edgelocsiz * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  *ventgsttax -= baseval;
  *vertgsttax -= vertlocnbr + baseval;
  *edgegsttax -= baseval;

#ifdef PAMPA_DEBUG_DMESH2
  memSet (*edgegsttax + baseval, ~0, edgelocsiz * sizeof (Gnum));
#endif /* PAMPA_DEBUG_DMESH2 */

  memCpy((*ventgsttax) + baseval, ventloctax + baseval, vertlocnbr * sizeof(Gnum));

  if (ovlpglbval > 0) /* If overlap */
 /* Update ventgsttax for local vertices */
    for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++)  {
      if ((ventloctax[vertlocnum] != PAMPA_ENTT_DUMMY) && (meshptr->esublocbax[ventloctax[vertlocnum] & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
        (*ventgsttax)[vertlocnum] = meshptr->esublocbax[ventloctax[vertlocnum] & meshptr->esublocmsk]; /* TRICK: ventgsttax contains number of entity instead of sub-entity */
      else
        (*ventgsttax)[vertlocnum] = ventloctax[vertlocnum];
    }

  /* Compute edgegsttax for each vertex and each neighbor */
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;                                                          
      vertlocnum < vertlocnnd; vertlocnum ++)  {                                                                         
    Gnum edgelocnum;

    for (edgelocnum = vertloctax[vertlocnum] ; edgelocnum < vendloctax[vertlocnum] ; edgelocnum ++) {                    
      Gnum vertlocend;


      vertlocend = edgeloctax[edgelocnum];                                                                               

      if (!((vertlocend >= bvrtlocmin) && (vertlocend < bvrtlocmax))) { /* If edge is not local */                      
  /* Search vertlocend which has been added, in (*hashtab) */
        for (hashnum = (vertlocend * DMESHBUILDGHSTHASHPRIME) & hashmsk; (*hashtab)[hashnum].mvrtnum != vertlocend; hashnum = (hashnum + 1) & hashmsk) ;

        (*edgegsttax)[edgelocnum] = (*hashtab)[hashnum].vertnum;                                                               
      }                                                                                                                  
      else {                                                             /* If edge is local     */                      
        (*edgegsttax)[edgelocnum] = vertlocend - vertlocbas;                                                                
      }                                                                                                                  
    }                                                                                                                    
  }                                                                                                                      
  vsnddsptab[0] = 0;

  /*
   * Communicate to update ventgsttax for ghost vertices *
   */

  /* Compute displacement sending array */
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
    vsnddsptab[procngbnum] = vsnddsptab[procngbnum - 1] + vsndcnttab[procngbnum - 1];
  }

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoall(vsndcnttab, 1, MPI_INT, vrcvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }

  /* Compute displacement receiving array */
  vrcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr + 1 ; procngbnum ++) {
    vrcvdsptab[procngbnum] = vrcvdsptab[procngbnum - 1] + vrcvcnttab[procngbnum - 1];
  }
  vertrcvnbr = vrcvdsptab[meshptr->procglbnbr];

  /* Fill in vertgsttax with procvertlocptr */
  procvertlocbas = vertlocnbr + baseval;
  for (vertlocnum = vertlocnbr + baseval, vertlocnnd = *vgstlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++) 
    (*vertgsttax)[vertlocnum] = (*procvertlocptr)[vertlocnum - procvertlocbas].mvrtnum;

  /* Allocate temporary receiving array */
  if ((vrcvdattab = (Gnum *) memAlloc (vertrcvnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

#ifdef PAMPA_DEBUG_DMESH2
  memSet (vrcvdattab, ~0, vertrcvnbr * sizeof (Gnum));
#endif /* PAMPA_DEBUG_DMESH2 */
  /* Communicate vertex number for each vertex which is considered as ghost neighbor on
   * distant processor */
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoallv((*vertgsttax) + vertlocnbr + baseval, vsndcnttab, vsnddsptab, GNUM_MPI, vrcvdattab, vrcvcnttab, vrcvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  /* Modify vrcvdattab by putting entity number for each vertex already present
   * in the array */
  for (procvertlocidx = 0 ; procvertlocidx < vertrcvnbr ; procvertlocidx ++) {
    Gnum ventlocnum;

#ifdef PAMPA_DEBUG_DMESH2
    {
      Gnum enttglbmax;

      if (vrcvdattab[procvertlocidx] == ~0) {
        errorPrint ("internal error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
      if (((vrcvdattab[procvertlocidx] - vertlocbas) < baseval) 
	   || ((vrcvdattab[procvertlocidx] - vertlocbas) > (vertlocnbr + meshptr->baseval))) {
        errorPrint ("internal error");
        cheklocval = 1;
      }
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
    ventlocnum = ventloctax[vrcvdattab[procvertlocidx] - vertlocbas]; // XXX ajouter un test en debug si l'indice ne dépasse pas la taille du tableau
    if ((ovlpglbval > 0) && (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE)) /* if overlap and sub-entity */
      vrcvdattab[procvertlocidx] = meshptr->esublocbax[ventlocnum & meshptr->esublocmsk];
    else
      vrcvdattab[procvertlocidx] = ventlocnum;
  }

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAlltoallv(vrcvdattab, vrcvcnttab, vrcvdsptab, GNUM_MPI, (*ventgsttax) + vertlocnbr + baseval, vsndcnttab, vsnddsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  memFree (vrcvdattab);
  memFreeGroup (vsndcnttab);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}



//! \brief This routine computes number of vertex for each entity, determines global
//! number of vertex for each entity and allocate private entity data for each not
//! null entity
//!
//! \pre meshptr->enttglbnbr defined
//! \pre meshptr->esublocbax defined
//! \pre meshptr->esublocmsk defined
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dmeshBuild4 (
    Dmesh * restrict const      meshptr,              /**< mesh */
    const Gnum                  vertlocnbr,           /**< Number of local vertices */
    Gnum *                      ventloctax,           /**< Local vertex entity array */
    Gnum * restrict *           vnbrloctax,           /**< Local array of number of vertex */
    Gnum * restrict *           vnbrglbtax)           /**< Global array of number of vertex */
{

  Gnum baseval;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum vsublocnum;
  Gnum esubnnd;
  Gnum ventlocnum;
  Gnum ventlocnnd;
  int cheklocval;
#ifdef PAMPA_DEBUG_REDUCE
  int chekglbval;
#endif /* PAMPA_DEBUG_REDUCE */

  baseval = meshptr->baseval;

  cheklocval = 0;

  /* Allocate and initialize memory for temporary arrays */
  if (memAllocGroup ((void **) (void *)
        vnbrloctax, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
        vnbrglbtax, (size_t) (meshptr->enttglbnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  *vnbrloctax -= baseval;
  *vnbrglbtax -= baseval;

  memSet (*vnbrloctax + baseval, 0, meshptr->enttglbnbr * sizeof (Gnum));

  /* Count local vertices for each entity */
  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      vertlocnum < vertlocnnd; vertlocnum ++) {
#ifdef PAMPA_DEBUG_DMESH
	if ((ventloctax[vertlocnum] != PAMPA_ENTT_DUMMY) && ((ventloctax[vertlocnum] < baseval) || (ventloctax[vertlocnum] > meshptr->enttglbnbr + baseval))) {
	  errorPrint ("invalid entity number");
	  cheklocval = 1;
	}
  	CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
    if (ventloctax[vertlocnum] != PAMPA_ENTT_DUMMY)
      (*vnbrloctax)[ventloctax[vertlocnum]] ++;
  }

  /* Updating entity counters with sub-entity counters */
  for (vsublocnum = baseval, esubnnd = meshptr->enttglbnbr + baseval ; vsublocnum < esubnnd ; vsublocnum ++) {
    Gnum ventlocnum;

    ventlocnum = meshptr->esublocbax[vsublocnum & meshptr->esublocmsk];

    if (ventlocnum > PAMPA_ENTT_SINGLE)
      (*vnbrloctax)[ventlocnum] += (*vnbrloctax)[vsublocnum];
  }

  /* Sum local entity counters to have global entity counters */
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce(*vnbrloctax + baseval, *vnbrglbtax + baseval, meshptr->enttglbnbr, GNUM_MPI, MPI_SUM, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }

  /* Allocate private entity arrays and adjacency list */
  if (memAllocGroup ((void **) (void *)
        &meshptr->enttloctax, (size_t) (meshptr->enttglbnbr * sizeof (DmeshEntity *)),
#ifdef PAMPA_DEBUG_MEM
        &meshptr->edgeloctax, (size_t) (1 * sizeof (Gnum)),
#endif /* PAMPA_DEBUG_MEM */
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  meshptr->enttloctax -= baseval;
#ifdef PAMPA_DEBUG_MEM
  meshptr->edgeloctax -= baseval;
#endif /* PAMPA_DEBUG_MEM */

  /* Allocate and intialize private entity data for each not null entity */
  for (meshptr->vertglbnbr = 0, ventlocnum = baseval, ventlocnnd = meshptr->enttglbnbr + baseval;
 	  ventlocnum < ventlocnnd ; ventlocnum ++) {
    if ((*vnbrglbtax)[ventlocnum] == 0) /* If no vertex in this entity globally */
      meshptr->enttloctax[ventlocnum] = NULL;
    else {
      if ((meshptr->enttloctax[ventlocnum] = (DmeshEntity *) memAlloc (sizeof (DmeshEntity))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

      memSet (meshptr->enttloctax[ventlocnum], 0, sizeof (DmeshEntity));
	  meshptr->enttloctax[ventlocnum]->vertglbnbr = (*vnbrglbtax)[ventlocnum];
      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) /* If ventlocnum is not a sub-entity */
		meshptr->vertglbnbr += (*vnbrglbtax)[ventlocnum];
    }
  }

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}






