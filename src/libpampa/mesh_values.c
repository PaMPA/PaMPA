/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_values.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the mesh value
//!                handling routines.
//!
//!   \date        Version 1.0: from: 10 May 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define MESH_VALUES

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

int meshValueLink (
Mesh * restrict const        meshptr,
void **                       valutab,
Gnum                         flagval,
Gnum *                        sizeval,
Gnum *			     typesiz,
MPI_Datatype                  typeval,
Gnum                         enttnum,
Gnum                         tagnum)
{

  int valunum;
  Gnum vertnbr;
  Gnum baseval;
  Values * valsptr;
  Value *valuptr;
  MPI_Aint            dummy;
  MPI_Aint typesiz2;

  baseval = meshptr->baseval;
  valsptr =  meshptr->valsptr;

  if (MPI_Type_get_extent (typeval, &dummy, &typesiz2)) {
    errorPrint ("get size error");
    return (1);
  }

  if (typesiz != NULL)
  	*typesiz = (Gnum) typesiz2;

#ifdef PAMPA_DEBUG_MESH
  if ((enttnum < PAMPA_ENTT_VIRT_PROC) || (enttnum >= meshptr->enttnbr + baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
  if (tagnum == PAMPA_TAG_NIL) {
    errorPrint("invalid tag");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  valunum = 0;
  while ((valunum < valsptr->valumax) && 
      (valsptr->valutab[valunum].tagnum != PAMPA_TAG_NIL)) {
#ifdef PAMPA_DEBUG_DMESH
    if ((valsptr->valutab[valunum].tagnum == tagnum) && (valsptr->valutab[valunum].enttnum == enttnum)) {
      errorPrint("values with the same tag has already been added on this entity");
      return (1);
    }
#endif /* PAMPA_DEBUG_DMESH */
    valunum++;
  }

#ifdef PAMPA_DEBUG_MESH
  if (valunum >= valsptr->valumax) {
    errorPrint("no more values could be linked");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  //if ((enttnum >= baseval) && (meshptr->entttax[enttnum] == NULL)) // XXX à modifier
  //  return (0);

  switch (enttnum) {
    case PAMPA_ENTT_VIRT_VERT :
      vertnbr = meshptr->vertnbr;
      if (sizeval != NULL)
        *sizeval = meshptr->vertnbr; 
      break;
    case PAMPA_ENTT_VIRT_PROC :
      vertnbr = meshptr->procglbnbr + 1;
      if (sizeval != NULL)
        *sizeval = meshptr->procglbnbr + 1; 
      break;
    default :
      if ((enttnum >= baseval) && (meshptr->entttax[enttnum] == NULL)) {
        vertnbr = 0;
        if (sizeval != NULL)
          *sizeval = 0; 
      }
      else {
        vertnbr = meshptr->entttax[enttnum]->vertnbr; 
        if (sizeval != NULL)
          *sizeval = meshptr->entttax[enttnum]->vertnbr; 
      }
  }

  if ((flagval & PAMPA_VALUE_ALLOCATED) != 0)
    valsptr->valutab[valunum].valutab = *valutab;
  else {
    if ((valsptr->valutab[valunum].valutab =
          (byte *) memAlloc (vertnbr * ((size_t) typesiz2) * sizeof(byte))) == NULL) {
      errorPrint ("out of memory");
      return (1);
    }
    *valutab = valsptr->valutab[valunum].valutab;
  }

  valsptr->valutab[valunum].typeval = typeval;
  valsptr->valutab[valunum].flagval = flagval;
  valsptr->valutab[valunum].enttnum = enttnum;
  valsptr->valutab[valunum].tagnum = tagnum;
  valsptr->valutab[valunum].next = NULL;

  valuptr = valsptr->evaltak[enttnum];

  if (valuptr == NULL) {
    valsptr->evaltak[enttnum] = &valsptr->valutab[valunum];
  }
  else {
    while (valuptr->next != NULL)
      valuptr = valuptr->next;

    valuptr->next = &valsptr->valutab[valunum];
  }

  valsptr->valunbr++;

  return (0);
}


int meshValueUnlink (
    Mesh * restrict const        meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum)
{

  Values * valsptr = meshptr->valsptr;
  Value *valuptr, *meshvalptz;
#ifdef PAMPA_DEBUG_MESH
  if ((enttnum < PAMPA_ENTT_VIRT_PROC) || (enttnum >= meshptr->enttnbr + meshptr->baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  valuptr = valsptr->evaltak[enttnum];
  meshvalptz = NULL;

  while (valuptr !=  NULL) {
    if (valuptr->tagnum == tagnum)
      break;
    meshvalptz = valuptr;
    valuptr = valuptr->next;
  }

  if (valuptr == NULL) {
    errorPrint("no values are linked with these parameters");
    return (1);
  }

  if ((valuptr->flagval & PAMPA_VALUE_NOTFREE) == 0)
    memFree (valuptr->valutab);

  if (meshvalptz == NULL)
    valsptr->evaltak[enttnum] = valuptr->next;
  else
    meshvalptz->next = valuptr->next;

  valuptr->tagnum = PAMPA_TAG_NIL;


  valsptr->valunbr--;

  return (0);
}

int meshValueData (
const Mesh * const     meshptr,                           //!< Centralized mesh
Gnum const              enttnum,                           //!< Entity number
Gnum                    tagnum,                            //!< Tag value
Gnum *                        sizeval,
Gnum *					     typesiz,
void **                      valutab)                           //!< Associated values
{

#ifdef PAMPA_DEBUG_MESH
  if ((enttnum < PAMPA_ENTT_VIRT_PROC) || (enttnum >= meshptr->enttnbr + meshptr->baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  if (sizeval != NULL) {
    switch (enttnum) {
      case PAMPA_ENTT_VIRT_VERT :
        *sizeval = meshptr->vertnbr; 
        break;
      case PAMPA_ENTT_VIRT_PROC :
        *sizeval = meshptr->procglbnbr + 1; 
        break;
      default :
        *sizeval = (meshptr->entttax[enttnum] == NULL) ? (0) : (meshptr->entttax[enttnum]->vertnbr); 
    }
  }

  return valueData (meshptr->valsptr, enttnum, tagnum, typesiz, valutab);
}

