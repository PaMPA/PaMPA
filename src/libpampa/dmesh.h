/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the distributed source mesh structure.
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/



#define DMESH_H

#ifndef PAMPA_COMM_PTOP_RAT
#define PAMPA_COMM_PTOP_RAT        0.25           /* Percentage under which point-to-point is allowed */
#endif /* PAMPA_COMM_PTOP_RAT */

/*
** The defines.
*/

/* mesh flags. */

#define DMESHNONE                  0x0000         //!< No options set

#define DMESHFREEPRIV              0x0001        //!< Set if private arrays freed on exit
#define DMESHFREECOMM              0x0002        //!< MPI communicator has to be freed
#define DMESHFREETABS              0x0004        //!< Set if ventloctax freed on exit
#define DMESHFREEENTT              0x0008        //!< Local entity private data have to be freed
#define DMESHFREELVL               0x0010        //!< Set if local data between levels (multigrid) have to be freed
#define DMESHFREEPSID              0x0040        //!< Set if procsidtab freed on exit
#define DMESHFREEALL               (DMESHFREEPRIV | DMESHFREECOMM | DMESHFREETABS | DMESHFREEENTT | \
                                    DMESHFREEEVALU | DMESHFREEPSID)
#define DMESHCOMMPTOP              0x0080         //!< Use point-to-point collective communication



/* Tags used for point-to-point communications. */

typedef enum DmeshTag_   
{
  TAGPROCVRTTAB = 0,                              //!< procvrttab message
  TAGHALO    = 100,                               //!< Tag class for halo
  TAGSCATTERTAB = 200,                            //!< scattertab message
} DmeshTag;






/*
** The type and structure definitions.
*/

/* The mesh basic types, which must be signed. */

#ifndef GNUMMAX                                   
typedef INT                 Gnum;                 //!< Vertex or edge number
#define GNUMMAX                     (INTVALMAX)   //!< Maximum Gnum value
#endif //!< GNUMMAX

#define GNUM_MPI                    COMM_INT      //!< MPI type for Gnum is MPI type for INT



typedef int DmeshFlag;                            //!< Mesh property flags




/* The distributed mesh structure. */

typedef struct DmeshVertBounds_       //!  Vertex bounds
{
  Gnum                 vertlocidx;    //!< Vertex beginning index
  Gnum                 vendlocidx;    //!< Vertex end index, \warn vend must be just after vert, because of iterators
} DmeshVertBounds;

typedef struct DmeshEnttNghb_         //! Vertex neighbors
{
  DmeshVertBounds *    vindloctax;    //!< Vertex index array [based]
} DmeshEnttNghb;

typedef struct DmeshEnttComm_
{
  int *                procrcvtab;    //!< Number of vertices to receive in ghost vertex sub-arrays
  int                  procsndnbr;    //!< Overall size of local send array
  int *                procsndtab;    //!< Number of vertices to send in ghost vertex sub-arrays
  int *                procsidtab;    //!< Array of indices to build communication vectors (send)
  int                  procsidnbr;    //!< Size of the send index array
  int                  procngbnbr;    //!< Number of neighboring processes
  int *                procngbtab;    //!< Array of neighbor process numbers [sorted]
} DmeshEnttComm;

typedef struct DmeshEntity_           //!  Partial mesh structure for each entity
{
  Gnum                 vfrnlocmin;    //!< Local minimum number of frontier vertex
  Gnum                 vfrnlocmax;    //!< Local maximum number of frontier vertex
  Gnum                 vertlocnbr;    //!< Local number of vertices
  Gnum                 vintlocnbr;    //!< Local number of internal vertices
  Gnum                 vovplocnbr;    //!< Number of local + overlap vertices
  Gnum                 vgstlocnbr;    //!< Number of local + ghost vertices
  Gnum                 vertglbnbr;    //!< Number of global vertices
  Gnum *               mvrtloctax;    //!< Array of vertex numbers (numbering in Mesh) for entity or sub-entity
  Gnum *               svrtloctax;    //!< Array of vertex numbers (numbering in entity or sub-entity - not for single entity)
  Gnum *               ventloctax;    //!< Array of entity values for each vertex (not for single entity)
  Gnum *               vprmloctax;    //!< Array of vertex numbers (group internal vertices before boundary ones)
  Gnum *               vmatloctax;    //!< Array of vertex numbers (matrix storage)
  DmeshEnttNghb **     nghbloctax;    //!< Array of neighors (ordered by entity for each neighbor)
  DmeshEnttNghb **     ngbuloctax;    //!< Upper neighbor array
  DmeshEnttNghb **     ngblloctax;    //!< Lower neighbor array
  DmeshEnttComm        commlocdat;    //!< Data to do communications
} DmeshEntity;
  
typedef struct Dmesh_   
{
  DmeshFlag            flagval;       //!< Mesh properties
  Gnum                 baseval;       //!< Base index for edge/vertex/entity arrays
  Gnum                 ovlpglbval;    //!< Overlap value
  Gnum                 vertlocnbr;    //!< Number of local vertices (dummy entity + true vertices)
  Gnum                 vtrulocnbr;    //!< Number of true vertices (not with dummy entity)
  Gnum                 vertlocmax;    //!< Number of maximum local vertices
  Gnum                 vertglbnbr;    //!< Number of global vertices
  Gnum                 enttglbnbr;    //!< Number of entities which make up the dmesh
  Gnum *               esublocbax;    //!< Array of main entity for each sub-entity
  Gnum                 esublocmsk;    //!< Mask used on both cases: with sub-entities or not
  Gnum                 degrlocmax;    //!< Number of maximum degree 
  DmeshEntity **       enttloctax;    //!< Array of entity private data
  Gnum                 edgelocnbr;    //!< Number of local edges
  Gnum                 edgelocsiz;    //!< Number of maximum local edges
  Gnum *               edgeloctax;    //!< Array of local edges
  Gnum                 medglocnbr;    //!< XXX
  Gnum                 medglocsiz;    //!< XXX
  Gnum *               medgloctax;    //!< XXX
  Gnum *               edloloctax;    //!< Array of local load edges
  Dvalues *            valslocptr;    //!< Pointer on associated values structure
  MPI_Comm             proccomm;      //!< Mesh communicator
  int                  procglbnbr;    //!< Number of processes sharing mesh data
  int                  proclocnum;    //!< Number of this process
  Gnum *               procvrttab;    //!< Global array of vertex number ranges [1,based]
  Gnum *               proccnttab;    //!< Count array for local number of vertices
  Gnum *               procdsptab;    //!< Displacement array with respect to proccnttab [1,based]
  int                  procngbnbr;    //!< Number of neighboring processes
  int                  procngbmax;    //!< Maximum number of neighboring processes
  int *                procngbtab;    //!< Array of neighbor process numbers [sorted]
  Gnum *               procloccnt;    //!< Counter on duplicated allocated arrays procvrttab,...
} Dmesh;

/*
** The functions prototypes.
*/

int dmeshInit (
    Dmesh * const                meshptr,
    MPI_Comm                     proccomm);

void dmeshExit (
    Dmesh * const                meshptr);

void dmeshFree (
    Dmesh * const                meshptr);

int dmeshBand (
    Dmesh * const                dmshptr,
    Gnum                         ventlocnum,
    Gnum *                       vtaggsttax,
    Gnum                         tagval,
    Gnum                         nentlocnum,
    Gnum                         bandval);

int dmeshBuild (
    Dmesh * const                meshptr,
    const Gnum                   baseval,
    const Gnum                   vertlocnbr,
    const Gnum                   vertlocmax,
    Gnum * const                 vertloctax,
    Gnum * const                 vendloctax,
    const Gnum                   edgelocnbr,
    const Gnum                   edgelocsiz,
    Gnum * const                 edgeloctax,
    Gnum * const                 edloloctax,
    Gnum                         enttglbnbr,
    Gnum *                       ventloctax,
    Gnum *                       esubloctab,
    Gnum                         valuglbmax,
    const Gnum                   ovlpglbval,
    Dvalues *                    valslocptr,
    Gnum                         degrlocmax,
    Gnum *                       procvrttab,
    Gnum *                       proccnttab,
    Gnum *                       procdsptab,
    int                          procngbnbr,
    int                          procngbmax,
    int *                        procngbtab);

int dmeshCheck (
    Dmesh * const                meshptr);


int dmeshGhst (
    Dmesh * restrict const       meshptr,
    Gnum                         vertlocnbr,
    Gnum                         vgstlocnbr,
    Gnum *                       vertloctax,
    Gnum *                       vendloctax,
    Gnum *                       edgeloctax,
    Gnum *                       ventgsttax,
    Gnum *                       vertgsttax);

int dmeshGhst2 (
    Dmesh * restrict const       meshptr);

int dmeshGrow (
    Dmesh * const                dmshptr,
    Gnum                         ventlocnum,
    Gnum *                       vtaggsttax,
    Gnum                         seedglbnbr,
    Gnum                         nentlocnum,
    Gnum                         bandval);

int dmeshHaloSync (
    Dmesh * const                meshptr,
    DmeshEntity const * restrict const enttlocptr,
    void * restrict const        attrgsttab,
    const MPI_Datatype           attrglbtyp);

int dmeshHaloSyncComm (
    Dmesh * const                meshptr,
    DmeshEnttComm const * restrict const commlocptr,
    const Gnum                   vertlocnbr,
    void * restrict const        attrgsttab,
    const MPI_Datatype           attrglbtyp);

int dmeshItBuild (
    const Dmesh * const          meshptr,
    Gnum const                   ventlocnum);

int dmeshItData (
    const Dmesh * const          meshptr,
    Gnum const                   ventlocnum,
    Gnum const                   nesblocnum,
    Gnum *                       nentlocnum,
    Gnum *                       ngstlocnbr,
    const Gnum * *               vindloctab,
    const Gnum * *               edgeloctab,
    Gnum *                       edgelocsiz,
    const Gnum * *               svrtloctab,
    const Gnum * *               mngbloctab,
    const Gnum * *               nentlocbas,
    Gnum *                       nentlocmsk,
    const Gnum **                nentloctab,
    const Gnum * *               sngbloctab,
    Gnum *                       vnumlocptr,
    Gnum *                       vnndlocptr,
    Gnum                         langflg);

int dmeshLoad (
    Dmesh * restrict const       meshptr,
    FILE * const                 stream,
    const Gnum                   baseval,
    const Gnum                   flagval);

int dmeshMatInit (
    const Dmesh * const          meshptr,   
    Gnum const                   enttlocnum);

int dmeshOverlap (
    Dmesh * restrict const       meshptr,   
    const Gnum                   vertlocnbr,
    Gnum * const                 vertloctax,
    Gnum * const                 vendloctax,
    const Gnum                   edgelocsiz,
    Gnum * restrict const        edgeloctax,
    Gnum *                       ventloctax,
    Gnum * restrict *            ventgsttax,
    Gnum *                       vertovpnbr,
    Gnum * restrict *            vertovptax,
    Gnum * restrict *            vendovptax,
    Gnum * restrict *            vnumovptax,
    Gnum *                       edgeovpnbr,
    Gnum * restrict *            edgegsttax,
    const Gnum                   ovlpglbval);

int dmeshRedist (
    Dmesh * const                srcmeshptr,
    Gnum * const                 partloctax,
    Gnum * const                 permgsttax,
    Gnum   const                 vertlocdlt,
    Gnum   const                 edgelocdlt,
    Gnum   const                 ovlpglbval,
    Dmesh * const                dstmeshptr);

int dmeshValueLink (
    Dmesh * restrict const       meshptr,
    void **                      valuloctab,
    Gnum                         flagval,
    Gnum *                       sizeval,
    Gnum *                       typesiz,
    MPI_Datatype                 typeval,
    Gnum                         enttnum,
    Gnum                         tagnum);

int dmeshSave (
    Dmesh * restrict const       meshptr,
    FILE * const                 stream);

int dmeshSaveAll (
    Dmesh * restrict const       meshptr,
    FILE * const                 stream);

int dmeshValueTagUnlink (
    Dmesh * restrict const       meshptr,
    Gnum                         tagnum);

int dmeshValueUnlink (
    Dmesh * restrict const       meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum);

int dmeshValueData (
    Dmesh * restrict const       meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                       sizeval,
    Gnum *                       typesiz,
    void **                      valuloctab);


#ifdef MESH_H

int dmeshGather (
    const Dmesh * restrict const dmshptr,
    Mesh * restrict              cmshptr);

int dmeshGatherAll (
    const Dmesh * restrict const dmshptr,
    Mesh * restrict              cmshptr);

int dmeshGatherAll2 (
    const Dmesh * restrict const dmshptr,
    Mesh * restrict              cmshptr,
    const Gnum                   edlosum,
    const int                    protnum);

int dmeshMeshSave (
    Dmesh * restrict const       meshptr,
    File * const                 meshstm,
    File * const                 solstm);

int dmeshSave2 (
    Dmesh * const                meshptr,
    char * const                 fileval,
    int                          (*funptr) (Mesh * const imshptr, Gnum const solflag, void * const dataptr, Gnum * const reftab, char * const fileval),
    void * const                 dataptr);

int dmeshScatter2 (
    Dmesh * restrict const       meshptr,
    const Mesh * restrict const  cmshptr,
    Gnum const                   typenbr,
    Gnum * const                 entttab,
    Gnum * const                 tagtab, 
    MPI_Datatype * const         typetab,
    Gnum   const                 ovlpglbval);

int dmeshScatterValue (
    Dmesh * restrict const       meshptr,
    const Mesh * restrict const  cmshptr,
    Gnum const                   enttnum,
    Gnum const                   tagnum,
    MPI_Datatype                 typeval);


#endif /* DMESH_H */
