
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "dvalues.h"
#include "mesh.h"
#include "smesh.h"
#include "mesh_rebuild.h"
#include "pampa.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine rebuild a given distributed mesh with some centralized
//! meshes which correspond to some part of the previous distributed one.
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

  int
PAMPA_meshRebuild (
    PAMPA_Mesh * restrict const       bmshptr,           //!< Boundary mesh
    PAMPA_Mesh * restrict const       imshptr,           //!< Internal mesh
    PAMPA_Mesh * restrict const       moutptr,           //!< Output mesh
    const PAMPA_Num                  erefnum,           //!< Reference entity number (XXX face)
    const PAMPA_Num                  trefnum,           //!< Tiny entity number (XXX node)
    const PAMPA_Num                  trefnbr,           //!< Number of vertex in ref. ent. number
    const PAMPA_Num                  trefsiz,           //!< Size of treftax
    PAMPA_Num * restrict * restrict const treftab)           //!< Contains permutation of XXX
{

  return meshRebuild ((Mesh *) bmshptr, (Mesh *) imshptr, (Mesh *) moutptr, (Gnum) erefnum, (Gnum) trefnum, (Gnum) trefnbr, (Gnum) trefsiz, (Gnum **) treftab);
}
