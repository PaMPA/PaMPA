/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_check.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module contains the centralized mesh
//!                consistency checking routine. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define MESH_CHECK

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

//! This function checks the consistency
//! of the given distributed mesh.
//! It returns:
//! - 0   : if mesh data are consistent.
//! - !0  : on error.

int
meshCheck (
Mesh * restrict const meshptr)
{
  Gnum                baseval;
  Gnum                ventnum;
  Gnum                enttnnd;
  Gnum                enttnbr;
  Gnum                vertnum;
  Gnum                edgenbr;                 /* Local number of edges                        */
  Gnum                chekval;
  Gnum                chekvl2;
  MeshEntity **      entttax;


  chekvl2 = 0;
  baseval  = meshptr->baseval;
  enttnbr = meshptr->enttnbr;
  entttax = meshptr->entttax;

  if ((meshptr->baseval < 0) ||                   /* Elementary constraints on mesh fields */
      (meshptr->baseval > 1) ||                   /* Strong limitation on base value        */
      (meshptr->edgenbr < 0) ||
      (meshptr->valsptr->valumax < meshptr->valsptr->valunbr)) {
    errorPrint ("inconsistent local mesh data");
    return (PAMPA_ERR_DATA);
  }



  for (ventnum = baseval, enttnnd = enttnbr + baseval, chekval = edgenbr = 0 ;
      ventnum < enttnnd && chekval == 0 ; ventnum ++) {
    MeshEnttNghb ** nghbtax;
    Gnum            vertnnd;
    Gnum            nentnum;
    Gnum            nentnnd;

    if (entttax[ventnum] == NULL)
      continue;

    vertnnd = entttax[ventnum]->vertnbr + baseval;

    if ((meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) // ce n'est pas une sous-entité
        || (vertnnd == 0)) // pas de sommets dans cette entité
      continue;

    nghbtax = entttax[ventnum]->nghbtax;

    for (nentnum = baseval, nentnnd = enttnbr + baseval ;
        nentnum < nentnnd && chekval == 0 ; nentnum ++) {
      MeshVertBounds *   vindtax;
      Gnum                nghbnnd;

      if (entttax[nentnum] == NULL)
        continue;

      if (meshptr->esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { // ce n'est pas une sous-entité

        vindtax = nghbtax[nentnum]->vindtax;

        //if (meshptr->ovlpglbval > 0) 
        nghbnnd = entttax[nentnum]->vertnbr + baseval;
        //else
        //  nghbnnd = entttax[nentnum]->vertnbr;

        for (vertnum = baseval; vertnum < vertnnd  && chekval == 0 ; vertnum ++) {
          Gnum                edgenum;
		  Gnum                mvrtnum;

		  mvrtnum = entttax[ventnum]->mvrttax[vertnum];


          if ((vindtax[vertnum].vendidx < vindtax[vertnum].vertidx) ||
              (vindtax[vertnum].vendidx > (meshptr->edgesiz + baseval))) {
            errorPrint ("inconsistent vertex arrays");
            edgenbr = meshptr->edgenbr;           /* Avoid unwanted cascaded error messages */
            chekval = PAMPA_ERR_VRT_ARRAY;
            break;
          }
          edgenbr += vindtax[vertnum].vendidx - vindtax[vertnum].vertidx;

          for (edgenum = vindtax[vertnum].vertidx;
              edgenum < vindtax[vertnum].vendidx; edgenum ++) {
            if ((meshptr->edgetax[edgenum] < meshptr->baseval) ||
                (meshptr->edgetax[edgenum] > nghbnnd)) {
              errorPrint ("inconsistent edge array");
              edgenbr = meshptr->edgenbr;       /* Avoid unwanted cascaded error messages */
              chekval = PAMPA_ERR_LNK_ARRAY;
              break;
            }
			if (entttax[nentnum]->mvrttax[meshptr->edgetax[edgenum]] == mvrtnum) {
          	  errorPrint ("The vertex %d is linked to itself", mvrtnum);
			  chekval = PAMPA_ERR_LNK_REDEF;
			  break;
			}
          }
        }
      }
    }
  }

  if (chekval != 0)
    return (chekval);

  if (edgenbr != meshptr->edgenbr) {
    errorPrint ("invalid number of edges");
    return (PAMPA_ERR_LNK_NBR);
  }
  
  for (ventnum = baseval, enttnnd = enttnbr + baseval ; ventnum < enttnnd ; ventnum ++) {
    if ((entttax[ventnum] == NULL) || ((ventnum == baseval) && (entttax[ventnum]->vertnbr == 1)))
      continue;


    if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { // ce n'est pas une sous-entité
      Gnum vertnnd;
      Gnum edgeidx;

      for (vertnum = baseval, vertnnd = entttax[ventnum]->vertnbr + baseval ; vertnum < vertnnd ; vertnum ++) {
        Gnum vmshnum;
        Gnum nentnum;
        Gnum nentnnd;

        vmshnum = entttax[ventnum]->mvrttax[vertnum];
        edgeidx = baseval;

        if (entttax[ventnum]->nghbtax[baseval]->vindtax[vertnum].vendidx == entttax[ventnum]->nghbtax[baseval]->vindtax[vertnum].vertidx) {
          errorPrint ("The vertex %d (glb: %d) with entity %d is not linked to an element", vertnum, vmshnum, ventnum);
          //return (PAMPA_ERR_LNK_ELEMENT);
          chekvl2 = PAMPA_ERR_LNK_ELEMENT;
        }

        for (nentnum = baseval, nentnnd = enttnbr + baseval ; nentnum < nentnnd ; nentnum ++) {
          if (entttax[nentnum] == NULL)
            continue;


          if (meshptr->esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { // ce n'est pas une sous-entité
            MeshVertBounds *vindtax;
            Gnum edgenum;
            Gnum edgennd;

            if (((entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx < edgeidx)
                  && (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx != 0)
                  && (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx != 0))
                || (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx < entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx)) {
              errorPrint("Internal error (1)\n");
              return (PAMPA_ERR_INTERNAL);
            }
            edgeidx = (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx != 0) ? entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx != 0 : edgeidx;

            vindtax = entttax[ventnum]->nghbtax[nentnum]->vindtax;

            for (edgenum = vindtax[vertnum].vertidx, edgennd = vindtax[vertnum].vendidx;
                edgenum < edgennd ; edgenum ++) {
              Gnum nmshnum;
              Gnum nvrtnum;
			  Gnum found;
			  Gnum edgenm2;
			  Gnum edgend2;

              nvrtnum = meshptr->edgetax[edgenum];
              nmshnum = entttax[nentnum]->mvrttax[nvrtnum];

              for (found = 0, edgenm2 = edgenum + 1, edgend2 = edgennd;
                  edgenm2 < edgend2 && found == 0; edgenm2 ++) 
				if (meshptr->edgetax[edgenm2] == nvrtnum) {
				  found = 1;
				  errorPrint ("Link %d -> %d is duplicated", vmshnum, nmshnum);
                                  return (PAMPA_ERR_LNK_DUP);
				}

              vindtax = entttax[nentnum]->nghbtax[ventnum]->vindtax;

              for (found = 0, edgenm2 = vindtax[nvrtnum].vertidx, edgend2 = vindtax[nvrtnum].vendidx;
                  edgenm2 < edgend2 ; edgenm2 ++) {
                if (entttax[ventnum]->mvrttax[meshptr->edgetax[edgenm2]] == vmshnum) {
                  found = 1;
                  break;
                }
              }

              if ((!found) && (vindtax[nvrtnum].vertidx != vindtax[nvrtnum].vendidx) && (nentnum != baseval) && (meshptr->esubbax[nentnum & meshptr->esubmsk] != baseval)) {
                errorPrint ("Link %d -> %d is missing", nmshnum, vmshnum);
                return (PAMPA_ERR_LNK_MISSING);
              }
            }
          }
        }
      }
    }
  }








  // XXX fin temp partie à modifier
  // il reste à vérifier :
  //  - que les indices sont croissants dans la nghbloctab
  //  - que tous les sommets soient bien reliés à au moins un sommet d'entité 0


  return (chekvl2);

}

//! This function checks the consistency
//! of the given distributed mesh.
//! It returns:
//! - 0   : if mesh data are consistent.
//! - !0  : on error.

int
meshCheck2 (
Mesh * restrict const meshptr,
int                   flagval)
{
  Gnum                baseval;
  Gnum                ventnum;
  Gnum                enttnnd;
  Gnum                enttnbr;
  Gnum                vertnum;
  Gnum                edgenbr;                 /* Local number of edges                        */
  Gnum                chekval;
  MeshEntity **      entttax;
  
  if (flagval != PAMPA_CHECK_ALONE && flagval != 0) {
    errorPrint ("Not yet implemented");
    return (2);
  }

  baseval  = meshptr->baseval;
  enttnbr = meshptr->enttnbr;
  entttax = meshptr->entttax;

  if ((meshptr->baseval < 0) ||                   /* Elementary constraints on mesh fields */
      (meshptr->baseval > 1) ||                   /* Strong limitation on base value        */
      (meshptr->edgenbr < 0) ||
      (meshptr->valsptr->valumax < meshptr->valsptr->valunbr)) {
    errorPrint ("inconsistent local mesh data");
    return (PAMPA_ERR_DATA);
  }



  for (ventnum = baseval, enttnnd = enttnbr + baseval, chekval = edgenbr = 0 ;
      ventnum < enttnnd && chekval == 0 ; ventnum ++) {
    MeshEnttNghb ** nghbtax;
    Gnum            vertnnd;
    Gnum            nentnum;
    Gnum            nentnnd;

    if (entttax[ventnum] == NULL)
      continue;

    vertnnd = entttax[ventnum]->vertnbr + baseval;

    if ((meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) // ce n'est pas une sous-entité
        || (vertnnd == 0)) // pas de sommets dans cette entité
      continue;

    nghbtax = entttax[ventnum]->nghbtax;

    for (nentnum = baseval, nentnnd = enttnbr + baseval ;
        nentnum < nentnnd && chekval == 0 ; nentnum ++) {
      MeshVertBounds *   vindtax;
      Gnum                nghbnnd;

      if (entttax[nentnum] == NULL)
        continue;

      if (meshptr->esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { // ce n'est pas une sous-entité

        vindtax = nghbtax[nentnum]->vindtax;

        //if (meshptr->ovlpglbval > 0) 
        nghbnnd = entttax[nentnum]->vertnbr + baseval;
        //else
        //  nghbnnd = entttax[nentnum]->vertnbr;

        for (vertnum = baseval; vertnum < vertnnd  && chekval == 0 ; vertnum ++) {
          Gnum                edgenum;
		  Gnum                mvrtnum;

		  mvrtnum = entttax[ventnum]->mvrttax[vertnum];


          if ((vindtax[vertnum].vendidx < vindtax[vertnum].vertidx) ||
              (vindtax[vertnum].vendidx > (meshptr->edgesiz + baseval))) {
            errorPrint ("inconsistent vertex arrays");
            edgenbr = meshptr->edgenbr;           /* Avoid unwanted cascaded error messages */
            chekval = PAMPA_ERR_VRT_ARRAY;
            break;
          }
          edgenbr += vindtax[vertnum].vendidx - vindtax[vertnum].vertidx;

          for (edgenum = vindtax[vertnum].vertidx;
              edgenum < vindtax[vertnum].vendidx; edgenum ++) {
            if ((meshptr->edgetax[edgenum] < meshptr->baseval) ||
                (meshptr->edgetax[edgenum] > nghbnnd)) {
              errorPrint ("inconsistent edge array");
              edgenbr = meshptr->edgenbr;       /* Avoid unwanted cascaded error messages */
              chekval = PAMPA_ERR_LNK_ARRAY;
              break;
            }
			if (entttax[nentnum]->mvrttax[meshptr->edgetax[edgenum]] == mvrtnum) {
          	  errorPrint ("The vertex %d is linked to itself", mvrtnum);
			  chekval = PAMPA_ERR_LNK_REDEF;
			  break;
			}
          }
        }
      }
    }
  }

  if (chekval != 0)
    return (chekval);

  if (edgenbr != meshptr->edgenbr) {
    errorPrint ("invalid number of edges");
    return (PAMPA_ERR_LNK_NBR);
  }
  
  for (ventnum = baseval, enttnnd = enttnbr + baseval ; ventnum < enttnnd ; ventnum ++) {
    if ((entttax[ventnum] == NULL) || ((ventnum == baseval) && (entttax[ventnum]->vertnbr == 1)))
      continue;


    if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { // ce n'est pas une sous-entité
      Gnum vertnnd;
      Gnum edgeidx;

      for (vertnum = baseval, vertnnd = entttax[ventnum]->vertnbr + baseval ; vertnum < vertnnd ; vertnum ++) {
        Gnum vmshnum;
        Gnum nentnum;
        Gnum nentnnd;

        vmshnum = entttax[ventnum]->mvrttax[vertnum];
        edgeidx = baseval;

        if (((flagval & PAMPA_CHECK_ALONE) == 0) && (entttax[ventnum]->nghbtax[baseval]->vindtax[vertnum].vendidx == entttax[ventnum]->nghbtax[baseval]->vindtax[vertnum].vertidx)) {
          errorPrint ("The vertex %d (glb: %d) with entity %d is not linked to an element", vertnum, vmshnum, ventnum);
          return (PAMPA_ERR_LNK_ELEMENT);
        }

        for (nentnum = baseval, nentnnd = enttnbr + baseval ; nentnum < nentnnd ; nentnum ++) {
          if (entttax[nentnum] == NULL)
            continue;


          if (meshptr->esubbax[nentnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) { // ce n'est pas une sous-entité
            MeshVertBounds *vindtax;
            Gnum edgenum;
            Gnum edgennd;

            if (((entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx < edgeidx)
                  && (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx != 0)
                  && (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx != 0))
                || (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx < entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vertidx)) {
              errorPrint("Internal error (1)\n");
              return (PAMPA_ERR_INTERNAL);
            }
            edgeidx = (entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx != 0) ? entttax[ventnum]->nghbtax[nentnum]->vindtax[vertnum].vendidx != 0 : edgeidx;

            vindtax = entttax[ventnum]->nghbtax[nentnum]->vindtax;

            for (edgenum = vindtax[vertnum].vertidx, edgennd = vindtax[vertnum].vendidx;
                edgenum < edgennd ; edgenum ++) {
              Gnum nmshnum;
              Gnum nvrtnum;
			  Gnum found;
			  Gnum edgenm2;
			  Gnum edgend2;

              nvrtnum = meshptr->edgetax[edgenum];
              nmshnum = entttax[nentnum]->mvrttax[nvrtnum];

              for (found = 0, edgenm2 = edgenum + 1, edgend2 = edgennd;
                  edgenm2 < edgend2 && found == 0; edgenm2 ++) 
				if (meshptr->edgetax[edgenm2] == nvrtnum) {
				  found = 1;
				  errorPrint ("Link %d -> %d is duplicated", vmshnum, nmshnum);
				  // return (PAMPA_ERR…);
				}

              vindtax = entttax[nentnum]->nghbtax[ventnum]->vindtax;

              for (found = 0, edgenm2 = vindtax[nvrtnum].vertidx, edgend2 = vindtax[nvrtnum].vendidx;
                  edgenm2 < edgend2 ; edgenm2 ++) {
                if (entttax[ventnum]->mvrttax[meshptr->edgetax[edgenm2]] == vmshnum) {
                  found = 1;
                  break;
                }
              }

              if ((!found) && (vindtax[nvrtnum].vertidx != vindtax[nvrtnum].vendidx) && (nentnum != baseval) && (meshptr->esubbax[nentnum & meshptr->esubmsk] != baseval)) {
                errorPrint ("Link %d -> %d is missing", nmshnum, vmshnum);
                return (PAMPA_ERR_LNK_MISSING);
              }
            }
          }
        }
      }
    }
  }
  
  // XXX fin temp partie à modifier
  // il reste à vérifier :
  //  - que les indices sont croissants dans la nghbloctab
  //  - que tous les sommets soient bien reliés à au moins un sommet d'entité 0

  return (0);

}
