/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_values_f.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the Fortran
//!                interface to the C value accessors.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include <ptscotch.h>
#include "value.h"
#include "dmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine link values with the given tag value and on the given entity
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

FORTRAN (                                       \
FDMESHVALUELINK, fdmeshvaluelink, (   \
PAMPA_Dmesh * const          meshptr,                               /*!< Distributed mesh              */   \
void *                       valuloctab,                            /*!< Values which will be linked   */   \
MPI_Fint * const             typeptr,                               /*!< Type of values                */   \
PAMPA_Num * const            enttnum,                               /*!< Entity number                 */   \
PAMPA_Num * const            tagnum,                                /*!< Tag value                     */   \
PAMPA_Num * const            revaptr),           \
(meshptr, valuloctab, typeptr, enttnum, tagnum, revaptr))
{
  MPI_Datatype        typeval;

  typeval = MPI_Type_f2c (*typeptr);
  *revaptr = PAMPA_dmeshValueLink (meshptr, valuloctab, typeval, *enttnum, *tagnum);
}
