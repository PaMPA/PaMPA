
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define SURFACE_IO_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"


#ifdef PAMPA_ADAPT
extern unsigned char MMG_idir[4][3];
#endif /* PAMPA_ADAPT */


/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
meshSurfaceSave (
Mesh * restrict const     meshptr,
// XXX on devrait utiliser une struct contenant les types d'entités (faces ?, noeuds, éléments)
FILE * const              meshstm,               /* One single centralized mesh stream */
FILE * const              solstm)                /* One single centralized sol stream  */
{
  Gnum baseval;
  Gnum basedif;
  Gnum nodeent;
  Gnum nodenum;
  Gnum nodennd;
  Gnum nodenbr;
  Gnum faceent;
  Gnum offset;
  PAMPA_Iterator it;
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it_tton;
  PAMPA_Num * restrict nreftax;
  PAMPA_Num * restrict ereftax;
  double * restrict geomtax;
  double * restrict soltax;
  int chekval;

  baseval = meshptr->baseval;
  basedif = 1 - baseval;
  faceent = (meshptr->enttnbr > 2) ? 1 : ~0; // XXX temporaire
  nodeent = meshptr->enttnbr - 1 + baseval; // XXX temporaire
  chekval = 0;

  chekval |= (fprintf(meshstm, "MeshVersionFormatted 2\n\nDimension 3\n\nVertices\n") == EOF); // XXX que du 3D ?
  CHECK_VDBG2 (chekval);

  CHECK_FDBG2 (meshValueData(meshptr, nodeent, PAMPA_TAG_GEOM, NULL, NULL, (void **) &geomtax));
  geomtax -= 3 * baseval;

  CHECK_FDBG2 (meshValueData (meshptr, nodeent, PAMPA_TAG_REF, NULL, NULL, (void **) &nreftax));
  nreftax -= baseval;

  nodenbr = meshptr->entttax[nodeent]->vertnbr;
  infoPrint ("nodenbr :" GNUMSTRING, nodenbr);
  chekval |= (fprintf(meshstm, GNUMSTRING "\n", nodenbr) == EOF);
  CHECK_VDBG2 (chekval);

  for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd ; nodenum ++) 
    chekval |= (fprintf(meshstm, "%.16lf %.16lf %.16lf " GNUMSTRING "\n", geomtax[nodenum * 3], geomtax[nodenum * 3 + 1], geomtax[nodenum * 3 + 2], nreftax[nodenum]) == EOF); 
  CHECK_VDBG2 (chekval);

  if (faceent != ~0) {
    PAMPA_Num facenbr;
    PAMPA_Num * restrict freftax;

    facenbr = (meshptr->entttax[faceent] == NULL) ? 0 : meshptr->entttax[faceent]->vertnbr;
    chekval = meshValueData (meshptr, faceent, PAMPA_TAG_REF, NULL, NULL, (void **) &freftax);
    if (chekval == 2) {
      freftax = NULL;
      chekval = 0;
    }
    else
      freftax -= baseval;

    chekval |= (fprintf(meshstm, "\n\nTriangles\n" GNUMSTRING "\n", facenbr) == EOF);
    CHECK_VDBG2 (chekval);

    PAMPA_meshItInitStart((PAMPA_Mesh *) meshptr, faceent, &it);
    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, faceent, nodeent, &it_nghb);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num facenum;
      PAMPA_Num i;
      PAMPA_Num v[3];
      PAMPA_Num v2[3];

      facenum = PAMPA_itCurEnttVertNum(&it);

      PAMPA_itStart(&it_nghb, facenum);

      i = 0;
      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num nodenum;

        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
        v[i ++] = nodenum + 1;
        PAMPA_itNext(&it_nghb);
      }

      if (freftax == NULL)
        chekval |= (fprintf(meshstm, GNUMSTRING " " GNUMSTRING " " GNUMSTRING " " GNUMSTRING "\n", v[0], v[1], v[2], 0) == EOF);
      else
        chekval |= (fprintf(meshstm, GNUMSTRING " " GNUMSTRING " " GNUMSTRING " " GNUMSTRING "\n", v[0], v[1], v[2], freftax[facenum]) == EOF);

      PAMPA_itNext(&it);
    }
  }
  CHECK_VDBG2 (chekval);

  chekval |= (fprintf(meshstm, "\n\nEnd\n") == EOF);


  chekval |= (fprintf(solstm, "MeshVersionFormatted 2\n\nDimension 3\n\nSolAtVertices\n") == EOF); // XXX dimesion 3 ?
  chekval = PAMPA_meshValueData((PAMPA_Mesh*) meshptr, nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
  if (chekval == 0) {  /* If there is a value with this tag */
    offset = 1;
    chekval |= (fprintf(solstm, GNUMSTRING "\n1 1\n", nodenbr) == EOF);
  }
  else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    return (chekval);
  }
  else { /* If there no value with this tag */
    chekval = PAMPA_meshValueData((PAMPA_Mesh*) meshptr, nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
    if (chekval == 0) {  /* If there is a value with this tag */
      offset = 6;
      chekval |= (fprintf(solstm, GNUMSTRING "\n1 3\n", nodenbr) == EOF);
    }
    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (chekval);
    }
    else { /* If there no value with this tag */
      errorPrint ("No associated metric");
      return (chekval);
    }
  }
  soltax -= offset * baseval;

  for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd ; nodenum ++) {
    Gnum tmp;
    Gnum tmpmax;
    for (tmp = 0, tmpmax = MIN (2, offset); tmp < tmpmax; tmp ++)
      chekval |= (fprintf(solstm, "%.16lf ", soltax[nodenum * offset + tmp]) == EOF); 
    if (offset == 6) {
      chekval |= (fprintf(solstm, "%.16lf ", soltax[nodenum * offset + 3]) == EOF); 
      chekval |= (fprintf(solstm, "%.16lf ", soltax[nodenum * offset + 2]) == EOF); 
      for (tmp = 4; tmp < offset; tmp ++)
        chekval |= (fprintf(solstm, "%.16lf ", soltax[nodenum * offset + tmp]) == EOF); 
    }
    chekval |= (fprintf(solstm, "\n") == EOF); 
  }

  chekval |= (fprintf(solstm, "\n\nEnd\n") == EOF);
  CHECK_VDBG2 (chekval);
  return (chekval);
}
