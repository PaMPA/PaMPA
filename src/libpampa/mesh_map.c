/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_map.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contain the centralized mesh
//!                mapping routines. 
//!
//!   \date        Version 1.0: from: 17 Feb 2012
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include <ptscotch.h>
#include "mesh.h"
#include "smesh.h"
#include "smesh_mesh.h"
#include "pampa.h"
#include "mesh_graph.h"

//! \brief  XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
meshMapElem (
Mesh * const            meshptr,              //!< mesh to map
const Gnum               partnbr,              //!< Number of parts 
PAMPA_Arch * const       archptr,              //!< Target architecture
PAMPA_Strat * const      stratptr,             //!< Mapping strategy
Gnum * const             maptax)            //!< Mapping array   
{

  MeshEntity * entttax;
  Gnum   baseval;
  Gnum   vertnbr;
  Gnum   vertnum;
  Gnum   vertnnd;
  Gnum   enttnum;
  Gnum   enttnnd;
  Gnum   vertbas;
  Gnum * restrict gmaptax;
  Gnum * restrict velotax;
  SCOTCH_Graph graph;
  int chekval;

  chekval = 0;

  baseval = meshptr->baseval;
  entttax = meshptr->entttax[baseval];

  CHECK_FDBG2 (meshGraphInit(&graph));

  if (memAllocGroup ((void **) (void *)
        &gmaptax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  CHECK_VDBG2 (chekval);
  gmaptax -= baseval;

  vertnbr = meshptr->vertnbr;

  chekval = meshValueData(meshptr, 0, PAMPA_TAG_LOAD, NULL, NULL, (void **) &velotax);
  if (chekval != 0) { /* If there is a value with this tag */
    if (chekval != 2) // XXX mettre un tag au lieu de 2
      return chekval;
    velotax = NULL;
    chekval = 0;
  }

  CHECK_FDBG2 (meshGraphBuild(meshptr, MESH_ENTT_MAIN, ~0, NULL, velotax, NULL, &graph, NULL)); // XXX pas de edlo ??

#ifdef PAMPA_DEBUG_GRAPH
  char s[30];
  sprintf(s, "graph-map-%d", meshptr->procnum);
  FILE *graph_file;
  graph_file = fopen(s, "w");
  SCOTCH_graphSave (&graph, graph_file);
  fclose(graph_file);

#endif /* PAMPA_DEBUG_GRAPH */
#ifdef PAMPA_INFO_PART
  time_t begin = time (NULL);
#endif /* PAMPA_INFO_PART */
  if (archptr == NULL) {
    //CHECK_FDBG2 (SCOTCH_graphPart(&graph, partnbr, (SCOTCH_Strat * const) stratptr, gmaptax + baseval), meshptr->proccomm);
    //XXX debut tmp
    SCOTCH_Arch archdat;
    SCOTCH_Mapping mappdat;
    SCOTCH_archInit  (&archdat);
    SCOTCH_archCmplt (&archdat, partnbr);

    //CHECK_FDBG2 (SCOTCH_graphPart(&graph, partnbr, (SCOTCH_Strat * const) stratptr, gmaptax + baseval), meshptr->proccomm);
    SCOTCH_graphMapInit    (&graph, &mappdat, &archdat, gmaptax + baseval);
    SCOTCH_graphMapCompute (&graph, &mappdat, (SCOTCH_Strat * const) stratptr); /* Perform mapping */
    SCOTCH_graphMapExit    (&graph, &mappdat);
    //XXX fin tmp

  }
  else {
#ifdef NET
    //CHECK_FDBG2 (SCOTCH_archFree ((SCOTCH_Arch * const) archptr), meshptr->proccomm);
    //CHECK_FDBG2 (netscotch_build_current_arch((SCOTCH_Arch * const) archptr), meshptr->proccomm);
#endif /* NET */
    CHECK_FDBG2 (SCOTCH_graphMap(&graph, (SCOTCH_Arch * const) archptr, (SCOTCH_Strat * const) stratptr, gmaptax + baseval));
  }
#ifdef PAMPA_INFO_PART
  infoPrint ("%s Elapsed time %lu secondes", "graphPart", (time(NULL) - begin)); 
#endif /* PAMPA_INFO_PART */

  memSet( maptax + meshptr->baseval, ~0, vertnbr * sizeof(Gnum));

  // Converting gmaptax to maptax for main entity
  for (vertnum = baseval, vertnnd = entttax->vertnbr + baseval; vertnum < vertnnd; vertnum ++)  {
    maptax[entttax->mvrttax[vertnum]] = gmaptax[vertnum];
  }

  // Luby algorythm for each vertex matching neighors with main entity
  for (enttnum = baseval + 1, enttnnd = meshptr->enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
    if ((meshptr->esubbax[enttnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) || (meshptr->entttax[enttnum] == NULL))
      continue;

    MeshEnttNghb * nghbtax;

    nghbtax = meshptr->entttax[enttnum]->nghbtax[baseval];

    for (vertnum = baseval, vertnnd = meshptr->entttax[enttnum]->vertnbr + baseval; vertnum < vertnnd; vertnum ++)  {
      Gnum vmshnum, nghbnum;

      nghbnum = meshptr->edgetax[nghbtax->vindtax[vertnum].vertidx];

      vmshnum = meshptr->entttax[enttnum]->mvrttax[vertnum];

      maptax[vmshnum] = gmaptax[nghbnum];
    }

  }

  meshGraphExit(&graph);

  memFreeGroup (gmaptax + baseval);
  CHECK_VDBG2 (chekval);
  return (0);
}



//! \brief  XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
meshMapVert (
Mesh * const            meshptr,              //!< mesh to map
const Gnum               partnbr,              //!< Number of parts 
PAMPA_Arch * const       archptr,              //!< Target architecture
PAMPA_Strat * const      stratptr,             //!< Mapping strategy
Gnum * const             maptax)            //!< Mapping array   
{

  MeshEntity * entttax;
  Gnum   baseval;
  Gnum   enttnum;
  Gnum   enttnnd;
  Gnum   vertnbr;
  Gnum   vertnum;
  Gnum   vertnnd;
  Gnum   vertbas;
  Gnum * gmaptax;
  Gnum * vlbltax;
  SCOTCH_Graph graph;
  int chekval;

  chekval = 0;

  baseval = meshptr->baseval;
  entttax = meshptr->entttax[baseval];
  //infoPrint("PartVert");
  
  //XXX mettre un avertissement disant qu'on n'utilise pas PAMPA_TAG_LOAD sur
  //les sommets

  CHECK_FDBG2 (meshGraphInit(&graph));

  CHECK_FDBG2 (meshGraphBuild(meshptr, MESH_ENTT_ANY, ~0, NULL, NULL, NULL, &graph, NULL)); // XXX pas de velo ni de edlo ??

#ifdef PAMPA_DEBUG_GRAPH
  char s[30];
  sprintf(s, "graph-map-%d", meshptr->procnum);
  FILE *graph_file;
  graph_file = fopen(s, "w");
  SCOTCH_graphSave (&graph, graph_file);
  fclose(graph_file);


#endif /* PAMPA_DEBUG_GRAPH */
  if (memAllocGroup ((void **) (void *)
        &gmaptax, (size_t) (meshptr->vertnbr * sizeof (Gnum)), // XXX on a pas vtrunbr ??
        NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  CHECK_VDBG2 (chekval);
  gmaptax -= baseval;

  memSet( maptax + meshptr->baseval, ~0, meshptr->vertnbr * sizeof(Gnum));

  time_t begin = time (NULL);
  if (archptr == NULL) {
    CHECK_FDBG2 (SCOTCH_graphPart(&graph, partnbr, (SCOTCH_Strat * const) stratptr, gmaptax + baseval));
  }
  else {
    CHECK_FDBG2 (SCOTCH_graphMap(&graph, (SCOTCH_Arch * const) archptr, (SCOTCH_Strat * const) stratptr, gmaptax + baseval));
  }
#ifdef PAMPA_INFO_PART
  infoPrint ("%s Elapsed time %lu secondes", "graphPart", (time(NULL) - begin)); 
#endif /* PAMPA_INFO_PART */

  SCOTCH_graphData(&graph, NULL, &vertnbr, NULL, NULL, NULL, &vlbltax, NULL, NULL, NULL); 
  vlbltax -= baseval;

#ifdef PAMPA_DEBUG_MESH
  if (vertnbr != meshptr->vertnbr) {
    errorPrint ("Invalid vertex number between mesh and graph");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  for (vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
    maptax[vlbltax[vertnum]] = gmaptax[vertnum];

  //// XXX on traite les sommets qui sont sur un proc ne correspondant pas à au
  //// moins un élément voisin
  //// XXX partie à finir : l'élément voisin n'est pas forcément al,
  //// du coup, nmshnum - vertbas est faux
  //vertbas = meshptr->procvrttab[meshptr->procnum] - baseval;
  //for (enttnum = baseval + 1, enttnnd = meshptr->enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
  //  Gnum nmshnum;

  //  if ((meshptr->esubbax[enttnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) || (meshptr->entttax[enttnum] == NULL))
  //    continue;

  //  MeshEnttNghb * nghbtax;

  //  nghbtax = meshptr->entttax[enttnum]->nghbtax[baseval];

  //  for (vertnum = baseval, vertnnd = meshptr->entttax[enttnum]->vertnbr + baseval; vertnum < vertnnd; vertnum ++)  {
  //    Gnum vmshnum;
  //    Gnum edgenum;
  //    Gnum edgennd;

  //    for (edgenum = nghbtax->vindtax[vertnum].vertidx, edgennd = nghbtax->vindtax[vertnum].vendidx; edgenum < edgennd; edgenum ++)  {
  //      Gnum nghbnum;

  //      nghbnum = meshptr->edgetax[edgenum];

  //      vmshnum = meshptr->entttax[enttnum]->mvrttax[vertnum];
  //      nmshnum = meshptr->entttax[baseval]->mvrttax[nghbnum];

  //      if (maptax[vmshnum - vertbas] == maptax[nmshnum - vertbas])
  //        break;
  //    }
  //    if (edgenum == nghbtax->vindtax[vertnum].vendidx)
  //      maptax[vmshnum - vertbas] = maptax[nmshnum - vertbas];
  //  }
  //}
  //// XXX fin partie à finir

  meshGraphExit(&graph);
  memFreeGroup (gmaptax + baseval);

  CHECK_VDBG2 (chekval);
  return (0);
}


int
meshPart (
Mesh * const             meshptr,              //!< mesh to map
const Gnum               partnbr,              //!< Number of parts 
PAMPA_Strat * const      stratptr,             //!< Mapping strategy
Gnum * const             maptax)               //!< Mapping array   
{

  MeshEntity * entttax;
  Gnum   baseval;
  Gnum   edgenbr;
  Gnum   vertnbr;
  Gnum   vertnum;
  Gnum   vertnnd;
  Gnum * verttax;
  Gnum * vendtax;
  Gnum * gmaptax;
  Gnum   chekval;
  Gnum   enttnum;
  Gnum   enttnnd;
  SCOTCH_Graph graph;
  Smesh smshdat;

  smeshInit (&smshdat);
  mesh2smesh (meshptr, &smshdat);

  baseval = meshptr->baseval;
  //entttax = meshptr->entttax[baseval];

  //if (memAllocGroup ((void **) (void *)
  //      &verttax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
  //      &vendtax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
  //      &gmaptax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
  //      (void *) NULL) == NULL) {
  //  errorPrint  ("out of memory");
  //  return      (1);
  //}
  //verttax -= baseval;
  //vendtax -= baseval;
  //gmaptax -= baseval;


  //for (edgenbr = 0, vertnum = baseval, vertnnd = entttax->vertnbr + baseval;
  //    vertnum < vertnnd; vertnum ++)  {
  //  verttax[vertnum] = entttax->nghbtax[baseval]->vindtax[vertnum].vertidx;
  //  vendtax[vertnum] = entttax->nghbtax[baseval]->vindtax[vertnum].vendidx;
  //  edgenbr += vendtax[vertnum] - verttax[vertnum];
  //}

  //SCOTCH_graphBuild(&graph,
  //    meshptr->baseval,
  //    entttax->vertnbr,
  //    verttax + baseval,
  //    vendtax + baseval,
  //    NULL,
  //    NULL,
  //    edgenbr,
  //    meshptr->edgetax + baseval,
  //    NULL);
  // XXX FIXME et le poids sur les sommets ???
  SCOTCH_graphBuild (&graph, baseval, smshdat.vertnbr, smshdat.verttax + baseval, smshdat.vendtax + baseval, NULL, NULL, smshdat.edgenbr, smshdat.edgetax + baseval, smshdat.edlotax + baseval);

#ifdef PAMPA_DEBUG_MESH
  chekval = SCOTCH_graphCheck(&graph);
  if (chekval != 0)
    return chekval;
#endif /* PAMPA_DEBUG_MESH */


  chekval = SCOTCH_graphPart(&graph, partnbr, (SCOTCH_Strat * const) stratptr, maptax + meshptr->baseval);
  if (chekval != 0)
    return chekval;
  smeshExit (&smshdat);

//#ifdef PAMPA_DEBUG_GRAPH
//  {
//    double *coortax;
//    FILE *graph_file, *graph_file2;
//    graph_file = fopen("graph-map.grf", "w");
//    SCOTCH_graphSave (&graph, graph_file);
//    fclose(graph_file);
//
//    PAMPA_Iterator it, it_nghb;
//    Gnum tetentt, nodentt;
//    double * coorloctax;
//    int i;
//
//    tetentt = 0;
//    nodentt = 1;
//    meshValueData(meshptr, nodentt, PAMPA_TAG_GEOM, NULL, NULL, (void **) &coortax);
//    coortax -= 3 * baseval;
//    graph_file = fopen("graph-map.xyz", "w");
//    graph_file2 = fopen("graph-map.map", "w");
//    fprintf (graph_file, "3\n%d\n", meshptr->entttax[baseval]->vertnbr);
//    fprintf (graph_file2, "%d\n", meshptr->entttax[baseval]->vertnbr);
//
//    PAMPA_meshItInitStart((PAMPA_Mesh *) meshptr, tetentt, &it);
//    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, tetentt, nodentt, &it_nghb);
//    while (PAMPA_itHasMore(&it)) {
//      PAMPA_Num tetnum;
//      double coorval[3];
//
//      tetnum = PAMPA_itCurEnttVertNum(&it);
//      coorval[0] =
//        coorval[1] =
//        coorval[2] = 0.0;
//
//
//      PAMPA_itStart(&it_nghb, tetnum);
//
//      while (PAMPA_itHasMore(&it_nghb)) {
//        PAMPA_Num nodenum;
//
//        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
//        for (i = 0; i < 3; i++)
//          coorval[i] += coortax[nodenum * 3 + i];
//        PAMPA_itNext(&it_nghb);
//      }
//
//      fprintf(graph_file, "%d", tetnum);
//      fprintf(graph_file2, "%d %d\n", tetnum, gmaptax[tetnum]);
//      for (i = 0; i < 3; i++)
//        fprintf(graph_file, " %lf", coorval[i] / 4);
//      fprintf(graph_file, "\n");
//
//      PAMPA_itNext(&it);
//    }
//    fclose (graph_file); 
//    fclose (graph_file2); 
//  }
//#endif /* PAMPA_DEBUG_GRAPH */
//
//  vertnbr = 0; // XXX ne doit-on pas stocker le nombre de sommets locaux ????
//  for (enttnum = baseval, enttnnd = meshptr->enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
//    if (meshptr->esubbax[enttnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE)
//      continue;
//    vertnbr += meshptr->entttax[enttnum]->vertnbr;
//  }
//
//  memSet( maptax + meshptr->baseval, ~0, vertnbr * sizeof(PAMPA_Num));
//
//  // Converting gmaptax to maptax for main entity
//  for (vertnum = baseval, vertnnd = entttax->vertnbr + baseval; vertnum < vertnnd; vertnum ++)  {
//    maptax[entttax->mvrttax[vertnum]] = gmaptax[vertnum];
//  }
//
//  // Luby algorythm for each vertex matching neighors with main entity
//  for (enttnum = baseval + 1, enttnnd = meshptr->enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {
//    if (meshptr->esubbax[enttnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE)
//      continue;
//
//    MeshEnttNghb * nghbtax;
//
//    nghbtax = meshptr->entttax[enttnum]->nghbtax[baseval];
//
//    for (vertnum = baseval, vertnnd = meshptr->entttax[enttnum]->vertnbr + baseval; vertnum < vertnnd; vertnum ++)  {
//      PAMPA_Num vmshnum, nghbnum, nmshnum;
//
//      nghbnum = meshptr->edgetax[nghbtax->vindtax[vertnum].vertidx];
//      nmshnum = entttax->mvrttax[nghbnum];
//
//      vmshnum = meshptr->entttax[enttnum]->mvrttax[vertnum];
//
//      maptax[vmshnum] = maptax[nmshnum];
//    }
//
//  }
//
//  memFreeGroup (verttax + baseval);
  return (0);
}



//! \brief  XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
meshRepart (
Mesh * const             meshptr,              //!< mesh to map
const Gnum               partnbr,              //!< Number of parts 
PAMPA_Strat * const      stratptr,             //!< Mapping strategy
Gnum * const             gmaptax)               //!< Mapping array   
{

  MeshEntity * entttax;
  Gnum   baseval;
  Gnum   edgenbr;
  Gnum   vertnbr;
  Gnum   vertnum;
  Gnum   vertnnd;
  Gnum * verttax;
  Gnum * vendtax;
  Gnum * omaptax;
  Gnum   chekval;
  Gnum   enttnum;
  Gnum   enttnnd;
  SCOTCH_Graph graph;


  baseval = meshptr->baseval;
  entttax = meshptr->entttax[baseval];

  if (memAllocGroup ((void **) (void *)
        &verttax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
        &vendtax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
        &omaptax, (size_t) (entttax->vertnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  verttax -= baseval;
  vendtax -= baseval;
  omaptax -= baseval;

  memCpy (omaptax + baseval, gmaptax + baseval, entttax->vertnbr * sizeof (Gnum));

  for (edgenbr = 0, vertnum = baseval, vertnnd = entttax->vertnbr + baseval;
      vertnum < vertnnd; vertnum ++)  {
    verttax[vertnum] = entttax->nghbtax[baseval]->vindtax[vertnum].vertidx;
    vendtax[vertnum] = entttax->nghbtax[baseval]->vindtax[vertnum].vendidx;
    edgenbr += vendtax[vertnum] - verttax[vertnum];
  }

  SCOTCH_graphBuild(&graph,
      meshptr->baseval,
      entttax->vertnbr,
      verttax + baseval,
      vendtax + baseval,
      NULL,
      NULL,
      edgenbr,
      meshptr->edgetax + baseval,
      NULL);

#ifdef PAMPA_DEBUG_MESH
  chekval = SCOTCH_graphCheck(&graph);
  if (chekval != 0)
    return chekval;
#endif /* PAMPA_DEBUG_MESH */

#ifdef PAMPA_DEBUG_GRAPH
  {
    double *coortax;
    FILE *graph_file, *graph_file2;
    graph_file = fopen("graph-remap.grf", "w");
    SCOTCH_graphSave (&graph, graph_file);
    fclose(graph_file);

    PAMPA_Iterator it, it_nghb;
    Gnum tetentt, nodentt;
    double * coorloctax;
    int i;

    tetentt = 0;
    nodentt = 1;
    meshValueData(meshptr, nodentt, PAMPA_TAG_GEOM, NULL, NULL, (void **) &coortax);
    coortax -= 3 * baseval;
    graph_file = fopen("graph-remap.xyz", "w");
    graph_file2 = fopen("graph-remap-old.map", "w");
    fprintf (graph_file, "3\n%d\n", meshptr->entttax[baseval]->vertnbr);
    fprintf (graph_file2, "%d\n", meshptr->entttax[baseval]->vertnbr);

    PAMPA_meshItInitStart((PAMPA_Mesh *) meshptr, tetentt, &it);
    PAMPA_meshItInit((PAMPA_Mesh *) meshptr, tetentt, nodentt, &it_nghb);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num tetnum;
      double coorval[3];

      tetnum = PAMPA_itCurEnttVertNum(&it);
      coorval[0] =
        coorval[1] =
        coorval[2] = 0.0;


      PAMPA_itStart(&it_nghb, tetnum);

      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num nodenum;

        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
        for (i = 0; i < 3; i++)
          coorval[i] += coortax[nodenum * 3 + i];
        PAMPA_itNext(&it_nghb);
      }

      fprintf(graph_file, "%d", tetnum);
      fprintf(graph_file2, "%d %d\n", tetnum, gmaptax[tetnum]);
      for (i = 0; i < 3; i++)
        fprintf(graph_file, " %lf", coorval[i] / 4);
      fprintf(graph_file, "\n");

      PAMPA_itNext(&it);
    }
    fclose (graph_file); 
    fclose (graph_file2); 
  }
#endif /* PAMPA_DEBUG_GRAPH */

  chekval = SCOTCH_graphRepart(&graph, partnbr, omaptax + meshptr->baseval, 0.01, NULL, (SCOTCH_Strat * const) stratptr, gmaptax + meshptr->baseval);
  if (chekval != 0)
    return chekval;

#ifdef PAMPA_DEBUG_GRAPH
  {
    FILE *graph_file2;
    PAMPA_Iterator it;
    Gnum tetentt;

    tetentt = 0;
    graph_file2 = fopen("graph-remap-new.map", "w");
    fprintf (graph_file2, "%d\n", meshptr->entttax[baseval]->vertnbr);

    PAMPA_meshItInitStart((PAMPA_Mesh *) meshptr, tetentt, &it);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num tetnum;
      tetnum = PAMPA_itCurEnttVertNum(&it);
      fprintf(graph_file2, "%d %d\n", tetnum, gmaptax[tetnum]);
      PAMPA_itNext(&it);
    }
    fclose (graph_file2); 
  }
#endif /* PAMPA_DEBUG_GRAPH */

  memFreeGroup (verttax + baseval);
  return (0);
}



