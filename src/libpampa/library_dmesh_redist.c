/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_redist.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API for the
//!                distributed mesh redistribution routines
//!                of the libpampa library. 
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/
// toto c
/************************************************************/
//!
//!   \file        library_dmesh_redist.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API for the distributed  source mesh handling routines of  the libpampa library.                   
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   20 Jul 2016
//!
/************************************************************/
// toto c
/************************************************************/
//!
//!   \file        library_dmesh_redist.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API for the distributed  source mesh handling routines of  the libpampa library.                   
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   20 Jul 2016
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include <ptscotch.h>
#include "dmesh.h"
#include "pampa.h"

/************************************/
/*                                  */
/* These routines are the C API for */
/* the graph handling routines.     */
/*                                  */
/************************************/

//! This routine computes another distributed mesh which corresponds to the
//! partition. The communicator is duplicated
//!
//! \returns 0   : if the computation succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshRedist (
PAMPA_Dmesh * const        smshptr,                   //!< Source distributed mesh
PAMPA_Num * const          partloctab,                //!< Partition local array
PAMPA_Num * const          permgsttab,                //!< Permutation ghost array
PAMPA_Num   const          vertlocdlt,                //!< Delta between number of vertices
PAMPA_Num   const          edgelocdlt,                //!< Delta between number of edges
PAMPA_Num const            ovlpglbval,                //!< Overlap value
PAMPA_Dmesh * const        dmshptr)                   //!< Destination distributed mesh
{
  Dmesh * srcmeshptr;
  Gnum * permgsttax;
  Gnum   baseval;

  srcmeshptr = (Dmesh *) smshptr;
  baseval = srcmeshptr->baseval;

  permgsttax = ((permgsttab == partloctab) || (permgsttab == NULL)) ? NULL : ((Gnum *) permgsttab) - baseval;


  return (dmeshRedist (srcmeshptr, partloctab - baseval, permgsttax, vertlocdlt, edgelocdlt, ovlpglbval, (Dmesh *) dmshptr));
}

