/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dsmesh_build.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the distributed
//!                simplified source mesh building routines. 
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define SMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dsmesh.h"
#include "pampa.h"

//! This routine builds a mesh from
//! the local arrays that are passed to it. If
//! a vertex label array is given, it is assumed
//! that edge ends are given with respect to these
//! labels, and thus they are updated so as to be
//! given with respect to the implicit (based)
//! global numbering.
//! As for all routines that build meshs, the private
//! fields of the mesh structure have to be initialized
//! if they are not already.
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
dsmeshBuild (
    Dsmesh * restrict const       smshptr,           //!< mesh
    const Gnum                  baseval,           //!< Base for indexing
    const Gnum                  vertlocnbr,           //!< Number of local vertlocices
	const Gnum                  vertlocmax,
    Gnum * const                vertloctax,           //!< Local vertlocex begin array
    Gnum * const                vendloctax,           //!< Local vertlocex end array
    const Gnum                  edgelocnbr,           //!< Number of local edgelocs
	const Gnum                  edgelocsiz,
    Gnum * restrict const       edgeloctax,           //!< Local edgeloc array
    Gnum * restrict const       edloloctax,           //!< Local edgeloc load array (if any)
    Gnum                        enttglbnbr,		   //!< XXX
    Gnum *                      ventloctax,		   //!< XXX
    Gnum *                      esubloctax,		   //!< XXX
    Gnum                        valuglbmax,
	const Gnum                  ovlpglbval,
	Dvalues *                    valslocptr,
	Gnum                        degrlocmax,		   //!< XXX
  	Gnum *                    procvrttab,
  	Gnum *                    proccnttab,
  	Gnum *                    procdsptab,
  	int                       procngbnbr,
  	int                       procngbmax,
  	int *                     procngbtab)

{
  Gnum                  vertlocnum;
  Gnum                  vertlocnnd;
  int                   cheklocval;

  //static Gnum     enttsingle = -1; // XXX à changer, peut-être utiliser DUMMY…

  smshptr->baseval = baseval;    /* set global and local data */

  //if (esubloctax != NULL) {
  //  if ((esublocbax = (Gnum *) memAlloc (enttglbnbr * sizeof (Gnum))) == NULL) {
  //    errorPrint  ("out of memory");
  //    return      (1);
  //  }
  //  esublocbax -= baseval;

  //  memCpy (esublocbax + baseval, esubloctax + baseval, enttglbnbr * sizeof (Gnum));
  //  smshptr->esublocmsk = ~((Gnum) 0);
  //}
  //else {
  //  esublocbax = &enttsingle;
  //  smshptr->esublocmsk = 0;
  //}
  //smshptr->esublocbax = (Gnum *) esublocbax;

  smshptr->baseval = baseval;
  smshptr->enttglbnbr = enttglbnbr;
  smshptr->vertlocnbr = vertlocnbr;
  smshptr->vertlocmax = vertlocmax;
  smshptr->ventloctax = ventloctax;
  smshptr->esubloctax = esubloctax;
  smshptr->vertloctax = vertloctax;
  smshptr->vendloctax = vendloctax;
  smshptr->edgelocnbr = edgelocnbr;
  smshptr->edgelocsiz = edgelocsiz;
  smshptr->edgeloctax = edgeloctax;
  smshptr->edloloctax = edloloctax;
  smshptr->ovlpglbval = ovlpglbval;

  if (degrlocmax == -1) {
  	degrlocmax = 0;
  	for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval;
      	vertlocnum < vertlocnnd; vertlocnum ++)  {

      if (vendloctax[vertlocnum] - vertloctax[vertlocnum] > degrlocmax)
      	degrlocmax = vendloctax[vertlocnum] - vertloctax[vertlocnum];
  	}
  }

  smshptr->degrlocmax = degrlocmax;

  if (procvrttab == NULL) {
    cheklocval = dsmeshBuild2 (smshptr, baseval, vertlocnbr, vertlocmax);
    if (cheklocval != 0)
      return cheklocval;
  }
  else {
	smshptr->flagval |= DSMESHFREEPRIV;
	(*smshptr->procloccnt) ++;
	smshptr->procvrttab = procvrttab;
	smshptr->proccnttab = proccnttab;
	smshptr->procdsptab = procdsptab;
	smshptr->procngbnbr = procngbnbr;
	smshptr->procngbmax = procngbmax;
	smshptr->procngbtab = procngbtab;
  }

  if (valslocptr == NULL)
  	return dvaluesInit(&smshptr->valslocptr, baseval, valuglbmax, enttglbnbr);
  else {
	valslocptr->cuntlocnbr ++;
	smshptr->valslocptr = valslocptr;
	return (0);
  }

}

/** This routine builds a distributed mesh from
 ** the local arrays that are passed to it. If
 ** a vertex label array is given, it is assumed
 ** that edge ends are given with respect to these
 ** labels, and thus they are updated so as to be
 ** given with respect to the implicit (based)
 ** global numbering.
 ** As for all routines that build meshes, the private
 ** fields of the Dmesh structure have to be initialized
 ** if they are not already.
 ** These meshes do not have holes, since procvrttab
 ** points to procdsptab.
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

int
dsmeshBuild2 (
    Dsmesh * restrict const smshptr,              /**< mesh */
    const Gnum              baseval,              /**< Base for indexing */
    const Gnum              vertlocnbr,           /**< Number of local vertices */
    const Gnum              vertlocmax)           /**< Maximum number of local vertices */
{
  Gnum                  procnum;
  int                   reduloctab[2];

#ifdef PAMPA_DEBUG_DMESH2
  if (vertlocmax < vertlocnbr) {
    errorPrint ("invalid parameters");
    return     (1);
  }
#endif /* PAMPA_DEBUG_DMESH2 */

  if (smshptr->procdsptab == NULL) {              /* If private data not yet allocated */
    int                   procglbnbr;

    procglbnbr = smshptr->procglbnbr;
    if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
          &smshptr->procdsptab, (size_t) ((procglbnbr + 1) * sizeof (Gnum)),
          &smshptr->procvrttab, (size_t) ((procglbnbr + 1) * sizeof (Gnum)),
#ifndef PAMPA_DEBUG_MEM
          &smshptr->procngbtab, (size_t) (procglbnbr       * sizeof (int)),
#else /* PAMPA_DEBUG_MEM */
          &smshptr->procngbtab, (size_t) (procglbnbr * 2   * sizeof (int)),
#endif /* PAMPA_DEBUG_MEM */
          &smshptr->proccnttab, (size_t) (procglbnbr       * sizeof (Gnum)),
		  &smshptr->procloccnt, (size_t) (                   sizeof (Gnum)), (void *) NULL) == NULL) {
      Gnum                dummyval[2];
      Gnum                dummytab[procglbnbr * 2];

      errorPrint ("out of memory");
      dummyval[0] =
        dummyval[1] = -1;
      if (commAllgather (dummyval, 2, MPI_INT,    /* Use dummy receive array (if can be allocated too) */
            dummytab, 2, MPI_INT, smshptr->proccomm) != MPI_SUCCESS)
        errorPrint ("communication error");
      return (1);
    }
	*smshptr->procloccnt = 1;
  }

  reduloctab[0] = (int) vertlocnbr;
  reduloctab[1] = (int) vertlocmax;
  if (commAllgather (reduloctab, 2, MPI_INT, /* Use procngbtab and proccnttab as a joint allreduce receive array */
        smshptr->procngbtab, 2, MPI_INT, smshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return     (1);
  }

  smshptr->procdsptab[0] =                        /* Build vertex-to-process array */
    smshptr->procvrttab[0] = baseval;
  for (procnum = 0; procnum < smshptr->procglbnbr; procnum ++) {
    if (smshptr->procngbtab[procnum] < 0) {       /* If error notified by another process during memory allocation */
      memFree (smshptr->procdsptab);
      smshptr->procdsptab = NULL;                 /* Free memory that has just been allocated */
      return (1);
    }

    smshptr->procdsptab[procnum + 1] = smshptr->procdsptab[procnum] + (Gnum) smshptr->procngbtab[2 * procnum];
    smshptr->procvrttab[procnum + 1] = smshptr->procvrttab[procnum] + (Gnum) smshptr->procngbtab[2 * procnum + 1];
  }

  for (procnum = 0; procnum < smshptr->procglbnbr; procnum ++) {
    smshptr->proccnttab[procnum]     = smshptr->procdsptab[procnum + 1]     - smshptr->procdsptab[procnum];
  }

  smshptr->flagval |= DSMESHFREEPRIV;
  return (0);
}

