/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dsmesh.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the distributed simplified mesh handling
//!                routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/



#define DSMESH_H

/*
** The defines.
*/

/* mesh flags. */ // XXX il ne faut pas dupliquer les drapeaux ni les tags FIXME

#define DSMESHNONE                  0x0000        //!< No options set

#define DSMESHFREEPRIV              0x0001        //!< Set if private arrays freed on exit
#define DSMESHFREECOMM              0x0002        //!< MPI communicator has to be freed
#define DSMESHFREETABS              0x0004        //!< Set if ventloctax freed on exit
#define DSMESHFREEALL               (DMESHFREEPRIV | DMESHFREETABS)
#define DSMESHCOMMPTOP              0x0080        //!< Use point-to-point collective communication




/* Tags used for point-to-point communications. */

typedef enum DsmeshTag_   
{
  STAGPROCVRTTAB = 0,                              //!< procvrttab message
  STAGHALO    = 100,                               /*+ Tag class for halo       +*/
} DsmeshTag;






/*
** The type and structure definitions.
*/

/* The mesh basic types, which must be signed. */

#ifndef GNUMMAX                                   
typedef INT                 Gnum;                 //!< Vertex or edge number
#define GNUMMAX                     (INTVALMAX)   //!< Maximum Gnum value
#endif //!< GNUMMAX

#define GNUM_MPI                    COMM_INT      //!< MPI type for Gnum is MPI type for INT



typedef int DsmeshFlag;                           //!< Mesh property flags



typedef struct DsmeshEnttComm_
{
  int *                procrcvtab;    //!< Number of vertices to receive in ghost vertex sub-arrays
  int                  procsndnbr;    //!< Overall size of local send array
  int *                procsndtab;    //!< Number of vertices to send in ghost vertex sub-arrays
  int *                procsidtab;    //!< Array of indices to build communication vectors (send)
  int                  procsidnbr;    //!< Size of the send index array
} DsmeshEnttComm;


  
typedef struct Dsmesh_   
{
  DsmeshFlag            flagval;       //!< Mesh properties
  Gnum                 baseval;       //!< Base index for edge/vertex/entity arrays
  Gnum                 ovlpglbval;    //!< Overlap value
  Gnum                 enttglbnbr;    //!< Number of entities which make up the dsmesh
  Gnum                 vertlocnbr;
  Gnum                 vertlocmax;
  Gnum                 vgstlocnbr;
  Gnum *               ventloctax;
  Gnum *               vertloctax;
  Gnum *               vendloctax;
  Gnum *               esubloctax;    //!< XXX
  Gnum                 degrlocmax;    //!< XXX
  Gnum                 edgelocnbr;    //!< XXX
  Gnum                 edgelocsiz;    //!< XXX
  Gnum *               edgeloctax;    //!< XXX
  Gnum *               edgegsttax;    //!< XXX
  Gnum *               edloloctax;    //!< XXX
  Dvalues *            valslocptr;    //!< Pointer on associated values structure
  MPI_Comm             proccomm;      //!< Mesh communicator
  DsmeshEnttComm *     commlocptr;
  int                  procglbnbr;    //!< Number of processes sharing mesh data
  int                  proclocnum;    //!< Number of this process
  Gnum *               procvrttab;    //!< Global array of vertex number ranges [1,based]
  Gnum *               proccnttab;    //!< Count array for local number of vertices
  Gnum *               procdsptab;    //!< Displacement array with respect to proccnttab [1,based]
  int                  procngbnbr;    //!< Number of neighboring processes
  int                  procngbmax;    //!< Maximum number of neighboring processes
  int *                procngbtab;    //!< Array of neighbor process numbers [sorted]
  Gnum *               procloccnt;    //!< Counter on duplicated allocated arrays procvrttab,...
} Dsmesh;

/*
** The functions prototypes.
*/

int dsmeshInit (
    Dsmesh * const                smshptr,
    MPI_Comm                     proccomm);

void dsmeshExit (
    Dsmesh * const                smshptr);

void dsmeshFree (
    Dsmesh * const                smshptr);

int dsmeshBuild (
    Dsmesh * const                smshptr,
    const Gnum                   baseval,
    const Gnum                   vertlocnbr,
    const Gnum                   vertlocmax,
    Gnum * const                 vertloctax,
    Gnum * const                 vendloctax,
    const Gnum                   edgelocnbr,
    const Gnum                   edgelocsiz,
    Gnum * const                 edgeloctax,
    Gnum * const                 edloloctax,
    Gnum                         enttglbnbr,
    Gnum *                       ventloctax,
    Gnum *                       esubloctab,
	Gnum                         valuglbmax,
    const Gnum                   ovlpglbval,
	Dvalues *                    valslocptr,
	Gnum                         degrlocmax,
  	Gnum *                       procvrttab,
  	Gnum *                       proccnttab,
  	Gnum *                       procdsptab,
  	int                          procngbnbr,
  	int                          procngbmax,
  	int *                        procngbtab);

int dsmeshBuild2 (
    Dsmesh * restrict const      smshptr,   
    const Gnum                   baseval,   
    const Gnum                   vertlocnbr,
    const Gnum                   vertlocmax);


int dsmeshValueData (
    Dsmesh * restrict const       smshptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                        sizeval,
    Gnum *                        typesiz,
    void **                      valuloctab);
