
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* mesh iterator handling routines.     */
/*                                      */
/****************************************/

#ifdef PAMPA_WORD_EXTERN
extern
#endif /* PAMPA_WORD_EXTERN */
inline int
mdmeshItData (
const Mdmesh * const  meshptr,                    //!< Distributed mesh
Gnum const            vlvllocnum,                 //!< Level number in multigrid for vertex
Gnum const            ventlocnum,                 //!< Entity number of vertex
Gnum const            nlvllocnum,                 //!< Level number in multigrid for neighbor
Gnum const            nesblocnum,                 //!< Entity number of neighbors
Gnum *                nentlocnum,                 //!< XXX
Gnum *                ngstlocnbr,                 //!< XXX
const Gnum * *        vindloctab,                 //!< XXX
const Gnum * *        edgeloctab,                 //!< XXX
Gnum *                edgelocsiz,                 //!< XXX
const Gnum * *        svrtloctab,                 //!< XXX
const Gnum * *        mngbloctab,                 //!< XXX
const Gnum * *        nentlocbas,                 //!< XXX
Gnum *                nentlocmsk,                 //!< XXX
const Gnum **         nentloctab,                 //!< XXX
const Gnum * *        sngbloctab,                 //!< XXX
Gnum *                vnumlocptr,                 //!< XXX
Gnum *                vnndlocptr,                 //!< XXX
Gnum                  langflg)
{

  Dmesh * meshvlvptr;                             /* Pointer on mesh for vertex  */
  Dmesh * meshnlvptr;                             /* Pointer on mesh for neighbor */
  Gnum * nentloctax;
  DmeshEnttNghb * restrict * nghbloctax;
  Gnum   baseval;
  Gnum   basevl2;
  Gnum   cheklocval;
  Gnum   nesblocnm2;

  cheklocval = 0;
  baseval = meshptr->baseval;
  basevl2 = langflg;

#ifdef PAMPA_DEBUG_ITER2
  if ((ventlocnum < baseval) || (ventlocnum >= meshptr->dmshglbtax[baseval].enttglbnbr + baseval)) { // XXX faut-il prendre le premier niveau ? Ne devrait-on pas stocker dans le Mdmesh ? Ainsi que les niveaux déjà remplis ?
    errorPrint("invalid entity number of vertex");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if (abs (vlvllocnum - nlvllocnum) > 1) {
    errorPrint ("The difference between two levels must be less or equal than 1");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_ITER2 */

  meshvlvptr = meshptr->dmshglbtax + vlvllocnum;
  meshnlvptr = meshptr->dmshglbtax + nlvllocnum;

  if (meshvlvptr->enttloctax[ventlocnum] == NULL) {
    errorPrint ("There is no vertex in entity %d", ventlocnum);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  nghbloctax = (vlvllocnum < nlvllocnum) ? meshvlvptr->enttloctax[ventlocnum]->ngbuloctax : meshvlvptr->enttloctax[ventlocnum]->ngblloctax;

  if ((nghbloctax == NULL) || (nghbloctax[nesblocnum] == NULL)) {
    errorPrint ("There is no vertex in level " GNUMSTRING " with entity " GNUMSTRING " for vertex with entity " GNUMSTRING " in level " GNUMSTRING, nlvllocnum, nesblocnum, ventlocnum, vlvllocnum);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);


  *vnumlocptr =
    *vnndlocptr = -1;
#ifdef PAMPA_DEBUG_ITER
  *vnndlocptr = meshvlvptr->enttloctax[ventlocnum]->vertlocnbr - 1 + baseval;
#endif /* PAMPA_DEBUG_ITER */

  if ((nesblocnum < baseval) || (nesblocnum >= meshptr->dmshglbtax[baseval].enttglbnbr + baseval)) {
    errorPrint ("Invalid entity number of neighbor or flag number");
    return -1;
  }
  *vindloctab = (Gnum *) (nghbloctax[nesblocnum]->vindloctax + basevl2);
  *svrtloctab = NULL;
  if (meshvlvptr->esublocbax[ventlocnum & meshvlvptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
    *svrtloctab = meshvlvptr->enttloctax[ventlocnum]->svrtloctax + basevl2;
  }
  *edgeloctab = meshvlvptr->medgloctax;
  *edgelocsiz = meshvlvptr->medglocsiz; //! \warning edgelocsiz is used, maybe it has to be modified

  nesblocnm2 = nesblocnum;
//#ifdef PAMPA_DEBUG_ITER2
//    if (((vertlocnum > meshptr->enttloctax[ventlocnum]->vloclocnbr + baseval)
//          && (meshptr->ovlpglbval > 0))
//        || ((vertlocnum > meshptr->enttloctax[ventlocnum]->vertlocnbr + baseval)
//          && (meshptr->ovlpglbval == 0))) {
//      errorPrint("invalid vertex number");
//      return (1);
//    }
//#endif /* PAMPA_DEBUG_ITER2 */


  



  *mngbloctab = meshnlvptr->enttloctax[nesblocnm2]->mvrtloctax + basevl2;
  *nentlocnum = nesblocnm2;

  switch (meshvlvptr->esublocbax[nesblocnm2 & meshvlvptr->esublocmsk]) {
    case PAMPA_ENTT_SINGLE :
      *ngstlocnbr = 1;
      *nentlocmsk = 0;

      *nentlocbas = 
      *nentloctab = 
      *sngbloctab = NULL;
      break;

    case PAMPA_ENTT_STABLE :
      *ngstlocnbr = meshnlvptr->enttloctax[nesblocnm2]->vgstlocnbr;
      *nentlocbas = NULL;
      *nentloctab = meshnlvptr->enttloctax[nesblocnm2]->ventloctax + basevl2;
      *nentlocmsk = 0;

      *sngbloctab = meshnlvptr->enttloctax[nesblocnm2]->svrtloctax + basevl2;
      break;

    default : // \attention case "> PAMPA_ENTT_SINGLE"
      *nentlocnum = meshnlvptr->esublocbax[nesblocnm2];
      *ngstlocnbr = meshnlvptr->enttloctax[*nentlocnum]->vgstlocnbr;

      nentloctax = meshnlvptr->enttloctax[*nentlocnum]->ventloctax;
      *nentlocbas = nentloctax + basevl2;
      *nentloctab = nentloctax + basevl2;
      *nentlocmsk = ~((Gnum) 0);

      *sngbloctab = meshnlvptr->enttloctax[*nentlocnum]->svrtloctax + basevl2;

  }

  *edgeloctab += basevl2;



  return (0);
}


