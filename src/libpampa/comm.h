/* Copyright 2010-2016 ENSEIRB, Inria & CNRS
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        comm.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                communication functions. 
//!
//!   \date        Version 1.0: from: 21 Mar 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define COMM_H

/*
**  The type and structure definitions.
*/

#ifndef GNUMMAX                                   /** If dmesh.h not included     */
typedef INT                   Gnum;               /** Vertex and edge numbers     */
typedef UINT                  Gunum;              /** Unsigned type of same width */
#define GNUMMAX                     (INTVALMAX)   /** Maximum signed Gnum value   */
#define GNUMSTRING                  INTSTRING     /** String to printf a Gnum     */
#endif /* GNUMMAX */

/*
**  The function prototypes.
*/

#ifndef COMM
#define static
#endif

int                         commGnumAllgatherv      (void * const, const Gnum, MPI_Datatype, void * const, const Gnum * const, const Gnum * const, MPI_Datatype, MPI_Comm);
int                         commGnumGatherv         (void * const, const Gnum, MPI_Datatype, void * const, const Gnum * const, const Gnum * const, MPI_Datatype, const int, MPI_Comm);
int                         commGnumScatterv        (void * const, const Gnum * const, const Gnum * const, MPI_Datatype, void * const, const Gnum, MPI_Datatype, const int, MPI_Comm);

#define commAllreduce2 commAllreduce
#define commAllreduce  MPI_Allreduce
#define commBarrier    MPI_Barrier
#define commAllgather  MPI_Allgather
#define commGather     MPI_Gather
#define commWaitall    MPI_Waitall
#define commIsend      MPI_Isend
#define commIrecv      MPI_Irecv
#define commSend       MPI_Send
#define commSendrecv   MPI_Sendrecv
#define commRecv       MPI_Recv
#define commBcast      MPI_Bcast
#define commAlltoall   MPI_Alltoall
#define commAlltoallv  MPI_Alltoallv
#define commScatter    MPI_Scatter
#define commAllgatherv MPI_Allgatherv
#define commGatherv    MPI_Gatherv
#define commScatterv   MPI_Scatterv

#ifndef COMM
#undef static
#endif /* COMM      */

/*
**  The macro definitions.
*/

