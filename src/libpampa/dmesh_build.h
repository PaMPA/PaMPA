/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_build.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the distributed source mesh building
//!                routines. 
//!
//!   \date        Version 1.0: from: 13 Oct 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define DMESHBUILDGHSTHASHPRIME     17            //!< Prime number for hashing

typedef struct DmeshBuildGhstVert_                //!  Data to compute number for each ghost vertex
{
  Gnum          vertnum;                          //!< Vertex number (locally)
  Gnum          mvrtnum;                          //!< Vertex number (in Mesh)
} DmeshBuildGhstVert;

typedef struct DmeshBuildNogtVert_                //!  Data to remove ghost vertices, which are not in the overlap
{
  Gnum          mvrtnum;                          //!< Vertex number (in Mesh)
  Gnum          vertnum;                          //!< Vertex number (locally)
  Gnum          vnewnum;                          //!< Vertex number (new one)
} DmeshBuildNogtVert;

typedef struct DmeshBuildProcVert_                //!  Data to send vertices
{
  Gnum         procnum;                           //!< Processor number
  Gnum         mvrtnum;                           //!< Vertex number (in Mesh)
} DmeshBuildProcVert;



int dmeshBuild2 (
    Dmesh * restrict const  meshptr,   
    const Gnum              baseval,   
    const Gnum              vertlocnbr,
    const Gnum              vertlocmax);

static
int
dmeshBuildGhstVertResize (
    DmeshBuildGhstVert * restrict * hashtabptr,
    Gnum * restrict const           hashsizptr,
    Gnum * restrict const           hashmaxptr,
    Gnum * restrict const           hashmskptr,
    DmeshBuildProcVert * restrict * procvertlocptr,
    const Gnum                      hashnbr);

int dmeshBuild3 (
    Dmesh * restrict const          meshptr,
    const Gnum                      baseval,
    const Gnum                      vertlocnbr,
    const Gnum                      vertlocmax,
    Gnum * const                    vertloctax,
    Gnum * const                    vendloctax,
    const Gnum                      edgelocsiz,
    Gnum * restrict const           edgeloctax,
    Gnum *                          ventloctax,
    const Gnum                      ovlpglbval,
    Gnum *                          vgstlocnbr,
    DmeshBuildGhstVert * restrict * hashtab,
    DmeshBuildProcVert * restrict * procvertloctab,
    Gnum * restrict *               ventgsttax,
    Gnum * restrict *               vertgsttax,
    Gnum * restrict *               edgegsttax);

int dmeshBuild4 (
    Dmesh * restrict const  meshptr,    
    const Gnum              vertlocnbr, 
    Gnum *                  ventloctax, 
    Gnum * restrict *       vnbrloctax, 
    Gnum * restrict *       vnbrglbtax);

