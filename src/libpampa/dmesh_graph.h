/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_graph.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the centralized graph converting routines
//!                from a distributed mesh.
//!
//!   \date        Version 1.0: from: 21 Jul 2015
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

//! \todo Following macros are already defined in dmesh_dgraph.h
#define DMESH_ENTT_MAIN      1               //!< Flag for main entity
#define DMESH_ENTT_MAIN_PART 2               //!< Flag for some vertices on main entity
#define DMESH_ENTT_ANY       3               //!< Flag for any vertex
#define DMESH_ENTT_ANY_PART  4               //!< Flag for some vertices

int dmeshGraphInit (
    SCOTCH_Graph *      grafptr);

int dmeshGraphBuild (
    Dmesh * const       meshptr,
    Gnum                flagval,
    Gnum                dvrtlocnbr,
    Gnum * const        dvrtloctab,
    Gnum * const        veloloctax,
    Gnum * const        edloloctax,
    SCOTCH_Graph *      grafptr,
    Gnum *              vertlocbas);

int dmeshGraphExit (
    SCOTCH_Graph *      grafptr);

int dmeshGraphFree (
    SCOTCH_Graph *      grafptr);
