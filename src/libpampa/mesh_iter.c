/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_iter.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the mesh iterator purpose routines.
//!
//!   \date        Version 1.0: from: 31 May 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/


#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

int
meshItData (
const Mesh * const     meshptr,                                    //!< Centralized mesh
Gnum const             ventnum,                                    //!< Entity number of vertex
Gnum const             nesbnum,                                    //!< Entity number of neighbors
Gnum *                 nentnum,                                    //!< XXX
Gnum *                 nghbnbr,                                    //!< XXX
const Gnum * *         vindtab,                                    //!< XXX
const Gnum * *         edgetab,                                    //!< XXX
Gnum *                 edgenbr,                                    //!< XXX
const Gnum * *         svrttab,                                    //!< XXX
const Gnum * *         mngbtab,                                    //!< XXX
const Gnum * *         ventbas,                                    //!< XXX
Gnum *                 ventmsk,                                    //!< XXX
const Gnum **          venttab,                                    //!< XXX
const Gnum * *         sngbtab,                                    //!< XXX
Gnum *                 vnumptr,                                    //!< XXX
Gnum *                 vnndptr)                                    //!< XXX
{

  Gnum nesbnm2;
  Gnum chekval;
  Gnum * venttax;


#ifdef PAMPA_DEBUG_ITER2
  // XXX modifier le test par rapport à nesbnum
//  if (((ventnum < meshptr->baseval) || (ventnum >= meshptr->enttnbr + meshptr->baseval)) ||
//      ((nesbnum < meshptr->baseval) || (nesbnum >= meshptr->enttnbr + meshptr->baseval))) {
//    errorPrint("invalid entity number");
//    return (1);
//  }
#endif /* PAMPA_DEBUG_ITER2 */

  *svrttab = NULL;
  if (nesbnum == -1) { // TRICK: -1 means that any vertex // XXX mettre un define au lieu de -1
    if (meshptr->entttax[ventnum] == NULL) {
      *vnumptr = 0;
      *vnndptr = 0;
      *edgetab = NULL;
      *edgenbr = 0;
    }
    else {
      if (meshptr->entttax[ventnum]->vprmtax == NULL) {
        chekval = meshItBuild(meshptr, ventnum);
        if (chekval != 0)
          return chekval;
      }

      *vnumptr = meshptr->baseval;
      *vnndptr = *vnumptr + meshptr->entttax[ventnum]->vertnbr;
      *edgetab = meshptr->entttax[ventnum]->vprmtax;
      *edgenbr = meshptr->entttax[ventnum]->vertnbr;
    }
    nesbnm2 = ventnum;
  }
  else {
    nesbnm2 = nesbnum;
#ifdef PAMPA_DEBUG_ITER
    *vnndptr = meshptr->entttax[ventnum]->vertnbr - 1 + meshptr->baseval;
#endif /* PAMPA_DEBUG_ITER */
    *vindtab = (Gnum *)
      ((meshptr->entttax[ventnum] == NULL)
      ? NULL : (meshptr->entttax[ventnum]->nghbtax[nesbnum]->vindtax + meshptr->baseval));
    if (meshptr->esubbax[ventnum & meshptr->esubmsk] > PAMPA_ENTT_SINGLE) {
      *svrttab = meshptr->entttax[ventnum]->svrttax + meshptr->baseval;
    }
    *edgetab = meshptr->edgetax;
    *edgenbr = meshptr->edgenbr;
  }



  *mngbtab = (meshptr->entttax[nesbnm2] == NULL) ? NULL : meshptr->entttax[nesbnm2]->mvrttax + meshptr->baseval;
  *nentnum = nesbnm2;

  switch (meshptr->esubbax[nesbnm2 & meshptr->esubmsk]) {
    case PAMPA_ENTT_SINGLE :
      *nghbnbr = 1;
      *ventmsk = 0;

      *ventbas = 
      *venttab = 
      *sngbtab = NULL;
      break;

    case PAMPA_ENTT_STABLE :
      *nghbnbr = meshptr->entttax[nesbnm2]->vertnbr;
      *ventbas = NULL;
      *venttab = meshptr->entttax[nesbnm2]->venttax + meshptr->baseval;
      *ventmsk = 0;

      *sngbtab = meshptr->entttax[nesbnm2]->svrttax + meshptr->baseval;
      break;

    default : // \attention case "> PAMPA_ENTT_SINGLE"
      *nentnum = meshptr->esubbax[nesbnm2];
      *nghbnbr = meshptr->entttax[*nentnum]->vertnbr;

      venttax = meshptr->entttax[*nentnum]->venttax;
      *ventbas = venttax + meshptr->baseval;
      *venttab = venttax + meshptr->baseval;
      *ventmsk = ~((Gnum) 0);

      *sngbtab = meshptr->entttax[*nentnum]->svrttax + meshptr->baseval;

  }

  *edgetab += meshptr->baseval;



  return (0);
}



int
meshItBuild (
    const Mesh * const meshptr,									   //!< XXX
    Gnum const          ventnum)								   //!< XXX
{

  Gnum vertnum;
  Gnum vertnnd;
  MeshEntity * enttptr;

  enttptr = meshptr->entttax[ventnum];

 if ((enttptr->vprmtax = (Gnum *) memAlloc (sizeof (Gnum) * enttptr->vertnbr)) == NULL) {
   errorPrint  ("out of memory");
   return      (1);
 }
 enttptr->vprmtax -= meshptr->baseval; // XXX s'il ne s'agit pas d'une sous-entité, peut-être que nous pourrions avoir un seul tableau dans la structure mesh et qui correspondrait au tableau identité (dont la taille serait le max de sommets dans une entité)

 // parcours des sommets aux
 for (vertnum = meshptr->baseval, vertnnd = enttptr->vertnbr + meshptr->baseval;
    vertnum < vertnnd; vertnum ++) {
   Gnum vertnm2;

   if (meshptr->esubbax[ventnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE)
     vertnm2 = vertnum;
   else
     vertnm2 = enttptr->svrttax[vertnum];

   enttptr->vprmtax[vertnum] = vertnm2;
 }

 return (0);
}
 
