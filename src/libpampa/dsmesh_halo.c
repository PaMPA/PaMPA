/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dsmesh_halo.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the halo update
//!                routines for distributed simplified mesh.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define DSMESH_HALO

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dsmesh.h"
#include "dsmesh_halo.h"
#include <pthread.h>


//! These routines fill the send arrays used by
//! all of the halo routines.
//! They return:
//! - void  : in all cases.

#define DSMESHHALOFILLNAME          dsmeshHaloFillGeneric
#define DSMESHHALOFILLFLAG
#define DSMESHHALOFILLSIZE          attrglbsiz
#define DSMESHHALOFILLCOPY(d,s,n)   memCpy ((d), (s), (n))
#include "dsmesh_halo_fill.c"
#undef DSMESHHALOFILLNAME
#undef DSMESHHALOFILLFLAG
#undef DSMESHHALOFILLSIZE
#undef DSMESHHALOFILLCOPY

#define DSMESHHALOFILLNAME          dsmeshHaloFillGnum
#define DSMESHHALOFILLSIZE          sizeof (Gnum)
#define DSMESHHALOFILLCOPY(d,s,n)   *((Gnum *) (d)) = *((Gnum *) (s))
#include "dsmesh_halo_fill.c"
#undef DSMESHHALOFILLNAME
#undef DSMESHHALOFILLSIZE
#undef DSMESHHALOFILLCOPY

#define DSMESHHALOFILLNAME          dsmeshHaloFillInt //!< In case Gnum is not int
#define DSMESHHALOFILLSIZE          sizeof (int)
#define DSMESHHALOFILLCOPY(d,s,n)   *((int *) (d)) = *((int *) (s))
#include "dsmesh_halo_fill.c"
#undef DSMESHHALOFILLNAME
#undef DSMESHHALOFILLSIZE
#undef DSMESHHALOFILLCOPY

static
void
dsmeshHaloFill (
const Dsmesh * restrict const meshptr,
const DsmeshEnttComm * restrict const commlocptr,
const void * restrict const   attrgsttab,         //!< Attribute array to diffuse
int                           attrglbsiz,         //!< Type extent of attribute
byte * restrict const         attrsndtab,         //!< Array for packing data to send
int * const                   senddsptab,         //!< Temporary displacement array
const int * restrict const    sendcnttab)         //!< Count array
{
  int                 procnum;
  byte **             attrdsptab;

  attrdsptab = (byte **) senddsptab;              /* TRICK: use senddsptab (int *) as attrdsptab (byte **) */
  attrdsptab[0] = attrsndtab;                     /* Pre-set send arrays for send buffer filling routines */
  for (procnum = 1; procnum < meshptr->procglbnbr; procnum ++)
    attrdsptab[procnum] = attrdsptab[procnum - 1] + sendcnttab[procnum - 1] * attrglbsiz;

  if (attrglbsiz == sizeof (Gnum))
    dsmeshHaloFillGnum (commlocptr, attrgsttab, attrdsptab);
  else if (attrglbsiz == sizeof (int))            /* In case Gnum is not int */
    dsmeshHaloFillInt (commlocptr, attrgsttab, attrdsptab);
  else                                            /* Generic but slower fallback routine */
    dsmeshHaloFillGeneric (commlocptr, attrgsttab, attrglbsiz, attrdsptab);

  senddsptab[0] = 0;                              /* Pre-set send arrays for data sending routines */
  for (procnum = 1; procnum < meshptr->procglbnbr; procnum ++)
    senddsptab[procnum] = senddsptab[procnum - 1] + sendcnttab[procnum - 1];
}

//! This function checks that the data of proc{snd,rcv}tab
//! are consistent.

#ifdef PAMPA_DEBUG_DSMESH2
int
dsmeshHaloCheck (
const Dsmesh * restrict const meshptr, 			 //!< XXX
const DsmeshEnttComm * restrict const commlocptr)	 //!< XXX
{
  int *               proctab;                    /* Array to collect data */
  int                 procnum;
  int                 o;

  if ((proctab = memAlloc (meshptr->procglbnbr * sizeof (int))) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }

  if (commAlltoall (commlocptr->procsndtab, 1, MPI_INT, proctab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    memFree    (proctab);                         /* Free group leader */
    return     (1);
  }

  o = 0;
  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    if (proctab[procnum] != commlocptr->procrcvtab[procnum]) {
      errorPrint ("data error");
      o = 1;
      break;
    }
  }

  memFree (proctab);                              /* Free group leader */

  return  (o);
}
#endif /* PAMPA_DEBUG_DSMESH2 */

//! These functions perform a synchronous collective
//! halo diffusion operation on the ghost array given
//! on input.
//! It returns:
//! - 0   : if the halo has been successfully propagated.
//! - !0  : on error.

static
int
dsmeshHaloSync2 (
Dsmesh * restrict const       meshptr,			  //!< XXX
const DsmeshEnttComm * restrict const commlocptr,	 //!< XXX
const Gnum                    vertlocnbr,
void * restrict const         attrgsttab,         //!< Attribute array to share
const MPI_Datatype            attrglbtype,        //!< Attribute datatype
byte ** const                 attrsndptr,         //!< Pointer to array for packing data to send
int ** const                  senddspptr,         //!< Pointers to communication displacement arrays
int ** const                  recvdspptr,		  //!< XXX
MPI_Request ** const          requptr)            /* Pointer to local request array for point-to-point */
{
  MPI_Aint            attrglbsiz;                 /* Extent of attribute datatype */
  MPI_Aint            dummy;
  int                 procngbsiz;                 /* Size of request array for point-to-point communications */
  int                 procngbnum;

  // XXX mettre un autre test à la place…
  //if (dsmeshGhst (meshptr) != 0) {                /* Compute ghost edge array if not already present */
  //  errorPrint ("cannot compute ghost edge array");
  //  return     (1);
  //}

  procngbsiz = ((meshptr->flagval & DSMESHCOMMPTOP) != 0) ? meshptr->procngbnbr : 0;

  MPI_Type_get_extent (attrglbtype, &dummy, &attrglbsiz);     /* Get type extent */
  if (memAllocGroup ((void **) (void *)
                     attrsndptr, (size_t) (commlocptr->procsndnbr * attrglbsiz),
                     senddspptr, (size_t) (meshptr->procglbnbr * MAX (sizeof (int), sizeof (byte *))), /* TRICK: use senddsptab (int *) as attrdsptab (byte **) */
                     recvdspptr, (size_t) (meshptr->procglbnbr * sizeof (int)),
                     requptr,    (size_t) (procngbsiz * 2      * sizeof (MPI_Request)), (void *) NULL) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }

  dsmeshHaloFill (meshptr, commlocptr, attrgsttab, attrglbsiz, *attrsndptr, *senddspptr, commlocptr->procsndtab); /* Fill data arrays */

  (*recvdspptr)[0] = (int) vertlocnbr;
  for (procngbnum = 1; procngbnum < meshptr->procglbnbr; procngbnum ++)
    (*recvdspptr)[procngbnum] = (*recvdspptr)[procngbnum - 1] + commlocptr->procrcvtab[procngbnum - 1];

#ifdef PAMPA_DEBUG_DSMESH2
  if (dsmeshHaloCheck (meshptr, commlocptr) != 0) {
    errorPrint ("invalid communication data");
    return     (1);
  }
#endif /* PAMPA_DEBUG_DSMESH2 */

  return (0);
}

int
dsmeshHaloSyncComm ( // XXX doit-on garder celle-la en plus de dsmeshHaloSync ???
Dsmesh * restrict const       meshptr,			  //!< XXX
const DsmeshEnttComm * restrict const commlocptr,	 //!< XXX
const Gnum                    vertlocnbr,
void * restrict const         attrgsttab,         //!< Attribute array to share
const MPI_Datatype            attrglbtype)        //!< Attribute datatype
{
  byte *              attrsndtab;                 /* Array for packing data to send */
  int *               senddsptab;
  int *               recvdsptab;
  MPI_Request *       requtab;
  int                 o;

  if (dsmeshHaloSync2 (meshptr, commlocptr, vertlocnbr, attrgsttab, attrglbtype, &attrsndtab, &senddsptab, &recvdsptab, &requtab) != 0) /* Prepare communication arrays */
    return (1);

  o = 0;                                          /* Assume success */
  if ((meshptr->flagval & DSMESHCOMMPTOP) != 0) { /* If point-to-point exchange   */
    MPI_Aint              attrglbsiz;             /* Extent of attribute datatype */
    MPI_Aint              dummy;
    const int * restrict  procrcvtab;
    const int * restrict  procsndtab;
    const int * restrict  procngbtab;
    int                   procngbnbr;
    int                   procngbnum;
    MPI_Comm              proccomm;
    int                   requnbr;

    proccomm   = meshptr->proccomm;
    procngbtab = meshptr->procngbtab;
    procngbnbr = meshptr->procngbnbr;
    procrcvtab = commlocptr->procrcvtab;
    MPI_Type_get_extent (attrglbtype,&dummy, &attrglbsiz);   /* Get type extent */
    for (procngbnum = procngbnbr - 1, requnbr = 0; procngbnum >= 0; procngbnum --, requnbr ++) { /* Post receives first */
      int                 procglbnum;

      procglbnum = procngbtab[procngbnum];
      if (commIrecv ((byte *) attrgsttab + recvdsptab[procglbnum] * attrglbsiz, procrcvtab[procglbnum],
                     attrglbtype, procglbnum, STAGHALO, proccomm, requtab + requnbr) != MPI_SUCCESS) {
        errorPrint ("communication error");
        o = 1;
        break;
      }
    }

    procsndtab = commlocptr->procsndtab;
    for (procngbnum = 0; procngbnum < procngbnbr; procngbnum ++, requnbr ++) { /* Post sends afterwards */
      int                 procglbnum;

      procglbnum = procngbtab[procngbnum];
      if (commIsend (attrsndtab + senddsptab[procglbnum] * attrglbsiz, procsndtab[procglbnum],
                     attrglbtype, procglbnum, STAGHALO, proccomm, requtab + requnbr) != MPI_SUCCESS) {
        errorPrint ("communication error");
        o = 1;
        break;
      }
    }
    if (commWaitall (requnbr, requtab, MPI_STATUSES_IGNORE) != MPI_SUCCESS) {
      errorPrint ("communication error");
      o = 1;
    }
  }
  else {                                          /* Collective communication */
    if (commAlltoallv (attrsndtab, commlocptr->procsndtab, senddsptab, attrglbtype, /* Perform diffusion */
                            attrgsttab, commlocptr->procrcvtab, recvdsptab, attrglbtype,
                            meshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            o = 1;
    }
  }

  memFreeGroup (attrsndtab);                           /* Free group leader */

  return (o);
}

