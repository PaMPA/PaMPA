/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_graph.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contains the routines to
//!                convert a distributed mesh into a
//!                centralized graph 
//!
//!   \date        Version 1.0: from: 21 Jul 2015
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 **  The defines and includes.
 */

#define DMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"
#include <ptscotch.h>
#include "dmesh_graph.h"

//! \brief Initialize a Graph
//! \todo  The graph type could be opaque, given that initialization and
//! finalization are hidden
//!
//! \returns  0 : on success.
//! \returns !0 : on error.
  int
dmeshGraphInit (
    SCOTCH_Graph *      grafptr)                  //!< [inout] Graph pointer
{
  return SCOTCH_graphInit(grafptr);
}


//! \brief This routine computes the centralized graph corresponding to the distributed
//! mesh
//!
//! \returns 0  : if the computation succeeded.
//! \returns !0 : on error.
  int
dmeshGraphBuild (
    Dmesh * const       meshptr,                  //!< [in] Mesh pointer
    Gnum                flagval,                  //!< [in] Flag value
    Gnum const          dvrtlocnbr,               //!< [in] Number of vertices in dvrtloctax
    Gnum * const        dvrtloctax,               //!< [in] Used vertices in the destination graph
    Gnum * const        veloloctax,               //!< [in] Vertex load array
    Gnum * const        edloloctax,               //!< [in] Edge load array (not currently used)
    SCOTCH_Graph *      grafptr,                  //!< [inout] Graph pointer
    Gnum *              vertlocbas)               //!< [out] Base of vertex
{

  Gnum baseval;
  Gnum cheklocval;
  Gnum chekglbval;
  Gnum edgelocnbr;
  Gnum vertlocnum;
  Gnum vertlocnm2;
  Gnum vertlocnnd;
  Gnum ventlocnum;
  Gnum nentlocnum;
  Gnum enttlocnnd;
  Gnum vertlocnbr;
  Gnum vertlocmax;
  Gnum bvrtlocmax;
  Gnum vlbllocnbr;
  Gnum velolocnbr;
  Gnum evrtlocsiz;
  Gnum * vertloctax;
  Gnum * vendloctax;
  Gnum * vlblloctax;
  Gnum * veloloctx2;
  Gnum * edgeloctax;
  Gnum * evrtloctax;
  DmeshEntity ** enttloctax;

  baseval = meshptr->baseval;
  cheklocval = 0;
  enttloctax = meshptr->enttloctax;

  if (dvrtloctax != NULL)
    intSort1asc1 (dvrtloctax + baseval, dvrtlocnbr * sizeof (Gnum)); /* must be sorted */

  if (edloloctax != NULL) {
    errorPrint ("edloloctax is not yet taken into account");
    return (1);
  }

  switch (flagval) {
    case DMESH_ENTT_MAIN_PART :
      enttlocnnd = baseval + 1;
      vertlocnbr = dvrtlocnbr;
      vertlocmax = dvrtlocnbr;

      evrtlocsiz = enttloctax[baseval]->vertlocnbr;
      break;
    case DMESH_ENTT_MAIN :
      enttlocnnd = baseval + 1;
      vertlocnbr = enttloctax[baseval]->vertlocnbr;
      vertlocmax = vertlocnbr;

      evrtlocsiz = enttloctax[baseval]->vertlocnbr;
      break;
    case DMESH_ENTT_ANY_PART :
      enttlocnnd = meshptr->enttglbnbr + baseval;
      vertlocnbr = dvrtlocnbr;
      vertlocmax = dvrtlocnbr;

      evrtlocsiz = meshptr->vertlocnbr;
      break;
    case DMESH_ENTT_ANY :
      enttlocnnd = meshptr->enttglbnbr + baseval;
      vertlocnbr = meshptr->vtrulocnbr;
      vertlocmax = meshptr->vertlocmax;

      evrtlocsiz = 0;
      break;
    default :
      errorPrint("unknown flag");
      return (1);
  }

  bvrtlocmax = vertlocmax - 1 + baseval;

  //! \todo could be simplified if no overlap and all entities
  for (edgelocnbr = 0, ventlocnum = baseval ; ventlocnum < enttlocnnd ; ventlocnum ++) {
    if ((enttloctax[ventlocnum] == NULL) || (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      continue;

    for (nentlocnum = baseval ; nentlocnum < enttlocnnd ; nentlocnum ++) {
      if ((enttloctax[ventlocnum]->nghbloctax[nentlocnum] == NULL) || (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
        continue;

      for (vertlocnum = baseval, vertlocnnd = enttloctax[ventlocnum]->vertlocnbr + baseval;
          vertlocnum < vertlocnnd; vertlocnum ++)  {
        edgelocnbr += enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx
          - enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx;
      }
    }
  }


  if (memAllocGroup ((void **) (void *)
        &evrtloctax, (size_t) (evrtlocsiz * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_MAX_REDUCE(cheklocval, chekglbval, meshptr->proccomm, "dmeshGraphBuild", __LINE__);

  evrtloctax -= baseval;
  memSet (evrtloctax + baseval, ~0, evrtlocsiz * sizeof (Gnum));

  //if (vertlocnbr != 0) {

  velolocnbr = (veloloctax == NULL) ?  vertlocnbr : 0;
  vlbllocnbr = (flagval == DMESH_ENTT_ANY) ? meshptr->vertlocnbr : 0;

  if (memAllocGroup ((void **) (void *)
        &vertloctax, (size_t) (MAX(vertlocnbr, 2) * sizeof (Gnum)),// TRICK: MAX(,2) for Scotch if vertlocnbr = 0
        &vendloctax, (size_t) (MAX(vertlocnbr, 2) * sizeof (Gnum)),
        &vlblloctax, (size_t) (vlbllocnbr       * sizeof (Gnum)),
        &veloloctx2, (size_t) (velolocnbr * sizeof (Gnum)),
        &edgeloctax, (size_t) (MAX(edgelocnbr, 2) * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_MAX_REDUCE(cheklocval, chekglbval, meshptr->proccomm, "dmeshGraphBuild", __LINE__);
  vertloctax -= baseval;
  vendloctax -= baseval;
  veloloctx2 = (veloloctax == NULL) ? veloloctx2 - baseval : veloloctax;
  vlblloctax = (vlbllocnbr == 0)
    ? ((flagval == DMESH_ENTT_ANY_PART) ? dvrtloctax : NULL) // XXX il faudrait avoir vlblloctax dans tous les cas
    : vlblloctax - baseval;
  edgeloctax -= baseval;

  memset (vertloctax + baseval, 0, MAX(vertlocnbr, 2) * sizeof (Gnum));
  memset (vendloctax + baseval, 0, MAX(vertlocnbr, 2) * sizeof (Gnum));
  if (veloloctax == NULL) 
    memset (veloloctx2 + baseval, 0, vertlocnbr * sizeof (Gnum));

  switch (flagval) {
    case DMESH_ENTT_ANY_PART :

        for (vertlocnum = baseval, vertlocnum = dvrtlocnbr + baseval; vertlocnum < dvrtlocnbr ; vertlocnum ++) 
          evrtloctax[dvrtloctax[vertlocnum]] = vertlocnum;


      if (vertlocbas != NULL)
        *vertlocbas = baseval;

      break;

    case DMESH_ENTT_MAIN_PART :
    case DMESH_ENTT_MAIN :

      for (vertlocnum = baseval, vertlocnnd = enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd ; vertlocnum ++)
        if (flagval == DMESH_ENTT_MAIN)
          evrtloctax[vertlocnum] = vertlocnum;
        else if (dvrtloctax[vertlocnum] != ~0)
          evrtloctax[vertlocnum] = dvrtloctax[vertlocnum];
        else
          evrtloctax[vertlocnum] = ~0;

	  if (vertlocbas != NULL)
		*vertlocbas = baseval;

      break;
    case DMESH_ENTT_ANY :

	  if (vertlocbas != NULL)
		*vertlocbas = 0;

      break;
    default :
      errorPrint("unknown flag");
      return (1);
  }

  if (flagval == DMESH_ENTT_ANY) {
    for (vertlocnum = baseval, vertlocnnd = baseval + meshptr->vertlocnbr ; vertlocnum < vertlocnnd ; vertlocnum ++)
      vlblloctax[vertlocnum] = meshptr->vertlocmax + 1;

    for (ventlocnum = baseval ; ventlocnum < enttlocnnd ; ventlocnum ++) {
      DmeshEntity * ventlocptr;

      ventlocptr = enttloctax[ventlocnum];

      if ((enttloctax[ventlocnum] == NULL) || (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
        continue;

      for (vertlocnum = baseval, vertlocnnd = ventlocptr->vertlocnbr + baseval;
          vertlocnum < vertlocnnd; vertlocnum ++) {
        vertlocnm2 = ventlocptr->mvrtloctax[vertlocnum];
        vlblloctax[vertlocnm2] = vertlocnm2;
      }
    }
    intSort1asc1 (vlblloctax + baseval, meshptr->vertlocnbr);
  }



  for (vertlocnm2 = baseval - 1, edgelocnbr = ventlocnum = baseval ; ventlocnum < enttlocnnd ; ventlocnum ++) {
    DmeshEntity * ventlocptr;

    ventlocptr = enttloctax[ventlocnum];

    if ((enttloctax[ventlocnum] == NULL) || (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      continue;

    for (vertlocnum = baseval, vertlocnnd = ventlocptr->vertlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++)  {
      Gnum edgelocnum;
      Gnum edgelocnnd;

      if (flagval == DMESH_ENTT_ANY) {
        Gnum vertlocnm3;
        Gnum vertlocmx2;

        vertlocnm2 = ventlocptr->mvrtloctax[vertlocnum];
        if (vlblloctax[vertlocnm2] != vertlocnm2) {
          for (vertlocnm3 = baseval, vertlocmx2 = meshptr->vertlocnbr + baseval;
              vertlocmx2 - vertlocnm3 > 1; ) {
            Gnum                 vertlocmed;

            vertlocmed = (vertlocmx2 + vertlocnm3) / 2;
            if (vlblloctax[vertlocmed] <= vertlocnm2)
              vertlocnm3 = vertlocmed;
            else
              vertlocmx2 = vertlocmed;
          }
#ifdef PAMPA_DMESH2
          if (vlblloctax[vertlocnm3] != vertlocnm2) {
            errorPrint ("%d not found", vertlocnm2);
            return (1);
          }
#endif /* PAMPA_DMESH2 */
          vertlocnm2 = vertlocnm3;
        }
      }
      else if (flagval == DMESH_ENTT_ANY_PART) {
        vertlocnm2 = ventlocptr->mvrtloctax[vertlocnum];
        if ((vertlocnm2 = evrtloctax[vertlocnm2]) == ~0)
          continue;
      }
      else if (flagval == DMESH_ENTT_MAIN)
        vertlocnm2 ++;
      else if (evrtloctax[vertlocnum] != ~0) //!< \todo This test could be improved?
        vertlocnm2 ++;
      else
        continue;

      vertloctax[vertlocnm2] = edgelocnbr;
      if (veloloctax == NULL)
        veloloctx2[vertlocnm2] = 1;

      for (nentlocnum = baseval ; nentlocnum < enttlocnnd ; nentlocnum ++) {
        DmeshEntity * nentlocptr;

        nentlocptr = enttloctax[nentlocnum];

        if ((ventlocptr->nghbloctax[nentlocnum] == NULL) || (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
          continue;

        if ((flagval == DMESH_ENTT_ANY) && (nentlocptr != NULL))
          evrtloctax = nentlocptr->mvrtloctax;



        for (edgelocnum = ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx,
            edgelocnnd = ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx;
            edgelocnum < edgelocnnd ; edgelocnum ++)
          if (meshptr->edgeloctax[edgelocnum] <= bvrtlocmax) {
            if ((flagval == DMESH_ENTT_ANY_PART) && (evrtloctax[nentlocptr->mvrtloctax[meshptr->edgeloctax[edgelocnum]]]))
              edgeloctax[edgelocnbr ++] = nentlocptr->mvrtloctax[meshptr->edgeloctax[edgelocnum]];
            else if ((flagval != DMESH_ENTT_MAIN_PART) || ((flagval == DMESH_ENTT_MAIN_PART) && (evrtloctax[meshptr->edgeloctax[edgelocnum]] != ~0)))
              edgeloctax[edgelocnbr ++] = evrtloctax[meshptr->edgeloctax[edgelocnum]];
          }

      }
      vendloctax[vertlocnm2] = edgelocnbr;
    }
  }
  edgelocnbr -= baseval;



  if (vertloctax != NULL)
    vertloctax += baseval;
  if (vlblloctax != NULL)
    vlblloctax += baseval;

  cheklocval = SCOTCH_graphBuild(grafptr,
      meshptr->baseval,
      vertlocnbr,
      vertloctax, // TRICK: vertloctax already unbased due to allocation group
      vendloctax + baseval,
      veloloctx2 + baseval,
      vlblloctax,
      edgelocnbr,
      edgeloctax + baseval,
      NULL);
  if (cheklocval != 0)
    return cheklocval;

//#define PAMPA_DEBUG_GRAPH
#ifdef PAMPA_DEBUG_GRAPH
  {
    static int i = 0;
    char s[30];
    sprintf(s, "dmesh-graph-%d-" GNUMSTRING ".grf", i ++, meshptr->proclocnum);
    FILE *graph_file;
    graph_file = fopen(s, "w");
    SCOTCH_graphSave (grafptr, graph_file);
    fclose(graph_file);
  }
#endif /* PAMPA_DEBUG_GRAPH */

#ifdef PAMPA_DEBUG_DMESH_GRAPH
  CHECK_FERR (SCOTCH_graphCheck(grafptr), meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH_GRAPH */

  memFreeGroup (evrtloctax);
  return (cheklocval);
}

//! \brief This function frees the graph pointer
//!
//! \returns  0 : on success
//! \returns !0 : on error
  int
dmeshGraphFree (
    SCOTCH_Graph *      grafptr)                  //!< [inout] Graph pointer
{

  Gnum * vertloctab;
  SCOTCH_graphData(grafptr, NULL, NULL, &vertloctab, NULL, NULL, NULL, NULL, NULL, NULL); 

  if (vertloctab != NULL)
    memFreeGroup (vertloctab);

  SCOTCH_graphFree (grafptr);

  return (0);
}

//! \brief This function exits the graph pointer
//!
//! \returns  0 : on success
//! \returns !0 : on error
  int
dmeshGraphExit (
    SCOTCH_Graph *      grafptr)                  //!< [inout] Graph pointer
{

  Gnum * vertloctab;
  SCOTCH_graphData(grafptr, NULL, NULL, &vertloctab, NULL, NULL, NULL, NULL, NULL, NULL); 

  if (vertloctab != NULL)
    memFreeGroup (vertloctab);

  SCOTCH_graphExit(grafptr);

  return (0);
}
