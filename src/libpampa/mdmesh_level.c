
/*
 ** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
 */

#define MDMESH

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dsmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle multi level */
/* distributed mesh meshs.           */
/*                                   */
/*************************************/

//! This routine initializes the first level of 
//! a multi distributed mesh
//! structure.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
mdmeshLevelInit (
    Mdmesh * restrict const     mdmhptr,              //!< Multi level distributed mesh structure
    Dmesh * restrict const      meshptr)              //!< Distributed mesh structure
{
  Dsmesh dsmshdat; // XXX en attendant d'avoir implanté dmeshCopy
  int    cheklocval = 0;

  // XXX est-ce que le test suivant est intéressant, puisqu'il ne sert pas si
  // baseval = 0
#ifdef PAMPA_DEBUG_MDMESH
  if (mdmhptr->dmshglbnxt != mdmhptr->baseval) {
    errorPrint ("Mdmesh has not been initialized");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */
  mdmhptr->flagval |= MDMESHFREEDMESH;

  // XXX it is not really good, maybe it will be better if dmshglbtax is a Dmesh**
  mdmhptr->dmshglbtax[mdmhptr->baseval] = *meshptr;
  //dsmeshInit (&dsmshdat, meshptr->proccomm);
  //dmesh2dsmesh (meshptr, &dsmshdat);
  //dsmesh2dmesh (&dsmshdat, mdmhptr->dmshglbtax + mdmhptr->baseval);
  //dsmeshExit (&dsmshdat);

  mdmhptr->dmshglbtax[mdmhptr->baseval].flagval |= DMESHFREELVL;
  mdmhptr->dmshglbnxt = mdmhptr->baseval + 1;
  
  return (0);
}
