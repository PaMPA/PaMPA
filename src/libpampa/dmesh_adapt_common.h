/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt_common.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 Oct 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

int dmeshAdapt2 (
	Dmesh * restrict const meshptr,
	SCOTCH_Dgraph * fgrfptr,
	Gnum *          fvrtloctax,
	SCOTCH_Dgraph * cgrfptr,
	Gnum *          cprcvrttab,
	const Gnum      coarval,
	const Gnum      coarnbr,
	const double    coarrat);


int dmeshAdaptVertResize (
  	Dmesh * restrict const    meshptr,              /**< mesh */
	Gnum * restrict *               hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr);

int dmeshAdaptGatherSave3 (
    Dmesh const * const meshptr,
    Gnum                vertlocnbr,
    Gnum  const * const dataloctax,
    Gnum        * const dataglbtax);

int dmeshAdaptGatherSave2 (
    Dmesh const * const meshptr,
    Gnum  const * const dataloctax,
    int (*meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void        * const dataptr,
    char        * const fileptr);

int dmeshAdaptGatherSave(
    Dmesh const * const meshptr,
    Gnum        * const dataglbtax,
    int (*meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void        * const dataptr,
    char        * const fileptr);
