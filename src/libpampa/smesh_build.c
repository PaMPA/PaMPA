/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        smesh_build.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the centralized
//!                simplified source mesh building routines. 
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define SMESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "smesh.h"
#include "pampa.h"

//! This routine builds a mesh from
//! the local arrays that are passed to it. If
//! a vertex label array is given, it is assumed
//! that edge ends are given with respect to these
//! labels, and thus they are updated so as to be
//! given with respect to the implicit (based)
//! global numbering.
//! As for all routines that build meshs, the private
//! fields of the mesh structure have to be initialized
//! if they are not already.
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
smeshBuild (
    Smesh * restrict const       smshptr,           //!< mesh
    const Gnum                  idnum,             //!< Id
    const Gnum                  baseval,           //!< Base for indexing
    const Gnum                  vertnbr,           //!< Number of local vertices
    Gnum * const                verttax,           //!< Local vertex begin array
    Gnum * const                vendtax,           //!< Local vertex end array
    const Gnum                  edgenbr,           //!< Number of local edges
    const Gnum                  edgesiz,           //!< Size of local edges
    Gnum * restrict const       edgetax,           //!< Local edge array
    Gnum * restrict const       edlotax,           //!< Local edge load array (if any)
    Gnum                        enttnbr,		   //!< XXX
    Gnum *                      venttax,		   //!< XXX
    Gnum *                      esubtax,		   //!< XXX
    Gnum                        valumax,
    Values *                    valsptr,
    Gnum                        degrmax,		   //!< XXX
    Gnum                        procglbnbr)

{
  Gnum                  vertnum;
  Gnum                  vertnnd;

  smshptr->idnum = idnum;    
  smshptr->baseval = baseval;    /* set global and local data */

  //if (esubtax != NULL) {
  //  if ((esubbax = (Gnum *) memAlloc (enttnbr * sizeof (Gnum))) == NULL) {
  //    errorPrint  ("out of memory");
  //    return      (1);
  //  }
  //  esubbax -= baseval;

  //  memCpy (esubbax + baseval, esubtax + baseval, enttnbr * sizeof (Gnum));
  //  smshptr->esubmsk = ~((Gnum) 0);
  //}
  //else {
  //  esubbax = &enttsingle;
  //  smshptr->esubmsk = 0;
  //}
  //smshptr->esubbax = (Gnum *) esubbax;

  smshptr->baseval = baseval;
  smshptr->enttnbr = enttnbr;
  smshptr->vertnbr = vertnbr;
  smshptr->venttax = venttax;
  smshptr->esubtax = esubtax;
  smshptr->verttax = verttax;
  smshptr->vendtax = vendtax;
  smshptr->edgenbr = edgenbr;
  smshptr->edgesiz = edgesiz;
  smshptr->edgetax = edgetax;
  smshptr->edlotax = edlotax;
  smshptr->procglbnbr = procglbnbr;

  if (degrmax == -1) {
  	degrmax = 0;
  	for (vertnum = baseval, vertnnd = vertnbr + baseval;
      	vertnum < vertnnd; vertnum ++)  {

      if (vendtax[vertnum] - verttax[vertnum] > degrmax)
      	degrmax = vendtax[vertnum] - verttax[vertnum];
  	}
  }

  smshptr->degrmax = degrmax;

  if (valsptr == NULL)
  	return valuesInit(&smshptr->valsptr, baseval, valumax, enttnbr);
  else {
	valsptr->cuntnbr ++;
	smshptr->valsptr = valsptr;
	return (0);
  }
}

