/*  Copyright 2014-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt_geompart.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 18 Jul 2014
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_ADAPT
#ifdef PAMPA_TIME_CHECK2
#define PAMPA_TIME_CHECK
#endif /* PAMPA_TIME_CHECK2 */
//#define PAMPA_DEBUG_GRAPH
//#define PAMPA_DEBUG_DMESH_SAVE

//#define PAMPA_TIME_CHECK
#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "pampa.h"
#include "pampa.h"
#include <math.h>

#include <ptscotch.h>
#include "dmesh_adapt.h"
#include "dmesh_adapt_common.h"
#include "dmesh_dgraph.h"
#include "dmesh_gather_induce_multiple.h"
#include "dmesh_rebuild.h"

typedef struct VertLoad_ {
  Gnum vertnum;
  Gnum loadval;
  Gnum edgenum;
} VertLoad;

/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshAdaptGeompart (
    Dmesh * const    srcmeshptr,              /**< mesh */
    PAMPA_AdaptInfo * const infoptr,
	Dmesh * const    dstmeshptr)
{
  Dmesh indmeshdat;
  Dmesh * orgmeshptr;
  SCOTCH_Dgraph dgrfdat;
  SCOTCH_Dgraph bgrfdat; /* Band graph */
  SCOTCH_Dgraph cgrfdat; /* Connex compound graph */
  SCOTCH_Dgraph dbgfdat; /* Distributed bigraph */
  SCOTCH_Graph  cbgfdat; /* Centralized bigraph */
  SCOTCH_Strat  strtdat; 
  Gnum * restrict rmshloctax;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  Gnum rmshlocnb2;
  Gnum rmshglbnb2;
  Gnum enodlocnbr;
  Gnum enodglbnbr;
  //Gnum rmshglbnb2;
  //Gnum rmshglbnb3;
  Gnum bvellocsum;
  Gnum bvelglbsum;
  Gnum baseval;
  Gnum vertlocmax;
  Gnum coarval;
  Gnum coarnbr;
  double coarrat;
  Gnum ballsi2;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum dvrtlocnbr;
  Gnum emraval;
  Gnum switval;
  Gnum cvrtglbnbr;
  Gnum ibndval;
  int cheklocval;
  Gnum * restrict partloctax;
  Gnum * restrict paroloctax;
  Gnum * restrict vmloloctab;
  Gnum * restrict cprcvrttab;
  int * restrict dsndcnttab;
  int * restrict dsnddsptab;
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  Gnum * restrict srcprocvrttab;
  MPI_Comm proccomm;
#ifdef PAMPA_DEBUG_DMESH_SAVE
  static Gnum wmshcnt = 0;
#endif /* PAMPA_DEBUG_DMESH_SAVE */

  //rmshglbnb2 = 
  //  rmshglbnb3 = -1;
#ifdef PAMPA_INFO_ADAPT
  infoPrint ("*******************\n\nPart\n\n*******************");
#endif /* PAMPA_INFO_ADAPT */
  orgmeshptr = srcmeshptr;
  cheklocval = 0;
  coarnbr = 1;
  coarrat = 1;
  switval = 1;
  for (coarval = 1, ballsi2 = 1; ballsi2 < infoptr->ballsiz; coarval ++, ballsi2 <<= 1);

  baseval = orgmeshptr->baseval;
  proccomm = orgmeshptr->proccomm;

  CHECK_FERR (dmeshInit (&indmeshdat, proccomm), proccomm);
  CHECK_FERR (SCOTCH_stratInit (&strtdat), proccomm);
  CHECK_FERR (dmeshDgraphInit (orgmeshptr, &dgrfdat), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&bgrfdat, orgmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&cgrfdat, orgmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&dbgfdat, proccomm), proccomm);
  if (orgmeshptr->proclocnum == 0)
	cheklocval = SCOTCH_graphInit (&cbgfdat);
  CHECK_VERR (cheklocval, proccomm);

  infoptr->iitenum = 1;

  if (memAllocGroup ((void **) (void *)
          &cprcvrttab, (size_t) ((orgmeshptr->procglbnbr + 1) * sizeof (Gnum)),
          &dsndcnttab, (size_t) (orgmeshptr->procglbnbr       * sizeof (int)),
          &dsnddsptab, (size_t) ((orgmeshptr->procglbnbr + 1) * sizeof (int)),
          &drcvcnttab, (size_t) (orgmeshptr->procglbnbr       * sizeof (int)),
          &drcvdsptab, (size_t) ((orgmeshptr->procglbnbr + 1) * sizeof (int)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  cvrtglbnbr = 0;
  ibndval = infoptr->ibndval;
  while (1) {
	Gnum dvrtlocbas;
	Gnum bvrtlocnbr;
	Gnum bvrtglbnbr;
	Gnum bvrtlocnum;
	Gnum bvrtlocnnd;
	Gnum bvrtgstnbr;
	Gnum bvrtgstmax;
	Gnum cvrtgstnbr;
	Gnum cvrtlocmin;
	Gnum cvrtlocmax;
	Gnum cvrtlocnbr;
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;
	Gnum datasndnbr;
	Gnum datarcvnbr;
	Gnum vertlocnbr;
	Gnum edgelocnbr;
	Gnum pvrtlocnum;
	Gnum vertlocbas;
	Gnum edgelocnum;
	Gnum partlocidx;
	Gnum meshlocnbr;
	Gnum meshlocnum;
	Gnum fronlocidx;
        Gnum nodeent;
	int procngbnum;
  	Gnum * restrict hashtab;
  	Gnum * restrict srcpartloctax;
	Gnum * restrict bvlbloctax;
	Gnum * restrict rmshloctx2;
	Gnum * restrict bvelloctax;
	Gnum * restrict dvelloctax;
	double * restrict dvelloctx2;
	Gnum * restrict vnumgsttax;
	Gnum * restrict bnumgsttax;
	Gnum * restrict bvrtloctax;
	Gnum * restrict bvndloctax;
	Gnum * restrict bedggsttax;
	Gnum * restrict cvelloctab;
	Gnum * restrict parttax;
	VertLoad * restrict veloglbtax;
	Mesh * imshloctab;
	Mesh * omshloctab;

        nodeent = orgmeshptr->enttglbnbr - (1 - baseval); // XXX temporaire, récupérer la valeur de data passé en argument
	CHECK_FERR (dmeshValueData (orgmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
	rmshloctax -= baseval;
        CHECK_FERR (PAMPA_dmeshHaloValue ((PAMPA_Dmesh *) orgmeshptr, baseval, PAMPA_TAG_REMESH), proccomm);
//#define PAMPA_DEBUG_DMESH_SAVE
#ifdef PAMPA_DEBUG_DMESH_SAVE
        {
          char s1[50];
          if (memAllocGroup ((void **) (void *)
                &vnumgsttax, (size_t) (orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
                &parttax,    (size_t) (sizeof (Gnum)),
                NULL) == NULL) {
            errorPrint ("Out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);
          vnumgsttax -= baseval;
          parttax -= baseval;

          memSet (vnumgsttax + baseval, ~0, orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum));
          for (vertlocnum = baseval, vertlocnnd = orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
            if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
              vnumgsttax[vertlocnum] = 0;
          //for (procngbnum = 0; procngbnum < orgmeshptr->procglbnbr; procngbnum ++) 
            parttax[baseval] = 0;

          imshloctab = NULL;
          meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
          CHECK_FERR (dmeshGatherInduceMultiple (orgmeshptr, 0, 1, vnumgsttax, parttax + baseval, &meshlocnbr, &imshloctab), proccomm);
          if (meshlocnbr > 0) {
            sprintf (s1, "dmesh-adapt-rmsh1-%d-%d.mesh", infoptr->eitenum, infoptr->iitenum);
            CHECK_FERR (infoptr->EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, infoptr->dataptr, NULL, s1), proccomm);

            meshExit (imshloctab);
          }
          memFree (imshloctab);
          memFreeGroup (vnumgsttax + baseval);
        }
#endif /* PAMPA_DEBUG_DMESH_SAVE */
//#undef PAMPA_DEBUG_DMESH_SAVE
        //commBarrier (MPI_COMM_WORLD);
        //exit (0);


	vertlocmax = orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;
	for (rmshlocnbr = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++) {
          //if (infoptr->iitenum < 5)
          //  rmshloctax[vertlocnum] = PAMPA_TAG_VERT_REMESH;
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		rmshlocnbr ++;
        }

  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);
#ifndef PAMPA_INFO_ADAPT
        if (orgmeshptr->proclocnum == 0) {
#endif /* PAMPA_INFO_ADAPT */
        infoPrint ("-- Iteration number: %d", infoptr->iitenum);
        infoPrint ("Number of elements to be remeshed before growing band: %d", rmshglbnbr);
#ifndef PAMPA_INFO_ADAPT
        }
#endif /* PAMPA_INFO_ADAPT */
        //rmshglbnb3 = rmshglbnb2;
        //rmshglbnb2 = rmshglbnbr;

	//if ((rmshglbnbr == 0) || (infoptr->iitenum > 1)) {//CHANGER NB ITER ICI --- CECILE
	if (rmshglbnbr == 0) {
	//if ((rmshglbnbr == 0) || ((infoptr->eitenum == 1) && (infoptr->iitenum == 3))) {
          CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
	  break;
        }

	// XXX ne devrait-on pas faire cette alloc une bonne fois pour toutes les
	// itérations ?
  	if (memAllocGroup ((void **) (void *)
		  &rmshloctx2, (size_t) (orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
		  &dvelloctax, (size_t) (orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
  	rmshloctx2 -= baseval;
  	dvelloctax -= baseval;
        
        CHECK_FERR (dmeshValueData (orgmeshptr, baseval, PAMPA_TAG_WEIGHT, NULL, NULL, (void **) &dvelloctx2), proccomm);
        dvelloctx2 -= baseval;

        memCpy (rmshloctx2 + baseval, rmshloctax + baseval, orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

        //if (infoptr->iitenum > 1) 
        CHECK_FERR (infoptr->EXT_dmeshBand ((PAMPA_Dmesh *) orgmeshptr, infoptr->dataptr, rmshloctx2 + baseval, ibndval), proccomm);

        for (rmshlocnb2 = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
          //rmshlocnb2 += ceil (dvelloctx2[vertlocnum]);
          if (rmshloctx2[vertlocnum] == PAMPA_TAG_VERT_REMESH)
            rmshlocnb2 ++;

        if (commAllreduce (&rmshlocnb2, &rmshglbnb2, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

	////printf("loc %d\n",enodlocnbr);
        //CHECK_VDBG (cheklocval, proccomm);
        //if (commAllreduce (&enodlocnbr, &enodglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
        //  errorPrint ("communication error");
        //  cheklocval = 1;
        //}
        //CHECK_VERR (cheklocval, proccomm);

        //if (orgmeshptr->proclocnum == 0)
	//printf("npcible %d ntetra %d\n",enodglbnbr, rmshglbnbr);
        //cvrtglbnbr = ((rmshglbnbr / 6) > 10000) ? MAX((enodglbnbr - orgmeshptr->enttloctax[baseval]->vertglbnbr / 6) / 300000, rmshglbnbr / 6000000): 1;
        double cond, val1, val2;
        cond = 10000;
        //cond = 100;
        if (infoptr->iitenum > 2) 
          if ((infoptr->enodglbnbr / (orgmeshptr->enttloctax[baseval]->vertglbnbr / 6)) > 2)
            infoptr->enodglbnbr = MAX (orgmeshptr->enttloctax[baseval]->vertglbnbr / 3, infoptr->enodglbnbr / 2);
          else if ((1.1 * infoptr->enodglbnbr / (orgmeshptr->enttloctax[baseval]->vertglbnbr / 6)) > 1)
            infoptr->enodglbnbr =
              ((1.1 * orgmeshptr->enttloctax[baseval]->vertglbnbr / 6) > (0.8 * infoptr->enodglbnbr)) ?
              1.1 * orgmeshptr->enttloctax[baseval]->vertglbnbr / 6 :
              0.8 * infoptr->enodglbnbr;

        val1 = (1.0 * infoptr->enodglbnbr - (1.0 * orgmeshptr->enttloctax[baseval]->vertglbnbr - rmshglbnb2) / 6) / 250000;
        //val1 = (1.0 * infoptr->enodglbnbr - (1.0 * orgmeshptr->enttloctax[baseval]->vertglbnbr - rmshglbnb2) / 6) / 2500;
        val2 = 1.0 * rmshglbnb2 / 1000000;
        //if (infoptr->iitenum != 1) 
        //  cvrtglbnbr = coldglbnbr;
        //else {
        cvrtglbnbr = ((1.0 * rmshglbnb2 / 6) > cond) ? MAX(ceil(val1), ceil(val2)) : 1;
        //cvrtglbnbr /= 5;
        //cvrtglbnbr = 2; //orgmeshptr->procglbnbr * 2.5;
        //if (infoptr->iitenum == 1) 
        //  cvrtglbnbr *= 2;
        //else
        //if (infoptr->iitenum == 13)
        //  cvrtglbnbr = 1;
        if ((infoptr->iitenum % 4) == 1)
          infoptr->zonenbr = -1;

        if ((infoptr->zonenbr != -1) && ((cvrtglbnbr != 1) || (infoptr->iitenum < 4)))
          cvrtglbnbr = infoptr->zonenbr;
        if (infoptr->zonenb2 != ~0)
          cvrtglbnbr = infoptr->zonenb2;

        //  coldglbnbr = cvrtglbnbr;
        //}
        // XXX temporaire
        //if (infoptr->iitenum == 2)
        //  cvrtglbnbr = coldglbnbr;
        //else if (infoptr->iitenum == 3)
        //  cvrtglbnbr = 1;
        //  XXX fin temporaire
        //if ((coldglbnbr == cvrtglbnbr) && (ibndval != 0) && (cvrtglbnbr != 1)) {
        //  ibndval = MAX(0, ibndval - 1);
        //  CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
        //  memFree (hashtab);
        //  continue;
        //}
        //else if ((coldglbnbr == cvrtglbnbr) && (cvrtglbnbr == 1)) {
        //  CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
        //  memFree (hashtab);
        //  break;
        //}

        //cvrtglbnbr = ceil (1.0 * rmshglbnb2 / 25000);
        if (orgmeshptr->proclocnum == 0)
          infoPrint ("vertglb: " GNUMSTRING ", rmshglb: " GNUMSTRING ", rmshglb2: " GNUMSTRING ", enodglbnbr: " GNUMSTRING ", cond: %lf, val1: %lf, val2: %lf, cvrt:" GNUMSTRING "\n",
              orgmeshptr->enttloctax[baseval]->vertglbnbr,
              rmshglbnbr,
              rmshglbnb2,
              infoptr->enodglbnbr,
              cond,
              val1,
              val2,
              cvrtglbnbr);
                                                         
                                                         
#ifdef PAMPA_DEBUG_ADAPT
        if (0 != PAMPA_TAG_VERT_NOREMESH) {
          errorPrint ("PAMPA_TAG_VERT_NOREMESH is not 0");
          cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
#endif /* PAMPA_DEBUG_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE
        {
          char s1[50];
          if (memAllocGroup ((void **) (void *)
                &vnumgsttax, (size_t) (orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
                &parttax,    (size_t) (sizeof (Gnum)),
                NULL) == NULL) {
            errorPrint ("Out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);
          vnumgsttax -= baseval;
          parttax -= baseval;

          memSet (vnumgsttax + baseval, ~0, orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum));
          for (vertlocnum = baseval, vertlocnnd = orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
            if (rmshloctx2[vertlocnum] == PAMPA_TAG_VERT_REMESH)
              vnumgsttax[vertlocnum] = 0;
          //for (procngbnum = 0; procngbnum < orgmeshptr->procglbnbr; procngbnum ++) 
            parttax[baseval] = 0;

          imshloctab = NULL;
          meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
          CHECK_FERR (dmeshGatherInduceMultiple (orgmeshptr, 0, 1, vnumgsttax, parttax + baseval, &meshlocnbr, &imshloctab), proccomm);
          if (meshlocnbr > 0) {
            sprintf (s1, "dmesh-adapt-rmsh2-%d.mesh", wmshcnt ++);
            CHECK_FERR (infoptr->EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, infoptr->dataptr, NULL, s1), proccomm);

            meshExit (imshloctab);
          }
          memFree (imshloctab);
          memFreeGroup (vnumgsttax + baseval);
        }
#endif /* PAMPA_DEBUG_DMESH_SAVE */

        for (vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++) {
          dvelloctax[vertlocnum] = round(dvelloctx2[vertlocnum]);
        }

  	// 1) Identification des zones à remailler
  	// 	a) conversion du maillage en graphe uniquement pour les sommets qui ont un drapeau
        // XXX FIXME bonne idée d'utiliser DMESH_ENTT_MAIN_PART, cela évite de
        // calculer ensuite le graphe induit, mais faut-il encore avoir
        // l'ancienne numérotation (vlblloctax à rajouter dans dmeshDgraphBuild
        // pour DMESH_ENTT_MAIN_PART). En attendant, on continue comme avant...
  	//CHECK_FERR (dmeshDgraphBuild (orgmeshptr, DMESH_ENTT_MAIN_PART, rmshlocnbr, dvrtgsttax, dvelloctax, &dgrfdat, &dvrtlocbas), proccomm);
  	CHECK_FERR (dmeshDgraphBuild (orgmeshptr, DMESH_ENTT_MAIN, ~0, NULL, dvelloctax, NULL, &dgrfdat, &dvrtlocbas), proccomm); // XXX pas de edlo ??
        CHECK_FERR (SCOTCH_dgraphGhst (&dgrfdat), proccomm);

#ifdef PAMPA_DEBUG_DGRAPH
  	char s[30];
  	sprintf(s, "dgrfdat-adapt-%d", orgmeshptr->proclocnum);
  	FILE *dgraph_file;
  	dgraph_file = fopen(s, "w");
  	SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
  	fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

        //CHECK_VDBG (cheklocval, proccomm);
        //if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
        //  errorPrint ("communication error");
        //  cheklocval = 1;
        //}
        //CHECK_VERR (cheklocval, proccomm);

  	// 	a bis) élargissement des zones avec un dgraphBand ok
  	//if (ibndval > 0) {
	//  CHECK_FERR (SCOTCH_dgraphBand (&dgrfdat, rmshlocnbr, fronloctax + baseval, ibndval, &bgrfdat), proccomm);
        //}
        //else {
          // 	b) sous-graphe induit des zones ok
        memCpy (rmshloctx2 + baseval, rmshloctax + baseval, orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
        CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnbr, &bgrfdat), proccomm);
        //}
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (&bgrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	CHECK_FERR (SCOTCH_dgraphGhst (&bgrfdat), proccomm);
  	//SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, &bvrtgstnbr, (SCOTCH_Num **) &bvrtloctax, (SCOTCH_Num **) &bvndloctax, NULL, (SCOTCH_Num **) NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bedggsttax, NULL, NULL); 
  	SCOTCH_dgraphData (&bgrfdat, NULL, &bvrtglbnbr, &bvrtlocnbr, NULL, &bvrtgstnbr, (SCOTCH_Num **) &bvrtloctax, (SCOTCH_Num **) &bvndloctax, (SCOTCH_Num **) &bvelloctax, (SCOTCH_Num **) &bvlbloctax, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bedggsttax, NULL, NULL); 
	bvlbloctax -= baseval;
	bvelloctax -= baseval;
	bvrtloctax -= baseval;
	bvndloctax -= baseval;
	bedggsttax -= baseval;

        for (bvellocsum = bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
          bvellocsum += bvelloctax[bvrtlocnum];

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllreduce (&bvellocsum, &bvelglbsum, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

        //if (switval == 1)
        //  cvrtglbnbr = (bvelglbsum > ballsiz) ? bvelglbsum / ballsiz : 1;
        //else
        //  cvrtglbnbr = (bvelglbsum > (2 * ballsiz)) ? bvelglbsum / (2 * ballsiz) : 1;
        //cvrtglbnbr = (bvelglbsum > ballsiz) ? bvelglbsum / ballsiz : 1;

        switval = 1 - switval;
#ifdef PAMPA_INFO_ADAPT
        //infoPrint ("dvelglbsum : %d, rmshglbnbr : %d, ballsiz : %d, cvrtglbnbr : %d", bvelglbsum, rmshglbnbr, ballsiz, cvrtglbnbr);
#else /* PAMPA_INFO_ADAPT */
        if (orgmeshptr->proclocnum == 0) {
          infoPrint ("estimated weight: %d, number of elements to be remeshed: %d, number of zones: %d", bvelglbsum, rmshglbnbr, cvrtglbnbr);
        }
#endif /* PAMPA_INFO_ADAPT */
  	// 	c) contraction du graphe des zones ok
  	if (memAllocGroup ((void **) (void *)
		  &bnumgsttax, (size_t) (bvrtgstnbr * sizeof (Gnum)),
                  // XXX supprimer ci-dessous « * 2 »
		  &cvelloctab, (size_t) (cvrtglbnbr * 2 * sizeof (Gnum)), // XXX * 2 temporaire pour le partitionnement géom
		  //&bvlbloctax, (size_t) (bvrtlocnbr * sizeof (Gnum)), 
		  &vnumgsttax, (size_t) (orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
                  &parttax, (size_t) ((cvrtglbnbr + orgmeshptr->procglbnbr) * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
  	bnumgsttax -= baseval;
  	vnumgsttax -= baseval;
        parttax -= baseval;
	//bvlbloctax -= baseval;
  	memSet (cvelloctab, 0, 2 * cvrtglbnbr * sizeof (Gnum)); // XXX * 2 temporaire pour le partitionnement géom
  	memSet (dsndcnttab, 0, orgmeshptr->procglbnbr * sizeof (int));
  	memSet (vnumgsttax + baseval, ~0, orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));



	//if ((bvelglbsum > ballsiz) && (rmshglbnb2 != rmshglbnb3)) {

        if (cvrtglbnbr > 1) {
  	  //CHECK_FERR (dmeshAdapt2 (orgmeshptr, &bgrfdat, bnumgsttax, &cgrfdat, cprcvrttab, coarval - 1, coarnbr, coarrat), orgmeshptr->proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
          CHECK_FERR (SCOTCH_dgraphCheck (&bgrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
//#define PAMPA_DEBUG_DGRAPH
#ifdef PAMPA_DEBUG_DGRAPH
          {
            char s[30];
            FILE *dgraph_file;
            errorPrint ("cvrtglbnbr: %d\n", cvrtglbnbr);
            sprintf(s, "dgrf-adapt-%d-%d", infoptr->iitenum, orgmeshptr->proclocnum);
            dgraph_file = fopen(s, "w");
            SCOTCH_dgraphSave (&bgrfdat, dgraph_file);
            fclose(dgraph_file);
          }
#endif /* PAMPA_DEBUG_DGRAPH */
//#undef PAMPA_DEBUG_DGRAPH
          //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "r{sep=q{strat=g},seq=r{sep=g}}"), orgmeshptr->proccomm);
          //CHECK_FERR (SCOTCH_dgraphPart (&bgrfdat, cvrtglbnbr, &strtdat, bnumgsttax + baseval), orgmeshptr->proccomm);

          // XXX début temporaire
          {
            PAMPA_Iterator it;
            PAMPA_Iterator it_nghb;
            PAMPA_Num X, Y, Z, num, flag;
            double x, y, z, valx, valy, valz, minx, maxx, miny, maxy, minz, maxz, dltx, dlty, dltz, * geomtax;

            CHECK_FERR (PAMPA_dmeshHaloValue ((PAMPA_Dmesh *) orgmeshptr, nodeent, PAMPA_TAG_GEOM), proccomm); // XXX baseval + 1 suppose qu'il n'y a que 2 entités
            CHECK_FERR (dmeshValueData(orgmeshptr, nodeent, PAMPA_TAG_GEOM, NULL, NULL, (void **) &geomtax), proccomm);
            geomtax -= 3 * baseval;

            if ((infoptr->bndgflg == 0) && ((infoptr->iitenum % 4) == 1)) { /* No given bounding box */
              minx = DBL_MAX;
              miny = DBL_MAX;
              minz = DBL_MAX;
              maxx = - DBL_MAX;
              maxy = - DBL_MAX;
              maxz = - DBL_MAX;



              PAMPA_dmeshItInitStart((PAMPA_Dmesh *) orgmeshptr, nodeent, PAMPA_VERT_LOCAL, &it);
              while (PAMPA_itHasMore(&it)) {
                PAMPA_Num nodnum;

                nodnum = PAMPA_itCurEnttVertNum(&it);
                x = geomtax[nodnum * 3];
                y = geomtax[nodnum * 3 + 1];
                z = geomtax[nodnum * 3 + 2];

                if (x < minx)
                  minx = x;
                if (x > maxx)
                  maxx = x;
                if (y < miny)
                  miny = y;
                if (y > maxy)
                  maxy = y;
                if (z < minz)
                  minz = z;
                if (z > maxz)
                  maxz = z;
                PAMPA_itNext(&it);
              }

#ifdef PAMPA_DEBUG_MEM
              {
                double min2x, max2x, min2y, max2y, min2z, max2z;
                CHECK_FMPI (cheklocval, commAllreduce (&minx, &min2x, 1, MPI_DOUBLE, MPI_MIN, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (&maxx, &max2x, 1, MPI_DOUBLE, MPI_MAX, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (&miny, &min2y, 1, MPI_DOUBLE, MPI_MIN, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (&maxy, &max2y, 1, MPI_DOUBLE, MPI_MAX, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (&minz, &min2z, 1, MPI_DOUBLE, MPI_MIN, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (&maxz, &max2z, 1, MPI_DOUBLE, MPI_MAX, orgmeshptr->proccomm), orgmeshptr->proccomm);
                minx = min2x;
                miny = min2y;
                minz = min2z;
                maxx = max2x;
                maxy = max2y;
                maxz = max2z;
              }
#else /* PAMPA_DEBUG_MEM */
                CHECK_FMPI (cheklocval, commAllreduce (MPI_IN_PLACE, &minx, 1, MPI_DOUBLE, MPI_MIN, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (MPI_IN_PLACE, &maxx, 1, MPI_DOUBLE, MPI_MAX, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (MPI_IN_PLACE, &miny, 1, MPI_DOUBLE, MPI_MIN, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (MPI_IN_PLACE, &maxy, 1, MPI_DOUBLE, MPI_MAX, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (MPI_IN_PLACE, &minz, 1, MPI_DOUBLE, MPI_MIN, orgmeshptr->proccomm), orgmeshptr->proccomm);
                CHECK_FMPI (cheklocval, commAllreduce (MPI_IN_PLACE, &maxz, 1, MPI_DOUBLE, MPI_MAX, orgmeshptr->proccomm), orgmeshptr->proccomm);
#endif /* PAMPA_DEBUG_MEM */
              infoptr->minxval = minx;
              infoptr->maxyval = maxx;
              infoptr->minyval = miny;
              infoptr->maxxval = maxy;
              infoptr->minzval = minz;
              infoptr->maxzval = maxz;
            }
            else {
              minx = infoptr->minxval;
              maxy = infoptr->maxxval;
              miny = infoptr->minyval;
              maxx = infoptr->maxyval;
              minz = infoptr->minzval;
              maxz = infoptr->maxzval;
            }

            dltx = maxx - minx;
            dlty = maxy - miny;
            dltz = maxz - minz;

            //minx = -6;
            //miny = -5;
            //minz = 0;
            //maxx = 4.5;
            //maxy = 5;
            //maxz = 6;
            //if ((infoptr->iitenum % 2) == 1) {
            //  minx = -3;
            //  miny = -2.5;
            //  minz = 0;
            //  maxx = 2.5;
            //  maxy = 2.5;
            //  maxz = 3;
            //}
            //else {
            //  minx = -3.5;
            //  miny = -3;
            //  minz = -0.5;
            //  maxx = 3;
            //  maxy = 3;
            //  maxz = 3.5;
            //}

            //maxx -= minx;
            //maxy -= miny;
            //maxz -= minz;

            if (infoptr->bndgflg != 0) 
              cvrtglbnbr --;

            //// XXX temp
            //X = 5;
            //Y = 5;
            //Z = 6 * (int) ceil(dltz);
            //if (infoptr->eitenum != 1) {
            //  for (; ((X * Y * Z) > cvrtglbnbr) && (X > 2) && (Y > 2); X--, Y--);
            //  if ((X * Y * Z) < cvrtglbnbr) {
            //    Y++;
            //    if ((X * Y * Z) < cvrtglbnbr)
            //      X++;
            //  }
            //  else
            //    for (; ((X * Y * Z) > cvrtglbnbr) && (Z > 3); Z >>= 1);
            //}
            //// XXX fin temp
            for (X = 1; (X * X * X) < cvrtglbnbr; X++);
            for (Y = 1; (X * Y * Y) < cvrtglbnbr; Y++);
            for (Z = 1; (X * Y * Z) < cvrtglbnbr; Z++);

            if (infoptr->bndgflg != 0) 
              cvrtglbnbr ++;

            if (infoptr->zonenbr == ~0) {
              if (infoptr->bndgflg == 0)
                cvrtglbnbr = X * Y * Z;
              else
                cvrtglbnbr = X * Y * Z + 1;
              infoptr->zonenbr = cvrtglbnbr;
            }

            if ((infoptr->iitenum % 4) == 2) {
              minx +=  (0.5 * dltx / X);
              maxx +=  (0.5 * dltx / X);
              miny +=  (0.5 * dlty / Y);
              maxy +=  (0.5 * dlty / Y);
              minz +=  (0.5 * dltz / Z);
              maxz +=  (0.5 * dltz / Z);
            }
            else if ((infoptr->iitenum % 4) == 3) {
              minx +=  (0.25 * dltx / X);
              maxx +=  (0.25 * dltx / X);
              miny +=  (0.25 * dlty / Y);
              maxy +=  (0.25 * dlty / Y);
              minz +=  (0.25 * dltz / Z);
              maxz +=  (0.25 * dltz / Z);
            }
            else if ((infoptr->iitenum % 4) == 0) {
              minx +=  (0.75 * dltx / X);
              maxx +=  (0.75 * dltx / X);
              miny +=  (0.75 * dlty / Y);
              maxy +=  (0.75 * dlty / Y);
              minz +=  (0.75 * dltz / Z);
              maxz +=  (0.75 * dltz / Z);
            }
            if (orgmeshptr->proclocnum == 0)
              infoPrint ("minx: %lf, maxx: %lf, miny: %lf, maxy: %lf, minz: %lf, maxz: %lf, X: %d, Y: %d, Z: %d", minx, maxx, miny, maxy, minz, maxz, X, Y, Z);
            //if (cvrtglbnbr != (X * Y * Z)) {
            //  if (orgmeshptr->proclocnum == 0)
            //    errorPrint ("Invalid number of zones");
            //  return (1);
            //}
            //printf ("cvrt: %d\n", cvrtglbnbr);

            if (orgmeshptr->proclocnum == 0)
              infoPrint ("nb zones : " GNUMSTRING, cvrtglbnbr);

            PAMPA_dmeshItInit((PAMPA_Dmesh *) orgmeshptr, baseval, nodeent, &it_nghb);


            // XXX temporaire
            memset (bnumgsttax + baseval, ~0, bvrtlocnbr * sizeof (Gnum));
            //cvrtglbnbr = 1;
            for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
              PAMPA_Num tetnum;

              tetnum = bvlbloctax[bvrtlocnum] - dvrtlocbas;
#ifdef PAMPA_DEBUG_ADAPT2
              if ((tetnum < baseval) || (tetnum > (orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval))) {
                errorPrint ("tetnum %d not correct\n", tetnum);
                return (1);
              }
#endif /* PAMPA_DEBUG_ADAPT2 */
              x = y = z = 0.0;

              PAMPA_itStart(&it_nghb, tetnum);
              while (PAMPA_itHasMore(&it_nghb)) {
                PAMPA_Num nodnum;

                nodnum = PAMPA_itCurEnttVertNum(&it_nghb);
                x += geomtax[nodnum * 3];
                y += geomtax[nodnum * 3 + 1];
                z += geomtax[nodnum * 3 + 2];
                //if ((geomtax[nodnum * 3] > x) || (geomtax[nodnum * 3 + 1] > y) || (geomtax[nodnum * 3 + 2] > z)) {
                //  x = geomtax[nodnum * 3];
                //  y = geomtax[nodnum * 3 + 1];
                //  z = geomtax[nodnum * 3 + 2];
                //}
                //if (geomtax[nodnum * 3] > x)
                //  x = geomtax[nodnum * 3];
                //if (geomtax[nodnum * 3 + 1] > y)
                //  y = geomtax[nodnum * 3 + 1];
                //if (geomtax[nodnum * 3 + 2] > z)
                //  z = geomtax[nodnum * 3 + 2];
                PAMPA_itNext(&it_nghb);
              }
              x /= 4;
              y /= 4;
              z /= 4;
              //flag = 0;
              //if (x < minx)
              //  flag = 1;
              //else if (x > maxx)
              //  flag = 1;
              //else if (y < miny)
              //  flag = 1;
              //else if (y > maxy)
              //  flag = 1;
              //else if (z < minz)
              //  flag = 1;
              //else if (z > maxz)
              //  flag = 1;
              //if ((x < 0.1) && (y < 0.1) && (z < 0.1))
              //if (x < 0.1)
              //  bnumgsttax[bvrtlocnum] = 0;
              //if (flag == 0) {
              valx = (x - minx) / dltx;
              valy = (y - miny) / dlty;
              valz = (z - minz) / dltz;
              //valx = valx + (0.25 * (infoptr->iitenum - 1) / X);
              //valy = valy + (0.25 * (infoptr->iitenum - 1) / Y);
              //valz = valz + (0.25 * (infoptr->iitenum - 1) / Z);
              //valx = 0.5-cos(valx * M_PI) / 2;
              //valy = 0.5-cos(valy * M_PI) / 2;
              //valz = 0.5-cos(valz * M_PI) / 2;
              if ((infoptr->bndgflg == 1) && (
                    (x < minx) || (x > maxx) ||
                    (y < miny) || (y > maxy) ||
                    (z < minz) || (z > maxz))) 
                num = cvrtglbnbr - 1;
              else
                num = (((PAMPA_Num) (valx * X)) % X) * Y * Z + (((PAMPA_Num) (valy * Y)) % Y) * Z + (((PAMPA_Num) (valz * Z)) % Z);
              //else
              //  num = cvrtglbnbr - 1;
              //if ((num < 0) || (num > cvrtglbnbr)) {
              //  errorPrint ("num %d invalid, max is %d\n", num, cvrtglbnbr);
              //  return (1);
              //}
              //if (orgmeshptr->proclocnum == 0)
              //  printf ("x: %lf, y: %lf, z: %lf, num: %d\n", x, y, z, num);
              bnumgsttax[bvrtlocnum] = num;
              //if (bvrtlocnum > 10)
              //  exit(0);
            }
          }



          // XXX fin temporaire
          cprcvrttab[0] = 0;
          for (procngbnum = 0; procngbnum < orgmeshptr->procglbnbr; procngbnum ++)
            cprcvrttab[procngbnum + 1] = cprcvrttab[procngbnum] + DATASIZE(cvrtglbnbr, orgmeshptr->procglbnbr, procngbnum);
          cvrtlocnbr = cprcvrttab[orgmeshptr->proclocnum + 1] - cprcvrttab[orgmeshptr->proclocnum];



#ifdef PAMPA_DEBUG_DMESH_SAVE2
          {
            char s1[100];
            Gnum * vnumloctax;
            Gnum * vflgloctax;
            Gnum * vflgglbtax;
            Gnum parttab[1];

            parttab[0] = 0;

            if (memAllocGroup ((void **) (void *)
                  &vnumloctax, (size_t) (orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
                  &vflgloctax, (size_t) (bvrtlocnbr * sizeof (Gnum)),
                  &vflgglbtax, (size_t) (bvrtglbnbr * sizeof (Gnum)),
                  NULL) == NULL) {
              errorPrint ("Out of memory");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, proccomm);
            vnumloctax -= baseval;
            vflgloctax -= baseval;
            vflgglbtax -= baseval;

            memSet (vnumloctax + baseval, ~0, orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
            for (vertlocnum = baseval, vertlocnnd = bvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
              if (rmshloctx2[bvlbloctax[vertlocnum] - dvrtlocbas] == PAMPA_TAG_VERT_REMESH) {
                vnumloctax[bvlbloctax[vertlocnum] - dvrtlocbas] = 0;
                vflgloctax[vertlocnum] = bnumgsttax[vertlocnum];
              }

            imshloctab = NULL;
            meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
            CHECK_FERR (dmeshGatherInduceMultiple (orgmeshptr, 0, 1, vnumloctax, parttab - baseval, &meshlocnbr, &imshloctab), proccomm);
            sprintf (s1, "dmesh-adapt-part-%d.mesh", infoptr->iitenum);
            CHECK_FERR (dmeshAdaptGatherSave3 (orgmeshptr, bvrtlocnbr, vflgloctax, vflgglbtax), proccomm);
            if (meshlocnbr > 0) {
              CHECK_FERR (infoptr->EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, infoptr->dataptr, vflgglbtax + baseval, s1), proccomm);
              meshExit (imshloctab);
            }
            memFreeGroup (vnumloctax + baseval);

            memFree (imshloctab);
          }
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
          // XXX pour enregistrer la partition, il faut ne garder que la partie
          // du maillage correspondand au graphe bgrfdat (cf. rmshloctx2)
//#ifdef PAMPA_DEBUG_DMESH_SAVE
//        char s1[50];
//        sprintf (s1, "dmesh-adapt-part-%d.mesh", wmshcnt);
//        CHECK_FERR (dmeshAdaptGatherSave2 (orgmeshptr, bnumgsttax, dataptr, s1), proccomm);
//#endif /* PAMPA_DEBUG_DMESH_SAVE */

          SCOTCH_stratExit (&strtdat);
          SCOTCH_stratInit (&strtdat);

	  CHECK_FERR (SCOTCH_dgraphHalo (&bgrfdat, bnumgsttax + baseval, GNUM_MPI), orgmeshptr->proccomm);

	  //CHECK_FERR (SCOTCH_dgraphGhst (&cgrfdat), proccomm);
	  //SCOTCH_dgraphData (&cgrfdat, NULL, &cvrtglbnbr, &cvrtlocnbr, NULL, &cvrtgstnbr, NULL, NULL, (SCOTCH_Num **) &cvelloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	  //cvelloctax -= baseval;

#ifdef PAMPA_INFO_ADAPT
          infoPrint ("-- Iteration number: %d", infoptr->iitenum);
          infoPrint ("Number of vertices to be remeshed : %d", rmshglbnbr);
          infoPrint ("Number of colors : %d", cvrtglbnbr);
          infoPrint ("Number of local colors : %d", cvrtlocnbr);
#endif /* PAMPA_INFO_ADAPT */

	  cvrtlocmin = cprcvrttab[orgmeshptr->proclocnum];
	  cvrtlocmax = cprcvrttab[orgmeshptr->proclocnum + 1] - 1;
	  bvrtgstmax = bvrtlocnbr + baseval - 1;
  	  // 	d) partitionnement des gros sommets ko
  	  //                 * partitionnement de graphes
  	  //                         * les gros sommets sont indépendants donc absence de relations
  	  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

	  //for (bvrtlocnum = dvrtlocnum = baseval, dvrtlocnnd = dvrtlocnbr + baseval; dvrtlocnum < dvrtlocnnd; dvrtlocnum ++)
	  //  if (rmshloctx2[dvrtlocnum] == PAMPA_TAG_VERT_REMESH)
	  //	bvlbloctax[bvrtlocnum ++] = dvrtlocnum + dvrtlocbas;

          memSet (vnumgsttax + baseval, ~0, orgmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
		//if (cvelgsttax[bnumgsttax[bvrtlocnum]] > (ballsiz / 10))
                //FIXME peut-on faire un test sur les tailles de boule, le
                //problème est que bnumgsttax contient un sommet qui peut ne pas
                //être dans les ghst
            //if ((bnumgsttax[bvrtlocnum] < 0) || (bnumgsttax[bvrtlocnum] >= cvrtglbnbr)) {
            //  errorPrint ("color " GNUMSTRING " invalid, max is " GNUMSTRING, bnumgsttax[bvrtlocnum], cvrtglbnbr);
            //  return (1);
            //}
	  	  vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = bnumgsttax[bvrtlocnum]; /* Propagate colors on elements */
                  // XXX temporaire YYY
                  if (bnumgsttax[bvrtlocnum] >= 0)
                    // XXX fin temporaire YYY
                  cvelloctab[bnumgsttax[bvrtlocnum]] ++;
          }
	  for (vertlocnum = baseval, vertlocnnd = orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
            if ((vnumgsttax[vertlocnum] < ~0) || (vnumgsttax[vertlocnum] >= cvrtglbnbr)) {
              errorPrint ("Color " GNUMSTRING " invalid for vertex " GNUMSTRING, vnumgsttax[vertlocnum] , vertlocnum);
              return (1);
            }
          }

          CHECK_FERR (dmeshGrow ( orgmeshptr, baseval, vnumgsttax, cvrtglbnbr, nodeent, ibndval), orgmeshptr->proccomm); // XXX on fait des suppositions sur les entités, il faut passer par la bib externe
          
	  /////* First pass to remove elements which have neighbors with different color */
  	  //for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
	  //	Gnum cvrtlocnum;
          //      Gnum bedglocnum;
          //      Gnum bedglocnnd;
	  //	Gnum ucollocval; /* If undo coloring */

	  //	cvrtlocnum = bnumgsttax[bvrtlocnum];

	  //	for (ucollocval = 0, bedglocnum = bvrtloctax[bvrtlocnum], bedglocnnd = bvndloctax[bvrtlocnum]; bedglocnum < bedglocnnd; bedglocnum ++) /* Undo coloring on elements which are at the boundary */
	  //    if (bnumgsttax[bedggsttax[bedglocnum]] != cvrtlocnum) { /* If vertex and neighbor have different colors */
	  //    	ucollocval = 1;
	  //    	if (bedggsttax[bedglocnum] <= bvrtgstmax) 
	  //  	  vnumgsttax[bvlbloctax[bedggsttax[bedglocnum]] - dvrtlocbas] = ~0;
	  //    }
	  //	if (ucollocval == 1) { /* There are at least one neighbor with different color */
	  //    vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = ~0;
	  //	  // FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	}
	  //}
	  memFreeGroup (rmshloctx2 + baseval);

	  //CHECK_FERR (dmeshHaloSync (orgmeshptr, orgmeshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), orgmeshptr->proccomm);

	  //// FIXME temporarly disabled
	  ///* Second pass to remove elements which are isolated */
	  //for (vertlocnum = baseval, vertlocnnd = orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
	  //	Gnum ucollocval; /* If undo coloring */
	  //	Gnum vnumlocval;
	  //	Gnum edgelocnnd;
	  //	const DmeshEntity * enttlocptr = orgmeshptr->enttloctax[baseval];

	  //	vnumlocval = vnumgsttax[vertlocnum];

	  //	if (vnumlocval != ~0) {
	  //    for (ucollocval = 0, edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
	  //  	  edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
	  //  	  ucollocval == 0 && edgelocnum < edgelocnnd; edgelocnum ++) {
	  //    	Gnum nghblocnum;

	  //    	nghblocnum = orgmeshptr->edgeloctax[edgelocnum];

	  //    	if (vnumgsttax[nghblocnum] == vnumlocval) /* If vertex and neighbor have same colors */
	  //    	  ucollocval = 1;
	  //    }
	  //	  if (ucollocval == 0) { /* If no neighbor with same color */
	  //    	vnumgsttax[vertlocnum] = ~0;
	  //	  	// FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	  }
	  //	}
	  //}

          CHECK_FERR (dmeshZoneDist (orgmeshptr, infoptr, bvrtlocnbr, bnumgsttax, cvrtlocnbr, cvrtglbnbr, cprcvrttab, cvelloctab, parttax), orgmeshptr->proccomm); 
          SCOTCH_dgraphFree (&bgrfdat);
	}
	else {
          memFree (hashtab);
	  memFreeGroup (rmshloctx2 + baseval);
#ifdef PAMPA_INFO_ADAPT
          infoPrint ("-- Iteration number: %d", infoptr->iitenum);
          infoPrint ("Number of vertices to be remeshed : %d", rmshglbnbr);
          infoPrint ("Last iteration");
#endif /* PAMPA_INFO_ADAPT */
	  memSet (bnumgsttax + baseval, 0, bvrtgstnbr * sizeof (Gnum));
  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
	  	vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = bnumgsttax[bvrtlocnum]; /* Propagate colors on elements */

          CHECK_FERR (dmeshGrow ( orgmeshptr, baseval, vnumgsttax, cvrtglbnbr, nodeent, ibndval), orgmeshptr->proccomm); // XXX on fait des suppositions sur les entités, il faut passer par la bib externe

  	  if ((parttax = (Gnum *) memAlloc (1 * sizeof (Gnum))) == NULL) {
      	errorPrint  ("out of memory");
      	cheklocval = 1;
  	  }
  	  CHECK_VERR (cheklocval, proccomm);
	  parttax -= baseval;
	  parttax[baseval] = 0; // FIXME pourquoi 0 ???
	  cvrtglbnbr = 1;
	}


  	// 2) Extraction des sous-maillages et envoie sur chaque proc ok
	meshlocnbr = -1;
	imshloctab = NULL;
  	CHECK_FERR_STR (dmeshGatherInduceMultiple (orgmeshptr, 1, cvrtglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), "extraction", proccomm);
        //CHECK_FERR (dmeshValueLink (orgmeshptr, (void **) &srcprocvrttab, PAMPA_VALUE_PRIVATE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_PROC, TAG_INT_PROCVRT), proccomm);
        //memCpy (srcprocvrttab, orgmeshptr->procvrttab, (orgmeshptr->procglbnbr + 1) * sizeof (Gnum));
        CHECK_FERR (dmeshValueData (orgmeshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, NULL, NULL, (void **) &srcpartloctax), proccomm);
        srcpartloctax -= baseval;

#ifdef PAMPA_INFO_ADAPT
        infoPrint ("dmeshInducePart, vertex number before: %d", orgmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */
  	//CHECK_FERR_STR (dmeshInducePart (orgmeshptr, srcpartloctax, PAMPA_TAG_VERT_INTERNAL, &indmeshdat), "mesh induit", proccomm);
#ifdef PAMPA_INFO_ADAPT
        infoPrint ("dmeshInducePart, vertex number after: %d", indmeshdat.vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

        //orgmeshptr = &indmeshdat;

        //// XXX debut temporaire
        //if (orgmeshptr->proclocnum == 0) {
        //  PAMPA_Mesh m2;
        //  // Initialisation of PaMPA mesh structure
        //  CHECK_FERR(PAMPA_meshInit(&m2), proccomm);

        //  CHECK_FERR(PAMPA_dmeshGather (orgmeshptr, &m2), proccomm);

        //  CHECK_FERR(infoptr->EXT_meshSave(&m2, 1, dataptr, NULL, "apres_gather.mesh"), proccomm);

        //  // Finalisation of PaMPA mesh structure
        //  PAMPA_meshExit(&m2);
        //}
        //else {
        //  // Gather PaMPA mesh
        //  CHECK_FERR (PAMPA_dmeshGather (orgmeshptr, NULL), proccomm);
        //}
        //// XXX fin temporaire


#ifdef PAMPA_INFO_ADAPT
	infoPrint ("Number of received meshes : %d", meshlocnbr);
	infoPrint ("Number of local vertices in source distributed mesh : %d", orgmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

	memFreeGroup (bnumgsttax + baseval);
	if (cvrtglbnbr == 1)
	  memFree (parttax + baseval);

  	// 3) Remaillage des zones
  	// 	a) conversion pampa -> mmg ok
  	// 	b) remaillage ok
  	// 	c) conversion mmg -> pampa quasi ok
  	if ((omshloctab = (Mesh *) memAlloc (meshlocnbr * sizeof (Mesh))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);

        infoptr->cuntval = 0;

	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) { // XXX cette partie pourrait être parallélisé avec pthread
          // XXX cette partie ne doit pas avoir de CHECK_{F,V}ERR, mais comme
          // pour les remailleurs vu que le nombre de zones peut être différent
          // d'un proc à un autre
          Gnum * restrict inbrtab;
          Gnum * restrict onbrtab;

#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (imshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshInit (omshloctab + meshlocnum);
	  CHECK_FERR_STR (infoptr->EXT_meshAdapt ((PAMPA_Mesh *) (imshloctab + meshlocnum), (PAMPA_Mesh *) (omshloctab + meshlocnum), infoptr, 0), "remesh zone", proccomm); // FIXME supprimer le 0 qui correspond à un drapeau pour le remailleur, qui est stocké dasn le infoptr
	  omshloctab[meshlocnum].idnum = imshloctab[meshlocnum].idnum;
          omshloctab[meshlocnum].procglbnbr = imshloctab[meshlocnum].procglbnbr;
          cheklocval = meshValueData (omshloctab + meshlocnum, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART, NULL, NULL, (void **) &inbrtab);
          if (cheklocval == 2) {
            CHECK_FERR (meshValueData (imshloctab + meshlocnum, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART, NULL, NULL, (void **) &inbrtab), orgmeshptr->proccomm);
            CHECK_FERR (meshValueLink(omshloctab + meshlocnum, (void **) &onbrtab, PAMPA_VALUE_NONE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART), orgmeshptr->proccomm);
            memCpy (onbrtab, inbrtab, orgmeshptr->procglbnbr * sizeof (Gnum));
            cheklocval = 0;
          }
          // FIXME : Et s'il y a d'autres valeurs associées, ne devrait-on pas
          // les recopier, sachant que :
          // 1) elles sont récupérées lors du rebuild
          // 2) pour les entt réelles, seule la routine PAMPA_remesh peut le faire
#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (omshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshExit (imshloctab + meshlocnum);
	}
	memFree (imshloctab);
	CHECK_VDBG (cheklocval, proccomm);


  	// 4) Reintegration des zones dans maillage distribué en cours
	CHECK_FERR_STR (dmeshRebuild (orgmeshptr, meshlocnbr, omshloctab), "reintegration", proccomm);

        if (infoptr->itdebug != NULL) {
          File f;
          char s[500];
          sprintf (s, infoptr->itdebug, infoptr->eitenum, infoptr->iitenum);
          f.name = s;
          f.mode = "w";
          f.pntr = stdout;
          CHECK_FERR (fileBlockOpen (&f, 1), proccomm);
          CHECK_FERR (PAMPA_dmeshSave (orgmeshptr, f.pntr), proccomm);
          fileBlockClose (&f, 1);
        }
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (dmeshCheck (orgmeshptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
        //// XXX debut temporaire
        //if (orgmeshptr->proclocnum == 0) {
        //  PAMPA_Mesh m2;
        //  // Initialisation of PaMPA mesh structure
        //  CHECK_FERR(PAMPA_meshInit(&m2), proccomm);

        //  CHECK_FERR(PAMPA_dmeshGather (orgmeshptr, &m2), proccomm);

        //  CHECK_FERR(infoptr->EXT_meshSave(&m2, 1, dataptr, NULL, "apres_rebuild.mesh"), proccomm);

        //  // Finalisation of PaMPA mesh structure
        //  PAMPA_meshExit(&m2);
        //}
        //else {
        //  // Gather PaMPA mesh
        //  CHECK_FERR (PAMPA_dmeshGather (orgmeshptr, NULL), proccomm);
        //}
        //// XXX fin temporaire

#ifdef PAMPA_INFO_ADAPT
	infoPrint ("Number of local vertices in destination distributed mesh : %d", orgmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE2
        {
          char s1[50];
          sprintf (s1, "dmesh-adapt-apres-%d", wmshcnt ++);
          CHECK_FERR (dmeshAdaptGatherSave (orgmeshptr, NULL, infoptr->EXT_meshSave, infoptr->dataptr, s1), proccomm);
        }
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) 
	  meshFree (omshloctab + meshlocnum);

	memFree (omshloctab);

//#ifdef PAMPA_DEBUG_DMESH_SAVE
//#ifndef PAMPA_DEBUG_DMESH_SAVE2
//        //char s1[50];
//#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
//        //sprintf (s1, "dmesh-adapt-%d-%d", orgmeshptr->proclocnum, wmshcnt ++);
//        //CHECK_FERR (dmeshAdaptGatherSave (orgmeshptr, NULL, dataptr, s1), proccomm);
//        if (memAllocGroup ((void **) (void *)
//              &vnumgsttax, (size_t) (orgmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
//              &parttax,    (size_t) (orgmeshptr->procglbnbr * sizeof (Gnum)),
//              NULL) == NULL) {
//          errorPrint ("Out of memory");
//          cheklocval = 1;
//        }
//        CHECK_VERR (cheklocval, proccomm);
//        vnumgsttax -= baseval;
//        parttax -= baseval;
//
//        for (vertlocnum = baseval, vertlocnnd = orgmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
//          vnumgsttax[vertlocnum] = orgmeshptr->proclocnum;
//        for (procngbnum = 0; procngbnum < orgmeshptr->procglbnbr; procngbnum ++) 
//          parttax[procngbnum + baseval] = procngbnum;
//
//        imshloctab = NULL;
//        meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
//        CHECK_FERR (dmeshGatherInduceMultiple (orgmeshptr, 0, orgmeshptr->procglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), proccomm);
//        sprintf (s1, "dmsh-adapt-%d-%d.mesh", orgmeshptr->proclocnum, wmshcnt ++);
//        CHECK_FERR (infoptr->EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, dataptr, vnumgsttax + baseval, s1), proccomm);
//
//        meshExit (imshloctab);
//        memFree (imshloctab);
//        memFreeGroup (vnumgsttax + baseval);
//#endif /* PAMPA_DEBUG_DMESH_SAVE */


	CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
	SCOTCH_dgraphFree (&dbgfdat);
        //break;

        //if ((infoptr->iitenum % 4) == 1) {
        {
          Gnum vertmax;
          Gnum vertmin;
          if (commAllreduce (&orgmeshptr->vertlocnbr, &vertmax, 1, GNUM_MPI, MPI_MAX, proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);
          if (commAllreduce (&orgmeshptr->vertlocnbr, &vertmin, 1, GNUM_MPI, MPI_MIN, proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          if (orgmeshptr->proclocnum == 0)
            infoPrint ("vertmin: " GNUMSTRING ", vertmax: " GNUMSTRING, vertmin, vertmax);
          if (vertmax > (vertmin * 1.25)) {
            if (orgmeshptr->proclocnum == 0)
              infoPrint ("je rentre dedans");
            Dmesh orgmeshdat;
            PAMPA_Strat strtdt2;
            Gnum * partloctx2;
            CHECK_FERR (PAMPA_stratInit (&strtdt2), proccomm);
            CHECK_FERR (dmeshInit (&orgmeshdat, proccomm), proccomm);
            if ((partloctx2 = (Gnum *) memAlloc (orgmeshptr->vertlocnbr * sizeof (Gnum))) == NULL) {
              errorPrint  ("out of memory");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, proccomm);
            partloctx2 -= baseval;

            CHECK_FERR_STR(dmeshMapElem(orgmeshptr, orgmeshptr->procglbnbr, NULL, &strtdt2, partloctx2), "part again", proccomm);
            CHECK_FERR(dmeshRedist(orgmeshptr, partloctx2, NULL, -1, -1, -1, &orgmeshdat), proccomm);
            memFree (partloctx2 + baseval);
            PAMPA_stratExit(&strtdt2);
            PAMPA_dmeshExit(orgmeshptr);
            *orgmeshptr = orgmeshdat;
#ifdef PAMPA_INFO_ADAPT
            infoPrint ("Number of local vertices in destination redistributed mesh : %d", orgmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */
          }
        }

        infoptr->iitenum ++;
  }

  memFreeGroup (cprcvrttab);
  //CHECK_FERR_STR (dmeshDgraphBuild (orgmeshptr, DMESH_ENTT_ANY, ~0, NULL, NULL, &dgrfdat, NULL), "graphe final", proccomm);
  //SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

  //if (memAllocGroup ((void **) (void *)
  //      	&paroloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
  //      	&partloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
  //      NULL) == NULL) {
  //  errorPrint ("Out of memory");
  //  cheklocval = 1;
  //}
  //CHECK_VERR (cheklocval, proccomm);
  //paroloctax -= baseval;
  //partloctax -= baseval;

  //for (vertlocnum = baseval, vertlocnnd = dvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
  // 	paroloctax[vertlocnum] = orgmeshptr->proclocnum;

  //// 5) Repartitionnement
  //vmloloctab = NULL;
  //emraval = 1;
  //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "q{strat=m{asc=b{width=3,bnd=(d{pass=40,dif=1,rem=0}|)f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.01,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=0}}"), proccomm);
  //CHECK_FERR (SCOTCH_dgraphRepart (&dgrfdat, orgmeshptr->procglbnbr, paroloctax + baseval, emraval, vmloloctab, &strtdat, partloctax + baseval), proccomm);

  dmeshDgraphExit (&dgrfdat);
  SCOTCH_dgraphExit (&bgrfdat);
  SCOTCH_dgraphExit (&cgrfdat);
  SCOTCH_dgraphExit (&dbgfdat);
  SCOTCH_stratExit (&strtdat);
//  if (orgmeshptr->proclocnum == 0)
//	SCOTCH_graphExit (&cbgfdat);
  // 5 bis) Redistribution ok
  //CHECK_FERR (dmeshRedist (orgmeshptr, partloctax, NULL, -1, -1, -1, dstmeshptr), proccomm); // XXX peut-être utiliser un dsmeshRedist qui serait moins gourmand en mémoire à mon avis
  //memFreeGroup (paroloctax + baseval);
  CHECK_VDBG (cheklocval, proccomm);
  return (0);
} 
