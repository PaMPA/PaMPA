/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       Declaration file for the libPampa
//!                parallel mesh partitioning and adaptation
//!                library.
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The type and structure definitions.
*/

#ifndef __PAMPA__
#define __PAMPA__

/*+ Version flags. +*/

#define PAMPA_VERSION DUMMYVERSION
#define PAMPA_RELEASE DUMMYRELEASE
#define PAMPA_PATCHLEVEL DUMMYPATCHLEVEL

/*+ Integer type and communicator. +*/

typedef DUMMYINT PAMPA_Num;

#define PAMPAF_NUMMAX               DUMMYMAXINT
#define PAMPA_MPI_NUM               DUMMYMPIINT
#define PAMPA_NUMSTRING             DUMMYPAMPASTRING





/*+ Flags for associated values. +*/

#define PAMPA_TAG_NIL             -42 // temporary tag
#define PAMPA_TAG_GEOM            -1
#define PAMPA_TAG_REMESH          -2
#define PAMPA_TAG_REF             -3 // 1 corresponding to the frontier, see: PAMPA_TAG_VERT_FRONTIER
#define PAMPA_TAG_SOL_2DI         -4 // 2D isotropic (1 value)
#define PAMPA_TAG_SOL_2DAI        -5 // 2D anisotropic (4 values)
#define PAMPA_TAG_SOL_3DI         -6 // 3D isotropic (1 value)
#define PAMPA_TAG_SOL_3DAI        -7 // 3D anisotropic (6 values)
#define PAMPA_TAG_STATUS          -8 // Vertex could be on boundary, external or internal
#define PAMPA_TAG_PART            -9 // Partitioning
#define PAMPA_TAG_PERM            -10 // Permutation
#define PAMPA_TAG_WEIGHT          -11 // Weight which corresponds to the remesher workload
#define PAMPA_TAG_LOAD            -12 // Load used by partitioner
#define PAMPA_TAG_LVL_PART        -13 // Partitionning for multigrid
#define PAMPA_TAG_INTSOL          -14 // Interpolated solution

#define PAMPA_TAG_VERT_REMESH     2
#define PAMPA_TAG_VERT_FRONTIER   1 /* we suppose that is 1 by default */
#define PAMPA_TAG_VERT_NOREMESH   0 /* 0 in order to fill in arrays with 0 */
#define PAMPA_TAG_VERT_EXTERNAL (~0) /* ~0 in order to fill in arrays with ~0 */
#define PAMPA_TAG_VERT_INTERNAL ((~0) - 1)

/*+ Flags for entities and sub-entities +*/
#define PAMPA_ENTT_SINGLE         -1    // TRICK: PAMPA_ENTT_SINGLE must be the max value of entity tags
#define PAMPA_ENTT_STABLE         -2

#define PAMPA_ENTT_DUMMY          -1
#define PAMPA_ENTT_VIRT_VERT      -2
#define PAMPA_ENTT_VIRT_PROC      -3   // TRICK: PAMPA_ENTT_VIRT_PROC must be the min value of virtual entity tags

/*+ Flags for mesh check +*/
#define PAMPA_CHECK_ALONE         0x0001


/*+ Flags for matrix +*/
#define PAMPA_MAT_NGHB_FIRST      -1

/*+ Flags for vertices +*/
#define PAMPA_VERT_LOCAL          (~ 0x1100002)
#define PAMPA_VERT_INTERNAL       (~ 0x2200004)
#define PAMPA_VERT_BOUNDARY       (~ 0x4400008)
#define PAMPA_VERT_OVERLAP        (~ 0x8800010)
#define PAMPA_VERT_ANY            (~ 0x0000020)
#define PAMPA_VERT_GHOST          (~ 0x0000040)
#define PAMPA_VERT_GLOBAL         (~ 0x0000080)
#define PAMPA_VERT_FRONTIER       (~ 0x0f00100)
#define PAMPA_VERT_NOT_FRONTIER   (~ 0xf000200)

/*+ Flags for errors +*/ // TODO: one bit per flag in order to combine them
#define PAMPA_ERR_MEM           1
#define PAMPA_ERR_COMM          2
#define PAMPA_ERR_DATA          3
#define PAMPA_ERR_LOC_DATA      4
#define PAMPA_ERR_GLB_DATA      5
#define PAMPA_ERR_VRT_ARRAY     6
#define PAMPA_ERR_LNK_ARRAY     7
#define PAMPA_ERR_LNK_NBR       8
#define PAMPA_ERR_LNK_ELEMENT   9
#define PAMPA_ERR_INTERNAL     10
#define PAMPA_ERR_LNK_MISSING  11
#define PAMPA_ERR_LNK_REDEF    12
#define PAMPA_ERR_LNK_DUP      13

/*+ Flags for values +*/
#define PAMPA_VALUE_NONE      0x0000
#define PAMPA_VALUE_PRIVATE   0x0001
#define PAMPA_VALUE_PUBLIC    0x0002
#define PAMPA_VALUE_ALLOCATED 0x0004
#define PAMPA_VALUE_NOTFREE   0x0008 // means that the value with this flag won't be freed by PaMPA (by default a value is freed)

/*+ Flags for overlap +*/
#define PAMPA_OVLP_NOGT (((PAMPA_Num) (1)) << 18)
#define PAMPA_OVLP_ADJ  (((PAMPA_Num) (1)) << 19) // TODO: maybe not used
#define PAMPA_OVLP_ELEM (((PAMPA_Num) (1)) << 5) // XXX à cause du fortran, car 20 est trop grand a priori
#define PAMPA_OVLP_HALO (((PAMPA_Num) (1)) << 21) // TODO: maybe not used
#define PAMPA_OVLP_FLAGS (PAMPA_OVLP_NOGT | PAMPA_OVLP_ADJ | PAMPA_OVLP_ELEM | PAMPA_OVLP_HALO) /* All overlap flags */

/*+ Flags for multigrid +*/
#define PAMPA_LVL_PART_LOCAL  0x0001
#define PAMPA_LVL_PART_GLOBAL 0x0002
#define PAMPA_LVL_ITER_ORDER  0x0004
#define PAMPA_LVL_ORIG_ORDER  0x0008

/*+ Other flags +*/
#ifdef PAMPA_NOT_REF_ONE
#ifndef PAMPA_REF_IS
#define PAMPA_REF_IS 1
#endif /* PAMPA_REF_IS */
#endif /* PAMPA_NOT_REF_ONE */

/*+ Opaque objects. The dummy sizes of these objects,
computed at compile-time by program "dummysizes",
are given as double values for proper padding +*/

typedef struct {            
  double                    PAMPA_dummy[DUMMYSIZEMDMESH];
} PAMPA_Mdmesh;                                        

typedef struct {
  double                    PAMPA_dummy[DUMMYSIZEMESH];
} PAMPA_Mesh;

typedef struct {
  double                    PAMPA_dummy[DUMMYSIZEDMESH];
} PAMPA_Dmesh;

typedef struct {
  double                    PAMPA_dummy[DUMMYSIZESMESH];
} PAMPA_Smesh;

typedef struct {
  double                    PAMPA_dummy[DUMMYSIZEARCH];
} PAMPA_Arch;

typedef struct {
  double                    PAMPA_dummy[DUMMYSIZESTRAT];
} PAMPA_Strat;

typedef struct {
  double                    PAMPA_dummy[DUMMYSIZEDMESHHALOREQS];
} PAMPA_DmeshHaloReqs;

typedef struct {
  PAMPA_Num                 vertnnd;
  PAMPA_Num                 nentmsk;
  PAMPA_Num                 nesbnum;
  PAMPA_Num                 ventnum;
  PAMPA_Num                 nentnum;
  const PAMPA_Num *         nentbas;
  const PAMPA_Num *         edgetab;
  const PAMPA_Num *         svrttab;
  const PAMPA_Num *         mngbtab;
  const PAMPA_Num *         vindtab;
  const PAMPA_Num *         nenttab;
  const PAMPA_Num *         sngbtab;
#if (defined PAMPA_DEBUG_ITER || defined PAMPA_DEBUG_ITER_INT)
  char                      s[500];
  PAMPA_Num                 vertmax;
#endif /* (defined PAMPA_DEBUG_ITER || defined PAMPA_DEBUG_ITER_INT) */
} PAMPA_IteratorConst;

typedef struct PAMPA_Iterator_ {
  PAMPA_Num                 vertnum;
  const PAMPA_IteratorConst c;

#ifdef __cplusplus

  PAMPA_Iterator_():c() {
  }
#endif /* __cplusplus */
} PAMPA_Iterator;

typedef struct PAMPA_AvIterator_ {
  void *                    valuptr;
} PAMPA_AvIterator;


/*
**  The function prototypes.
*/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

int PAMPA_mdmeshCoarLevelBuild (
    PAMPA_Mdmesh * const         meshptr,
    PAMPA_Num * const            meshglbptr,
    const PAMPA_Num              velolocsiz,
    const PAMPA_Num * const      veloloctab,
    const PAMPA_Num              edlolocsiz,
    const PAMPA_Num * const      edloloctab,
    const PAMPA_Num * const      enttglbtab,
    const PAMPA_Num              elvlglbsiz,
    const PAMPA_Num * const      elvlglbtab,
    const PAMPA_Num * const      lnumglbtab,
    const PAMPA_Num * const      etagglbtab,
    const PAMPA_Num              loadglbval,
    const PAMPA_Num              flagval);

int PAMPA_mdmeshDmeshData (
    const PAMPA_Mdmesh * const mdmhptr,
    PAMPA_Num      const       mdmhglbnum,
    PAMPA_Dmesh ** const       meshptr);

int PAMPA_mdmeshInit (
    PAMPA_Mdmesh * const         mdmeshptr,
    PAMPA_Num      const         baseval,
    PAMPA_Num      const         mdmhglbnbr,
    MPI_Comm                     proccomm);

int PAMPA_mdmeshItInit (
    PAMPA_Mdmesh * const         meshptr,   
    PAMPA_Num const              vlvllocnum,
    PAMPA_Num const              ventnum,   
    PAMPA_Num const              nlvllocnum,
    PAMPA_Num const              nesbnum,   
    PAMPA_Iterator *             iterptr);  

int PAMPA_mdmeshItInitStart (
    PAMPA_Mdmesh * const         meshptr,   
    PAMPA_Num const              levllocnum,
    PAMPA_Num const              ventlocnum,
    PAMPA_Num const              flagval,   
    PAMPA_Iterator *             iterptr);

void PAMPA_mdmeshExit (
    PAMPA_Mdmesh * const         mdmeshptr);

void PAMPA_mdmeshFree (
    PAMPA_Mdmesh * const         mdmeshptr);

int PAMPA_mdmeshLevelInit (
    PAMPA_Mdmesh * const         mdmhptr,
    PAMPA_Dmesh * const          meshptr);

int PAMPA_mdmeshRedist (
    PAMPA_Mdmesh * const         srcmeshptr,
    PAMPA_Num * const            partloctab,
    PAMPA_Num   const            vertlocdlt,
    PAMPA_Num   const            edgelocdlt,
    PAMPA_Num const              ovlpglbval,
    PAMPA_Mdmesh * const         dstmeshptr);

int PAMPA_meshInit (
      PAMPA_Mesh * const           meshptr);

void PAMPA_meshExit (
    PAMPA_Mesh * const           meshptr);

void PAMPA_meshFree (
    PAMPA_Mesh * const           meshptr);

int PAMPA_meshBuild (
    PAMPA_Mesh * const           meshptr,
    const PAMPA_Num              baseval,
    const PAMPA_Num              vertnbr,
    PAMPA_Num * const            verttab,
    PAMPA_Num * const            vendtab,
    PAMPA_Num * const            vlbltab,
    const PAMPA_Num              edgenbr,
    const PAMPA_Num              edgesiz,
    PAMPA_Num * const            edgetab,
    PAMPA_Num * const            edlotab,
    PAMPA_Num                    enttnbr,
    PAMPA_Num *                  venttab,
    PAMPA_Num *                  esubtab,
    PAMPA_Num *                  enlotab,
    PAMPA_Num                    valumax);

int PAMPA_meshCreate (
    PAMPA_Mesh * const           meshptr,
    const PAMPA_Num              vertnbr,
    PAMPA_Num * const            verttab,
    PAMPA_Num * const            vendtab,
    const PAMPA_Num              edgenbr,
    PAMPA_Num * const            edgetab,
    PAMPA_Num                    enttnbr,
    PAMPA_Num *                  venttab,
    PAMPA_Num                    baseval,
    PAMPA_Num                    degnghb);

int PAMPA_meshCheck (
    PAMPA_Mesh * const           meshptr);

int PAMPA_meshCheck2 (
    PAMPA_Mesh * const           meshptr,
    int                          flagval);

int PAMPA_meshValueLink (
    PAMPA_Mesh * const           meshptr,
    void **                      valutab,
    PAMPA_Num                    flagval,
    MPI_Datatype                 typeval,
    PAMPA_Num                    enttnum,
    PAMPA_Num                    tagnum);

int PAMPA_meshValueUnlink (
    PAMPA_Mesh * const           meshptr,
    PAMPA_Num                    enttnum,
    PAMPA_Num                    tagnum);

int PAMPA_meshValueData (
    const PAMPA_Mesh * const     meshptr,
    PAMPA_Num const              enttnum,
    PAMPA_Num                    tagnum,
    void **                      valutab);

void PAMPA_meshSize (
    const PAMPA_Mesh * const     meshptr,
    PAMPA_Num * const            enttnbr,
    PAMPA_Num * const            vertnbr);

int PAMPA_meshEnttSize (
    const PAMPA_Mesh * const     meshptr,
    PAMPA_Num const              enttnbr,
    PAMPA_Num * const            vertnbr);

void PAMPA_meshData (
    const PAMPA_Mesh * const     meshptr,
    PAMPA_Num * const            baseptr,
    PAMPA_Num * const            enttptr,
    PAMPA_Num * const            vertptr,
    PAMPA_Num * const            edgeptr,
    PAMPA_Num ** const           edgetab,
    PAMPA_Num * const            valuptr,
    PAMPA_Num * const            valuptz);

int PAMPA_meshVertData (
	const PAMPA_Mesh * const  meshptr, 
	const PAMPA_Num            vertnum,
	const PAMPA_Num            ventnum,
	const PAMPA_Num            nentnum,
	PAMPA_Num * const          nghbptr,
	PAMPA_Num * const          mvrtptr);

int PAMPA_meshPart (
    PAMPA_Mesh * const           meshptr,    
    const PAMPA_Num              partnbr,   
    PAMPA_Strat * const          stratptr,  
    PAMPA_Num * const            maptab);

int PAMPA_meshRepart (
    PAMPA_Mesh * const           meshptr,    
    const PAMPA_Num              partnbr,   
    PAMPA_Strat * const          stratptr,  
    PAMPA_Num * const            gmaptab);

int PAMPA_smeshInit (
      PAMPA_Smesh * const           meshptr);

void PAMPA_smeshExit (
    PAMPA_Smesh * const           meshptr);

void PAMPA_smeshFree (
    PAMPA_Smesh * const           meshptr);

int PAMPA_smeshBuild (
    PAMPA_Smesh * const           meshptr,
    const PAMPA_Num              baseval,
    const PAMPA_Num              vertnbr,
    PAMPA_Num * const            verttab,
    PAMPA_Num * const            vendtab,
    const PAMPA_Num              edgenbr,
    const PAMPA_Num              edgesiz,
    PAMPA_Num * const            edgetab,
    PAMPA_Num * const            edlotab,
    PAMPA_Num                    enttnbr,
    PAMPA_Num *                  venttab,
    PAMPA_Num *                  esubtab,
    PAMPA_Num *                  enlotab,
    PAMPA_Num                    valumax);

int PAMPA_smeshSave (
PAMPA_Smesh * const  meshptr,
FILE * const         stream);

int PAMPA_smeshSaveByName (
PAMPA_Smesh * const  meshptr,
char * const         nameval,
PAMPA_Num const      procnbr,
PAMPA_Num * const    procvrttab);

int PAMPA_smeshValueLink (
PAMPA_Smesh * const       meshptr,
void **                  valutab,
PAMPA_Num                flagval,
MPI_Datatype             typeval,
PAMPA_Num                enttnum,
PAMPA_Num                tagnum);

int PAMPA_smeshValueUnlink (
PAMPA_Smesh * const       meshptr,
PAMPA_Num                enttnum,
PAMPA_Num                tagnum);

int PAMPA_smeshValueData (
const PAMPA_Smesh * const     meshptr,
PAMPA_Num const              enttnum,
PAMPA_Num                    tagnum,
void **                      valutab);

int PAMPA_dmeshInit (
    PAMPA_Dmesh * const          meshptr,
    MPI_Comm                     proccomm);

// XXX debut tmp
#ifdef PAMPA_SCOTCH
int PAMPA_dmeshDgraph (
    PAMPA_Dmesh * const  meshptr,
    SCOTCH_Dgraph *      grafptr);
#endif /* PAMPA_SCOTCH */
// XXX fin tmp

void PAMPA_dmeshExit (
    PAMPA_Dmesh * const          meshptr);

void PAMPA_dmeshFree (
    PAMPA_Dmesh * const          meshptr);

int PAMPA_dmeshBuild (
    PAMPA_Dmesh * const          meshptr,
    const PAMPA_Num              baseval,
    const PAMPA_Num              vertlocnbr,
    const PAMPA_Num              vertlocmax,
    PAMPA_Num * const            vertloctab,
    PAMPA_Num * const            vendloctab,
    PAMPA_Num * const            vlblloctab,
    const PAMPA_Num              edgelocnbr,
    const PAMPA_Num              edgelocsiz,
    PAMPA_Num * const            edgeloctab,
    PAMPA_Num * const            edloloctab,
    PAMPA_Num                    enttglbnbr,
    PAMPA_Num *                  ventloctab,
    PAMPA_Num *                  esubloctab,
    PAMPA_Num *                  enloglbtab,
    PAMPA_Num                    valuglbmax,
    const PAMPA_Num              ovlpglbval);

int PAMPA_dmeshCheck (
    PAMPA_Dmesh * const          meshptr);

int PAMPA_dmeshGather (
    const PAMPA_Dmesh * const    dmshptr,
    PAMPA_Mesh * const           cmshptr);

int PAMPA_dmeshRedist (
	PAMPA_Dmesh * const	 smshptr,
	PAMPA_Num * const        partloctab,
	PAMPA_Num * const        permgsttab,
	PAMPA_Num   const        vertlocdlt,
	PAMPA_Num   const        edgelocdlt,
	PAMPA_Num   const        ovlpglbval,
	PAMPA_Dmesh * const      dmshptr);

int PAMPA_dmeshValueLink (
    PAMPA_Dmesh * const          meshptr,
    void **                      valuloctab,
    PAMPA_Num                    flagval,
    MPI_Datatype                 typeval,
    PAMPA_Num                    enttnum,
    PAMPA_Num                    tagnum);

int PAMPA_dmeshValueUnlink (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num                    enttnum,
    PAMPA_Num                    tagnum);

int PAMPA_dmeshValueTagUnlink (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num                    tagnum);

int PAMPA_dmeshValueData (
    const PAMPA_Dmesh * const    meshptr,
    PAMPA_Num const              enttnum,
    PAMPA_Num                    tagnum,
    void **                      valuloctab);

int PAMPA_dmeshVertData (
    const PAMPA_Dmesh * const    meshptr,   
    const PAMPA_Num              vertlocnum,
    const PAMPA_Num              ventlocnum,
    const PAMPA_Num              nentlocnum,
    PAMPA_Num * const            nghbgstptr,
    PAMPA_Num * const            vertglbptr,
    int * const                  procptr);

void PAMPA_dmeshSize (
    const PAMPA_Dmesh * const    meshptr,
    PAMPA_Num * const            enttglbptr,
    PAMPA_Num * const            vertlocptr,
    PAMPA_Num * const            vgstlocptr,
    PAMPA_Num * const            vertglbptr);

int PAMPA_dmeshEnttSize (
    const PAMPA_Dmesh * const    meshptr,
    PAMPA_Num const              enttlocnum,
    ...);

void PAMPA_dmeshData (
	const PAMPA_Dmesh * const  meshptr,   
	PAMPA_Num * const          baseptr,   
	PAMPA_Num * const          vertlocptr,
	PAMPA_Num * const          vertlocptz,
	PAMPA_Num * const          vertgstptr,
	PAMPA_Num ** const         vertloctab,
	PAMPA_Num ** const         vendloctab,
	PAMPA_Num * const          edgelocptr,
	PAMPA_Num * const          edgelocptz,
	PAMPA_Num ** const         edgeloctab,
	PAMPA_Num ** const         edloloctab,
	PAMPA_Num * const          enttglbptr,
	PAMPA_Num ** const         ventloctab,
	PAMPA_Num ** const         esubloctab,
	PAMPA_Num * const          valuglbptr,
	PAMPA_Num * const          valuglbptz,
	PAMPA_Num * const          ovlpglbptr,
	int       * const          procngbptr,
	int       ** const         procngbtab,
	MPI_Comm  * const          proccomm);

void PAMPA_dmeshData2 (
	const PAMPA_Dmesh * const  meshptr,   
	PAMPA_Num ** const         procvrtptr);

int PAMPA_dmeshGhst (
    PAMPA_Dmesh * const          meshptr);

int PAMPA_dmeshHalo (
    PAMPA_Dmesh * const          meshptr);

int PAMPA_dmeshHaloAsync (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_DmeshHaloReqs * const   requptr);

int PAMPA_dmeshHaloValue (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num                    enttnum,
    PAMPA_Num                    tagnum);

int PAMPA_dmeshHaloValueAsync (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num                    enttnum,
    PAMPA_Num                    tagnum, 
    PAMPA_DmeshHaloReqs * const   requptr);

int PAMPA_dmeshHaloEntt (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num                    enttnum);

int PAMPA_dmeshHaloEnttAsync (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num                    enttnum,
    PAMPA_DmeshHaloReqs * const   requptr);

int PAMPA_dmeshHaloWait (
    PAMPA_DmeshHaloReqs * const   requptr);

int PAMPA_dmeshHaloValueMultAsync (
    PAMPA_Dmesh * const           meshptr,
    PAMPA_Num                     enttnbr,
    PAMPA_Num  *                  entttab,
    PAMPA_Num  *                  tagtab,
    PAMPA_DmeshHaloReqs * const   reqsptr);

int PAMPA_dmeshHaloTagAsync (
    PAMPA_Dmesh * const         meshptr,
    PAMPA_Num     const         tagval,
    PAMPA_DmeshHaloReqs * const  requptr);

int PAMPA_dmeshHaloValueMult (
    PAMPA_Dmesh * const           meshptr,
    PAMPA_Num                     enttnbr,
    PAMPA_Num  *                  entttab,
    PAMPA_Num  *                  tagtab);

int PAMPA_dmeshLoad(
    PAMPA_Dmesh * const           meshptr,
    FILE * const                  stream,
    const PAMPA_Num               baseval,
    const PAMPA_Num               flagval);

int PAMPA_dmeshMatInit (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num const              enttlocnum);

int PAMPA_dmeshMatSize (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num const              enttlocnum,
    PAMPA_Num *                  matlocsize);

int PAMPA_dmeshMatVertData (
    PAMPA_Dmesh * const         meshptr,
    PAMPA_Num const             enttlocnum,
    PAMPA_Num const             vertlocnum,
    PAMPA_Num const             nghblocnum,
    PAMPA_Num *                 matindval);

int PAMPA_dmeshMatLineData (
    PAMPA_Dmesh * const         meshptr,   
    PAMPA_Num const             enttlocnum,
    PAMPA_Num const             vertlocnum,
    PAMPA_Num *                 matindnum, 
    PAMPA_Num *                 matindnnd);

int PAMPA_dmeshPart (
    PAMPA_Dmesh * const          meshptr,    
    const PAMPA_Num              partnbr,   
    PAMPA_Strat * const          stratptr,  
    PAMPA_Num * const            termloctab);

int PAMPA_dmeshPart2 (
    PAMPA_Dmesh * const          meshptr,    
    const char *               methval,
    const PAMPA_Num              partnbr,   
    PAMPA_Strat * const          stratptr,  
    PAMPA_Num * const            termloctab);

int PAMPA_dmeshMap (
    PAMPA_Dmesh * const          meshptr,   
    PAMPA_Arch * const           archptr,   
    PAMPA_Strat * const          stratptr,  
    PAMPA_Num * const            termloctab);

int PAMPA_dmeshSave (
    PAMPA_Dmesh * const  meshptr,
    FILE * const         stream);

int PAMPA_dmeshSaveAll (
    PAMPA_Dmesh * const  meshptr,
    FILE * const         stream);

int PAMPA_dmeshScatter (
    PAMPA_Dmesh * const          dmshptr,
    const PAMPA_Mesh * const     cmshptr,
    PAMPA_Num   const            ovlpglbval);

int PAMPA_dmeshScatter2 (
    PAMPA_Dmesh * const          dmshptr,
    const PAMPA_Mesh * const     cmshptr,
    PAMPA_Num const              typenbr,
    PAMPA_Num * const            entttab,
    PAMPA_Num * const            tagtab, 
    MPI_Datatype * const         typetab,
    PAMPA_Num   const            ovlpglbval);

int PAMPA_dmeshSave2 (
    PAMPA_Dmesh * const  meshptr,
    char * const fileval,
    int (*PAMPA_meshSave) (PAMPA_Mesh * const imshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval),
    void * const              dataptr);

int PAMPA_stratInit (
    PAMPA_Strat * const          stratptr);

void PAMPA_stratExit (
    PAMPA_Strat * const          stratptr);

int PAMPA_stratSave (
    const PAMPA_Strat * const    stratptr,
    FILE * const                 stream);

int PAMPA_dmeshItInitStart (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num const              ventlocnum,
    PAMPA_Num const              flagval,
    PAMPA_Iterator *             iterptr);

int PAMPA_meshItInitStart (
    PAMPA_Mesh * const           meshptr,
    PAMPA_Num const              ventlocnum,
    PAMPA_Iterator *             iterptr);

int PAMPA_dmeshItInit (
    PAMPA_Dmesh * const          meshptr,
    PAMPA_Num const              ventlocnum,
    PAMPA_Num const              nentlocnum,
    PAMPA_Iterator *             iterptr);

int PAMPA_meshItInit (
    PAMPA_Mesh * const           meshptr,
    PAMPA_Num const              ventlocnum,
    PAMPA_Num const              nentlocnum,
    PAMPA_Iterator *             iterptr);

int PAMPA_meshLoad(
    PAMPA_Mesh * const           meshptr,
    FILE * const                  stream,
    const PAMPA_Num               baseval,
    const PAMPA_Num               flagval);

int PAMPA_meshMeshSave (
    PAMPA_Mesh * const  meshptr,
    FILE * const              meshstm,
    FILE * const              solstm);

int PAMPA_meshSurfaceSave (
    PAMPA_Mesh * const  meshptr,
    FILE * const              meshstm,
    FILE * const              solstm);

int PAMPA_meshRebuild (
    PAMPA_Mesh * restrict const       bmshptr,
    PAMPA_Mesh * restrict const       imshptr,
    PAMPA_Mesh * restrict const       moutptr,
    const PAMPA_Num                  erefnum, 
    const PAMPA_Num                  trefnum, 
    const PAMPA_Num                  trefnbr, 
    const PAMPA_Num                  trefsiz, 
    PAMPA_Num * restrict * restrict const treftab);

int PAMPA_meshSave (
    PAMPA_Mesh * const  meshptr,
    FILE * const         stream);

int PAMPA_meshSaveAll (
    PAMPA_Mesh * const  meshptr,
    FILE * const         stream);

int PAMPA_smeshInit (
PAMPA_Smesh * const      smshptr);

void PAMPA_smeshExit (
PAMPA_Smesh * const      smshptr);

void PAMPA_smeshFree (
PAMPA_Smesh * const      smshptr);

void PAMPA_smeshSize (
const PAMPA_Smesh * const smshptr,
PAMPA_Num * const         enttnbr,
PAMPA_Num * const         vertptr);

int PAMPA_smeshEnttSize (
const PAMPA_Smesh * const smshptr,
PAMPA_Num const           enttnum,
PAMPA_Num * const         vertnbr);

void PAMPA_smeshData (
const PAMPA_Smesh * const  smshptr,              //!< Smesh structure to read
PAMPA_Num * const          baseptr,              //!< Base value            
PAMPA_Num * const          enttptr,              //!< Number of entities
PAMPA_Num * const          vertptr,              //!< Number of vertices
PAMPA_Num ** const         verttab,              //!< Index of start in edge tab
PAMPA_Num ** const         vendtab,              //!< Index of end in edge tab
PAMPA_Num * const          edgeptr,              //!< Number of links
PAMPA_Num * const          edgesiz,              //!< Size of edgetab
PAMPA_Num ** const         edgetab,              //!< Link array
PAMPA_Num ** const         venttab,              //!< Array of entity values for each vertex
PAMPA_Num * const          valuptr,              //!< Number of linked values
PAMPA_Num * const          valuptz);             //!< Maximum number of linked values

int PAMPA_smesh2mesh (
PAMPA_Smesh * const      smshptr,
PAMPA_Mesh *  const      meshptr);

int PAMPA_mesh2smesh (
PAMPA_Mesh * const       meshptr,
PAMPA_Smesh *  const     smshptr);

void PAMPA_printVersions ();

int PAMPA_meshAvItInit (
const PAMPA_Mesh * const     meshptr,
PAMPA_Num const              enttnum,
PAMPA_AvIterator *           iterptr);

int PAMPA_avItHasMore (
PAMPA_AvIterator * const    iterptr);

int PAMPA_avItNext (
PAMPA_AvIterator * const    iterptr);

void * PAMPA_avItCurValue (
PAMPA_AvIterator * const    iterptr);

PAMPA_Num PAMPA_avItCurTagNum (
PAMPA_AvIterator * const    iterptr);

MPI_Datatype PAMPA_avItCurTypeVal (
PAMPA_AvIterator * const    iterptr);

#ifdef PAMPA_DEBUG_ITER
int PAMPA_itHasMoreDebug(
    const PAMPA_Iterator *       iterptr);

PAMPA_Num PAMPA_itNextDebug(
    const PAMPA_Iterator *       iterptr);

int PAMPA_itStartDebug (
    const PAMPA_Iterator *       iterptr,   
    PAMPA_Num const              vertnum);

PAMPA_Num PAMPA_itCurEnttVertNumDebug(
    const PAMPA_Iterator *       iterptr);

PAMPA_Num PAMPA_itCurSubEnttVertNumDebug(
    const PAMPA_Iterator *       iterptr);

PAMPA_Num PAMPA_itCurMeshVertNumDebug(
    const PAMPA_Iterator *       iterptr);

PAMPA_Num PAMPA_itCurEnttNumDebug(
    const PAMPA_Iterator *       iterptr);

PAMPA_Num PAMPA_itCurSubEnttNumDebug(
    const PAMPA_Iterator *       iterptr);

#define PAMPA_itStart(iterptr, vertnum2) \
PAMPA_itStartDebug (iterptr, vertnum2)

#define PAMPA_itHasMore(iterptr) \
PAMPA_itHasMoreDebug (iterptr)

#define PAMPA_itNext(iterptr) \
PAMPA_itNextDebug (iterptr)

#define PAMPA_itCurEnttVertNum(iterptr) \
PAMPA_itCurEnttVertNumDebug (iterptr)

#define PAMPA_itCurMeshVertNum(iterptr) \
PAMPA_itCurMeshVertNumDebug (iterptr)

#define PAMPA_itCurSubEnttVertNum(iterptr) \
PAMPA_itCurSubEnttVertNumDebug (iterptr)

#define PAMPA_itCurEnttNum(iterptr) \
PAMPA_itCurEnttNumDebug (iterptr)

#define PAMPA_itCurSubEnttNum(iterptr) \
PAMPA_itCurSubEnttNumDebug (iterptr)

#define PAMPA_itIsSubEntt(iterptr) \
PAMPA_itIsSubEnttDebug (iterptr)

#define PAMPA_itHasSubEntt(iterptr) \
PAMPA_itHasSubEnttDebug (iterptr)


#else /* PAMPA_DEBUG_ITER */

#define PAMPA_itStart(iterptr, vertnum2)  \
  do {  \
    if ((iterptr)->c.vindtab != NULL) { \
      PAMPA_Num           vertnum_;  \
      PAMPA_Num           vertnnd_;  \
      if ((iterptr)->c.svrttab != NULL) {  \
        vertnum_ = (iterptr)->c.vindtab[(iterptr)->c.svrttab[vertnum2] << 1];  \
        vertnnd_ = (iterptr)->c.vindtab[((iterptr)->c.svrttab[vertnum2] << 1) + 1];  \
      }  \
      else {  \
        vertnum_ = (iterptr)->c.vindtab[vertnum2 << 1];  \
        vertnnd_ = (iterptr)->c.vindtab[(vertnum2 << 1) + 1];  \
      }  \
      \
      if ((iterptr)->c.nentbas == (iterptr)->c.nenttab) {  \
        while ((vertnum_ < vertnnd_) && ((iterptr)->c.nenttab[(iterptr)->c.edgetab[vertnum_]] != (iterptr)->c.nesbnum))  \
          vertnum_ ++;  \
      }  \
      (iterptr)->vertnum = vertnum_; \
      *((PAMPA_Num *) &((iterptr)->c.vertnnd)) = vertnnd_;  \
    } \
} while (0);

#define PAMPA_itHasMore(iterptr)  \
    ((iterptr)->vertnum < (iterptr)->c.vertnnd)

#define PAMPA_itNext(iterptr)  \
  do {  \
    PAMPA_Num           vertnum = (iterptr)->vertnum;  \
    do  \
      vertnum ++;  \
    while ((vertnum < (iterptr)->c.vertnnd) && ((iterptr)->c.nentbas[(iterptr)->c.edgetab[vertnum] & (iterptr)->c.nentmsk] != (iterptr)->c.nesbnum));  \
      (iterptr)->vertnum = vertnum;  \
  } while (0);

#define PAMPA_itIsSubEntt(iterptr)  \
  		((iterptr)->c.nentnum != (iterptr)->c.nesbnum)

#define PAMPA_itHasSubEntt(iterptr)  \
        ((iterptr)->c.sngbtab != NULL)

#define PAMPA_itCurSubEnttVertNum(iterptr)  \
        ((iterptr)->c.sngbtab[(iterptr)->c.edgetab[(iterptr)->vertnum]])

#define PAMPA_itCurMeshVertNum(iterptr)  \
        ((iterptr)->c.mngbtab[(iterptr)->c.edgetab[(iterptr)->vertnum]])

#define PAMPA_itCurEnttVertNum(iterptr)  \
        ((iterptr)->c.edgetab[(iterptr)->vertnum])

#define PAMPA_itCurSubEnttNum(iterptr)  \
        ((iterptr)->c.nenttab[(iterptr)->c.edgetab[(iterptr)->vertnum]])

#define PAMPA_itCurEnttNum(iterptr)  \
		(iterptr)->c.nentnum

#endif /* PAMPA_DEBUG_ITER */







#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __PAMPA__ */
