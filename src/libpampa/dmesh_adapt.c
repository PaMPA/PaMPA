/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Aug 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_ADAPT

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "pampa.h"
#include "pampa.h"
// FIXME ce n'est pas propre du tout
//#include "pampa-mmg3d.h"

#include <ptscotch.h>
#include "dmesh_adapt.h"
#include "dmesh_dgraph.h"
#include "dmesh_gather_induce_multiple.h"
#include "dmesh_rebuild.h"

/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshAdaptCoarsen (
    Dmesh * const    srcmeshptr,              /**< mesh */
	const Gnum                bandval,              /**< Band value */
	const Gnum                ballsiz,
	int (*PAMPA_remesh) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
	void * const              dataptr,
	Dmesh * const    dstmeshptr)
{
  SCOTCH_Dgraph dgrfdat;
  SCOTCH_Dgraph bgrfdat; /* Band graph */
  SCOTCH_Dgraph igrfdat; /* Induced graph */
  SCOTCH_Dgraph cgrfdat; /* Connex compound graph */
  SCOTCH_Dgraph dbgfdat; /* Distributed bigraph */
  SCOTCH_Graph  cbgfdat; /* Centralized bigraph */
  SCOTCH_Strat  strtdat; 
  Gnum * restrict rmshloctax;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  Gnum baseval;
  Gnum vertlocmax;
  Gnum coarval;
  Gnum coarnbr;
  double coarrat;
  Gnum ballsi2;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum dvrtlocnbr;
  Gnum emraval;
#ifdef PAMPA_DEBUG_ADAPT
  Gnum iternbr;
#endif /* PAMPA_DEBUG_ADAPT */
  int cheklocval;
  Gnum * restrict partloctax;
  Gnum * restrict paroloctax;
  Gnum * restrict vmloloctab;
  MPI_Comm proccomm;
#ifdef PAMPA_DEBUG_DMESH_SAVE
    Gnum wmshcnt = 0;
#endif /* PAMPA_DEBUG_DMESH_SAVE */

    printf("*******************\n\nCoarsen\n\n*******************\n");
  cheklocval = 0;
  coarnbr = 1;
  coarrat = 1;
  for (coarval = 1, ballsi2 = 1; ballsi2 < ballsiz; coarval ++, ballsi2 <<= 1);

  baseval = srcmeshptr->baseval;
  proccomm = srcmeshptr->proccomm;

  CHECK_FERR (SCOTCH_stratInit (&strtdat), proccomm);
  CHECK_FERR (dmeshDgraphInit (srcmeshptr, &dgrfdat), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&bgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&igrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&cgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&dbgfdat, proccomm), proccomm);
  if (srcmeshptr->proclocnum == 0)
	cheklocval = SCOTCH_graphInit (&cbgfdat);
  CHECK_VERR (cheklocval, proccomm);

#ifdef PAMPA_DEBUG_ADAPT
  iternbr = 1;
#endif /* PAMPA_DEBUG_ADAPT */

  while (1) {
	Gnum dvrtlocbas;
	Gnum bvrtlocnbr;
	Gnum bvrtlocnum;
	Gnum bvrtlocnnd;
	Gnum ivrtlocnbr;
	Gnum ivrtgstnbr;
	Gnum ivrtgstmax;
	Gnum cvrtlocnbr;
	Gnum cvrtgstnbr;
	Gnum cvrtglbnbr;
	Gnum cvrtlocmin;
	Gnum cvrtlocmax;
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;
	Gnum ivrtlocnum;
	Gnum ivrtlocnnd;
	Gnum datasndnbr;
	Gnum datarcvnbr;
	Gnum vertlocnbr;
	Gnum edgelocnbr;
	Gnum pvrtlocnum;
	Gnum vertlocbas;
	Gnum edgelocnum;
	Gnum partlocidx;
	Gnum meshlocnbr;
	Gnum meshlocnum;
	Gnum fronlocidx;
	int procngbnum;
	int procngbnnd;
  	Gnum hashsiz;          /* Size of hash table    */
  	Gnum hashnum;          /* Hash value            */
  	Gnum hashmax;
	Gnum hashbas;
  	Gnum hashnbr;
  	Gnum hashmsk;
	Gnum * fronloctax;
  	Gnum * restrict hashtab;
	Gnum * restrict bvlbloctax;
	Gnum * restrict rmshloctx2;
	Gnum * restrict vnumgsttax;
	Gnum * restrict inumgsttax;
	Gnum * restrict ivlbloctax;
	Gnum * restrict ivrtloctax;
	Gnum * restrict ivndloctax;
	Gnum * restrict iedggsttax;
	Gnum * cprcvrttab;
	Gnum * restrict dsndcnttab;
	Gnum * restrict dsnddsptab;
	Gnum * restrict drcvcnttab;
	Gnum * restrict drcvdsptab;
	Gnum * restrict cvelloctax;
	Gnum * restrict cvelgsttax;
    Gnum * restrict datasndtab;
    Gnum * restrict datarcvtab;
    Gnum * restrict datarcvtb2;
	Gnum * restrict vertloctax;
	Gnum * restrict veloloctax;
	Gnum * restrict edgeloctax;
	Gnum * restrict parttax;
	Gnum * restrict vertsidtab;
	Mesh * imshloctab;
	Mesh * omshloctab;

	CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
	rmshloctax -= baseval;

	vertlocmax = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;
	for (rmshlocnbr = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		rmshlocnbr ++;

#ifdef PAMPA_DEBUG_ADAPT
  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);
	  if (srcmeshptr->proclocnum == 0) {
		printf ("-- Iteration number: %d\n", iternbr);
	  	printf ("Number of vertices to be remeshed before dgraphBand: %d\n", rmshglbnbr);
	  }
#endif /* PAMPA_DEBUG_ADAPT */

	hashbas = 
  	hashnbr = vertlocmax / 10; // XXX N'est-ce pas trop grand pour hashnbr ?
  	for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */

  	hashmsk = hashsiz - 1;
  	hashmax = hashsiz >> 2;

  	if ((hashtab = (Gnum *) memAlloc (hashsiz * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	memSet (hashtab, ~0, hashsiz * sizeof (Gnum));
	
	// XXX ne devrait-on pas faire cette alloc une bonne fois pour toutes les
	// itérations ?
  	if (memAllocGroup ((void **) (void *)
		  &fronloctax, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
		  &rmshloctx2, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	fronloctax -= baseval;
  	rmshloctx2 -= baseval;

  	memCpy (rmshloctx2 + baseval, rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

	for (fronlocidx = vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		fronloctax[fronlocidx ++] = vertlocnum;

  	// 1) Identification des zones à remailler
  	// 	a) conversion du maillage en graphe uniquement pour les sommets qui ont un drapeau
  	dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_MAIN, &dgrfdat, &dvrtlocbas);

#ifdef PAMPA_DEBUG_DGRAPH
  	char s[30];
  	sprintf(s, "dgrfdat-adapt-%d", srcmeshptr->proclocnum);
  	FILE *dgraph_file;
  	dgraph_file = fopen(s, "w");
  	SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
  	fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

  	// 	a bis) élargissement des zones avec un dgraphBand ok
  	if (bandval > 0) {
	  CHECK_FERR (SCOTCH_dgraphBand (&dgrfdat, rmshlocnbr, fronloctax + baseval, bandval, &bgrfdat), proccomm);

  	  SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bvlbloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
	  	if (rmshloctx2[bvlbloctax[bvrtlocnum] - dvrtlocbas] != PAMPA_TAG_VERT_REMESH) {
	  	  rmshloctx2[bvlbloctax[bvrtlocnum] - dvrtlocbas] = PAMPA_TAG_VERT_REMESH;
	  	  rmshlocnbr ++;
  	  	}

	  SCOTCH_dgraphFree (&bgrfdat);
  	}

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

	if (rmshglbnbr == 0)
	  break;


  	// 	b) sous-graphe induit des zones ok
	CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnbr, &igrfdat), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (&igrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	CHECK_FERR (SCOTCH_dgraphGhst (&igrfdat), proccomm);
  	//SCOTCH_dgraphData (&igrfdat, NULL, NULL, &ivrtlocnbr, NULL, &ivrtgstnbr, (SCOTCH_Num **) &ivrtloctax, (SCOTCH_Num **) &ivndloctax, NULL, (SCOTCH_Num **) NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &iedggsttax, NULL, NULL); 
  	SCOTCH_dgraphData (&igrfdat, NULL, NULL, &ivrtlocnbr, NULL, &ivrtgstnbr, (SCOTCH_Num **) &ivrtloctax, (SCOTCH_Num **) &ivndloctax, NULL, (SCOTCH_Num **) &ivlbloctax, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &iedggsttax, NULL, NULL); 
	ivlbloctax -= baseval;
	ivrtloctax -= baseval;
	ivndloctax -= baseval;
	iedggsttax -= baseval;

  	// 	c) contraction du graphe des zones ok
  	if (memAllocGroup ((void **) (void *)
		  &inumgsttax, (size_t) (ivrtgstnbr * sizeof (Gnum)),
		  //&ivlbloctax, (size_t) (ivrtlocnbr * sizeof (Gnum)), 
		  &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
          &cprcvrttab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (Gnum)),
          &dsndcnttab, (size_t) (srcmeshptr->procglbnbr       * sizeof (int)),
          &dsnddsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
          &drcvcnttab, (size_t) (srcmeshptr->procglbnbr       * sizeof (int)),
          &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
  	inumgsttax -= baseval;
  	vnumgsttax -= baseval;
	//ivlbloctax -= baseval;
  	memSet (dsndcnttab, 0, srcmeshptr->procglbnbr * sizeof (Gnum));
  	memSet (vnumgsttax + baseval, ~0, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

	if (rmshglbnbr > ballsiz) {
  	  CHECK_FERR (dmeshAdapt2 (srcmeshptr, &igrfdat, inumgsttax, &cgrfdat, cprcvrttab, coarval - 1, coarnbr, coarrat), srcmeshptr->proccomm);

	  CHECK_FERR (SCOTCH_dgraphHalo (&igrfdat, inumgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);

	  CHECK_FERR (SCOTCH_dgraphGhst (&cgrfdat), proccomm);
	  SCOTCH_dgraphData (&cgrfdat, NULL, &cvrtglbnbr, &cvrtlocnbr, NULL, &cvrtgstnbr, NULL, NULL, (SCOTCH_Num **) &cvelloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	  cvelloctax -= baseval;

  	if ((cvelgsttax = (Gnum *) memAlloc (cvrtgstnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	cvelgsttax -= baseval;
	memCpy (cvelgsttax + baseval, cvelloctax + baseval, cvrtlocnbr * sizeof (Gnum));
	CHECK_FERR (SCOTCH_dgraphHalo (&cgrfdat, cvelgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);
#ifdef PAMPA_DEBUG_ADAPT
	  if (srcmeshptr->proclocnum == 0) {
		printf ("-- Iteration number: %d\n", iternbr ++);
	  	printf ("Number of vertices to be remeshed : %d\n", rmshglbnbr);
	  	printf ("Number of colors : %d\n", cvrtglbnbr);
	  }
	  printf ("[%d] Number of local colors : %d\n", srcmeshptr->proclocnum, cvrtlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

	  cvrtlocmin = cprcvrttab[srcmeshptr->proclocnum];
	  cvrtlocmax = cprcvrttab[srcmeshptr->proclocnum + 1] - 1;
	  ivrtgstmax = ivrtlocnbr + baseval - 1;
  	  // 	d) partitionnement des gros sommets ko
  	  //                 * partitionnement de graphes
  	  //                         * les gros sommets sont indépendants donc absence de relations
  	  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

	  //for (ivrtlocnum = dvrtlocnum = baseval, dvrtlocnnd = dvrtlocnbr + baseval; dvrtlocnum < dvrtlocnnd; dvrtlocnum ++)
	  //  if (rmshloctx2[dvrtlocnum] == PAMPA_TAG_VERT_REMESH)
	  //	ivlbloctax[ivrtlocnum ++] = dvrtlocnum + dvrtlocbas;

  	  for (ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) 
		//if (cvelgsttax[inumgsttax[ivrtlocnum]] > (ballsiz / 10))
                //FIXME peut-on faire un test sur les tailles de boule, le
                //problème est que inumgsttax contient un sommet qui peut ne pas
                //être dans les ghst
	  	  vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = inumgsttax[ivrtlocnum]; /* Propagate colors on elements */

	  ///* First pass to remove elements which have neighbors with different color */
  	  //for (ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) {
	  //	Gnum cvrtlocnum;
	  //	Gnum ucollocval; /* If undo coloring */

	  //	cvrtlocnum = inumgsttax[ivrtlocnum];

	  //	for (ucollocval = 0, iedglocnum = ivrtloctax[ivrtlocnum], iedglocnnd = ivndloctax[ivrtlocnum]; iedglocnum < iedglocnnd; iedglocnum ++) /* Undo coloring on elements which are at the boundary */
	  //    if (inumgsttax[iedggsttax[iedglocnum]] != cvrtlocnum) { /* If vertex and neighbor have different colors */
	  //    	ucollocval = 1;
	  //    	if (iedggsttax[iedglocnum] <= ivrtgstmax) 
	  //  	  vnumgsttax[ivlbloctax[iedggsttax[iedglocnum]] - dvrtlocbas] = ~0;
	  //    }
	  //	if (ucollocval == 1) { /* There are at least one neighbor with different color */
	  //    vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = ~0;
	  //	  // FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	}
	  //}
	  //memFree (fronloctax + baseval);

	  //CHECK_FERR (dmeshHaloSync (srcmeshptr, srcmeshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);

	  //// FIXME temporarly disabled
	  ///* Second pass to remove elements which are isolated */
	  //for (vertlocnum = baseval, vertlocnnd = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
	  //	Gnum ucollocval; /* If undo coloring */
	  //	Gnum vnumlocval;
	  //	Gnum edgelocnnd;
	  //	const DmeshEntity * enttlocptr = srcmeshptr->enttloctax[baseval];

	  //	vnumlocval = vnumgsttax[vertlocnum];

	  //	if (vnumlocval != ~0) {
	  //    for (ucollocval = 0, edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
	  //  	  edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
	  //  	  ucollocval == 0 && edgelocnum < edgelocnnd; edgelocnum ++) {
	  //    	Gnum nghblocnum;

	  //    	nghblocnum = srcmeshptr->edgeloctax[edgelocnum];

	  //    	if (vnumgsttax[nghblocnum] == vnumlocval) /* If vertex and neighbor have same colors */
	  //    	  ucollocval = 1;
	  //    }
	  //	  if (ucollocval == 0) { /* If no neighbor with same color */
	  //    	vnumgsttax[vertlocnum] = ~0;
	  //	  	// FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	  }
	  //	}
	  //}

  	  for (procngbnum = srcmeshptr->proclocnum, ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) {
	  	Gnum cvrtlocnum;

	  	cvrtlocnum = inumgsttax[ivrtlocnum];

      	for (hashnum = (cvrtlocnum * DMESHADAPTHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum] != cvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

      	if (hashtab[hashnum] == cvrtlocnum) /* If vertex already added */
          continue;

	  	if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
    	  if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
		  	int procngbmax;
          	for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (cprcvrttab[procngbmed] <= cvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
		  }

		  dsndcnttab[procngbnum] ++;
	  	}

	  	hashtab[hashnum] = cvrtlocnum;
	  	hashnbr ++;

      	if (hashnbr >= hashmax) { /* If (*hashtab) is too much filled */
		  cheklocval = dmeshAdaptVertResize (srcmeshptr, &hashtab, &hashsiz, &hashmax, &hashmsk);
  		  CHECK_VERR (cheklocval, proccomm);
      	}
	  }
	  SCOTCH_dgraphFree (&igrfdat);

  	  /* Compute displacement sending array */
  	  dsnddsptab[0] = 0;
  	  for (procngbnum = 1, procngbnnd = srcmeshptr->procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) 
      	dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];

  	  datasndnbr = dsnddsptab[srcmeshptr->procglbnbr];

  	  //* envoyer dsndcnttab et recevoir drcvcnttab
  	  CHECK_VDBG (cheklocval, proccomm);
  	  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, proccomm) != MPI_SUCCESS) {
      	errorPrint ("communication error");
	  	cheklocval = 1;
  	  }
	  CHECK_VERR (cheklocval, proccomm);

  	  //* en déduire ddsp{snd,rcv}tab
  	  /* Compute displacement receiving array */
  	  drcvdsptab[0] = 0;
  	  for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
      	drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
      	dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  	  }
  	  drcvdsptab[srcmeshptr->procglbnbr] = drcvdsptab[srcmeshptr->procglbnbr - 1] + drcvcnttab[srcmeshptr->procglbnbr - 1];
  	  datarcvnbr = drcvdsptab[srcmeshptr->procglbnbr];
  	  dsndcnttab[0] = dsnddsptab[0];

	  vertlocnbr = cvrtlocnbr + 1; /* Adding processor as a vertex */
	  edgelocnbr = (hashnbr - hashbas) * 2 + datarcvnbr;
  	  //* allouer datasndtab et datarcvtab
  	  //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
  	  if (memAllocGroup ((void **) (void *)
          	&datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
          	&datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
          	&datarcvtb2, (size_t) (datasndnbr * sizeof (Gnum)),
		  	&vertloctax, (size_t) ((vertlocnbr + 1) * sizeof (Gnum)),
		  	&veloloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
		  	&edgeloctax, (size_t) (edgelocnbr * sizeof (Gnum)),
		  	&partloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
		  	&parttax, (size_t) ((cvrtglbnbr + srcmeshptr->procglbnbr) * sizeof (Gnum)),
		  	&vertsidtab, (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
          	NULL) == NULL) {
      	errorPrint ("Out of memory");
      	cheklocval = 1;
  	  }
  	  CHECK_VERR (cheklocval, proccomm);
	  vertloctax -= baseval;
	  veloloctax -= baseval;
	  edgeloctax -= baseval;
	  partloctax -= baseval;
	  parttax -= baseval;

	  for (hashnum = 0; hashnum < hashsiz; hashnum ++)
	  	if (hashtab[hashnum] != ~0) {
		  Gnum cvrtlocnum;

		  cvrtlocnum = hashtab[hashnum];

	  	  if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
		  	Gnum cvrtlocbas;
    	  	if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
			  int procngbmax;
          	  for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
              	  procngbmax - procngbnum > 1; ) {
          	  	int                 procngbmed;

          	  	procngbmed = (procngbmax + procngbnum) / 2;
          	  	if (cprcvrttab[procngbmed] <= cvrtlocnum)
              	  procngbnum = procngbmed;
          	  	else
              	  procngbmax = procngbmed;
          	  }
			  cvrtlocbas = cprcvrttab[procngbnum] + baseval;
		  	}
		  	datasndtab[dsndcnttab[procngbnum] ++] = cvrtlocnum - cvrtlocbas;
	  	  }
	  	}

  	  for (procngbnum = 0 ; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
      	dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
	  	intSort1asc1 (datasndtab + dsnddsptab[procngbnum], dsndcnttab[procngbnum]);
  	  }

  	  //* envoyer datasndtab et recevoir datarcvtab
  	  CHECK_VDBG (cheklocval, proccomm);
  	  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
      	errorPrint ("communication error");
	  	cheklocval = 1;
  	  }
	  CHECK_VERR (cheklocval, proccomm);

	  for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++)
	  	vertsidtab[procngbnum] = drcvdsptab[procngbnum];

	  /* Adding processor as a vertex and their neighbors */
	  vertlocnum = baseval;
	  veloloctax[vertlocnum] = 1;
	  vertloctax[vertlocnum ++] = baseval;
	  pvrtlocnum = cprcvrttab[srcmeshptr->proclocnum] + srcmeshptr->proclocnum; /* Vertex number of processor */
	  vertlocbas = srcmeshptr->proclocnum + 1 - baseval;
	  for (procngbnum = srcmeshptr->proclocnum, edgelocnum = baseval, hashnum = 0; hashnum < hashsiz; hashnum ++)
	  	if (hashtab[hashnum] != ~0) {
		  Gnum cvrtlocnum;
		  cvrtlocnum = hashtab[hashnum];

	  	  if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
		  	Gnum vertlocbs2;
    	  	if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
			  int procngbmax;
          	  for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
              	  procngbmax - procngbnum > 1; ) {
          	  	int                 procngbmed;

          	  	procngbmed = (procngbmax + procngbnum) / 2;
          	  	if (cprcvrttab[procngbmed] <= cvrtlocnum)
              	  procngbnum = procngbmed;
          	  	else
              	  procngbmax = procngbmed;
          	  }
		  	  vertlocbs2 = procngbnum + 1 - baseval;
		  	}
		  	edgeloctax[edgelocnum ++] = cvrtlocnum + vertlocbs2;
	  	  }
		  else
	  	  	edgeloctax[edgelocnum ++] = cvrtlocnum + vertlocbas;
	  	}
	  memFree (hashtab);

	  for (cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval; cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
	  	veloloctax[vertlocnum] = cvelloctax[cvrtlocnum];
	  	vertloctax[vertlocnum ++] = edgelocnum;
	  	edgeloctax[edgelocnum ++] = pvrtlocnum;

	  	for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++)
		  if ((procngbnum != srcmeshptr->proclocnum) && (vertsidtab[procngbnum] < drcvdsptab[procngbnum + 1])
			  && (datarcvtab[vertsidtab[procngbnum]] == cvrtlocnum)) {
		  	Gnum pvrtlocnm2;

		  	pvrtlocnm2 = cprcvrttab[procngbnum] + procngbnum;
		  	edgeloctax[edgelocnum ++] = pvrtlocnm2;
		  	vertsidtab[procngbnum] ++;
		  }
	  }
	  vertloctax[vertlocnum] = edgelocnum;
	  edgelocnbr = edgelocnum - baseval;
	  SCOTCH_dgraphFree (&cgrfdat);

	  CHECK_FERR (SCOTCH_dgraphBuild (&dbgfdat, baseval, vertlocnbr, vertlocnbr, vertloctax + baseval, NULL, veloloctax + baseval, NULL, edgelocnbr, edgelocnbr, edgeloctax + baseval, NULL, NULL), proccomm); // XXX doit-on mettre edloloctab ? Si oui, à quoi est-ce qu'il correspond ?

#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (SCOTCH_dgraphCheck (&dbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
#ifdef PAMPA_DEBUG_DGRAPH
  	  sprintf(s, "dbgf-adapt-%d", srcmeshptr->proclocnum);
  	  dgraph_file = fopen(s, "w");
  	  SCOTCH_dgraphSave (&dbgfdat, dgraph_file);
  	  fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */
	  memSet (partloctax + baseval, ~0, vertlocnbr * sizeof (Gnum));
	  partloctax[baseval] = srcmeshptr->proclocnum;

	  if (srcmeshptr->proclocnum == 0) {
	  	CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, &cbgfdat), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
		CHECK_FERR (SCOTCH_graphCheck (&cbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */


	  	drcvdsptab[0] = 0;
	  	for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
	  	  drcvcnttab[procngbnum] = cprcvrttab[procngbnum + 1] - cprcvrttab[procngbnum] + 1;
	  	  drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
	  	}

  	  	if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
          	  parttax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);

	  	CHECK_FERR (SCOTCH_graphPartFixed (&cbgfdat, srcmeshptr->procglbnbr, &strtdat, parttax + baseval), proccomm);
	  	SCOTCH_graphFree (&cbgfdat);

	  	for (partlocidx = baseval, procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
		  Gnum partlocid2;
		  Gnum partlocnnd;
		  for (partlocid2 = drcvdsptab[procngbnum], partlocnnd = drcvdsptab[procngbnum + 1];
			  partlocid2 < partlocnnd; partlocid2 ++, partlocidx ++)
		  	parttax[partlocidx] = parttax[partlocid2];
	  	}

  	  	if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);

  	  	//if (commScatterv (parttax + baseval, prcvcnttab, prcvdsptab, GNUM_MPI,
      	//    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	//	errorPrint ("communication error");
      	//	return     (1);
  	  	//}
	  }
	  else {
	  	CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, NULL), proccomm); /* For graphPartFixed */

  	  	if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
          	  NULL, NULL, NULL, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);

  	  	if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);
  	  	//if (commScatterv (NULL, NULL, NULL, GNUM_MPI,
      	//    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	//	errorPrint ("communication error");
      	//	return     (1);
  	  	//}
	  }
	  CHECK_VERR (cheklocval, proccomm);
	}
	else {
#ifdef PAMPA_DEBUG_ADAPT
	  if (srcmeshptr->proclocnum == 0) {
		printf ("-- Iteration number: %d\n", iternbr ++);
	  	printf ("Number of vertices to be remeshed : %d\n", rmshglbnbr);
	  	printf ("Last iteration\n");
	  }
#endif /* PAMPA_DEBUG_ADAPT */
	  datasndtab = NULL;
	  memSet (inumgsttax + baseval, 0, ivrtgstnbr * sizeof (Gnum));
  	  for (ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) 
	  	vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = inumgsttax[ivrtlocnum]; /* Propagate colors on elements */
  	  if ((parttax = (Gnum *) memAlloc (1 * sizeof (Gnum))) == NULL) {
      	errorPrint  ("out of memory");
      	cheklocval = 1;
  	  }
  	  CHECK_VERR (cheklocval, proccomm);
	  parttax -= baseval;
	  parttax[baseval] = 0; // FIXME pourquoi 0 ???
	  cvrtglbnbr = 1;
	}


  	// 2) Extraction des sous-maillages et envoie sur chaque proc ok
	meshlocnbr = -1;
	imshloctab = NULL;
  	CHECK_FERR (dmeshGatherInduceMultiple (srcmeshptr, 1, cvrtglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), proccomm);

#ifdef PAMPA_DEBUG_ADAPT
	printf ("[%d] Number of received meshes : %d\n", srcmeshptr->proclocnum, meshlocnbr);
	printf ("[%d] Number of local vertices in source distributed mesh : %d\n", srcmeshptr->proclocnum, srcmeshptr->vertlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

	memFree (inumgsttax + baseval);
	if (datasndtab != NULL)
	  memFree (datasndtab);
	else
	  memFree (parttax + baseval);

  	// 3) Remaillage des zones
  	// 	a) conversion pampa -> mmg ok
  	// 	b) remaillage ok
  	// 	c) conversion mmg -> pampa quasi ok
  	if ((omshloctab = (Mesh *) memAlloc (meshlocnbr * sizeof (Mesh))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);

	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) { // XXX cette partie pourrait être parallélisé avec pthread
#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (imshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshInit (omshloctab + meshlocnum);
	  CHECK_FERR (PAMPA_remesh ((PAMPA_Mesh *) (imshloctab + meshlocnum), (PAMPA_Mesh *) (omshloctab + meshlocnum), dataptr, 0), proccomm); // FIXME remplacer le 0 par un drapeau
	  omshloctab[meshlocnum].idnum = imshloctab[meshlocnum].idnum;
#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (omshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshExit (imshloctab + meshlocnum);
	}
	memFree (imshloctab);
	CHECK_VDBG (cheklocval, proccomm);


  	// 4) Reintegration des zones dans maillage distribué en cours
	CHECK_FERR (dmeshRebuild (srcmeshptr, meshlocnbr, omshloctab), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (dmeshCheck (srcmeshptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
#ifdef PAMPA_DEBUG_ADAPT
	printf ("[%d] Number of local vertices in destination distributed mesh : %d\n", srcmeshptr->proclocnum, srcmeshptr->vertlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE

        Mesh wmshdat;
        char s1[50];
        sprintf (s1, "dmesh-adapt-%d-%d", srcmeshptr->proclocnum, wmshcnt ++);
        if (srcmeshptr->proclocnum == 0) {
          meshInit (&wmshdat);
          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
          CHECK_FERR (PAMPA_MMG3D_meshSave(&wmshdat, 1, dataptr, NULL, s1), proccomm);
		  meshExit (&wmshdat);
        }
        else
  		  CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);

#endif /* PAMPA_DEBUG_DMESH_SAVE */
	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) 
	  meshFree (omshloctab + meshlocnum);

	memFree (omshloctab);

	CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
	SCOTCH_dgraphFree (&dbgfdat);
  }

  CHECK_FERR (dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_ANY, &dgrfdat, NULL), proccomm);
  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

  if (memAllocGroup ((void **) (void *)
		&paroloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
		&partloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  paroloctax -= baseval;
  partloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = dvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
   	paroloctax[vertlocnum] = srcmeshptr->proclocnum;

  //// 5) Repartitionnement
  //vmloloctab = NULL;
  //emraval = 1;
  //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "q{strat=m{asc=b{width=3,bnd=(d{pass=40,dif=1,rem=0}|)f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.01,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=0}}"), proccomm);
  //CHECK_FERR (SCOTCH_dgraphRepart (&dgrfdat, srcmeshptr->procglbnbr, paroloctax + baseval, emraval, vmloloctab, &strtdat, partloctax + baseval), proccomm);

  dmeshDgraphExit (&dgrfdat);
  SCOTCH_dgraphExit (&bgrfdat);
  SCOTCH_dgraphExit (&igrfdat);
  SCOTCH_dgraphExit (&cgrfdat);
  SCOTCH_dgraphExit (&dbgfdat);
  SCOTCH_stratExit (&strtdat);
  if (srcmeshptr->proclocnum == 0)
	SCOTCH_graphExit (&cbgfdat);
  // 5 bis) Redistribution ok
  //CHECK_FERR (dmeshRedist (srcmeshptr, partloctax, NULL, -1, -1, -1, dstmeshptr), proccomm); // XXX peut-être utiliser un dsmeshRedist qui serait moins gourmand en mémoire à mon avis
  memFree (paroloctax + baseval);
  CHECK_VDBG (cheklocval, proccomm);
  return (0);
} 

  int
dmeshAdaptGrow (
    Dmesh * const    srcmeshptr,              /**< mesh */
	const Gnum                bandval,              /**< Band value */
	const Gnum                ballsiz,
	int (*PAMPA_remesh) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
	void * const              dataptr,
	Dmesh * const    dstmeshptr)
{
  SCOTCH_Dgraph dgrfdat;
  SCOTCH_Dgraph bgrfdat; /* Band graph */
  SCOTCH_Dgraph igrfdat; /* Induced graph */
  SCOTCH_Strat  strtdat; 
  Gnum * restrict rmshloctax;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  Gnum baseval;
  Gnum vertlocmax;
  Gnum distval;
  Gnum seedlocnbr;
  Gnum ballglbnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum dvrtlocnbr;
#ifdef PAMPA_DEBUG_ADAPT
  Gnum iternbr;
#endif /* PAMPA_DEBUG_ADAPT */
  int cheklocval;
  Gnum * restrict seedloctab;
  Gnum * restrict partloctax;
  Gnum * restrict paroloctax;
  MPI_Comm proccomm;
#ifdef PAMPA_DEBUG_DMESH_SAVE
    Gnum wmshcnt = 0;
#endif /* PAMPA_DEBUG_DMESH_SAVE */

    printf("*******************\n\nGrow\n\n*******************\n");
  cheklocval = 0;

  distval = sqrt(ballsiz / 3);
  baseval = srcmeshptr->baseval;
  proccomm = srcmeshptr->proccomm;

  CHECK_FERR (SCOTCH_stratInit (&strtdat), proccomm);
  CHECK_FERR (dmeshDgraphInit (srcmeshptr, &dgrfdat), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&bgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&igrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_VERR (cheklocval, proccomm);

#ifdef PAMPA_DEBUG_ADAPT
  iternbr = 1;
#endif /* PAMPA_DEBUG_ADAPT */

  while (1) {
	Gnum dvrtlocbas;
	Gnum bvrtlocnbr;
	Gnum bvrtglbnbr;
	Gnum bvrtlocnum;
	Gnum bvrtlocnnd;
	Gnum ivrtlocnbr;
	Gnum ivrtlocmax;
	Gnum ivrtgstnbr;
	Gnum ivrtlocnum;
	Gnum ivrtlocnnd;
        Gnum iedglocnum;
        Gnum iedglocnnd;
	Gnum meshlocnbr;
	Gnum meshlocnum;
	Gnum fronlocidx;
	int procngbnum;
	Gnum * fronloctax;
	Gnum * restrict bvlbloctax;
	Gnum * restrict rmshloctx2;
	Gnum * restrict vnumgsttax;
	Gnum * restrict inumgsttax;
	Gnum * restrict ivlbloctax;
	Gnum * restrict ivrtloctax;
	Gnum * restrict ivndloctax;
	Gnum * restrict iedggsttax;
	Gnum * restrict parttax;
	Mesh * imshloctab;
	Mesh * omshloctab;

	CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
	rmshloctax -= baseval;

	vertlocmax = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;
	for (rmshlocnbr = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		rmshlocnbr ++;

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

	if (rmshglbnbr == 0)
	  break;
#ifdef PAMPA_DEBUG_ADAPT
        if (srcmeshptr->proclocnum == 0) {
          printf ("-- Iteration number: %d\n", iternbr);
          printf ("Number of vertices to be remeshed before dgraphBand: %d\n", rmshglbnbr);
        }
#endif /* PAMPA_DEBUG_ADAPT */

	
	// XXX ne devrait-on pas faire cette alloc une bonne fois pour toutes les
	// itérations ?
  	if (memAllocGroup ((void **) (void *)
		  &fronloctax, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
		  &rmshloctx2, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	fronloctax -= baseval;
  	rmshloctx2 -= baseval;

  	memCpy (rmshloctx2 + baseval, rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

	for (fronlocidx = vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		fronloctax[fronlocidx ++] = vertlocnum;

  	// 1) Identification des zones à remailler
  	// 	a) conversion du maillage en graphe uniquement pour les sommets qui ont un drapeau
  	dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_MAIN, &dgrfdat, &dvrtlocbas);

#ifdef PAMPA_DEBUG_DGRAPH
  	char s[30];
  	sprintf(s, "dgrfdat-adapt-%d", srcmeshptr->proclocnum);
  	FILE *dgraph_file;
  	dgraph_file = fopen(s, "w");
  	SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
  	fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

  	// 	a bis) élargissement des zones avec un dgraphBand ok
        if (bandval > 0) {
	  CHECK_FERR (SCOTCH_dgraphBand (&dgrfdat, rmshlocnbr, fronloctax + baseval, bandval, &bgrfdat), proccomm);

  	  SCOTCH_dgraphData (&bgrfdat, NULL, &bvrtglbnbr, &bvrtlocnbr, NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bvlbloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
	  	if (rmshloctx2[bvlbloctax[bvrtlocnum] - dvrtlocbas] != PAMPA_TAG_VERT_REMESH) {
	  	  rmshloctx2[bvlbloctax[bvrtlocnum] - dvrtlocbas] = PAMPA_TAG_VERT_REMESH;
	  	  rmshlocnbr ++;
  	  	}

	  SCOTCH_dgraphFree (&bgrfdat);
        }



  	// 	b) sous-graphe induit des zones ok
	CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnbr, &igrfdat), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (&igrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	CHECK_FERR (SCOTCH_dgraphGhst (&igrfdat), proccomm);
  	//SCOTCH_dgraphData (&igrfdat, NULL, NULL, &ivrtlocnbr, NULL, &ivrtgstnbr, (SCOTCH_Num **) &ivrtloctax, (SCOTCH_Num **) &ivndloctax, NULL, (SCOTCH_Num **) NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &iedggsttax, NULL, NULL); 
  	SCOTCH_dgraphData (&igrfdat, NULL, NULL, &ivrtlocnbr, NULL, &ivrtgstnbr, (SCOTCH_Num **) &ivrtloctax, (SCOTCH_Num **) &ivndloctax, NULL, (SCOTCH_Num **) &ivlbloctax, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &iedggsttax, NULL, NULL); 
	ivlbloctax -= baseval;
	ivrtloctax -= baseval;
	ivndloctax -= baseval;
	iedggsttax -= baseval;
        ivrtlocmax = ivrtlocnbr + baseval - 1;

  	// 	c) contraction du graphe des zones ok
        if (memAllocGroup ((void **) (void *)
              &inumgsttax, (size_t) (ivrtgstnbr * sizeof (Gnum)),
              //&ivlbloctax, (size_t) (ivrtlocnbr * sizeof (Gnum)), 
              &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
              &seedloctab, (size_t) (ivrtlocnbr * sizeof (Gnum)),
              &parttax,    (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
              NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
  	CHECK_VERR (cheklocval, proccomm);
  	inumgsttax -= baseval;
  	vnumgsttax -= baseval;
        parttax -= baseval;
	//ivlbloctax -= baseval;
  	memSet (vnumgsttax + baseval, ~0, srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum));
  	memSet (inumgsttax + baseval, ~0, ivrtgstnbr * sizeof (Gnum));

    if (rmshglbnbr > ballsiz) {
      //Gnum dvrtlocnum;
      //Gnum dvrtlocnnd;

      //for (seedlocnbr = 0, dvrtlocnum = baseval, dvrtlocnnd = dvrtlocnbr + baseval; dvrtlocnum < dvrtlocnnd; dvrtlocnum ++) 
      //  if (rmshloctax[dvrtlocnum] == PAMPA_TAG_VERT_REMESH) {
      //    vnumgsttax[dvrtlocnum] = srcmeshptr->proclocnum;
      //    seedloctab[seedlocnbr ++] = dvrtlocnum;
      //    break;
      //  }

      //CHECK_FERR (SCOTCH_dgraphGrow (&dgrfdat, seedlocnbr, seedloctab, distval, vnumgsttax + baseval), proccomm);
      for (seedlocnbr = 0, ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) 
        if (rmshloctax[ivrtlocnum] == PAMPA_TAG_VERT_REMESH) {
          inumgsttax[ivrtlocnum] = srcmeshptr->proclocnum;
          seedloctab[seedlocnbr ++] = ivrtlocnum;
          break;
        }

      CHECK_FERR (SCOTCH_dgraphGrow (&igrfdat, seedlocnbr, seedloctab, distval, inumgsttax + baseval), proccomm);

      for (ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) 
            if (inumgsttax[ivrtlocnum] != ~0)
              vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = inumgsttax[ivrtlocnum];
	  ///* First pass to remove elements which have neighbors with different color */
  	  //for (ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) {
	  //	Gnum colrlocnum;
	  //	Gnum ucollocval; /* If undo coloring */

	  //	colrlocnum = inumgsttax[ivrtlocnum];

	  //	for (ucollocval = 0, iedglocnum = ivrtloctax[ivrtlocnum], iedglocnnd = ivndloctax[ivrtlocnum]; iedglocnum < iedglocnnd; iedglocnum ++) /* Undo coloring on elements which are at the boundary */
	  //    if (inumgsttax[iedggsttax[iedglocnum]] != colrlocnum) { /* If vertex and neighbor have different colors */
	  //    	ucollocval = 1;
	  //    	if (iedggsttax[iedglocnum] <= ivrtlocmax) 
	  //  	  vnumgsttax[ivlbloctax[iedggsttax[iedglocnum]] - dvrtlocbas] = ~0;
	  //    }
	  //	if (ucollocval == 1) { /* There are at least one neighbor with different color */
	  //    vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = ~0;
	  //	  // FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	}
	  //}



	  SCOTCH_dgraphFree (&igrfdat);

          CHECK_VDBG (cheklocval, proccomm);
          if (commAllreduce (&seedlocnbr, &ballglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) 
            parttax[procngbnum + baseval] = procngbnum;
        }
	else {
#ifdef PAMPA_DEBUG_ADAPT
	  if (srcmeshptr->proclocnum == 0) {
		printf ("-- Iteration number: %d\n", iternbr ++);
	  	printf ("Number of vertices to be remeshed : %d\n", rmshglbnbr);
	  	printf ("Last iteration\n");
	  }
#endif /* PAMPA_DEBUG_ADAPT */
	  memSet (inumgsttax + baseval, 0, ivrtgstnbr * sizeof (Gnum));
  	  for (ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) 
	  	vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = 0; /* Propagate colors on elements */
	  parttax[baseval] = 0; // FIXME pourquoi 0 ???
	  ballglbnbr = 1;
	}
	memFree (fronloctax + baseval);

#ifdef PAMPA_DEBUG_DMESH_SAVE2
        Gnum * restrict drcvcnttab;
        Gnum * restrict drcvdsptab;
        Gnum * restrict vnumglbtax;
        Mesh wmshdat;
        char s1[50];

        if (memAllocGroup ((void **) (void *)
              &drcvcnttab, (size_t) (srcmeshptr->procglbnbr * sizeof (int)),
              &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
              NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
  	CHECK_VERR (cheklocval, proccomm);
        
        sprintf (s1, "dmesh-adapt-avant-%d", wmshcnt);
        CHECK_VDBG (cheklocval, proccomm);
        if (commGather (&srcmeshptr->enttloctax[baseval]->vertlocnbr, 1, GNUM_MPI,
              drcvcnttab, 1, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        if (srcmeshptr->proclocnum == 0) {
          drcvdsptab[0] = 0;
          for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr + 1; procngbnum ++) {
            drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
          }
          if ((vnumglbtax = (Gnum *) memAlloc (drcvdsptab[srcmeshptr->procglbnbr] * sizeof (Gnum))) == NULL) {
            errorPrint  ("out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);
          vnumglbtax -= baseval;
          
          CHECK_VDBG (cheklocval, proccomm);
          if (commGatherv (vnumgsttax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
                vnumglbtax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          meshInit (&wmshdat);
          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
          CHECK_FERR (PAMPA_MMG3D_meshSave((PAMPA_Mesh *) &wmshdat, 1, dataptr, vnumglbtax + baseval, s1), proccomm);
		  meshExit (&wmshdat);
        }
        else {
          CHECK_VDBG (cheklocval, proccomm);
          if (commGatherv (vnumgsttax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
                NULL, NULL, NULL, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);
        }
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

  	// 2) Extraction des sous-maillages et envoie sur chaque proc ok
	meshlocnbr = -1;
	imshloctab = NULL;
  	CHECK_FERR (dmeshGatherInduceMultiple (srcmeshptr, 1, ballglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), proccomm);

#ifdef PAMPA_DEBUG_ADAPT
	printf ("[%d] Number of received meshes : %d\n", srcmeshptr->proclocnum, meshlocnbr);
	printf ("[%d] Number of local vertices in source distributed mesh : %d\n", srcmeshptr->proclocnum, srcmeshptr->vertlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

	memFree (inumgsttax + baseval);

  	// 3) Remaillage des zones
  	// 	a) conversion pampa -> mmg ok
  	// 	b) remaillage ok
  	// 	c) conversion mmg -> pampa quasi ok
  	if ((omshloctab = (Mesh *) memAlloc (meshlocnbr * sizeof (Mesh))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);

	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) { // XXX cette partie pourrait être parallélisé avec pthread
#ifdef PAMPA_DEBUG_MESH_SAVE
      char s2[50];
      sprintf (s2, "imsh-adapt-%d.mesh", imshloctab[meshlocnum].idnum);
      CHECK_FERR (PAMPA_MMG3D_meshSave((PAMPA_Mesh *) (imshloctab + meshlocnum), 1, dataptr, NULL, s2), proccomm);
#endif /* PAMPA_DEBUG_MESH_SAVE */
#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (imshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshInit (omshloctab + meshlocnum);
	  CHECK_FERR (PAMPA_remesh ((PAMPA_Mesh *) (imshloctab + meshlocnum), (PAMPA_Mesh *) (omshloctab + meshlocnum), dataptr, 0), proccomm); // FIXME remplacer le 0 par un drapeau
	  omshloctab[meshlocnum].idnum = imshloctab[meshlocnum].idnum;
#ifdef PAMPA_DEBUG_MESH_SAVE
      sprintf (s2, "omsh-adapt-%d.mesh", omshloctab[meshlocnum].idnum);
      CHECK_FERR (PAMPA_MMG3D_meshSave((PAMPA_Mesh *) (omshloctab + meshlocnum), 1, dataptr, NULL, s2), proccomm);
#endif /* PAMPA_DEBUG_MESH_SAVE */
#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (omshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshExit (imshloctab + meshlocnum);
	}
	memFree (imshloctab);
	CHECK_VDBG (cheklocval, proccomm);


  	// 4) Reintegration des zones dans maillage distribué en cours
	CHECK_FERR (dmeshRebuild (srcmeshptr, meshlocnbr, omshloctab), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (dmeshCheck (srcmeshptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
#ifdef PAMPA_DEBUG_ADAPT
	printf ("[%d] Number of local vertices in destination distributed mesh : %d\n", srcmeshptr->proclocnum, srcmeshptr->vertlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE2
        sprintf (s1, "dmesh-adapt-apres-%d", wmshcnt ++);
        if (srcmeshptr->proclocnum == 0) {
          meshInit (&wmshdat);
          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
          CHECK_FERR (PAMPA_MMG3D_meshSave((Mesh *) &wmshdat, 1, dataptr, NULL, s1), proccomm);
		  meshExit (&wmshdat);
        }
        else
  		  CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);

#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) 
	  meshFree (omshloctab + meshlocnum);

	memFree (omshloctab);

#ifdef PAMPA_DEBUG_DMESH_SAVE
#ifndef PAMPA_DEBUG_DMESH_SAVE2
      Mesh wmshdat;
      char s1[50];
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
    sprintf (s1, "dmesh-adapt-%d-%d", srcmeshptr->proclocnum, wmshcnt ++);
    if (srcmeshptr->proclocnum == 0) {
      meshInit (&wmshdat);
      CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
      CHECK_FERR (PAMPA_MMG3D_meshSave(&wmshdat, 1, dataptr, NULL, s1), proccomm);
      meshExit (&wmshdat);
    }
    else
      CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);

#endif /* PAMPA_DEBUG_DMESH_SAVE */
	CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
  }

  CHECK_FERR (dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_ANY, &dgrfdat, NULL), proccomm);
  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

  if (memAllocGroup ((void **) (void *)
		&paroloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
		&partloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  paroloctax -= baseval;
  partloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = dvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
   	paroloctax[vertlocnum] = srcmeshptr->proclocnum;

  //// 5) Repartitionnement
  //vmloloctab = NULL;
  //emraval = 1;
  //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "q{strat=m{asc=b{width=3,bnd=(d{pass=40,dif=1,rem=0}|)f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.01,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=0}}"), proccomm);
  //CHECK_FERR (SCOTCH_dgraphRepart (&dgrfdat, srcmeshptr->procglbnbr, paroloctax + baseval, emraval, vmloloctab, &strtdat, partloctax + baseval), proccomm);

  dmeshDgraphExit (&dgrfdat);
  SCOTCH_dgraphExit (&bgrfdat);
  SCOTCH_dgraphExit (&igrfdat);
  SCOTCH_stratExit (&strtdat);
  // 5 bis) Redistribution ok
  //CHECK_FERR (dmeshRedist (srcmeshptr, partloctax, NULL, -1, -1, -1, dstmeshptr), proccomm); // XXX peut-être utiliser un dsmeshRedist qui serait moins gourmand en mémoire à mon avis
  memFree (paroloctax + baseval);
  CHECK_VDBG (cheklocval, proccomm);
  return (0);
} 

  int
dmeshAdaptCoarsenGrow (
    Dmesh * const    srcmeshptr,              /**< mesh */
	const Gnum                bandval,              /**< Band value */
	const Gnum                ballsiz,
	int (*PAMPA_remesh) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
	void * const              dataptr,
	Dmesh * const    dstmeshptr)
{
  SCOTCH_Dgraph dgrfdat;
  SCOTCH_Dgraph bgrfdat; /* Band graph */
  SCOTCH_Dgraph igrfdat; /* Induced graph */
  SCOTCH_Dgraph jgrfdat; /* Induced graph + band graph */
  SCOTCH_Dgraph cgrfdat; /* Connex compound graph */
  SCOTCH_Dgraph dbgfdat; /* Distributed bigraph */
  SCOTCH_Graph  cbgfdat; /* Centralized bigraph */
  SCOTCH_Strat  strtdat; 
  Gnum * restrict rmshloctax;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  Gnum rmshlocnb2;
  Gnum rmshglbnb2;
  Gnum baseval;
  Gnum distval;
  Gnum vertlocmax;
  Gnum coarval;
  Gnum coarnbr;
  double coarrat;
  Gnum ballsi2;
  Gnum ballmin;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum dvrtlocnbr;
  Gnum emraval;
#ifdef PAMPA_DEBUG_ADAPT
  Gnum iternbr;
#endif /* PAMPA_DEBUG_ADAPT */
  int cheklocval;
  Gnum * restrict partloctax;
  Gnum * restrict paroloctax;
  Gnum * restrict vmloloctab;
  MPI_Comm proccomm;
#ifdef PAMPA_DEBUG_DMESH_SAVE
    Gnum wmshcnt = 0;
#endif /* PAMPA_DEBUG_DMESH_SAVE */

    printf("*******************\n\nCoarsenGrow\n\n*******************\n");
    ballmin = 0;
    //ballmin = ballsiz / 10;
  cheklocval = 0;
  distval = sqrt(ballsiz / 3);
  coarnbr = 1;
  coarrat = 1;
  for (coarval = 1, ballsi2 = 1; ballsi2 < ballsiz; coarval ++, ballsi2 <<= 1);

  baseval = srcmeshptr->baseval;
  proccomm = srcmeshptr->proccomm;

  CHECK_FERR (SCOTCH_stratInit (&strtdat), proccomm);
  CHECK_FERR (dmeshDgraphInit (srcmeshptr, &dgrfdat), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&bgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&igrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&jgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&cgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&dbgfdat, proccomm), proccomm);
  if (srcmeshptr->proclocnum == 0)
	cheklocval = SCOTCH_graphInit (&cbgfdat);
  CHECK_VERR (cheklocval, proccomm);

#ifdef PAMPA_DEBUG_ADAPT
  iternbr = 1;
#endif /* PAMPA_DEBUG_ADAPT */

  while (1) {
    Gnum dvrtlocbas;
    Gnum dvrtlocnum;
    Gnum dvrtlocnnd;
    Gnum mvrtlocnum;
    Gnum bvrtlocnbr;
    Gnum bvrtlocnum;
    Gnum bvrtlocnnd;
    Gnum ivrtlocnbr;
    Gnum jvrtlocnbr;
    Gnum jvrtgstnbr;
    Gnum jvrtgstmax;
    Gnum iedglocnum;
    Gnum iedglocnnd;
    Gnum cvrtlocnbr;
    Gnum cvrtgstnbr;
    Gnum cvrtglbnbr;
    Gnum cvrtlocmin;
    Gnum cvrtlocmax;
    Gnum cvrtlocnum;
    Gnum cvrtlocnnd;
    Gnum ivrtlocnum;
    Gnum ivrtlocnnd;
    Gnum jvrtlocnum;
    Gnum jvrtlocnnd;
    Gnum datasndnbr;
    Gnum datarcvnbr;
    Gnum vertlocnbr;
    Gnum edgelocnbr;
    Gnum pvrtlocnum;
    Gnum vertlocbas;
    Gnum edgelocnum;
    Gnum partlocidx;
    Gnum meshlocnbr;
    Gnum meshlocnum;
    Gnum fronlocidx;
    int procngbnum;
    int procngbnnd;
    Gnum seedlocnbr;
    Gnum hashsiz;          /* Size of hash table    */
    Gnum hashnum;          /* Hash value            */
    Gnum hashmax;
    Gnum hashbas;
    Gnum hashnbr;
    Gnum hashmsk;
    Gnum * fronloctax;
    Gnum * restrict hashtab;
    Gnum * restrict seedloctab;
    Gnum * restrict bvlbloctax;
    Gnum * restrict rmshloctx2;
    Gnum * restrict vnumgsttax;
    Gnum * restrict inumloctax;
    Gnum * restrict jnumgsttax;
    Gnum * restrict ivlbloctax;
    Gnum * restrict jvlbloctax;
    Gnum * cprcvrttab;
    Gnum * restrict dsndcnttab;
    Gnum * restrict dsnddsptab;
    Gnum * restrict drcvcnttab;
    Gnum * restrict drcvdsptab;
    Gnum * restrict cvelloctax;
    Gnum * restrict datasndtab;
    Gnum * restrict datarcvtab;
    Gnum * restrict datarcvtb2;
    Gnum * restrict vertloctax;
    Gnum * restrict veloloctax;
    Gnum * restrict edgeloctax;
    Gnum * restrict parttax;
    Gnum * restrict vertsidtab;
    Mesh * imshloctab;
    Mesh * omshloctab;

    CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
    rmshloctax -= baseval;


#ifdef PAMPA_DEBUG_DMESH_SAVE2
      Gnum * restrict vnumglbtax;
      Mesh wmshdat;
      char s1[50];

      if (memAllocGroup ((void **) (void *)
            &drcvcnttab, (size_t) (srcmeshptr->procglbnbr * sizeof (int)),
            &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
            NULL) == NULL) {
        errorPrint ("Out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);

      sprintf (s1, "dmesh-adapt-avant0-%d", wmshcnt);
      CHECK_VDBG (cheklocval, proccomm);
      if (commGather (&srcmeshptr->enttloctax[baseval]->vertlocnbr, 1, GNUM_MPI,
            drcvcnttab, 1, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);

      if (srcmeshptr->proclocnum == 0) {
        drcvdsptab[0] = 0;
        for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr + 1; procngbnum ++) {
          drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
        }
        if ((vnumglbtax = (Gnum *) memAlloc (drcvdsptab[srcmeshptr->procglbnbr] * sizeof (Gnum))) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);
        vnumglbtax -= baseval;

        CHECK_VDBG (cheklocval, proccomm);
        if (commGatherv (rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              vnumglbtax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        meshInit (&wmshdat);
        CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
        CHECK_FERR (PAMPA_MMG3D_meshSave((PAMPA_Mesh *) &wmshdat, 1, dataptr, vnumglbtax + baseval, s1), proccomm);
        meshExit (&wmshdat);
      }
      else {
        CHECK_VDBG (cheklocval, proccomm);
        if (commGatherv (rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              NULL, NULL, NULL, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);
      }
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
    vertlocmax = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;
    for (rmshlocnbr = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
      if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
        rmshlocnbr ++;

#ifdef PAMPA_DEBUG_ADAPT
    if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);
    if (srcmeshptr->proclocnum == 0) {
      printf ("-- Iteration number: %d\n", iternbr);
      printf ("Number of vertices to be remeshed before dgraphBand: %d\n", rmshglbnbr);
    }
#endif /* PAMPA_DEBUG_ADAPT */

    hashbas = 
      hashnbr = vertlocmax / 10; // XXX N'est-ce pas trop grand pour hashnbr ?
    for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */

    hashmsk = hashsiz - 1;
    hashmax = hashsiz >> 2;

    if ((hashtab = (Gnum *) memAlloc (hashsiz * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);
    memSet (hashtab, ~0, hashsiz * sizeof (Gnum));

    // XXX ne devrait-on pas faire cette alloc une bonne fois pour toutes les
    // itérations ?
    if (memAllocGroup ((void **) (void *)
          &fronloctax, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
          &rmshloctx2, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);
    fronloctax -= baseval;
    rmshloctx2 -= baseval;

    memCpy (rmshloctx2 + baseval, rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

    for (fronlocidx = vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
      if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
        fronloctax[fronlocidx ++] = vertlocnum;

    // 1) Identification des zones à remailler
    // 	a) conversion du maillage en graphe uniquement pour les sommets qui ont un drapeau
    dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_MAIN, &dgrfdat, &dvrtlocbas);

#ifdef PAMPA_DEBUG_DGRAPH
    char s[30];
    sprintf(s, "dgrfdat-adapt-%d", srcmeshptr->proclocnum);
    FILE *dgraph_file;
    dgraph_file = fopen(s, "w");
    SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
    fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

    rmshlocnb2 = rmshlocnbr;

    // 	a bis) élargissement des zones avec un dgraphBand ok
    if (bandval > 0) {
      CHECK_FERR (SCOTCH_dgraphBand (&dgrfdat, rmshlocnbr, fronloctax + baseval, bandval, &bgrfdat), proccomm);

      SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bvlbloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


      for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
        if (rmshloctx2[bvlbloctax[bvrtlocnum] - dvrtlocbas] != PAMPA_TAG_VERT_REMESH) {
          rmshloctx2[bvlbloctax[bvrtlocnum] - dvrtlocbas] = PAMPA_TAG_VERT_REMESH;
          rmshlocnb2 ++;
        }

      SCOTCH_dgraphFree (&bgrfdat);
    }

    CHECK_VDBG (cheklocval, proccomm);
    if (commAllreduce (&rmshlocnb2, &rmshglbnb2, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    if (rmshglbnb2 == 0) {
      CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
      break;
    }


    // 	b) sous-graphe induit des zones ok
    CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctax + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnbr, &igrfdat), proccomm);
#ifdef PAMPA_DEBUG_ADAPT
    CHECK_FERR (SCOTCH_dgraphCheck (&igrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT */
    CHECK_FERR (SCOTCH_dgraphGhst (&igrfdat), proccomm);
    //SCOTCH_dgraphData (&igrfdat, NULL, NULL, &ivrtlocnbr, NULL, &ivrtgstnbr, (SCOTCH_Num **) &ivrtloctax, (SCOTCH_Num **) &jvndloctax, NULL, (SCOTCH_Num **) NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &iedggsttax, NULL, NULL); 
    SCOTCH_dgraphData (&igrfdat, NULL, NULL, &ivrtlocnbr, NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &ivlbloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
    ivlbloctax -= baseval;

    // 	c) contraction du graphe des zones ok
    if (memAllocGroup ((void **) (void *)
          &inumloctax, (size_t) (ivrtlocnbr * sizeof (Gnum)),
          //&ivlbloctax, (size_t) (ivrtlocnbr * sizeof (Gnum)), 
          &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
          &cprcvrttab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (Gnum)),
          &dsndcnttab, (size_t) (srcmeshptr->procglbnbr       * sizeof (int)),
          &dsnddsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
          &drcvcnttab, (size_t) (srcmeshptr->procglbnbr       * sizeof (int)),
          &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);
    inumloctax -= baseval;
    vnumgsttax -= baseval;
    //ivlbloctax -= baseval;
    memSet (dsndcnttab, 0, srcmeshptr->procglbnbr * sizeof (Gnum));
    memSet (vnumgsttax + baseval, ~0, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
    memSet (inumloctax + baseval, ~0, ivrtlocnbr * sizeof (Gnum));

    if (rmshglbnb2 > ballsiz) {
      CHECK_FERR (dmeshAdapt2 (srcmeshptr, &igrfdat, inumloctax, &cgrfdat, cprcvrttab, coarval - 1, coarnbr, coarrat), srcmeshptr->proccomm);
      cvrtlocmin = cprcvrttab[srcmeshptr->proclocnum];
      cvrtlocmax = cprcvrttab[srcmeshptr->proclocnum + 1] - 1;

      CHECK_FERR (SCOTCH_dgraphGhst (&cgrfdat), proccomm);
      SCOTCH_dgraphData (&cgrfdat, NULL, &cvrtglbnbr, &cvrtlocnbr, NULL, &cvrtgstnbr, NULL, NULL, (SCOTCH_Num **) &cvelloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      cvelloctax -= baseval;

      CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnb2, &jgrfdat), proccomm);
      //SCOTCH_dgraphData (&jgrfdat, NULL, NULL, &jvrtlocnbr, NULL, &jvrtgstnbr, (SCOTCH_Num **) &jvrtloctax, (SCOTCH_Num **) &jvndloctax, NULL, (SCOTCH_Num **) &jvlbloctax, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &jedggsttax, NULL, NULL); 
      CHECK_FERR (SCOTCH_dgraphGhst (&jgrfdat), proccomm);
      SCOTCH_dgraphData (&jgrfdat, NULL, NULL, &jvrtlocnbr, NULL, &jvrtgstnbr, NULL, NULL, NULL, (SCOTCH_Num **) &jvlbloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 

      if (memAllocGroup ((void **) (void *)
            &jnumgsttax, (size_t) (jvrtgstnbr * sizeof (Gnum)),
            &seedloctab, (size_t) (jvrtlocnbr * sizeof (Gnum)),
            NULL) == NULL) {
        errorPrint ("Out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);
      jnumgsttax -= baseval;

      memSet (seedloctab, ~0, jvrtlocnbr * sizeof (Gnum));
      memSet (jnumgsttax + baseval, ~0, jvrtgstnbr * sizeof (Gnum));

      for (seedlocnbr = 0, ivrtlocnum = baseval, ivrtlocnnd = ivrtlocnbr + baseval; ivrtlocnum < ivrtlocnnd; ivrtlocnum ++) {
        cvrtlocnum = inumloctax[ivrtlocnum];
#ifdef PAMPA_DEBUG_DMESH_SAVE2
        if (cvrtlocnum != -1)
          vnumgsttax[ivlbloctax[ivrtlocnum] - dvrtlocbas] = inumloctax[ivrtlocnum];
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

        /* Find global vertex number in jgrfdat */
        if ((cvrtlocnum != -1) && (cvrtlocnum >= cvrtlocmin) && (cvrtlocnum <= cvrtlocmax) && (cvelloctax[cvrtlocnum - cvrtlocmin] >= ballmin) && (seedloctab[cvrtlocnum - cvrtlocmin] == -1)) {
          Gnum jvrtlocmax;
          mvrtlocnum = ivlbloctax[ivrtlocnum];

          for (jvrtlocnum = baseval, jvrtlocmax = jvrtlocnbr + baseval;
              jvrtlocmax - jvrtlocnum > 1; ) {
            int                 jvrtlocmed;

            jvrtlocmed = (jvrtlocmax + jvrtlocnum) / 2;
            if (jvlbloctax[jvrtlocmed] <= mvrtlocnum)
              jvrtlocnum = jvrtlocmed;
            else
              jvrtlocmax = jvrtlocmed;
          }
#ifdef PAMPA_DEBUG_ADAPT2
          if (jvlbloctax[jvrtlocnum] != mvrtlocnum) {
            errorPrint ("Vertex %d not found", mvrtlocnum);
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
          seedloctab[cvrtlocnum - cvrtlocmin] = jvrtlocnum;
          jnumgsttax[jvrtlocnum] = cvrtlocnum;
          seedlocnbr ++;
        }
      }

      intSort1desc1 (seedloctab, cvrtlocnbr);

#ifdef PAMPA_DEBUG_DMESH_SAVE2
      Gnum * restrict drcvcnttab;
      Gnum * restrict drcvdsptab;
      Gnum * restrict vnumglbtax;
      Mesh wmshdat;
      char s1[50];

      if (memAllocGroup ((void **) (void *)
            &drcvcnttab, (size_t) (srcmeshptr->procglbnbr * sizeof (int)),
            &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
            NULL) == NULL) {
        errorPrint ("Out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);

      sprintf (s1, "dmesh-adapt-avant1-%d", wmshcnt);
      CHECK_VDBG (cheklocval, proccomm);
      if (commGather (&srcmeshptr->enttloctax[baseval]->vertlocnbr, 1, GNUM_MPI,
            drcvcnttab, 1, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);

      if (srcmeshptr->proclocnum == 0) {
        drcvdsptab[0] = 0;
        for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr + 1; procngbnum ++) {
          drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
        }
        if ((vnumglbtax = (Gnum *) memAlloc (drcvdsptab[srcmeshptr->procglbnbr] * sizeof (Gnum))) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);
        vnumglbtax -= baseval;

        CHECK_VDBG (cheklocval, proccomm);
        if (commGatherv (vnumgsttax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              vnumglbtax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        meshInit (&wmshdat);
        CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
        CHECK_FERR (PAMPA_MMG3D_meshSave((PAMPA_Mesh *) &wmshdat, 1, dataptr, vnumglbtax + baseval, s1), proccomm);
        meshExit (&wmshdat);
      }
      else {
        CHECK_VDBG (cheklocval, proccomm);
        if (commGatherv (vnumgsttax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              NULL, NULL, NULL, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);
      }
      memSet (vnumgsttax + baseval, ~0, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

      SCOTCH_dgraphFree (&igrfdat);
      CHECK_FERR (SCOTCH_dgraphGrow (&jgrfdat, seedlocnbr, seedloctab, distval, jnumgsttax + baseval), proccomm);



      //if ((cvelgsttax = (Gnum *) memAlloc (cvrtgstnbr * sizeof (Gnum))) == NULL) {
      //  errorPrint  ("out of memory");
      //  cheklocval = 1;
      //}
      //CHECK_VERR (cheklocval, proccomm);
      //cvelgsttax -= baseval;
      //memCpy (cvelgsttax + baseval, cvelloctax + baseval, cvrtlocnbr * sizeof (Gnum));
      //CHECK_FERR (SCOTCH_dgraphHalo (&cgrfdat, cvelgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);
#ifdef PAMPA_DEBUG_ADAPT
      if (srcmeshptr->proclocnum == 0) {
        printf ("-- Iteration number: %d\n", iternbr ++);
        printf ("Number of vertices to be remeshed : %d\n", rmshglbnbr);
        printf ("Number of colors : %d\n", cvrtglbnbr);
      }
      printf ("[%d] Number of local colors : %d\n", srcmeshptr->proclocnum, cvrtlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

      jvrtgstmax = jvrtlocnbr + baseval - 1;
      // 	d) partitionnement des gros sommets ko
      //                 * partitionnement de graphes
      //                         * les gros sommets sont indépendants donc absence de relations
      SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

      //for (ivrtlocnum = dvrtlocnum = baseval, dvrtlocnnd = dvrtlocnbr + baseval; dvrtlocnum < dvrtlocnnd; dvrtlocnum ++)
      //  if (rmshloctx2[dvrtlocnum] == PAMPA_TAG_VERT_REMESH)
      //	jvlbloctax[ivrtlocnum ++] = dvrtlocnum + dvrtlocbas;

      for (jvrtlocnum = baseval, jvrtlocnnd = jvrtlocnbr + baseval; jvrtlocnum < jvrtlocnnd; jvrtlocnum ++) 
        //if (cvelgsttax[jnumgsttax[jvrtlocnum]] > (ballsiz / 10))
        //FIXME peut-on faire un test sur les tailles de boule, le
        //problème est que jnumgsttax contient un sommet qui peut ne pas
        //être dans les ghst
        vnumgsttax[jvlbloctax[jvrtlocnum] - dvrtlocbas] = jnumgsttax[jvrtlocnum]; /* Propagate colors on elements */

      ///* First pass to remove elements which have neighbors with different color */
      //for (jvrtlocnum = baseval, jvrtlocnnd = jvrtlocnbr + baseval; jvrtlocnum < jvrtlocnnd; jvrtlocnum ++) {
      //	Gnum cvrtlocnum;
      //	Gnum ucollocval; /* If undo coloring */

      //	cvrtlocnum = jnumgsttax[jvrtlocnum];

      //	for (ucollocval = 0, iedglocnum = jvrtloctax[jvrtlocnum], iedglocnnd = jvndloctax[jvrtlocnum]; iedglocnum < iedglocnnd; iedglocnum ++) /* Undo coloring on elements which are at the boundary */
      //    if (jnumgsttax[iedggsttax[iedglocnum]] != cvrtlocnum) { /* If vertex and neighbor have different colors */
      //    	ucollocval = 1;
      //    	if (iedggsttax[iedglocnum] <= jvrtgstmax) 
      //  	  vnumgsttax[jvlbloctax[iedggsttax[iedglocnum]] - dvrtlocbas] = ~0;
      //    }
      //	if (ucollocval == 1) { /* There are at least one neighbor with different color */
      //    vnumgsttax[jvlbloctax[jvrtlocnum] - dvrtlocbas] = ~0;
      //	  // FIXME il faudrait diminuer le cvelloctax pour cette couleur
      //	}
      //}
      //memFree (fronloctax + baseval);

      //CHECK_FERR (dmeshHaloSync (srcmeshptr, srcmeshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);

      //// FIXME temporarly disabled
      ///* Second pass to remove elements which are isolated */
      //for (vertlocnum = baseval, vertlocnnd = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
      //	Gnum ucollocval; /* If undo coloring */
      //	Gnum vnumlocval;
      //	Gnum edgelocnnd;
      //	const DmeshEntity * enttlocptr = srcmeshptr->enttloctax[baseval];

      //	vnumlocval = vnumgsttax[vertlocnum];

      //	if (vnumlocval != ~0) {
      //    for (ucollocval = 0, edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
      //  	  edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
      //  	  ucollocval == 0 && edgelocnum < edgelocnnd; edgelocnum ++) {
      //    	Gnum nghblocnum;

      //    	nghblocnum = srcmeshptr->edgeloctax[edgelocnum];

      //    	if (vnumgsttax[nghblocnum] == vnumlocval) /* If vertex and neighbor have same colors */
      //    	  ucollocval = 1;
      //    }
      //	  if (ucollocval == 0) { /* If no neighbor with same color */
      //    	vnumgsttax[vertlocnum] = ~0;
      //	  	// FIXME il faudrait diminuer le cvelloctax pour cette couleur
      //	  }
      //	}
      //}

      for (procngbnum = srcmeshptr->proclocnum, jvrtlocnum = baseval, jvrtlocnnd = jvrtlocnbr + baseval; jvrtlocnum < jvrtlocnnd; jvrtlocnum ++) {
        Gnum cvrtlocnum;

        cvrtlocnum = jnumgsttax[jvrtlocnum];

        for (hashnum = (cvrtlocnum * DMESHADAPTHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum] != cvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

        if (hashtab[hashnum] == cvrtlocnum) /* If vertex already added */
          continue;

        if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
          if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
            int procngbmax;
            for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
                procngbmax - procngbnum > 1; ) {
              int                 procngbmed;

              procngbmed = (procngbmax + procngbnum) / 2;
              if (cprcvrttab[procngbmed] <= cvrtlocnum)
                procngbnum = procngbmed;
              else
                procngbmax = procngbmed;
            }
          }

          dsndcnttab[procngbnum] ++;
        }

        hashtab[hashnum] = cvrtlocnum;
        hashnbr ++;

        if (hashnbr >= hashmax) { /* If (*hashtab) is too much filled */
          cheklocval = dmeshAdaptVertResize (srcmeshptr, &hashtab, &hashsiz, &hashmax, &hashmsk);
          CHECK_VERR (cheklocval, proccomm);
        }
      }
      SCOTCH_dgraphFree (&jgrfdat);

      /* Compute displacement sending array */
      dsnddsptab[0] = 0;
      for (procngbnum = 1, procngbnnd = srcmeshptr->procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) 
        dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];

      datasndnbr = dsnddsptab[srcmeshptr->procglbnbr];

      //* envoyer dsndcnttab et recevoir drcvcnttab
      CHECK_VDBG (cheklocval, proccomm);
      if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);

      //* en déduire ddsp{snd,rcv}tab
      /* Compute displacement receiving array */
      drcvdsptab[0] = 0;
      for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
        drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
        dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
      }
      drcvdsptab[srcmeshptr->procglbnbr] = drcvdsptab[srcmeshptr->procglbnbr - 1] + drcvcnttab[srcmeshptr->procglbnbr - 1];
      datarcvnbr = drcvdsptab[srcmeshptr->procglbnbr];
      dsndcnttab[0] = dsnddsptab[0];

      vertlocnbr = cvrtlocnbr + 1; /* Adding processor as a vertex */
      edgelocnbr = (hashnbr - hashbas) * 2 + datarcvnbr;
      //* allouer datasndtab et datarcvtab
      //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
      if (memAllocGroup ((void **) (void *)
            &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
            &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
            &datarcvtb2, (size_t) (datasndnbr * sizeof (Gnum)),
            &vertloctax, (size_t) ((vertlocnbr + 1) * sizeof (Gnum)),
            &veloloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
            &edgeloctax, (size_t) (edgelocnbr * sizeof (Gnum)),
            &partloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
            &parttax, (size_t) ((cvrtglbnbr + srcmeshptr->procglbnbr) * sizeof (Gnum)),
            &vertsidtab, (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
            NULL) == NULL) {
        errorPrint ("Out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);
      vertloctax -= baseval;
      veloloctax -= baseval;
      edgeloctax -= baseval;
      partloctax -= baseval;
      parttax -= baseval;

      for (hashnum = 0; hashnum < hashsiz; hashnum ++)
        if (hashtab[hashnum] != ~0) {
          Gnum cvrtlocnum;

          cvrtlocnum = hashtab[hashnum];

          if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
            Gnum cvrtlocbas;
            if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
              int procngbmax;
              for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
                  procngbmax - procngbnum > 1; ) {
                int                 procngbmed;

                procngbmed = (procngbmax + procngbnum) / 2;
                if (cprcvrttab[procngbmed] <= cvrtlocnum)
                  procngbnum = procngbmed;
                else
                  procngbmax = procngbmed;
              }
              cvrtlocbas = cprcvrttab[procngbnum] + baseval;
            }
            datasndtab[dsndcnttab[procngbnum] ++] = cvrtlocnum - cvrtlocbas;
          }
        }

      for (procngbnum = 0 ; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
        dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
        intSort1asc1 (datasndtab + dsnddsptab[procngbnum], dsndcnttab[procngbnum]);
      }

      //* envoyer datasndtab et recevoir datarcvtab
      CHECK_VDBG (cheklocval, proccomm);
      if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);

      for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++)
        vertsidtab[procngbnum] = drcvdsptab[procngbnum];

      /* Adding processor as a vertex and their neighbors */
      vertlocnum = baseval;
      veloloctax[vertlocnum] = 1;
      vertloctax[vertlocnum ++] = baseval;
      pvrtlocnum = cprcvrttab[srcmeshptr->proclocnum] + srcmeshptr->proclocnum; /* Vertex number of processor */
      vertlocbas = srcmeshptr->proclocnum + 1 - baseval;
      for (procngbnum = srcmeshptr->proclocnum, edgelocnum = baseval, hashnum = 0; hashnum < hashsiz; hashnum ++)
        if (hashtab[hashnum] != ~0) {
          Gnum cvrtlocnum;
          cvrtlocnum = hashtab[hashnum];

          if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
            Gnum vertlocbs2;
            if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
              int procngbmax;
              for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
                  procngbmax - procngbnum > 1; ) {
                int                 procngbmed;

                procngbmed = (procngbmax + procngbnum) / 2;
                if (cprcvrttab[procngbmed] <= cvrtlocnum)
                  procngbnum = procngbmed;
                else
                  procngbmax = procngbmed;
              }
              vertlocbs2 = procngbnum + 1 - baseval;
            }
            edgeloctax[edgelocnum ++] = cvrtlocnum + vertlocbs2;
          }
          else
            edgeloctax[edgelocnum ++] = cvrtlocnum + vertlocbas;
        }
      memFree (hashtab);

      for (cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval; cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
        veloloctax[vertlocnum] = cvelloctax[cvrtlocnum];
        vertloctax[vertlocnum ++] = edgelocnum;

        if (cvelloctax[cvrtlocnum] < ballmin) {
          veloloctax[vertlocnum - 1] = 0;
          continue;
        }

        edgeloctax[edgelocnum ++] = pvrtlocnum;

        for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++)
          if ((procngbnum != srcmeshptr->proclocnum) && (vertsidtab[procngbnum] < drcvdsptab[procngbnum + 1])
              && (datarcvtab[vertsidtab[procngbnum]] == cvrtlocnum)) {
            Gnum pvrtlocnm2;

            pvrtlocnm2 = cprcvrttab[procngbnum] + procngbnum;
            edgeloctax[edgelocnum ++] = pvrtlocnm2;
            vertsidtab[procngbnum] ++;
          }
      }
      vertloctax[vertlocnum] = edgelocnum;
      edgelocnbr = edgelocnum - baseval;
      SCOTCH_dgraphFree (&cgrfdat);

      CHECK_FERR (SCOTCH_dgraphBuild (&dbgfdat, baseval, vertlocnbr, vertlocnbr, vertloctax + baseval, NULL, veloloctax + baseval, NULL, edgelocnbr, edgelocnbr, edgeloctax + baseval, NULL, NULL), proccomm); // XXX doit-on mettre edloloctab ? Si oui, à quoi est-ce qu'il correspond ?

#ifdef PAMPA_DEBUG_ADAPT
      CHECK_FERR (SCOTCH_dgraphCheck (&dbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT */
#ifdef PAMPA_DEBUG_DGRAPH
      sprintf(s, "dbgf-adapt-%d", srcmeshptr->proclocnum);
      dgraph_file = fopen(s, "w");
      SCOTCH_dgraphSave (&dbgfdat, dgraph_file);
      fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */
      memSet (partloctax + baseval, ~0, vertlocnbr * sizeof (Gnum));
      partloctax[baseval] = srcmeshptr->proclocnum;

      if (srcmeshptr->proclocnum == 0) {
        CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, &cbgfdat), proccomm);
#ifdef PAMPA_DEBUG_ADAPT
        CHECK_FERR (SCOTCH_graphCheck (&cbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT */


        drcvdsptab[0] = 0;
        for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
          drcvcnttab[procngbnum] = cprcvrttab[procngbnum + 1] - cprcvrttab[procngbnum] + 1;
          drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
        }

        if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
              parttax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        CHECK_FERR (SCOTCH_graphPartFixed (&cbgfdat, srcmeshptr->procglbnbr, &strtdat, parttax + baseval), proccomm);
        SCOTCH_graphFree (&cbgfdat);

        for (partlocidx = baseval, procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
          Gnum partlocid2;
          Gnum partlocnnd;
          for (partlocid2 = drcvdsptab[procngbnum], partlocnnd = drcvdsptab[procngbnum + 1];
              partlocid2 < partlocnnd; partlocid2 ++, partlocidx ++)
            parttax[partlocidx] = parttax[partlocid2];
        }

        if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        //if (commScatterv (parttax + baseval, prcvcnttab, prcvdsptab, GNUM_MPI,
        //    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
        //	errorPrint ("communication error");
        //	return     (1);
        //}
      }
      else {
        CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, NULL), proccomm); /* For graphPartFixed */

        if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
              NULL, NULL, NULL, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);
        //if (commScatterv (NULL, NULL, NULL, GNUM_MPI,
        //    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
        //	errorPrint ("communication error");
        //	return     (1);
        //}
      }
      CHECK_VERR (cheklocval, proccomm);
    }
    else {
#ifdef PAMPA_DEBUG_ADAPT
      if (srcmeshptr->proclocnum == 0) {
        printf ("-- Iteration number: %d\n", iternbr ++);
        printf ("Number of vertices to be remeshed : %d\n", rmshglbnbr);
        printf ("Last iteration\n");
      }
#endif /* PAMPA_DEBUG_ADAPT */
      SCOTCH_dgraphFree (&igrfdat);
      CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnb2, &jgrfdat), proccomm);
      CHECK_FERR (SCOTCH_dgraphGhst (&jgrfdat), proccomm);
      SCOTCH_dgraphData (&jgrfdat, NULL, NULL, &jvrtlocnbr, NULL, &jvrtgstnbr, NULL, NULL, NULL, (SCOTCH_Num **) &jvlbloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
      datasndtab = NULL;
      if ((jnumgsttax = (Gnum *) memAlloc (jvrtgstnbr * sizeof (Gnum))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);
      memSet (jnumgsttax + baseval, 0, jvrtgstnbr * sizeof (Gnum));
      for (jvrtlocnum = baseval, jvrtlocnnd = jvrtlocnbr + baseval; jvrtlocnum < jvrtlocnnd; jvrtlocnum ++) 
        vnumgsttax[jvlbloctax[jvrtlocnum] - dvrtlocbas] = jnumgsttax[jvrtlocnum]; /* Propagate colors on elements */
      if ((parttax = (Gnum *) memAlloc (1 * sizeof (Gnum))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, proccomm);
      parttax -= baseval;
      parttax[baseval] = 0; // FIXME pourquoi 0 ???
      cvrtglbnbr = 1;
    }

#ifdef PAMPA_DEBUG_DMESH_SAVE2

        if (memAllocGroup ((void **) (void *)
              &drcvcnttab, (size_t) (srcmeshptr->procglbnbr * sizeof (int)),
              &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
              NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
  	CHECK_VERR (cheklocval, proccomm);
        
        sprintf (s1, "dmesh-adapt-avant2-%d", wmshcnt);
        CHECK_VDBG (cheklocval, proccomm);
        if (commGather (&srcmeshptr->enttloctax[baseval]->vertlocnbr, 1, GNUM_MPI,
              drcvcnttab, 1, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        if (srcmeshptr->proclocnum == 0) {
          drcvdsptab[0] = 0;
          for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr + 1; procngbnum ++) {
            drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
          }
          if ((vnumglbtax = (Gnum *) memAlloc (drcvdsptab[srcmeshptr->procglbnbr] * sizeof (Gnum))) == NULL) {
            errorPrint  ("out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);
          vnumglbtax -= baseval;
          
          CHECK_VDBG (cheklocval, proccomm);
          if (commGatherv (vnumgsttax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
                vnumglbtax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          meshInit (&wmshdat);
          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
          CHECK_FERR (PAMPA_MMG3D_meshSave((PAMPA_Mesh *) &wmshdat, 1, dataptr, vnumglbtax + baseval, s1), proccomm);
		  meshExit (&wmshdat);
        }
        else {
          CHECK_VDBG (cheklocval, proccomm);
          if (commGatherv (vnumgsttax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
                NULL, NULL, NULL, GNUM_MPI, 0, srcmeshptr->proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);
        }
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

    // 2) Extraction des sous-maillages et envoie sur chaque proc ok
    meshlocnbr = -1;
    imshloctab = NULL;
    CHECK_FERR (dmeshGatherInduceMultiple (srcmeshptr, 1, cvrtglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), proccomm);

#ifdef PAMPA_DEBUG_ADAPT
    printf ("[%d] Number of received meshes : %d\n", srcmeshptr->proclocnum, meshlocnbr);
    printf ("[%d] Number of local vertices in source distributed mesh : %d\n", srcmeshptr->proclocnum, srcmeshptr->vertlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

    memFree (inumloctax + baseval);
    memFree (jnumgsttax + baseval);
    if (datasndtab != NULL)
      memFree (datasndtab);
    else
      memFree (parttax + baseval);

    // 3) Remaillage des zones
    // 	a) conversion pampa -> mmg ok
    // 	b) remaillage ok
    // 	c) conversion mmg -> pampa quasi ok
    if ((omshloctab = (Mesh *) memAlloc (meshlocnbr * sizeof (Mesh))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) { // XXX cette partie pourrait être parallélisé avec pthread
#ifdef PAMPA_DEBUG_ADAPT
      CHECK_FERR (meshCheck (imshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT */
      meshInit (omshloctab + meshlocnum);
      CHECK_FERR (PAMPA_remesh ((PAMPA_Mesh *) (imshloctab + meshlocnum), (PAMPA_Mesh *) (omshloctab + meshlocnum), dataptr, 0), proccomm); // FIXME remplacer le 0 par un drapeau
      omshloctab[meshlocnum].idnum = imshloctab[meshlocnum].idnum;
#ifdef PAMPA_DEBUG_ADAPT
      CHECK_FERR (meshCheck (omshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT */
      meshExit (imshloctab + meshlocnum);
    }
    memFree (imshloctab);
    CHECK_VDBG (cheklocval, proccomm);


    // 4) Reintegration des zones dans maillage distribué en cours
    CHECK_FERR (dmeshRebuild (srcmeshptr, meshlocnbr, omshloctab), proccomm);
#ifdef PAMPA_DEBUG_ADAPT
    CHECK_FERR (dmeshCheck (srcmeshptr), proccomm);
    printf ("[%d] Number of local vertices in destination distributed mesh : %d\n", srcmeshptr->proclocnum, srcmeshptr->vertlocnbr);
#endif /* PAMPA_DEBUG_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE
#ifndef PAMPA_DEBUG_DMESH_SAVE2
      Mesh wmshdat;
      char s1[50];
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
    sprintf (s1, "dmesh-adapt-%d-%d", srcmeshptr->proclocnum, wmshcnt ++);
    if (srcmeshptr->proclocnum == 0) {
      meshInit (&wmshdat);
      CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
      CHECK_FERR (PAMPA_MMG3D_meshSave(&wmshdat, 1, dataptr, NULL, s1), proccomm);
      meshExit (&wmshdat);
    }
    else
      CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);

#endif /* PAMPA_DEBUG_DMESH_SAVE */
    for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) 
      meshFree (omshloctab + meshlocnum);

    memFree (omshloctab);

    CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
    SCOTCH_dgraphFree (&dbgfdat);
  }

  CHECK_FERR (dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_ANY, &dgrfdat, NULL), proccomm);
  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

  if (memAllocGroup ((void **) (void *)
		&paroloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
		&partloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  paroloctax -= baseval;
  partloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = dvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
   	paroloctax[vertlocnum] = srcmeshptr->proclocnum;

  //// 5) Repartitionnement
  //vmloloctab = NULL;
  //emraval = 1;
  //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "q{strat=m{asc=b{width=3,bnd=(d{pass=40,dif=1,rem=0}|)f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.01,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=0}}"), proccomm);
  //CHECK_FERR (SCOTCH_dgraphRepart (&dgrfdat, srcmeshptr->procglbnbr, paroloctax + baseval, emraval, vmloloctab, &strtdat, partloctax + baseval), proccomm);

  dmeshDgraphExit (&dgrfdat);
  SCOTCH_dgraphExit (&bgrfdat);
  SCOTCH_dgraphExit (&igrfdat);
  SCOTCH_dgraphExit (&cgrfdat);
  SCOTCH_dgraphExit (&dbgfdat);
  SCOTCH_stratExit (&strtdat);
  if (srcmeshptr->proclocnum == 0)
	SCOTCH_graphExit (&cbgfdat);
  // 5 bis) Redistribution ok
  //CHECK_FERR (dmeshRedist (srcmeshptr, partloctax, NULL, -1, -1, -1, dstmeshptr), proccomm); // XXX peut-être utiliser un dsmeshRedist qui serait moins gourmand en mémoire à mon avis
  memFree (paroloctax + baseval);
  CHECK_VDBG (cheklocval, proccomm);
  return (0);
} 

int dmeshAdapt2 (
	Dmesh * restrict const meshptr,
	SCOTCH_Dgraph * fgrfptr,
	Gnum *          fvrtloctax,
	SCOTCH_Dgraph * cgrfptr,
	Gnum *          cprcvrttab,
	const Gnum      coarval,
	const Gnum      coarnbr,
	const double    coarrat)
{
  Gnum baseval;
  Gnum cvrtlocbas;
  Gnum cvrtlocnbr;
  Gnum tvrtlocnbr;
  Gnum fvrtlocbas;
  Gnum fvrtlocnbr;
  Gnum fvrtglbnbr;
  Gnum fvrtlocmx2;
  int procngbnum;
  int procngbnnd;
  Gnum datasndnbr;
  Gnum datarcvnbr;
  Gnum datarcvidx;
  Gnum * restrict multloctax;
  Gnum * restrict tvrtloctax;
  Gnum * restrict dsndcnttab;
  Gnum * restrict dsnddsptab;
  Gnum * restrict drcvcnttab;
  Gnum * restrict drcvdsptab;
  Gnum * restrict fprcngbtab;
  Gnum * restrict fprcvrttab;
  Gnum * restrict datasndtab;
  Gnum * restrict datarcvtab;
  MPI_Comm proccomm;
  int        cheklocval;

  cheklocval = 0;
  baseval = meshptr->baseval;
  proccomm = meshptr->proccomm;

  SCOTCH_dgraphData (fgrfptr, NULL, &fvrtglbnbr, &fvrtlocnbr, &fvrtlocmx2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 


  if (memAllocGroup ((void **) (void *)
		&fprcngbtab,  (size_t) (meshptr->procglbnbr       * sizeof (Gnum)),
		&fprcvrttab,  (size_t) ((meshptr->procglbnbr + 1) * sizeof (Gnum)),
        &multloctax,  (size_t) (2 * fvrtlocnbr            * sizeof (Gnum)),
        &dsndcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
        &dsnddsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
        &drcvcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
        &drcvdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
		NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  multloctax -= 2 * baseval;
  memSet (dsndcnttab, 0, meshptr->procglbnbr * sizeof (Gnum));

  CHECK_VDBG (cheklocval, proccomm);
  if (commAllgather (&fvrtlocmx2, 1, GNUM_MPI,
        fprcngbtab, 1, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  fprcvrttab[0] = baseval;
  for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) 
    fprcvrttab[procngbnum + 1] = fprcvrttab[procngbnum] + fprcngbtab[procngbnum];

  fvrtlocbas = fprcvrttab[meshptr->proclocnum] - baseval;
  const Gnum fvrtlocmin = fprcvrttab[meshptr->proclocnum];
  const Gnum fvrtlocmax = fprcvrttab[meshptr->proclocnum + 1] - 1;

  // arrivée à la fin de la récursion
  if ((coarval == 0) || (fvrtglbnbr == 1)) {
	Gnum cvrtlocmax;
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;
	Gnum * restrict cprcngbtab;

	CHECK_FERR (SCOTCH_dgraphCoarsen (fgrfptr, coarnbr, coarrat, SCOTCH_COARSENNOMERGE, cgrfptr, multloctax + 2 * baseval), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (cgrfptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

  	SCOTCH_dgraphData (cgrfptr, NULL, NULL, &cvrtlocnbr, &cvrtlocmax, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 

  	if ((cprcngbtab = (Gnum *) memAlloc (meshptr->procglbnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllgather (&cvrtlocmax, 1, GNUM_MPI,
          cprcngbtab, 1, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

  	cprcvrttab[0] = baseval;
  	for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) 
      cprcvrttab[procngbnum + 1] = cprcvrttab[procngbnum] + cprcngbtab[procngbnum];

	memFree (cprcngbtab);
	cvrtlocbas = cprcvrttab[meshptr->proclocnum] - baseval;
	for (cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval;
		cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
        fvrtlocnum = multloctax[2 * cvrtlocnum + multdsp];

		if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
		  }
		  dsndcnttab[procngbnum] ++;
		}
		else 
		  fvrtloctax[fvrtlocnum - fvrtlocbas] = cvrtlocnum + cvrtlocbas;
	  }
	}
  }
  // n-ième étape de la récursion
  else {
  	SCOTCH_Dgraph tgrfdat; /* Temporary graph */
	Gnum tvrtlocnum;
	Gnum tvrtlocnnd;

	CHECK_FERR (SCOTCH_dgraphInit (&tgrfdat, meshptr->proccomm), proccomm);

	CHECK_FERR (SCOTCH_dgraphCoarsen (fgrfptr, coarnbr, coarrat, SCOTCH_COARSENNOMERGE, &tgrfdat, multloctax + 2 * baseval), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (&tgrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

	SCOTCH_dgraphSize (&tgrfdat, NULL, &tvrtlocnbr, NULL, NULL);

  	if ((tvrtloctax = (Gnum *) memAlloc (tvrtlocnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	tvrtloctax -= baseval;

  	cheklocval = dmeshAdapt2 (meshptr, &tgrfdat, tvrtloctax, cgrfptr, cprcvrttab, coarval - 1, coarnbr, coarrat);
  	CHECK_VERR (cheklocval, proccomm);
	SCOTCH_dgraphExit (&tgrfdat);

	SCOTCH_dgraphSize (cgrfptr, NULL, &cvrtlocnbr, NULL, NULL);

	for (tvrtlocnum = baseval, tvrtlocnnd = tvrtlocnbr + baseval;
		tvrtlocnum < tvrtlocnnd; tvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
        fvrtlocnum = multloctax[2 * tvrtlocnum + multdsp];

		if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
		  }
		  dsndcnttab[procngbnum] ++;
		}
		else 
		  fvrtloctax[fvrtlocnum - fvrtlocbas] = tvrtloctax[tvrtlocnum];
	  }
	}
  }

  /* Compute displacement sending array */
  dsnddsptab[0] = 0;
  for (procngbnum = 1, procngbnnd = meshptr->procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) {
	dsndcnttab[procngbnum - 1] <<= 1;
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  }

  datasndnbr = dsnddsptab[meshptr->procglbnbr];

  //* envoyer dsndcnttab et recevoir drcvcnttab
  CHECK_VDBG (cheklocval, proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  //* en déduire ddsp{snd,rcv}tab
  /* Compute displacement receiving array */
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  drcvdsptab[meshptr->procglbnbr] = drcvdsptab[meshptr->procglbnbr - 1] + drcvcnttab[meshptr->procglbnbr - 1];
  datarcvnbr = drcvdsptab[meshptr->procglbnbr];
  dsndcnttab[0] = dsnddsptab[0];

  //* allouer datasndtab et datarcvtab
  //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
  if (memAllocGroup ((void **) (void *)
        &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
        &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  if ((coarval == 0) || (fvrtglbnbr == 1)) {
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;

  	for (procngbnum = meshptr->proclocnum, cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval;
	  	cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
		Gnum fvrtlocbs2;
      	fvrtlocnum = multloctax[2 * cvrtlocnum + multdsp];

	  	if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
			fvrtlocbs2 = fprcvrttab[procngbnum] - baseval;
		  }
		  datasndtab[dsndcnttab[procngbnum] ++] = fvrtlocnum - fvrtlocbs2;
		  datasndtab[dsndcnttab[procngbnum] ++] = cvrtlocnum + cvrtlocbas;
	  	}
	  }
  	}
  }
  else {
	Gnum tvrtlocnum;
	Gnum tvrtlocnnd;
	for (procngbnum = meshptr->proclocnum, tvrtlocnum = baseval, tvrtlocnnd = tvrtlocnbr + baseval;
		tvrtlocnum < tvrtlocnnd; tvrtlocnum ++) {
	  Gnum multdsp;

	  for (multdsp = 0; multdsp < 2 ; multdsp ++) {
		Gnum fvrtlocnum;
		Gnum fvrtlocbs2;
        fvrtlocnum = multloctax[2 * tvrtlocnum + multdsp];

		if ((fvrtlocnum < fvrtlocmin) || (fvrtlocnum > fvrtlocmax)) {
    	  if (!((fprcvrttab[procngbnum] <= fvrtlocnum) && (fprcvrttab[procngbnum+1] > fvrtlocnum))) {
			int procngbmax;
          	for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (fprcvrttab[procngbmed] <= fvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
			fvrtlocbs2 = fprcvrttab[procngbnum] - baseval;
		  }

		  datasndtab[dsndcnttab[procngbnum] ++] = fvrtlocnum - fvrtlocbs2;
		  datasndtab[dsndcnttab[procngbnum] ++] = tvrtloctax[tvrtlocnum];
		}
	  }
	}

	memFree (tvrtloctax + baseval);
  }

  for (procngbnum = 0 ; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
  }

  //* envoyer datasndtab et recevoir datarcvtab
  CHECK_VDBG (cheklocval, proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  intSort2asc1 (datarcvtab, datarcvnbr >> 1);

  for (datarcvidx = 0; datarcvidx < datarcvnbr; datarcvidx += 2)
	fvrtloctax[datarcvtab[datarcvidx]] = datarcvtab[datarcvidx + 1];

  memFree (fprcngbtab);
  memFree (datasndtab);

  return (0);
}

static
  int
dmeshAdaptVertResize (
  	Dmesh * restrict const    meshptr,              /**< mesh */
	Gnum * restrict *               hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_DMESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx];

    if (vertlocnum == ~0) // If empty slot
      continue;

  	for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if ((*hashtabptr)[hashnum] == vertlocnum)
      	errorPrint ("vertex number is already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_DMESH */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (Gnum *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (Gnum)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (Gnum)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx] != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx];

    if (vertlocnum == ~0) // If empty slot
      continue;

    for (hashnum = (vertlocnum * DMESHADAPTHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum] != ~0 && (*hashtabptr)[hashnum] != vertlocnum; hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx] = ~0;
  }

#ifdef PAMPA_DEBUG_DMESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx];

    if (vertlocnum == ~0) // If empty slot
      continue;

  	for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if ((*hashtabptr)[hashnum] == vertlocnum)
      	errorPrint ("vertex number is already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_DMESH */
  return (0);
}
