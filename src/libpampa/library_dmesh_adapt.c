/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_adapt.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 10 Sep 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"
#include "pampa.h"
#include "dmesh_adapt.h"

/************************************/
/*                                  */
/* These routines are the C API for */
/* the distributed mesh handling    */
/* routines.                        */
/*                                  */
/************************************/

//! \brief This routine checks the consistency
//! of the given mesh.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_dmeshAdapt (
PAMPA_Dmesh * const  srcmeshptr,                                           //!< Distributed mesh to check
PAMPA_AdaptInfo * const infoptr,
PAMPA_Dmesh * const    dstmeshptr)
{
  Dmesh * meshptr;
  Gnum vertglbnbr; /* Global number of vertices */
  Gnum rmshglbnbr; /* Global number of vertices which need to be remeshed */
  Gnum rmsoglbnbr;
  Gnum vertlocnbr;
  Gnum rmshlocnbr; /* number of local vertices which need to be remeshed */
  Gnum vertlocnnd;
  Gnum vertlocnum;
  Gnum enodlocnbr;
  Gnum cvrtglbnbr;
  double alphval;
  double alphvl2;
  double rateval;
  Gnum baseval;
  Gnum * restrict rmshloctab;
  double * restrict wghtloctab;
  int cheklocval;

  meshptr = (Dmesh *) srcmeshptr;
  baseval = meshptr->baseval;
  cheklocval = 0;

  infoptr->eitenum = 0;
  infoptr->eitenbr = 2;
  //infoptr->eitenbr = 2;
  //infoptr->ibndval = 2;

  //infoptr->enodglbnbr = 0;
  //return dmeshAdaptPart ((Dmesh * const) srcmeshptr, infoptr, dstmeshptr);

  vertlocnbr = meshptr->enttloctax[baseval]->vertlocnbr;
  rmsoglbnbr = 0;
  CHECK_FERR (PAMPA_dmeshValueLink(srcmeshptr, (void **) &rmshloctab, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, baseval, PAMPA_TAG_REMESH), meshptr->proccomm);
  CHECK_FERR (PAMPA_dmeshValueLink(srcmeshptr, (void **) &wghtloctab, PAMPA_VALUE_PRIVATE, MPI_DOUBLE, baseval, PAMPA_TAG_WEIGHT), meshptr->proccomm);

  if (meshptr->proclocnum == 0)
    infoPrint ("alpha: %lf\n", infoptr->alpha);
  if (infoptr->alpha != 1.0)
    CHECK_FERR (infoptr->EXT_dmeshMetricCompute (srcmeshptr, infoptr->dataptr, infoptr->alpha), meshptr->proccomm);

  do {
    double cond, val1, val2;
    int pass = 1;
    alphval = 1.0;

    infoptr->eitenum ++;
    if (infoptr->itdebug != NULL) {
      char s[500];
      File f;
      f.name = s;
      sprintf (s, infoptr->itdebug, (int) infoptr->eitenum, (int) 0);
      f.mode = "w";
      f.pntr = stdout;
      CHECK_FERR (fileBlockOpen (&f, 1), meshptr->proccomm);
      CHECK_FERR (PAMPA_dmeshSave ((PAMPA_Dmesh *) meshptr, f.pntr), meshptr->proccomm);
      CHECK_FERR (fileBlockClose (&f, 1), meshptr->proccomm);
    }

    do {
      if (pass == 2)
        pass = 0;

      CHECK_FERR (infoptr->EXT_dmeshCheck(srcmeshptr, infoptr->dataptr, 0), meshptr->proccomm);
	  //if (infoptr->eitenum == 1) {
	  //      vertlocnbr = meshptr->enttloctax[baseval]->vertlocnbr;
	  //      CHECK_FERR (PAMPA_dmeshValueData(srcmeshptr, baseval, PAMPA_TAG_REMESH, (void **) &rmshloctab), meshptr->proccomm);
	  //      for (rmshlocnbr = vertlocnum = 0; vertlocnum < vertlocnbr; vertlocnum ++) 
	  //        rmshloctab[vertlocnum] = PAMPA_TAG_VERT_REMESH;
	  //}

//      // XXX début temporaire
//#define PAMPA_DEBUG_DMESH_SAVE2
//      // XXX fin temporaire
//#ifdef PAMPA_DEBUG_DMESH_SAVE2
//          {
//            Mesh * imshloctab;
//            char s1[100];
//            Gnum * vnumloctax;
//            Gnum parttab[1];
//            Gnum meshlocnbr;
//
//            parttab[0] = 0;
//
//            if (memAllocGroup ((void **) (void *)
//                  &vnumloctax, (size_t) (meshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
//                  NULL) == NULL) {
//              errorPrint ("Out of memory");
//              cheklocval = 1;
//            }
//            CHECK_VERR (cheklocval, proccomm);
//            vnumloctax -= baseval;
//
//            memSet (vnumloctax + baseval, ~0, meshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
//            for (vertlocnum = baseval, vertlocnnd = meshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
//              if (rmshloctab[vertlocnum] == PAMPA_TAG_VERT_REMESH) { // XXX attention rmshloctab au lieu de rmshloctax, à changer !!
//                vnumloctax[vertlocnum] = 0;
//              }
//
//            imshloctab = NULL;
//            meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
//            CHECK_FERR (dmeshGatherInduceMultiple (meshptr, 0, 1, vnumloctax, parttab - baseval, &meshlocnbr, &imshloctab), proccomm);
//            sprintf (s1, "dmesh-adapt-part-%d.mesh", infoptr->iitenum);
//            if (meshlocnbr > 0) {
//              CHECK_FERR (infoptr->EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, infoptr->dataptr, NULL, s1), proccomm);
//              meshExit (imshloctab);
//            }
//            memFreeGroup (vnumloctax + baseval);
//
//            memFree (imshloctab);
//          }
//#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
      CHECK_FERR (PAMPA_dmeshValueData(srcmeshptr, baseval, PAMPA_TAG_WEIGHT, (void **) &wghtloctab), meshptr->proccomm);
      CHECK_FERR (infoptr->EXT_dmeshWeightCompute (srcmeshptr, infoptr->dataptr, wghtloctab, &enodlocnbr), meshptr->proccomm);

      //printf("loc %d\n",enodlocnbr);
      CHECK_VDBG (cheklocval, meshptr->proccomm);
      if (commAllreduce (&enodlocnbr, &infoptr->enodglbnbr, 1, GNUM_MPI, MPI_SUM, meshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);
      // XXX tmp
      //infoptr->enodglbnbr = 8145397974; // 5e4
      //infoptr->enodglbnbr = 164998919; // 2e3
      //infoptr->enodglbnbr = 10166413; // 5e3
      //infoptr->enodglbnbr = 11382618; // 4e3
      //infoptr->enodglbnbr = 34512745; // 3e3
      // XXX fin tmp

      if (meshptr->proclocnum == 0)
        printf("npcible" GNUMSTRING "\n",infoptr->enodglbnbr);

      vertlocnbr = meshptr->enttloctax[baseval]->vertlocnbr;
      CHECK_FERR (PAMPA_dmeshValueData(srcmeshptr, baseval, PAMPA_TAG_REMESH, (void **) &rmshloctab), meshptr->proccomm);
      for (rmshlocnbr = vertlocnum = 0; vertlocnum < vertlocnbr; vertlocnum ++) 
        if (rmshloctab[vertlocnum] == PAMPA_TAG_VERT_REMESH)
          rmshlocnbr ++;

      //infoPrint ("rmshlocnbr: " GNUMSTRING, rmshlocnbr);
      commAllreduce(&rmshlocnbr, &rmshglbnbr, 1, PAMPA_MPI_NUM, MPI_SUM, MPI_COMM_WORLD);

      if (meshptr->proclocnum == 0)
        infoPrint ("rmshglbnbr: " GNUMSTRING, rmshglbnbr);
      cond = 10000;
      val1 = (1.0 * infoptr->enodglbnbr - (1.0 * meshptr->enttloctax[baseval]->vertglbnbr - rmshglbnbr) / 6) / 250000;
      val2 = 1.0 * rmshglbnbr / 1000000;
      cvrtglbnbr = ((1.0 * rmshglbnbr / 6) > cond) ? MAX(ceil(val1), ceil(val2)) : 1;
      if (infoptr->estmflg != 0) {
        if (meshptr->proclocnum == 0)
          infoPrint ("Estimation number of zones: %d", (int) cvrtglbnbr);
        return (0);
      }
      //cvrtglbnbr = ceil (MAX((1.0 * infoptr->enodglbnbr - (1.0 * meshptr->enttloctax[baseval]->vertglbnbr - rmshglbnbr) / 6) / 300000, 1.0 * rmshglbnbr / 6000000)); // XXX mettre ces valeurs dans une structure qui pourrait être modifiée par l'utilisateur (comme pour les valeurs associées ?)

      alphvl2 = alphval;
      if (pass == 1) {
        //if ((cvrtglbnbr > 1) && ((rmshglbnbr / (6 * cvrtglbnbr)) < 1000000)) {
        //  alphval = infoptr->enodglbnbr * 1.0 / (3.0 * 300000);
        //  if ((rmshglbnbr / 1000000) > 1)
        //    alphval *= 1000000 * 1.0 / rmshglbnbr;
        //  if ((alphval > 2) && (infoptr->eitenum < 3)) // XXX temporaire
        //    alphval = 2;
        //  else
        //    if ((alphval > 1.5) && (infoptr->eitenum < 5)) // XXX temporaire
        //      alphval = 1.5;
        //    else
        //      alphval = 1;

        //  pass = 2;
        //}
        //else
          pass = 0;

      }

      if (alphval != alphvl2)
        CHECK_FERR (infoptr->EXT_dmeshMetricCompute (srcmeshptr, infoptr->dataptr, alphval), meshptr->proccomm);

    } while (pass != 0);

    if (meshptr->proclocnum == 0)
      infoPrint ("alphval: %f, ebnd: " GNUMSTRING ", ibnd: " GNUMSTRING, infoptr->ebndval, infoptr->ibndval, alphval);



    infoptr->EXT_dmeshBand (srcmeshptr, infoptr->dataptr, rmshloctab, infoptr->ebndval);

    if (meshptr->proclocnum == 0)
    infoPrint ("external loop, iteration %d", infoptr->eitenum);
    //infoPrint ("external loop, iteration %d on %d", infoptr->eitenum, infoptr->eitenbr);
    vertglbnbr = meshptr->enttloctax[baseval]->vertglbnbr;
    if (rmsoglbnbr < rmshglbnbr)
      rateval = 1 - rmsoglbnbr * 1.0 / rmshglbnbr;
    else
      rateval = 1 - rmshglbnbr * 1.0 / rmsoglbnbr;
    rmsoglbnbr = rmshglbnbr;
    //if ((infoptr->loopval == 1) && (meshptr->proclocnum == 0)) 
    if (meshptr->proclocnum == 0)
      infoPrint ("external loop, rateval: %f, ratemin: %f, vertglb: " GNUMSTRING, rateval, infoptr->ratemin, rmshglbnbr);
      
    //if (infoptr->eitenum == 1)
    //  infoptr->ibndval = 1;
    //else
    //  infoptr->ibndval = 3;

    if ((infoptr->eitenum <= 2) || (rateval >= infoptr->ratemin) || (infoptr->loopval == 0))  {
      if (strcmp(infoptr->methval, "mbGeompart") == 0) { /* mb: mesh band */
        CHECK_FERR (dmeshAdaptGeompart ((Dmesh * const) srcmeshptr, infoptr, dstmeshptr), meshptr->proccomm);
      }
      else if (strcmp(infoptr->methval, "mbPart") == 0) { /* mb: mesh band */
        CHECK_FERR (dmeshAdaptPart ((Dmesh * const) srcmeshptr, infoptr, dstmeshptr), meshptr->proccomm);
      }
      //else if (strcmp(infoptr->methval, "mbMeshGrow") == 0) { /* mb: mesh band */
      //  CHECK_FERR (dmeshAdaptMeshGrow ((Dmesh * const) srcmeshptr, (Gnum) ibndval, (Gnum) ballsiz, EXT_meshAdapt, EXT_dmeshCheck, EXT_dmeshBand, EXT_dmeshWeightCompute, EXT_meshSave, dataptr, dstmeshptr), meshptr->proccomm);
      //}
      //else if (strcmp(methval, "gbCoarsen") == 0) { /* gb: graph band */
      //  CHECK_FERR (dmeshAdaptCoarsen ((Dmesh * const) srcmeshptr, (Gnum) ibndval, (Gnum) ballsiz, EXT_meshAdapt, EXT_dmeshCheck, EXT_dmeshBand, EXT_dmeshWeightCompute, EXT_meshSave, dataptr, dstmeshptr), meshptr->proccomm);
      //}
      //else if (strcmp(methval, "gbCoarsenGrow") == 0) {
      //  CHECK_FERR (dmeshAdaptCoarsenGrow ((Dmesh * const) srcmeshptr, (Gnum) ibndval, (Gnum) ballsiz, EXT_meshAdapt, EXT_dmeshCheck, EXT_dmeshBand, EXT_dmeshWeightCompute, EXT_meshSave, dataptr, dstmeshptr), meshptr->proccomm);
      //}
      //else if (strcmp(methval, "mbGraphGrow") == 0) {
      //  CHECK_FERR (dmeshAdaptGraphGrow ((Dmesh * const) srcmeshptr, (Gnum) ibndval, (Gnum) ballsiz, EXT_meshAdapt, EXT_dmeshCheck, EXT_dmeshBand, EXT_dmeshWeightCompute, EXT_meshSave, dataptr, dstmeshptr), meshptr->proccomm);
      //}
      else {
        errorPrint("undefined method");
        return (1);
      }
    }

    if (alphval != 1.0)
      CHECK_FERR (infoptr->EXT_dmeshMetricCompute (srcmeshptr, infoptr->dataptr, 1.0 / alphval), meshptr->proccomm);

    // XXX temporaire
    infoptr->zonenb2 = ~0;
    // XXX fin temporaire
  //} while (((rateval >= infoptr->ratemin) && (infoptr->loopval != 0)) || (alphval != 1));
  //} while (infoptr->eitenum < 2); // XXX temporaire
  //} while (infoptr->eitenum < infoptr->eitenbr); // XXX temporaire
  } while (((infoptr->loopval != 0) && ((infoptr->eitenum <= 2) || (infoptr->eitenum < infoptr->eitenbr) || (rateval >= infoptr->ratemin))) || (alphval != 1));

  CHECK_FERR (PAMPA_dmeshValueUnlink(srcmeshptr, baseval, PAMPA_TAG_REMESH), meshptr->proccomm);
  CHECK_FERR (PAMPA_dmeshValueUnlink(srcmeshptr, baseval, PAMPA_TAG_WEIGHT), meshptr->proccomm);
  return (0);

}
