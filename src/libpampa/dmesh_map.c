/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_map.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contain the distributed mesh
//!                mapping routines. 
//!
//!   \date        Version 1.0: from: 20 Feb 2012
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "comm.h"
#include <ptscotch.h>
#include "dvalues.h"
#include "dmesh.h"
#include "arch.h"
#include "strat.h"
#include "values.h"
#include "mesh.h"
#include "dmesh_dgraph.h"
#include "pampa.h"

//! \brief  XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dmeshMapElem (
Dmesh * const            meshptr,              //!< dmesh to map
const Gnum               partnbr,              //!< Number of parts 
Arch * const             archptr,              //!< Target architecture
Strat * const            stratptr,             //!< Mapping strategy
Gnum * const             maploctax)            //!< Mapping array   
{

  DmeshEntity * enttloctax;
  Gnum   baseval;
  Gnum   vertlocnbr;
  Gnum   vertlocnum;
  Gnum   vertlocnnd;
  Gnum   enttlocnum;
  Gnum   enttlocnnd;
  Gnum   vertlocbas;
  Gnum * restrict gmaploctax;
  Gnum * restrict veloloctax;
  SCOTCH_Dgraph dgraph;
  int cheklocval;

  cheklocval = 0;

  baseval = meshptr->baseval;
  enttloctax = meshptr->enttloctax[baseval];

  CHECK_FERR (dmeshDgraphInit(meshptr, &dgraph), meshptr->proccomm);

  if (memAllocGroup ((void **) (void *)
        &gmaploctax, (size_t) (enttloctax->vgstlocnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  gmaploctax -= baseval;

  vertlocnbr = meshptr->vertlocnbr;

  cheklocval = dmeshValueData(meshptr, baseval, PAMPA_TAG_LOAD, NULL, NULL, (void **) &veloloctax);
  if (cheklocval != 0) { /* If there is a value with this tag */
    if (cheklocval != 2) // XXX mettre un tag au lieu de 2
      return cheklocval;
    veloloctax = NULL;
    cheklocval = 0;
  }
  else
    veloloctax -= baseval;

  //infoPrint ("veloloctax: %p", veloloctax);
  CHECK_FERR (dmeshDgraphBuild(meshptr, DMESH_ENTT_MAIN, ~0, NULL, veloloctax, NULL, &dgraph, NULL), meshptr->proccomm); // XXX pas de edlo ??

#ifdef PAMPA_DEBUG_DGRAPH
  char s[30];
  sprintf(s, "dgraph-map-%d", meshptr->proclocnum);
  FILE *dgraph_file;
  dgraph_file = fopen(s, "w");
  SCOTCH_dgraphSave (&dgraph, dgraph_file);
  fclose(dgraph_file);

#endif /* PAMPA_DEBUG_DGRAPH */
#ifdef PAMPA_INFO_PART
  time_t begin = time (NULL);
#endif /* PAMPA_INFO_PART */
  if (archptr == NULL) {
    //CHECK_FERR (SCOTCH_dgraphPart(&dgraph, partnbr, (SCOTCH_Strat * const) stratptr, gmaploctax + baseval), meshptr->proccomm);
    //XXX debut tmp
    SCOTCH_Arch archdat;
    SCOTCH_Dmapping mappdat;
    SCOTCH_archInit  (&archdat);
    SCOTCH_archCmplt (&archdat, partnbr);

    SCOTCH_dgraphGhst (&dgraph);
    //CHECK_FERR (SCOTCH_dgraphPart(&dgraph, partnbr, (SCOTCH_Strat * const) stratptr, gmaploctax + baseval), meshptr->proccomm);
    SCOTCH_dgraphMapInit    (&dgraph, &mappdat, &archdat, gmaploctax + baseval);
    SCOTCH_dgraphMapCompute (&dgraph, &mappdat, (SCOTCH_Strat * const) stratptr); /* Perform mapping */
    SCOTCH_dgraphMapExit    (&dgraph, &mappdat);
    //XXX fin tmp

  }
  else {
    CHECK_FERR (SCOTCH_dgraphMap(&dgraph, (SCOTCH_Arch * const) archptr, (SCOTCH_Strat * const) stratptr, gmaploctax + baseval), meshptr->proccomm);
  }
#ifdef PAMPA_INFO_PART
  infoPrint ("%s Elapsed time %lu secondes", "dgraphPart", (time(NULL) - begin)); 
#endif /* PAMPA_INFO_PART */

  memSet( maploctax + meshptr->baseval, ~0, vertlocnbr * sizeof(Gnum));

  vertlocbas = meshptr->procvrttab[meshptr->proclocnum] - baseval;
  // Converting gmaptax to maptax for main entity
  for (vertlocnum = baseval, vertlocnnd = enttloctax->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)  {
    maploctax[enttloctax->mvrtloctax[vertlocnum] - vertlocbas] = gmaploctax[vertlocnum];
  }

  CHECK_FERR (dmeshHaloSync (meshptr, enttloctax, gmaploctax + baseval, GNUM_MPI), meshptr->proccomm);

  // Luby algorythm for each vertex matching neighors with main entity
  for (enttlocnum = baseval + 1, enttlocnnd = meshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
    if ((meshptr->esublocbax[enttlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshptr->enttloctax[enttlocnum] == NULL))
      continue;

    DmeshEnttNghb * nghbloctax;

    nghbloctax = meshptr->enttloctax[enttlocnum]->nghbloctax[baseval];

    for (vertlocnum = baseval, vertlocnnd = meshptr->enttloctax[enttlocnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)  {
      Gnum vmshlocnum, nghblocnum;

      nghblocnum = meshptr->edgeloctax[nghbloctax->vindloctax[vertlocnum].vertlocidx];

      vmshlocnum = meshptr->enttloctax[enttlocnum]->mvrtloctax[vertlocnum];

      maploctax[vmshlocnum - vertlocbas] = gmaploctax[nghblocnum];
    }

  }

  dmeshDgraphExit(&dgraph);

  memFreeGroup (gmaploctax + baseval);
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}


int
dmeshMapElem2 (
Dmesh * const            meshptr,              //!< dmesh to map
const Gnum               partnbr,              //!< Number of parts 
PAMPA_Arch * const       archptr,              //!< Target architecture
PAMPA_Strat * const      stratptr,             //!< Mapping strategy
Gnum * const             maploctax)            //!< Mapping array   
{

  DmeshEntity * enttloctax;
  Gnum   baseval;
  Gnum   vertlocnbr;
  Gnum   vertlocnum;
  Gnum   vertlocnnd;
  Gnum   enttlocnum;
  Gnum   enttlocnnd;
  Gnum   vertlocbas;
  int cheklocval;
  SCOTCH_Arch archdat;
  FILE * arch_file;

  cheklocval = 0;

  baseval = meshptr->baseval;
  if (meshptr->proclocnum == 0) {
    Mesh meshdat;
    Gnum * parttax;
    //FILE *arch_file;
    //SCOTCH_Arch archdat;

#ifdef TEST_NETLOC
    infoPrint ("graphpart with netloc");
    CHECK_FDBG2(SCOTCH_archInit (&archdat));
    arch_file = fopen ("subarch.scotch", "r");
    CHECK_FDBG2(SCOTCH_archLoad (&archdat, arch_file));
#endif /* TEST_NETLOC */

    CHECK_FERR (PAMPA_dmeshGather ((PAMPA_Dmesh *) meshptr, (PAMPA_Mesh *) &meshdat), meshptr->proccomm);

    if ((parttax = (Gnum *) memAlloc (meshdat.vertnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
    parttax -= baseval;

#ifndef TEST_NETLOC
    CHECK_FDBG2(PAMPA_meshPart((PAMPA_Mesh*) &meshdat, partnbr, stratptr, parttax));
#else /* TEST_NETLOC */
    CHECK_FDBG2(PAMPA_meshMap(&meshdat, &archdat, stratptr, parttax));
#endif /* TEST_NETLOC */

    meshExit (&meshdat);

    CHECK_VDBG (cheklocval, meshptr->proccomm);
    //{
    //  int i;
    //  infoPrint ("vertnbr: " GNUMSTRING, meshdat.vertnbr);
    //  for (i = 0; i < meshptr->procglbnbr; i ++)
    //    infoPrint ("cnt[%d] = " GNUMSTRING ", dsp[%d] = " GNUMSTRING, i, meshptr->proccnttab[i], i, meshptr->procdsptab[i]);
    //  infoPrint ("dsp[%d] = " GNUMSTRING, i, meshptr->procdsptab[i]);
    //}
    //infoPrint ("vertglbnbr: " GNUMSTRING, meshptr->vertglbnbr);
    //infoPrint ("vtrulocnbr: " GNUMSTRING ", vertlocnbr: " GNUMSTRING, meshptr->vtrulocnbr, meshptr->vertlocnbr);
    if (commGnumScatterv (parttax + baseval, meshptr->proccnttab, meshptr->procdsptab, GNUM_MPI,
          maploctax + baseval, meshptr->vertlocnbr, GNUM_MPI, 0, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
    memFree (parttax + baseval);
  }
  else {
    CHECK_FERR (PAMPA_dmeshGather ((PAMPA_Dmesh *) meshptr, NULL), meshptr->proccomm);

    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commGnumScatterv (NULL, NULL, NULL, GNUM_MPI,
          maploctax + baseval, meshptr->vertlocnbr, GNUM_MPI, 0, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
#ifdef PAMPA_INFO_PART
  infoPrint ("%s Elapsed time %lu secondes", "dgraphPart", (time(NULL) - begin)); 
#endif /* PAMPA_INFO_PART */
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}



//! \brief  XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dmeshMapVert (
Dmesh * const            meshptr,              //!< dmesh to map
const Gnum               partnbr,              //!< Number of parts 
PAMPA_Arch * const       archptr,              //!< Target architecture
PAMPA_Strat * const      stratptr,             //!< Mapping strategy
Gnum * const             maploctax)            //!< Mapping array   
{

  DmeshEntity * enttloctax;
  Gnum   baseval;
  Gnum   enttlocnum;
  Gnum   enttlocnnd;
  Gnum   vertlocnbr;
  Gnum   vertlocnum;
  Gnum   vertlocnnd;
  Gnum   vertlocbas;
  Gnum * gmaploctax;
  Gnum * vlblloctax;
  SCOTCH_Dgraph dgraph;
  int cheklocval;

  cheklocval = 0;

  baseval = meshptr->baseval;
  enttloctax = meshptr->enttloctax[baseval];
  //infoPrint("PartVert");
  
  //XXX mettre un avertissement disant qu'on n'utilise pas PAMPA_TAG_LOAD sur
  //les sommets

  CHECK_FERR (dmeshDgraphInit(meshptr, &dgraph), meshptr->proccomm);

  CHECK_FERR (dmeshDgraphBuild(meshptr, DMESH_ENTT_ANY, ~0, NULL, NULL, NULL, &dgraph, NULL), meshptr->proccomm); // XXX pas de velo ni de edlo ??

#ifdef PAMPA_DEBUG_DGRAPH
  char s[30];
  sprintf(s, "dgraph-map-%d", meshptr->proclocnum);
  FILE *dgraph_file;
  dgraph_file = fopen(s, "w");
  SCOTCH_dgraphSave (&dgraph, dgraph_file);
  fclose(dgraph_file);


#endif /* PAMPA_DEBUG_DGRAPH */
  if (memAllocGroup ((void **) (void *)
        &gmaploctax, (size_t) (meshptr->vtrulocnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  gmaploctax -= baseval;

  memSet( maploctax + meshptr->baseval, ~0, meshptr->vertlocnbr * sizeof(Gnum));

  time_t begin = time (NULL);
  if (archptr == NULL) {
    CHECK_FERR (SCOTCH_dgraphPart(&dgraph, partnbr, (SCOTCH_Strat * const) stratptr, gmaploctax + baseval), meshptr->proccomm);
  }
  else {
    CHECK_FERR (SCOTCH_dgraphMap(&dgraph, (SCOTCH_Arch * const) archptr, (SCOTCH_Strat * const) stratptr, gmaploctax + baseval), meshptr->proccomm);
  }
#ifdef PAMPA_INFO_PART
  infoPrint ("%s Elapsed time %lu secondes", "dgraphPart", (time(NULL) - begin)); 
#endif /* PAMPA_INFO_PART */

  vertlocbas = meshptr->procvrttab[meshptr->proclocnum] - baseval;
  SCOTCH_dgraphData(&dgraph, NULL, NULL, &vertlocnbr, NULL, NULL, NULL, NULL, NULL, &vlblloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
  vlblloctax -= baseval;

#ifdef PAMPA_DEBUG_DMESH
  if (vertlocnbr != meshptr->vtrulocnbr) {
    errorPrint ("Invalid vertex number between mesh and graph");
    return (1);
  }
#endif /* PAMPA_DEBUG_DMESH */

  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
    maploctax[vlblloctax[vertlocnum] - vertlocbas] = gmaploctax[vertlocnum];

  //// XXX on traite les sommets qui sont sur un proc ne correspondant pas à au
  //// moins un élément voisin
  //// XXX partie à finir : l'élément voisin n'est pas forcément local,
  //// du coup, nmshlocnum - vertlocbas est faux
  //vertlocbas = meshptr->procvrttab[meshptr->proclocnum] - baseval;
  //for (enttlocnum = baseval + 1, enttlocnnd = meshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
  //  Gnum nmshlocnum;

  //  if ((meshptr->esublocbax[enttlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshptr->enttloctax[enttlocnum] == NULL))
  //    continue;

  //  DmeshEnttNghb * nghbloctax;

  //  nghbloctax = meshptr->enttloctax[enttlocnum]->nghbloctax[baseval];

  //  for (vertlocnum = baseval, vertlocnnd = meshptr->enttloctax[enttlocnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)  {
  //    Gnum vmshlocnum;
  //    Gnum edgelocnum;
  //    Gnum edgelocnnd;

  //    for (edgelocnum = nghbloctax->vindloctax[vertlocnum].vertlocidx, edgelocnnd = nghbloctax->vindloctax[vertlocnum].vendlocidx; edgelocnum < edgelocnnd; edgelocnum ++)  {
  //      Gnum nghblocnum;

  //      nghblocnum = meshptr->edgeloctax[edgelocnum];

  //      vmshlocnum = meshptr->enttloctax[enttlocnum]->mvrtloctax[vertlocnum];
  //      nmshlocnum = meshptr->enttloctax[baseval]->mvrtloctax[nghblocnum];

  //      if (maploctax[vmshlocnum - vertlocbas] == maploctax[nmshlocnum - vertlocbas])
  //        break;
  //    }
  //    if (edgelocnum == nghbloctax->vindloctax[vertlocnum].vendlocidx)
  //      maploctax[vmshlocnum - vertlocbas] = maploctax[nmshlocnum - vertlocbas];
  //  }
  //}
  //// XXX fin partie à finir

  dmeshDgraphExit(&dgraph);
  memFreeGroup (gmaploctax + baseval);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}
