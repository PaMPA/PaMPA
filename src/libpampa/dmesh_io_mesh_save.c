
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define DMESH_IO_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"


#ifdef PAMPA_ADAPT
extern unsigned char MMG_idir[4][3];
#endif /* PAMPA_ADAPT */


/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
dmeshMeshSave (
Dmesh * restrict const     meshptr,
// XXX on devrait utiliser une struct contenant les types d'entités (faces ?, noeuds, éléments)
File * const              meshstm,               /* One single centralized mesh stream */
File * const              solstm)                /* One single centralized sol stream  */
{
  Gnum baseval;
  Gnum basedif;
  Gnum nodeent;
  Gnum nodenum;
  Gnum nodennd;
  Gnum nodenbr;
  Gnum nodelocnbr;
  Gnum nodelocbas;
  Gnum tetrent;
  Gnum tetrnbr;
  Gnum faceent;
  Gnum offset;
  PAMPA_Iterator it;
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it_ftot;
  PAMPA_Iterator it_tton;
  PAMPA_Num * restrict nreftax;
  PAMPA_Num * restrict ereftax;
  Gnum * restrict nodeloctax;
  double * restrict geomtax;
  double * restrict soltax;
  int cheklocval;
  int procnum;
  FILE * stream;

  baseval = meshptr->baseval;
  basedif = 1 - baseval;
  tetrent = 0; // XXX temporaire
  faceent = (meshptr->enttglbnbr > 2) ? 1 : ~0; // XXX temporaire
  nodeent = meshptr->enttglbnbr - 1 + baseval; // XXX temporaire
  cheklocval = 0;

  if (meshptr->proclocnum == 0) {
    fileBlockOpen (meshstm, 1);
    stream = fileBlockFile (meshstm, 0);
    cheklocval |= (fprintf(stream, "MeshVersionFormatted 2\n\nDimension 3\n\nVertices\n") == EOF); // XXX que du 3D ?
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }

  CHECK_FERR (dmeshValueData(meshptr, nodeent, PAMPA_TAG_GEOM, NULL, NULL, (void **) &geomtax), meshptr->proccomm);
  geomtax -= 3 * baseval;

  CHECK_FERR (dmeshValueData (meshptr, nodeent, PAMPA_TAG_REF, NULL, NULL, (void **) &nreftax), meshptr->proccomm);
  nreftax -= baseval;

  CHECK_FERR (dmeshValueData (meshptr, tetrent, PAMPA_TAG_REF, NULL, NULL, (void **) &ereftax), meshptr->proccomm);
  ereftax -= baseval;

  if (meshptr->proclocnum == 0) {
    nodenbr = meshptr->enttloctax[nodeent]->vertglbnbr;
    cheklocval |= (fprintf(stream, GNUMSTRING "\n", nodenbr) == EOF);
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }

  nodelocnbr = meshptr->enttloctax[nodeent]->vertlocnbr;
  CHECK_FMPI (cheklocval, MPI_Scan (&nodelocnbr, &nodelocbas, 1, GNUM_MPI, MPI_SUM, meshptr->proccomm), meshptr->proccomm);
  nodelocbas -= nodelocnbr;

  CHECK_FERR (dmeshValueLink(meshptr, (void **) &nodeloctax, PAMPA_VALUE_PUBLIC, NULL, NULL, GNUM_MPI, nodeent, -50), meshptr->proccomm); // XXX attention au tag
  nodeloctax -= baseval;

  for (nodenum = baseval, nodennd = baseval + nodelocnbr; nodenum < nodennd ; nodenum ++) 
    nodeloctax[nodenum] = nodelocbas + nodenum + basedif;
  PAMPA_dmeshHaloValue ((PAMPA_Dmesh *) meshptr, nodeent, -50);

  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    if (procnum == meshptr->proclocnum) {
      if (procnum != 0) 
        fileBlockOpen (meshstm, 1);
      stream = fileBlockFile (meshstm, 0);
      for (nodenum = baseval, nodennd = baseval + nodelocnbr; nodenum < nodennd ; nodenum ++) 
        cheklocval |= (fprintf(stream, "%.16lf %.16lf %.16lf " GNUMSTRING "\n", geomtax[nodenum * 3], geomtax[nodenum * 3 + 1], geomtax[nodenum * 3 + 2], nreftax[nodenum]) == EOF); 
      CHECK_VERR (cheklocval, meshptr->proccomm);
      fileBlockClose (meshstm, 1);
    }
    MPI_Barrier (meshptr->proccomm);
  }

  if (faceent != ~0) {
    PAMPA_Num facenbr;
    PAMPA_Num * restrict freftax;

    facenbr = (meshptr->enttloctax[faceent] == NULL) ? 0 : meshptr->enttloctax[faceent]->vertglbnbr;
    CHECK_FERR (dmeshValueData (meshptr, faceent, PAMPA_TAG_REF, NULL, NULL, (void **) &freftax), meshptr->proccomm);
    freftax -= baseval;

    if (meshptr->proclocnum == 0) {
      fileBlockOpen (meshstm, 1);
      stream = fileBlockFile (meshstm, 0);
      cheklocval |= (fprintf(stream, "\n\nTriangles\n" GNUMSTRING "\n", facenbr) == EOF);
      CHECK_VERR (cheklocval, meshptr->proccomm);
    }


    PAMPA_dmeshItInitStart((PAMPA_Dmesh *) meshptr, faceent, PAMPA_VERT_LOCAL, &it);
    PAMPA_dmeshItInit((PAMPA_Dmesh *) meshptr, faceent, nodeent, &it_nghb);
    PAMPA_dmeshItInit((PAMPA_Dmesh *) meshptr, faceent, tetrent, &it_ftot);
    PAMPA_dmeshItInit((PAMPA_Dmesh *) meshptr, tetrent, nodeent, &it_tton);

    for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
      if (procnum == meshptr->proclocnum) {
        if (procnum != 0)
          fileBlockOpen (meshstm, 1);
        stream = fileBlockFile (meshstm, 0);
        while (PAMPA_itHasMore(&it)) {
          PAMPA_Num facenum;
          PAMPA_Num tetrnum;
          PAMPA_Num i;
          PAMPA_Num v[3];
          PAMPA_Num v2[3];

          facenum = PAMPA_itCurEnttVertNum(&it);

          PAMPA_itStart(&it_nghb, facenum);

          i = 0;
          while (PAMPA_itHasMore(&it_nghb)) {
            PAMPA_Num nodenum;

            nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
            v[i ++] = nodeloctax[nodenum];
            PAMPA_itNext(&it_nghb);
          }

          // XXX cette partie est incompatible avec MMG3D, à corriger
          //#ifdef PAMPA_ADAPT
          //      PAMPA_itStart(&it_ftot, facenum);
          //
          //      if (PAMPA_itHasMore(&it_ftot))
          //        tetrnum = PAMPA_itCurEnttVertNum(&it_ftot);
          //      else {
          //        errorPrint ("internal error");
          //        return (1);
          //      }
          //
          //      PAMPA_itStart(&it_tton, tetrnum);
          //
          //      i = 0;
          //      while (PAMPA_itHasMore(&it_tton)) {
          //        PAMPA_Num nodenum;
          //
          //        nodenum = PAMPA_itCurEnttVertNum(&it_tton);
          //        v2[i ++] = nodenum + 1;
          //        PAMPA_itNext(&it_tton);
          //      }
          //
          //      for (i = 0; i < 4; i ++) {
          //        int j;
          //        for (j = 0; j < 3; j ++)
          //          if ((v[j] == v2[MMG_idir[i][0]]) && (v[(j + 1) % 3] == v2[MMG_idir[i][1]]) && (v[(j + 2) % 3] == v2[MMG_idir[i][2]]))
          //            break;
          //        if (j < 3)
          //          break;
          //      }
          //
          //      if (i != 4) {
          //        PAMPA_Num tmp;
          //        //infoPrint ("face trouvée");
          //        tmp = v[1];
          //        v[1] = v[2];
          //        v[2] = tmp;
          //      }
          //#endif /* PAMPA_ADAPT */

          cheklocval |= (fprintf(stream, GNUMSTRING " " GNUMSTRING " " GNUMSTRING " " GNUMSTRING "\n", v[0], v[1], v[2], freftax[facenum]) == EOF);

          PAMPA_itNext(&it);
        }
        fileBlockClose (meshstm, 1);
      }
      MPI_Barrier (meshptr->proccomm);
    }
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  tetrnbr = meshptr->enttloctax[tetrent]->vertglbnbr;
  if (meshptr->proclocnum == 0) {
    fileBlockOpen (meshstm, 1);
    stream = fileBlockFile (meshstm, 0);
    cheklocval |= (fprintf(stream, "\n\nTetrahedra\n" GNUMSTRING "\n", tetrnbr) == EOF);
  }
  PAMPA_dmeshItInitStart((PAMPA_Dmesh *) meshptr, tetrent, PAMPA_VERT_LOCAL, &it);
  PAMPA_dmeshItInit((PAMPA_Dmesh *) meshptr, tetrent, nodeent, &it_nghb);
  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    if (procnum == meshptr->proclocnum) {
      if (procnum != 0)
        fileBlockOpen (meshstm, 1);
      stream = fileBlockFile (meshstm, 0);
      while (PAMPA_itHasMore(&it)) {
        PAMPA_Num tetrnum;

        tetrnum = PAMPA_itCurEnttVertNum(&it);

        PAMPA_itStart(&it_nghb, tetrnum);

        while (PAMPA_itHasMore(&it_nghb)) {
          PAMPA_Num nodenum;

          nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
          cheklocval |= (fprintf(stream, GNUMSTRING " ", nodeloctax[nodenum]) == EOF);
          PAMPA_itNext(&it_nghb);
        }
        cheklocval |= (fprintf(stream, GNUMSTRING "\n", ereftax[tetrnum]) == EOF);

        PAMPA_itNext(&it);
      }
      if (meshptr->proclocnum != (meshptr->procglbnbr - 1)) 
        fileBlockClose (meshstm, 1);
    }
    MPI_Barrier (meshptr->proccomm);
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if (meshptr->proclocnum == (meshptr->procglbnbr - 1)) {
    cheklocval |= (fprintf(stream, "\n\nEnd\n") == EOF);
    fileBlockClose (meshstm, 1);
  }


  if (meshptr->proclocnum == 0) {
    fileBlockOpen (solstm, 1);
    stream = fileBlockFile (solstm, 0);
    cheklocval |= (fprintf(stream, "MeshVersionFormatted 2\n\nDimension 3\n\nSolAtVertices\n") == EOF); // XXX dimesion 3 ?
  }
  cheklocval = PAMPA_dmeshValueData((PAMPA_Dmesh *) meshptr, nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
  if (cheklocval == 0) {  /* If there is a value with this tag */
    offset = 1;
    if (meshptr->proclocnum == 0) {
      cheklocval |= (fprintf(stream, GNUMSTRING "\n1 1\n", nodenbr) == EOF);
    }
  }
  else if (cheklocval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    return (cheklocval);
  }
  else { /* If there no value with this tag */
    cheklocval = PAMPA_dmeshValueData((PAMPA_Dmesh *) meshptr, nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
    if (cheklocval == 0) {  /* If there is a value with this tag */
      offset = 6;
      if (meshptr->proclocnum == 0) {
        cheklocval |= (fprintf(stream, GNUMSTRING "\n1 3\n", nodenbr) == EOF);
      }
    }
    else if (cheklocval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (cheklocval);
    }
    else { /* If there no value with this tag */
      errorPrint ("No associated metric");
      return (cheklocval);
    }
  }
  soltax -= offset * baseval;

  for (procnum = 0; procnum < meshptr->procglbnbr; procnum ++) {
    if (procnum == meshptr->proclocnum) {
      if (procnum != 0)
        fileBlockOpen (solstm, 1);
      stream = fileBlockFile (solstm, 0);
      for (nodenum = baseval, nodennd = baseval + nodelocnbr; nodenum < nodennd ; nodenum ++) {
        Gnum tmp;
        Gnum tmpmax;
        for (tmp = 0, tmpmax = MIN (2, offset); tmp < tmpmax; tmp ++)
          cheklocval |= (fprintf(stream, "%.16lf ", soltax[nodenum * offset + tmp]) == EOF); 
        if (offset == 6) {
          cheklocval |= (fprintf(stream, "%.16lf ", soltax[nodenum * offset + 3]) == EOF); 
          cheklocval |= (fprintf(stream, "%.16lf ", soltax[nodenum * offset + 2]) == EOF); 
          for (tmp = 4; tmp < offset; tmp ++)
            cheklocval |= (fprintf(stream, "%.16lf ", soltax[nodenum * offset + tmp]) == EOF); 
        }
        cheklocval |= (fprintf(stream, "\n") == EOF); 
      }
      if (meshptr->proclocnum != (meshptr->procglbnbr - 1)) 
        fileBlockClose (solstm, 1);
    }
    MPI_Barrier (meshptr->proccomm);
  }

  if (meshptr->proclocnum == (meshptr->procglbnbr - 1)) {
    cheklocval |= (fprintf(stream, "\n\nEnd\n") == EOF);
    fileBlockClose (solstm, 1);
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  return (cheklocval);
}
