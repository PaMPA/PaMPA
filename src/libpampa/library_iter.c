/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_iter.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations
//!                for the mesh iterator purpose routines.
//!
//!   \date        Version 1.0: from: 17 Mar 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#ifdef PAMPA_DEBUG_ITER
#undef PAMPA_DEBUG_ITER
#define PAMPA_DEBUG_ITER_INT
#include "pampa.h"
#define PAMPA_DEBUG_ITER
#undef PAMPA_DEBUG_ITER_INT
#else /* PAMPA_DEBUG_ITER */
#include "pampa.h"
#endif /* PAMPA_DEBUG_ITER */

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* mesh iterator handling routines.     */
/*                                      */
/****************************************/




#ifdef PAMPA_DEBUG_ITER

//! \brief This routine initialize the iterator by using vertex number.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_itStartDebug (
PAMPA_Iterator *            iterptr,                                    //!< Iterator to fill in
PAMPA_Num const             vertnum)                                    //!< Vertex number
{

  if ((iterptr->c.vertmax != ~0) && (vertnum > iterptr->c.vertmax)) {
    errorPrint ("Invalid vertex number (test only available for local vertices, there is no overlap for mdmesh)");
    return (1);
  }
  PAMPA_itStart(iterptr, vertnum);


  return (0);
}

//! \brief This routine test if the vertex in the iterator has more neighbors.
//!
//! \returns 0   : no more neighbor.
//! \returns !0  : more neighbors.
//!
//! \todo Add some debugging tests
int PAMPA_itHasMoreDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  int o = PAMPA_itHasMore(iterptr);
  //infoPrint ("%d", o);
  return (o);
}

//! \brief This routine returns the next vertex or neighbor in the iterator.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
int PAMPA_itNextDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  PAMPA_itNext(iterptr);
  return (0);
}


//! \brief This routine gives the current number of vertex by entity.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itCurEnttVertNumDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  if (!PAMPA_itHasMore (iterptr)) {
    errorPrint ("No more item in the iterator");
    return (~0);
  }
  PAMPA_Num o = PAMPA_itCurEnttVertNum(iterptr);
  //infoPrint ("%d", o);
  return (o);
}



//! \brief This routine gives the current number of vertex by sub-entity.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itCurSubEnttVertNumDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  if (!PAMPA_itHasMore (iterptr)) {
    errorPrint ("No more item in the iterator");
    return (~0);
  }
  PAMPA_Num o = PAMPA_itCurSubEnttVertNum(iterptr);
  //infoPrint ("%d", o);
  return (o);
}



//! \brief This routine gives the current number of vertex by mesh.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itCurMeshVertNumDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  if (!PAMPA_itHasMore (iterptr)) {
    errorPrint ("No more item in the iterator");
    return (~0);
  }
  PAMPA_Num o = PAMPA_itCurMeshVertNum(iterptr);
  //infoPrint ("%d", o);
  return (o);
}



//! \brief This routine gives the number of sub-entity of the current vertex
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itCurSubEnttNumDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  if (!PAMPA_itHasMore (iterptr)) {
    errorPrint ("No more item in the iterator");
    return (~0);
  }
  PAMPA_Num o = PAMPA_itCurSubEnttNum(iterptr);
  //infoPrint ("%d", o);
  return (o);
}



//! \brief This routine gives if the vertex belongs to a sub-entity and iterator
//! created with this sub-entity
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itIsSubEnttDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  PAMPA_Num o = PAMPA_itIsSubEntt(iterptr);
  //infoPrint ("%d", o);
  return (o);
}



//! \brief This routine gives if the vertex belongs to a sub-entity
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itHasSubEnttDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  PAMPA_Num o = PAMPA_itHasSubEntt(iterptr);
  //infoPrint ("%d", o);
  return (o);
}



//! \brief This routine gives the number of entity of the current vertex
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
//!
//! \todo Add some debugging tests
PAMPA_Num PAMPA_itCurEnttNumDebug(
PAMPA_Iterator *            iterptr)                                    //!< Iterator structure
{
  if (!PAMPA_itHasMore (iterptr)) {
    errorPrint ("No more item in the iterator");
    return (~0);
  }
  PAMPA_Num o = PAMPA_itCurEnttNum(iterptr);
  //infoPrint ("%d", o);
  return (o);
}

#endif /* PAMPA_DEBUG_ITER */
