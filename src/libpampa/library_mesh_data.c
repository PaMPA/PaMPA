/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_data.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API for the mesh data
//!                accessors of the libpampa library. 
//!
//!   \date        Version 1.0: from: 13 Oct 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief  This routine accesses mesh size data.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \returns VOID  : in all cases.

void
PAMPA_meshSize (
const PAMPA_Mesh * const  meshptr,               //!< Centralized mesh
PAMPA_Num * const         enttnbr,               //!< Number of entities
PAMPA_Num * const         vertptr)               //!< Number of vertices
{
  const Mesh *       srcmeshptr;
  Gnum enttnum, enttnnd;


  srcmeshptr = (Mesh *) meshptr;


  if (vertptr != NULL)
    *vertptr = (PAMPA_Num) (srcmeshptr->vertnbr);
  if (enttnbr != NULL)
    *enttnbr = (PAMPA_Num) (srcmeshptr->enttnbr);
}

//! \brief  This routine accesses mesh entity size data to a given entity number.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_meshEnttSize (
const PAMPA_Mesh * const  meshptr,               //!< Centralized mesh
PAMPA_Num const           enttnum,               //!< Entity number
PAMPA_Num * const         vertnbr)               //!< Number of vertices
{
  const Mesh *       srcmeshptr;

  srcmeshptr = (Mesh *) meshptr;

#ifdef PAMPA_DEBUG_MESH
  if ((enttnum < srcmeshptr->baseval) || (enttnum >= srcmeshptr->enttnbr + srcmeshptr->baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  if (vertnbr != NULL)
    *vertnbr = (PAMPA_Num) ((srcmeshptr->entttax[enttnum] == NULL) ? (0) : (srcmeshptr->entttax[enttnum]->vertnbr)); /* TRICK: if srcmeshptr is a part of a distributed mesh, an entity could not be present (as boundary edges) */

  return (0);
}

//! \brief  This routine accesses all of the mesh data.
//! NULL pointers on input indicate unwanted
//! data. NULL pointers on output indicate
//! unexisting arrays.
//!
//! \returns VOID  : in all cases.

void
PAMPA_meshData (
const PAMPA_Mesh * const   meshptr,              //!< Mesh structure to read
PAMPA_Num * const          baseptr,              //!< Base value            
PAMPA_Num * const          enttptr,              //!< Number of entities
PAMPA_Num * const          vertptr,              //!< Number of vertices
PAMPA_Num * const          edgeptr,              //!< Number of links
PAMPA_Num ** const         edgetab,              //!< Link array
PAMPA_Num * const          valuptr,              //!< Number of linked values
PAMPA_Num * const          valuptz)              //!< Maximum number of linked values
{
  const Mesh *       srcmeshptr;                 /* Pointer to source mesh structure */

  srcmeshptr = (const Mesh *) meshptr;

  if (baseptr != NULL)
    *baseptr = srcmeshptr->baseval;
  if (enttptr != NULL)
    *enttptr = srcmeshptr->enttnbr;
  if (vertptr != NULL)
    *vertptr = (PAMPA_Num) (srcmeshptr->vertnbr);
  if (edgeptr != NULL)
    *edgeptr = srcmeshptr->edgenbr;
  if (edgetab != NULL)
    *edgetab = srcmeshptr->edgetax + srcmeshptr->baseval;
  if (valuptr != NULL)
    *valuptr = srcmeshptr->valsptr->valunbr;
  if (valuptz != NULL)
    *valuptz = srcmeshptr->valsptr->valumax;
}

//! \brief This routine gives the number of neighors of a local vertex and his
//! global number.
//! Neighbors could belong to one entity or all entities with value -1.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int 
PAMPA_meshVertData (
const PAMPA_Mesh * const  meshptr,              //!< Distributed mesh structure to read
const PAMPA_Num            vertnum,              //!< Vertex number
const PAMPA_Num            ventnum,              //!< Entity number of vertex
const PAMPA_Num            nentnum,              //!< Entity number of neighbors
PAMPA_Num * const          nghbptr,           //!< Number of neighbors
PAMPA_Num * const          mvrtptr)           //!< Global number of vertex
{
  const Mesh *       srcmeshptr;                 /* Pointer to source mesh structure */
  MeshEnttNghb **    nghbtax;

  srcmeshptr = (const Mesh *) meshptr;
  nghbtax = srcmeshptr->entttax[ventnum]->nghbtax;

  if (nghbptr != NULL) {
    if (nentnum == -1) { // FIXME pourquoi -1, mettre plutôt un #define
      *nghbptr = nghbtax[srcmeshptr->enttnbr + srcmeshptr->baseval - 1]->vindtax[vertnum].vendidx -
        nghbtax[srcmeshptr->baseval]->vindtax[vertnum].vertidx;
    }
    else {
      *nghbptr = nghbtax[nentnum]->vindtax[vertnum].vendidx -
        nghbtax[nentnum]->vindtax[vertnum].vertidx;
    }
  }

  if (mvrtptr != NULL) 
    *mvrtptr = srcmeshptr->entttax[ventnum]->mvrttax[vertnum];


  return (0);
}

