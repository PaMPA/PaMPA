/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_values.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the distributed mesh
//!                values accessors.
//!
//!   \date        Version 1.0: from: 10 May 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define DMESH_VALUES

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

int dmeshValueLink (
    Dmesh * restrict const       meshptr,
    void **                      valuloctab,
	Gnum                         flagval,
    Gnum *                        sizeval,
    Gnum *                        typesiz,
    MPI_Datatype                 typeval,
    Gnum                         enttnum,
    Gnum                         tagnum)
{

  Gnum valulocnum;
#ifdef PAMPA_DEBUG_DMESH2
  Gnum reduloctab[2];
  Gnum reduglbtab[2];
#endif /* PAMPA_DEBUG_DMESH2 */
  Gnum vertnbr;
  Dvalues * valslocptr = meshptr->valslocptr;
  Dvalue *meshvalptr;
  MPI_Aint            dummy;
  MPI_Aint typesiz2;
  int cheklocval;

  cheklocval = 0;


  if (MPI_Type_get_extent (typeval, &dummy, &typesiz2)) {
    errorPrint ("get size error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if (typesiz != NULL)
  	*typesiz = (Gnum) typesiz2;

#ifdef PAMPA_DEBUG_DMESH2
  if ((enttnum < PAMPA_ENTT_VIRT_PROC) || (enttnum >= meshptr->enttglbnbr + meshptr->baseval)) {
    errorPrint("invalid entity number");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  if (tagnum == PAMPA_TAG_NIL) {
    errorPrint("invalid tag");
	cheklocval = 1;
  }

  reduloctab[0] = enttnum;
  reduloctab[1] = tagnum;
  if (commAllreduce (reduloctab, reduglbtab, 2, GNUM_MPI, MPI_MAX, meshptr->proccomm) != MPI_SUCCESS)
    errorPrint ("communication error");

  if (reduglbtab[0] != enttnum) {
    errorPrint ("Entity number is not the same on all the processors: %d and %d", enttnum, reduglbtab[0]);
    cheklocval = 1;
  }
  if (reduglbtab[1] != tagnum) {
    errorPrint ("Tag number is not the same on all the processors: %d and %d", tagnum, reduglbtab[1]);
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */

  valulocnum = 0;
  while ((valulocnum < valslocptr->valuglbmax) && 
      (valslocptr->valuloctab[valulocnum].tagnum != PAMPA_TAG_NIL)) {
#ifdef PAMPA_DEBUG_DMESH
  	if ((valslocptr->valuloctab[valulocnum].tagnum == tagnum) && (valslocptr->valuloctab[valulocnum].enttnum == enttnum)) {
          errorPrint("values with the same tag (" GNUMSTRING ") has already been added on this entity (" GNUMSTRING ")", tagnum, enttnum);
	cheklocval = 1;
  	}
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */
  	valulocnum++;
  }

#ifdef PAMPA_DEBUG_DMESH
  if (valulocnum >= valslocptr->valuglbmax) {
  	errorPrint("no more values could be linked");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  if ((enttnum < meshptr->baseval) && ((flagval & PAMPA_VALUE_PUBLIC) != 0)) {
  	errorPrint("could not link value on mesh with flag PAMPA_VALUE_PUBLIC");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

  switch (enttnum) {
    case PAMPA_ENTT_VIRT_VERT :
      vertnbr = meshptr->vertlocmax; // XXX n'est-ce pas plutot vertlocmax ??
      if (sizeval != NULL)
        *sizeval = meshptr->vertlocnbr; 
      break;
    case PAMPA_ENTT_VIRT_PROC :
      vertnbr = meshptr->procglbnbr + 1;
      if (sizeval != NULL)
        *sizeval = meshptr->procglbnbr + 1; 
      break;
    default :
#ifdef PAMPA_DEBUG_DMESH
      {
        if (meshptr->enttloctax[enttnum] == NULL) {
          errorPrint ("Invalid entity number");
          return (1);
        }
      }
#endif /* PAMPA_DEBUG_DMESH */
      if ((flagval & PAMPA_VALUE_PRIVATE) != 0) {
        vertnbr = meshptr->enttloctax[enttnum]->vertlocnbr; 
        if (sizeval != NULL)
          *sizeval = meshptr->enttloctax[enttnum]->vertlocnbr; 
      }
      else { /* PAMPA_VALUE_PUBLIC */
        vertnbr = meshptr->enttloctax[enttnum]->vgstlocnbr; 
        if (sizeval != NULL)
          *sizeval = meshptr->enttloctax[enttnum]->vgstlocnbr; 
      }
  }

  //printf ("val: %d, %d, %d, %lf, %lf, %lf\n", flagval & PAMPA_VALUE_ALLOCATED, flagval, PAMPA_VALUE_ALLOCATED, (*((double**) valuloctab))[0], (*((double**)valuloctab))[1], (*((double**)valuloctab))[1]);
  if ((flagval & PAMPA_VALUE_ALLOCATED) != 0)
    valslocptr->valuloctab[valulocnum].valuloctab = *valuloctab;
  else {
    if ((valslocptr->valuloctab[valulocnum].valuloctab =
          (byte *) memAlloc (vertnbr * ((size_t) typesiz2) * sizeof(byte))) == NULL) {
      errorPrint ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
    *valuloctab = valslocptr->valuloctab[valulocnum].valuloctab;
  }

  valslocptr->valuloctab[valulocnum].typeval = typeval;
  valslocptr->valuloctab[valulocnum].flagval = flagval;
  valslocptr->valuloctab[valulocnum].enttnum = enttnum;
  valslocptr->valuloctab[valulocnum].tagnum = tagnum;
  valslocptr->valuloctab[valulocnum].next = NULL;

  meshvalptr = valslocptr->evalloctak[enttnum];

  if (meshvalptr == NULL) {
  	valslocptr->evalloctak[enttnum] = &valslocptr->valuloctab[valulocnum];
  }
  else {
  	while (meshvalptr->next != NULL)
      meshvalptr = meshvalptr->next;

  	meshvalptr->next = &valslocptr->valuloctab[valulocnum];
  }

  valslocptr->valuglbnbr++;

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

int dmeshValueTagUnlink (
    Dmesh * restrict const       meshptr,
    Gnum                         tagnum)
{
  // TODO Check with valuglbmax if PAMPA_DEBUG_DMESH is set for example

  Gnum enttnum;
  Gnum enttnnd;
  Dvalues * valslocptr = meshptr->valslocptr;
  Dvalue *meshvalptr, *meshvalptz;
  int cheklocval;

  cheklocval = 0;
  
  for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = meshptr->enttglbnbr + meshptr->baseval;
      enttnum < enttnnd; enttnum ++) {

    meshvalptr = valslocptr->evalloctak[enttnum];
    meshvalptz = NULL;

    while (meshvalptr !=  NULL) {
      if (meshvalptr->tagnum == tagnum)
        break;
      meshvalptz = meshvalptr;
      meshvalptr = meshvalptr->next;
    }

    if ((meshvalptr != NULL) && (meshvalptr->tagnum == tagnum)) {
      if ((meshvalptr->flagval & PAMPA_VALUE_ALLOCATED) == 0)
        memFree (meshvalptr->valuloctab);

      if (meshvalptz == NULL) {
        valslocptr->evalloctak[enttnum] = meshvalptr->next;
      }
      else {
        meshvalptz->next = meshvalptr->next;
      }

      meshvalptr->tagnum = PAMPA_TAG_NIL;


      valslocptr->valuglbnbr--;
    }
  }

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}



int dmeshValueUnlink (
    Dmesh * restrict const       meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum)
{
  // TODO Check with valuglbmax if PAMPA_DEBUG_DMESH is set for example

  Dvalues * valslocptr = meshptr->valslocptr;
  Dvalue *meshvalptr, *meshvalptz;
  int cheklocval;

  cheklocval = 0;

#ifdef PAMPA_DEBUG_DMESH
  if ((enttnum < PAMPA_ENTT_VIRT_PROC) || (enttnum >= meshptr->enttglbnbr + meshptr->baseval)) {
    errorPrint("invalid entity number");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

  if (valslocptr->evalloctak[enttnum] == NULL) {
    errorPrint("no values are linked with these parameters");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  meshvalptr = valslocptr->evalloctak[enttnum];
  meshvalptz = NULL;

  while (meshvalptr !=  NULL) {
    if (meshvalptr->tagnum == tagnum)
      break;
    meshvalptz = meshvalptr;
    meshvalptr = meshvalptr->next;
  }

  if (meshvalptr == NULL) {
    errorPrint("no values are linked with these parameters");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if ((meshvalptr->flagval & PAMPA_VALUE_ALLOCATED) == 0)
    memFree (meshvalptr->valuloctab);

  if (meshvalptz == NULL) {
    valslocptr->evalloctak[enttnum] = meshvalptr->next;
  }
  else {
    meshvalptz->next = meshvalptr->next;
  }

  meshvalptr->tagnum = PAMPA_TAG_NIL;


  valslocptr->valuglbnbr--;

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}




// retourne 2 s'il n'y pas de valeur avec ce drapeau pour cette entité
int dmeshValueData (
    Dmesh * restrict const       meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                        sizeval,
    Gnum *                        typesiz,
    void **                      valuloctab)
{
  Dvalues * valslocptr = meshptr->valslocptr;
  Dvalue *           valuptr;
  int cheklocval;

  cheklocval = 0;

#ifdef PAMPA_DEBUG_DMESH
  if ((enttnum < PAMPA_ENTT_VIRT_PROC) || (enttnum >= meshptr->enttglbnbr + meshptr->baseval)) {
    errorPrint("invalid entity number");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH */

  cheklocval = dvalueData (meshptr->valslocptr, enttnum, tagnum, typesiz, &valuptr, valuloctab);
  CHECK_FERR ((cheklocval != 0) && (cheklocval != 2), meshptr->proccomm);

  if ((cheklocval == 0) && (sizeval != NULL)) {
    switch (enttnum) {
      case PAMPA_ENTT_VIRT_VERT :
        *sizeval = meshptr->vertlocnbr; 
        break;
      case PAMPA_ENTT_VIRT_PROC :
        *sizeval = meshptr->procglbnbr + 1; 
        break;
      default :
        if ((valuptr->flagval & PAMPA_VALUE_PRIVATE) != 0)
          *sizeval = meshptr->enttloctax[enttnum]->vertlocnbr; 
        else /* PAMPA_VALUE_PUBLIC */
          *sizeval = meshptr->enttloctax[enttnum]->vgstlocnbr; 
    }
  }
  
  return (cheklocval);
}
