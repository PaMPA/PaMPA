/* Copyright 2008,2015,2016 ENSEIRB, Inria & CNRS
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        common_file_compress.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the file (de)compression routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The type and structure definitions.
*/

/* Buffer size. */

#define FILECOMPRESSDATASIZE        (128 * 1024) /* Size of (un)compressing buffers */

/* Available types of (un)compression. */

typedef enum FileCompressType_ {
  FILECOMPRESSTYPENOTIMPL = -1,                   /* Error code     */
  FILECOMPRESSTYPENONE,                           /* No compression */
  FILECOMPRESSTYPEBZ2,
  FILECOMPRESSTYPEGZ,
  FILECOMPRESSTYPELZMA
} FileCompressType;

/* (Un)compression type slot. */

typedef struct FileCompressTab_ {
  char *                    name;                 /* File extension name  */
  FileCompressType          type;                 /* (Un)compression type */
} FileCompressTab;

/*
**  The type and structure definitions.
*/

typedef struct FileCompressData_ {
  int                       typeval;              /*+ Type of (un)compression      +*/
  int                       innerfd;              /*+ Inner file handle (pipe end) +*/
  FILE *                    outerstream;          /*+ Outer stream                 +*/
  double                    datatab;              /*+ Start of data buffer         +*/
} FileCompressData;

/*
**  The function prototypes.
*/

#ifdef COMMON_FILE_COMPRESS_BZ2
#ifdef COMMON_FILE_COMPRESS
static void                 fileCompressBz2     (FileCompressData * const  dataptr);
#endif /* COMMON_FILE_COMPRESS */
#ifdef COMMON_FILE_UNCOMPRESS
static void                 fileUncompressBz2   (FileCompressData * const  dataptr);
#endif /* COMMON_FILE_UNCOMPRESS */
#endif /* COMMON_FILE_COMPRESS_Bz2 */
#ifdef COMMON_FILE_COMPRESS_GZ
#ifdef COMMON_FILE_COMPRESS
static void                 fileCompressGz      (FileCompressData * const  dataptr);
#endif /* COMMON_FILE_COMPRESS */
#ifdef COMMON_FILE_UNCOMPRESS
static void                 fileUncompressGz    (FileCompressData * const  dataptr);
#endif /* COMMON_FILE_UNCOMPRESS */
#endif /* COMMON_FILE_COMPRESS_GZ */
#ifdef COMMON_FILE_COMPRESS_LZMA
/* #ifdef COMMON_FILE_COMPRESS */
/* static void                 fileCompressLzma    (FileCompressData * const  dataptr); */
/* #endif /\* COMMON_FILE_COMPRESS *\/ */
#ifdef COMMON_FILE_UNCOMPRESS
static void                 fileUncompressLzma  (FileCompressData * const  dataptr);
#endif /* COMMON_FILE_UNCOMPRESS */
#endif /* COMMON_FILE_COMPRESS_LZMA */
