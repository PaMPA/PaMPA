/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dvalues.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file contains the distributed value
//!                handling routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define DVALUES

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* values.                           */
/*                                   */
/*************************************/

//! This routine initializes a distributed values
//! structure. XXX
//!
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
dvaluesInit (
    Dvalues * restrict * const    valslocptr,              //!< Distributed values structure
    Gnum                          baseval,
    Gnum                          valuglbmax,
    Gnum                          enttglbnbr)
{
  Gnum valulocnum;
  int cheklocval;

  cheklocval = 0;

  if ((*valslocptr = (Dvalues *) memAlloc (sizeof (Dvalues))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  // XXX il faut mettre le proccomm soit comme paramètre soit dans Dvalues
  // pour qu'on puisse tester cheklocval avec CHECK_VDBG qui prend comme
  // argument un communicateur
  memSet (*valslocptr, 0, sizeof (Dvalues));            /* Clear values fields */
  (*valslocptr)->cuntlocnbr = 1;

  if (memAllocGroup ((void **) (void *)
                     &(*valslocptr)->valuloctab,       (size_t) (valuglbmax * sizeof (Dvalue)),
                     &(*valslocptr)->evalloctak, (size_t) ((enttglbnbr - PAMPA_ENTT_VIRT_PROC + baseval) * sizeof (Dvalue *)),
					 (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  // XXX il faut mettre le proccomm soit comme paramètre soit dans Dvalues
  // pour qu'on puisse tester cheklocval avec CHECK_VDBG qui prend comme
  // argument un communicateur
  (*valslocptr)->evalloctak -= PAMPA_ENTT_VIRT_PROC;

  memSet ((*valslocptr)->evalloctak + PAMPA_ENTT_VIRT_PROC, 0, (enttglbnbr - PAMPA_ENTT_VIRT_PROC + baseval) * sizeof (Dvalue *));            /* Clear values fields */
  memSet ((*valslocptr)->valuloctab, 0, valuglbmax * sizeof (Dvalue));            /* Clear values fields */

  (*valslocptr)->valuglbmax = valuglbmax;
  (*valslocptr)->valuglbnbr = 0;
  for (valulocnum = 0 ; valulocnum < valuglbmax ; valulocnum ++) {
    (*valslocptr)->valuloctab[valulocnum].tagnum = PAMPA_TAG_NIL;
  }

  return (cheklocval); // XXX à changer une fois qu'on aura le communicateur
}


void
dvaluesFree (
    Dvalues * restrict const     valsptr)					//!< Distributed values structure
{

  if (valsptr->cuntlocnbr == 1) { /* If associated value arrays must be freed */


    if (valsptr->valuloctab != NULL) {
	  Gnum valulocnum;
  	  Dvalue * valuloctab;

      valuloctab = valsptr->valuloctab;

      for (valulocnum = 0 ; valulocnum < valsptr->valuglbmax ; valulocnum++) {
        //! \bug Invalid read of size 1 (Vincent)
        //! \bug Invalid free (Vincent)
        if ((valuloctab[valulocnum].tagnum != PAMPA_TAG_NIL) && (valuloctab[valulocnum].valuloctab != NULL))
          memFree (valuloctab[valulocnum].valuloctab);
      }
    }
    memFreeGroup (valsptr->valuloctab);
  	memFree (valsptr);
  }
  else
	valsptr->cuntlocnbr --;
}

// retourne 2 s'il n'y pas de valeur avec ce drapeau pour cette entité
int dvalueData (
    Dvalues * restrict const       valslocptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                        typesiz,
	Dvalue **                    valulocptr,
    void **                      valuloctab)
{
  Dvalue * valuptr;
  MPI_Aint            dummy;
  MPI_Aint typesiz2;
  int cheklocval;

  cheklocval = 0;

  valuptr = valslocptr->evalloctak[enttnum];

  while (valuptr !=  NULL) {
    if (valuptr->tagnum == tagnum)
      break;
    valuptr = valuptr->next;
  }

  if (valulocptr != NULL)
	*valulocptr = valuptr;

  if (valuptr == NULL)
	return (2); // XXX voir si on n'a pas besoin de faire un reduce sur tous les procs

  if (valuloctab != NULL)
    *valuloctab = valuptr->valuloctab;
  
  if (MPI_Type_get_extent (valuptr->typeval, &dummy, &typesiz2)) {
    errorPrint ("get size error");
	cheklocval = 1;
  }
  // XXX il faut mettre le proccomm soit comme paramètre soit dans Dvalues
  // pour qu'on puisse tester cheklocval avec CHECK_VDBG qui prend comme
  // argument un communicateur
  //CHECK_VERR (cheklocval, meshptr->proccomm);
  if (typesiz != NULL)
  	*typesiz = (Gnum) typesiz2;

  // XXX il faut mettre le proccomm soit comme paramètre soit dans Dvalues
  // pour qu'on puisse tester cheklocval avec CHECK_VDBG qui prend comme
  // argument un communicateur
  //CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (cheklocval); // XXX à changer une fois qu'on aura le communicateur
}
