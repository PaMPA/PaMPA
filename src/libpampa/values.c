/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        values.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the value handling
//!                routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define VALUES

#include "module.h"
#include "common.h"
#include "values.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* values.                           */
/*                                   */
/*************************************/

//! This routine initializes a distributed values
//! structure. XXX
//!
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
valuesInit (
    Values * restrict * const    valsptr,              //!< Distributed values structure
    Gnum                          baseval,
    Gnum                          valumax,
    Gnum                          enttnbr)
{
  Gnum valunum;

  if ((*valsptr = (Values *) memAlloc (sizeof (Values))) == NULL) {
    errorPrint  ("out of memory");
	return (1);
  }
  memSet (*valsptr, 0, sizeof (Values));            /* Clear values fields */
  (*valsptr)->cuntnbr = 1;

  if (memAllocGroup ((void **) (void *)
                     &(*valsptr)->valutab,       (size_t) (valumax * sizeof (Value)),
                     &(*valsptr)->evaltak, (size_t) ((enttnbr - PAMPA_ENTT_VIRT_PROC + baseval) * sizeof (Value *)),
					 (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
	return (1);
  }
  (*valsptr)->evaltak -= PAMPA_ENTT_VIRT_PROC;

  memSet ((*valsptr)->evaltak + PAMPA_ENTT_VIRT_PROC, 0, (enttnbr - PAMPA_ENTT_VIRT_PROC + baseval) * sizeof (Value *));            /* Clear values fields */
  memSet ((*valsptr)->valutab, 0, valumax * sizeof (Value));            /* Clear values fields */

  (*valsptr)->valumax = valumax;
  (*valsptr)->valunbr = 0;
  for (valunum = 0 ; valunum < valumax ; valunum ++) {
    (*valsptr)->valutab[valunum].tagnum = PAMPA_TAG_NIL;
  }

  return (0);
}


void
valuesFree (
    Values * restrict const     valsptr)					//!< Distributed values structure
{

  if (valsptr->cuntnbr == 1) { /* If associated value arrays must be freed */


    if (valsptr->valutab != NULL) {
	  Gnum valunum;
  	  Value * valutab;

      valutab = valsptr->valutab;

      for (valunum = 0 ; valunum < valsptr->valunbr ; valunum++) {
        //! \bug Invalid read of size 1 (Vincent)
        //! \bug Invalid free (Vincent)
        if ((valutab[valunum].tagnum != PAMPA_TAG_NIL) && (valutab[valunum].valutab != NULL))
          memFree (valutab[valunum].valutab);
      }
    }
    memFreeGroup (valsptr->valutab);
  	memFree (valsptr);
  }
  else
	valsptr->cuntnbr --;
}

// retourne 2 s'il n'y pas de valeur avec ce drapeau pour cette entité
int
valueData (
Values * const          valsptr,
Gnum const              enttnum,                           //!< Entity number
Gnum                    tagnum,                            //!< Tag value
Gnum *					     typesiz,
void **                      valutab)                           //!< Associated values
{
  Value *           valuptr;
  MPI_Aint            dummy;
  MPI_Aint typesiz2;

  valuptr = valsptr->evaltak[enttnum];

  while (valuptr !=  NULL) {
    if (valuptr->tagnum == tagnum)
      break;
    valuptr = valuptr->next;
  }

  if (valuptr == NULL)
	return (2); // XXX voir si on n'a pas besoin de faire un reduce sur tous les procs

  if (valutab != NULL)
    *valutab = valuptr->valutab;

  if (MPI_Type_get_extent (valuptr->typeval, &dummy, &typesiz2)) {
    errorPrint ("get size error");
	return (1);
  }
  if (typesiz != NULL)
  	*typesiz = (Gnum) typesiz2;

  return (0);
}

