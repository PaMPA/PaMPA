/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_grow.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 20 Dec 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include <pampa.h>
#include <pampa.h>

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
dmeshGrow (
Dmesh * const              dmshptr,             //!< PaMPA distributed mesh
Gnum                       ventlocnum,
Gnum *                     vtaggsttax,
Gnum                       seedglbnbr,
Gnum                       nentlocnum,
Gnum                       bandval)
{
  Gnum baseval;
  Gnum vertlocmax;
  Gnum nghblocmax;
  Gnum vertlocinc;
  Gnum vertlocnnd;
  Gnum vertlocidx;
  Gnum vertlocnum;
  Gnum vertlocsiz;
  Gnum bandnum;
  PAMPA_Iterator iterdat;
  PAMPA_Iterator iterdt2;
  Gnum * restrict vnumloctab;
  Gnum * restrict ntaggsttax;
#ifdef PAMPA_NOT_REF_ONE
  Gnum * restrict erefgsttax;
#endif /* PAMPA_NOT_REF_ONE */
#ifdef PAMPA_DEBUG_DMESH_SAVE2
  static int countval = 0;
  char s1[50];
  Gnum meshlocnbr;
  Gnum seedglbnum;
  Gnum seedglbnnd;
  Gnum * vnumloctax;
  Gnum * vnumtab;
  Gnum * parttax;
  Mesh * imshloctab;
  Gnum data[2];
  data[0] = 0; // entity number for main entity
  data[1] = 1; // entity number for nodes
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
  int cheklocval;

  cheklocval = 0;
  baseval = dmshptr->baseval;

  vertlocmax = dmshptr->enttloctax[ventlocnum]->vertlocnbr + baseval - 1;
  nghblocmax = dmshptr->enttloctax[nentlocnum]->vertlocnbr + baseval - 1;

#ifdef PAMPA_NOT_REF_ONE
  CHECK_FERR(dmeshValueData(dmshptr, baseval, PAMPA_TAG_REF, NULL, NULL, (void **) &erefgsttax), dmshptr->proccomm);
  erefgsttax -= baseval;
  CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[baseval], erefgsttax + baseval, GNUM_MPI), dmshptr->proccomm);
#endif /* PAMPA_NOT_REF_ONE */


  if (memAllocGroup ((void **) (void *)
        &ntaggsttax, (size_t) (dmshptr->enttloctax[nentlocnum]->vgstlocnbr * sizeof (Gnum)),
#ifdef PAMPA_DEBUG_DMESH_SAVE2
        &parttax,    (size_t) (seedglbnbr                                  * sizeof (Gnum)),
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  ntaggsttax -= baseval;
#ifdef PAMPA_DEBUG_DMESH_SAVE2
  parttax -= baseval;
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

  memSet (ntaggsttax + baseval, ~0, dmshptr->enttloctax[nentlocnum]->vgstlocnbr * sizeof (Gnum));

  // XXX il faut l'enregistrement par PaMPA
#ifdef PAMPA_DEBUG_DMESH_SAVE2
  /* Save centralized mesh to debug */
  CHECK_FERR (dmeshValueLink (dmshptr, (void **) &vnumloctax, PAMPA_VALUE_PRIVATE, NULL, NULL, GNUM_MPI, baseval, 42), dmshptr->proccomm);
  vnumloctax -= baseval;
  for (vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
    vnumloctax[vertlocnum] = dmshptr->proclocnum;

  if (dmshptr->proclocnum == 0) {
    PAMPA_Mesh cmshdat;

    sprintf (s1, "dmsh-grow-%d.mesh", countval);
    // Initialisation of PaMPA mesh structure
    CHECK_FERR(PAMPA_meshInit(&cmshdat), dmshptr->proccomm);

    CHECK_FERR(PAMPA_dmeshGather (dmshptr, &cmshdat), dmshptr->proccomm);

    CHECK_FERR (meshValueData ((Mesh *) &cmshdat, baseval, 42, NULL, NULL, (void **) &vnumtab), dmshptr->proccomm);
    CHECK_FERR(PAMPA_MMG3D4_meshSave(&cmshdat, 0, data, vnumtab, s1), dmshptr->proccomm);

    // Finalisation of PaMPA mesh structure
    PAMPA_meshExit(&cmshdat);
  }
  else {
    // Gather PaMPA mesh
    CHECK_FERR(PAMPA_dmeshGather (dmshptr, NULL), dmshptr->proccomm);
  }
  CHECK_FERR (dmeshValueUnlink (dmshptr, baseval, 42), dmshptr->proccomm);
  for (seedglbnum = baseval, seedglbnnd = seedglbnbr + baseval; seedglbnum < seedglbnnd; seedglbnum ++)
    parttax[seedglbnum] = seedglbnum % dmshptr->procglbnbr; // XXX pas très équilibré :(

#endif /* PAMPA_DEBUG_DMESH_SAVE2 */


  /* Grow seeds on a band defined by bandval */
  for (bandnum = 0; bandnum < bandval; bandnum ++) {
    Gnum vertlocid2;

  // XXX il faut l'enregistrement par PaMPA
#ifdef PAMPA_DEBUG_DMESH_SAVE2
    /* Save growing seeds */
    Gnum meshlocnum;
    imshloctab = NULL;
    meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
    CHECK_FERR (dmeshGatherInduceMultiple (dmshptr, 0, seedglbnbr, vtaggsttax, parttax, &meshlocnbr, &imshloctab), dmshptr->proccomm);
    for (meshlocnum = 0; meshlocnum < meshlocnbr; meshlocnum ++) {
      sprintf (s1, "dmsh-grow-%d-%d-%d-%d.mesh", countval, bandnum, dmshptr->proclocnum, meshlocnum);
      CHECK_FERR (PAMPA_MMG3D4_meshSave((PAMPA_Mesh *) (imshloctab) +meshlocnum, 1, data, NULL, s1), dmshptr->proccomm);
      meshExit (imshloctab + meshlocnum);
  }

    memFree (imshloctab);
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

    CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[ventlocnum], vtaggsttax + baseval, GNUM_MPI), dmshptr->proccomm);

    PAMPA_dmeshItInitStart ((PAMPA_Dmesh *) dmshptr, nentlocnum, PAMPA_VERT_LOCAL, &iterdat);
    PAMPA_dmeshItInit((PAMPA_Dmesh *) dmshptr, nentlocnum, ventlocnum, &iterdt2);
    /* boucle de niveau 1 sur tous les nœuds reliés à un sommet distant */
    while (PAMPA_itHasMore (&iterdat)) {
      PAMPA_Num nghblocnum;

      nghblocnum = PAMPA_itCurEnttVertNum(&iterdat);
      if (ntaggsttax[nghblocnum] == ~0) {
        PAMPA_itStart(&iterdt2, nghblocnum);
        /* boucle de niveau 2 sur les éléments voisins du nœud nghblocnum */
        while (PAMPA_itHasMore (&iterdt2)) {
          PAMPA_Num vertlocnum;

          vertlocnum = PAMPA_itCurEnttVertNum(&iterdt2);
          /* si l'élément est local et qu'il a le drapeau */
#ifdef PAMPA_NOT_REF_ONE
          if ((vtaggsttax[vertlocnum] != ~0) && (erefgsttax[vertlocnum] != PAMPA_REF_IS)) {
#else  /* PAMPA_NOT_REF_ONE */
          if (vtaggsttax[vertlocnum] != ~0) {
#endif /* PAMPA_NOT_REF_ONE */
            ntaggsttax[nghblocnum] = vtaggsttax[vertlocnum];
            break;
          }
          PAMPA_itNext(&iterdt2);
        }
      }

      PAMPA_itNext(&iterdat);
    }
    CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[nentlocnum], ntaggsttax + baseval, GNUM_MPI), dmshptr->proccomm);


    PAMPA_dmeshItInitStart ((PAMPA_Dmesh *) dmshptr, ventlocnum, PAMPA_VERT_LOCAL, &iterdat);
    PAMPA_dmeshItInit((PAMPA_Dmesh *) dmshptr, ventlocnum, nentlocnum, &iterdt2);
    /* boucle de niveau 1 sur tous les éléments reliés à un sommet distant */
    while (PAMPA_itHasMore (&iterdat)) {
      PAMPA_Num vertlocnum;

      vertlocnum = PAMPA_itCurEnttVertNum(&iterdat);
      if (vtaggsttax[vertlocnum] == ~0) {
        PAMPA_itStart(&iterdt2, vertlocnum);
        /* boucle de niveau 2 sur les nœuds voisins de l'élément vertlocnum */
        while (PAMPA_itHasMore (&iterdt2)) {
          PAMPA_Num nghblocnum;

          nghblocnum = PAMPA_itCurEnttVertNum(&iterdt2);
          /* si le nœud est local et qu'il a le drapeau */
          if (ntaggsttax[nghblocnum] != ~0) {
            vtaggsttax[vertlocnum] = ntaggsttax[nghblocnum];
            break;
          }
          PAMPA_itNext(&iterdt2);
        }
      }

      PAMPA_itNext(&iterdat);
    }
  }
  CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[ventlocnum], vtaggsttax + baseval, GNUM_MPI), dmshptr->proccomm);
  memFreeGroup (ntaggsttax + baseval);

  // XXX il faut l'enregistrement par PaMPA
#ifdef PAMPA_DEBUG_DMESH_SAVE2
  /* save growed seeds on dmesh */
  Gnum meshlocnum;
  imshloctab = NULL;
  meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
  CHECK_FERR (dmeshGatherInduceMultiple (dmshptr, 0, seedglbnbr, vtaggsttax, parttax, &meshlocnbr, &imshloctab), dmshptr->proccomm);
  for (meshlocnum = 0; meshlocnum < meshlocnbr; meshlocnum ++) {
    sprintf (s1, "dmsh-grow-%d-%d-%d-%d.mesh", countval, bandnum, dmshptr->proclocnum, meshlocnum);
    CHECK_FERR (PAMPA_MMG3D4_meshSave((PAMPA_Mesh *) (imshloctab) +meshlocnum, 1, data, NULL, s1), dmshptr->proccomm);
    meshExit (imshloctab + meshlocnum);
  }
  countval ++;
  memFree (imshloctab);
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  return (0);
}

