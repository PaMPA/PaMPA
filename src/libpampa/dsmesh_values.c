/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dsmesh_values.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This file contains the distributed
//!                simplified mesh value accessors.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define DSMESH_VALUES

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dsmesh.h"
#include "pampa.h"

// retourne 2 s'il n'y pas de valeur avec ce drapeau pour cette entité
int dsmeshValueData (
    Dsmesh * restrict const       smshptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                        sizeval,
    Gnum *                        typesiz,
    void **                      valuloctab)
{
  Dvalues * valslocptr = smshptr->valslocptr;
  Dvalue *           valuptr;
  MPI_Aint            dummy;
  MPI_Aint typesiz2;
  int cheklocval;

  cheklocval = 0;

#ifdef PAMPA_DEBUG_DSMESH
  if ((enttnum < smshptr->baseval - 1) || (enttnum >= smshptr->enttglbnbr + smshptr->baseval)) {
    errorPrint("invalid entity number");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, smshptr->proccomm);
#endif /* PAMPA_DEBUG_DSMESH */

  CHECK_FERR (dvalueData (smshptr->valslocptr, enttnum, tagnum, typesiz, &valuptr, valuloctab), smshptr->proccomm);

  if (sizeval != NULL) {
  	if (enttnum == smshptr->baseval - 1) 
  	  *sizeval = smshptr->vertlocnbr; 
  	else if (valuptr->flagval == PAMPA_VALUE_PRIVATE)
  	  *sizeval = 0; // FIXME *sizeval = smshptr->enttloctax[enttnum]->vertlocnbr; 
  	else /* PAMPA_VALUE_PUBLIC */
  	  *sizeval = 0; // FIXME *sizeval = smshptr->enttloctax[enttnum]->vgstlocnbr; 
  }
  
  return (0);
}
