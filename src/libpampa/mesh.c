/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module handles the source mesh
//!                functions. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* mesh meshs.                      */
/*                                   */
/*************************************/

//! This routine initializes a distributed mesh
//! structure. In order to avoid collective
//! communication whenever possible, the allocation
//! of send and receive index arrays is not performed
//! in the routine itself, but rather delegated to
//! subsequent routines such as meshBuild.
//! However, these arrays will not be freed by
//! meshFree, but by meshExit.
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshInit (
    Mesh * restrict const       meshptr)              //!< Mesh structure
{
  memSet (meshptr, 0, sizeof (Mesh));            /* Clear public and private mesh fields */


  return (0);
}

//! This routine frees the public data of the given
//! distributed mesh, but not its private data.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! It returns:
//! - VOID  : in all cases.

void
meshFree (
    Mesh * const     meshptr)
{
  MeshEntity ** entttax;
  Gnum enttnum, nentlocnum;
  Gnum baseval;
  Gnum enttnnd;

  baseval = meshptr->baseval;

  if ((meshptr->flagval & MESHFREETABS) != 0) { /* If local arrays must be freed */
  }


  if ((meshptr->flagval & MESHFREEENTT) != 0) { /* If internal arrays must be freed */
    if (meshptr->entttax != NULL) {
      entttax = meshptr->entttax;
      for (enttnum = baseval, enttnnd = meshptr->enttnbr + baseval ; enttnum < enttnnd ; enttnum ++) {

        if (entttax[enttnum] != NULL) {
          MeshEnttNghb ** nghbtax;
          Gnum ngbenttnnd;

          if (meshptr->esubbax[enttnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) {
            nghbtax = entttax[enttnum]->nghbtax;

            for (nentlocnum = baseval, ngbenttnnd = meshptr->enttnbr + baseval ; nentlocnum < ngbenttnnd ; nentlocnum ++) {
              if (nghbtax[nentlocnum] != NULL) {
                if ((meshptr->esubbax[nentlocnum & meshptr->esubmsk] <= PAMPA_ENTT_SINGLE) && (nghbtax[nentlocnum]->vindtax != NULL)) // TRICK: verttax and vendtax are allocated together
                  memFreeGroup (nghbtax[nentlocnum]->vindtax+baseval);

                memFree (nghbtax[nentlocnum]);
              }
            }

            memFreeGroup (entttax[enttnum]->nghbtax + baseval);
          }
          else if (entttax[enttnum]->venttax != NULL)
            memFreeGroup (entttax[enttnum]->venttax + baseval);

          if (entttax[enttnum]->vprmtax != NULL)
            memFree (entttax[enttnum]->vprmtax + baseval);

          memFree (entttax[enttnum]);

        }
      }
      memFreeGroup (meshptr->entttax + baseval);
    }

    if ((meshptr->esubmsk != 0) && (meshptr->esubbax != NULL))
      memFree (meshptr->esubbax + baseval);
  }

  // XXX : gérer proccomm avec FREECOMM



  // XXX : à vérifier quand les données associées seront dispos
  // surtout si les drapeaux sont correctement utilisés
  valuesFree (meshptr->valsptr);
}


//! This routine destroys a distributed mesh structure.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! Private data are always destroyed. If this is not
//! wanted, use meshFree() instead.
//! It returns:
//! - VOID  : in all cases.

void
meshExit (
    Mesh * restrict const     meshptr)
{
  meshFree (meshptr);


#ifdef PAMPA_DEBUG_MESH1
  memSet (meshptr, ~0, sizeof (Mesh));
#endif /* PAMPA_DEBUG_MESH1 */
}
