/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        module.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This is the global configuration file for
//!                the whole libPampa library module. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

#define MODULE_H

/*
** Version string.
*/

#define PAMPA_VERSION_STRING       PAMPA_VERSION_STRING2(PAMPA_VERSION) "." PAMPA_VERSION_STRING2(PAMPA_RELEASE) "." PAMPA_VERSION_STRING2(PAMPA_PATCHLEVEL)
#define PAMPA_VERSION_STRING2(x)   PAMPA_VERSION_STRING3(x)
#define PAMPA_VERSION_STRING3(x)   #x


/*
** Debug values.
*/

// XXX réordonner les drapeaux par catégorie
// peut-être en faire une PAMPA_DGRAPH_SAVE
// et PAMPA_DEBUG_NO_HALO où le met-on ?
#ifdef PAMPA_DEBUG_EXTRA
#ifndef PAMPA_DEBUG_ALL
#define PAMPA_DEBUG_ALL
#endif /* PAMPA_DEBUG_ALL */

#define PAMPA_DEBUG_MEM
#define PAMPA_DEBUG_DMESH3
#endif /* PAMPA_DEBUG_EXTRA */

#ifdef PAMPA_DEBUG_ALL
#ifndef PAMPA_DEBUG
#define PAMPA_DEBUG
#endif /* PAMPA_DEBUG */

#define PAMPA_DEBUG_CHECK
#define PAMPA_DEBUG_DMESH2
#define PAMPA_DEBUG_MESH2
#define PAMPA_DEBUG_ITER2
#endif /* PAMPA_DEBUG_ALL */

#ifdef PAMPA_DEBUG
#define PAMPA_DEBUG_ADAPT
#define PAMPA_DEBUG_MESH
#define PAMPA_DEBUG_SMESH
#define PAMPA_DEBUG_DMESH
#define PAMPA_DEBUG_DSMESH
#define PAMPA_DEBUG_DMESH_DGRAPH
#define PAMPA_DEBUG_DMESH_GRAPH
#define PAMPA_DEBUG_LIBRARY1
//#define PAMPA_DEBUG_ITER // XXX resoudre le pb de ce drapeau qui doit etre ajoute dans pampa et dans l'appli pour que ca marche :(
#endif /* PAMPA_DEBUG */

#ifdef PAMPA_DEBUG_SAVE_ALL
#ifndef PAMPA_DEBUG_SAVE
#define PAMPA_DEBUG_SAVE
#endif /* PAMPA_DEBUG_SAVE */

#define PAMPA_DEBUG_DMESH_SAVE2
#endif /* PAMPA_DEBUG_SAVE_ALL */

#ifdef PAMPA_DEBUG_SAVE
#define PAMPA_DEBUG_GRAPH
#define PAMPA_DEBUG_DGRAPH
#define PAMPA_DEBUG_DMESH_SAVE
#define PAMPA_DEBUG_MESH_SAVE
#endif /* PAMPA_DEBUG_SAVE */

#ifdef PAMPAF_DEBUG_EXTRA
#ifndef PAMPAF_DEBUG_ALL
#define PAMPAF_DEBUG_ALL
#endif /* PAMPAF_DEBUG_ALL */

#endif /* PAMPAF_DEBUG_EXTRA */

#ifdef PAMPAF_DEBUG_ALL
#ifndef PAMPAF_DEBUG
#define PAMPAF_DEBUG
#endif /* PAMPAF_DEBUG */

#endif /* PAMPAF_DEBUG_ALL */

#ifdef PAMPAF_DEBUG
#define PAMPAF_DEBUG_ITER
#endif /* PAMPAF_DEBUG */




/*
 ** Macro checking
 */

#ifdef PAMPA_DEBUG_CHECK
#define CHECK_FERR(inst,proccomm) CHECK_FERR2(inst, proccomm, __FUNCTION__, __LINE__)
#define CHECK_FERR_STR(inst,str,proccomm) CHECK_FERR2(inst, proccomm, __FUNCTION__, __LINE__)
#define CHECK_FMPI(chekval,inst,proccomm) CHECK_FMPI2(chekval, inst, proccomm, __FUNCTION__, __LINE__)
#define INIT_TIME(t) ;
#define START_TIME(t) ;
#define END_TIME(t) ;
#else /* PAMPA_DEBUG_CHECK */
#ifdef PAMPA_TIME_CHECK
#define CHECK_FERR(inst,proccomm) CHECK_FDBG4(inst, __FUNCTION__, __LINE__)
#define CHECK_FERR_STR(inst,str,proccomm) CHECK_FDBG5(inst, (0),  __FUNCTION__, (str), __LINE__)
#define CHECK_FMPI(chekval, inst,proccomm) CHECK_FMPI3(chekval, inst, proccomm, __FUNCTION__, __LINE__)
#define INIT_TIME(t) \
  time_t t;
#define START_TIME(t) \
  t = time (NULL); 

#define END_TIME(t) \
  infoPrint ("Elapsed time: %lu secondes", (time(NULL) - t)); 
#else /* PAMPA_TIME_CHECK */
#define CHECK_FERR(inst,proccomm) CHECK_FDBG3(inst, (0), __FUNCTION__, __LINE__)
#define CHECK_FERR_STR(inst,str,proccomm) CHECK_FDBG3(inst, (0), __FUNCTION__, __LINE__)
#define CHECK_FMPI(chekval, inst,proccomm) CHECK_FMPI2(chekval, inst, proccomm, __FUNCTION__, __LINE__)
#define INIT_TIME(t) ;
#define START_TIME(t) ;
#define END_TIME(t) ;
#endif /* PAMPA_TIME_CHECK */
#endif /* PAMPA_DEBUG_CHECK */

#define CHECK_FMPI2(chekval, inst,proccomm,fun,line) { \
  CHECK_VDBG (chekval, proccomm); \
  CHECK_FDBG3(inst, (MPI_SUCCESS), fun, line); \
}

#define CHECK_FMPI3(chekval, inst,proccomm,fun,line) { \
  CHECK_VDBG (chekval, proccomm); \
  CHECK_FDBG5(inst, (MPI_SUCCESS), fun, "MPI", line); \
}

#define CHECK_FERR2(inst,proccomm,fun,line) { \
  int cheklocvl2; \
  int chekglbvl2; \
  cheklocvl2 = (inst); \
  if (cheklocvl2 != 0) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), cheklocvl2, (line)); \
  	if (MPI_Allreduce (&(cheklocvl2), &(chekglbvl2), 1, MPI_INT, MPI_MAX, (proccomm)) != MPI_SUCCESS) { \
      printf ("communication error"); \
      return     (-1); \
  	} \
  	if ((chekglbvl2) != 0) { \
  	  if (cheklocvl2 != 0) \
	    PAMPA_errorPrint ("%s: error code %d at line %d", (fun), chekglbvl2, (line)); \
  	  return ((chekglbvl2)); \
  	} \
  } \
}

#define CHECK_DUMMY3() CHECK_DUMMY4(__FUNCTION__, __LINE__) 
#ifdef PAMPA_DEBUG_CHECK
#define CHECK_DUMMY() {} // CHECK_DUMMY2(__FUNCTION__, __LINE__) 
#define CHECK_FDBG(inst,proccomm) CHECK_FUNC2(inst, proccomm, __FUNCTION__, __LINE__)
#else /* PAMPA_DEBUG_CHECK */
#define CHECK_DUMMY() {}
#define CHECK_FDBG(inst,proccomm) CHECK_FDBG3(inst, (0), __FUNCTION__, __LINE__)
#endif /* PAMPA_DEBUG_CHECK */
#define CHECK_FDBG2_NOTIM(inst) CHECK_FDBG3(inst, (0), __FUNCTION__, __LINE__)
#ifdef PAMPA_TIME_CHECK
#define CHECK_FDBG2(inst) CHECK_FDBG4(inst, __FUNCTION__, __LINE__)
#else /* PAMPA_TIME_CHECK */
#define CHECK_FDBG2(inst) CHECK_FDBG3(inst, (0), __FUNCTION__, __LINE__)
#endif /* PAMPA_TIME_CHECK */

#define CHECK_DUMMY2(fun,line) { \
int rank; \
MPI_Comm_rank (MPI_COMM_WORLD, &rank); \
if (rank == 0) \
infoPrint ("je suis %s, avant la barriere a la ligne %d", (fun), (line)); \
	MPI_Barrier (MPI_COMM_WORLD); \
if (rank == 0) \
infoPrint ("je suis %s, apres la barriere a la ligne %d", (fun), (line)); \
}

#define CHECK_DUMMY4(fun,line) { \
infoPrint ("je suis %s, avant la barriere a la ligne %d", (fun), (line)); \
	MPI_Barrier (MPI_COMM_WORLD); \
infoPrint ("je suis %s, apres la barriere a la ligne %d", (fun), (line)); \
}

#define CHECK_FDBG3(inst,val,fun,line) { \
  int cheklocvl2; \
  cheklocvl2 = (inst); \
  if ((cheklocvl2) != (val)) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), cheklocvl2, (line)); \
  	return ((cheklocvl2)); \
  } \
}

#define CHECK_FDBG4(inst,fun,line) { \
  int cheklocvl2; \
  time_t begin = time (NULL); \
  cheklocvl2 = (inst); \
  PAMPA_infoPrint("%s: Elapsed time %lu secondes, line %d", (fun), (time(NULL) - begin), (line)); \
  if ((cheklocvl2) != 0) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), cheklocvl2, (line)); \
  	return ((cheklocvl2)); \
  } \
}

#define CHECK_FDBG5(inst,val,fun,str,line) { \
  int cheklocvl2; \
  time_t begin = time (NULL); \
  cheklocvl2 = (inst); \
  PAMPA_infoPrint("%s: Elapsed time %lu secondes for %s, line %d", (fun), (time(NULL) - begin), (str), (line)); \
  if ((cheklocvl2) != (val)) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), cheklocvl2, (line)); \
  	return ((cheklocvl2)); \
  } \
}

#define CHECK_FUNC(inst,proccomm) CHECK_FUNC2(inst, proccomm, __FUNCTION__, __LINE__)

#define CHECK_FUNC2(inst,proccomm,fun,line) { \
  int cheklocvl2; \
  int chekglbvl2; \
  cheklocvl2 = (inst); \
  if (MPI_Allreduce (&(cheklocvl2), &(chekglbvl2), 1, MPI_INT, MPI_MAX, (proccomm)) != MPI_SUCCESS) { \
    printf ("communication error"); \
    return     (-1); \
  } \
  if ((chekglbvl2) != 0) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), chekglbvl2, (line)); \
  	return ((chekglbvl2)); \
  } \
}

#ifdef PAMPA_DEBUG_CHECK
#define CHECK_VERR(valu,proccomm) CHECK_VERR2(valu, proccomm, __FUNCTION__, __LINE__)
#else /* PAMPA_DEBUG_CHECK */
#define CHECK_VERR(valu,proccomm) CHECK_VDBG3(valu, __FUNCTION__, __LINE__)
#endif /* PAMPA_DEBUG_CHECK */

#define CHECK_VERR2(valu,proccomm,fun,line) { \
  int chekglbvl2; \
  if ((valu) != 0) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), (valu), (line)); \
  	if (MPI_Allreduce (&(valu), &(chekglbvl2), 1, MPI_INT, MPI_MAX, (proccomm)) != MPI_SUCCESS) { \
	  PAMPA_errorPrint ("%s: communication error at line %d", (fun), (line)); \
      return     (-1); \
  	} \
  	if ((chekglbvl2) != 0) { \
	  if ((valu) != 0) \
	    PAMPA_errorPrint ("%s: error code %d at line %d", (fun), chekglbvl2, (line)); \
  	  return ((chekglbvl2)); \
  	} \
  } \
}

#ifdef PAMPA_DEBUG_CHECK
#define CHECK_VDBG(valu,proccomm) CHECK_VALU2(valu, proccomm, __FUNCTION__, __LINE__)
#else /* PAMPA_DEBUG_CHECK */
#define CHECK_VDBG(valu,proccomm) CHECK_VDBG3(valu, __FUNCTION__, __LINE__)
#endif /* PAMPA_DEBUG_CHECK */
#define CHECK_VDBG2(valu) CHECK_VDBG3(valu, __FUNCTION__, __LINE__)

#define CHECK_VDBG3(valu,fun,line) { \
  if ((valu) != 0) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), (valu), (line)); \
  	return ((valu)); \
  } \
}

#define CHECK_VALU(valu,proccomm) CHECK_VALU2(valu, proccomm, __FUNCTION__, __LINE__)

#define CHECK_VALU2(valu,proccomm,fun,line) { \
  int chekglbvl2 = 0; \
  if (MPI_Allreduce (&(valu), &(chekglbvl2), 1, MPI_INT, MPI_MAX, (proccomm)) != MPI_SUCCESS) { \
    printf ("communication error"); \
    return     (-1); \
  } \
  if ((chekglbvl2) != 0) { \
	PAMPA_errorPrint ("%s: error code %d at line %d", (fun), chekglbvl2, (line)); \
  	return ((chekglbvl2)); \
  } \
}


#ifdef PAMPA_DEBUG_REDUCE
// ! TODO à quoi sert cette macro ??
#define CHECK_MAX_REDUCE(locval,glbval,proccomm,name,line) \
  if (MPI_Allreduce (&(locval), &(glbval), 1, MPI_INT, MPI_MAX, (proccomm)) != MPI_SUCCESS) { \
    errorPrint ("communication error"); \
    return     (-1); \
  } \
if ((glbval) != 0) \
return ((glbval)); 
#else /* PAMPA_DEBUG_REDUCE */
// ! TODO à quoi sert cette macro ??
#define CHECK_MAX_REDUCE(locval,glbval,proccomm,name,line) \
  if ((locval) != 0) \
return ((locval));
#endif /* PAMPA_DEBUG_REDUCE */

#define CHECK_MAX_REDUCE2(locval,glbval,proccomm,name,line) \
  if (MPI_Allreduce (&(locval), &(glbval), 1, MPI_INT, MPI_MAX, (proccomm)) != MPI_SUCCESS) { \
    errorPrint ("communication error"); \
    return     (-1); \
  } \
if ((glbval) != 0) \
return ((glbval)); 



/*
** Function renaming.
*/

#if ((! defined PAMPA_COMMON_EXTERNAL) || (defined PAMPA_COMMON_RENAME))
#define clockGet                    _PAMPAclockGet


#define usagePrint                  _PAMPAusagePrint

//#define errorPrint(str)             PAMPA_errorPrint("%s: %s, line %d", __FUNCTION__, (str), __LINE__)
//#define infoPrint(str)             PAMPA_infoPrint("%s: %s, line %d", __FUNCTION__, (str), __LINE__)
#ifdef PAMPA_PRINT_LINENUMBER
#define infoPrint(format,...)       PAMPA_infoPrint("%s: "format", line %d", __FUNCTION__, ##__VA_ARGS__,  __LINE__)
#define errorPrint(format,...)      PAMPA_errorPrint("%s: " format", line %d", __FUNCTION__, ##__VA_ARGS__,  __LINE__)
#else /* PAMPA_PRINT_LINENUMBER */
#define infoPrint(format,...)       PAMPA_infoPrint("%s: "format, __FUNCTION__, ##__VA_ARGS__)
#define errorPrint(format,...)      PAMPA_errorPrint("%s: " format, __FUNCTION__, ##__VA_ARGS__)
#endif /* PAMPA_PRINT_LINENUMBER */
#define errorPrintW                 PAMPA_errorPrintW
#define errorProg                   PAMPA_errorProg

#endif /* ((! defined PAMPA_COMMON_EXTERNAL) || (defined PAMPA_COMMON_RENAME)) */

#ifdef PAMPA_RENAME
//#ifndef INTSIZE64
//#define commAllgatherv          _PAMPAcommAllgatherv
//#define commGatherv             _PAMPAcommGatherv
//#define commScatterv            _PAMPAcommScatterv
//#endif /* INTSIZE64 */
#define dmeshBuild              _PAMPAdmeshBuild
#define dmeshBuild2             _PAMPAdmeshBuild2
#define dmeshBuild3             _PAMPAdmeshBuild3
#define dmeshBuild4             _PAMPAdmeshBuild4
#define dmeshCheck              _PAMPAdmeshCheck
#define dmeshDgraphBuild        _PAMPAdmeshDgraphBuild
#define dmeshDgraphExit         _PAMPAdmeshDgraphExit
#define dmeshExit               _PAMPAdmeshExit
#define dmeshFree               _PAMPAdmeshFree
#define dmeshGhst               _PAMPAdmeshGhst
#define dmeshGhst2              _PAMPAdmeshGhst2
#define dmeshHaloAsync          _PAMPAdmeshHaloAsync
#define dmeshHaloCheck          _PAMPAdmeshHaloCheck
#define dmeshHaloSync           _PAMPAdmeshHaloSync
#define dmeshHaloWait           _PAMPAdmeshHaloWait
#define dmeshInit               _PAMPAdmeshInit
#define dmeshItBuild            _PAMPAdmeshItBuild
#define dmeshItData             _PAMPAdmeshItData
#define dmeshMatInit            _PAMPAdmeshMatInit
#define dmeshOverlap            _PAMPAdmeshOverlap
#define dmeshPart               _PAMPAdmeshPart
#define dmeshRedist             _PAMPAdmeshRedist
#define dmeshScatter            _PAMPAdmeshScatter
#define dmeshScatter2           _PAMPAdmeshScatter2
#define dmeshValueData          _PAMPAdmeshValueData
#define dmeshValueLink          _PAMPAdmeshValueLink
#define dmeshValueUnlink        _PAMPAdmeshValueUnlink
#define intSort1asc1            _PAMPAintSort1asc1
#define intSort2asc1            _PAMPAintSort2asc1
#define intSort2asc2            _PAMPAintSort2asc2
#define intSort3asc1            _PAMPAintSort3asc1
#define intSort3asc2            _PAMPAintSort3asc2
#define intSort3asc3            _PAMPAintSort3asc3
#define intSort5asc1            _PAMPAintSort5asc1
#define intSort5asc3            _PAMPAintSort5asc3
#define intSort6asc1            _PAMPAintSort6asc1
#define intSort6asc3            _PAMPAintSort6asc3
#define intSort7asc1            _PAMPAintSort7asc1
#ifndef COMMON_MEMORY_TRACE
#define memAllocGroup           _PAMPAmemAllocGroup
#define memFreeGroup            _PAMPAmemFreeGroup
#define memReallocGroup         _PAMPAmemReallocGroup
#endif /* COMMON_MEMORY_TRACE */
#define memAllocGroupRecord     _PAMPAmemAllocGroupRecord
#define memAllocRecord          _PAMPAmemAllocRecord
#define memFreeRecord           _PAMPAmemFreeRecord
#define memFreeGroupRecord      _PAMPAmemFreeGroupRecord
#define memOffset               _PAMPAmemOffset
#define memMax                  _PAMPAmemMax
#define memReallocGroupRecord   _PAMPAmemReallocGroupRecord
#define memReallocRecord        _PAMPAmemReallocRecord
#define meshBuild               _PAMPAmeshBuild
#define meshBuild2              _PAMPAmeshBuild2
#define meshCheck               _PAMPAmeshCheck
#define meshCheck2              _PAMPAmeshCheck2
#define meshExit                _PAMPAmeshExit
#define meshFree                _PAMPAmeshFree
#define meshInit                _PAMPAmeshInit
#define meshItBuild             _PAMPAmeshItBuild
#define meshItData              _PAMPAmeshItData
#define meshPart                _PAMPAmeshPart
#define meshValueLink           _PAMPAmeshValueLink
#define meshValueUnlink         _PAMPAmeshValueUnlink
#define typeInit                _PAMPAtypeInit
#define typeMpiDatatype         _PAMPAtypeMpiDatatype
#endif /* PAMPA_RENAME */

