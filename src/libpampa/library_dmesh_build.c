/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_build.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This module is the API for the
//!                distributed source mesh handling routines
//!                of the libPampa library. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   23 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine fills the contents of the given
//! opaque distributed mesh structure with the
//! data provided by the user. The base value
//! allows the user to set the mesh base to 0 or 1.
//! 
//! \returns  0   : on success.
//! \returns !0  : on error.

int
PAMPA_dmeshBuild (
PAMPA_Dmesh * const         meshptr,            //!< Distributed mesh structure to fill        
const PAMPA_Num             baseval,            //!< Base for indexing                         
const PAMPA_Num             vertlocnbr,         //!< Number of local vertices                  
const PAMPA_Num             vertlocmax,         //!< Maximum number of local vertices          
PAMPA_Num * const           vertloctab,         //!< Local vertex begin array                  
PAMPA_Num * const           vendloctab,         //!< Local vertex end array                    
PAMPA_Num * const           vlblloctab,         //!< Local vertex label array (not yet used)    
const PAMPA_Num             edgelocnbr,         //!< Number of local edges                     
const PAMPA_Num             edgelocsiz,         //!< Size of local edge array                  
PAMPA_Num * const           edgeloctab,         //!< Local edge array                          
PAMPA_Num * const           edloloctab,         //!< Local edge load array (if any)            
PAMPA_Num                   enttglbnbr,         //!< Number of entities which make up the dmesh
PAMPA_Num *                 ventloctab,         //!< Array of entity values for each vertex    
PAMPA_Num *                 esubloctab,         //!< Array of entity values for each sub-entity    
PAMPA_Num *                 enloglbtab,         //!< Global entity load array (if any) (triplet : [entity, entity, load] and first value is the number of triplets)
PAMPA_Num                   valuglbmax,         //!< Maximum number of associated values       
const PAMPA_Num             ovlpglbval)         //!< Value of the overlap. 0 means a vertex overlap of size 1, \b n (>0) means an element overlap of size \b n
{
  Dmesh *                   srcmeshptr;         /* Pointer to source mesh structure           */
  Gnum *                    ventloctax;
  Gnum *                    vertloctax;
  Gnum *                    vendloctax;
  Gnum *                    edgeloctax;
  Gnum *                    edloloctax;
  Gnum *                    esubloctax;

#ifdef PAMPA_DEBUG_LIBRARY1
  if (sizeof (PAMPA_Dmesh) < sizeof (Dmesh)) {
    errorPrint ("internal error");
    return     (1);
  }

  if (vlblloctab != NULL) {
    errorPrint ("vlblloctab not yet used");
    return     (1);
  }
#endif /* PAMPA_DEBUG_LIBRARY1 */

  srcmeshptr = (Dmesh *) meshptr;                /* Use structure as source mesh */

  
  vertloctax = (Gnum *) vertloctab - baseval;
  vendloctax = (vendloctab == NULL) ? vertloctax + 1 : (Gnum *) vendloctab - baseval;
  edgeloctax = (Gnum *) edgeloctab - baseval;
  edloloctax = (edloloctab == NULL) ? NULL : (Gnum *) edloloctab - baseval;
  ventloctax = (Gnum *) ventloctab - baseval;
  esubloctax = (esubloctab == NULL) ? NULL : (Gnum *) esubloctab - baseval;

  return (dmeshBuild (srcmeshptr, baseval, vertlocnbr, vertlocmax, vertloctax, vendloctax, edgelocnbr, edgelocsiz, edgeloctax, edloloctax, enttglbnbr, ventloctax, esubloctax, valuglbmax, ovlpglbval, NULL, -1, NULL, NULL, NULL, -1, -1, NULL));

}
