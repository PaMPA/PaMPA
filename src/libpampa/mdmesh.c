
/*
 ** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
 */

#define MDMESH

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle distributed */
/* mesh meshs.                      */
/*                                   */
/*************************************/

//! This routine initializes a multi distributed mesh
//! structure. In order to avoid collective
//! communication whenever possible, the allocation
//! of send and receive index arrays is not performed
//! in the routine itself, but rather delegated to
//! subsequent routines such as mdmeshBuild.
//! However, these arrays will not be freed by
//! mdmeshFree, but by mdmeshExit.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
mdmeshInit (
    Mdmesh * restrict const     meshptr,              //!< Distributed mesh structure
    Gnum const                  baseval,              //!< Base value for arrays (0 or 1)
    Gnum const                  mdmhglbnbr,           //!< Number of levels
    MPI_Comm                    proccomm)             //!< Communicator to be used for all communications
{
  Gnum                dmshglbnum;
  Gnum                dmshglbnnd;
  Gnum                cheklocval = 0;

  memSet (meshptr, 0, sizeof (Mdmesh));            /* Clear public and private mesh fields */

  meshptr->baseval = baseval;
  meshptr->proccomm = proccomm;                   /* Set private fields    */
  meshptr->dmshglbnbr = mdmhglbnbr;

  meshptr->flagval |= MDMESHFREEPRIV;
  if ((meshptr->dmshglbtax = (Dmesh *) memAlloc (mdmhglbnbr * sizeof (Dmesh))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  meshptr->dmshglbtax -= baseval;

  for (dmshglbnum = meshptr->baseval, dmshglbnnd = meshptr->baseval + meshptr->dmshglbnbr;
      dmshglbnum < dmshglbnnd; dmshglbnum ++)
    dmeshInit (meshptr->dmshglbtax + dmshglbnum, meshptr->proccomm);

  meshptr->dmshglbnxt = baseval;
  return (0);
}

//! This routine frees the public data of the given
//! distributed mesh, but not its private data.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! 
//! \returns VOID  : in all cases.

static void
mdmeshFree2 (
    Mdmesh * restrict const     meshptr)					//!< Distributed mesh structure
{
  Gnum                dmshglbnum;
  Gnum                dmshglbnnd;

  if ((meshptr->flagval & MDMESHFREEDMESH) != 0) { /* If levels must be freed */
    for (dmshglbnum = meshptr->baseval + 1, dmshglbnnd = meshptr->baseval + meshptr->dmshglbnbr;
        dmshglbnum < dmshglbnnd; dmshglbnum ++)
      dmeshFree (meshptr->dmshglbtax + dmshglbnum);
  }
  meshptr->dmshglbnxt = ~0;

  return;
}

void
mdmeshFree (
    Mdmesh * restrict const     meshptr)					//!< Distributed mesh structure
{
  MdmeshFlag          flagval;
  Gnum                baseval;
  Gnum                dmshglbnbr;
  Dmesh *             dmshglbtax;
  Gnum *              levlglbtax;
  MPI_Comm            proccomm;

  mdmeshFree2 (meshptr);                          /* Free all user fields        */

  flagval    = meshptr->flagval & (MDMESHFREEPRIV | MDMESHFREECOMM);
  baseval    = meshptr->baseval;
  dmshglbnbr = meshptr->dmshglbnbr;
  dmshglbtax = meshptr->dmshglbtax;
  levlglbtax = meshptr->levlglbtax;
  proccomm    = meshptr->proccomm;

  memSet (meshptr, 0, sizeof (Mdmesh));           /* Reset all the mesh structure */

  meshptr->flagval    = flagval;
  meshptr->baseval    = baseval;
  meshptr->dmshglbnbr = dmshglbnbr;
  meshptr->dmshglbtax = dmshglbtax;
  meshptr->levlglbtax = levlglbtax;
  meshptr->proccomm    = proccomm;

  return;
}

//! This routine destroys a distributed mesh structure.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! Private data are always destroyed. If this is not
//! wanted, use mdmeshFree() instead.
//!
//! \returns VOID  : in all cases.

void
mdmeshExit (
    Mdmesh * restrict const     meshptr)					//!< Distributed mesh structure
{
  Gnum                dmshglbnum;
  Gnum                dmshglbnnd;
  Gnum                flagval;

  flagval = meshptr->flagval;
  meshptr->flagval = (meshptr->flagval & (~ MDMESHFREEDMESH));

  mdmeshFree2 (meshptr);

  meshptr->flagval = flagval;

  if ((meshptr->flagval & MDMESHFREEDMESH) != 0) { /* If levels must be freed */
    for (dmshglbnum = meshptr->baseval + 1, dmshglbnnd = meshptr->baseval + meshptr->dmshglbnbr;
        dmshglbnum < dmshglbnnd; dmshglbnum ++)
      dmeshExit (meshptr->dmshglbtax + dmshglbnum);
  }
  meshptr->dmshglbnxt = ~0;

  if ((meshptr->flagval & MDMESHFREEPRIV) != 0)  /* If private data has to be freed */
    memFree (meshptr->dmshglbtax + meshptr->baseval);

  if ((meshptr->flagval & MDMESHFREECOMM) != 0)   /* If communicator has to be freed */
    MPI_Comm_free (&meshptr->proccomm);           /* Free it                         */

#ifdef PAMPA_DEBUG_MDMESH1
  memSet (meshptr, 0, sizeof (Mdmesh));
#endif /* PAMPA_DEBUG_MDMESH1 */
  return;
}
