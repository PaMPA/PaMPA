/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_dgraph.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contains the distributed
//!                graph building routines from a
//!                distributed mesh.
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 **  The defines and includes.
 */

#define DMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"
#include <ptscotch.h>
#include "dmesh_dgraph.h"

//! \brief This function initialize the graph pointer
//!
//! \returns  0 : on success.
//! \returns !0 : on error.
  int
dmeshDgraphInit (
    Dmesh * const        meshptr,
    SCOTCH_Dgraph *      grafptr)
{
  return SCOTCH_dgraphInit(grafptr, meshptr->proccomm);
}


//! This routine computes the distributed graph corresponding to the distributed
//! mesh
//!
//! \returns 0   : if the computation succeeded.
//! \returns !0  : on error.

  int
dmeshDgraphBuild (
    Dmesh * const        meshptr,
    Gnum                 flagval,
    Gnum const           dvrtlocnbr,
    Gnum * const         dvrtgsttax,
    Gnum * const         veloloctax,
    Gnum * const         edloloctax, // \todo edloloctax must be used by callers of this function
    SCOTCH_Dgraph *      grafptr,
    Gnum *               vertlocbas)
{

  Gnum baseval;
  Gnum cheklocval;
  Gnum chekglbval;
  Gnum edgelocnbr;
  Gnum vertlocnum;
  Gnum vertlocnm2;
  Gnum vertlocnnd;
  Gnum ventlocnum;
  Gnum nentlocnum;
  Gnum enttlocnnd;
  Gnum vertlocnbr;
  Gnum vertlocmax;
  Gnum vlbllocnbr;
  Gnum velolocnbr;
  Gnum evrtlocsiz;
  Gnum vnbrlocsiz;
  Gnum * vertloctax;
  Gnum * vendloctax;
  Gnum * vlblloctax;
  Gnum * veloloctx2;
  Gnum * edgeloctax;
  Gnum * vnbrloctab;
  Gnum * evrtloctax;
  DmeshEntity ** enttloctax;
  Gnum vertlocbs2;

  baseval = meshptr->baseval;
  cheklocval = 0;
  enttloctax = meshptr->enttloctax;

  if (edloloctax != NULL) {
    errorPrint ("edloloctax is not yet taken into account");
    return (1);
  }

  switch (flagval) {
    /* If entity is baseval and only some vertices are used */
    case DMESH_ENTT_MAIN_PART :
      CHECK_FERR (dmeshHaloSync (meshptr, enttloctax[baseval], dvrtgsttax + baseval, GNUM_MPI), meshptr->proccomm);

      enttlocnnd = baseval + 1;
      vertlocnbr = dvrtlocnbr;
      vertlocmax = vertlocnbr;

      evrtlocsiz = enttloctax[baseval]->vgstlocnbr;
      vnbrlocsiz = meshptr->procglbnbr;
      break;
    /* If entity is baseval */
    case DMESH_ENTT_MAIN :
      enttlocnnd = baseval + 1;
      vertlocnbr = enttloctax[baseval]->vertlocnbr;
      vertlocmax = vertlocnbr;

      evrtlocsiz = enttloctax[baseval]->vgstlocnbr;
      vnbrlocsiz = meshptr->procglbnbr;
      break;
    /* If all entities */
    case DMESH_ENTT_ANY :
      enttlocnnd = meshptr->enttglbnbr + baseval;
      vertlocnbr = meshptr->vtrulocnbr;
      vertlocmax = meshptr->vertlocmax;

      evrtlocsiz =
        vnbrlocsiz = 0;
      break;
    /* Unknown case */
    default :
      errorPrint("unknown flag");
      return (1);
  }


  // \todo this part could be simplified if no overlap and all entities
  for (edgelocnbr = 0, ventlocnum = baseval ; ventlocnum < enttlocnnd ; ventlocnum ++) {
    /* If no vertex or sub-entity */
    if ((enttloctax[ventlocnum] == NULL) || (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      continue;

    /* For each entity on neighbors */
    for (nentlocnum = baseval ; nentlocnum < enttlocnnd ; nentlocnum ++) {
      /* If there are no vertex for neighbor entity or if neighbor entity is a
       * sub-entity */
      if ((enttloctax[ventlocnum]->nghbloctax[nentlocnum] == NULL) || (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
        continue;

      /* For each vertex with ventlocnum entity, add number of neighbors to
       * edgelocnbr */
      for (vertlocnum = baseval, vertlocnnd = enttloctax[ventlocnum]->vertlocnbr + baseval;
          vertlocnum < vertlocnnd; vertlocnum ++)  {
        edgelocnbr += enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx
          - enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx;
      }
    }
  }


  if (memAllocGroup ((void **) (void *)
        &vnbrloctab, (size_t) (vnbrlocsiz * sizeof (Gnum)),
        &evrtloctax, (size_t) (evrtlocsiz * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_MAX_REDUCE(cheklocval, chekglbval, meshptr->proccomm, "dmeshDgraphBuild", __LINE__);

  evrtloctax -= baseval;

  velolocnbr = (veloloctax == NULL) ?  vertlocnbr : 0;
  vlbllocnbr = (flagval == DMESH_ENTT_ANY) ? meshptr->vertlocnbr : 0;

  if (memAllocGroup ((void **) (void *)
        &vertloctax, (size_t) (MAX(vertlocnbr, 2) * sizeof (Gnum)), // TRICK: MAX(,2) for Scotch if vertlocnbr = 0
        &vendloctax, (size_t) (MAX(vertlocnbr, 2) * sizeof (Gnum)),
        &vlblloctax, (size_t) (vlbllocnbr         * sizeof (Gnum)),
        &veloloctx2, (size_t) (velolocnbr         * sizeof (Gnum)),
        &edgeloctax, (size_t) (MAX(edgelocnbr, 2) * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_MAX_REDUCE(cheklocval, chekglbval, meshptr->proccomm, "dmeshDgraphBuild", __LINE__);
  vertloctax -= baseval;
  vendloctax -= baseval;
  veloloctx2 = (veloloctax == NULL) ? veloloctx2 - baseval : veloloctax;
  vlblloctax = (vlbllocnbr == 0) ? NULL : vlblloctax - baseval;
  edgeloctax -= baseval;

  memset (vertloctax + baseval, 0, MAX(vertlocnbr, 2) * sizeof (Gnum));
  memset (vendloctax + baseval, 0, MAX(vertlocnbr, 2) * sizeof (Gnum));
  if (veloloctax == NULL) 
    memset (veloloctx2 + baseval, 0, vertlocnbr * sizeof (Gnum));

  /* Compute graph numbering and store it in evrtloctax */
  switch (flagval) {
    case DMESH_ENTT_MAIN_PART :
    case DMESH_ENTT_MAIN :
      if (commAllgather (&vertlocnbr, 1, GNUM_MPI,
            vnbrloctab, 1, GNUM_MPI, meshptr->proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        return     (1);
      }

      Gnum procnum;
      Gnum procnnd;

      vertlocbs2 = 0;
      for (procnum = 0, procnnd = meshptr->proclocnum ; procnum < procnnd ; procnum ++)
        vertlocbs2 += vnbrloctab[procnum];

      for (vertlocnum = baseval, vertlocnnd = enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd ; vertlocnum ++)
        if (flagval == DMESH_ENTT_MAIN)
          evrtloctax[vertlocnum] = vertlocnum + vertlocbs2;
        else if (dvrtgsttax[vertlocnum] != ~0)
          evrtloctax[vertlocnum] = dvrtgsttax[vertlocnum] + vertlocbs2;
        else
          evrtloctax[vertlocnum] = ~0;

      if (vertlocbas != NULL)
        *vertlocbas = vertlocbs2;

      /* Exchange evrtloctax in order to have numbering on ghost vertices */
      CHECK_FERR (dmeshHaloSync (meshptr, enttloctax[baseval], evrtloctax + baseval, GNUM_MPI), meshptr->proccomm);
      break;
    case DMESH_ENTT_ANY :
      vertlocbs2 = meshptr->procvrttab[meshptr->proclocnum] - baseval;

      if (vertlocbas != NULL)
        *vertlocbas = vertlocbs2;

      break;
    default :
      errorPrint("unknown flag");
      return (1);
  }

  /* If entity is baseval, fill vlblloctax which will contains the vertex
   * numbering of the graph */
  if (flagval == DMESH_ENTT_ANY) {
    for (vertlocnum = baseval, vertlocnnd = baseval + meshptr->vertlocnbr ; vertlocnum < vertlocnnd ; vertlocnum ++)
      vlblloctax[vertlocnum] = meshptr->vertlocmax + 1;

    for (ventlocnum = baseval ; ventlocnum < enttlocnnd ; ventlocnum ++) {
      DmeshEntity * ventlocptr;

      ventlocptr = enttloctax[ventlocnum];

      if ((enttloctax[ventlocnum] == NULL) || (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
        continue;

      for (vertlocnum = baseval, vertlocnnd = ventlocptr->vertlocnbr + baseval;
          vertlocnum < vertlocnnd; vertlocnum ++) {
        vertlocnm2 = ventlocptr->mvrtloctax[vertlocnum] - vertlocbs2;
        vlblloctax[vertlocnm2] = vertlocnm2;
      }
    }
    intSort1asc1 (vlblloctax + baseval, meshptr->vertlocnbr);
  }


  /* For each entity */
  for (vertlocnm2 = baseval - 1, edgelocnbr = ventlocnum = baseval ; ventlocnum < enttlocnnd ; ventlocnum ++) {
    DmeshEntity * ventlocptr;

    ventlocptr = enttloctax[ventlocnum];

    /* If no vertex or if sub-entity */
    if ((enttloctax[ventlocnum] == NULL) || (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      continue;

    /* For each vertex in ventlocnum entity */
    for (vertlocnum = baseval, vertlocnnd = ventlocptr->vertlocnbr + baseval;
        vertlocnum < vertlocnnd; vertlocnum ++)  {
      Gnum edgelocnum;
      Gnum edgelocnnd;

      if (flagval == DMESH_ENTT_ANY) {
        Gnum vertlocnm3;
        Gnum vertlocmx2;

        vertlocnm2 = ventlocptr->mvrtloctax[vertlocnum] - vertlocbs2;
        /* Dichotomy search for vertlocnm2 in vlblloctax, vertlocnm3 will be the
         * global number in the graph */
        if (vlblloctax[vertlocnm2] != vertlocnm2) {
          for (vertlocnm3 = baseval, vertlocmx2 = meshptr->vertlocnbr + baseval;
              vertlocmx2 - vertlocnm3 > 1; ) {
            Gnum                 vertlocmed;

            vertlocmed = (vertlocmx2 + vertlocnm3) / 2;
            if (vlblloctax[vertlocmed] <= vertlocnm2)
              vertlocnm3 = vertlocmed;
            else
              vertlocmx2 = vertlocmed;
          }
#ifdef PAMPA_DMESH2
          if (vlblloctax[vertlocnm3] != vertlocnm2) {
            errorPrint ("%d not found", vertlocnm2);
            return (1);
          }
#endif /* PAMPA_DMESH2 */
          vertlocnm2 = vertlocnm3;
        }
      }
      else if (flagval == DMESH_ENTT_MAIN)
        vertlocnm2 ++;
      else if (evrtloctax[vertlocnum] != ~0) //!< \todo better test?
        vertlocnm2 ++;
      else
        continue;

      vertloctax[vertlocnm2] = edgelocnbr;
      if (veloloctax == NULL)
        veloloctx2[vertlocnm2] = 1;

      for (nentlocnum = baseval ; nentlocnum < enttlocnnd ; nentlocnum ++) {
        DmeshEntity * nentlocptr;

        nentlocptr = enttloctax[nentlocnum];

        if ((ventlocptr->nghbloctax[nentlocnum] == NULL) || (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
          continue;

        if ((flagval == DMESH_ENTT_ANY) && (nentlocptr != NULL))
          evrtloctax = nentlocptr->mvrtloctax;



        for (edgelocnum = ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx,
            edgelocnnd = ventlocptr->nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx;
            edgelocnum < edgelocnnd ; edgelocnum ++)
          if ((flagval != DMESH_ENTT_MAIN_PART) || ((flagval == DMESH_ENTT_MAIN_PART) && (evrtloctax[meshptr->edgeloctax[edgelocnum]] != ~0)))
            edgeloctax[edgelocnbr ++] = evrtloctax[meshptr->edgeloctax[edgelocnum]];

      }
      vendloctax[vertlocnm2] = edgelocnbr;
    }
  }
  edgelocnbr -= baseval;

  if (flagval == DMESH_ENTT_ANY) {
    for (vertlocnum = baseval, vertlocnnd = baseval + meshptr->vtrulocnbr ; vertlocnum < vertlocnnd ; vertlocnum ++)
      vlblloctax[vertlocnum] += vertlocbs2;
  }



  if (vertloctax != NULL)
    vertloctax += baseval;
  if (vlblloctax != NULL)
    vlblloctax += baseval;

  cheklocval = SCOTCH_dgraphBuild(grafptr,
      meshptr->baseval,
      vertlocnbr,
      vertlocmax,
      vertloctax, // TRICK: vertloctax already unbased due to allocation group
      vendloctax + baseval,
      veloloctx2 + baseval,
      vlblloctax,
      edgelocnbr,
      edgelocnbr,
      edgeloctax + baseval,
      NULL,
      NULL);
  if (cheklocval != 0)
    return cheklocval;

//#define PAMPA_DEBUG_DGRAPH
#ifdef PAMPA_DEBUG_DGRAPH
  {
    static int i = 0;
    char s[30];
    sprintf(s, "dmesh-dgraph-%d-" GNUMSTRING ".grf", i ++, meshptr->proclocnum);
    FILE *graph_file;
    graph_file = fopen(s, "w");
    SCOTCH_dgraphSave (grafptr, graph_file);
    fclose(graph_file);
  }
#endif /* PAMPA_DEBUG_DGRAPH */

#ifdef PAMPA_DEBUG_DMESH_DGRAPH
  CHECK_FERR (SCOTCH_dgraphCheck(grafptr), meshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH_DGRAPH */

  memFreeGroup (vnbrloctab);
  return (cheklocval);
}

//! This routine frees the graph
  int
dmeshDgraphFree (
    SCOTCH_Dgraph *      grafptr)
{

  Gnum * vertloctab;
  SCOTCH_dgraphData(grafptr, NULL, NULL, NULL, NULL, NULL, &vertloctab, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 

  if (vertloctab != NULL)
    memFreeGroup (vertloctab);

  SCOTCH_dgraphFree (grafptr);

  return (0);
}

//! This routine frees all data of the graph
  int
dmeshDgraphExit (
    SCOTCH_Dgraph *      grafptr)
{

  Gnum * vertloctab;
  SCOTCH_dgraphData(grafptr, NULL, NULL, NULL, NULL, NULL, &vertloctab, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 

  if (vertloctab != NULL)
    memFreeGroup (vertloctab);

  SCOTCH_dgraphExit(grafptr);

  return (0);
}
