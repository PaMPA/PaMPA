/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
#define MDMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dsmesh.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "mdmesh.h"
#include "pampa.h"
#include "mdmesh_coar_level_build.h"
#include "mdmesh_mesh_levels.h"
#include <ptscotch.h>
#include "dmesh_dgraph.h"
#include "dmesh_graph.h"
#include "dsmesh_dmesh.h"
#include "smesh_mesh.h"
#include "dsmesh_halo.h"
#include "dmesh_gather_induce_multiple.h"
#define PAMPA_DEBUG_MDMESH2 // XXX temporaire tant qu'il y a l'effet météorite

int
mdmeshAggrCheck (
    SCOTCH_Graph * grafptr,
    Gnum           baseval,
    Gnum         * partnbr,
    Gnum         * parttax)
{
  SCOTCH_Graph graflvl;
  Gnum coarvertnbr;
  Gnum vertnbr;
  Gnum colrflg;
  Gnum vertnum;
  Gnum vertnnd;
  Gnum * restrict verttax;
  Gnum * restrict vendtax;
  Gnum * restrict edgetax;
  Gnum * restrict parttx2;
  Gnum * restrict finematetax;
  Gnum * restrict coarmulttax;
  int cheklocval = 0;

  CHECK_FDBG2 (SCOTCH_graphInit (&graflvl));
  SCOTCH_graphData (grafptr, NULL, &vertnbr, (Gnum **) &verttax, (Gnum **) &vendtax, NULL, NULL, NULL, (Gnum **) &edgetax, NULL);
  verttax -= baseval;
  vendtax -= baseval;
  edgetax -= baseval;

  if (memAllocGroup ((void **) (void *)
        &parttx2,     (size_t) (vertnbr     * sizeof (Gnum)),
        &finematetax, (size_t) (vertnbr     * sizeof (Gnum)),
        &coarmulttax, (size_t) (vertnbr * 2 * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  parttx2 -= baseval;
  finematetax -= baseval;
  coarmulttax -= 2 * baseval;

  memSet (finematetax + baseval, ~0, vertnbr * sizeof (Gnum));
  for (colrflg = 0, coarvertnbr = vertnbr, vertnum = baseval, vertnnd = vertnbr + baseval;
      vertnum < vertnnd; vertnum ++) {
    Gnum edgenum;
    Gnum edgennd;

    if (finematetax[vertnum] == ~0)
      for (edgenum = verttax[vertnum], edgennd = vendtax[vertnum]; edgenum < edgennd; edgenum ++)
        if ((finematetax[edgetax[edgenum]] == ~0) && (parttax[vertnum] == parttax[edgetax[edgenum]])) {
          if (colrflg == 0)
            colrflg = 1;
          finematetax[vertnum] = edgetax[edgenum];
          finematetax[edgetax[edgenum]] = vertnum;
          coarvertnbr --;
          break;
        }
  }
  if (colrflg == 1) {
    for (vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnbr; vertnum ++) /* If there are still alone vertices */
      if (finematetax[vertnum] == ~0)
        finematetax[vertnum] = vertnum;

    // TODO: check the order of arguments
    CHECK_FDBG2 (SCOTCH_graphCoarsenBuild (grafptr, &graflvl, finematetax + baseval, coarvertnbr, coarmulttax + 2 * baseval));


    for (vertnum = baseval, vertnnd = coarvertnbr + baseval; vertnum < vertnnd; vertnum ++)
      parttx2[vertnum] = parttax[coarmulttax[vertnum << 1]];

    SCOTCH_graphExit (grafptr);
    grafptr = &graflvl;

    mdmeshAggrCheck (grafptr, baseval, partnbr, parttx2);
    for (vertnum = baseval, vertnnd = coarvertnbr + baseval; vertnum < vertnnd; vertnum ++) {
      parttax[coarmulttax[vertnum << 1]] = parttx2[vertnum];
      parttax[coarmulttax[(vertnum << 1) + 1]] = parttx2[vertnum];
    }
  }
  else {
    SCOTCH_graphSize (grafptr, &vertnbr, NULL);

    if (vertnbr != (*partnbr)) {
      infoPrint ("At least 2 CC for one aggregated element");

      memSet (parttx2 + baseval, 0, vertnbr * sizeof (Gnum));
      for (vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++)
        if (parttx2[parttax[vertnum]] != 0)
          parttax[vertnum] = (*partnbr) ++;
        else
          parttx2[parttax[vertnum]] ++;
    }
    SCOTCH_graphExit (grafptr);
  }
  memFreeGroup (parttx2 + baseval);
  return (0);
}

/** This routine builds a multi distributed mesh from
 ** the local arrays that are passed to it.
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

int
mdmeshCoarLevelBuild (
    Mdmesh * const                meshptr,
    Gnum   * const               meshglbptr,
    const Gnum                   velolocsiz, /* Size of veloloctax */
    const Gnum * const           veloloctax, /* Weight on vertices which are only elements */
    const Gnum                   edlolocsiz, /* Size of edloloctax */
    const Gnum * const           edloloctax, /* Weight on edges between edges */
    const Gnum * const           enttglbtax, /* Concordance table on entities between two levels */
    const Gnum                   elvlglbsiz, /* Size of elvlglbtax */
    const Gnum * const           elvlglbtax, /* Array which indicate the order to treat the entities */
    const Gnum * const           lnumglbtax, /* Array which indicate the number of the levels */
    const Gnum * const           etagglbtax, /* Tag array for each entity, tag is compared before merging vertices with the same entity */
    const Gnum                   loadglbval, /* Weight for coarsened element */
    const Gnum                   flagval)
{
  Dsmesh dsmshdat;
  Gnum elvlglbsz2;
  Gnum vertlocnbr;
  Gnum edgelocidx;
  Gnum edgelocnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum vertlocidx;
  Gnum pglbngbnum;
  Gnum pglbngbnbr;
  Gnum velolocsum;
  Gnum veloglbsum;
  Gnum enttidx;
  Gnum enttoldidx;
  Gnum enttnnd;
  Gnum enttnum;
  Gnum levlglbval;
  SCOTCH_Strat strtdat;
  Gnum partlocnbr;
  Gnum * restrict partedloloctax;
  Gnum * origedloloctax;
  Gnum * restrict enttpartgsttax;
  Gnum * restrict ventloctax;
  Gnum * restrict elvlglbtx2;
  Gnum * restrict lnumglbtx2;
  Gnum * restrict vnumgsttax; // tableau contenant pour chaque sommet le numéro du nouveau sommet dans le niveau de grille en cours
  Gnum * restrict vnumlvlgsttax;
  Gnum * restrict vnumnewgsttax;
  Gnum * restrict tagloctax;
  int * restrict dsndcnttab;
  int * restrict dsnddsptab;
  Gnum * restrict datasndtab;
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  Gnum * restrict datarcvtab;
  int datasndnbr;
  int datasndsiz;
  int datarcvnbr;
  int datarcvsiz;
  Gnum * nghbloctab;
  Gnum nghblocsiz;
  MdmeshCrLvlBldPart * restrict parthashtab;
  Gnum parthashsiz;          /* Size of hash table    */
  Gnum parthashnum;          /* Hash value            */
  Gnum parthashmax;
  Gnum parthashnbr;
  Gnum parthashmsk;
  MdmeshCrLvlBldEntt * restrict entthashtab;
  Gnum entthashsiz;          /* Size of hash table    */
  Gnum entthashnum;          /* Hash value            */
  Gnum entthashmax;
  Gnum entthashnbr;
  Gnum entthashmsk;
  MdmeshCrLvlBldVert * restrict verthashtab;
  Gnum verthashsiz;          /* Size of hash table    */
  Gnum verthashnum;          /* Hash value            */
  Gnum verthashmax;
  Gnum verthashnbr;
  Gnum verthashmsk;
  Gnum * restrict vertloctax;
  Gnum * restrict vendloctax;
  Gnum * restrict edgeloctax;

  DmeshEntity * restrict enttlocptr;
  int  cheklocval = 0;
  const Gnum baseval = meshptr->baseval;
#ifdef PAMPA_DEBUG_MDMESH
  if (meshptr->dmshglbnxt == meshptr->baseval) {
    errorPrint ("No level has been initialized");
    cheklocval = 1;
  }
  else if (meshptr->dmshglbnxt >= (meshptr->dmshglbnbr + meshptr->baseval)) {
    errorPrint ("No more level could be built");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

  *meshglbptr = meshptr->dmshglbnxt;
  Dmesh * const mesholdptr = meshptr->dmshglbtax + meshptr->dmshglbnxt - 1;
  Dmesh * const meshcurptr = meshptr->dmshglbtax + meshptr->dmshglbnxt;
  meshptr->dmshglbnxt ++;
  const Gnum vertlocmin = mesholdptr->procvrttab[mesholdptr->proclocnum];
  const Gnum vertlocmax = mesholdptr->procvrttab[mesholdptr->proclocnum + 1] - 1;
  const Gnum vertlocbas = mesholdptr->procvrttab[mesholdptr->proclocnum] - meshptr->baseval;
  const int datadsp = 7; /* If value is changed, modify intSort6asc1 */
  const int datasnddsp = datadsp - 1;
  const int datavrt1dsp = 0;
  const int datavrt2dsp = 1;
  const int dataenttnewdsp = 2;
  const int dataenttolddsp = 3;
  const int datatagdsp = 4;
  const int datavertnumdsp = 5;
  const int dataprocdsp = 1;
// XXX sans drapeau temporaire tant qu'il y a effet meteorite #ifdef PAMPA_DEBUG_MDMESH2
  SCOTCH_Graph *  cgrfptr = NULL;
  SCOTCH_Dgraph * dgrfptr = NULL;
  SCOTCH_Graph    cgrfdat;
  SCOTCH_Dgraph   dgrfdat;
  Smesh           smshdat;
  Gnum          * tmpptr;
  Gnum            partglbnbr;
// XXX sans drapeau temporaire tant qu'il y a effet meteorite #endif /* PAMPA_DEBUG_MDMESH2 */


  cheklocval = 0;



#ifdef PAMPA_DEBUG_MDMESH // XXX après avoir fusionné avec la branche pampa_1_0_0, ajouter les drapeaux PAMPA_DEBUG_MDMESH et PAMPA_DEBUG_MDMESH2 dans module.h
  {
    Gnum flagvl2;

    // - si une entité est dans etagglbtax et pas dans elvlglbtax (l'entité ou l'entité mère) => pb
    // - si une entité et une de ses sous-entité est présente dans etagglbtax => pb
    if (etagglbtax != NULL) {
      for (enttidx = baseval, enttnnd = baseval + mesholdptr->enttglbnbr; enttidx < enttnnd; enttidx ++)
        if (etagglbtax[enttidx] != ~0) {
          Gnum enttid2;
          Gnum enttnn2;

          for (flagvl2 = 0, enttid2 = baseval, enttnn2 = baseval + elvlglbsiz; enttid2 < enttnnd; enttid2 ++) { // TRICK: enttnn2 is used in followed line
            if ((enttid2 < enttnn2) && ((enttidx == elvlglbtax[enttid2]) || (mesholdptr->esublocbax[enttidx & mesholdptr->esublocmsk] == elvlglbtax[enttid2])))
              flagvl2 |= 1;
            if ((mesholdptr->esublocbax[enttidx & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) && (etagglbtax[mesholdptr->esublocbax[enttidx & mesholdptr->esublocmsk]] != ~0))
              flagvl2 |= 2;
          }
          if ((flagvl2 & 1) == 0) {
            errorPrint ("Entity " GNUMSTRING " is present in etagglbtab but not in elvlglbtab", enttidx);
            cheklocval = 1;
          }
          if ((flagvl2 & 2) != 0) {
            errorPrint ("Entity " GNUMSTRING " is present in etagglbtab and also its super entity", enttidx);
            cheklocval = 1;
          }
        }
    } /* if (etagglbtax != NULL) */

    // - si une entité et une de ses sous-entité est présente dans elvlglbtax => pb
    for (flagvl2 = 0, enttidx = baseval, enttnnd = baseval + elvlglbsiz; enttidx < enttnnd && flagvl2 == 0; enttidx ++) {
      Gnum enttid2;

      for (enttid2 = enttidx + 1; enttid2 < enttnnd && flagvl2 == 0; enttid2 ++) {
        if (mesholdptr->esublocbax[enttidx & mesholdptr->esublocmsk] == enttid2) {
          errorPrint ("Entity " GNUMSTRING " and its super entity " GNUMSTRING "are present in elvlglbtab", enttidx, enttid2);
          flagvl2 |= 1;
        }
      }
    }

    // - si deux entités ont la même valeur dans enttglbtax et ne sont pas dans le même niveau (elvlglbtax) => pb
    if (enttglbtax != NULL) {
      for (flagvl2 = 0, enttidx = baseval, enttnnd = baseval + elvlglbsiz; enttidx < enttnnd && flagvl2 == 0; enttidx ++) {
        Gnum enttid2;

        for (enttid2 = enttidx + 1; enttid2 < enttnnd && flagvl2 == 0; enttid2 ++) {
          if ((enttglbtax[elvlglbtax[enttidx]] == enttglbtax[elvlglbtax[enttid2]]) && (lnumglbtax[enttidx] != lnumglbtax[enttid2])) {
            errorPrint ("Entities " GNUMSTRING " and " GNUMSTRING " have the same value in enttglbtab but are not on the same level", elvlglbtax[enttidx], elvlglbtax[enttid2]);
            flagvl2 |= 1;
          }
        }
      }
    }
    if (flagvl2 != 0)
      cheklocval = 1;
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
#endif /* PAMPA_DEBUG_MDMESH */


  // - convertir dmshptr en Dsmesh
  dsmeshInit(&dsmshdat, meshptr->proccomm);
  CHECK_FERR (dmesh2dsmesh(mesholdptr, &dsmshdat), meshptr->proccomm);

#ifdef PAMPA_DEBUG_MDMESH
  if (((edlolocsiz == 0) && (edloloctax != NULL)) || ((velolocsiz == 0) && (veloloctax != NULL))) {
    errorPrint ("Invalid size or pointer for edloloctab or veloloctab");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_MDMESH */

  nghblocsiz = MAX(dsmshdat.degrlocmax, 2);

  if (memAllocGroup ((void **) (void *)
        &enttpartgsttax, (size_t) (dsmshdat.vgstlocnbr       * sizeof (Gnum)),
        &partedloloctax, (size_t) (edlolocsiz                * sizeof (Gnum)),
        &vnumgsttax,     (size_t) (dsmshdat.vgstlocnbr       * sizeof (Gnum)),
        &vnumlvlgsttax,  (size_t) (dsmshdat.vgstlocnbr       * sizeof (Gnum)),
        &vnumnewgsttax,  (size_t) (dsmshdat.vgstlocnbr       * sizeof (Gnum)),
        &ventloctax,     (size_t) (dsmshdat.vertlocnbr       * sizeof (Gnum)),
        &elvlglbtx2,     (size_t) (dsmshdat.enttglbnbr       * sizeof (Gnum)),
        &lnumglbtx2,     (size_t) (dsmshdat.enttglbnbr       * sizeof (Gnum)),
        &dsndcnttab,     (size_t) (dsmshdat.procglbnbr       * sizeof (int)),
        &dsnddsptab,     (size_t) ((dsmshdat.procglbnbr + 1) * sizeof (int)),
        &drcvcnttab,     (size_t) (dsmshdat.procglbnbr       * sizeof (int)),
        &drcvdsptab,     (size_t) ((dsmshdat.procglbnbr + 1) * sizeof (int)),
        &nghbloctab,     (size_t) (nghblocsiz                * sizeof (Gnum)),
        &vertloctax,     (size_t) (dsmshdat.vertlocmax       * sizeof (Gnum)),
        &vendloctax,     (size_t) (dsmshdat.vertlocmax       * sizeof (Gnum)),
        &edgeloctax,     (size_t) (dsmshdat.edgelocsiz       * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  origedloloctax  = (mesholdptr->edloloctax != NULL) ? mesholdptr->edloloctax : NULL;
  partedloloctax  = (edlolocsiz != 0) ? partedloloctax - baseval : NULL;
  enttpartgsttax -= baseval;
  vnumgsttax     -= baseval;
  vnumlvlgsttax  -= baseval;
  vnumnewgsttax  -= baseval;
  ventloctax     -= baseval;
  elvlglbtx2      = (elvlglbtax != NULL) ? elvlglbtx2 - baseval : NULL;
  lnumglbtx2      = (elvlglbtax != NULL) ? lnumglbtx2 - baseval : NULL;
  vertloctax     -= baseval;
  vendloctax     -= baseval;
  edgeloctax     -= baseval;

  // XXX vérifier que ~0 correspond à pampa_entt_dummy
  memSet (vnumgsttax + baseval, ~0, dsmshdat.vgstlocnbr * sizeof (Gnum)); // XXX vertlocnbr ou vertlocmax ??
  memSet (vnumlvlgsttax + baseval, ~0, dsmshdat.vgstlocnbr * sizeof (Gnum));
  memSet (vnumnewgsttax + baseval, ~0, dsmshdat.vgstlocnbr * sizeof (Gnum));
  memSet (ventloctax + baseval, ~0, dsmshdat.vertlocnbr * sizeof (Gnum));
  memSet (vertloctax + baseval, ~0, dsmshdat.vertlocnbr * sizeof (Gnum));
  memSet (vendloctax + baseval, ~0, dsmshdat.vertlocmax * sizeof (Gnum));

  mesholdptr->edloloctax = partedloloctax;

  for (elvlglbsz2 = enttidx = baseval, enttnnd = baseval + elvlglbsiz; enttidx < enttnnd; enttidx ++) {
    enttnum = elvlglbtax[enttidx];
    if (mesholdptr->esublocbax[enttnum & mesholdptr->esublocmsk] < PAMPA_ENTT_SINGLE) { /* If super entity */
      Gnum enttid2;
      Gnum enttnn2;

      for (enttid2 = baseval, enttnn2 = baseval + mesholdptr->enttglbnbr; enttid2 < enttnn2; enttid2 ++)
        if (mesholdptr->esublocbax[enttid2 & mesholdptr->esublocmsk] == enttnum) { /* If enttnum is the super entity */
          elvlglbtx2[elvlglbsz2] = enttid2;
          lnumglbtx2[elvlglbsz2 ++] = lnumglbtax[enttidx];
        }
    }
    else {
      elvlglbtx2[elvlglbsz2] = enttnum;
      lnumglbtx2[elvlglbsz2 ++] = lnumglbtax[enttidx];
    }
  }
  elvlglbsz2 -= baseval;


#ifdef PAMPA_DEBUG_MDMESH
  if (edlolocsiz != 0)
    memSet (partedloloctax + baseval, ~0, mesholdptr->edgelocsiz * sizeof (Gnum));
#endif /* PAMPA_DEBUG_MDMESH */

  enttlocptr = mesholdptr->enttloctax[baseval];
  if (edlolocsiz != 0) {
    if ((flagval & PAMPA_LVL_ITER_ORDER) != 0) {
      Gnum edlolocidx;
      PAMPA_Iterator it;

      PAMPA_dmeshItInitStart((PAMPA_Dmesh *) mesholdptr, baseval, PAMPA_VERT_LOCAL, &it);

      edlolocidx = baseval;
      while (PAMPA_itHasMore(&it)) {
        PAMPA_Num tetnum;
        Gnum edgelocnum;
        Gnum edgelocnnd;

        tetnum = PAMPA_itCurEnttVertNum(&it);
        for (edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[tetnum].vertlocidx,
            edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[tetnum].vendlocidx;
            edgelocnum < edgelocnnd; edgelocnum ++)
          partedloloctax[edgelocnum] = edloloctax[edlolocidx ++];
        PAMPA_itNext(&it);
      }
    }
    else if ((flagval & PAMPA_LVL_ORIG_ORDER) != 0) { /* flagval & PAMPA_LVL_ORIG_ORDER */
      Gnum edlolocidx;
      Gnum edgelocnum;
      Gnum edgelocnnd;

      for (edlolocidx = vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
        for (edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
            edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
            edgelocnum < edgelocnnd; edgelocnum ++)
          partedloloctax[edgelocnum] = edloloctax[edlolocidx ++];
      }
    }
    else {
      errorPrint ("Invalid flag value, PAMPA_LVL_ORIG_ORDER or PAMPA_LVL_ITER_ORDER is needed");
      cheklocval = 1;
    }
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

#ifdef PAMPA_DEBUG_MDMESH
  {
    if (edlolocsiz != 0) {
      Gnum edlolocidx;
      Gnum edgelocnum;
      Gnum edgelocnnd;

      for (edlolocidx = vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
        for (edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
            edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
            edgelocnum < edgelocnnd; edgelocnum ++)
          if (partedloloctax[edgelocnum] == ~0) {
            errorPrint ("Load of edge between vertices " GNUMSTRING " and " GNUMSTRING " is absent", enttlocptr->mvrtloctax[vertlocnum], enttlocptr->mvrtloctax[mesholdptr->edgeloctax[edgelocnum]]);
            cheklocval = 1;
          }
      }
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);
  }
#endif /* PAMPA_DEBUG_MDMESH */

  /* Compute the local sum of vertex weight */
  if (velolocsiz != 0) {
    for (velolocsum = 0, vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
      velolocsum += veloloctax[vertlocnum];
  }
  else { /* Without vertex load */
    velolocsum = enttlocptr->vertlocnbr;
  }

  /* Deduce the number of parts */
  partlocnbr = ceil (1.0 * velolocsum / loadglbval);


  CHECK_FERR (SCOTCH_stratInit (&strtdat), mesholdptr->proccomm);

  if ((flagval & PAMPA_LVL_PART_GLOBAL) != 0) {
#ifndef PAMPA_DEBUG_MDMESH2
    SCOTCH_Dgraph dgrfdat;
    Gnum partglbnbr;
#endif /* PAMPA_DEBUG_MDMESH2 */

    errorPrint ("flagval & PAMPA_LVL_PART_GLOBAL not yet implemented");
    return (1);

    CHECK_VDBG (cheklocval, meshptr->proccomm);
    if (commAllreduce(&velolocsum, &veloglbsum, 1, GNUM_MPI, MPI_SUM, meshptr->proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    /* Deduce the number of parts */
    partglbnbr = ceil (1.0 * veloglbsum / loadglbval);


    /* Get element graph from dmesh */
    CHECK_FERR (dmeshDgraphInit (mesholdptr, &dgrfdat), meshptr->proccomm);
    CHECK_FERR (dmeshDgraphBuild (mesholdptr, DMESH_ENTT_MAIN, ~0, NULL, (Gnum * const) veloloctax, (Gnum * const) partedloloctax, &dgrfdat, NULL), meshptr->proccomm);
    SCOTCH_dgraphSize (&dgrfdat, NULL, &vertlocnbr, NULL, &edgelocnbr);
// XXX sans drapeau temporaire tant qu'il y a effet meteorite #ifdef PAMPA_DEBUG_MDMESH2
    dgrfptr = &dgrfdat;
// XXX sans drapeau temporaire tant qu'il y a effet meteorite #endif /* PAMPA_DEBUG_MDMESH2 */

    if (((velolocsiz > 0) && (vertlocnbr != velolocsiz)) || ((edlolocsiz > 0) && (edgelocnbr != edlolocsiz))) {
      errorPrint ("Invalid size of veloloctab or edloloctab");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);

    CHECK_FERR (SCOTCH_dgraphPart (&dgrfdat, partglbnbr, &strtdat, enttpartgsttax), dsmshdat.proccomm);

  }
  else if ((flagval & PAMPA_LVL_PART_LOCAL) != 0) { /* flagval & PAMPA_LVL_PART_LOCAL */
#ifndef PAMPA_DEBUG_MDMESH2
      SCOTCH_Graph cgrfdat;
#endif /* PAMPA_DEBUG_MDMESH2 */


    /* Get element graph from dmesh */
    CHECK_FERR (dmeshGraphInit (&cgrfdat), meshptr->proccomm);
    CHECK_FERR (dmeshGraphBuild (mesholdptr, DMESH_ENTT_MAIN, ~0, NULL, (Gnum * const) veloloctax, (Gnum * const) partedloloctax, &cgrfdat, NULL), meshptr->proccomm);
    SCOTCH_graphSize (&cgrfdat, &vertlocnbr, &edgelocnbr);
#ifdef PAMPA_DEBUG_MDMESH2
    cgrfptr = &cgrfdat;
#endif /* PAMPA_DEBUG_MDMESH2 */

    if (((velolocsiz > 0) && (vertlocnbr != velolocsiz)) || ((edlolocsiz > 0) && (edgelocnbr != edlolocsiz))) {
      errorPrint ("Invalid size of veloloctab or edloloctab");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);

    CHECK_FERR (SCOTCH_graphPart (&cgrfdat, partlocnbr, &strtdat, enttpartgsttax), dsmshdat.proccomm);
#ifdef PAMPA_DEBUG_MDMESH2
    SCOTCH_graphData(&cgrfdat, NULL, NULL, &tmpptr, NULL, NULL, NULL, NULL, NULL, NULL);
#else /* PAMPA_DEBUG_MDMESH2 */
    CHECK_FERR (dmeshGraphExit (&cgrfdat), dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_MDMESH2 */
  }
  else {
    errorPrint ("Invalid flag value, PAMPA_LVL_PART_GLOBAL or PAMPA_LVL_PART_LOCAL is needed");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  SCOTCH_stratExit (&strtdat);
  mesholdptr->edloloctax = (origedloloctax != NULL) ? origedloloctax : NULL;


// XXX sans drapeau temporaire tant qu'il y a effet meteorite #ifdef PAMPA_DEBUG_MDMESH2
  {
    int procngbnum;
    int tmpval;
    Gnum elemglbnbr;

    if (dgrfptr != NULL) { /* distributed graph */
      CHECK_FMPI (cheklocval, MPI_Reduce (&mesholdptr->enttloctax[baseval]->vertlocnbr, &elemglbnbr, 1, GNUM_MPI,
            MPI_SUM, 0, dsmshdat.proccomm), dsmshdat.proccomm);

      // XXX vérifier qu'il n'y a pas d'élément isolé
      if (dsmshdat.proclocnum == 0) {
        Gnum vertnbr;
        Gnum vertnum;
        Gnum vertnnd;
        Gnum colrnum;
        Gnum colrflg;
        Gnum * restrict parttax;

        if ((parttax = (Gnum *) memAlloc (elemglbnbr * sizeof (Gnum))) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VDBG2 (cheklocval);

        parttax -= baseval;

        if ((cgrfptr = (SCOTCH_Graph *) memAlloc (sizeof (SCOTCH_Graph))) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VDBG2 (cheklocval);

        CHECK_FDBG2 (SCOTCH_graphInit (cgrfptr));
        CHECK_FERR (SCOTCH_dgraphGather (dgrfptr, cgrfptr), dsmshdat.proccomm);

        tmpval = mesholdptr->enttloctax[baseval]->vertlocnbr;
        CHECK_FMPI (cheklocval, commGather (&tmpval, 1, MPI_INT,
              drcvcnttab, 1, MPI_INT, 0, dsmshdat.proccomm), dsmshdat.proccomm);
        drcvdsptab[0] = 0;
        for (procngbnum = 0; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
          drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
        }

        CHECK_FMPI (cheklocval, commGatherv (enttpartgsttax + baseval, mesholdptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              parttax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, dsmshdat.proccomm), dsmshdat.proccomm);

        cheklocval = mdmeshAggrCheck (cgrfptr, baseval, &partglbnbr, parttax);
        CHECK_FMPI (cheklocval, commScatterv (parttax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI,
              enttpartgsttax + baseval, mesholdptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI, 0, dsmshdat.proccomm), dsmshdat.proccomm);
        // XXX il manque scatterv
        memFree (parttax);
      }
      else { /* proclocnum != 0) */

        CHECK_FERR (SCOTCH_dgraphGather (dgrfptr, NULL), dsmshdat.proccomm);
        tmpval = mesholdptr->enttloctax[baseval]->vertlocnbr;
        CHECK_FMPI (cheklocval, commGather (&tmpval, 1, MPI_INT,
              NULL, 0, MPI_INT, 0, dsmshdat.proccomm), dsmshdat.proccomm);
        CHECK_FMPI (cheklocval, commGatherv (enttpartgsttax + baseval, mesholdptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              NULL, NULL, NULL, GNUM_MPI, 0, dsmshdat.proccomm), dsmshdat.proccomm);

        tmpval = mesholdptr->enttloctax[baseval]->vertlocnbr;
        CHECK_FMPI (cheklocval, commGather (&tmpval, 1, MPI_INT,
              drcvcnttab, 1, MPI_INT, 0, dsmshdat.proccomm), dsmshdat.proccomm);
        drcvdsptab[0] = 0;
        for (procngbnum = 0; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
          drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
        }

        CHECK_FMPI (cheklocval, commGatherv (enttpartgsttax + baseval, mesholdptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI,
              NULL, NULL, NULL, GNUM_MPI, 0, dsmshdat.proccomm), dsmshdat.proccomm);

        CHECK_FMPI (cheklocval, commScatterv (NULL, NULL, NULL, GNUM_MPI,
              enttpartgsttax + baseval, mesholdptr->enttloctax[baseval]->vertlocnbr, GNUM_MPI, 0, dsmshdat.proccomm), dsmshdat.proccomm);
      }
    }
    else { /* centralized graph */
      cheklocval = mdmeshAggrCheck (cgrfptr, baseval, &partlocnbr, enttpartgsttax);
      memFreeGroup (tmpptr);
    }
    CHECK_VERR (cheklocval, dsmshdat.proccomm);
  }
// XXX temporaire tant qu'il y a effet meteorite #endif /* PAMPA_DEBUG_MDMESH2 */

  parthashnbr = partlocnbr * 4;
  for (parthashsiz = 256; parthashsiz < parthashnbr; parthashsiz <<= 1) ;
  parthashnbr = 0;

  parthashmsk = parthashsiz - 1;
  parthashmax = parthashsiz >> 2;
  if ((parthashtab = (MdmeshCrLvlBldPart *) memAlloc (parthashsiz * sizeof (MdmeshCrLvlBldPart))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  memSet (parthashtab, ~0, parthashsiz * sizeof (MdmeshCrLvlBldPart));

  // * numéroter les éléments en gardant le numéro d'un seul élément par macro-élément
  // ** on parcourt enttpartgsttax
  for (vertlocidx = vertlocbas, vertlocnum = baseval, vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
  // *** si on n'a pas déjà attribué de numéro pour ce macro-élément
    for (parthashnum = (enttpartgsttax[vertlocnum] * MDMESHLVLBLDHASHPRIME) & parthashmsk; parthashtab[parthashnum].partnum != ~0 && parthashtab[parthashnum].partnum != enttpartgsttax[vertlocnum]; parthashnum = (parthashnum + 1) & parthashmsk) ;
    if (parthashtab[parthashnum].partnum != enttpartgsttax[vertlocnum]) {
      // *** on donne un numéro au nouvel élément dans la table de hachage
      parthashtab[parthashnum].partnum = enttpartgsttax[vertlocnum];
      parthashtab[parthashnum].vertnum = vertlocidx ++;
      parthashnbr ++;

      if (parthashnbr >= parthashmax) /* If hashtab is too much filled */
        CHECK_FERR (mdmeshCrLvlBldPartResize (&dsmshdat, &parthashtab, &parthashsiz, &parthashmax, &parthashmsk), dsmshdat.proccomm);
    }
  }

  // ** on re-parcourt enttpartgsttax
  for (vertlocnum = baseval, vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
    Gnum bvrtlocnum;

    bvrtlocnum = enttlocptr->mvrtloctax[vertlocnum] - vertlocbas;
    // *** on donne le nouveau numéro dans vnumgsttax avec la table de hachage
    for (parthashnum = (enttpartgsttax[vertlocnum] * MDMESHLVLBLDHASHPRIME) & parthashmsk; parthashtab[parthashnum].partnum != enttpartgsttax[vertlocnum]; parthashnum = (parthashnum + 1) & parthashmsk) ;
    vnumlvlgsttax[bvrtlocnum] = parthashtab[parthashnum].vertnum;
  }
  memFree (parthashtab);

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  if (commAllreduce(&dsmshdat.procngbnbr, &pglbngbnbr, 1, GNUM_MPI, MPI_MAX, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  if ((flagval & PAMPA_LVL_PART_GLOBAL) != 0) {
    /* In the worst case, a coarsened vertex could be on all neighbor processors */
    for (pglbngbnum = 0; pglbngbnum < pglbngbnbr; pglbngbnum ++) {
      // * communiquer la numérotation des éléments (vnumlvlgsttax)
      CHECK_FERR (dsmeshHaloSyncComm (&dsmshdat, dsmshdat.commlocptr, dsmshdat.vertlocnbr, vnumlvlgsttax, GNUM_MPI), dsmshdat.proccomm);
      for (vertlocnum = baseval, vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
        Gnum bvrtlocnum;
        Gnum edgelocnum;
        Gnum edgelocnnd;

        bvrtlocnum = enttlocptr->mvrtloctax[vertlocnum] - vertlocbas;
        for (edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[bvrtlocnum].vertlocidx, edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[bvrtlocnum].vendlocidx; edgelocnum < edgelocnnd; edgelocnum ++) {
          Gnum nghblocnum;
          Gnum bngblocnum;

          nghblocnum = dsmshdat.edgegsttax[edgelocnum];
          bngblocnum = enttlocptr->mvrtloctax[nghblocnum] - vertlocbas;
          if ((enttpartgsttax[nghblocnum] == enttpartgsttax[vertlocnum]) && (vnumlvlgsttax[nghblocnum] < vnumlvlgsttax[vertlocnum]))
            vnumlvlgsttax[bvrtlocnum] = vnumlvlgsttax[bngblocnum];
        }
      }
    }
  }
  else { /* flagval & PAMPA_LVL_PART_LOCAL */
    CHECK_FERR (dsmeshHaloSyncComm (&dsmshdat, dsmshdat.commlocptr, dsmshdat.vertlocnbr, vnumlvlgsttax, GNUM_MPI), dsmshdat.proccomm);
  }


  for (vertlocnum = baseval, vertlocnnd = baseval + dsmshdat.vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
    if (vnumlvlgsttax[vertlocnum] != ~0) {
      Gnum vertlocnm2;

      vertlocnm2 = vnumlvlgsttax[vertlocnum] - vertlocbas;
      ventloctax[vertlocnm2] = (enttglbtax == NULL) ? dsmshdat.ventloctax[vertlocnum] : enttglbtax[dsmshdat.ventloctax[vertlocnum]];
    }


  entthashnbr = (parthashnbr > 0) ? (dsmshdat.vertlocnbr * (enttlocptr->vertlocnbr / parthashnbr) * 4) : (0);
  for (entthashsiz = 256; entthashsiz < entthashnbr; entthashsiz <<= 1) ;
  entthashnbr = 0;

  entthashmsk = entthashsiz - 1;
  entthashmax = entthashsiz >> 2;


  if ((entthashtab = (MdmeshCrLvlBldEntt *) memAlloc (entthashsiz * sizeof (MdmeshCrLvlBldEntt))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  memSet (entthashtab, ~0, entthashsiz * sizeof (MdmeshCrLvlBldEntt));

  datasndnbr = 0;
  datasndsiz = MAX (50, (dsmshdat.vgstlocnbr - dsmshdat.vertlocnbr)); // XXX bon incrément ?
  if ((datasndtab = (Gnum *) memAlloc (datasndsiz * sizeof (Gnum))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  datarcvtab = NULL;
  levlglbval = lnumglbtx2[baseval];
  // * pour chaque entité de elvlglbtax
  for (enttoldidx = enttidx = baseval, enttnnd = baseval + elvlglbsz2; enttidx <= enttnnd; enttidx ++) {
    Gnum enttnum;
    int procngbnum;

    if ((enttidx == enttnnd) || (levlglbval != lnumglbtx2[enttidx])) { /* If new level */
      int datasndidx;
      int datasndid2;
      int procngbnnd;

      memSet (dsndcnttab, 0, dsmshdat.procglbnbr * sizeof (int));
      intSort7asc1 (datasndtab, datasndnbr / datadsp);

      /* The number of processors are removed */
      for (datasndid2 = datasndidx = 0; datasndidx < datasndnbr; datasndidx += datadsp, datasndid2 += datasnddsp) {
        int tmpnum;

        dsndcnttab[datasndtab[datasndidx]] += datasnddsp;
        for (tmpnum = 0; tmpnum < datasnddsp; tmpnum ++)
          datasndtab[datasndid2 + tmpnum] = datasndtab[datasndidx + tmpnum + 1];
      }
      datasndnbr -= datasndnbr / datadsp;

      /* Compute displacement sending array */
      dsnddsptab[0] = 0;
      for (procngbnum = 1, procngbnnd = dsmshdat.procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) {
        dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
      }

      datasndnbr = dsnddsptab[dsmshdat.procglbnbr];

      //* envoyer dsndcnttab et recevoir drcvcnttab
      CHECK_FMPI (cheklocval, commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, dsmshdat.proccomm), dsmshdat.proccomm);

      //* en déduire ddsp{snd,rcv}tab
      /* Compute displacement receiving array */
      drcvdsptab[0] = 0;
      for (procngbnum = 1 ; procngbnum <= dsmshdat.procglbnbr; procngbnum ++) {
        drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
      }
      datarcvnbr = drcvdsptab[dsmshdat.procglbnbr];

      //* allouer datasndtab et datarcvtab
      //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
      if (datarcvtab == NULL) {
        if ((datarcvtab = (Gnum *) memAlloc (datarcvnbr * sizeof (Gnum))) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dsmshdat.proccomm);

        datarcvsiz = datarcvnbr;
      }
      else if (datarcvnbr > datarcvsiz) {
        if ((datarcvtab = (Gnum *) memRealloc (datarcvtab, (datarcvnbr * sizeof (Gnum)))) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dsmshdat.proccomm);

        datarcvsiz = datarcvnbr;
      }

      //* envoyer datasndtab et recevoir datarcvtab
      CHECK_FMPI (cheklocval, commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, dsmshdat.proccomm), dsmshdat.proccomm);

      /* Each received vertex is compared with local data */
      for (procngbnum = 0; procngbnum < dsmshdat.procglbnbr; procngbnum ++) {
        int datarcvidx;
        int datarcvnnd;

        for (datarcvidx = drcvdsptab[procngbnum], datarcvnnd = drcvdsptab[procngbnum + 1]; datarcvidx < datarcvnnd; datarcvidx += datasnddsp) {
          Gnum enttnewnum;
          Gnum enttoldnum;
          Gnum tagnum;

          nghbloctab[0] = datarcvtab[datarcvidx + datavrt1dsp];
          nghbloctab[1] = datarcvtab[datarcvidx + datavrt2dsp];
          enttnewnum    = datarcvtab[datarcvidx + dataenttnewdsp];
          enttoldnum    = datarcvtab[datarcvidx + dataenttolddsp];
          tagnum        = datarcvtab[datarcvidx + datatagdsp];
          for (entthashnum = ((nghbloctab[0] + nghbloctab[1]) * MDMESHLVLBLDHASHPRIME) & entthashmsk;
              entthashtab[entthashnum].verttab[0] != ~0 &&
              (entthashtab[entthashnum].verttab[0] != nghbloctab[0]
               || entthashtab[entthashnum].verttab[1] != nghbloctab[1]
               || entthashtab[entthashnum].enttnewnum != enttnewnum
               || (entthashtab[entthashnum].enttoldnum == enttoldnum && entthashtab[entthashnum].tagnum != tagnum)); /* TRICK : good value if (enttoldnum are different or tagnum are equal) */
              entthashnum = (entthashnum + 1) & entthashmsk) ;
          if ((entthashtab[entthashnum].verttab[0] == nghbloctab[0]) && (entthashtab[entthashnum].verttab[1] == nghbloctab[1]) && ((entthashtab[entthashnum].enttoldnum != enttoldnum) || entthashtab[entthashnum].tagnum == tagnum)) {
            if ((procngbnum < dsmshdat.proclocnum) && (entthashtab[entthashnum].vertnum > ~0))
              entthashtab[entthashnum].vertnum = - datarcvtab[datarcvidx + datavertnumdsp] - 2; /* TRICK: - 2 in order to not overwrite vertnum for p as (procngbnum < p < proclocnum) */
            else
              datarcvtab[datarcvidx + datavertnumdsp] = (entthashtab[entthashnum].vertnum < ~0) ? (- entthashtab[entthashnum].vertnum - 2) : entthashtab[entthashnum].vertnum;
          }
          else { /* n-uplet is not present in hash table and could be necessary for other processors */
            entthashtab[entthashnum].verttab[0] = nghbloctab[0];
            entthashtab[entthashnum].verttab[1] = nghbloctab[1];
            entthashtab[entthashnum].enttnewnum = enttnewnum;
            entthashtab[entthashnum].enttoldnum = enttoldnum;
            entthashtab[entthashnum].tagnum     = tagnum;
            entthashtab[entthashnum].vertnum    = - datarcvtab[datarcvidx + datavertnumdsp] - 2;
            entthashnbr ++;

            if (entthashnbr >= entthashmax) /* If hashtab is too much filled */
              CHECK_FERR (mdmeshCrLvlBldEnttResize (&dsmshdat, &entthashtab, &entthashsiz, &entthashmax, &entthashmsk), dsmshdat.proccomm);
          }
        } /* for (datarcvidx = drcvdsptab[procngbnum], datarcvnnd = drcvdsptab[procngbnum + 1]; datarcvidx < datarcvnnd; datarcvidx += datasnddsp) */
      } /* for (procngbnum = 0; procngbnum < dsmshdat.procglbnbr; procngbnum ++) */

      /* datarcvtab which contain the true value of vertices, is sent */
      CHECK_FMPI (cheklocval, commAlltoallv(datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, dsmshdat.proccomm), dsmshdat.proccomm);

      for (datasndidx = 0; datasndidx < datasndnbr; datasndidx += datasnddsp) {
        Gnum enttnewnum;
        Gnum enttoldnum;
        Gnum tagnum;

        nghbloctab[0] = datasndtab[datasndidx + datavrt1dsp];
        nghbloctab[1] = datasndtab[datasndidx + datavrt2dsp];
        enttnewnum    = datasndtab[datasndidx + dataenttnewdsp];
        enttoldnum    = datasndtab[datasndidx + dataenttolddsp];
        tagnum        = datasndtab[datasndidx + datatagdsp];

        for (entthashnum = ((nghbloctab[0] + nghbloctab[1]) * MDMESHLVLBLDHASHPRIME) & entthashmsk;
            entthashtab[entthashnum].verttab[0] != ~0 &&
            (entthashtab[entthashnum].verttab[0] != nghbloctab[0]
             || entthashtab[entthashnum].verttab[1] != nghbloctab[1]
             || entthashtab[entthashnum].enttnewnum != enttnewnum
             || (entthashtab[entthashnum].enttoldnum == enttoldnum && entthashtab[entthashnum].tagnum != tagnum));
            entthashnum = (entthashnum + 1) & entthashmsk) ;
#ifdef PAMPA_DEBUG_MDMESH
        if ((entthashtab[entthashnum].verttab[0] != nghbloctab[0]) || (entthashtab[entthashnum].verttab[1] != nghbloctab[1]) || (entthashtab[entthashnum].enttnewnum != enttnewnum) || ((entthashtab[entthashnum].enttoldnum == enttoldnum) && (entthashtab[entthashnum].tagnum != tagnum))) {
          errorPrint ("internal error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_MDMESH */
        entthashtab[entthashnum].vertnum = datasndtab[datasndidx + datavertnumdsp];
      } /* for (datasndidx = 0; datasndidx < datasndnbr; datasndidx += datasnddsp) */

      for (entthashnum = 0; entthashnum < entthashsiz; entthashnum ++)
        if (entthashtab[entthashnum].vertnum < ~0)
          entthashtab[entthashnum].vertnum = - entthashtab[entthashnum].vertnum - 2;

      for ( ; enttoldidx < enttidx; enttoldidx ++) {
        enttnum = elvlglbtx2[enttoldidx];
        enttlocptr = mesholdptr->enttloctax[enttnum];
        if (enttlocptr == NULL)
          continue;

        // XXX on peut récupérer la valeur de la sous-entité s'il y en a une, mais
        // l'acces à tagloctax doit se faire avec la sous-entité (avoir un drapeau
        // pour savoir ;)
        if ((etagglbtax != NULL) && (etagglbtax[enttnum] != ~0)) {
          CHECK_FERR (dsmeshValueData (&dsmshdat, enttnum, etagglbtax[enttnum], NULL, NULL, (void **) &tagloctax), dsmshdat.proccomm);
        }
        else
          tagloctax = NULL;

        for (procngbnum = 0, vertlocnum = baseval, vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
          Gnum bvrtlocnum;
          Gnum nghblocnbr;
          Gnum edgelocnum;
          Gnum edgelocnnd;

          bvrtlocnum = ((mesholdptr->esublocbax[enttnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) ? enttlocptr->mvrtloctax[enttlocptr->svrtloctax[vertlocnum]] : enttlocptr->mvrtloctax[vertlocnum]) - vertlocbas;
          memSet (nghbloctab, ~0, nghblocsiz * sizeof (Gnum));
          for (nghblocnbr = 0, edgelocnum = dsmshdat.vertloctax[bvrtlocnum], edgelocnnd = dsmshdat.vendloctax[bvrtlocnum]; edgelocnum < edgelocnnd; edgelocnum ++)
            if (vnumlvlgsttax[dsmshdat.edgegsttax[edgelocnum]] != ~0)
              nghbloctab[nghblocnbr ++] = vnumlvlgsttax[dsmshdat.edgegsttax[edgelocnum]];

          // *** si le sommet est entre plusieurs éléments (sommets d'entité sup) on regarde dans la table de hachage
          if (nghblocnbr > 0) {
            Gnum * nghbloctb2;
            Gnum nghblocnb2;

            intSort1asc1 (nghbloctab, nghblocnbr);
            for (nghblocnb2 = nghblocnbr, nghbloctb2 = nghbloctab; nghblocnb2 > 1 && nghbloctb2[0] == nghbloctb2[1]; nghbloctb2 ++, nghblocnb2 --) ;
            if ((nghblocnbr == 1) || (nghblocnb2 > 1)) { /* vertices with neighbors which are collapsed must be ignored */
              Gnum enttnewnum;
              Gnum enttoldnum;
              Gnum tagnum;

              enttnewnum = (enttglbtax != NULL) ? enttglbtax[enttnum] : enttnum;
              enttoldnum = enttnum;
              tagnum = (tagloctax == NULL) ? (~0) : (tagloctax[vertlocnum]); // XXX peut-être que dans certains cas, on ne soit pas dans la bonne entité ou sous-entité
              // **** s'il n'est pas présent
              for (entthashnum = ((nghbloctb2[0] + nghbloctb2[1]) * MDMESHLVLBLDHASHPRIME) & entthashmsk;
                  entthashtab[entthashnum].verttab[0] != ~0 &&
                  (entthashtab[entthashnum].verttab[0] != nghbloctb2[0]
                   || entthashtab[entthashnum].verttab[1] != nghbloctb2[1]
                   || entthashtab[entthashnum].enttnewnum != enttnewnum
                   || (entthashtab[entthashnum].enttoldnum == enttoldnum && entthashtab[entthashnum].tagnum != tagnum));
                  entthashnum = (entthashnum + 1) & entthashmsk) ;
#ifdef PAMPA_DEBUG_MDMESH
              if ((entthashtab[entthashnum].verttab[0] != nghbloctb2[0]) || (entthashtab[entthashnum].verttab[1] != nghbloctb2[1]) || (entthashtab[entthashnum].enttnewnum != enttnewnum) || ((entthashtab[entthashnum].enttoldnum == enttoldnum) && (entthashtab[entthashnum].tagnum != tagnum))) {
                errorPrint ("internal error");
                cheklocval = 1;
              }
              CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_MDMESH */
              vnumnewgsttax[bvrtlocnum] = entthashtab[entthashnum].vertnum;
              if ((entthashtab[entthashnum].vertnum >= vertlocmin) && (entthashtab[entthashnum].vertnum <= vertlocmax))
                ventloctax[entthashtab[entthashnum].vertnum - vertlocbas] = entthashtab[entthashnum].enttnewnum;
            } /* if ((nghblocnbr == 1) || (nghblocnb2 > 1)) */
          } /* if (nghblocnbr > 0) */
        } /* for (...vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) */
      } /* for ( ; enttoldidx < enttidx; enttoldidx ++) */

      for (vertlocnum = baseval, vertlocnnd = baseval + dsmshdat.vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
        if (vnumlvlgsttax[vertlocnum] != ~0)
          vnumgsttax[vertlocnum] = vnumlvlgsttax[vertlocnum];
      memCpy (vnumlvlgsttax + baseval, vnumnewgsttax + baseval, dsmshdat.vgstlocnbr * sizeof (Gnum));
      memSet (vnumnewgsttax + baseval, ~0, dsmshdat.vertlocnbr * sizeof (Gnum));

      CHECK_FERR (dsmeshHaloSyncComm (&dsmshdat, dsmshdat.commlocptr, dsmshdat.vertlocnbr, vnumlvlgsttax, GNUM_MPI), dsmshdat.proccomm);

      memSet (entthashtab, ~0, entthashsiz * sizeof (MdmeshCrLvlBldEntt));

      datasndnbr = 0;

      if (enttidx == enttnnd) {
        for (vertlocnum = baseval, vertlocnnd = baseval + dsmshdat.vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          if (vnumlvlgsttax[vertlocnum] != ~0)
            vnumgsttax[vertlocnum] = vnumlvlgsttax[vertlocnum];
        break;
      }
    } /* if ((enttidx == enttnnd) || (levlglbval != lnumglbtx2[enttidx])) */

    enttnum = elvlglbtx2[enttidx];
    enttlocptr = mesholdptr->enttloctax[enttnum];

    if (enttlocptr == NULL)
      continue;

    // XXX on peut récupérer la valeur de la sous-entité s'il y en a une, mais
    // l'acces à tagloctax doit se faire avec la sous-entité (avoir un drapeau
    // pour savoir ;)
    if ((etagglbtax != NULL) && (etagglbtax[enttnum] != ~0)) {
      CHECK_FERR (dsmeshValueData (&dsmshdat, enttnum, etagglbtax[enttnum], NULL, NULL, (void **) &tagloctax), dsmshdat.proccomm);
    }
    else
      tagloctax = NULL;


    for (procngbnum = 0, vertlocnum = baseval, vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
      Gnum bvrtlocnum;
      Gnum nghblocnbr;
      Gnum edgelocnum;
      Gnum edgelocnnd;

      bvrtlocnum = ((mesholdptr->esublocbax[enttnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) ? enttlocptr->mvrtloctax[enttlocptr->svrtloctax[vertlocnum]] : enttlocptr->mvrtloctax[vertlocnum]) - vertlocbas;
      memSet (nghbloctab, ~0, nghblocsiz * sizeof (Gnum));
      for (nghblocnbr = 0, edgelocnum = dsmshdat.vertloctax[bvrtlocnum], edgelocnnd = dsmshdat.vendloctax[bvrtlocnum]; edgelocnum < edgelocnnd; edgelocnum ++)
        if (vnumlvlgsttax[dsmshdat.edgegsttax[edgelocnum]] != ~0)
          nghbloctab[nghblocnbr ++] = vnumlvlgsttax[dsmshdat.edgegsttax[edgelocnum]];

      // *** si le sommet est entre plusieurs éléments (sommets d'entité sup) on regarde dans la table de hachage
      if (nghblocnbr > 0) {
        Gnum * nghbloctb2;
        Gnum nghblocnb2;

        intSort1asc1 (nghbloctab, nghblocnbr);

        for (nghblocnb2 = nghblocnbr, nghbloctb2 = nghbloctab; nghblocnb2 > 1 && nghbloctb2[0] == nghbloctb2[1]; nghbloctb2 ++, nghblocnb2 --) ;
        if ((nghblocnbr == 1) || (nghblocnb2 > 1)) {
          Gnum enttnewnum;
          Gnum enttoldnum;
          Gnum tagnum;

          enttnewnum = (enttglbtax != NULL) ? enttglbtax[enttnum] : enttnum;
          enttoldnum = enttnum;
          tagnum = (tagloctax == NULL) ? (~0) : (tagloctax[vertlocnum]); // XXX peut-être que dans certains cas, on ne soit pas dans la bonne entité ou sous-entité
          // XXX affecter tagnum
          // **** s'il n'est pas présent
          for (entthashnum = ((nghbloctb2[0] + nghbloctb2[1]) * MDMESHLVLBLDHASHPRIME) & entthashmsk;
              entthashtab[entthashnum].verttab[0] != ~0 &&
              (entthashtab[entthashnum].verttab[0] != nghbloctb2[0]
               || entthashtab[entthashnum].verttab[1] != nghbloctb2[1]
               || entthashtab[entthashnum].enttnewnum != enttnewnum
               || (entthashtab[entthashnum].enttoldnum == enttoldnum && entthashtab[entthashnum].tagnum != tagnum));
              entthashnum = (entthashnum + 1) & entthashmsk) ;
          if ((entthashtab[entthashnum].verttab[0] != nghbloctb2[0]) || (entthashtab[entthashnum].verttab[1] != nghbloctb2[1]) || (entthashtab[entthashnum].enttnewnum != enttnewnum) || ((entthashtab[entthashnum].enttoldnum == enttoldnum) && (entthashtab[entthashnum].tagnum != tagnum))) {
            // ***** on l'ajoute sous forme d'un triplet (e1,e2,s)
            entthashtab[entthashnum].verttab[0] = nghbloctb2[0];
            entthashtab[entthashnum].verttab[1] = nghbloctb2[1];
            entthashtab[entthashnum].enttnewnum = enttnewnum;
            entthashtab[entthashnum].enttoldnum = enttoldnum;
            entthashtab[entthashnum].tagnum     = tagnum;
            entthashtab[entthashnum].vertnum    = vertlocidx ++;
            entthashnbr ++;

            if (entthashnbr >= entthashmax) /* If hashtab is too much filled */
              CHECK_FERR (mdmeshCrLvlBldEnttResize (&dsmshdat, &entthashtab, &entthashsiz, &entthashmax, &entthashmsk), dsmshdat.proccomm);

            if ((nghbloctb2[0] < vertlocmin) || ((nghbloctb2[1] != ~0) && (nghbloctb2[1] > vertlocmax))) { /* e1 or e2 is not a local vertex */
              Gnum tmpval;

              if (datasndnbr + datadsp > datasndsiz) {
                datasndsiz += MAX (50, (dsmshdat.vgstlocnbr - dsmshdat.vertlocnbr)); // XXX bon incrément ?
                if ((datasndtab = (Gnum *) memRealloc (datasndtab, (datasndsiz * sizeof (Gnum)))) == NULL) {
                  errorPrint ("Out of memory");
                  cheklocval = 1;
                }
                CHECK_VERR (cheklocval, dsmshdat.proccomm);
              }

              for (tmpval = 0; tmpval < 2; tmpval ++) {
                Gnum vertlocend;

                vertlocend = nghbloctb2[tmpval];
                if ((vertlocend == ~0) || ((vertlocend >= vertlocmin) && (vertlocend <= vertlocmax)))
                  continue;
                if ((dsmshdat.procvrttab[procngbnum] > vertlocend) || (dsmshdat.procvrttab[procngbnum + 1] < vertlocend)) {
                  int procngbmax;

                  for (procngbnum = 0, procngbmax = mesholdptr->procglbnbr;
                      procngbmax - procngbnum > 1; ) {
                    int                 procngbmed;

                    procngbmed = (procngbmax + procngbnum) / 2;
                    if (dsmshdat.procvrttab[procngbmed] <= vertlocend)
                      procngbnum = procngbmed;
                    else
                      procngbmax = procngbmed;
                  }
                }
#ifdef PAMPA_DEBUG_MDMESH
                if ((dsmshdat.procvrttab[procngbnum] > vertlocend) || (dsmshdat.procvrttab[procngbnum + 1] < vertlocend)) {
                  errorPrint ("internal error");
                  cheklocval = 1;
                }
                CHECK_VERR (cheklocval, dsmshdat.proccomm);
#endif /* PAMPA_DEBUG_MDMESH */
                datasndtab[datasndnbr] = procngbnum;
                datasndtab[datasndnbr + dataprocdsp + datavrt1dsp] = nghbloctb2[0];
                datasndtab[datasndnbr + dataprocdsp + datavrt2dsp] = nghbloctb2[1];
                datasndtab[datasndnbr + dataprocdsp + dataenttnewdsp] = enttnewnum;
                datasndtab[datasndnbr + dataprocdsp + dataenttolddsp] = enttoldnum;
                datasndtab[datasndnbr + dataprocdsp + datatagdsp] = tagnum;
                datasndtab[datasndnbr + dataprocdsp + datavertnumdsp] = entthashtab[entthashnum].vertnum;
                datasndnbr += datadsp;
              } /* for (tmpval = 0; tmpval < 2; tmpval ++) */
            } /* if e1 or e2 is not a local vertex */
          } /* if vertex is not in hash tab entthashtab */
        } /* if ((nghblocnbr == 1) || (nghblocnb2 > 1)) */
      } /* if (nghblocnbr > 0) */
    } /* for (...vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) */
  } /* for (...enttnnd = baseval + elvlglbsz2; enttidx <= enttnnd; enttidx ++) */

  memFree (datasndtab);
  memFree (datarcvtab);
  memFree (entthashtab);

  // XXX début de partie à modifier si on veut utiliser dgraphPart au lieu de
  // graphPart, vu qu'un sommet agrégé peut se trouver sur plusieurs processeurs
  // * il faudrait parcourir la entthashtab
  // ** pour chaque nouveau sommet n'étant pas local
  // *** le communiquer au processeur dont il est le possesseur
  // ** ajouter les sommets reçus uniquement s'ils ne sont pas déjà présent (en utilisant la verthashtab)
  // * nous n'aurions plus également la correspondance entre les deux niveaux,
  //   parce qu'un sommet local ne l'est pas forcément dans un niveau supérieur
  // ** du coup, cela demande de reconstruire le niveau précédent en modifiant
  //    le recouvrement (passer hashtab au dmeshBuild qui appellerait dmeshOverlap2
  //    et non dmeshOverlap)
  // *** le problème réside dans comment déterminer efficacement les éléments
  //     locaux qui font partie du recouvrement d'autres processeurs
  // **** peut-être envoyer pour chaque proc, les éléments qui font partie du
  //      recouvrement
  // **** construire la ovlphashtab (pour dmeshOverlap) à partir des sommets reçus.
  // **** stocker le nombre d'éléments dans la ovlphashtab, comme par exemple hashnb2
  // **** pour chaque élément dans la entthashtab
  // ***** si le nouveau numéro du sommet n'est pas local
  // ****** on l'ajoute dans ovlphashtab

  verthashnbr = dsmshdat.degrlocmax * 4;
  for (verthashsiz = 256; verthashsiz < verthashnbr; verthashsiz <<= 1) ;
  verthashnbr = 0;

  verthashmsk = verthashsiz - 1;
  verthashmax = verthashsiz >> 2;

  if ((verthashtab = (MdmeshCrLvlBldVert *) memAlloc (verthashsiz * sizeof (MdmeshCrLvlBldVert))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshdat.proccomm);

  memSet (verthashtab, ~0, verthashsiz * sizeof (MdmeshCrLvlBldVert));

  CHECK_FERR (dsmeshHaloSyncComm (&dsmshdat, dsmshdat.commlocptr, dsmshdat.vertlocnbr, vnumgsttax, GNUM_MPI), dsmshdat.proccomm);

  // * pour chaque sommet
  for (edgelocidx = vertlocnum = baseval, vertlocnnd = baseval + dsmshdat.vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
    if ((vnumgsttax[vertlocnum] != ~0) && (vnumgsttax[vertlocnum] >= vertlocmin) && (vnumgsttax[vertlocnum] <= vertlocmax) && (vertloctax[vnumgsttax[vertlocnum] - vertlocbas] == ~0)) {
      Gnum vertnewlocnum;
      Gnum vertlocnm2;
      Gnum edgelocnum;
      Gnum edgelocnnd;

      memSet (verthashtab, ~0, verthashsiz * sizeof (MdmeshCrLvlBldVert));
      for (vertlocnm2 = vertlocnum; vertlocnm2 < vertlocnnd; vertlocnm2 ++) {
        if (vnumgsttax[vertlocnum] != vnumgsttax[vertlocnm2])
          continue;

        // ** pour chaque sommet voisin « v » avec vnumgsttax != ~0
        for (edgelocnum = dsmshdat.vertloctax[vertlocnm2], edgelocnnd = dsmshdat.vendloctax[vertlocnm2]; edgelocnum < edgelocnnd; edgelocnum ++) {
          Gnum nghblocnum;

          nghblocnum = vnumgsttax[dsmshdat.edgegsttax[edgelocnum]];
          if ((nghblocnum != ~0) && (nghblocnum != vnumgsttax[vertlocnum])) {
            for (verthashnum = (nghblocnum * MDMESHLVLBLDHASHPRIME) & verthashmsk; verthashtab[verthashnum].vertnum != ~0 && verthashtab[verthashnum].vertnum != nghblocnum; verthashnum = (verthashnum + 1) & verthashmsk) ;
            if (verthashtab[verthashnum].vertnum != nghblocnum) {
              verthashtab[verthashnum].vertnum = nghblocnum;
              verthashnbr ++;

              if (verthashnbr >= verthashmax) /* If hashtab is too much filled */
                CHECK_FERR (mdmeshCrLvlBldVertResize (&dsmshdat, &verthashtab, &verthashsiz, &verthashmax, &verthashmsk), dsmshdat.proccomm);
            }
          }
        }
      }

      vertnewlocnum = vnumgsttax[vertlocnum] - vertlocbas;
      vertloctax[vertnewlocnum] = edgelocidx;
        for (verthashnum = 0; verthashnum < verthashsiz; verthashnum ++)
          if (verthashtab[verthashnum].vertnum != ~0) {
            edgeloctax[edgelocidx ++] = verthashtab[verthashnum].vertnum;
            // XXX et pour edloloctax ?
          }
      vendloctax[vertnewlocnum] = edgelocidx;
    }
  }
  edgelocnbr = edgelocidx;

  memFree (verthashtab);

  // * construire le dmesh (dmeshbuild)
  CHECK_FERR (dmeshBuild (meshcurptr, baseval, dsmshdat.vertlocnbr /* vertlocidx - vertlocbas */, dsmshdat.vertlocmax, vertloctax, vendloctax, edgelocnbr, edgelocnbr, edgeloctax, NULL, dsmshdat.enttglbnbr, ventloctax, dsmshdat.esubloctax, dsmshdat.valslocptr->valuglbmax, dsmshdat.ovlpglbval, NULL, ~0, NULL, NULL, NULL, ~0, ~0, NULL), mesholdptr->proccomm); // XXX et pour edloloctax ??
#ifdef PAMPA_DEBUG_MDMESH
  CHECK_FERR (dmeshCheck (meshcurptr), mesholdptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH */
  dsmeshExit (&dsmshdat);


  // XXX pré-traitement pour supprimer les sous-entités de ventloctax
  for (vertlocnum = baseval, vertlocnnd = baseval + mesholdptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
    Gnum ventlocnum;

    ventlocnum = ventloctax[vertlocnum];

    if ((ventlocnum != ~0) && (mesholdptr->esublocbax[ventlocnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE)) /* If ventlocnum is a sub-entity */
      ventloctax[vertlocnum] = mesholdptr->esublocbax[ventlocnum & mesholdptr->esublocmsk];
  }

  CHECK_FERR (mdmeshMeshLevels (meshcurptr, mesholdptr, ventloctax, vnumgsttax, enttglbtax), mesholdptr->proccomm);
  memFreeGroup (enttpartgsttax + baseval);
  // XXX on pourrait faire en sorte de mutualiser certaines données au sein du
  // mdmesh (comme par exemple esublocbax)

  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

static
  int
mdmeshCrLvlBldPartResize (
    Dsmesh * restrict const    dsmshptr,              /**< mesh */
    MdmeshCrLvlBldPart * restrict * hashtabptr,
    Gnum * restrict const           hashsizptr,
    Gnum * restrict const           hashmaxptr,
    Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;
  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (MdmeshCrLvlBldPart *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (MdmeshCrLvlBldPart)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshptr->proccomm);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (MdmeshCrLvlBldPart)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].partnum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum partnum;

    partnum = (*hashtabptr)[hashidx].partnum;

    if (partnum == ~0) // If empty slot
      continue;

    for (hashnum = (partnum * MDMESHLVLBLDHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].vertnum != ~0 && (*hashtabptr)[hashnum].partnum != partnum; hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].partnum = ~0;
    (*hashtabptr)[hashidx].vertnum = ~0;
  }
  return (0);
}

static
  int
mdmeshCrLvlBldEnttResize (
    Dsmesh * restrict const    dsmshptr,              /**< mesh */
    MdmeshCrLvlBldEntt * restrict * hashtabptr,
    Gnum * restrict const           hashsizptr,
    Gnum * restrict const           hashmaxptr,
    Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;
  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (MdmeshCrLvlBldEntt *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (MdmeshCrLvlBldEntt)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshptr->proccomm);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (MdmeshCrLvlBldEntt)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].verttab[0] != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum verttab[2];
    Gnum enttnewnum;
    Gnum enttoldnum;
    Gnum tagnum;

    verttab[0] = (*hashtabptr)[hashidx].verttab[0];
    verttab[1] = (*hashtabptr)[hashidx].verttab[1];
    enttnewnum = (*hashtabptr)[hashidx].enttnewnum;
    enttoldnum = (*hashtabptr)[hashidx].enttoldnum;
    tagnum = (*hashtabptr)[hashidx].tagnum;

    if (verttab[0] == ~0) // If empty slot
      continue;

    for (hashnum = ((verttab[0] + verttab[1]) * MDMESHLVLBLDHASHPRIME) && (*hashmskptr);
        (*hashtabptr)[hashnum].verttab[0] != ~0 &&
        ((*hashtabptr)[hashnum].verttab[0] != verttab[0]
         || (*hashtabptr)[hashnum].verttab[1] != verttab[1]
         || (*hashtabptr)[hashnum].enttnewnum != enttnewnum
         || ((*hashtabptr)[hashnum].enttoldnum == enttoldnum && (*hashtabptr)[hashnum].tagnum != tagnum)); /* TRICK : good value if (enttoldnum are different or tagnum are equal) */
        hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].verttab[0] = ~0;
    (*hashtabptr)[hashidx].verttab[1] = ~0;
    (*hashtabptr)[hashidx].enttnewnum = ~0;
    (*hashtabptr)[hashidx].enttoldnum = ~0;
    (*hashtabptr)[hashidx].tagnum = ~0;

  }
  return (0);
}


static
  int
mdmeshCrLvlBldVertResize (
    Dsmesh * restrict const    dsmshptr,              /**< mesh */
    MdmeshCrLvlBldVert * restrict * hashtabptr,
    Gnum * restrict const           hashsizptr,
    Gnum * restrict const           hashmaxptr,
    Gnum * restrict const           hashmskptr)
{
  Gnum                          oldhashsiz;          /* Size of hash table    */
  Gnum                          hashnum;          /* Hash value            */
  Gnum                          oldhashmsk;
  int                           cheklocval;
  Gnum hashidx;
  Gnum hashtmp;

  cheklocval = 0;
  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (MdmeshCrLvlBldVert *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (MdmeshCrLvlBldVert)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dsmshptr->proccomm);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (MdmeshCrLvlBldVert)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].vertnum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    Gnum vertlocnum;

    vertlocnum = (*hashtabptr)[hashidx].vertnum;

    if (vertlocnum == ~0) // If empty slot
      continue;

    for (hashnum = (vertlocnum * MDMESHLVLBLDHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].vertnum != ~0 && (*hashtabptr)[hashnum].vertnum != vertlocnum; hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].vertnum = ~0;
  }
  return (0);
}
