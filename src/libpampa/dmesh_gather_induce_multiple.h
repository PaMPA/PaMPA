/*  Copyright XXX-2017 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_gather_induce_multiple.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This file is the header to  gather a
//!                distributed mesh and induce multiple
//!                centralized meshes.
//!
//!
/************************************************************/


#ifdef DMESH_GATHER_INDUCE_MULTIPLE
typedef struct DataSndEntt_ {
  Gnum enttnum;
  Gnum vertnum;
  Gnum procnum;
} DataSndEntt;

typedef struct DataRcvEntt_ {
  Gnum enttnum;
  Gnum compidx;
  Gnum vertnum;
  Gnum procnum;
  Gnum indxnum;
  Gnum indxnm2;
} DataRcvEntt;

typedef struct VertProc_ {
  Gnum vertnbr;
  Gnum procnum;
} VertProc;

static Gnum idcpt = 1;
#endif /* DMESH_GATHER_INDUCE_MULTIPLE */

typedef struct dmeshGatherInfo_ {
  int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval);
  void * dataptr;
} dmeshGatherInfo;   



int dmeshGatherInduceMultiple (
    Dmesh * restrict const    meshptr,
    const Gnum                adptflg,
    const Gnum                partnbr,
    Gnum * const              vnumloctax,
    Gnum * const              partloctax,
    Gnum * const              meshlocnbr,
    Mesh **                   meshloctab);

int dmeshGatherInduceMultiple2 (
    Dmesh * restrict const    meshptr,
    const Gnum                adptflg,
    dmeshGatherInfo * const infoptr,
    const Gnum                partnbr,
    Gnum * const              vnumloctax,
    Gnum * const              partloctax,
    Gnum * const              meshlocnbr,
    Mesh **                   meshloctab);
