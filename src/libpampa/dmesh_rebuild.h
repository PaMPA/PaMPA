/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_rebuild.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 10 Sep 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESHREBLDHASHPRIME     17            //!< Prime number for hashing
#define TAG_INT_GLOBAL          -50 // FIXME trouver un meilleur moyen pour attribuer la valeur
#define TAG_INT_PROCVRT         -51 // FIXME trouver un meilleur moyen pour attribuer la valeur

#ifdef DMESH_REBUILD
#define DSTVERTLOCNUM(v) ((v) < (dsmshdat.vertlocnbr + baseval)) ? (v + dstvertlocbas) : vprmgsttax[(v)]
typedef struct SmeshRcv_ {
  Gnum smshnum;
  Gnum dataidx;
  Gnum vertmin;
  Gnum vertmax;
} SmeshRcv;

typedef struct PermVert_ {
  Gnum vertnum;
  Gnum vprmnum;
} PermVert;

typedef struct PermNdst_ { // Permutation for distant neighbors
  Gnum nghbnum;
  Gnum nprmnum;
} PermNdst;

typedef struct ProcNdst_ {
  Gnum procnum;
  Gnum nghbnum;
} ProcNdst;

typedef struct DataSndEntt_ {
  Gnum enttnum;
  Gnum smshidx;
  Gnum procnum;
  Gnum vertnum;
  Gnum vertnm2;
} DataSndEntt;

typedef struct DataRcvEntt_ {
  Gnum enttnum;
  Gnum procnum;
  Gnum smshnum;
  Gnum vertnum;
  Gnum vprmnum;
} DataRcvEntt;

typedef union ProcSmesh_
{
  int procnum;
  Gnum smshidx;
} ProcSmesh;

typedef struct DmeshRebldSmeshVert_                    //!  XXX
{
  Gnum          vertnum;                                //!< XXX
  ProcSmesh     prshval; 
#ifdef PAMPA_DEBUG_DMESH2
  Gnum          smshnum;
#endif /* PAMPA_DEBUG_DMESH2 */
} DmeshRebldSmeshVert;                                 //!< XXX

typedef struct NdstAdj_ { /* Adjacency list for distant neighbors */
  Gnum indxnum;
  Gnum indxnnd;
} NdstAdj;

typedef struct ProcLoad_ {
  Gnum loadval;
  Gnum procnum; /* Gnum instead of int for sorting elements */
} ProcLoad;

typedef struct ZoneLoad_ {
  Gnum loadval;
  Gnum idnum;
  Gnum procnum; /* Gnum instead of int for sorting elements */
  Gnum procidx; // proc?? ou datarcv??
} ZoneLoad;

typedef struct ZoneProc_ {
  Gnum procsrc; /* Gnum instead of int for sorting elements */
  Gnum idnum;
  Gnum procdst; /* Gnum instead of int for sorting elements */
} ZoneProc;

static
int
dmeshRebldSmeshVertResize (
  	Dsmesh * restrict const    dmshptr,              /**< mesh */
	DmeshRebldSmeshVert * restrict * hashtabptr,
	Gnum * restrict const           hashsizptr,
	Gnum * restrict const           hashmaxptr,
	Gnum * restrict const           hashmskptr);
#endif /* DMESH_REBUILD */

  int
dmeshRebuild (
    Dmesh * restrict const    dmshptr,
    const Gnum                meshnbr,
    Mesh *                    meshloctab);

  int
dsmeshRebuild (
    Dmesh * restrict const    dmshptr,
    const Gnum                meshnbr,
    Smesh *                   smshloctab);

