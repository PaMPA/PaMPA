/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_data.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the distributed mesh
//!                private data accessors.
//!
//!   \date        Version 1.0: from: 22 Aug 2011
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief This routine accesses mesh size data.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \returns VOID  : in all cases.

void
PAMPA_dmeshSize (
const PAMPA_Dmesh * const  meshptr,              //!< Distributed mesh structure to read
PAMPA_Num * const          enttglbptr,           //!< Number of entities which make up the dmesh
PAMPA_Num * const          vertlocptr,           //!< Number of local vertices
PAMPA_Num * const          vgstlocptr,           //!< Number of local + ghost vertices
PAMPA_Num * const          vertglbptr)           //!< Number of global vertices
{
  const Dmesh *       srcmeshptr;
  PAMPA_Num vertlocnbr;
  PAMPA_Num vgstlocnbr;
  PAMPA_Num vertglbnbr;
  Gnum enttnum, enttnnd;


  srcmeshptr = (Dmesh *) meshptr;

  vertlocnbr = (PAMPA_Num) srcmeshptr->vertlocnbr;
  vertglbnbr =
  vgstlocnbr = 0;

  for (enttnum = (Gnum) srcmeshptr->baseval, enttnnd = (Gnum) srcmeshptr->enttglbnbr + srcmeshptr->baseval ; enttnum < enttnnd ; enttnum ++) {

    if ((srcmeshptr->enttloctax[enttnum] != NULL) && (srcmeshptr->esublocbax[enttnum & srcmeshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)) {
      vgstlocnbr += srcmeshptr->enttloctax[enttnum]->vgstlocnbr;
      vertglbnbr += srcmeshptr->enttloctax[enttnum]->vertglbnbr;
    }
  }


  
  if (vertlocptr != NULL)
    *vertlocptr = vertlocnbr;
  if (vgstlocptr != NULL)
    *vgstlocptr = vgstlocnbr;
  if (vertglbptr != NULL)
    *vertglbptr = vertglbnbr;
  if (enttglbptr != NULL)
    *enttglbptr = (PAMPA_Num) (srcmeshptr->enttglbnbr);
}

//! \brief This routine accesses mesh entity size data.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_dmeshEnttSize (
const PAMPA_Dmesh * const  meshptr,              //!< Distributed mesh structure to read
PAMPA_Num const            enttlocnum,           //!< Number of entity for which size data will be given
...)
{
  va_list             taglist;                    /* Argument list of the call              */
  const Dmesh *       srcmeshptr;
  PAMPA_Num *         vertlocptr;
  PAMPA_Num           flaglocval;
  PAMPA_Num           baseval;
  int cheklocval;

  srcmeshptr = (Dmesh *) meshptr;
  baseval = srcmeshptr->baseval;

#ifdef PAMPA_DEBUG_DMESH
  if ((enttlocnum < srcmeshptr->baseval) || (enttlocnum >= srcmeshptr->enttglbnbr + srcmeshptr->baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
#endif /* PAMPA_DEBUG_DMESH */

  if ((srcmeshptr->enttloctax[enttlocnum] != NULL) && (srcmeshptr->enttloctax[enttlocnum]->vprmloctax == NULL)) {
    CHECK_FERR (dmeshItBuild(srcmeshptr, enttlocnum), srcmeshptr->proccomm);
  }

  va_start (taglist, enttlocnum);                     /* Start argument parsing         */
  while (1) {
    vertlocptr = va_arg (taglist, PAMPA_Num *);

    if (vertlocptr == NULL)
      break;
    flaglocval = va_arg (taglist, PAMPA_Num);

#ifdef PAMPA_DEBUG_DMESH
    {
      if (srcmeshptr->enttloctax[enttlocnum] == NULL) {
        errorPrint ("Invalid entity number");
        return (1);
      }
    }
#endif /* PAMPA_DEBUG_DMESH */
    switch ((int) flaglocval) {
      case PAMPA_VERT_GLOBAL : 
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vertglbnbr;
        break;
      case PAMPA_VERT_LOCAL & PAMPA_VERT_OVERLAP : 
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vovplocnbr;
        break;
      case PAMPA_VERT_LOCAL & PAMPA_VERT_GHOST : 
      case PAMPA_VERT_ANY : 
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vgstlocnbr;
        break;
      case PAMPA_VERT_LOCAL : 
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vertlocnbr;
        break;
      case PAMPA_VERT_LOCAL & PAMPA_VERT_NOT_FRONTIER : 
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vfrnlocmin - baseval;
        break;
      case PAMPA_VERT_BOUNDARY & PAMPA_VERT_FRONTIER :
      case PAMPA_VERT_LOCAL & PAMPA_VERT_FRONTIER : 
        *vertlocptr = baseval + srcmeshptr->enttloctax[enttlocnum]->vertlocnbr - srcmeshptr->enttloctax[enttlocnum]->vfrnlocmin;
        break;
      case PAMPA_VERT_INTERNAL : 
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vintlocnbr;
        break;
      case PAMPA_VERT_BOUNDARY :
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vertlocnbr - srcmeshptr->enttloctax[enttlocnum]->vintlocnbr;
        break;
      case PAMPA_VERT_OVERLAP & PAMPA_VERT_FRONTIER :
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vfrnlocmax - baseval - srcmeshptr->enttloctax[enttlocnum]->vertlocnbr;
        break;
      case PAMPA_VERT_OVERLAP & PAMPA_VERT_NOT_FRONTIER :
        *vertlocptr = baseval + srcmeshptr->enttloctax[enttlocnum]->vovplocnbr - srcmeshptr->enttloctax[enttlocnum]->vfrnlocmax - 1;
        break;
      case PAMPA_VERT_FRONTIER :
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vfrnlocmax - srcmeshptr->enttloctax[enttlocnum]->vfrnlocmin;
        break;
      case PAMPA_VERT_OVERLAP :
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vovplocnbr - srcmeshptr->enttloctax[enttlocnum]->vertlocnbr;
        break;
      case PAMPA_VERT_GHOST :
        *vertlocptr = srcmeshptr->enttloctax[enttlocnum]->vgstlocnbr - srcmeshptr->enttloctax[enttlocnum]->vertlocnbr;
        break;
      default:
        errorPrint ("Unrecognized flag");
        return (1);
    }
  }

  return (0);
}


//! \brief This routine accesses mesh entity size data.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \deprecated This function must not be used 
//! \todo used by Fortran Interface, this must be modified
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_dmeshEnttSizeOld (
const PAMPA_Dmesh * const  meshptr,              //!< Distributed mesh structure to read
PAMPA_Num const            enttlocnum,           //!< Number of entity for which size data will be given
PAMPA_Num * const          vintlocptr,           //!< Number of internal vertices which have the entity number
PAMPA_Num * const          vertlocptr,           //!< Number of local vertices which have the entity number
PAMPA_Num * const          vovplocptr,           //!< Number of local + overlap vertices which have the entity number
PAMPA_Num * const          vgstlocptr,           //!< Number of local + ghost vertices which have the entity number
PAMPA_Num * const          vertglbptr)           //!< Number of global vertices which have the entity number
{
  const Dmesh *       srcmeshptr;
  int cheklocval;

  srcmeshptr = (Dmesh *) meshptr;

#ifdef PAMPA_DEBUG_DMESH
  if ((enttlocnum < srcmeshptr->baseval) || (enttlocnum >= srcmeshptr->enttglbnbr + srcmeshptr->baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
#endif /* PAMPA_DEBUG_DMESH */

  if (vintlocptr != NULL)
    *vintlocptr = (srcmeshptr->enttloctax[enttlocnum] != NULL) ? (PAMPA_Num) (srcmeshptr->enttloctax[enttlocnum]->vintlocnbr) : 0;
  if (vertlocptr != NULL)
    *vertlocptr = (srcmeshptr->enttloctax[enttlocnum] != NULL) ? (PAMPA_Num) (srcmeshptr->enttloctax[enttlocnum]->vertlocnbr) : 0;
  if (vgstlocptr != NULL)
    *vgstlocptr = (srcmeshptr->enttloctax[enttlocnum] != NULL) ? (PAMPA_Num) (srcmeshptr->enttloctax[enttlocnum]->vgstlocnbr) : 0;
  if (vertglbptr != NULL)
    *vertglbptr = (srcmeshptr->enttloctax[enttlocnum] != NULL) ? (PAMPA_Num) (srcmeshptr->enttloctax[enttlocnum]->vertglbnbr) : 0;
  if (vovplocptr != NULL) {
    if ((srcmeshptr->enttloctax[enttlocnum] != NULL) && (srcmeshptr->enttloctax[enttlocnum]->vprmloctax == NULL)) {
      cheklocval = dmeshItBuild((Dmesh * const) meshptr, enttlocnum);
      if (cheklocval != 0)
        return cheklocval;
    }
    *vovplocptr = (srcmeshptr->enttloctax[enttlocnum] != NULL) ? (PAMPA_Num) (srcmeshptr->enttloctax[enttlocnum]->vovplocnbr) : 0;
  }

  return (0);
}
//! \brief This routine accesses all of the mesh data.
//! NULL pointers on input indicate unwanted
//! data. NULL pointers on output indicate
//! unexisting arrays.
//!
//! \returns VOID  : in all cases.

void
PAMPA_dmeshData (
const PAMPA_Dmesh * const  meshptr,              //!< Distributed mesh structure to read
PAMPA_Num * const          baseptr,              //!< Base value             
PAMPA_Num * const          vertlocptr,           //!< Number of local vertices
PAMPA_Num * const          vertlocptz,           //!< Maximum number of local vertices
PAMPA_Num * const          vertgstptr,           //!< Number of local + ghost vertices
PAMPA_Num ** const         vertloctab,           //!< Local vertex begin array
PAMPA_Num ** const         vendloctab,           //!< Local vertex end array
PAMPA_Num * const          edgelocptr,           //!< Number of local links
PAMPA_Num * const          edgelocptz,           //!< Size of local link array
PAMPA_Num ** const         edgeloctab,           //!< Local link array
PAMPA_Num ** const         edloloctab,           //!< Local edge load array (if any)
PAMPA_Num * const          enttglbptr,           //!< Number of global entities
PAMPA_Num ** const         ventloctab,           //!< Local vertex entity array
PAMPA_Num ** const         esubloctab,           //!< Local sub-entity array
PAMPA_Num * const          valuglbptr,           //!< Number of linked values
PAMPA_Num * const          valuglbptz,           //!< Size of linked value array
PAMPA_Num * const          ovlpglbptr,           //!< Global overlap value
int       * const          procngbptr,           //!< Number of neighboring processes
int ** const               procngbtab,           //!< Array of neighbor process numbers [sorted]
MPI_Comm * const           proccomm)             //!< Communicator
{
  const Dmesh *       srcmeshptr;                 /* Pointer to source mesh structure */

  srcmeshptr = (const Dmesh *) meshptr;
  PAMPA_Num vertlocnbr, vgstlocnbr;
  Gnum enttnum, enttnnd;

  vertlocnbr = (PAMPA_Num) srcmeshptr->vertlocnbr;
  vgstlocnbr = 0;

  for (enttnum = (Gnum) srcmeshptr->baseval, enttnnd = (Gnum) srcmeshptr->enttglbnbr + srcmeshptr->baseval ; enttnum < enttnnd ; enttnum ++) {
	if ((srcmeshptr->enttloctax[enttnum] != NULL) && (srcmeshptr->esublocbax[enttnum & srcmeshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)) 
      vgstlocnbr += srcmeshptr->enttloctax[enttnum]->vgstlocnbr;
  }

  if ((srcmeshptr->baseval == 0) // except fortran
      & ((vertloctab != NULL) || (vendloctab != NULL) || (edgeloctab != NULL) || (edloloctab != NULL) || (ventloctab != NULL) || (esubloctab != NULL)))
	errorPrint("vertloctab, vendloctab, edgeloctab, edloloctab, ventloctab and esubloctab are not yet available");

  if (baseptr != NULL)
    *baseptr = srcmeshptr->baseval;
  if (enttglbptr != NULL)
    *enttglbptr = srcmeshptr->enttglbnbr;
  if (vertlocptr != NULL)
    *vertlocptr = vertlocnbr;
  if (vertgstptr != NULL)
    *vertgstptr = vgstlocnbr;
  if (edgelocptz != NULL)
    *edgelocptz = srcmeshptr->edgelocsiz;
  if (edgelocptr != NULL)
    *edgelocptr = srcmeshptr->edgelocnbr;
  if (edgeloctab != NULL)
    *edgeloctab = srcmeshptr->edgeloctax + srcmeshptr->baseval;
  if (valuglbptr != NULL)
    *valuglbptr = srcmeshptr->valslocptr->valuglbnbr;
  if (valuglbptz != NULL)
    *valuglbptz = srcmeshptr->valslocptr->valuglbmax;
  if (ovlpglbptr != NULL)
    *ovlpglbptr = srcmeshptr->ovlpglbval;
  if (procngbptr != NULL)
    *procngbptr = srcmeshptr->procngbnbr;
  if (procngbtab != NULL)
    *procngbtab = srcmeshptr->procngbtab;
  if (proccomm != NULL)
    *proccomm = srcmeshptr->proccomm;
}

//! \brief This routine accesses the global array of vertex number ranges
//!
//! \returns VOID  : in all cases.

void
PAMPA_dmeshData2 (
const PAMPA_Dmesh * const  meshptr,              //!< Distributed mesh structure to read
PAMPA_Num ** const         procvrtptr)           //!< Global array of vertex number ranges [1,based]
{
  const Dmesh *       srcmeshptr;                 /* Pointer to source mesh structure */

  srcmeshptr = (const Dmesh *) meshptr;

  if (procvrtptr != NULL)
    *procvrtptr = srcmeshptr->procvrttab;
}

//! \brief This routine gives the number of neighors of a local vertex and his
//! global number.
//! Neighbors could belong to one entity or all entities with value -1.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int 
PAMPA_dmeshVertData (
const PAMPA_Dmesh * const  meshptr,              //!< Distributed mesh structure to read
const PAMPA_Num            vertlocnum,           //!< Local vertex number
const PAMPA_Num            ventlocnum,           //!< Entity number of local vertex
const PAMPA_Num            nentlocnum,           //!< Entity number of neighbors
PAMPA_Num * const          nghbgstptr,           //!< Number of neighbors
PAMPA_Num * const          vertglbptr,           //!< Global number of vertex
int * const                procptr)              //!< Processor number of vertex
{
  const Dmesh *       srcmeshptr;                 /* Pointer to source mesh structure */
  DmeshEnttNghb **    nghbloctax;
  Gnum                vertglbnum;

  srcmeshptr = (const Dmesh *) meshptr;
  nghbloctax = srcmeshptr->enttloctax[ventlocnum]->nghbloctax;

  if (nghbgstptr != NULL) {
    if (nentlocnum == -1) {
      *nghbgstptr = nghbloctax[srcmeshptr->enttglbnbr + srcmeshptr->baseval - 1]->vindloctax[vertlocnum].vendlocidx -
        nghbloctax[srcmeshptr->baseval]->vindloctax[vertlocnum].vertlocidx;
    }
    else {
      *nghbgstptr = nghbloctax[nentlocnum]->vindloctax[vertlocnum].vendlocidx -
        nghbloctax[nentlocnum]->vindloctax[vertlocnum].vertlocidx;
    }
  }

  if ((vertglbptr != NULL) || (procptr != NULL))
    vertglbnum = srcmeshptr->enttloctax[ventlocnum]->mvrtloctax[vertlocnum];


  if (vertglbptr != NULL) 
    *vertglbptr = vertglbnum;



  // XXX n'est-ce pas proclocnum ???
  // le piège doit venir du recouvrement :
  // vertlocnum n'est pas forcément local
  if (procptr != NULL) {
    int                 procngbmax;

    for (*procptr = 0, procngbmax = srcmeshptr->procglbnbr;
        procngbmax - *procptr > 1; ) {
      int                 procngbmed;

      procngbmed = (procngbmax + *procptr) / 2;
      if (srcmeshptr->procvrttab[procngbmed] <= vertglbnum)
        *procptr = procngbmed;
      else
        procngbmax = procngbmed;
    }
  }
    



  return (0);
}

