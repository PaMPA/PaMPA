
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines. */
/*                                      */
/****************************************/

int PAMPA_meshCreate (
    PAMPA_Mesh * restrict const      meshptr,           //!< mesh
    const PAMPA_Num                  vertnbr,           //!< Number of local vertices
    PAMPA_Num * const                verttab,           //!< Local vertex begin array
    PAMPA_Num * const                vendtab,           //!< Local vertex end array
    const PAMPA_Num                  edgenbr,           //!< Number of local edges
    PAMPA_Num * restrict const       edgetab,           //!< Local edge array
    PAMPA_Num                        enttnbr,		   //!< XXX
    PAMPA_Num *                      venttab,		   //!< XXX
    const PAMPA_Num                  baseval,           //!< Base for indexing
    PAMPA_Num                        degnghb)
{
  Mesh *                   srcmeshptr;         /* Pointer to source mesh structure          */
  Gnum *                   venttax;
  Gnum *                   verttax;
  Gnum *                   vendtax;
  Gnum *                   edgetax;


#ifdef PAMPA_DEBUG_LIBRARY1
  if (sizeof (PAMPA_Mesh) < sizeof (Mesh)) {
    errorPrint ("internal error");
    return     (1);
  }
#endif /* PAMPA_DEBUG_LIBRARY1 */

  // XXX : faire l'appel à graphdata pour avoir le degrmax !
  
  srcmeshptr = (Mesh *) meshptr;                /* Use structure as source mesh */

  verttax = (Gnum *) verttab - baseval;
  vendtax = ((vendtab == NULL) || (vendtab == verttab + 1)) ? verttax + 1 : (Gnum *) vendtab - baseval;
  edgetax = (Gnum *) edgetab - baseval;
  venttax = (Gnum *) venttab - baseval;
  
  return meshCreate (srcmeshptr, vertnbr, verttax, vendtax, edgenbr, edgetax, enttnbr, venttab, baseval, degnghb);
}
