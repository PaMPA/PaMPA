/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_mat.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API for the
//!                distributed mesh handling routines of the
//!                libPampa library. 
//!
//!   \date        Version 1.0: from: 16 Sep 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* mesh matrix handling routines.       */
/*                                      */
/****************************************/

//! \brief This routine initialize and fills the internal structure of access
//! matrix arrays.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_dmeshMatInit (
PAMPA_Dmesh * const         meshptr,                                    //!< Distributed mesh
PAMPA_Num const             enttlocnum)                                 //!< Entity number
{

  return dmeshMatInit((const Dmesh *) meshptr, enttlocnum);

}

//! \brief This routine accesses matrix size data.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_dmeshMatSize (
PAMPA_Dmesh * const         meshptr,                                    //!< Distributed mesh
PAMPA_Num const             enttlocnum,                                 //!< Entity number
PAMPA_Num *                 matlocsiz)                                  //!< Matrix size
{

  const Dmesh *       srcmeshptr;                 /* Pointer to source mesh structure */
  DmeshEntity *       enttlocptr;
  PAMPA_Num           enttlocsiz;

  srcmeshptr = (const Dmesh *) meshptr;
  enttlocptr = srcmeshptr->enttloctax[enttlocnum];

  if (srcmeshptr->ovlpglbval > 0)
    enttlocsiz = enttlocptr->vgstlocnbr;
  else
    enttlocsiz = enttlocptr->vertlocnbr;

  *matlocsiz = enttlocptr->vmatloctax[enttlocsiz + srcmeshptr->baseval] - srcmeshptr->baseval;
  
  return (0);

}

//! \brief This routine accesses the index in the matrix to a specific vertex
//! and its neighbor.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_dmeshMatVertData (
PAMPA_Dmesh * const         meshptr,                                    //!< Distributed mesh
PAMPA_Num const             enttlocnum,                                 //!< Entity number
PAMPA_Num const             vertlocnum,                                 //!< Vertex number
PAMPA_Num const             nghblocnum,                                 //!< Neighbor number
PAMPA_Num *                 matindnum)                                  //!< Index number
{

  const Dmesh *       srcmeshptr;                 /* Pointer to source mesh structure */
  DmeshEntity *       enttlocptr;
  Gnum                vertngbnum;
  Gnum                vertngbmax;

  srcmeshptr = (const Dmesh *) meshptr;
  enttlocptr = srcmeshptr->enttloctax[enttlocnum];

  *matindnum = enttlocptr->vmatloctax[vertlocnum];

  if (nghblocnum != PAMPA_MAT_NGHB_FIRST) {

    for (vertngbnum = enttlocptr->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vertlocidx,
        vertngbmax = enttlocptr->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vendlocidx ;
        vertngbnum < vertngbmax && srcmeshptr->edgeloctax[vertngbnum] != nghblocnum ;
        vertngbnum ++) ;

#ifdef PAMPA_DEBUG_DMESH
    if (srcmeshptr->edgeloctax[vertngbnum] != nghblocnum) {
      errorPrint ("neighbor not found, val : %d, nghb : %d", nghblocnum, srcmeshptr->edgeloctax[vertngbnum]);
    }
#endif /* PAMPA_DEBUG_DMESH */

    *matindnum += vertngbnum - enttlocptr->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vertlocidx;
  }

  return (0);

}

//! \brief This routine accesses the range of a line in the matrix.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.
int
PAMPA_dmeshMatLineData (
PAMPA_Dmesh * const         meshptr,                                    //!< Distributed mesh
PAMPA_Num const             enttlocnum,                                 //!< Entity number
PAMPA_Num const             vertlocnum,                                 //!< Vertex number
PAMPA_Num *                 matindnum,                                  //!< Beginning index number
PAMPA_Num *                 matindnnd)                                  //!< Ending index number
{

  const Dmesh *       srcmeshptr;                 /* Pointer to source mesh structure */
  DmeshEntity *       enttlocptr;

  srcmeshptr = (const Dmesh *) meshptr;
  enttlocptr = srcmeshptr->enttloctax[enttlocnum];

  *matindnum = enttlocptr->vmatloctax[vertlocnum];
  *matindnnd = enttlocptr->vmatloctax[vertlocnum + 1] - 1;

  return (0);

}
