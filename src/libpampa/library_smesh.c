
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "smesh.h"
#include <pampa.h>

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed smesh handling routines.  */
/*                                      */
/****************************************/

//! \brief  This routine initializes the opaque
//! centralized smesh structure used to
//! handle distributed smeshs in the
//! pampa library.
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_smeshInit (
PAMPA_Smesh * const       meshptr)               //!< Centralized smesh
{
  if (sizeof (PAMPA_Num) != sizeof (Gnum)) {
    errorPrint ("internal error");
    return     (1);
  }
  if (sizeof (PAMPA_Smesh) < sizeof (Smesh)) {
    errorPrint ("internal error");
    return     (1);
  }

  return (smeshInit ((Smesh *) meshptr));
}

//! \brief  This routine frees the contents of the
//! given opaque smesh structure.
//!
//! \returns VOID  : in all cases.

void
PAMPA_smeshExit (
PAMPA_Smesh * const       meshptr)               //!< Centralized smesh
{
  smeshExit ((Smesh *) meshptr);
}

//! \brief  This routine frees the contents of the
//! given opaque smesh structure but does
//! not free its private data.
//!
//! \returns VOID  : in all cases.

void
PAMPA_smeshFree (
PAMPA_Smesh * const       meshptr)               //!< Centralized smesh
{
  smeshFree ((Smesh *) meshptr);
}
