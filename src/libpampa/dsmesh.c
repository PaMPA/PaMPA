/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dsmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module contains the distributed
//!                simplified mesh handling routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define MESH

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dsmesh.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle centralized */
/* mesh meshs.                      */
/*                                   */
/*************************************/

//! This routine initializes a simplified centralized mesh
//! structure. In order to avoid collective
//! communication whenever possible, the allocation
//! of send and receive index arrays is not performed
//! in the routine itself, but rather delegated to
//! subsequent routines such as meshBuild.
//! However, these arrays will not be freed by
//! meshFree, but by meshExit.
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
dsmeshInit (
    Dsmesh * restrict const       smshptr,              //!< Dsmesh structure
    MPI_Comm                     proccomm)
{
  memSet (smshptr, 0, sizeof (Dsmesh));            /* Clear public and private mesh fields */

  smshptr->proccomm = proccomm;                   /* Set private fields    */
  MPI_Comm_size (proccomm, &smshptr->procglbnbr); /* Get communicator data */
  MPI_Comm_rank (proccomm, &smshptr->proclocnum);

  return (0);
}

//! This routine frees the public data of the given
//! distributed mesh, but not its private data.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! It returns:
//! - VOID  : in all cases.

void
dsmeshFree (
    Dsmesh * const     smshptr)
{
  Gnum baseval;

  // XXX ne doit-on pas faire comme dans Dmesh.c en recopiant les bonnes valeurs ?
  baseval = smshptr->baseval;

  if ((smshptr->flagval & DSMESHFREETABS) != 0) { /* If local arrays must be freed */
	if (smshptr->ventloctax != NULL)
	  memFreeGroup (smshptr->ventloctax + baseval);
	if (smshptr->commlocptr != NULL) {
	  if (smshptr->commlocptr->procrcvtab != NULL)
  		memFreeGroup (smshptr->commlocptr->procrcvtab);
	  memFree (smshptr->commlocptr); // XXX ne doit-on pas mettre un drapeau exprès pour commlocptr pour savoir s'il a été alloué ???
	}
  }

  dvaluesFree(smshptr->valslocptr);
}


//! This routine destroys a distributed mesh structure.
//! It is not a collective routine, as no communication
//! is needed to perform the freeing of memory structures.
//! Private data are always destroyed. If this is not
//! wanted, use meshFree() instead.
//! It returns:
//! - VOID  : in all cases.

void
dsmeshExit (
    Dsmesh * restrict const     smshptr)
{
  dsmeshFree (smshptr);

  if (((smshptr->flagval & DSMESHFREEPRIV) != 0) && (*smshptr->procloccnt == 1)) {   /* If private data has to be freed */
    if (smshptr->procdsptab != NULL)
      memFreeGroup (smshptr->procdsptab);              /* Free group leader of mesh private data */
  }
  else 
	(*smshptr->procloccnt) --;
  if ((smshptr->flagval & DSMESHFREECOMM) != 0)   /* If communicator has to be freed         */
    MPI_Comm_free (&smshptr->proccomm);           /* Free it                                 */


#ifdef PAMPA_DEBUG_MESH1
  memSet (smshptr, ~0, sizeof (Dsmesh));
#endif /* PAMPA_DEBUG_MESH1 */
}
