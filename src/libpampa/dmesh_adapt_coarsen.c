/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt_coarsen.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 Oct 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_ADAPT
#define PAMPA_TIME_CHECK

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "pampa.h"
#include "pampa.h"

#include <ptscotch.h>
#include "dmesh_adapt.h"
#include "dmesh_adapt_common.h"
#include "dmesh_dgraph.h"
#include "dmesh_gather_induce_multiple.h"
#include "dmesh_rebuild.h"

/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshAdaptCoarsen (
    Dmesh * const    srcmeshptr,              /**< mesh */
	const Gnum                bandval,              /**< Band value */
	const Gnum                ballsiz,
        int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
        int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
        int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
        int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
        int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
	void * const              dataptr,
	Dmesh * const    dstmeshptr)
{
  SCOTCH_Dgraph dgrfdat;
  SCOTCH_Dgraph bgrfdat; /* Band graph */
  SCOTCH_Dgraph cgrfdat; /* Connex compound graph */
  SCOTCH_Dgraph dbgfdat; /* Distributed bigraph */
  SCOTCH_Graph  cbgfdat; /* Centralized bigraph */
  SCOTCH_Strat  strtdat; 
  Gnum * restrict rmshloctax;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  Gnum baseval;
  Gnum vertlocmax;
  Gnum coarval;
  Gnum coarnbr;
  double coarrat;
  Gnum ballsi2;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum dvrtlocnbr;
  Gnum emraval;
#ifdef PAMPA_INFO_ADAPT
  Gnum iternbr;
#endif /* PAMPA_INFO_ADAPT */
  int cheklocval;
  Gnum * restrict partloctax;
  Gnum * restrict paroloctax;
  Gnum * restrict vmloloctab;
  MPI_Comm proccomm;
#ifdef PAMPA_DEBUG_DMESH_SAVE
    Gnum wmshcnt = 0;
#endif /* PAMPA_DEBUG_DMESH_SAVE */

    infoPrint("*******************\n\nCoarsen\n\n*******************\n");
  cheklocval = 0;
  coarnbr = 1;
  coarrat = 1;
  for (coarval = 1, ballsi2 = 1; ballsi2 < ballsiz; coarval ++, ballsi2 <<= 1);

  baseval = srcmeshptr->baseval;
  proccomm = srcmeshptr->proccomm;

  CHECK_FERR (SCOTCH_stratInit (&strtdat), proccomm);
  CHECK_FERR (dmeshDgraphInit (srcmeshptr, &dgrfdat), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&bgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&cgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&dbgfdat, proccomm), proccomm);
  if (srcmeshptr->proclocnum == 0)
	cheklocval = SCOTCH_graphInit (&cbgfdat);
  CHECK_VERR (cheklocval, proccomm);

#ifdef PAMPA_INFO_ADAPT
  iternbr = 1;
#endif /* PAMPA_INFO_ADAPT */

  while (1) {
	Gnum dvrtlocbas;
	Gnum bvrtlocnbr;
	Gnum bvrtlocnum;
	Gnum bvrtlocnnd;
	Gnum bvrtgstnbr;
	Gnum bvrtgstmax;
	Gnum cvrtlocnbr;
	Gnum cvrtgstnbr;
	Gnum cvrtglbnbr;
	Gnum cvrtlocmin;
	Gnum cvrtlocmax;
	Gnum cvrtlocnum;
	Gnum cvrtlocnnd;
	Gnum datasndnbr;
	Gnum datarcvnbr;
	Gnum vertlocnbr;
	Gnum edgelocnbr;
	Gnum pvrtlocnum;
	Gnum vertlocbas;
	Gnum edgelocnum;
	Gnum partlocidx;
	Gnum meshlocnbr;
	Gnum meshlocnum;
	Gnum fronlocidx;
	int procngbnum;
	int procngbnnd;
  	Gnum hashsiz;          /* Size of hash table    */
  	Gnum hashnum;          /* Hash value            */
  	Gnum hashmax;
	Gnum hashbas;
  	Gnum hashnbr;
  	Gnum hashmsk;
	Gnum * fronloctax;
  	Gnum * restrict hashtab;
	Gnum * restrict bvlbloctax;
	Gnum * restrict rmshloctx2;
	Gnum * restrict vnumgsttax;
	Gnum * restrict bnumgsttax;
	Gnum * restrict bvrtloctax;
	Gnum * restrict bvndloctax;
	Gnum * restrict bedggsttax;
	Gnum * cprcvrttab;
	int * restrict dsndcnttab;
	int * restrict dsnddsptab;
	int * restrict drcvcnttab;
	int * restrict drcvdsptab;
	Gnum * restrict cvelloctax;
	//Gnum * restrict cvelgsttax;
    Gnum * restrict datasndtab;
    Gnum * restrict datarcvtab;
    Gnum * restrict datarcvtb2;
	Gnum * restrict vertloctax;
	Gnum * restrict veloloctax;
	Gnum * restrict edgeloctax;
	Gnum * restrict parttax;
	Gnum * restrict vertsidtab;
	Mesh * imshloctab;
	Mesh * omshloctab;

	CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
	rmshloctax -= baseval;

	vertlocmax = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;
	for (rmshlocnbr = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		rmshlocnbr ++;

#ifdef PAMPA_INFO_ADAPT
  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
        CHECK_VERR (cheklocval, proccomm);
        infoPrint ("-- Iteration number: %d", iternbr);
        infoPrint ("Number of vertices to be remeshed before dgraphBand: %d", rmshglbnbr);
#endif /* PAMPA_INFO_ADAPT */

	hashbas = 
  	hashnbr = vertlocmax / 10; // XXX N'est-ce pas trop grand pour hashnbr ?
  	for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */

  	hashmsk = hashsiz - 1;
  	hashmax = hashsiz >> 2;

  	if ((hashtab = (Gnum *) memAlloc (hashsiz * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	memSet (hashtab, ~0, hashsiz * sizeof (Gnum));
	
	// XXX ne devrait-on pas faire cette alloc une bonne fois pour toutes les
	// itérations ?
  	if (memAllocGroup ((void **) (void *)
		  &fronloctax, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
		  &rmshloctx2, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	fronloctax -= baseval;
  	rmshloctx2 -= baseval;

  	memCpy (rmshloctx2 + baseval, rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

	for (fronlocidx = vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		fronloctax[fronlocidx ++] = vertlocnum;

  	// 1) Identification des zones à remailler
  	// 	a) conversion du maillage en graphe uniquement pour les sommets qui ont un drapeau
  	dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_MAIN, ~0, NULL, NULL, NULL, &dgrfdat, &dvrtlocbas); // XXX pas de velo ni de edlo ??

#ifdef PAMPA_DEBUG_DGRAPH
  	char s[30];
  	sprintf(s, "dgrfdat-adapt-%d", srcmeshptr->proclocnum);
  	FILE *dgraph_file;
  	dgraph_file = fopen(s, "w");
  	SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
  	fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

	if (rmshglbnbr == 0) {
	  memFreeGroup (fronloctax + baseval);
          CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
	  memFree (hashtab);
	  break;
        }

  	// 	a bis) élargissement des zones avec un dgraphBand ok
  	if (bandval > 0) {
	  CHECK_FERR (SCOTCH_dgraphBand (&dgrfdat, rmshlocnbr, fronloctax + baseval, bandval, &bgrfdat), proccomm);
        }
        else {
          // 	b) sous-graphe induit des zones ok
          CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnbr, &bgrfdat), proccomm);
        }
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (SCOTCH_dgraphCheck (&bgrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	CHECK_FERR (SCOTCH_dgraphGhst (&bgrfdat), proccomm);
  	//SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, &bvrtgstnbr, (SCOTCH_Num **) &bvrtloctax, (SCOTCH_Num **) &bvndloctax, NULL, (SCOTCH_Num **) NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bedggsttax, NULL, NULL); 
  	SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, &bvrtgstnbr, (SCOTCH_Num **) &bvrtloctax, (SCOTCH_Num **) &bvndloctax, NULL, (SCOTCH_Num **) &bvlbloctax, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bedggsttax, NULL, NULL); 
	bvlbloctax -= baseval;
	bvrtloctax -= baseval;
	bvndloctax -= baseval;
	bedggsttax -= baseval;

  	// 	c) contraction du graphe des zones ok
  	if (memAllocGroup ((void **) (void *)
		  &bnumgsttax, (size_t) (bvrtgstnbr * sizeof (Gnum)),
		  //&bvlbloctax, (size_t) (bvrtlocnbr * sizeof (Gnum)), 
		  &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
          &cprcvrttab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (Gnum)),
          &dsndcnttab, (size_t) (srcmeshptr->procglbnbr       * sizeof (int)),
          &dsnddsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
          &drcvcnttab, (size_t) (srcmeshptr->procglbnbr       * sizeof (int)),
          &drcvdsptab, (size_t) ((srcmeshptr->procglbnbr + 1) * sizeof (int)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
  	bnumgsttax -= baseval;
  	vnumgsttax -= baseval;
	//bvlbloctax -= baseval;
  	memSet (dsndcnttab, 0, srcmeshptr->procglbnbr * sizeof (Gnum));
  	memSet (vnumgsttax + baseval, ~0, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

	if (rmshglbnbr > ballsiz) {
  	  CHECK_FERR (dmeshAdapt2 (srcmeshptr, &bgrfdat, bnumgsttax, &cgrfdat, cprcvrttab, coarval - 1, coarnbr, coarrat), srcmeshptr->proccomm);

	  CHECK_FERR (SCOTCH_dgraphHalo (&bgrfdat, bnumgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);

	  CHECK_FERR (SCOTCH_dgraphGhst (&cgrfdat), proccomm);
	  SCOTCH_dgraphData (&cgrfdat, NULL, &cvrtglbnbr, &cvrtlocnbr, NULL, &cvrtgstnbr, NULL, NULL, (SCOTCH_Num **) &cvelloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	  cvelloctax -= baseval;

        //  if ((cvelgsttax = (Gnum *) memAlloc (cvrtgstnbr * sizeof (Gnum))) == NULL) {
        //    errorPrint  ("out of memory");
        //    cheklocval = 1;
        //  }
  	//CHECK_VERR (cheklocval, proccomm);
	//cvelgsttax -= baseval;
	//memCpy (cvelgsttax + baseval, cvelloctax + baseval, cvrtlocnbr * sizeof (Gnum));
	//CHECK_FERR (SCOTCH_dgraphHalo (&cgrfdat, cvelgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);
#ifdef PAMPA_INFO_ADAPT
        infoPrint ("-- Iteration number: %d", iternbr ++);
        infoPrint ("Number of vertices to be remeshed : %d", rmshglbnbr);
        infoPrint ("Number of colors : %d", cvrtglbnbr);
        infoPrint ("Number of local colors : %d", cvrtlocnbr);
#endif /* PAMPA_INFO_ADAPT */

	  cvrtlocmin = cprcvrttab[srcmeshptr->proclocnum];
	  cvrtlocmax = cprcvrttab[srcmeshptr->proclocnum + 1] - 1;
	  bvrtgstmax = bvrtlocnbr + baseval - 1;
  	  // 	d) partitionnement des gros sommets ko
  	  //                 * partitionnement de graphes
  	  //                         * les gros sommets sont indépendants donc absence de relations
  	  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

	  //for (bvrtlocnum = dvrtlocnum = baseval, dvrtlocnnd = dvrtlocnbr + baseval; dvrtlocnum < dvrtlocnnd; dvrtlocnum ++)
	  //  if (rmshloctx2[dvrtlocnum] == PAMPA_TAG_VERT_REMESH)
	  //	bvlbloctax[bvrtlocnum ++] = dvrtlocnum + dvrtlocbas;

  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
		//if (cvelgsttax[bnumgsttax[bvrtlocnum]] > (ballsiz / 10))
                //FIXME peut-on faire un test sur les tailles de boule, le
                //problème est que bnumgsttax contient un sommet qui peut ne pas
                //être dans les ghst
	  	  vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = bnumgsttax[bvrtlocnum]; /* Propagate colors on elements */

	  ///* First pass to remove elements which have neighbors with different color */
  	  //for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
	  //	Gnum cvrtlocnum;
	  //	Gnum ucollocval; /* If undo coloring */

	  //	cvrtlocnum = bnumgsttax[bvrtlocnum];

	  //	for (ucollocval = 0, bedglocnum = bvrtloctax[bvrtlocnum], bedglocnnd = bvndloctax[bvrtlocnum]; bedglocnum < bedglocnnd; bedglocnum ++) /* Undo coloring on elements which are at the boundary */
	  //    if (bnumgsttax[bedggsttax[bedglocnum]] != cvrtlocnum) { /* If vertex and neighbor have different colors */
	  //    	ucollocval = 1;
	  //    	if (bedggsttax[bedglocnum] <= bvrtgstmax) 
	  //  	  vnumgsttax[bvlbloctax[bedggsttax[bedglocnum]] - dvrtlocbas] = ~0;
	  //    }
	  //	if (ucollocval == 1) { /* There are at least one neighbor with different color */
	  //    vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = ~0;
	  //	  // FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	}
	  //}
	  memFreeGroup (fronloctax + baseval);

	  //CHECK_FERR (dmeshHaloSync (srcmeshptr, srcmeshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm);

	  //// FIXME temporarly disabled
	  ///* Second pass to remove elements which are isolated */
	  //for (vertlocnum = baseval, vertlocnnd = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
	  //	Gnum ucollocval; /* If undo coloring */
	  //	Gnum vnumlocval;
	  //	Gnum edgelocnnd;
	  //	const DmeshEntity * enttlocptr = srcmeshptr->enttloctax[baseval];

	  //	vnumlocval = vnumgsttax[vertlocnum];

	  //	if (vnumlocval != ~0) {
	  //    for (ucollocval = 0, edgelocnum = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx,
	  //  	  edgelocnnd = enttlocptr->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx;
	  //  	  ucollocval == 0 && edgelocnum < edgelocnnd; edgelocnum ++) {
	  //    	Gnum nghblocnum;

	  //    	nghblocnum = srcmeshptr->edgeloctax[edgelocnum];

	  //    	if (vnumgsttax[nghblocnum] == vnumlocval) /* If vertex and neighbor have same colors */
	  //    	  ucollocval = 1;
	  //    }
	  //	  if (ucollocval == 0) { /* If no neighbor with same color */
	  //    	vnumgsttax[vertlocnum] = ~0;
	  //	  	// FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	  }
	  //	}
	  //}

  	  for (procngbnum = srcmeshptr->proclocnum, bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
	  	Gnum cvrtlocnum;

	  	cvrtlocnum = bnumgsttax[bvrtlocnum];

      	for (hashnum = (cvrtlocnum * DMESHADAPTHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum] != cvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

      	if (hashtab[hashnum] == cvrtlocnum) /* If vertex already added */
          continue;

	  	if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
    	  if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
		  	int procngbmax;
          	for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
              	procngbmax - procngbnum > 1; ) {
          	  int                 procngbmed;

          	  procngbmed = (procngbmax + procngbnum) / 2;
          	  if (cprcvrttab[procngbmed] <= cvrtlocnum)
              	procngbnum = procngbmed;
          	  else
              	procngbmax = procngbmed;
          	}
		  }

		  dsndcnttab[procngbnum] ++;
	  	}

	  	hashtab[hashnum] = cvrtlocnum;
	  	hashnbr ++;

      	if (hashnbr >= hashmax) { /* If (*hashtab) is too much filled */
		  cheklocval = dmeshAdaptVertResize (srcmeshptr, &hashtab, &hashsiz, &hashmax, &hashmsk);
  		  CHECK_VERR (cheklocval, proccomm);
      	}
	  }
	  SCOTCH_dgraphFree (&bgrfdat);

  	  /* Compute displacement sending array */
  	  dsnddsptab[0] = 0;
  	  for (procngbnum = 1, procngbnnd = srcmeshptr->procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) 
      	dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];

  	  datasndnbr = dsnddsptab[srcmeshptr->procglbnbr];

  	  //* envoyer dsndcnttab et recevoir drcvcnttab
  	  CHECK_VDBG (cheklocval, proccomm);
  	  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, proccomm) != MPI_SUCCESS) {
      	errorPrint ("communication error");
	  	cheklocval = 1;
  	  }
	  CHECK_VERR (cheklocval, proccomm);

  	  //* en déduire ddsp{snd,rcv}tab
  	  /* Compute displacement receiving array */
  	  drcvdsptab[0] = 0;
  	  for (procngbnum = 1 ; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
      	drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
      	dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  	  }
  	  drcvdsptab[srcmeshptr->procglbnbr] = drcvdsptab[srcmeshptr->procglbnbr - 1] + drcvcnttab[srcmeshptr->procglbnbr - 1];
  	  datarcvnbr = drcvdsptab[srcmeshptr->procglbnbr];
  	  dsndcnttab[0] = dsnddsptab[0];

	  vertlocnbr = cvrtlocnbr + 1; /* Adding processor as a vertex */
	  edgelocnbr = (hashnbr - hashbas) * 2 + datarcvnbr;
  	  //* allouer datasndtab et datarcvtab
  	  //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
  	  if (memAllocGroup ((void **) (void *)
          	&datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
          	&datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
          	&datarcvtb2, (size_t) (datasndnbr * sizeof (Gnum)),
		  	&vertloctax, (size_t) ((vertlocnbr + 1) * sizeof (Gnum)),
		  	&veloloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
		  	&edgeloctax, (size_t) (edgelocnbr * sizeof (Gnum)),
		  	&partloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
		  	&parttax, (size_t) ((cvrtglbnbr + srcmeshptr->procglbnbr) * sizeof (Gnum)),
		  	&vertsidtab, (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
          	NULL) == NULL) {
      	errorPrint ("Out of memory");
      	cheklocval = 1;
  	  }
  	  CHECK_VERR (cheklocval, proccomm);
	  vertloctax -= baseval;
	  veloloctax -= baseval;
	  edgeloctax -= baseval;
	  partloctax -= baseval;
	  parttax -= baseval;

	  for (hashnum = 0; hashnum < hashsiz; hashnum ++)
	  	if (hashtab[hashnum] != ~0) {
		  Gnum cvrtlocnum;

		  cvrtlocnum = hashtab[hashnum];

	  	  if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
		  	Gnum cvrtlocbas;
    	  	if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
			  int procngbmax;
          	  for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
              	  procngbmax - procngbnum > 1; ) {
          	  	int                 procngbmed;

          	  	procngbmed = (procngbmax + procngbnum) / 2;
          	  	if (cprcvrttab[procngbmed] <= cvrtlocnum)
              	  procngbnum = procngbmed;
          	  	else
              	  procngbmax = procngbmed;
          	  }
			  cvrtlocbas = cprcvrttab[procngbnum] + baseval;
		  	}
		  	datasndtab[dsndcnttab[procngbnum] ++] = cvrtlocnum - cvrtlocbas;
	  	  }
	  	}

  	  for (procngbnum = 0 ; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
      	dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
	  	intSort1asc1 (datasndtab + dsnddsptab[procngbnum], dsndcnttab[procngbnum]);
  	  }

  	  //* envoyer datasndtab et recevoir datarcvtab
  	  CHECK_VDBG (cheklocval, proccomm);
  	  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
      	errorPrint ("communication error");
	  	cheklocval = 1;
  	  }
	  CHECK_VERR (cheklocval, proccomm);

	  for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++)
	  	vertsidtab[procngbnum] = drcvdsptab[procngbnum];

	  /* Adding processor as a vertex and their neighbors */
	  vertlocnum = baseval;
	  veloloctax[vertlocnum] = 1;
	  vertloctax[vertlocnum ++] = baseval;
	  pvrtlocnum = cprcvrttab[srcmeshptr->proclocnum] + srcmeshptr->proclocnum; /* Vertex number of processor */
	  vertlocbas = srcmeshptr->proclocnum + 1 - baseval;
	  for (procngbnum = srcmeshptr->proclocnum, edgelocnum = baseval, hashnum = 0; hashnum < hashsiz; hashnum ++)
	  	if (hashtab[hashnum] != ~0) {
		  Gnum cvrtlocnum;
		  cvrtlocnum = hashtab[hashnum];

	  	  if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
		  	Gnum vertlocbs2;
    	  	if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
			  int procngbmax;
          	  for (procngbnum = 0, procngbmax = srcmeshptr->procglbnbr;
              	  procngbmax - procngbnum > 1; ) {
          	  	int                 procngbmed;

          	  	procngbmed = (procngbmax + procngbnum) / 2;
          	  	if (cprcvrttab[procngbmed] <= cvrtlocnum)
              	  procngbnum = procngbmed;
          	  	else
              	  procngbmax = procngbmed;
          	  }
		  	  vertlocbs2 = procngbnum + 1 - baseval;
		  	}
		  	edgeloctax[edgelocnum ++] = cvrtlocnum + vertlocbs2;
	  	  }
		  else
	  	  	edgeloctax[edgelocnum ++] = cvrtlocnum + vertlocbas;
	  	}

	  for (cvrtlocnum = baseval, cvrtlocnnd = cvrtlocnbr + baseval; cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
	  	veloloctax[vertlocnum] = cvelloctax[cvrtlocnum];
	  	vertloctax[vertlocnum ++] = edgelocnum;
	  	edgeloctax[edgelocnum ++] = pvrtlocnum;

	  	for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++)
		  if ((procngbnum != srcmeshptr->proclocnum) && (vertsidtab[procngbnum] < drcvdsptab[procngbnum + 1])
			  && (datarcvtab[vertsidtab[procngbnum]] == cvrtlocnum)) {
		  	Gnum pvrtlocnm2;

		  	pvrtlocnm2 = cprcvrttab[procngbnum] + procngbnum;
		  	edgeloctax[edgelocnum ++] = pvrtlocnm2;
		  	vertsidtab[procngbnum] ++;
		  }
	  }
	  vertloctax[vertlocnum] = edgelocnum;
	  edgelocnbr = edgelocnum - baseval;
	  SCOTCH_dgraphFree (&cgrfdat);

	  CHECK_FERR (SCOTCH_dgraphBuild (&dbgfdat, baseval, vertlocnbr, vertlocnbr, vertloctax + baseval, NULL, veloloctax + baseval, NULL, edgelocnbr, edgelocnbr, edgeloctax + baseval, NULL, NULL), proccomm); // XXX doit-on mettre edloloctab ? Si oui, à quoi est-ce qu'il correspond ?

#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (SCOTCH_dgraphCheck (&dbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
#ifdef PAMPA_DEBUG_DGRAPH
  	  sprintf(s, "dbgf-adapt-%d", srcmeshptr->proclocnum);
  	  dgraph_file = fopen(s, "w");
  	  SCOTCH_dgraphSave (&dbgfdat, dgraph_file);
  	  fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */
	  memSet (partloctax + baseval, ~0, vertlocnbr * sizeof (Gnum));
	  partloctax[baseval] = srcmeshptr->proclocnum;

	  if (srcmeshptr->proclocnum == 0) {
	  	CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, &cbgfdat), proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
		CHECK_FERR (SCOTCH_graphCheck (&cbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */


	  	drcvdsptab[0] = 0;
	  	for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
	  	  drcvcnttab[procngbnum] = cprcvrttab[procngbnum + 1] - cprcvrttab[procngbnum] + 1;
	  	  drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
	  	}

  	  	if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
          	  parttax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);

	  	CHECK_FERR (SCOTCH_graphPartFixed (&cbgfdat, srcmeshptr->procglbnbr, &strtdat, parttax + baseval), proccomm);
	  	SCOTCH_graphFree (&cbgfdat);

	  	for (partlocidx = baseval, procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
		  Gnum partlocid2;
		  Gnum partlocnnd;
		  for (partlocid2 = drcvdsptab[procngbnum], partlocnnd = drcvdsptab[procngbnum + 1];
			  partlocid2 < partlocnnd; partlocid2 ++, partlocidx ++)
		  	parttax[partlocidx] = parttax[partlocid2];
	  	}

  	  	if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);

  	  	//if (commScatterv (parttax + baseval, prcvcnttab, prcvdsptab, GNUM_MPI,
      	//    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	//	errorPrint ("communication error");
      	//	return     (1);
  	  	//}
	  }
	  else {
	  	CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, NULL), proccomm); /* For graphPartFixed */

  	  	if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
          	  NULL, NULL, NULL, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);

  	  	if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	  errorPrint ("communication error");
	  	  cheklocval = 1;
  	  	}
	  	CHECK_VERR (cheklocval, proccomm);
  	  	//if (commScatterv (NULL, NULL, NULL, GNUM_MPI,
      	//    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      	//	errorPrint ("communication error");
      	//	return     (1);
  	  	//}
	  }
	  CHECK_VERR (cheklocval, proccomm);
	}
	else {
	  memFreeGroup (fronloctax + baseval);
#ifdef PAMPA_INFO_ADAPT
                infoPrint ("-- Iteration number: %d", iternbr ++);
                infoPrint ("Number of vertices to be remeshed : %d", rmshglbnbr);
                infoPrint ("Last iteration");
#endif /* PAMPA_INFO_ADAPT */
	  datasndtab = NULL;
	  memSet (bnumgsttax + baseval, 0, bvrtgstnbr * sizeof (Gnum));
  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
	  	vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = bnumgsttax[bvrtlocnum]; /* Propagate colors on elements */
  	  if ((parttax = (Gnum *) memAlloc (1 * sizeof (Gnum))) == NULL) {
      	errorPrint  ("out of memory");
      	cheklocval = 1;
  	  }
  	  CHECK_VERR (cheklocval, proccomm);
	  parttax -= baseval;
	  parttax[baseval] = 0; // FIXME pourquoi 0 ???
	  cvrtglbnbr = 1;
	}
        memFree (hashtab);


  	// 2) Extraction des sous-maillages et envoie sur chaque proc ok
	meshlocnbr = -1;
	imshloctab = NULL;
  	CHECK_FERR_STR (dmeshGatherInduceMultiple (srcmeshptr, 1, cvrtglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), "extraction", proccomm);

#ifdef PAMPA_INFO_ADAPT
	infoPrint ("Number of received meshes : %d", meshlocnbr);
	infoPrint ("Number of local vertices in source distributed mesh : %d", srcmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

	memFreeGroup (bnumgsttax + baseval);
	if (datasndtab != NULL)
	  memFreeGroup (datasndtab);
	else
	  memFree (parttax + baseval);

  	// 3) Remaillage des zones
  	// 	a) conversion pampa -> mmg ok
  	// 	b) remaillage ok
  	// 	c) conversion mmg -> pampa quasi ok
  	if ((omshloctab = (Mesh *) memAlloc (meshlocnbr * sizeof (Mesh))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);

	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) { // XXX cette partie pourrait être parallélisé avec pthread
          Gnum * restrict inbrtab;
          Gnum * restrict onbrtab;

#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (imshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshInit (omshloctab + meshlocnum);
	  CHECK_FERR_STR (EXT_meshAdapt ((PAMPA_Mesh *) (imshloctab + meshlocnum), (PAMPA_Mesh *) (omshloctab + meshlocnum), dataptr, 0), "remesh zone", proccomm); // FIXME remplacer le 0 par un drapeau
	  omshloctab[meshlocnum].idnum = imshloctab[meshlocnum].idnum;
          omshloctab[meshlocnum].procglbnbr = imshloctab[meshlocnum].procglbnbr;
          CHECK_FERR (meshValueData (imshloctab + meshlocnum, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART, NULL, NULL, (void **) &inbrtab), srcmeshptr->proccomm);
          CHECK_FERR (meshValueLink(omshloctab + meshlocnum, (void **) &onbrtab, PAMPA_VALUE_NONE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART), srcmeshptr->proccomm);
          memCpy (onbrtab, inbrtab, srcmeshptr->procglbnbr * sizeof (Gnum));
#ifdef PAMPA_DEBUG_ADAPT2
	  CHECK_FERR (meshCheck (omshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
	  meshExit (imshloctab + meshlocnum);
	}
	memFree (imshloctab);
	CHECK_VDBG (cheklocval, proccomm);


  	// 4) Reintegration des zones dans maillage distribué en cours
        //errorPrint ("à modifier pour le rebuild");
        //return (1);
	CHECK_FERR_STR (dmeshRebuild (srcmeshptr, meshlocnbr, omshloctab), "reintegration", proccomm);
#ifdef PAMPA_DEBUG_ADAPT2
	CHECK_FERR (dmeshCheck (srcmeshptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
#ifdef PAMPA_INFO_ADAPT
	infoPrint ("Number of local vertices in destination distributed mesh : %d", srcmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE

        Mesh wmshdat;
        char s1[50];
        sprintf (s1, "dmesh-adapt-%d-%d", srcmeshptr->proclocnum, wmshcnt ++);
        if (srcmeshptr->proclocnum == 0) {
          meshInit (&wmshdat);
          CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, &wmshdat), proccomm);
          CHECK_FERR (EXT_meshSave(&wmshdat, 1, dataptr, NULL, s1), proccomm);
		  meshExit (&wmshdat);
        }
        else
  		  CHECK_FERR (PAMPA_dmeshGather (srcmeshptr, NULL), proccomm);

#endif /* PAMPA_DEBUG_DMESH_SAVE */
	for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) 
	  meshFree (omshloctab + meshlocnum);

	memFree (omshloctab);

	CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
	SCOTCH_dgraphFree (&dbgfdat);
  }

  CHECK_FERR_STR (dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_ANY, ~0, NULL, NULL, NULL, &dgrfdat, NULL), "graphe final", proccomm); // XXX pas de velo ni de edlo ??
  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

  if (memAllocGroup ((void **) (void *)
		&paroloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
		&partloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  paroloctax -= baseval;
  partloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = dvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
   	paroloctax[vertlocnum] = srcmeshptr->proclocnum;

  //// 5) Repartitionnement
  //vmloloctab = NULL;
  //emraval = 1;
  //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "q{strat=m{asc=b{width=3,bnd=(d{pass=40,dif=1,rem=0}|)f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.01,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=0}}"), proccomm);
  //CHECK_FERR (SCOTCH_dgraphRepart (&dgrfdat, srcmeshptr->procglbnbr, paroloctax + baseval, emraval, vmloloctab, &strtdat, partloctax + baseval), proccomm);

  dmeshDgraphExit (&dgrfdat);
  SCOTCH_dgraphExit (&bgrfdat);
  SCOTCH_dgraphExit (&cgrfdat);
  SCOTCH_dgraphExit (&dbgfdat);
  SCOTCH_stratExit (&strtdat);
  if (srcmeshptr->proclocnum == 0)
	SCOTCH_graphExit (&cbgfdat);
  // 5 bis) Redistribution ok
  //CHECK_FERR (dmeshRedist (srcmeshptr, partloctax, NULL, -1, -1, -1, dstmeshptr), proccomm); // XXX peut-être utiliser un dsmeshRedist qui serait moins gourmand en mémoire à mon avis
  memFree (paroloctax + baseval);
  CHECK_VDBG (cheklocval, proccomm);
  return (0);
} 
