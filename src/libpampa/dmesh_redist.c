/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_redist.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the distributed mesh
//!                redistribution routine.
//!
//!   \date        Version 1.0: from: 13 Mar 2012
//!                             to:   29 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 **  The defines and includes.
 */

#define DMESH

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_build.h"
#include "pampa.h"
#include <ptscotch.h>
#include "dmesh_dgraph.h"
#include "dgraph_graph.h"
#include "dmesh_overlap.h"
#include "dmesh_redist.h"

//! This routine computes another distributed mesh which corresponds to the
//! partition
//XXX ne pas oublier de mettre un mot sur le fait que c'est cette fonction qui
//initialise le Dmesh destinataire en faisant une copie du communicateur
//!
//! \returns 0   : if the computation succeeded.
//! \returns !0  : on error.

int
dmeshRedist (
	Dmesh * const        srcmeshptr,                        //!< XXX
	Gnum * const         srcpartloctax,                        //!< XXX
	Gnum * const         srcpermgsttax,                        //!< XXX
	Gnum   const         dstvertlocdlt,                        //!< XXX
	Gnum   const         dstedgelocdlt,                        //!< XXX
        Gnum const           dstovlpglbval,
	Dmesh * const        dstmeshptr)                        //!< XXX
{
  return (dmeshRedistFull (srcmeshptr, srcpartloctax, srcpermgsttax, dstvertlocdlt, dstedgelocdlt, dstovlpglbval, dstmeshptr, NULL, ~0, ~0, ~0));
}

//! This routine computes another distributed mesh which corresponds to the
//! partition, with all arguments (especially for the overlap).
//XXX ne pas oublier de mettre un mot sur le fait que c'est cette fonction qui
//initialise le Dmesh destinataire en faisant une copie du communicateur
//!
//! \returns 0   : if the computation succeeded.
//! \returns !0  : on error.

int
dmeshRedistFull (
	Dmesh * const        srcmeshptr,                        //!< XXX
	Gnum * const         srcpartloctax,                        //!< XXX
	Gnum * const         srcpermgsttax,                        //!< XXX
	Gnum   const         dstvertlocdlt,                        //!< XXX
	Gnum   const         dstedgelocdlt,                        //!< XXX
        Gnum const           dstovlpglbval,
	Dmesh * const        dstmeshptr,                        //!< XXX
        DmeshOvlpProcVert * const ovphashtab,
        Gnum                      ovphashsiz,          /* Size of hash table    */
        Gnum                      ovphashnbr,
        Gnum                      ovphashmsk)
{

  //errorPrint("is not yet implemented");
  //return (1);

  // XXX et les restrict ????
  Gnum cheklocval;
  Gnum baseval;
  Gnum vertlocbas;
  Gnum enttlocnum;
  Gnum enttlocnnd;
  Gnum vrcvlocnbr;
  Gnum vertlocnbr;
  Gnum vertlocmax;
  int * valusndcnttab;
  int * valusnddsptab;
  int * valurcvcnttab;
  int * valurcvdsptab;
  Gnum * srcenttloctax;
  Gnum * dstenttloctax;
  Gnum * dstenttloctx2;
  Gnum dstpermlocnbr;
  Gnum * dstpermloctax;
  DmeshRedistVert * dstpermloctx2;
  DmeshRedistVert * dstenttpermloctax;
  Gnum srcpermlocnbr;
  Gnum * srcpermloctax;
  Gnum srcpartlocnbr;
  Gnum * srcpcntloctab;
  Gnum * vtruloctax;
  Gnum enttglbnbr;
  Gnum valuglbmax;
  Gnum * partloctx2;
  Gnum * esubglbtax;
  Gnum * vertloctax;
  Gnum * vendloctax;
  Gnum * vlblloctax;
  Gnum * edgegsttax;
  Gnum * edloloctax;
  Gnum * edgeloctax;
  Gnum * vprmloctax;
  Gnum * vcntloctab;
  Gnum edgelocnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  SCOTCH_Dgraph srcgrafptr;
  SCOTCH_Dgraph dstgrafptr;

  MPI_Comm 		     proccomm;							//!< XXX

  cheklocval = 0;

  proccomm = dstmeshptr->proccomm;
  baseval = srcmeshptr->baseval;
  enttglbnbr = srcmeshptr->enttglbnbr;
  valuglbmax = srcmeshptr->valslocptr->valuglbmax;
  dstmeshptr->ovlpglbval = (dstovlpglbval == -1) ? srcmeshptr->ovlpglbval : dstovlpglbval;
  srcpermlocnbr = (srcpermgsttax == NULL) ? 0 : srcmeshptr->vertlocnbr;

  // XXX boucle suivante parce qu'on n'a pas srcpartlocnbr : peut-être modifier l'API
  srcpartlocnbr = 0;
  if (srcpermlocnbr != 0) {
    for (srcpartlocnbr = 0, vertlocnum = baseval, vertlocnnd = srcmeshptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++)
      if (srcpartlocnbr < srcpartloctax[vertlocnum])
        srcpartlocnbr = srcpartloctax[vertlocnum];
    srcpartlocnbr ++;
  }

  if (memAllocGroup ((void **) (void *)
        &valusndcnttab, (size_t) (dstmeshptr->procglbnbr       * sizeof (int)),
        &valusnddsptab, (size_t) ((dstmeshptr->procglbnbr + 1) * sizeof (int)),
        &valurcvcnttab, (size_t) (dstmeshptr->procglbnbr       * sizeof (int)),
        &valurcvdsptab, (size_t) ((dstmeshptr->procglbnbr + 1) * sizeof (int)),
        &srcenttloctax, (size_t) (srcmeshptr->vertlocnbr       * sizeof (Gnum)),
        &vtruloctax,    (size_t) (srcmeshptr->vertlocnbr       * sizeof (Gnum)),
        &srcpermloctax, (size_t) (srcpermlocnbr                * sizeof (Gnum)),
        &srcpcntloctab, (size_t) ((srcpartlocnbr + 1)          * sizeof (Gnum)),
        &partloctx2,    (size_t) (srcmeshptr->vtrulocnbr       * sizeof (Gnum)),
        &vprmloctax,    (size_t) (srcmeshptr->vertlocnbr       * sizeof (Gnum)),
        &vcntloctab,    (size_t) (dstmeshptr->procglbnbr       * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);

  srcenttloctax -= baseval;
  memSet (srcenttloctax + baseval, ~0, srcmeshptr->vertlocnbr * sizeof (Gnum)); /* We assume that PAMPA_ENTT_DUMMY is ~0 */
  partloctx2 -= baseval;
  vprmloctax -= baseval;
  vtruloctax -= baseval;
  srcpermloctax = (srcpermgsttax == NULL) ? NULL : srcpermloctax - baseval;

  esubglbtax = (srcmeshptr->esublocmsk != 0) ? srcmeshptr->esublocbax : NULL;

  memSet (vtruloctax + baseval, 0, srcmeshptr->vertlocnbr * sizeof (Gnum));

  if (srcpermlocnbr != 0) {
    Gnum srcpartlocnum;

    memSet (srcpcntloctab, 0, (srcpartlocnbr + 1) * sizeof (Gnum));

    srcpcntloctab += 1; /* TRICK: shift in order to have value for next part */
    for (vertlocnum = baseval, vertlocnnd = srcmeshptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++)
      srcpcntloctab[srcpartloctax[vertlocnum]] ++;
    srcpcntloctab -= 1;

    for (srcpartlocnum = 0; srcpartlocnum < srcpartlocnbr; srcpartlocnum ++)
      srcpcntloctab[srcpartlocnum] += baseval;
  }

  vertlocbas = srcmeshptr->procvrttab[srcmeshptr->proclocnum] - baseval;
  for (enttlocnum = baseval, enttlocnnd = srcmeshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
    Gnum vertlocnum;
    Gnum vertlocnnd;
    DmeshEntity * restrict enttlocptr;

    if ((srcmeshptr->enttloctax[enttlocnum] == NULL) || (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      continue;

    enttlocptr = srcmeshptr->enttloctax[enttlocnum];

    for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
      Gnum bvrtlocnum;

      bvrtlocnum = enttlocptr->mvrtloctax[vertlocnum] - vertlocbas;
      vtruloctax[bvrtlocnum] = 1;
    }
  }

  memSet (valusndcnttab,  0, dstmeshptr->procglbnbr * sizeof (int));
  memSet (srcpermloctax + baseval, ~0, srcpermlocnbr * sizeof (Gnum));

  for (vertlocnum = baseval, vertlocnnd = srcmeshptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) // XXX vertlocnbr ou vertlocmax ??
    if (vtruloctax[vertlocnum] != 0) {
      valusndcnttab[srcpartloctax[vertlocnum]] ++;

      if (srcpermgsttax != NULL)
        srcpermloctax[srcpcntloctab[srcpartloctax[vertlocnum]] ++] = srcpermgsttax[vertlocnum];
    }

  valusnddsptab[0] = 0;

  Gnum procngbnum;

  for (procngbnum = 1 ; procngbnum < dstmeshptr->procglbnbr + 1 ; procngbnum ++) {
    vcntloctab[procngbnum - 1] = baseval;
    valusnddsptab[procngbnum] = valusnddsptab[procngbnum - 1] + valusndcnttab[procngbnum - 1];
  }

  for (vertlocnum = baseval, vertlocnnd = srcmeshptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) { // XXX vertlocnbr ou vertlocmax ??
    Gnum procngbnum;

    if (vtruloctax[vertlocnum] != 0) {
      procngbnum = srcpartloctax[vertlocnum];
      vprmloctax[vertlocnum] = valusnddsptab[procngbnum] + vcntloctab[procngbnum] ++;
    }
  }

  // XXX ne se limite-on pas en mettant MPI_INT ?
  CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
  if (commAlltoall(valusndcnttab, 1, MPI_INT, valurcvcnttab, 1, MPI_INT, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);

  valurcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < dstmeshptr->procglbnbr + 1 ; procngbnum ++) {
    valurcvdsptab[procngbnum] = valurcvdsptab[procngbnum - 1] + valurcvcnttab[procngbnum - 1];
  }
  vrcvlocnbr = valurcvdsptab[dstmeshptr->procglbnbr];
  dstpermlocnbr = (srcpermgsttax == NULL) ? 0 : vrcvlocnbr;


  /* Exchanging entity numbers */
  if (memAllocGroup ((void **) (void *)
        &dstenttloctax, (size_t)     (vrcvlocnbr         * sizeof (Gnum)),
        &dstenttloctx2, (size_t)     (dstpermlocnbr      * sizeof (Gnum)),
        &dstpermloctax, (size_t)    ((dstpermlocnbr * 2) * sizeof (Gnum)),
        &dstenttpermloctax, (size_t) (dstpermlocnbr      * sizeof (DmeshRedistVert)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcmeshptr->proccomm);

  dstenttloctax -= baseval;
  dstenttloctx2 = (srcpermgsttax == NULL) ? NULL : dstenttloctx2 - baseval;
  dstpermloctax = (srcpermgsttax == NULL) ? NULL : dstpermloctax - baseval;
  dstenttpermloctax = (srcpermgsttax == NULL) ? NULL : dstenttpermloctax - baseval;

  memSet (dstenttloctax + baseval, ~0, vrcvlocnbr * sizeof (Gnum)); /* We suppose that PAMPA_ENTT_DUMMY is ~0 */


  for (enttlocnum = baseval, enttlocnnd = srcmeshptr->enttglbnbr + baseval ; enttlocnum < enttlocnnd ; enttlocnum ++) {
    Gnum vertlocnum;
    Gnum vertlocnnd;
    DmeshEntity * restrict enttlocptr;

    if ((srcmeshptr->enttloctax[enttlocnum] == NULL) || (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] > PAMPA_ENTT_SINGLE))
      continue;

    enttlocptr = srcmeshptr->enttloctax[enttlocnum];

    for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
      Gnum bvrtlocnum;
      Gnum esublocnum;

      bvrtlocnum = vprmloctax[enttlocptr->mvrtloctax[vertlocnum] - vertlocbas];
      if (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] < PAMPA_ENTT_SINGLE)
        esublocnum = enttlocptr->ventloctax[vertlocnum];
      else
        esublocnum = enttlocnum;
      srcenttloctax[bvrtlocnum] = esublocnum;

    }
  }

  if (dstpermloctax != NULL) {
    CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
    if (commAlltoallv(srcpermloctax + baseval, valusndcnttab, valusnddsptab, GNUM_MPI, dstpermloctax + baseval, valurcvcnttab, valurcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      return (1);
    }
  }

  CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
  if (commAlltoallv(srcenttloctax + baseval, valusndcnttab, valusnddsptab, GNUM_MPI, dstenttloctax + baseval, valurcvcnttab, valurcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }


  CHECK_FERR (dmeshDgraphInit(srcmeshptr, &srcgrafptr), srcmeshptr->proccomm);
  CHECK_FERR (dmeshDgraphBuild(srcmeshptr, DMESH_ENTT_ANY, ~0, NULL, NULL, NULL, &srcgrafptr, NULL), srcmeshptr->proccomm); // XXX pas de velo ni de edlo ??
  SCOTCH_dgraphData(&srcgrafptr, &baseval, NULL, &vertlocnbr, NULL, NULL, NULL, NULL, NULL, &vlblloctax, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
  vlblloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++)
    partloctx2[vertlocnum] = srcpartloctax[vlblloctax[vertlocnum] - vertlocbas];

#ifdef PAMPA_DEBUG_DGRAPH
  char s[30];
  sprintf(s, "dgraph-redist-%d", srcmeshptr->proclocnum);
  FILE *dgraph_file;
  dgraph_file = fopen(s, "w");
  SCOTCH_dgraphSave (&srcgrafptr, dgraph_file);
  fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

  CHECK_FERR (dmeshDgraphInit(dstmeshptr, &dstgrafptr), dstmeshptr->proccomm);
  if (srcpermgsttax != NULL) // XXX doit-on le faire tout le temps ?
    CHECK_FERR (SCOTCH_dgraphHalo (&srcgrafptr, srcpermgsttax + baseval, GNUM_MPI), srcmeshptr->proccomm); // XXX est-ce que ça marche avec des sommets à gruyère ?

  CHECK_FERR (SCOTCH_dgraphRedist (&srcgrafptr, partloctx2 + baseval, (srcpermgsttax != NULL) ? (srcpermgsttax + baseval) : (NULL), dstvertlocdlt, dstedgelocdlt, &dstgrafptr), srcmeshptr->proccomm); // XXX à changer au niveau des paramètres
#ifdef PAMPA_DEBUG_DMESH_DGRAPH
  CHECK_FERR (SCOTCH_dgraphCheck(&dstgrafptr), srcmeshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH_DGRAPH */

  CHECK_FERR (SCOTCH_dgraphGhst (&srcgrafptr), srcmeshptr->proccomm);

  // XXX faire les check pour les retours de fonctions
  SCOTCH_dgraphData(&dstgrafptr, &baseval, NULL, &vertlocnbr, &vertlocmax, NULL, &vertloctax, &vendloctax, NULL, &vlblloctax, NULL, &edgelocnbr, NULL, &edgeloctax, &edgegsttax, &edloloctax, NULL); 
  vertloctax -= baseval;
  vendloctax = (vendloctax != NULL) ? vendloctax - baseval : NULL;
  vlblloctax = (vlblloctax != NULL) ? vlblloctax - baseval : NULL;
  edgeloctax -= baseval;
  edgegsttax = (edgegsttax != NULL) ? edgegsttax - baseval : NULL;
  edloloctax = (edloloctax != NULL) ? edloloctax - baseval : NULL;

  if (dstpermloctax != NULL) {
    Gnum vertlocbas;

    CHECK_FERR (dmeshBuild2 (dstmeshptr, baseval, vertlocnbr, vertlocmax), dstmeshptr->proccomm);
    vertlocbas = dstmeshptr->procvrttab[dstmeshptr->proclocnum] - baseval;

    // XXX il faut faire des tests avec des sommets à gruyère
    // XXX 3012 (attention deux endroits à corriger)
    // XXX il faut appeler dmeshbuild2 et redonner les paramètres au dmeshbuild
    // qui suit
    // après l'appel du dmeshbuild2, on peut calculer le vertlocbas et le
    // soustraire à chaque valeur de dstpermloctax
    // attention toutefois aux sommets avec des gruyères, que se passe-t-il dans
    // ce cas ?
    //CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
    ////if (MPI_Alltoallv(srcpermgsttax + baseval, valusndcnttab, valusnddsptab, GNUM_MPI, dstpermloctax + baseval, valurcvcnttab, valurcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
    ////  errorPrint ("communication error");
    ////  return (1);
    //}

    for (vertlocnum = baseval, vertlocnnd = baseval + vrcvlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
      if (dstpermloctax[vertlocnum] != ~0) { // XXX ne pourrait-on pas faire sans le postulat sur dstpermloctax ?
        dstpermloctax[vertlocnum] -= vertlocbas;
        dstenttloctx2[dstpermloctax[vertlocnum]] = dstenttloctax[vertlocnum];
      }
    }
    
    memCpy (dstenttloctax + baseval, dstenttloctx2 + baseval, vrcvlocnbr * sizeof (Gnum));
  }

  // XXX voir comment faire pour les valeurs uniques comme valuglbmax,
  // esubloctax pour les diffuser

  CHECK_FERR (dmeshBuild (dstmeshptr, baseval, vertlocnbr, vertlocmax, vertloctax, vendloctax, edgelocnbr, edgelocnbr, edgeloctax, edloloctax, enttglbnbr, dstenttloctax, esubglbtax, valuglbmax, dstmeshptr->ovlpglbval, NULL, -1, NULL, NULL, NULL, -1, -1, NULL), srcmeshptr->proccomm);

  SCOTCH_dgraphExit (&dstgrafptr);
  CHECK_FERR (dmeshDgraphExit(&srcgrafptr), srcmeshptr->proccomm);
  
  {
    Dvalues * valslocptr = srcmeshptr->valslocptr;
    Dvalue * restrict valulocptr;

    /* Entity PAMPA_ENTT_VIRT_VERT */
    for (valulocptr = valslocptr->evalloctak[PAMPA_ENTT_VIRT_VERT]; valulocptr != NULL; valulocptr = valulocptr->next) {
      byte * restrict dstvaluloctmp;
      byte * restrict dstvaluloctm2;
      int typesiz;
      Gnum dummy;
      Gnum dummy2;



      if (MPI_Type_size(valulocptr->typeval, &typesiz) != MPI_SUCCESS) {
        errorPrint ("get size error");
        return (1);
      }

      CHECK_FERR (dmeshValueLink(dstmeshptr, (void **) &dstvaluloctmp, (valulocptr->flagval & ~PAMPA_VALUE_ALLOCATED), &dummy, &dummy2, valulocptr->typeval, PAMPA_ENTT_VIRT_VERT, valulocptr->tagnum), srcmeshptr->proccomm);

      if (dstpermloctax == NULL) {
        dstvaluloctm2 = dstvaluloctmp;
      }
      else {
        if ((dstvaluloctm2 = (byte *) memAlloc (dstmeshptr->vertlocmax * typesiz * sizeof(byte))) == NULL) {
          memFree    (dstvaluloctm2);
          errorPrint ("out of memory");
          return (1);
        }
      }
        
      // XXX et si les sommets locaux ne sont pas compacts (avec des trous),
      // valuloctab n'est pas correct, à tester !
      CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
      if (commAlltoallv(valulocptr->valuloctab, valusndcnttab, valusnddsptab, valulocptr->typeval, dstvaluloctm2, valurcvcnttab, valurcvdsptab, valulocptr->typeval, proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        return (1);
      }

      if (dstpermloctax != NULL) {
        for (vertlocnum = baseval, vertlocnnd = baseval + dstmeshptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          if (dstpermloctax[vertlocnum] != ~0) // XXX ne pourrait-on pas faire sans le postulat sur dstpermloctax ?
            memCpy (dstvaluloctmp + (dstpermloctax[vertlocnum] - baseval) * typesiz, dstvaluloctm2 + (vertlocnum - baseval) * typesiz, typesiz);
        
        memFree (dstvaluloctm2 + baseval);
      }
    }


    /* Entity PAMPA_ENTT_VIRT_PROC */
    for (valulocptr = valslocptr->evalloctak[PAMPA_ENTT_VIRT_PROC]; valulocptr != NULL; valulocptr = valulocptr->next) {
      byte * restrict dstvaluloctmp;
      int typesiz;
      Gnum dummy;
      Gnum dummy2;



      if (MPI_Type_size(valulocptr->typeval, &typesiz) != MPI_SUCCESS) {
        errorPrint ("get size error");
        return (1);
      }

      CHECK_FERR (dmeshValueLink(dstmeshptr, (void **) &dstvaluloctmp, (valulocptr->flagval & ~PAMPA_VALUE_ALLOCATED), &dummy, &dummy2, valulocptr->typeval, PAMPA_ENTT_VIRT_PROC, valulocptr->tagnum), srcmeshptr->proccomm);

      memCpy (dstvaluloctmp, valulocptr->valuloctab, dstmeshptr->procglbnbr * typesiz); // XXX est-ce que le nb de procs est toujours le meme entre src et dst ?
    }
  }

  if (dstpermloctax != NULL) {
    for (vertlocnum = baseval + vrcvlocnbr - 1, vertlocnnd = baseval - 1; vertlocnum > vertlocnnd; vertlocnum --) {
      if (dstpermloctax[vertlocnum] != ~0) { // XXX ne pourrait-on pas faire sans le postulat sur dstpermloctax ?
        dstpermloctax[vertlocnum] -= vertlocbas;
        dstenttloctx2[dstpermloctax[vertlocnum]] = dstenttloctax[vertlocnum];
        dstpermloctax[2 * vertlocnum] = dstpermloctax[vertlocnum];
        dstpermloctax[2 * vertlocnum + 1] = vertlocnum;
      }
    }

    intSort2asc1 (dstpermloctax + baseval, vrcvlocnbr);
    dstpermloctx2 = (DmeshRedistVert *) (dstpermloctax + baseval) - baseval;
  }
  else
    dstpermloctx2 = NULL;

  for (enttlocnum = baseval, enttlocnnd = srcmeshptr->enttglbnbr + baseval; enttlocnum < enttlocnnd; enttlocnum ++) {
    DmeshEntity * restrict enttlocptr;
	Dvalues * valslocptr = srcmeshptr->valslocptr;
    Dvalue * restrict valulocptr;

    enttlocptr = srcmeshptr->enttloctax[enttlocnum];

    if (enttlocptr == NULL)
      continue;

    if (valslocptr->evalloctak[enttlocnum] != NULL) {

      memSet (valusndcnttab,  0, dstmeshptr->procglbnbr * sizeof (int));

      for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
        Gnum bvrtlocnum;
        if (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
          bvrtlocnum = enttlocptr->mvrtloctax[enttlocptr->svrtloctax[vertlocnum]] - vertlocbas;
        else
          bvrtlocnum = enttlocptr->mvrtloctax[vertlocnum] - vertlocbas;
        valusndcnttab[srcpartloctax[bvrtlocnum]] ++;
      }



      valusnddsptab[0] = 0;

      Gnum procngbnum;

      for (procngbnum = 1 ; procngbnum < dstmeshptr->procglbnbr + 1 ; procngbnum ++) {
        vcntloctab[procngbnum - 1] = baseval;
        valusnddsptab[procngbnum] = valusnddsptab[procngbnum - 1] + valusndcnttab[procngbnum - 1];
      }

      for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
        Gnum procngbnum;
        Gnum bvrtlocnum;

        if (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
          bvrtlocnum = enttlocptr->mvrtloctax[enttlocptr->svrtloctax[vertlocnum]] - vertlocbas;
        else
          bvrtlocnum = enttlocptr->mvrtloctax[vertlocnum] - vertlocbas;
        procngbnum = srcpartloctax[bvrtlocnum];
        vprmloctax[bvrtlocnum] = valusnddsptab[procngbnum] + vcntloctab[procngbnum] ++;
      }

      if (dstenttpermloctax != NULL) {
        Gnum dstvertlocbas;

        enttlocptr = dstmeshptr->enttloctax[enttlocnum];
        dstvertlocbas = dstmeshptr->procvrttab[dstmeshptr->proclocnum] - baseval;
        for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
          Gnum bvrtlocnum;

          if (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
            bvrtlocnum = enttlocptr->mvrtloctax[enttlocptr->svrtloctax[vertlocnum]] - dstvertlocbas;
          else
            bvrtlocnum = enttlocptr->mvrtloctax[vertlocnum] - dstvertlocbas;

          dstenttpermloctax[vertlocnum].vertnum = dstpermloctx2[bvrtlocnum].indxval;
          dstenttpermloctax[vertlocnum].indxval = vertlocnum;
        }

        intSort2asc1 (dstenttpermloctax + baseval, enttlocptr->vertlocnbr);

        for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval ; vertlocnum < vertlocnnd ; vertlocnum ++) {
          dstenttpermloctax[dstenttpermloctax[vertlocnum].indxval].vertnum = vertlocnum;
        }

        enttlocptr = srcmeshptr->enttloctax[enttlocnum];
      }

      CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
      if (commAlltoall(valusndcnttab, 1, MPI_INT, valurcvcnttab, 1, MPI_INT, proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        return (1);
      }

      valurcvdsptab[0] = 0;
      for (procngbnum = 1 ; procngbnum < dstmeshptr->procglbnbr + 1 ; procngbnum ++) {
        valurcvdsptab[procngbnum] = valurcvdsptab[procngbnum - 1] + valurcvcnttab[procngbnum - 1];
      }
    }



    for (valulocptr = valslocptr->evalloctak[enttlocnum]; valulocptr != NULL; valulocptr = valulocptr->next) {
      byte * restrict srcvaluloctmp;
      byte * restrict dstvaluloctmp;
      byte * restrict dstvaluloctm2;
      int typesiz;
      Gnum dummy;
      Gnum dummy2;



      if (MPI_Type_size(valulocptr->typeval, &typesiz) != MPI_SUCCESS) {
        errorPrint ("get size error");
        return (1);
      }

      if ((srcvaluloctmp = (byte *) memAlloc (srcmeshptr->enttloctax[enttlocnum]->vertlocnbr * typesiz * sizeof(byte))) == NULL) {
        errorPrint ("out of memory");
        return (1);
      }
      srcvaluloctmp -= baseval * typesiz;

      for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval; vertlocnum < vertlocnnd ; vertlocnum ++) {
        Gnum bvrtlocnum;

        if (srcmeshptr->esublocbax[enttlocnum & srcmeshptr->esublocmsk] > PAMPA_ENTT_SINGLE)
          bvrtlocnum = vprmloctax[srcmeshptr->enttloctax[enttlocnum]->mvrtloctax[srcmeshptr->enttloctax[enttlocnum]->svrtloctax[vertlocnum]] - vertlocbas];
        else
          bvrtlocnum = vprmloctax[srcmeshptr->enttloctax[enttlocnum]->mvrtloctax[vertlocnum] - vertlocbas];
        memCpy (srcvaluloctmp + bvrtlocnum * typesiz, (byte *) valulocptr->valuloctab + (vertlocnum - baseval) * typesiz, typesiz);
      }

      CHECK_FERR (dmeshValueLink(dstmeshptr, (void **) &dstvaluloctmp, (valulocptr->flagval & ~PAMPA_VALUE_ALLOCATED), &dummy, &dummy2, valulocptr->typeval, enttlocnum, valulocptr->tagnum), srcmeshptr->proccomm);
      if (dstpermloctx2 == NULL) {
        dstvaluloctm2 = dstvaluloctmp;
      }
      else {
        if ((dstvaluloctm2 = (byte *) memAlloc (dstmeshptr->enttloctax[enttlocnum]->vertlocnbr * typesiz * sizeof(byte))) == NULL) {
          memFree    (dstvaluloctm2);
          errorPrint ("out of memory");
          return (1);
        }
      }
        

      CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
      if (commAlltoallv(srcvaluloctmp + baseval * typesiz, valusndcnttab, valusnddsptab, valulocptr->typeval, dstvaluloctm2, valurcvcnttab, valurcvdsptab, valulocptr->typeval, proccomm) != MPI_SUCCESS) {
        errorPrint ("communication error");
        return (1);
      }

      if (dstpermloctax != NULL) {
        enttlocptr = dstmeshptr->enttloctax[enttlocnum];
        for (vertlocnum = baseval, vertlocnnd = baseval + enttlocptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          // XXX 3012 : la copie n'est pas bonne, car la permutation n'est pas
          // par entité
          memCpy (dstvaluloctmp + (dstenttpermloctax[vertlocnum].vertnum - baseval) * typesiz, dstvaluloctm2 + (vertlocnum - baseval) * typesiz, typesiz);
        enttlocptr = srcmeshptr->enttloctax[enttlocnum];
        memFree (dstvaluloctm2);
      }

      memFree (srcvaluloctmp + baseval * typesiz);
    }
  }

  memFreeGroup (dstenttloctax + baseval);
  memFreeGroup (valusndcnttab);
  CHECK_VDBG (cheklocval, srcmeshptr->proccomm);
  return (0);
}

