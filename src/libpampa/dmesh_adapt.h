/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Aug 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

// FIXME il faudrait séparer les tags de la fonction pour éviter d'inclure ptscotch dans dmesh_rebuild.c
#define VERT_EXTERNAL (~0) /* ~0 in order to fill in arrays with ~0 */
#define VERT_INTERNAL ((~0) - 1)
#define VERT_BOUNDARY ((~0) - 2)


#define TAG_INT_REMESH      -44

#define DMESHADAPTHASHPRIME     17            //!< Prime number for hashing

#ifdef DMESH_ADAPT
int dmeshAdaptBase (
    Dmesh * const                srcmeshptr,
    const Gnum                   bandval,
    const Gnum                   ballsiz,
    int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
    int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
    int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void * const                 dataptr,
    Dmesh * const                dstmeshptr);

int dmeshAdaptCoarsen (
    Dmesh * const                srcmeshptr,
    const Gnum                   bandval,
    const Gnum                   ballsiz,
    int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
    int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
    int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void * const                 dataptr,
    Dmesh * const                dstmeshptr);

int dmeshAdaptGrow (
    Dmesh * const                srcmeshptr,
    const Gnum                   bandval,
    const Gnum                   ballsiz,
    int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
    int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
    int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void * const                 dataptr,
    Dmesh * const                dstmeshptr);

int dmeshAdaptCoarsenGrow (
    Dmesh * const                srcmeshptr,
    const Gnum                   bandval,
    const Gnum                   ballsiz,
    int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
    int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
    int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
    int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
    void * const                 dataptr,
    Dmesh * const                dstmeshptr);

int dmeshAdaptPart (
    Dmesh * const                srcmeshptr,
    PAMPA_AdaptInfo * const      infoptr,
    Dmesh * const                dstmeshptr);

#endif /* DMESH_ADAPT */
