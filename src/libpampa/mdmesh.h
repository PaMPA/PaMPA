/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
#define MDMESH_H

/*
** The defines.
*/

/* mesh flags. */

#define MDMESHNONE                 0x0000        //!< No options set

#define MDMESHFREEPRIV             0x0001        //!< Set if private arrays freed on exit
#define MDMESHFREECOMM             0x0002        //!< MPI communicator has to be freed
#define MDMESHFREEDMESH            0x0004        //!< Set if levels freed on exit
#define MDMESHFREEALL              (MDMESHFREEPRIV | MDMESHFREECOMM | MDMESHFREEDMESH)

/*
** The type and structure definitions.
*/


typedef int MdmeshFlag;                           //!< Multiple distributed mesh property flags


typedef struct Mdmesh_
{
  MdmeshFlag           flagval;       //!< Mesh properties
  Gnum                 baseval;
  Gnum                 dmshglbnxt;
  Gnum                 dmshglbnbr;
  Dmesh *              dmshglbtax;
  Gnum *               levlglbtax;
  MPI_Comm             proccomm;      //!< Mesh communicator
} Mdmesh;

/*
** The functions prototypes.
*/

int mdmeshInit (
    Mdmesh * const               meshptr,
    Gnum const                   baseval,
    Gnum const                   mdmhglbnbr,
    MPI_Comm                     proccomm);

int mdmeshItData (
    const Mdmesh * const         meshptr,
    Gnum const                   vlvllocnum,
    Gnum const                   ventlocnum,
    Gnum const                   nlvllocnum,
    Gnum const                   nesblocnum,
    Gnum *                       nentlocnum,
    Gnum *                       ngstlocnbr,
    const Gnum * *               vindloctab,
    const Gnum * *               edgeloctab,
    Gnum *                       edgelocsiz,
    const Gnum * *               svrtloctab,
    const Gnum * *               mngbloctab,
    const Gnum * *               nentlocbas,
    Gnum *                       nentlocmsk,
    const Gnum **                nentloctab,
    const Gnum * *               sngbloctab,
    Gnum *                       vnumlocptr,
    Gnum *                       vnndlocptr,
    Gnum                         langflg);

void mdmeshExit (
    Mdmesh * const               meshptr);

void mdmeshFree (
    Mdmesh * const               meshptr);

int mdmeshBuild (
    Mdmesh * const               meshptr,
    const Gnum                   dmshglbnbr,
    const Dmesh *                dmshtax);

int mdmeshCheck (
    Mdmesh * const               meshptr);

int mdmeshLevelInit (
    Mdmesh * restrict const      mdmhptr,
    Dmesh * restrict const       meshptr);

int mdmeshCoarLevelBuild (
    Mdmesh * const               meshptr,
    Gnum * const                 meshglbptr,
    const Gnum                   velolocsiz,
    const Gnum *                 veloloctax,
    const Gnum                   edlolocsiz,
    const Gnum *                 edloloctax,
    const Gnum *                 enttglbtax,
    const Gnum                   elvlglbsiz,
    const Gnum *                 elvlglbtax,
    const Gnum *                 lnumglbtax,
    const Gnum *                 etagglbtax,
    const Gnum                   loadglbval,
    const Gnum                   flagval);

int mdmeshDmeshData (
    Mdmesh * const               meshptr,
    const Gnum                   levlval,
    Dmesh *                      dmshptr);

int mdmeshRedist (
    Mdmesh * const       srcmeshptr,
    Gnum * const         partloctax,
    Gnum   const         vertlocdlt,
    Gnum   const         edgelocdlt,
    Gnum const           ovlpglbval,
    Mdmesh * const       dstmeshptr);

int mdmeshValueLink (
    Mdmesh * restrict const      meshptr,
    void **                      valuloctab,
    Gnum                         flagval,
    Gnum *                        sizeval,
    Gnum *                        typesiz,
    MPI_Datatype                 typeval,
    Gnum                         enttnum,
    Gnum                         tagnum);


int mdmeshValueTagUnlink (
    Mdmesh * restrict const      meshptr,
    Gnum                         tagnum);

int mdmeshValueUnlink (
    Mdmesh * restrict const      meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum);

int mdmeshValueData (
    Mdmesh * restrict const      meshptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                        sizeval,
    Gnum *                        typesiz,
    void **                      valuloctab);
