/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_iter.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contains the distributed mesh
//!                iterator purpose routines.
//!
//!   \date        Version 1.0: from: 31 May 2011
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_ghst.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* mesh iterator handling routines.     */
/*                                      */
/****************************************/

int
dmeshItData (
const Dmesh * const    meshptr,                                    //!< Distributed mesh
Gnum const             ventlocnum,                                 //!< Entity number of vertex
Gnum const             nesblocnum,                                 //!< Entity number of neighbors
Gnum *                 nentlocnum,                                 //!< XXX
Gnum *                 ngstlocnbr,                                 //!< XXX
const Gnum * *         vindloctab,                                 //!< XXX
const Gnum * *         edgeloctab,                                 //!< XXX
Gnum *                 edgelocsiz,                                 //!< XXX
const Gnum * *         svrtloctab,                                 //!< XXX
const Gnum * *         mngbloctab,                                 //!< XXX
const Gnum * *         nentlocbas,                                 //!< XXX
Gnum *                 nentlocmsk,                                 //!< XXX
const Gnum **          nentloctab,                                 //!< XXX
const Gnum * *         sngbloctab,                                 //!< XXX
Gnum *                 vnumlocptr,                                 //!< XXX
Gnum *                 vnndlocptr,                                 //!< XXX
Gnum                   langflg)
{

  Gnum * nentloctax;
  Gnum   baseval;
  Gnum   basevl2;
  Gnum   cheklocval;
  Gnum   nesblocnm2;

  baseval = meshptr->baseval;
  basevl2 = langflg;

#ifdef PAMPA_DEBUG_ITER2
  if ((ventlocnum < baseval) || (ventlocnum >= meshptr->enttglbnbr + baseval)) {
    errorPrint("invalid entity number of vertex");
    return (1);
  }
#endif /* PAMPA_DEBUG_ITER2 */

  if (meshptr->enttloctax[ventlocnum] == NULL) {
    //errorPrint ("There is no vertex in entity %d", ventlocnum);
    *vindloctab = NULL;
    *vnumlocptr = 0;
    *vnndlocptr = 0;

    return (0);
  }

  *svrtloctab = NULL;
  if ((nesblocnum < 0) && (meshptr->enttloctax[ventlocnum]->vprmloctax == NULL)) {
    cheklocval = dmeshItBuild(meshptr, ventlocnum);
    if (cheklocval != 0)
      return cheklocval;
  }

  *vnumlocptr =
    *vnndlocptr = -1;

  switch (nesblocnum) {
    case PAMPA_VERT_ANY : 
      *vnumlocptr = baseval;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vovplocnbr;
      break;
    case PAMPA_VERT_LOCAL : 
      *vnumlocptr = baseval;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vertlocnbr;
      break;
    case PAMPA_VERT_LOCAL & PAMPA_VERT_NOT_FRONTIER : 
      *vnumlocptr = baseval;
      *vnndlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmin;
      break;
    case PAMPA_VERT_BOUNDARY & PAMPA_VERT_FRONTIER :
    case PAMPA_VERT_LOCAL & PAMPA_VERT_FRONTIER : 
      *vnumlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmin;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vertlocnbr;
      break;
    case PAMPA_VERT_INTERNAL : 
      *vnumlocptr = baseval;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vintlocnbr;
      break;
    case PAMPA_VERT_BOUNDARY :
      *vnumlocptr = baseval + meshptr->enttloctax[ventlocnum]->vintlocnbr;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vertlocnbr;
      break;
    case PAMPA_VERT_OVERLAP & PAMPA_VERT_FRONTIER :
      *vnumlocptr = baseval + meshptr->enttloctax[ventlocnum]->vertlocnbr;
      *vnndlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmax;
      break;
    case PAMPA_VERT_LOCAL | PAMPA_VERT_FRONTIER :
      *vnumlocptr = baseval;
      *vnndlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmax;
      break;
    case PAMPA_VERT_FRONTIER | PAMPA_VERT_OVERLAP :
      *vnumlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmin;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vovplocnbr;
      break;
    case PAMPA_VERT_OVERLAP & PAMPA_VERT_NOT_FRONTIER :
      *vnumlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmax + 1;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vovplocnbr;
      break;
    case PAMPA_VERT_FRONTIER :
      *vnumlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmin;
      *vnndlocptr = meshptr->enttloctax[ventlocnum]->vfrnlocmax;
      break;
    case PAMPA_VERT_OVERLAP :
      *vnumlocptr = baseval + meshptr->enttloctax[ventlocnum]->vertlocnbr;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vovplocnbr;
      break;
    case PAMPA_VERT_GHOST :
      *vnumlocptr = baseval + meshptr->enttloctax[ventlocnum]->vertlocnbr;
      *vnndlocptr = baseval + meshptr->enttloctax[ventlocnum]->vgstlocnbr;
      break;
    default :
      if ((nesblocnum < baseval) || (nesblocnum >= meshptr->enttglbnbr + baseval)) {
        errorPrint ("Invalid entity number of neighbor or flag number");
        return -1;
      }

      if (meshptr->enttloctax[ventlocnum]->nghbloctax[nesblocnum] == NULL) {
        //errorPrint ("internal error");
        *vindloctab = NULL;
        *vnumlocptr = 
        *vnndlocptr = 0;

        return (0);
      }

#ifdef PAMPA_DEBUG_ITER
      // XXX TODO modifier l'affectation quand on a un recouvrement
      if (meshptr->ovlpglbval == 0)
        *vnndlocptr = meshptr->enttloctax[ventlocnum]->vovplocnbr - 1 + baseval;
      else
        *vnndlocptr = ~0;
#endif /* PAMPA_DEBUG_ITER */
      *vindloctab = (Gnum *) (meshptr->enttloctax[ventlocnum]->nghbloctax[nesblocnum]->vindloctax + basevl2);
      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
        *svrtloctab = meshptr->enttloctax[ventlocnum]->svrtloctax + basevl2;
      }
      *edgeloctab = meshptr->edgeloctax;
      *edgelocsiz = meshptr->edgelocsiz; //! \warning edgelocsiz is used, maybe it has to be modified
  }

  if (nesblocnum < 0) {
    *edgeloctab = meshptr->enttloctax[ventlocnum]->vprmloctax;
    *edgelocsiz = meshptr->enttloctax[ventlocnum]->vovplocnbr;
    nesblocnm2 = ventlocnum;
  }
  else
    nesblocnm2 = nesblocnum;
//#ifdef PAMPA_DEBUG_ITER2
//    if (((vertlocnum > meshptr->enttloctax[ventlocnum]->vloclocnbr + baseval)
//          && (meshptr->ovlpglbval > 0))
//        || ((vertlocnum > meshptr->enttloctax[ventlocnum]->vertlocnbr + baseval)
//          && (meshptr->ovlpglbval == 0))) {
//      errorPrint("invalid vertex number");
//      return (1);
//    }
//#endif /* PAMPA_DEBUG_ITER2 */


  



  *mngbloctab = meshptr->enttloctax[nesblocnm2]->mvrtloctax + basevl2;
  *nentlocnum = nesblocnm2;

  switch (meshptr->esublocbax[nesblocnm2 & meshptr->esublocmsk]) {
    case PAMPA_ENTT_SINGLE :
      *ngstlocnbr = 1;
      *nentlocmsk = 0;

      *nentlocbas = 
      *nentloctab = 
      *sngbloctab = NULL;
      break;

    case PAMPA_ENTT_STABLE :
      *ngstlocnbr = meshptr->enttloctax[nesblocnm2]->vgstlocnbr;
      *nentlocbas = NULL;
      *nentloctab = meshptr->enttloctax[nesblocnm2]->ventloctax + basevl2;
      *nentlocmsk = 0;

      *sngbloctab = meshptr->enttloctax[nesblocnm2]->svrtloctax + basevl2;
      break;

    default : // \attention case "> PAMPA_ENTT_SINGLE"
      *nentlocnum = meshptr->esublocbax[nesblocnm2];
      *ngstlocnbr = meshptr->enttloctax[*nentlocnum]->vgstlocnbr;

      nentloctax = meshptr->enttloctax[*nentlocnum]->ventloctax;
      *nentlocbas = nentloctax + basevl2;
      *nentloctab = nentloctax + basevl2;
      *nentlocmsk = ~((Gnum) 0);

      *sngbloctab = meshptr->enttloctax[*nentlocnum]->svrtloctax + basevl2;

  }

  *edgeloctab += basevl2;



  return (0);
}


int
dmeshItBuild (
    const Dmesh * const meshptr,                                   //!< XXX
    Gnum const          ventlocnum)                                //!< XXX
{

  Gnum procsidnum;
  Gnum procsidnnd;
  Gnum vertlocnm2;
  Gnum vintlocnbr; // internal vertex number
  Gnum * vintloctax;
  Gnum vbndlocnbr; // boundary vertex number
  Gnum * vbndloctax;
  Gnum vfrnlocnbr; // middle overlap vertex number
  Gnum * vfrnloctax;
  Gnum vovplocnbr; // overlap vertex number
  Gnum * vovploctax;
  Gnum vovplocsiz;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  DmeshEntity * enttlocptr;
  Gnum vertlocmax = meshptr->enttloctax[meshptr->baseval]->vertlocnbr + meshptr->baseval; 

  enttlocptr = meshptr->enttloctax[ventlocnum];

  if (enttlocptr->vprmloctax != NULL) /* Already filled */
    return (0);

  vbndlocnbr = meshptr->baseval;
  vintlocnbr = meshptr->baseval; 
  vfrnlocnbr = meshptr->baseval;
  if (meshptr->ovlpglbval > 0) {
    vovplocsiz = enttlocptr->vgstlocnbr;
    vovplocnbr = meshptr->baseval;
  }
  else {
    vovplocsiz = enttlocptr->vertlocnbr;
    vovplocnbr = enttlocptr->vertlocnbr + meshptr->baseval;
  }

  if (memAllocGroup ((void **) (void *)
        &vintloctax, (size_t) (enttlocptr->vertlocnbr * sizeof (Gnum)),
        &vfrnloctax, (size_t) (MAX(enttlocptr->vertlocnbr, enttlocptr->vgstlocnbr) * sizeof (Gnum)), // à changer le nom
        &vovploctax, (size_t) ((enttlocptr->vgstlocnbr - enttlocptr->vertlocnbr) * sizeof (Gnum)), // à changer le nom
        &vbndloctax, (size_t) (enttlocptr->commlocdat.procsidnbr * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  vintloctax -= meshptr->baseval;
  vbndloctax -= meshptr->baseval;
  vfrnloctax -= meshptr->baseval;
  vovploctax -= meshptr->baseval;

 if ((enttlocptr->vprmloctax = (Gnum *) memAlloc (sizeof (Gnum) * vovplocsiz)) == NULL) {
   errorPrint  ("out of memory");
   return      (1);
 }
 enttlocptr->vprmloctax -= meshptr->baseval;

 for (vertlocnum = vertlocnm2 = meshptr->baseval, procsidnum = 0; procsidnum < enttlocptr->commlocdat.procsidnbr && enttlocptr->commlocdat.procsidtab[procsidnum] < 0; procsidnum ++)
   if (enttlocptr->commlocdat.procsidtab[procsidnum] < 0) {
     vertlocnum -= enttlocptr->commlocdat.procsidtab[procsidnum];

     if (enttlocptr->commlocdat.procsidtab[procsidnum] != -DMESHGHSTSIDMAX) {
       Gnum uvrtlocnm2;

       for (; vertlocnm2 < vertlocnum ; vertlocnm2 ++) {
         if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
           uvrtlocnm2 = vertlocnm2; // entt or sub-entt, trouver un meilleur nom
         else
           uvrtlocnm2 = enttlocptr->svrtloctax[vertlocnm2];

         vintloctax[vintlocnbr ++] = uvrtlocnm2;
       }
     }
     vertlocnm2 = vertlocnum;
     if (enttlocptr->commlocdat.procsidtab[procsidnum] != -DMESHGHSTSIDMAX) 
       vertlocnm2 ++;
   }

 if ((enttlocptr->commlocdat.procsidnbr > 0) && (enttlocptr->commlocdat.procsidtab[0] >= 0)) {
   Gnum uvrtlocnum;
   Gnum edgelocnum;
   Gnum edgelocnnd;
   Gnum vfrnval;

   if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
     uvrtlocnum = meshptr->baseval; // entt or sub-entt, trouver un meilleur nom
   else
     uvrtlocnum = enttlocptr->svrtloctax[meshptr->baseval];

   for (edgelocnum = enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[uvrtlocnum].vertlocidx,
       edgelocnnd = enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[uvrtlocnum].vendlocidx,
       vfrnval = 0;
       edgelocnum < edgelocnnd && vfrnval != 3; edgelocnum ++)
     if (meshptr->edgeloctax[edgelocnum] < vertlocmax)
       vfrnval |= 1;
     else if (meshptr->edgeloctax[edgelocnum] >= vertlocmax)
       vfrnval |= 2;

   if (vfrnval == 3)
     vfrnloctax[vfrnlocnbr ++] = uvrtlocnum;
   else
     vbndloctax[vbndlocnbr ++] = uvrtlocnum;
   vertlocnm2 ++;
 }

 for (vertlocnum = meshptr->baseval, procsidnum = 0, procsidnnd = enttlocptr->commlocdat.procsidnbr; procsidnum < procsidnnd; procsidnum ++)
   if (enttlocptr->commlocdat.procsidtab[procsidnum] < 0) {
     Gnum edgelocnum;
     Gnum edgelocnnd;
     Gnum vfrnval;

     vertlocnum -= enttlocptr->commlocdat.procsidtab[procsidnum];

     if (enttlocptr->commlocdat.procsidtab[procsidnum] != -DMESHGHSTSIDMAX) {
       Gnum uvrtlocnum;
       Gnum uvrtlocnm2;

       if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
         uvrtlocnum = vertlocnum; // entt or sub-entt, trouver un meilleur nom
       else
         uvrtlocnum = enttlocptr->svrtloctax[vertlocnum];
       
       for (edgelocnum = enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[uvrtlocnum].vertlocidx,
           edgelocnnd = enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[uvrtlocnum].vendlocidx,
           vfrnval = 0;
           edgelocnum < edgelocnnd && vfrnval != 3; edgelocnum ++)
         if (meshptr->edgeloctax[edgelocnum] < vertlocmax)
           vfrnval |= 1;
         else if (meshptr->edgeloctax[edgelocnum] >= vertlocmax)
           vfrnval |= 2;

       if (vfrnval == 3)
         vfrnloctax[vfrnlocnbr ++] = uvrtlocnum;
       else
         vbndloctax[vbndlocnbr ++] = uvrtlocnum;

       for (; vertlocnm2 < vertlocnum ; vertlocnm2 ++) {
         if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
           uvrtlocnm2 = vertlocnm2; // entt or sub-entt, trouver un meilleur nom
         else
           uvrtlocnm2 = enttlocptr->svrtloctax[vertlocnm2];

         vintloctax[vintlocnbr ++] = uvrtlocnm2;
       }
     }
     vertlocnm2 = vertlocnum + 1;
   }

 for (vertlocnnd = enttlocptr->vertlocnbr + meshptr->baseval; vertlocnm2 < vertlocnnd; vertlocnm2 ++) {
   Gnum uvrtlocnm2;

   if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
     uvrtlocnm2 = vertlocnm2; // entt or sub-entt, trouver un meilleur nom
   else
     uvrtlocnm2 = enttlocptr->svrtloctax[vertlocnm2];

   vintloctax[vintlocnbr ++] = uvrtlocnm2;
 }

 vintlocnbr -= meshptr->baseval;
 vbndlocnbr -= meshptr->baseval;
 vfrnlocnbr -= meshptr->baseval;
 memCpy(enttlocptr->vprmloctax + meshptr->baseval, vintloctax + meshptr->baseval, vintlocnbr * sizeof(Gnum));
 memCpy(enttlocptr->vprmloctax + meshptr->baseval + vintlocnbr, vbndloctax + meshptr->baseval, vbndlocnbr * sizeof(Gnum));
 if (vfrnlocnbr > 0)
   memCpy(enttlocptr->vprmloctax + meshptr->baseval + vintlocnbr + vbndlocnbr, vfrnloctax + meshptr->baseval, vfrnlocnbr * sizeof(Gnum));






 if (meshptr->ovlpglbval > 0) { // XXX 0 ou 0.5 ???
   Gnum enttglbmax = meshptr->enttglbnbr + meshptr->baseval - 1;
   Gnum vertlocmax = meshptr->enttloctax[meshptr->baseval]->vertlocnbr + meshptr->baseval; 

   vfrnlocnbr = meshptr->baseval;

   for (vertlocnum = enttlocptr->vertlocnbr + meshptr->baseval,
       vertlocnnd = enttlocptr->vgstlocnbr + meshptr->baseval;
       vertlocnum < vertlocnnd; vertlocnum ++) {
     Gnum vertlocnm2;

     if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)
       vertlocnm2 = vertlocnum;
     else
       vertlocnm2 = enttlocptr->svrtloctax[vertlocnum];

     if (enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[vertlocnm2].vertlocidx !=
         enttlocptr->nghbloctax[enttglbmax]->vindloctax[vertlocnm2].vendlocidx) { // if vertex is not halo
       Gnum edgelocnum;
       Gnum edgelocnnd;
       Gnum vfrnval;


       for (edgelocnum = enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[vertlocnm2].vertlocidx,
           edgelocnnd = enttlocptr->nghbloctax[meshptr->baseval]->vindloctax[vertlocnm2].vendlocidx,
           vfrnval = 0;
           edgelocnum < edgelocnnd && vfrnval != 3; edgelocnum ++)
         if (meshptr->edgeloctax[edgelocnum] < vertlocmax)
           vfrnval |= 1;
         else if (meshptr->edgeloctax[edgelocnum] >= vertlocmax)
           vfrnval |= 2;

       if (vfrnval == 3)
         vfrnloctax[vfrnlocnbr ++] = vertlocnm2;
       else
         vovploctax[vovplocnbr ++] = vertlocnm2;
     }
   }
   vfrnlocnbr -= meshptr->baseval;
   vovplocnbr -= meshptr->baseval;
   memCpy(enttlocptr->vprmloctax + meshptr->baseval + enttlocptr->vertlocnbr, vfrnloctax + meshptr->baseval, vfrnlocnbr * sizeof(Gnum));

   memCpy(enttlocptr->vprmloctax + meshptr->baseval + vfrnlocnbr + enttlocptr->vertlocnbr, vovploctax + meshptr->baseval, vovplocnbr * sizeof(Gnum));
   vovplocnbr += vfrnlocnbr + enttlocptr->vertlocnbr;
 }
   // XXX à finir

 memFreeGroup (vintloctax + meshptr->baseval);

 enttlocptr->vovplocnbr = vovplocnbr;
 enttlocptr->vfrnlocmin = vbndlocnbr + vintlocnbr + meshptr->baseval;
 enttlocptr->vfrnlocmax = enttlocptr->vertlocnbr + vfrnlocnbr;
 enttlocptr->vintlocnbr = vintlocnbr;

 if ((enttlocptr->vprmloctax = (Gnum *) memRealloc (enttlocptr->vprmloctax + meshptr->baseval, sizeof (Gnum) * enttlocptr->vovplocnbr)) == NULL) {
   errorPrint  ("out of memory");
   return      (1);
 }
 enttlocptr->vprmloctax -= meshptr->baseval;

 return (0);
}
 







