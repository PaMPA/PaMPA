/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dvalues.h
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the distributed value handling routines.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/



#define DVALUES_H

/*
** The type and structure definitions.
*/

/* The mesh basic types, which must be signed. */

#ifndef GNUMMAX                                   
typedef INT                 Gnum;                 /* Vertex or edge number   */
#define GNUMMAX                     (INTVALMAX)   /* Maximum Gnum value      */
#endif /* GNUMMAX */
/*
** The type and structure definitions.
*/

/* The value structure. */

typedef struct Dvalue_            //!  Mesh structure which contains associated values
{
  void *               valuloctab;    //!< Array of mesh associated values
  MPI_Datatype         typeval;       //!< Type of mesh associated values
  Gnum                 flagval;       //!< Flag which could be PAMPA_VALUE_PRIVATE or PAMPA_VALUE_PUBLIC
  Gnum                 enttnum;       //!< Number of entity of which values are associated
  Gnum                 tagnum;        //!< Tag of mesh associated values
  struct Dvalue_ *      next;          //!< Pointer to next associated values with the same entity
} Dvalue;
  
typedef struct Dvalues_           //!  Mesh structure which contains associated values
{
  Gnum                  cuntlocnbr;       //!< Pointer numbers on this Dvalue
  Gnum                  valuglbnbr;
  Gnum                  valuglbmax;
  Dvalue *              valuloctab;     //!< XXX
  Dvalue **             evalloctak;     //!< Array of linked values by entity (based at -1 for C and 0 for fortran)
} Dvalues;
  
int dvaluesInit (
    Dvalues * restrict * const    valslocptr,
    Gnum                          baseval,
    Gnum                          valuglbmax,
    Gnum                          enttglbnbr);

void dvaluesFree (
    Dvalues * restrict const     valsptr);

int dvalueData (
    Dvalues * restrict const       valslocptr,
    Gnum                         enttnum,
    Gnum                         tagnum,
    Gnum *                        typesiz,
	Dvalue **                    valulocptr,
    void **                      valuloctab);
