/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
#define MESH
#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "pampa.h"

// XXX
#define TETR_NBR_NGHB     4
#define TETR_NBR_NODE     4
#define TETR_FAC_NODE     3
#define TRIA_NBR_NODE     3
// XXX

int meshCreate (
    Mesh * restrict const       meshptr,           //!< mesh
    const Gnum                  vertnbr,           //!< Number of local vertices
    Gnum * const                verttab,           //!< Local vertex begin array
    Gnum * const                vendtab,           //!< Local vertex end array
    const Gnum                  edgenbr,           //!< Number of local edges
    Gnum * restrict const       edgetab,           //!< Local edge array
    Gnum                        enttnbr,           //!< XXX
    Gnum *                      venttab,           //!< XXX
    const Gnum                  baseval,           //!< Base for indexing
    Gnum                        degnghb)
{
  Gnum * verttax;
  Gnum * vendtax;
  Gnum   edgesiz;
  Gnum   edgenbr2;
  Gnum * edgetax;
  Gnum   num;
  Gnum   nghbid;
  Gnum   nghbnum;
  Gnum   ndnghbid;
  Gnum   ndnghbnum;
  Gnum   iter;
  Gnum   test;
  Gnum   match;
  Gnum i, j;

  edgesiz=0;
  for (num = baseval ; num < (vertnbr + baseval) ; ++num) {
    if (venttab[num] == 0) { // XXX tétraèdre
      edgesiz += TETR_NBR_NGHB + 2 * TETR_NBR_NODE;
      if (degnghb == 1) {
        for (nghbid = verttab[num] ; nghbid < vendtab[num] ; ++nghbid) {
          nghbnum = edgetab[nghbid];
          if (venttab[nghbnum] == 2) { // XXX nœud
            edgesiz += vendtab[nghbnum] - verttab[nghbnum];
          }
        }
      }
    }
    if (venttab[num] == 1) { // XXX triangle
      edgesiz += 2 + 2 * TRIA_NBR_NODE;
    }
  }

  /***
   * BEGIN: Allocate all tables
   */
  if (memAllocGroup ((void **) (void *)
        &verttax, (size_t) ((vertnbr + 1) * sizeof (Gnum)),
        &vendtax, (size_t) (vertnbr       * sizeof (Gnum)),
        &edgetax, (size_t) (edgesiz       * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint("TETGEN_finalize: out of memory (1)");
    return (1);
  }
  memSet (verttax,  0, (vertnbr + 1) * sizeof (Gnum));
  memSet (vendtax,  0, vertnbr * sizeof (Gnum));
  memSet (edgetax, -1, edgesiz * sizeof (Gnum));
  edgetax -= baseval;
  verttax -= baseval;
  vendtax -= baseval;

  /***
   * BEGIN: add adjacence
   */
  edgenbr2 = 0;
  for (num = baseval ; num < (vertnbr + baseval) ; ++num) {
    vendtax[num] = verttax[num];
    if (venttab[num] == 0) { // XXX tétraèdre
      for (nghbid = verttab[num] ; nghbid < vendtab[num] ; ++nghbid) { // There is only nodes
        nghbnum = edgetab[nghbid];
        edgetax[vendtax[num]] = nghbnum;
        ++vendtax[num];
        ++edgenbr2;
        if (degnghb == 1) { // 3D elements neigboord by node
          for (ndnghbid = verttab[nghbnum] ; ndnghbid < vendtab[nghbnum] ; ++ndnghbid) {
            ndnghbnum = edgetab[ndnghbid];
            if (ndnghbnum == num) continue;
            test = 0;
            for (i = verttax[num] ; i < vendtax[num] ; ++i) {
              if (edgetax[i] == ndnghbnum) {
                ++test;
                break;
              }
            }
            if (test!=0) continue;
            test = 0;
            if (venttab[ndnghbnum] == 0) { // XXX tétraèdre
              for (iter = verttax[num] ; iter < vendtax[num] ; ++iter) {
                if (edgetax[iter] == ndnghbnum) {
                  ++test;
                  break;
                }
              }
              if (test == 0) {
                edgetax[vendtax[num]] = ndnghbnum;
                ++vendtax[num];
                ++edgenbr2;
              }
            }
            else { // There is only 3D and 2D elements in nodes' neighboord
                   // All 2Ds' neighboord nodes have to match
              match = 0;
              for (i = verttab[ndnghbnum] ; i < vendtab[ndnghbnum] ; ++i) {
                for (j = verttab[num] ; j < vendtab[num] ; ++j) {
                  if (edgetab[i] == edgetab[j]) {
                    ++match;
                    break;
                  }
                }
                if (match == TRIA_NBR_NODE) break;
              }
              if (match == TRIA_NBR_NODE) {
                edgetax[vendtax[num]] = ndnghbnum;
                ++vendtax[num];
                ++edgenbr2;
              }
            }
          }
        }
        else { // 3D elements neigboord by face
          for (ndnghbid = verttab[nghbnum] ; ndnghbid < vendtab [nghbnum] ; ++ndnghbid) {
            ndnghbnum = edgetab[ndnghbid];
            if (ndnghbnum == num) continue;
            test = 0;
            for (i = verttax[num] ; i < vendtax[num] ; ++i) {
              if (edgetax[i] == ndnghbnum) {
                ++test;
                break;
              }
            }
            if (test!=0) continue;
            if (venttab[ndnghbnum] == 0) { // XXX tétraèdre (3D) // TETR_FAC_NODE on TETR_NBR_NODE have to match
              match = 0;
              for (i = verttab[ndnghbnum] ; i < vendtab[ndnghbnum] ; ++i) {
                for (j = verttab[num] ; j < vendtab[num] ; ++j) {
                  if (edgetab[i] == edgetab[j]) {
                    ++match;
                    break;
                  }
                }
                if (match == TETR_FAC_NODE) break;
              }
              if (match == TETR_FAC_NODE) {
                edgetax[vendtax[num]] = ndnghbnum;
                ++vendtax[num];
                ++edgenbr2;
              }
            }
            else { // There is only 3D and 2D elements in nodes' neighboord
                   // All 2Ds' neighboord nodes have to match
              match = 0;
              for (i = verttab[ndnghbnum] ; i < vendtab[ndnghbnum] ; ++i) {
                for (j = verttab[num] ; j < vendtab[num] ; ++j) {
                  if (edgetab[i] == edgetab[j]) {
                    ++match;
                    break;
                  }
                }
                if (match == TRIA_NBR_NODE) break;
              }
              if (match == TRIA_NBR_NODE) {
                edgetax[vendtax[num]] = ndnghbnum;
                ++vendtax[num];
                ++edgenbr2;
              }
            }
          }
        }
      }
    }
    else if (venttab[num] == 1) { // XXX triangle
      for (nghbid = verttab[num] ; nghbid < vendtab[num] ; ++nghbid) { // There is only nodes
        nghbnum = edgetab[nghbid];
        edgetax[vendtax[num]] = nghbnum;
        ++vendtax[num];
        ++edgenbr2;
        for (ndnghbid = verttab[nghbnum] ; ndnghbid < vendtab [nghbnum] ; ++ndnghbid) {
          ndnghbnum = edgetab[ndnghbid];
          if (ndnghbnum == num) continue;
          test = 0;
          for (i = verttax[num] ; i < vendtax[num] ; ++i) {
            if (edgetax[i] == ndnghbnum) {
              ++test;
              break;
            }
          }
          if (test!=0) continue;
          if (venttab[ndnghbnum] == 0) { // XXX tétraèdre (3D) // TETR_FAC_NODE on TETR_NBR_NODE have to match
            match = 0;
            for (i = verttab[ndnghbnum] ; i < vendtab[ndnghbnum] ; ++i) {
              for (j = verttab[num] ; j < vendtab[num] ; ++j) {
                if (edgetab[i] == edgetab[j]) {
                  ++match;
                  break;
                }
              }
              if (match == TETR_FAC_NODE) break;
            }
            if (match == TETR_FAC_NODE) {
              edgetax[vendtax[num]] = ndnghbnum;
              ++vendtax[num];
              ++edgenbr2;
            }
          }
        }
      }
    }
    else if (venttab[num] == 2) { // XXX node
      for (nghbid = verttab[num] ; nghbid < vendtab[num] ; ++nghbid) {
        nghbnum = edgetab[nghbid];
        edgetax[vendtax[num]] = nghbnum;
        ++vendtax[num];
        ++edgenbr2;
      }
    }
    verttax[num+1] = vendtax[num];
  }
  /***
   * END: add adjacence
   */

#ifdef PAMPA_DEBUG_MESH
  for (num =  baseval ; num < vertnbr ; ++num) {
    for (nghbid = verttax[num] ; nghbid < vendtax[num] ; ++nghbid) {
      if (edgetax[nghbid] == num) {
        errorPrint ("Element %d linked to itself, nghbid : %d", num, nghbid);
      }
    }
  }
#endif /* PAMPA_DEBUG_MESH */
  CHECK_FDBG2 (PAMPA_meshBuild( (PAMPA_Mesh *) meshptr, baseval, vertnbr, verttax + baseval, vendtax + baseval, NULL, edgenbr2,
        edgesiz, edgetax + baseval, NULL, enttnbr, venttab, NULL, NULL, 50)); // XXX pourquoi 50 comme valeurs max ??

  memFreeGroup (verttax + baseval);

  return 0;
}
