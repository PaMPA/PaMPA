/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_ghst.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       These lines are the data declarations for
//!                the halo building routine.
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines.
*/

/* procsidtab-related values. */

#define DMESHGHSTSIDMAX            ((int) ((unsigned int) (1 << (sizeof (int) * 8 - 1)) - 2U)) //!< Maximum leap value for procsidtab entries

#define DMESHGHSTSIDEPSILON 5 //!< Epsilon value for reallocate procsidtab for each entity

typedef struct DmeshGhstEntt   //!< Structure to store some data on entities
{
  Gnum          enttlocnum; //!< Entity number
  Gnum          vertlocnum; //!< Last vertex which will be sent
  Gnum          esublocnbr; //!< Number of sub-entities
  Gnum *        esubloctab; //!< Array of sub-entity numbers
  Gnum *        vertnumtab; //!< Array of last send vertex for each sub-entity
  Gnum *        procsidtab; //!< Array of current index in procsidtab for each sub-entity
} DmeshGhstEntt;

int dmeshGhst3 (
    Dmesh * restrict const     meshptr,
    Gnum * restrict            vertnumtax);
