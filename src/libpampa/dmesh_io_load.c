/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_io_load.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contains the input/output
//!                routines for distributed meshes.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define DMESH_IO_LOAD

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dmesh_allreduce.h"
#include "dmesh_io_load.h"
#include "pampa.h"

/* This routine loads a distributed source
** mesh from the given stream(s). Either
** one processor holds a non-NULL stream
** of a centralized mesh, or all of them
** hold valid streams to either a centralized
** or a distributed mesh.
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

DMESHALLREDUCEMAXSUMOP (6, 3)
DMESHALLREDUCEMAXSUMOP (10, 2)

int
dmeshLoad (
Dmesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream,               /* One single centralized stream or distributed ones */
const Gnum                 baseval,              /* Base value (-1 means keep file base)              */
const Gnum                 flagval)              /* Mesh loading flags                               */
{
  Gnum                reduloctab[9];
  Gnum                reduglbtab[9];
  Gnum                parval;
  int                 versvl2;
  int                 parvl2;
  int                 gnumvl2;
  int                 cheklocval;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_DMESH2
  if (commBarrier (meshptr->proccomm) != MPI_SUCCESS) { /* Synchronize for debugging */
    errorPrint ("communication error");
    return     (1);
  }
#endif /* PAMPA_DEBUG_DMESH2 */

  reduloctab[0] = baseval;                        /* Exchange baseval to check it is the same for all */
  reduloctab[1] = - baseval;
  reduloctab[2] = flagval;                        /* Exchange flagval to check it is the same for all */
  reduloctab[3] = - flagval;
  reduloctab[4] = 0;                              /* Set uneffective values for versval */
  reduloctab[5] = -2;
  reduloctab[6] =                                 /* Assume everything will be fine */
  reduloctab[7] =                                 /* Assume does not have a stream  */
  reduloctab[8] = 0;
  if (stream != NULL) {
    cheklocval |= (fscanf (stream, "%d\t%d\t%d\n", /* version, parallel, Gnum size  */
        &versvl2, &parvl2, &gnumvl2) == EOF);
    CHECK_VERR (cheklocval, meshptr->proccomm);
    parval = (Gnum) parvl2;
   // XXX versval et parval ne sont pas utilisés, à faire... TODO 
    //if (intLoad (stream, &parval) != 1) {        /* Read version number */
    //  errorPrint ("bad input");
    //  parval       = 0;
    //  reduloctab[6] = 1;
    //}
    //else if ((parval != 0) && (parval != 2)) {  /* If not a mesh format */
    //  errorPrint ("dmeshLoad: not a mesh format");
    //  reduloctab[6] = 1;
    //}
    reduloctab[4] = parval;
    reduloctab[5] = - parval;
    reduloctab[7] = 1;                            /* One more process involved in loading */
    reduloctab[8] = meshptr->proclocnum;
  }

  if (dmeshAllreduceMaxSum (reduloctab, reduglbtab, 6, 3, meshptr->proccomm) != 0) {
    errorPrint ("communication error");
    return     (1);
  }

  if (reduglbtab[6] != 0)                         /* Return from previous errors */
    return (1);

  if ((reduglbtab[0] != - reduglbtab[1])) {
    errorPrint ("dmeshLoad: inconsistent base value");
    return     (1);
  }
  if ((reduglbtab[2] != - reduglbtab[3])) {
    errorPrint ("dmeshLoad: inconsistent flag value");
    return     (1);
  }
  if ((reduglbtab[7] != 0) &&
      (reduglbtab[4] != - reduglbtab[5])) {
    errorPrint ("dmeshLoad: inconsistent mesh file version value");
    return     (1);
  }

  if (reduglbtab[4] == 2) {                       /* If distributed mesh format             */
    if (reduglbtab[7] == meshptr->procglbnbr)     /* If as many input streams as processors  */
      return (dmeshLoadDist (meshptr, stream, baseval, flagval)); /* Read distributed mesh */
  }
  else {
    if (reduglbtab[4] == 3) {                       /* If distributed mesh format with all data */
      if (reduglbtab[7] == meshptr->procglbnbr)     /* If as many input streams as processors  */
        return (dmeshLoadDistAll (meshptr, stream, baseval, flagval)); /* Read distributed mesh */
    }
    else {                                          /* If centralized mesh format */
      if (reduglbtab[7] == 1)                       /* If only one reader stream   */
        return (dmeshLoadCent (meshptr, stream, baseval, flagval, reduglbtab[8])); /* Distribute centralized mesh from known root */
      else if (reduglbtab[7] == meshptr->procglbnbr)
        return (dmeshLoadMulti (meshptr, stream, baseval, flagval)); /* Read multi-centralized mesh */
    }
  }

  errorPrint ("dmeshLoad: invalid number of input streams");
  return     (1);
}

/* This routine loads a centralized source
** mesh from a single stream.
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
dmeshLoadCent (
Dmesh * restrict const     meshptr,              /* Distributed mesh to load            */
FILE * const                stream,               /* One single centralized stream        */
Gnum                        baseval,              /* Base value (-1 means keep file base) */
const DmeshFlag            flagval,              /* Mesh loading flags                  */
const int                   protnum)              /* Root process number                  */
{
  errorPrint ("not yet implemented");
  return     (1);
}

static inline int valueScan (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  int cheklocval;

  cheklocval = 0;

  if (!strcmp (format, "%d\t")) 
    cheklocval |= (fscanf (stream, format, ((int *) value)) == EOF);
  else if (!strcmp (format, "%lf\t")) // XXX bug voir dmesh_io_save
    cheklocval |= (fscanf (stream, format, ((double *) value)) == EOF);
  else if (!strcmp (format, "%lld\t")) 
    cheklocval |= (fscanf (stream, format, ((int64_t *) value)) == EOF);
  else {
    errorPrint ("Unrecognized format");
    return (1);
  }

  return (cheklocval);
}
 
/** XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
dmeshLoadValues (
Dmesh * restrict const     meshptr,              /* Distributed mesh to load            */
FILE * const                stream)               /* */
{
  Gnum enttglbnum;
  Gnum enttglbnnd;
  int cheklocval;

  cheklocval = 0;

  for (enttglbnum = PAMPA_ENTT_VIRT_PROC, enttglbnnd = meshptr->enttglbnbr + meshptr->baseval; enttglbnum < enttglbnnd; enttglbnum ++) {
    DmeshEntity * restrict enttlocptr;
    Gnum valulocnum;
    Gnum valulocnbr;
    Gnum vertlocnum;
    Gnum vertlocnnd;

    cheklocval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\n", &enttglbnum, &valulocnbr) == EOF);

    if (enttglbnum >= meshptr->baseval)
      enttlocptr = meshptr->enttloctax[enttglbnum];
    for (valulocnum = 0; valulocnum < valulocnbr; valulocnum ++) {
      int combdat;
      MPI_Aint            valuglbsiz;                 /* Extent of attribute datatype */
      MPI_Aint            dummy;
      Gnum typeval;
      Gnum countnbr;
      Gnum countnum;
      Gnum tagnum;
      Gnum flagval;
      size_t elemsiz;
      char format[9];
      byte * valuloctab;
      MPI_Datatype typedat;
      MPI_Datatype typedt2;
      int procglbnum;

      cheklocval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\t%d\t", &tagnum, &flagval, &combdat) == EOF);
      switch (combdat) {
        case 1: /* MPI_COMBINER_NAMED */
          countnbr = 1;
          cheklocval |= (fscanf (stream, GNUMSTRING "\n", &typeval) == EOF);

          if (typeval == 1) { /* MPI_INT */
            typedat = MPI_INT;
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (typeval == 2) { /* MPI_DOUBLE */
            typedat = MPI_DOUBLE;
            strcpy (format, "%lf\t");
            elemsiz = sizeof (double);
          }
          else if (typeval == 3) { /* MPI_LONG_LONG */
            typedat = MPI_LONG_LONG;
            strcpy (format, "%lld\t");
            elemsiz = sizeof (int64_t);
          }
          else {
            errorPrint ("Type not implemented");
            cheklocval = 1;
          }
          break;
        case 2: /* MPI_COMBINER_CONTIGUOUS */
          cheklocval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\n", &countnbr, &typeval) == EOF);

          if (typeval == 1) { /* MPI_INT */
            typedt2 = MPI_INT;
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (typeval == 2) { /* MPI_DOUBLE */
            typedt2 = MPI_DOUBLE;
            strcpy (format, "%lf\t");
            elemsiz = sizeof (double);
          }
          else if (typeval == 3) { /* MPI_LONG_LONG */
            typedt2 = MPI_LONG_LONG;
            strcpy (format, "%lld\t");
            elemsiz = sizeof (int64_t);
          }
          else {
            errorPrint ("Type not implemented");
            cheklocval = 1;
          }


          MPI_Type_contiguous(countnbr, typedt2, &typedat);
          MPI_Type_commit(&typedat);

          break;
        default:
          errorPrint ("case not implemented");
          cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

      CHECK_FERR (dmeshValueLink (meshptr, (void **) &valuloctab, flagval, NULL, NULL, typedat, enttglbnum, tagnum), meshptr->proccomm);
      MPI_Type_get_extent (typedat, &dummy, &valuglbsiz);     /* Get type extent */

      switch (enttglbnum) {
        case PAMPA_ENTT_VIRT_PROC:
          for (procglbnum = 0; procglbnum < meshptr->procglbnbr; procglbnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valueScan (stream, format, valuloctab + procglbnum * valuglbsiz + countnum * elemsiz);
          cheklocval |= (fscanf (stream, "\n") == EOF);
          break;
        case PAMPA_ENTT_VIRT_VERT:
          for (vertlocnum = 0; vertlocnum < meshptr->vertlocnbr; vertlocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valueScan (stream, format, valuloctab + vertlocnum * valuglbsiz + countnum * elemsiz);
          cheklocval |= (fscanf (stream, "\n") == EOF);
          break;
        default: /* real entity */

          for (vertlocnum = meshptr->baseval, vertlocnnd = enttlocptr->vertlocnbr + meshptr->baseval; vertlocnum < vertlocnnd; vertlocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valueScan (stream, format, valuloctab + vertlocnum * valuglbsiz + countnum * elemsiz);
          cheklocval |= (fscanf (stream, "\n") == EOF);
      }
    }
  }   
  CHECK_VDBG (cheklocval, meshptr->proccomm);
  return (0);
}

/* This routine loads a distributed source
** mesh from a distributed source mesh
** file spread across all of the streams.
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
dmeshLoadDist (
Dmesh * restrict const     meshptr,              /* Distributed mesh to load            */
FILE * const                stream,               /* */
Gnum                        baseval,              /* Base value (-1 means keep file base) */
const DmeshFlag            flagval)              /* Mesh loading flags                  */
{
  int                 proclocnum;
  int                 procglbnbr;
  Gnum                enttglbnbr;
  Gnum                enttglbnum;
  Gnum                enttglbnnd;
  Gnum                vertglbnbr;
  Gnum                edgeglbnbr;
  Gnum                edgelocsiz;
  Gnum                valuglbnbr;
  Gnum                valuglbmax;
  Gnum                ovlpglbval;
  Gnum                esublocsiz;
  Gnum                vlbllocsiz;
  Gnum                edlolocsiz;
  Gnum                vertlocnbr;
  Gnum                vertlocmax;
  Gnum                vertlocnnd;
  Gnum                vertlocnum;
  Gnum * restrict     vertloctax;
  Gnum * restrict     ventloctax;
  Gnum * restrict     esubloctax;
  Gnum                edgelocnbr;
  Gnum                edgelocnnd;
  Gnum                edgelocnum;
  Gnum * restrict     edgeloctax;
  Gnum * restrict     edloloctax;
  int                 cheklocval;
#ifdef PAMPA_DEBUG_REDUCE
  int                 chekglbval;
#endif /* PAMPA_DEBUG_REDUCE */

  cheklocval = 0;
#ifdef PAMPA_DEBUG_DMESH2
  if (stream == NULL) {
    errorPrint ("dmeshLoadDist: invalid parameter");
    return     (1);
  }
#endif /* PAMPA_DEBUG_DMESH2 */

  cheklocval |= (fscanf (stream,
      "%d\t%d\n"  /* proclocnum, procglbnbr */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n"  /* enttglbnbr, vertglbnbr, edgeglbnbr */
      GNUMSTRING "\t" GNUMSTRING "\n"  /* vertlocnbr, edgelocnbr */
      GNUMSTRING "\t" GNUMSTRING "\n"  /* valuglbnbr, valuglbmax */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n" GNUMSTRING "\n", /* baseval, ovlpglbval, esubloctab, vlbllocsiz, edlolocsiz */
      &proclocnum, &procglbnbr,
      &enttglbnbr, &vertglbnbr, &edgeglbnbr, /* enttglbnbr, vertglbnbr, edgeglbnbr */
      &vertlocnbr, &edgelocnbr,
      &valuglbnbr, &valuglbmax,
      &baseval, &ovlpglbval, &esublocsiz, &vlbllocsiz, &edlolocsiz) == EOF);

  if (vlbllocsiz == 1) { /* vlblloctax not yet implemented" */
    errorPrint ("vlblloctab not yet used, please wait for v1.1.0");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  edlolocsiz *= edgelocnbr;
  esublocsiz *= enttglbnbr;
  edgelocsiz = edgelocnbr;
  vertlocmax = vertlocnbr;

  if (memAllocGroup ((void **) (void *)
        &vertloctax, (size_t) ((vertlocnbr + 1) * sizeof (Gnum)),
        &ventloctax, (size_t) (vertlocnbr       * sizeof (Gnum)),
        &edgeloctax, (size_t) (edgelocsiz       * sizeof (Gnum)),
        &edloloctax, (size_t) (edgelocsiz       * sizeof (Gnum)),
        &esubloctax, (size_t) (esublocsiz       * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);
  vertloctax -= baseval;
  edgeloctax -= baseval;
  if (esublocsiz != 0)
    esubloctax -= baseval;
  else
    esubloctax = NULL;
  if (edlolocsiz != 0)
    edloloctax -= baseval;
  else
    edloloctax = NULL;

  if (esubloctax != NULL) {
    for (enttglbnum = baseval, enttglbnnd = baseval + enttglbnbr; enttglbnum < enttglbnnd; enttglbnum ++) {
      cheklocval |= (fscanf (stream, GNUMSTRING "\t", &esubloctax[enttglbnum + baseval]) == EOF);
    }
    cheklocval |= (fscanf (stream, "\n") == EOF);
  }

  vertloctax[baseval] = baseval;
  for (vertlocnum = baseval, vertlocnnd = baseval + vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
    cheklocval |= (fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\t", &ventloctax[vertlocnum], &vertloctax[vertlocnum + 1]) == EOF);
    vertloctax[vertlocnum + 1] += vertloctax[vertlocnum];

    for (edgelocnum = vertloctax[vertlocnum], edgelocnnd = vertloctax[vertlocnum + 1]; edgelocnum < edgelocnnd; edgelocnum ++) {
      if (edlolocsiz != 0)
        cheklocval |= (fscanf (stream, GNUMSTRING "\t", &edloloctax[edgelocnum]) == EOF);
      cheklocval |= (fscanf (stream, GNUMSTRING "\t", &edgeloctax[edgelocnum]) == EOF);
    }
  }
  cheklocval |= (fscanf (stream, "\n") == EOF);

 CHECK_FERR (dmeshBuild ( meshptr, baseval, vertlocnbr, vertlocmax, vertloctax, vertloctax + 1, edgelocnbr, edgelocsiz, edgeloctax, edloloctax, enttglbnbr, ventloctax, esubloctax, valuglbmax, ovlpglbval, NULL, -1, NULL, NULL, NULL, -1, -1, NULL), meshptr->proccomm);

 memFreeGroup (vertloctax + baseval);
  CHECK_VDBG(cheklocval, meshptr->proccomm);
  return (dmeshLoadValues (meshptr, stream));
}

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
dmeshLoadDistAll (
Dmesh * restrict const     meshptr,              /* Distributed mesh to load            */
FILE * const                stream,               /* */
Gnum                        baseval,              /* Base value (-1 means keep file base) */
const DmeshFlag            flagval)              /* Mesh loading flags                  */
{
  static Gnum     enttsingle = -1;
  Gnum ventlocnum;
  Gnum ventlocnnd;
  Gnum edgelocnum;
  Gnum edgelocnnd;
  Gnum enttglbnum;
  Gnum enttglbnnd;
  Gnum edlolocsiz;
  int  proclocnum;
  int  procglbnum;
  Dvalues * restrict valslocptr;
  Gnum valuglbmax;
  int tmp;
  int cheklocval;

  cheklocval = 0;
  valslocptr = meshptr->valslocptr;

  cheklocval |= (fscanf (stream,
      "%d\t" /* meshptr->flagval    */
      GNUMSTRING "\t" /* meshptr->baseval    */
      GNUMSTRING "\t" /* meshptr->ovlpglbval */
      GNUMSTRING "\t" /* meshptr->vertlocnbr */
      GNUMSTRING "\t" /* meshptr->vtrulocnbr */
      GNUMSTRING "\t" /* meshptr->vertlocmax */
      GNUMSTRING "\t" /* meshptr->vertglbnbr */
      GNUMSTRING "\t" /* meshptr->enttglbnbr */
      GNUMSTRING "\t" /* meshptr->esublocmsk */
      GNUMSTRING "\t" /* meshptr->degrlocmax */
      GNUMSTRING "\t" /* meshptr->edgelocnbr */
      GNUMSTRING "\t" /* meshptr->edgelocsiz */
      "%d\t"          /* meshptr->procglbnbr */
      "%d\t"          /* meshptr->proclocnum */
      "%d\t"          /* meshptr->procngbnbr */
      "%d\t"          /* meshptr->procngbmax */
      GNUMSTRING "\t", /* valuglbmax          */
      &meshptr->flagval,   
      &meshptr->baseval,   
      &meshptr->ovlpglbval,
      &meshptr->vertlocnbr,
      &meshptr->vtrulocnbr,
      &meshptr->vertlocmax,
      &meshptr->vertglbnbr,
      &meshptr->enttglbnbr,
      &meshptr->esublocmsk,
      &meshptr->degrlocmax,
      &meshptr->edgelocnbr,
      &meshptr->edgelocsiz,
      &meshptr->procglbnbr,
      &meshptr->proclocnum,
      &meshptr->procngbnbr,
      &meshptr->procngbmax,
      &valuglbmax) == EOF);
  if (baseval == ~0)
    baseval = meshptr->baseval;

  if (meshptr->esublocmsk != 0) {
    if ((meshptr->esublocbax = (Gnum *) memAlloc (meshptr->enttglbnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshptr->proccomm);

    for (enttglbnum = meshptr->baseval, enttglbnnd = meshptr->baseval + meshptr->enttglbnbr; enttglbnum < enttglbnnd; enttglbnum ++)
      cheklocval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->esublocbax[enttglbnum]) == EOF);
    cheklocval |= (fscanf (stream, "\n") == EOF);
  }
  else 
    meshptr->esublocbax = &enttsingle;

  cheklocval |= (fscanf (stream, "%d\n", &tmp) == EOF); // meshptr->edloloctax
  if (tmp == 1)
    edlolocsiz = meshptr->edgelocsiz;
  else
    edlolocsiz = 0;

  /* Allocate private entity arrays and adjacency list */
  if (memAllocGroup ((void **) (void *)
        &meshptr->enttloctax, (size_t) (meshptr->enttglbnbr * sizeof (DmeshEntity *)),
        &meshptr->edgeloctax, (size_t) (meshptr->edgelocsiz * sizeof (Gnum)),
        &meshptr->edloloctax, (size_t) (edlolocsiz          * sizeof (Gnum)),
        (void *) NULL) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  meshptr->enttloctax -= baseval;
  meshptr->edgeloctax -= baseval;
  meshptr->edloloctax = (edlolocsiz != 0) ? (meshptr->edloloctax - baseval) : (NULL);


  if (tmp == 1) { //if (meshptr->edloloctax != NULL) 
    Gnum edlolocnum;
    for (edlolocnum = baseval; edlolocnum < meshptr->edgelocsiz; edlolocnum ++)
      cheklocval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->edloloctax[edlolocnum]) == EOF);
    cheklocval |= (fscanf (stream, "\n") == EOF);
  }

  for (edgelocnum = baseval; edgelocnum < meshptr->edgelocsiz; edgelocnum ++)
    cheklocval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->edgeloctax[edgelocnum]) == EOF);
  cheklocval |= (fscanf (stream, "\n") == EOF);

  if (memAllocGroup ((void **) (void *)         /* Allocate distributed mesh private data */
        &meshptr->procdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (Gnum)),
        &meshptr->procvrttab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (Gnum)),
        &meshptr->procngbtab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
        &meshptr->proccnttab, (size_t) (meshptr->procglbnbr       * sizeof (Gnum)),
        &meshptr->procloccnt, (size_t) (                   sizeof (Gnum)), (void *) NULL) == NULL) {
    errorPrint ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, meshptr->proccomm);

  *meshptr->procloccnt = 1;

  for (proclocnum = 0; proclocnum <= meshptr->procglbnbr; proclocnum ++)
    cheklocval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->procvrttab[proclocnum]) == EOF);
  cheklocval |= (fscanf (stream, "\n") == EOF);

  for (proclocnum = 0; proclocnum < meshptr->procglbnbr; proclocnum ++)
    cheklocval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->proccnttab[proclocnum]) == EOF);
  cheklocval |= (fscanf (stream, "\n") == EOF);

  for (proclocnum = 0; proclocnum <= meshptr->procglbnbr; proclocnum ++)
    cheklocval |= (fscanf (stream, GNUMSTRING "\t", &meshptr->procdsptab[proclocnum]) == EOF);
  cheklocval |= (fscanf (stream, "\n") == EOF);

  for (proclocnum = 0; proclocnum < meshptr->procngbnbr; proclocnum ++)
    cheklocval |= (fscanf (stream, "%d\t", &meshptr->procngbtab[proclocnum]) == EOF);
  cheklocval |= (fscanf (stream, "\n") == EOF);

  for (ventlocnum = baseval, ventlocnnd = meshptr->enttglbnbr + baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
    DmeshEntity * restrict ventlocptr;
    Gnum vertlocnum;
    Gnum vertlocnnd;
    Gnum nentlocnum;
    Gnum nentlocnnd;
    Gnum vertlocsiz;
    Gnum vgstlocnbr;
    int procsidnum;

    cheklocval |= (fscanf (stream, "%d\n", &tmp) == EOF); 

    if (tmp == 1) { //if (meshptr->enttloctax[ventlocnum] != NULL) 
      if ((meshptr->enttloctax[ventlocnum] = (DmeshEntity *) memAlloc (sizeof (DmeshEntity))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

      ventlocptr = meshptr->enttloctax[ventlocnum];
      cheklocval |= (fscanf (stream, GNUMSTRING "\n", &vgstlocnbr) == EOF);
      switch (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk]) {

        case PAMPA_ENTT_STABLE : /* Entity with sub-entities and sorting is stable inside the entity */

          /* Allocate private entity neighbors data*/
          if (memAllocGroup ((void **) (void *)
                &ventlocptr->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
                &ventlocptr->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
                &ventlocptr->nghbloctax,    (size_t) (meshptr->enttglbnbr * sizeof (DmeshEnttNghb *)),
                &ventlocptr->mvrtloctax, (size_t) (vgstlocnbr * sizeof (Gnum)),
                &ventlocptr->ventloctax,    (size_t) (vgstlocnbr * sizeof (Gnum)),
                &ventlocptr->svrtloctax, (size_t) (vgstlocnbr * sizeof (Gnum)),
                (void *) NULL) == NULL) {
            errorPrint  ("out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          ventlocptr->nghbloctax -= baseval;
          ventlocptr->mvrtloctax -= baseval;
          ventlocptr->ventloctax -= baseval;
          ventlocptr->svrtloctax -= baseval;
          break;

        case PAMPA_ENTT_SINGLE : /* Entity without sub-entities */

          /* Allocate private entity neighbors data*/
          if (memAllocGroup ((void **) (void *)
                &ventlocptr->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
                &ventlocptr->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
                &ventlocptr->nghbloctax, (size_t) (meshptr->enttglbnbr * sizeof (DmeshEnttNghb *)),
                &ventlocptr->mvrtloctax, (size_t) (vgstlocnbr * sizeof (Gnum)),
                (void *) NULL) == NULL) {
            errorPrint  ("out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          ventlocptr->nghbloctax -= baseval;
          ventlocptr->mvrtloctax -= baseval;
          ventlocptr->ventloctax = NULL;
          ventlocptr->svrtloctax = NULL;
          break;

        default :

          /* Allocate private entity neighbors data*/
          if (memAllocGroup ((void **) (void *)
                &ventlocptr->commlocdat.procrcvtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
                &ventlocptr->commlocdat.procsndtab, (size_t) (meshptr->procglbnbr * sizeof (int)),
                &ventlocptr->ventloctax,    (size_t) (vgstlocnbr * sizeof (Gnum)),
                &ventlocptr->svrtloctax, (size_t) (vgstlocnbr * sizeof (Gnum)),
                (void *) NULL) == NULL) {
            errorPrint  ("out of memory");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, meshptr->proccomm);

          ventlocptr->mvrtloctax = NULL;
          ventlocptr->ventloctax -= baseval;
          ventlocptr->svrtloctax -= baseval;
      }
      ventlocptr->vgstlocnbr = vgstlocnbr;

      cheklocval |= (fscanf (stream,
          GNUMSTRING "\t" /* vfrnlocmin */
          GNUMSTRING "\t" /* vfrnlocmax */
          GNUMSTRING "\t" /* vertlocnbr */
          GNUMSTRING "\t" /* vintlocnbr */
          GNUMSTRING "\t" /* vovplocnbr */
          GNUMSTRING "\t", /* vertglbnbr */
          &ventlocptr->vfrnlocmin,
          &ventlocptr->vfrnlocmax,
          &ventlocptr->vertlocnbr,
          &ventlocptr->vintlocnbr,
          &ventlocptr->vovplocnbr,
          &ventlocptr->vertglbnbr) == EOF);
      cheklocval |= (fscanf (stream, "\n") == EOF);

      if (meshptr->ovlpglbval > 0) 
        vertlocsiz = ventlocptr->vgstlocnbr;
      else 
        vertlocsiz = ventlocptr->vertlocnbr;

      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { /* If ventlocnum is not a sub-entity */
        for (vertlocnum = baseval, vertlocnnd = ventlocptr->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fscanf (stream, GNUMSTRING "\t", &ventlocptr->mvrtloctax[vertlocnum]) == EOF);
        cheklocval |= (fscanf (stream, "\n") == EOF);
      }

      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] != PAMPA_ENTT_SINGLE) { /* If ventlocnum is not a single entity */
        for (vertlocnum = baseval, vertlocnnd = ventlocptr->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fscanf (stream, GNUMSTRING "\t", &ventlocptr->svrtloctax[vertlocnum]) == EOF);
        cheklocval |= (fscanf (stream, "\n") == EOF);
        for (vertlocnum = baseval, vertlocnnd = ventlocptr->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fscanf (stream, GNUMSTRING "\t", &ventlocptr->ventloctax[vertlocnum]) == EOF);
        cheklocval |= (fscanf (stream, "\n") == EOF);
      }

      cheklocval |= (fscanf (stream, "%d\n", &tmp) == EOF); 
      if (tmp == 1) { //if (meshptr->vprmloctax != NULL) 
        if ((ventlocptr->vprmloctax = (Gnum *) memAlloc (sizeof (Gnum) * vertlocsiz)) == NULL) {
          errorPrint  ("out of memory");
          return      (1);
        }
        ventlocptr->vprmloctax -= meshptr->baseval;

        for (vertlocnum = baseval; vertlocnum < vertlocsiz; vertlocnum ++)
          cheklocval |= (fscanf (stream, GNUMSTRING "\t", &ventlocptr->vprmloctax[vertlocnum]) == EOF);
        cheklocval |= (fscanf (stream, "\n") == EOF);
      }
      else
        ventlocptr->vprmloctax = NULL;


      cheklocval |= (fscanf (stream, "%d\n", &tmp) == EOF); 
      if (tmp == 1) // if (ventlocptr->vmatloctax != NULL)
        infoPrint ("Not yet implemented for dmeshMat");

    }
    else
      meshptr->enttloctax[ventlocnum] = NULL;
  }

  for (ventlocnum = baseval, ventlocnnd = meshptr->enttglbnbr + baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
    DmeshEntity * restrict ventlocptr;
    Gnum vertlocnum;
    Gnum vertlocnnd;
    Gnum nentlocnum;
    Gnum nentlocnnd;
    Gnum vertlocsiz;
    Gnum vgstlocnbr;
    int procsidnum;

    if (meshptr->enttloctax[ventlocnum] != NULL) {
      ventlocptr = meshptr->enttloctax[ventlocnum];
      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE) { /* If ventlocnum is a sub-entity */
        ventlocptr->mvrtloctax = meshptr->enttloctax[meshptr->esublocbax[ventlocnum & meshptr->esublocmsk]]->mvrtloctax;
        ventlocptr->nghbloctax = meshptr->enttloctax[meshptr->esublocbax[ventlocnum & meshptr->esublocmsk]]->nghbloctax;
      }
      else {

        if (meshptr->ovlpglbval > 0) 
          vertlocsiz = ventlocptr->vgstlocnbr;
        else 
          vertlocsiz = ventlocptr->vertlocnbr;

        for (nentlocnum = baseval, nentlocnnd = meshptr->enttglbnbr + baseval; nentlocnum < nentlocnnd; nentlocnum ++) {
          DmeshEnttNghb * restrict nghblocptr;

          if ((meshptr->enttloctax[nentlocnum] != NULL) || (nentlocnum == baseval) || (nentlocnum == nentlocnnd - 1)) {
            cheklocval |= (fscanf (stream, "%d\n", &tmp) == EOF); 

            if (tmp == 1) { //if (ventlocptr->nghbloctax[nentlocnum] != NULL)
              if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { /* If ventlocnum is not a sub-entity */
                if ((ventlocptr->nghbloctax[nentlocnum] = (DmeshEnttNghb *) memAlloc (sizeof (DmeshEnttNghb))) == NULL) {
                  errorPrint  ("out of memory");
                  cheklocval = 1;
                }
                CHECK_VERR (cheklocval, meshptr->proccomm);

                nghblocptr = ventlocptr->nghbloctax[nentlocnum];

                if (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { /* If nentlocnum is not a sub-entity */
                  /* Allocate distributed entity private data */
                  if (memAllocGroup ((void **) (void *)
                        &nghblocptr->vindloctax, (size_t) (vertlocsiz * sizeof (DmeshVertBounds)),
                        (void *) NULL) == NULL) {
                    errorPrint  ("out of memory");
                    cheklocval = 1;
                  }
                  CHECK_VERR (cheklocval, meshptr->proccomm);

                  nghblocptr->vindloctax -= baseval;

                  for (vertlocnum = baseval; vertlocnum < vertlocsiz; vertlocnum ++)
                    fscanf (stream, GNUMSTRING "\t" GNUMSTRING "\t", 
                        &nghblocptr->vindloctax[vertlocnum].vertlocidx,
                        &nghblocptr->vindloctax[vertlocnum].vendlocidx);
                  cheklocval |= (fscanf (stream, "\n") == EOF);
                }
              }
            }
          }
          else
            ventlocptr->nghbloctax[nentlocnum] = NULL;
        }
      }

      cheklocval |= (fscanf (stream,
          GNUMSTRING "\t"  /* procsndnbr */
          GNUMSTRING "\t"  /* procsidnbr */
          GNUMSTRING "\t", /* procngbnbr */
          &ventlocptr->commlocdat.procsndnbr,
          &ventlocptr->commlocdat.procsidnbr,
          &ventlocptr->commlocdat.procngbnbr) == EOF);
      cheklocval |= (fscanf (stream, "\n") == EOF);

      for (proclocnum = 0; proclocnum < meshptr->procglbnbr; proclocnum ++)
        cheklocval |= (fscanf (stream, "%d\t", &ventlocptr->commlocdat.procrcvtab[proclocnum]) == EOF);
      cheklocval |= (fscanf (stream, "\n") == EOF);

      for (proclocnum = 0; proclocnum < meshptr->procglbnbr; proclocnum ++)
        cheklocval |= (fscanf (stream, "%d\t", &ventlocptr->commlocdat.procsndtab[proclocnum]) == EOF);
      cheklocval |= (fscanf (stream, "\n") == EOF);

      if ((ventlocptr->commlocdat.procsidtab = (int *) memAlloc (ventlocptr->commlocdat.procsidnbr *sizeof (int))) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
      }
      for (procsidnum = 0; procsidnum < ventlocptr->commlocdat.procsidnbr; procsidnum ++)
        cheklocval |= (fscanf (stream, "%d\t", &ventlocptr->commlocdat.procsidtab[procsidnum]) == EOF);
      cheklocval |= (fscanf (stream, "\n") == EOF);

      if ((ventlocptr->commlocdat.procngbtab = (int *) memAlloc (ventlocptr->commlocdat.procngbnbr * sizeof (int))) == NULL) {
        errorPrint  ("out of memory");
        return      (1);
      }
      for (proclocnum = 0; proclocnum < ventlocptr->commlocdat.procngbnbr; proclocnum ++)
        cheklocval |= (fscanf (stream, "%d\t", &ventlocptr->commlocdat.procngbtab[proclocnum]) == EOF);
      cheklocval |= (fscanf (stream, "\n") == EOF);

    }
  }

  for (ventlocnum = baseval, ventlocnnd = meshptr->enttglbnbr + baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
    Gnum nentlocnum;
    Gnum nentlocnnd;

    if ((meshptr->enttloctax[ventlocnum] != NULL) && (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE)) /* If ventlocnum is not a sub-entity */
      for (nentlocnum = baseval, nentlocnnd = meshptr->enttglbnbr + baseval; nentlocnum < nentlocnnd; nentlocnum ++) 
        if ((meshptr->enttloctax[nentlocnum] != NULL) && (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] > PAMPA_ENTT_SINGLE)) /* If nentlocnum is a sub-entity */
          meshptr->enttloctax[ventlocnum]->nghbloctax[nentlocnum]->vindloctax =
            meshptr->enttloctax[ventlocnum]->nghbloctax[meshptr->esublocbax[nentlocnum & meshptr->esublocmsk]]->vindloctax;
  }

  CHECK_VDBG(cheklocval, meshptr->proccomm);
  CHECK_FERR (dvaluesInit(&meshptr->valslocptr, meshptr->baseval, valuglbmax, meshptr->enttglbnbr), meshptr->proccomm);
  return (dmeshLoadValues (meshptr, stream));
}
/* This routine loads a distributed source
** mesh from a centralized source mesh
** file replicated on all of the streams.
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

static
int
dmeshLoadMulti (
Dmesh * restrict const     meshptr,              /* Distributed mesh to load            */
FILE * const                stream,               /* One single centralized stream        */
Gnum                        baseval,              /* Base value (-1 means keep file base) */
const DmeshFlag            flagval)              /* Mesh loading flags                  */
{
  errorPrint ("dmeshLoadMulti: not implemented");
  return     (1);
}
