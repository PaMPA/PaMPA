/*  Copyright XXX-2017 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_gather_induce_multiple.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This file gather a distributed mesh and
//!                induce multiple centralized meshes.
//!
//!
/************************************************************/

/*
 ** The defines and includes.
 */


#define DMESH_GATHER_INDUCE_MULTIPLE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "values.h"
#include "dmesh.h"
#include "mesh.h"
#include <pampa.h>
#ifdef PAMPA_ADAPT
#include "dmesh_adapt.h"
#endif /* PAMPA_ADAPT */
#include "dmesh_gather_induce_multiple.h"

//! \brief This function is a specific use of dmeshGatherInduceMultiple2()
//! without dmeshGatherInfo pointer
//! \pre meshlocnbr has to be initialized to correct value or -1 if value is unknown
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dmeshGatherInduceMultiple (
    Dmesh *          const    dmshptr,              //!< Input mesh
    const Gnum                adptflg,              //!< Flag 1 if adaptation, 0 else
    const Gnum                partnbr,              //!< Number of parts (required)
    Gnum * const              vnumloctax,           //!< Part number for each element
    Gnum * const              partloctax,           //!< Processor number for each part
    Gnum * const              meshlocnbr,           //!< Number of meshes
    Mesh **                   meshloctab)           //!< Array of meshes
{
  return dmeshGatherInduceMultiple2 (dmshptr, adptflg, NULL, partnbr, vnumloctax, partloctax, meshlocnbr, meshloctab);
}

//! \brief This function gathers and induce meshes
//! \pre meshlocnbr has to be initialized to correct value or -1 if value is unknown
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

  int
dmeshGatherInduceMultiple2 (
    Dmesh *          const    dmshptr,              //!< Input mesh
    const Gnum                adptflg,              //!< Flag 1 if adaptation, 0 else
    dmeshGatherInfo * const   infoptr,              //!< Information about entities
    const Gnum                partnbr,              //!< Number of parts (required)
    Gnum * const              vnumloctax,           //!< Part number for each element
    Gnum * const              partloctax,           //!< Processor number for each part
    Gnum * const              meshlocnbr,           //!< Number of meshes
    Mesh **                   meshloctab)           //!< Array of meshes
{

  int cheklocval;
  Gnum enttlocnum;
  Gnum vertlocinc;
  Gnum edgelocinc;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum compnum;
  int procngbnum;
  int datasndnbr;
  int datarcvnbr;
  Gnum datarcvidx;
  Gnum medglocmax;
  Gnum mvrtlocmax;
  Gnum edlolocmax;
  Gnum datasndcur;
  Gnum datasndsiz;
  Gnum datarcvcur;
  Gnum datarcvsiz;
  Gnum datasndidx;
  Gnum meshlocnb2;
  Gnum passval;
  Gnum elemlocmax;
  int  procnum;
  int * restrict dsndcnttab;
  int * restrict dsnddsptab;
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  Gnum * restrict vertsidtab;
  Gnum * restrict vnumgsttax;
  Gnum * restrict vnodgsttax;
  Gnum * restrict erefgsttax;
  Gnum * restrict datasndtab;
  Gnum * restrict datarcvtab;
  Gnum * restrict meshrcvtab;
  Gnum * restrict mvrtloctab;
  Gnum * restrict medgloctab;
  Gnum * restrict meshcnttab;
  Gnum * restrict meshdsptab;
  Gnum * restrict sortrcvtab;
  Gnum * restrict * restrict sortvrttab;
  Gnum * restrict enbrloctax;
  Gnum * restrict scntvrttab;
  Gnum * restrict vertloctax;
  Gnum * restrict vendloctax;
  Gnum * restrict ventloctax;
  Gnum * restrict vflgtax;
  Gnum * restrict vflgtx2;
  Gnum * restrict edgeloctax;
  Gnum * restrict edloloctax;
  Gnum * restrict vflgloctax;
  VertProc * restrict vnbrloctab;
  DataSndEntt * restrict dsndloctab;
  DataRcvEntt * restrict drcvloctab;
  DmeshEntity * restrict * restrict enttloctax;
  Dvalues * valslocptr;
  PAMPA_Iterator it, it_nghb;
  Gnum idbas;
  Gnum nodeent;
#ifdef PAMPA_DEBUG_REDUCE
  int chekglbval;
#endif /* PAMPA_DEBUG_REDUCE */
#ifdef PAMPA_INFO_ADAPT
  Gnum vfrnlocnbr = 0;
  Gnum vzonlocnbr = 0;
#endif /* PAMPA_INFO_ADAPT */

  const Gnum baseval = dmshptr->baseval;
  const Gnum vertlocbas = dmshptr->procvrttab[dmshptr->proclocnum] - baseval;
  const Gnum enttglbmax = dmshptr->enttglbnbr + baseval - 1;
  const Gnum mvrtdsp = 0;
  const Gnum enttdsp = 1;
  const Gnum compdsp = 2;
  //! \todo the following constant is still used even if vertex load is now as
  //! an associated value. Before removing, see if there are consequences on
  //! drcvloctab
  const Gnum velodsp = 3; /* Displacement for vertex load */
  const Gnum nnbrdsp = 4; /* Number of neighbors */
  const Gnum nghbdsp = 0; /* TRICK: same value if neighbor is local or distant */
  const Gnum edlodsp = 1; /* TRICK: same value if neighbor is local or distant */

  cheklocval = 0;
  enttloctax = dmshptr->enttloctax;

  if (adptflg == 1) {
    /* Link value on each vertex to know its numbering on the global distributed
     * mesh */
    CHECK_FERR (dmeshValueLink (dmshptr, (void **) &vflgloctax, PAMPA_VALUE_PRIVATE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS), dmshptr->proccomm);
  }
  else { /* adptflg = 0 */
    if ((vflgloctax = (Gnum *) memAlloc (dmshptr->vertlocmax * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
  	CHECK_VERR (cheklocval, dmshptr->proccomm);
  }

  vflgloctax -= baseval;
#ifdef PAMPA_DEBUG_DMESH2
  CHECK_FERR ((!(PAMPA_TAG_VERT_EXTERNAL == ~0)), dmshptr->proccomm); /* Test if PAMPA_TAG_VERT_EXTERNAL is equal to ~0 */
  /* Two reasons for the necessity of -1 : 1) To initialize with memSet; 2) To
   * disinguish value from internal vertices (baseval -> vertnbr + baseval - 1)
   * and from frontier vertices (- vertnbr - baseval - 2 -> - baseval - 2) */
#endif /* PAMPA_DEBUG_DEMESH2 */
  memSet (vflgloctax + baseval, PAMPA_TAG_VERT_EXTERNAL, dmshptr->vertlocnbr * sizeof (Gnum));

  /* Determine for each vertex value of vflgloctax */
  for (enttlocnum = baseval; enttlocnum <= enttglbmax; enttlocnum ++) {
    DmeshEntity * restrict enttlocptr;
    Gnum * restrict refloctax;

    if ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (dmshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
      continue;

    enttlocptr = dmshptr->enttloctax[enttlocnum];

    cheklocval = dmeshValueData (dmshptr, enttlocnum, PAMPA_TAG_REF, NULL, NULL, (void **) &refloctax);
    if (cheklocval == 0) { /* If there is a value with this tag */
      refloctax -= baseval;
      for (vertlocnum = baseval, vertlocnnd = enttlocptr->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) 
        if (refloctax[vertlocnum] == PAMPA_TAG_VERT_FRONTIER) {
          Gnum mvrtlocnum;

          mvrtlocnum = enttlocptr->mvrtloctax[vertlocnum];
          vflgloctax[mvrtlocnum - vertlocbas] = mvrtlocnum; /* Is on the frontier */
        }
    }
    else if (cheklocval != 2) { /* If error code is different than "No value with this tag" */
      CHECK_VERR (cheklocval, dmshptr->proccomm);
    }
    else /* cheklocval different than 2 */
      cheklocval = 0;
  }

  if (memAllocGroup ((void **) (void *)
        &vnumgsttax, (size_t) (enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
        &dsndcnttab, (size_t) (dmshptr->procglbnbr       * sizeof (int)),
        &dsnddsptab, (size_t) ((dmshptr->procglbnbr + 1) * sizeof (int)),
        &drcvcnttab, (size_t) (dmshptr->procglbnbr       * sizeof (int)),
        &drcvdsptab, (size_t) ((dmshptr->procglbnbr + 1) * sizeof (int)),
        &vnbrloctab, (size_t) (partnbr                   * sizeof (VertProc)),
        &vertsidtab, (size_t) (partnbr                   * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  vnumgsttax -= baseval;
  memset (dsndcnttab, 0, dmshptr->procglbnbr * sizeof (int));
  memSet (vnbrloctab, 0, partnbr * sizeof (VertProc));
  memSet (vertsidtab, ~0, partnbr * sizeof (Gnum));
  memCpy (vnumgsttax + baseval, vnumloctax + baseval, enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

  elemlocmax = dmshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;

  CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), dmshptr->proccomm);

  cheklocval = dmeshValueData (dmshptr, baseval, PAMPA_TAG_REF, NULL, NULL, (void **) &erefgsttax);
  erefgsttax = (cheklocval == 0) ? erefgsttax - baseval : NULL;
  if (cheklocval == 2) /* If error is "No value with this tag" */
    cheklocval = 0;
  CHECK_VERR (cheklocval, dmshptr->proccomm);
  if (erefgsttax != NULL)
    CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[baseval], erefgsttax + baseval, GNUM_MPI), dmshptr->proccomm);


//  /* The following part is commented because it was just used to test.
//  *  It assumed that we know the entity number of nodes and elements.
//  *  If these information are saved, prefer this part as the one which is
//  *  between // begin tmp part & // end tmp part */
//  *  and change value of nodeent */
//  nodeent = dmshptr->enttglbnbr - (1 - baseval);
//  CHECK_FERR (dmeshValueLink (dmshptr, (void **) &vnodgsttax, PAMPA_VALUE_PUBLIC, NULL, NULL, GNUM_MPI, nodeent, PAMPA_TAG_REMESH), dmshptr->proccomm);
//  vnodgsttax -= baseval;
//
//  memSet (vnodgsttax + baseval, 0, enttloctax[nodeent]->vertlocnbr * sizeof (Gnum));
//
//  PAMPA_dmeshItInitStart((PAMPA_Dmesh *) dmshptr, nodeent, PAMPA_VERT_LOCAL, &it);
//  PAMPA_dmeshItInit((PAMPA_Dmesh *) dmshptr, nodeent, baseval, &it_nghb);
//  while (PAMPA_itHasMore(&it)) {
//    PAMPA_Num nodenum;
//    PAMPA_Num colrval;
//
//    nodenum = PAMPA_itCurEnttVertNum(&it);
//    PAMPA_itStart(&it_nghb, nodenum);
//
//    if (PAMPA_itHasMore(&it_nghb)) {
//      PAMPA_Num elemnum;
//
//      elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
//      colrval = vnumgsttax[elemnum];
//      PAMPA_itNext(&it_nghb);
//    }
//
//    while (PAMPA_itHasMore(&it_nghb)) {
//      PAMPA_Num elemnum;
//
//      elemnum = PAMPA_itCurEnttVertNum(&it_nghb);
//      if (colrval != vnumgsttax[elemnum]) {
//        vnodgsttax[nodenum] = 1;
//        break;
//      }
//      PAMPA_itNext(&it_nghb);
//    }
//    PAMPA_itNext(&it);
//  }
//  PAMPA_dmeshHaloValue ((PAMPA_Dmesh *) dmshptr, nodeent, PAMPA_TAG_REMESH);
//
//  PAMPA_dmeshItInitStart((PAMPA_Dmesh *) dmshptr, baseval, PAMPA_VERT_LOCAL, &it);
//  PAMPA_dmeshItInit((PAMPA_Dmesh *) dmshptr, baseval, nodeent, &it_nghb);
//  while (PAMPA_itHasMore(&it)) {
//    PAMPA_Num elemnum;
//
//    elemnum = PAMPA_itCurEnttVertNum(&it);
//    PAMPA_itStart(&it_nghb, elemnum);
//
//    while (PAMPA_itHasMore(&it_nghb)) {
//      PAMPA_Num nodenum;
//
//      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
//#ifdef PAMPA_NOT_REF_ONE
//      if ((vnodgsttax[nodenum] == 1) || (erefgsttax[elemnum] == PAMPA_REF_IS)) {
//#else  /* PAMPA_NOT_REF_ONE */
//      if (vnodgsttax[nodenum] == 1) {
//#endif /* PAMPA_NOT_REF_ONE */
//        vnumgsttax[elemnum] = - vnumgsttax[elemnum] - 2;
//        break;
//      }
//      PAMPA_itNext(&it_nghb);
//    }
//    PAMPA_itNext(&it);
//  }
//  CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), dmshptr->proccomm);
//  CHECK_FERR (dmeshValueUnlink (dmshptr, nodeent, PAMPA_TAG_REMESH), dmshptr->proccomm);

  // begin tmp part
  cheklocval = 0;
  PAMPA_dmeshItInitStart((PAMPA_Dmesh *) dmshptr, baseval, PAMPA_VERT_LOCAL, &it);
  PAMPA_dmeshItInit((PAMPA_Dmesh *) dmshptr, baseval, baseval, &it_nghb);
  while (PAMPA_itHasMore(&it)) {
    PAMPA_Num elemnum;

    elemnum = PAMPA_itCurEnttVertNum(&it);

#ifdef PAMPA_DEBUG_DMESH2
    if ((vnumloctax[elemnum] < ~0) || (vnumloctax[elemnum] > partnbr)) {
      errorPrint ("Invalid CC (%d) for vertex %d", vnumloctax[elemnum], elemnum);
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
    if (vnumloctax[elemnum] != ~0) {
      PAMPA_itStart(&it_nghb, elemnum);

      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num elemnm2;

        elemnm2 = PAMPA_itCurEnttVertNum(&it_nghb);
        if (elemnm2 <= elemlocmax) {
#ifdef PAMPA_NOT_REF_ONE
          if ((vnumloctax[elemnum] != vnumloctax[elemnm2]) || (erefgsttax[elemnum] == PAMPA_REF_IS)) {
#else  /* PAMPA_NOT_REF_ONE */
          if (vnumloctax[elemnum] != vnumloctax[elemnm2]) {
#endif /* PAMPA_NOT_REF_ONE */
            vnumgsttax[elemnum] = - vnumloctax[elemnum] - 2;
            break;
          }
        }
        else {
#ifdef PAMPA_NOT_REF_ONE
          if ((vnumloctax[elemnum] != vnumgsttax[elemnm2]) || (erefgsttax[elemnum] == PAMPA_REF_IS)) {
#else  /* PAMPA_NOT_REF_ONE */
          if (vnumloctax[elemnum] != vnumgsttax[elemnm2]) {
#endif /* PAMPA_NOT_REF_ONE */
            //if (vnumloctax[elemnum] != vnumgsttax[elemnm2]) {
            vnumgsttax[elemnum] = - vnumloctax[elemnum] - 2;
            break;
          }
        }
        PAMPA_itNext(&it_nghb);
      }
    }
    PAMPA_itNext(&it);
  }
  // end tmp part
  CHECK_VERR (cheklocval, dmshptr->proccomm);

#ifdef PAMPA_DEBUG_DMESH_SAVE
  {
    if (infoptr != NULL) {
      Mesh * imshloctab;
      Gnum * vnumgsttx2;
      Gnum * vnumgsttx3;
      Gnum * parttax;
      Gnum meshlocnbr;
      char s1[50];
      static int wmshcnt = 0;

      CHECK_FERR (dmeshValueLink (dmshptr, (void **) &vnumgsttx3, PAMPA_VALUE_PRIVATE, NULL, NULL, GNUM_MPI, baseval, 232), dmshptr->proccomm);
      vnumgsttx3 -= baseval;

      if (memAllocGroup ((void **) (void *)
            &vnumgsttx2, (size_t) (dmshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
            &parttax,    (size_t) (sizeof (Gnum)),
            NULL) == NULL) {
        errorPrint ("Out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, dmshptr->proccomm);
      vnumgsttx2 -= baseval;
      parttax -= baseval;

      memSet (vnumgsttx2 + baseval, ~0, dmshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum));
      for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
        if (vnumgsttax[vertlocnum] != ~0) {
          vnumgsttx2[vertlocnum] = 0;
          if (vnumgsttax[vertlocnum] < 0)
            vnumgsttx3[vertlocnum] = 1;
          else
            vnumgsttx3[vertlocnum] = 0;
        }
      parttax[baseval] = 0;

      imshloctab = NULL;
      meshlocnbr = -1;
      CHECK_FERR (dmeshGatherInduceMultiple (dmshptr, 0, 1, vnumgsttx2, parttax + baseval, &meshlocnbr, &imshloctab), dmshptr->proccomm);
      if (meshlocnbr > 0) {
        Gnum vertnbr;
        Gnum * reftax;

        PAMPA_meshEnttSize (((PAMPA_Mesh *) imshloctab), baseval, &vertnbr);
        meshValueData (imshloctab, baseval, 232, NULL, NULL, (void **) &reftax);
        reftax -= baseval;

        sprintf (s1, "dmesh-gather-%d.mesh", wmshcnt ++);
        CHECK_FERR (infoptr->EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, infoptr->dataptr, reftax + baseval, s1), dmshptr->proccomm);

        meshExit (imshloctab);
      }
      memFree (imshloctab);
      CHECK_FERR (dmeshValueUnlink (dmshptr, baseval, 232), dmshptr->proccomm);
      memFreeGroup (vnumgsttx2 + baseval);
    }
  }
#endif /* PAMPA_DEBUG_DMESH_SAVE */

  CHECK_FERR (dmeshHaloSync (dmshptr, dmshptr->enttloctax[baseval], vnumgsttax + baseval, GNUM_MPI), dmshptr->proccomm); 

  vertlocinc = 5;

  edgelocinc = (dmshptr->edloloctax != NULL) ? 2 : 1;

  /* Compute the data size to send for each processor, only on elements */
  for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
    if (vnumgsttax[vertlocnum] != ~0) {
      Gnum compnum;
      Gnum procnum;

      compnum = (vnumgsttax[vertlocnum] < 0) ? (- vnumgsttax[vertlocnum] - 2) : vnumgsttax[vertlocnum];
      procnum = partloctax[compnum];
      vnbrloctab[compnum].vertnbr ++;

      /* vertex data and number of neighbors */
      dsndcnttab[procnum] += vertlocinc + (enttloctax[baseval]->nghbloctax[enttglbmax]->vindloctax[vertlocnum].vendlocidx - enttloctax[baseval]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx) * edgelocinc;
    }
  /* Compute the data size to send, but for each vertex (which is not an
   * element) */
  for (enttlocnum = baseval + 1; enttlocnum <= enttglbmax; enttlocnum ++) {
    if ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (dmshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
      continue;

    for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[enttlocnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
      Gnum edgelocnum;
      Gnum edgelocnnd;

      Gnum mvrtlocnum;

      mvrtlocnum = enttloctax[enttlocnum]->mvrtloctax[vertlocnum];
      for (edgelocnum = enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx, edgelocnnd = enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx; edgelocnum < edgelocnnd; edgelocnum ++) {
        Gnum nghblocnum;

        nghblocnum = dmshptr->edgeloctax[edgelocnum];

        if (vnumgsttax[nghblocnum] != ~0) {
          Gnum procnum;

          compnum = (vnumgsttax[nghblocnum] < 0) ? (- vnumgsttax[nghblocnum] - 2) : vnumgsttax[nghblocnum];
          procnum = partloctax[compnum];

          /* If vertex not already sent for this part */
          if (vertsidtab[compnum] != mvrtlocnum) {
            vnbrloctab[compnum].vertnbr ++;
            vertsidtab[compnum] = mvrtlocnum;
            /* vertex data and number of neighbors */
            dsndcnttab[procnum] += vertlocinc + (enttloctax[enttlocnum]->nghbloctax[enttglbmax]->vindloctax[vertlocnum].vendlocidx - enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx) * edgelocinc;
          }
        }
      }
    }
  }

  /* Compute displacement sending array */
  dsnddsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr + 1; procngbnum ++) {
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  }

  datasndnbr = dsnddsptab[dmshptr->procglbnbr];

  //* send dsndcnttab and receive drcvcnttab
  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, dmshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }

  //* deduce ddsp{snd,rcv}tab
  /* Compute displacement receiving array */
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  drcvdsptab[dmshptr->procglbnbr] = drcvdsptab[dmshptr->procglbnbr - 1] + drcvcnttab[dmshptr->procglbnbr - 1];
  datarcvnbr = drcvdsptab[dmshptr->procglbnbr];
  dsndcnttab[0] = dsnddsptab[0];

  if (memAllocGroup ((void **) (void *)
        &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
        &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  memSet (datasndtab, 0, datasndnbr * sizeof (Gnum));

  /* Fill data to send, only on elements */
  for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
    if (vnumgsttax[vertlocnum] != ~0) {
      Gnum compnum;
      Gnum procnum;
      Gnum enttlocnum;
      Gnum mvrtlocnum;

#ifdef PAMPA_INFO_ADAPT
      vzonlocnbr ++;
#endif /* PAMPA_INFO_ADAPT */
      compnum = (vnumgsttax[vertlocnum] < 0) ? (- vnumgsttax[vertlocnum] - 2) : vnumgsttax[vertlocnum];
      procnum = partloctax[compnum];
      mvrtlocnum = enttloctax[baseval]->mvrtloctax[vertlocnum];
#ifdef PAMPA_INFO_ADAPT
      if (vnumgsttax[vertlocnum] < 0) { /* Negative: frontier */
        datasndtab[dsndcnttab[procnum] + mvrtdsp] =  
          vflgloctax[mvrtlocnum - vertlocbas] = mvrtlocnum;               /* is on the frontier */
        vfrnlocnbr ++;
      }
      else
        datasndtab[dsndcnttab[procnum] + mvrtdsp] =  
          vflgloctax[mvrtlocnum - vertlocbas] = (- mvrtlocnum - 2);       /* is internal        */
#else /* PAMPA_INFO_ADAPT */
      datasndtab[dsndcnttab[procnum] + mvrtdsp] = 
        vflgloctax[mvrtlocnum - vertlocbas] = (vnumgsttax[vertlocnum] < 0) /* Negative: frontier */
        ? mvrtlocnum               /* is on the frontier */
        : (- mvrtlocnum - 2);      /* is internal        */
#endif /* PAMPA_INFO_ADAPT */
      datasndtab[dsndcnttab[procnum] + enttdsp] = (enttloctax[baseval]->ventloctax != NULL) ? enttloctax[baseval]->ventloctax[vertlocnum] : baseval; // entity or sub-entity number 
      datasndtab[dsndcnttab[procnum] + compdsp] = compnum;
      datasndtab[dsndcnttab[procnum] + nnbrdsp] = enttloctax[baseval]->nghbloctax[enttglbmax]->vindloctax[vertlocnum].vendlocidx - enttloctax[baseval]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx;
      dsndcnttab[procnum] += vertlocinc;

      //! \todo Add test if one element is connected to another one with
      //! different color
      /* For each neighbor vertex connected to the element */
      for (enttlocnum = baseval; enttlocnum <= enttglbmax; enttlocnum ++) {
        Gnum edgelocnum;
        Gnum edgelocnnd;

        if ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (dmshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
          continue;

        for (edgelocnum = enttloctax[baseval]->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vertlocidx, edgelocnnd = enttloctax[baseval]->nghbloctax[enttlocnum]->vindloctax[vertlocnum].vendlocidx; edgelocnum < edgelocnnd; edgelocnum ++) {
          Gnum nghblocnum;

          nghblocnum = dmshptr->edgeloctax[edgelocnum];
          datasndtab[dsndcnttab[procnum] + nghbdsp] = enttloctax[enttlocnum]->mvrtloctax[nghblocnum];
          if (dmshptr->edloloctax != NULL)
            datasndtab[dsndcnttab[procnum] + edlodsp] = dmshptr->edloloctax[edgelocnum];
          dsndcnttab[procnum] += edgelocinc;
        }
      }
    }
#ifdef PAMPA_INFO_ADAPT
  infoPrint ("%d elements, %d elements to be remeshed of which %d elements on the frontier", enttloctax[baseval]->vertlocnbr, vzonlocnbr, vfrnlocnbr);
#endif /* PAMPA_INFO_ADAPT */

  /* Update vflgloctax */
  for (enttlocnum = baseval + 1; enttlocnum <= enttglbmax; enttlocnum ++) {
    Gnum edgelocnum;
    Gnum edgelocnnd;

    if ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (dmshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
      continue;

    for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[enttlocnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
	  Gnum mvrtlocnum;

	  mvrtlocnum = enttloctax[enttlocnum]->mvrtloctax[vertlocnum];

      for (edgelocnum = enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx, edgelocnnd = enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx; edgelocnum < edgelocnnd; edgelocnum ++) {
        Gnum nghblocnum;

        nghblocnum = dmshptr->edgeloctax[edgelocnum];

        if (vnumgsttax[nghblocnum] != ~0) /* element has to be sent */
          vflgloctax[mvrtlocnum - vertlocbas] = - mvrtlocnum - 2; /* Considered as internal */
        if (vnumgsttax[nghblocnum] < 0) {
          vflgloctax[mvrtlocnum - vertlocbas] = mvrtlocnum; /* Is on the frontier */
          break;
        }
      }
    }
  }

  memSet (vertsidtab, -1, partnbr * sizeof (Gnum));
  /* Send data for each vertex which is not an element */
  for (enttlocnum = baseval + 1; enttlocnum <= enttglbmax; enttlocnum ++) {
    Gnum edgelocnum;
    Gnum edgelocnnd;

    if ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (dmshptr->enttloctax[enttlocnum] == NULL)) /* If sub-entity or empty entity */
      continue;

    for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[enttlocnum]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
      Gnum mvrtlocnum;

      mvrtlocnum = enttloctax[enttlocnum]->mvrtloctax[vertlocnum];

      for (edgelocnum = enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx, edgelocnnd = enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vendlocidx; edgelocnum < edgelocnnd; edgelocnum ++) {
        Gnum nghblocnum;
        Gnum enttlocnm2;

        nghblocnum = dmshptr->edgeloctax[edgelocnum];

        if (vnumgsttax[nghblocnum] != ~0) { /* element has to be sent */
          Gnum procnum;

          compnum = (vnumgsttax[nghblocnum] < 0) ? (- vnumgsttax[nghblocnum] - 2) : vnumgsttax[nghblocnum];
          procnum = partloctax[compnum];

          if (vertsidtab[compnum] != mvrtlocnum) {
            vertsidtab[compnum] = mvrtlocnum;
            datasndtab[dsndcnttab[procnum] + mvrtdsp] = vflgloctax[mvrtlocnum - vertlocbas];
            datasndtab[dsndcnttab[procnum] + enttdsp] = (enttloctax[enttlocnum]->ventloctax != NULL) ? enttloctax[enttlocnum]->ventloctax[vertlocnum] : enttlocnum; // entity or sub-entity number 
            datasndtab[dsndcnttab[procnum] + compdsp] = compnum;
            datasndtab[dsndcnttab[procnum] + nnbrdsp] = enttloctax[enttlocnum]->nghbloctax[enttglbmax]->vindloctax[vertlocnum].vendlocidx - enttloctax[enttlocnum]->nghbloctax[baseval]->vindloctax[vertlocnum].vertlocidx;
            dsndcnttab[procnum] += vertlocinc;

            for (enttlocnm2 = baseval; enttlocnm2 <= enttglbmax; enttlocnm2 ++) {
              Gnum edgelocnm2;
              Gnum edgelocnd2;

              if ((dmshptr->esublocbax[enttlocnm2 & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (dmshptr->enttloctax[enttlocnm2] == NULL)) /* If sub-entity or empty entity */
                continue;

              for (edgelocnm2 = enttloctax[enttlocnum]->nghbloctax[enttlocnm2]->vindloctax[vertlocnum].vertlocidx, edgelocnd2 = enttloctax[enttlocnum]->nghbloctax[enttlocnm2]->vindloctax[vertlocnum].vendlocidx; edgelocnm2 < edgelocnd2; edgelocnm2 ++) {
                Gnum nghblocnm2;

                nghblocnm2 = dmshptr->edgeloctax[edgelocnm2];
                datasndtab[dsndcnttab[procnum] + nghbdsp] = enttloctax[enttlocnm2]->mvrtloctax[nghblocnm2];
                if (dmshptr->edloloctax != NULL)
                  datasndtab[dsndcnttab[procnum] + edlodsp] = dmshptr->edloloctax[edgelocnm2];
                dsndcnttab[procnum] += edgelocinc;
              }
            }
          }
        }
      }
    }
  }

  for (procngbnum = 0 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
  }

  //* send datasndtab and receive datarcvtab
  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, dmshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }

  if (memAllocGroup ((void **) (void *)
        &meshrcvtab, (size_t) (partnbr * sizeof (Gnum)),
        &mvrtloctab, (size_t) (partnbr * sizeof (Gnum)),
        &medgloctab, (size_t) (partnbr * sizeof (Gnum)),
        &meshcnttab, (size_t) (partnbr * sizeof (Gnum)), //!< also used to count the number of vertices on each CC
        &meshdsptab, (size_t) ((partnbr + 1) * sizeof (Gnum)), //!< also used to count the number of vertices on each CC with a reduce on proccomm
        &sortrcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  memSet (meshcnttab, 0, partnbr * sizeof (Gnum));
  memSet (meshdsptab, 0, partnbr * sizeof (Gnum));

  /* Search CC id */
  for (vertlocnum = baseval, vertlocnnd = dmshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
    if (vnumgsttax[vertlocnum] != ~0) {
      compnum = (vnumgsttax[vertlocnum] < 0) ? (- vnumgsttax[vertlocnum] - 2) : vnumgsttax[vertlocnum];
      meshcnttab[compnum] ++;
    }
  }
  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  if (commAllreduce (meshcnttab, meshdsptab, partnbr, GNUM_MPI, MPI_SUM, dmshptr->proccomm) != MPI_SUCCESS) {
  	errorPrint ("communication error");
  	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  memSet (meshrcvtab, 0, partnbr * sizeof (Gnum));
  memSet (mvrtloctab, 0, partnbr * sizeof (Gnum));
  memSet (medgloctab, 0, partnbr * sizeof (Gnum));

  for (meshlocnb2 = compnum = 0; compnum < partnbr; compnum ++) {
    if (vnbrloctab[compnum].vertnbr != 0) {
      vnbrloctab[compnum].procnum = partloctax[compnum];
    }
    if ((partloctax[compnum] == dmshptr->proclocnum) && (meshdsptab[compnum] > 0))
      meshrcvtab[meshlocnb2 ++] = compnum;
  }
  if (*meshlocnbr == -1)
    *meshlocnbr = meshlocnb2;
  else if ((*meshlocnbr != meshlocnb2) || ((meshlocnb2 > 0) && (meshloctab == NULL))) {
    errorPrint ("Invalid meshlocnbr or meshloctab has not been allocated\n");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  memSet (meshcnttab, 0, partnbr * sizeof (Gnum));
  memSet (meshdsptab, 0, partnbr * sizeof (Gnum));

  /* Compute number of vertices and edges in each CC (comp) and count items for each CC */
  for (datarcvidx = 0 ; datarcvidx < datarcvnbr; ) {
	Gnum nghbnbr;

    compnum = datarcvtab[datarcvidx + compdsp];
    mvrtloctab[compnum] ++;
    nghbnbr = datarcvtab[datarcvidx + nnbrdsp];
    medgloctab[compnum] += nghbnbr;
    meshcnttab[compnum] += nghbnbr * edgelocinc + vertlocinc;
    datarcvidx += nghbnbr * edgelocinc + vertlocinc;
  }

  /* With number of items by CC, determine the displacements */
  meshdsptab[0] = 0;
  for (compnum = 1; compnum < partnbr + 1; compnum ++)
    meshdsptab[compnum] = meshdsptab[compnum - 1] + meshcnttab[compnum - 1];

  /* copy datarcvtab on sortrcvtab according to the number of the CC */
  for (datarcvidx = 0; datarcvidx < datarcvnbr; ) {
    Gnum datarcvsiz;

    compnum = datarcvtab[datarcvidx + compdsp];

    datarcvsiz = vertlocinc + datarcvtab[datarcvidx + nnbrdsp] * edgelocinc;
    memCpy (sortrcvtab + meshdsptab[compnum], datarcvtab + datarcvidx, datarcvsiz * sizeof (Gnum));
    meshdsptab[compnum] += datarcvsiz;
    datarcvidx += datarcvsiz;
  }

  for (compnum = 0; compnum < partnbr; compnum ++)
    meshdsptab[compnum] -= meshcnttab[compnum];

  if (memAllocGroup ((void **) (void *)
        &sortvrttab, (size_t) (meshlocnb2 * sizeof (Gnum *)),
        &scntvrttab, (size_t) (meshlocnb2 * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  memSet (scntvrttab, 0, meshlocnb2 * sizeof (Gnum));

  for (medglocmax = mvrtlocmax = compnum = 0; compnum < meshlocnb2; compnum ++) {
    if ((sortvrttab[compnum] = (Gnum *) memAlloc (mvrtloctab[meshrcvtab[compnum]] * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

    if (medglocmax < medgloctab[meshrcvtab[compnum]])
      medglocmax = medgloctab[meshrcvtab[compnum]];
    if (mvrtlocmax < mvrtloctab[meshrcvtab[compnum]])
      mvrtlocmax = mvrtloctab[meshrcvtab[compnum]];
  }

  edlolocmax = (dmshptr->edloloctax != NULL) ? medglocmax : 0;
  if (memAllocGroup ((void **) (void *)
        &vertloctax, (size_t) (mvrtlocmax * sizeof (Gnum)),
        &vendloctax, (size_t) (mvrtlocmax * sizeof (Gnum)),
        &ventloctax, (size_t) (mvrtlocmax * sizeof (Gnum)),
        &vflgtax, (size_t) (mvrtlocmax * sizeof (Gnum)),
        &edgeloctax, (size_t) (medglocmax * sizeof (Gnum)),
        &edloloctax, (size_t) (edlolocmax * sizeof (Gnum)),
        &enbrloctax, (size_t) (dmshptr->enttglbnbr * sizeof (Gnum)), // XXX changer de nom
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  vertloctax -= baseval;
  vendloctax -= baseval;
  ventloctax -= baseval;
  vflgtax -= baseval;
  edgeloctax -= baseval;
  edloloctax = (dmshptr->edloloctax == NULL) ? NULL : edloloctax - baseval;
  enbrloctax -= baseval;

  if (dmshptr->edloloctax == NULL)
    edloloctax = NULL;

  /* For each CC, compute mesh (global) vertex numbering */
  for (compnum = 0; compnum < meshlocnb2; compnum ++) {
    Gnum sortrcvidx;
    Gnum sortrcvnnd;

    for (sortrcvidx = meshdsptab[meshrcvtab[compnum]], sortrcvnnd = meshdsptab[meshrcvtab[compnum] + 1]; sortrcvidx < sortrcvnnd; sortrcvidx += vertlocinc + sortrcvtab[sortrcvidx + nnbrdsp] * edgelocinc) {
      if (sortrcvtab[sortrcvidx + mvrtdsp] < 0)
        sortvrttab[compnum][scntvrttab[compnum] ++]  = - sortrcvtab[sortrcvidx + mvrtdsp] - 2; /* internal */
      else 
        sortvrttab[compnum][scntvrttab[compnum] ++]  = sortrcvtab[sortrcvidx + mvrtdsp];       /* frontier */
    }
  }

  /* Sort vertex numbering array */
  for (compnum = 0; compnum < meshlocnb2; compnum ++)
    intSort1asc1(sortvrttab[compnum], scntvrttab[compnum]);

  if ((meshloctab != NULL) && (*meshloctab == NULL)) { /* We suppose that if meshloctab != NULL that meshloctab has been allocated correctly */
    if ((*meshloctab = (Mesh *) memAlloc (meshlocnb2 * sizeof (Mesh))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    for (compnum = 0; compnum < meshlocnb2; compnum ++) 
      CHECK_FERR (meshInit (*meshloctab + compnum), dmshptr->proccomm);
  }

  /* Compute id for Mesh */
  do {
    Gnum procnbr;

    for (idbas = 1, procnbr = dmshptr->procglbnbr; procnbr > 0; procnbr /= 10, idbas *= 10);
  } while (0);


  /* Fill out the arrays given to meshBuild() */
  for (compnum = 0; compnum < meshlocnb2; compnum ++) {
    Mesh * mesh;
    Gnum sortrcvidx;
    Gnum sortrcvnnd;
    Gnum edgelocidx;
    Gnum vertlocnbr;


    mesh = *meshloctab + compnum;
    for (vertlocnbr = 0, edgelocidx = baseval, sortrcvidx = meshdsptab[meshrcvtab[compnum]], sortrcvnnd = meshdsptab[meshrcvtab[compnum] + 1]; sortrcvidx < sortrcvnnd; ) {
      Gnum mvrtlocnum;
      Gnum vertlocmax;
      Gnum nghblocnum;
      Gnum nghblocnm2;
      Gnum fronlocflg; /* Flag to know if vertex in on the frontier */

      if (sortrcvtab[sortrcvidx + mvrtdsp] < 0) {
        fronlocflg = 0;
        mvrtlocnum = - sortrcvtab[sortrcvidx + mvrtdsp] - 2;
      }
      else {
        fronlocflg = 1;
        mvrtlocnum = sortrcvtab[sortrcvidx + mvrtdsp];
      }

      /* Search the index of vertex which have current number */
      for (vertlocnum = baseval, vertlocmax = scntvrttab[compnum]; //!< \todo use a local variable instead of scntvrttab[compnum]
          vertlocmax - vertlocnum > 1; ) {
        Gnum                 vertlocmed;

        vertlocmed = (vertlocmax + vertlocnum) / 2;
        if (sortvrttab[compnum][vertlocmed] <= mvrtlocnum)
          vertlocnum = vertlocmed;
        else
          vertlocmax = vertlocmed;
      }

      if (sortvrttab[compnum][vertlocnum] == mvrtlocnum) {
        vertlocnbr ++;
        vertloctax[vertlocnum] = edgelocidx;
        ventloctax[vertlocnum] = sortrcvtab[sortrcvidx + enttdsp];
        vflgtax[vertlocnum] = (fronlocflg == 1) ? mvrtlocnum : PAMPA_TAG_VERT_INTERNAL;

        for (nghblocnm2 = sortrcvtab[sortrcvidx + nnbrdsp], sortrcvidx += vertlocinc; nghblocnm2 > 0; nghblocnm2 --, sortrcvidx += edgelocinc) {
          Gnum mngblocnum;
          Gnum nghblocmax;

          mngblocnum = sortrcvtab[sortrcvidx + nghbdsp];

          /* Search the index of neighbor which have current number */
          for (nghblocnum = 0, nghblocmax = scntvrttab[compnum];
              nghblocmax - nghblocnum > 1; ) {
            int                 nghblocmed;

            nghblocmed = (nghblocmax + nghblocnum) / 2;
            if (sortvrttab[compnum][nghblocmed] <= mngblocnum)
              nghblocnum = nghblocmed;
            else
              nghblocmax = nghblocmed;
          }

          if (sortvrttab[compnum][nghblocnum] == mngblocnum) {
            edgeloctax[edgelocidx] = nghblocnum;
            if (dmshptr->edloloctax != NULL)
              edloloctax[edgelocidx] = sortrcvtab[sortrcvidx + edlodsp];
            edgelocidx ++;
          }
        }

        vendloctax[vertlocnum] = edgelocidx;
      }
      else {
        sortrcvidx += vertlocinc + sortrcvtab[sortrcvidx + nnbrdsp] * edgelocinc;
      }
    }
    CHECK_FERR (
        meshBuild(mesh, (idbas * idcpt ++) + dmshptr->proclocnum, baseval, vertlocnbr, vertloctax, vendloctax,
          edgelocidx, edgelocidx, edgeloctax, edloloctax, dmshptr->enttglbnbr,
          ventloctax, dmshptr->esublocmsk == 0 ? NULL : dmshptr->esublocbax,
          dmshptr->valslocptr->valuglbmax, NULL, -1, dmshptr->procglbnbr),
        dmshptr->proccomm);


    //! \todo The link could now be done before or change the flag to give
    //! directly vflgtax. When these lines were written, allocation was done
    //! only by PaMPA, now the user could do it.
    CHECK_FERR (meshValueLink (mesh, (void **) &vflgtx2, PAMPA_VALUE_NONE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS), dmshptr->proccomm);
    vflgtx2 -= baseval;
    memCpy (vflgtx2 + baseval, vflgtax + baseval, vertlocnbr * sizeof (Gnum));
  }

  dsndloctab = (DataSndEntt *) datasndtab;
  /* we suppose that there is at leat one neighbor to have enough space to fill in drcvloctab*/
  drcvloctab = (DataRcvEntt *) datarcvtab;



  /* Determine the dsndloctab by wandering datasndtab */
  memset (dsndcnttab, 0, dmshptr->procglbnbr * sizeof (int));
  for (datasndcur = procnum = 0; procnum < dmshptr->procglbnbr; procnum ++) 
    for (datasndidx = dsnddsptab[procnum]; datasndidx < dsnddsptab[procnum + 1]; ) {
      Gnum enttnum;
      Gnum mvrtlocnum;
      Gnum vertlocnum;
      Gnum vertlocmax;

      enttnum = datasndtab[datasndidx + enttdsp];
      if (datasndtab[datasndidx + mvrtdsp] < 0) 
        mvrtlocnum = - datasndtab[datasndidx + mvrtdsp] - 2;
      else 
        mvrtlocnum = datasndtab[datasndidx + mvrtdsp];
      datasndidx += vertlocinc + datasndtab[datasndidx + nnbrdsp] * edgelocinc;

      /* Search the index of vertex which have current number */
      for (vertlocnum = 0, vertlocmax = enttloctax[enttnum]->vertlocnbr;
          vertlocmax - vertlocnum > 1; ) {
        Gnum                 vertlocmed;

        vertlocmed = (vertlocmax + vertlocnum) / 2;
        if (enttloctax[enttnum]->mvrtloctax[vertlocmed] <= mvrtlocnum)
          vertlocnum = vertlocmed;
        else
          vertlocmax = vertlocmed;
      }

      dsndloctab[datasndcur].enttnum = enttnum;
      dsndloctab[datasndcur].vertnum = vertlocnum;
      dsndloctab[datasndcur ++].procnum = (Gnum) procnum;
    }

  datasndsiz = datasndcur;

  memset (enbrloctax + baseval, 0, dmshptr->enttglbnbr * sizeof (Gnum));
  memset (drcvcnttab, 0, dmshptr->procglbnbr * sizeof (int));
  datarcvsiz = 0;

  /* Determine the drcvloctab by wandering datarcvtab */
  for (datarcvcur = procnum = 0; procnum < dmshptr->procglbnbr; procnum ++) {
    Gnum compidx;
    for (compnum = -1, datarcvidx = drcvdsptab[procnum]; datarcvidx < drcvdsptab[procnum + 1]; ) {
      Gnum enttnum;
      Gnum esubnum;
      Gnum vertnum;

      enttnum = datarcvtab[datarcvidx + enttdsp];
      esubnum = dmshptr->esublocbax[enttnum & dmshptr->esublocmsk];
      if (compnum != datarcvtab[datarcvidx + compdsp]) {
        Gnum compmax;

        compnum = datarcvtab[datarcvidx + compdsp];
        /* Search the index of CC which have current number */
        for (compidx = 0, compmax = meshlocnb2;
            compmax - compidx > 1; ) {
          Gnum                 compmed;

          compmed = (compmax + compidx) / 2;
          if (meshrcvtab[compmed] <= compnum)
            compidx = compmed;
          else
            compmax = compmed;
        }
#ifdef PAMPA_DEBUG_DMESH2
        if (meshrcvtab[compidx] != compnum) {
          errorPrint ("CC number %d not found", compnum);
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
      }

      if (datarcvtab[datarcvidx + mvrtdsp] < 0) 
        vertnum = - datarcvtab[datarcvidx + mvrtdsp] - 2;
      else 
        vertnum = datarcvtab[datarcvidx + mvrtdsp];
      datarcvidx += vertlocinc + datarcvtab[datarcvidx + nnbrdsp] * edgelocinc;

      drcvloctab[datarcvcur].enttnum = enttnum;
      drcvloctab[datarcvcur].compidx = compidx;
      drcvloctab[datarcvcur].vertnum = vertnum;
      drcvloctab[datarcvcur].procnum = (Gnum) procnum;
      if (esubnum > PAMPA_ENTT_SINGLE) /* If sub-entity */
        drcvloctab[datarcvcur].indxnm2 = enbrloctax[esubnum] ++;
      drcvloctab[datarcvcur ++].indxnum = enbrloctax[enttnum] ++;

    }
  }
  datarcvsiz = datarcvcur;

  /* Sort dsndloctab and drcvloctab to avoid communications */
  for (passval = 0; passval < 2; passval ++) { /* 0 for sub-entities and 1 for entities */
    intSort3asc3(dsndloctab, datasndsiz);
    intSort6asc3(drcvloctab, datarcvsiz);

    for (valslocptr = dmshptr->valslocptr, datasndcur = datarcvcur = 0, enttlocnum = baseval; enttlocnum <= enttglbmax; enttlocnum ++)
    {
      Dvalue * valulocptr;


      if ((enttloctax[enttlocnum] == NULL) || (valslocptr->evalloctak[enttlocnum] == NULL) /* If empty entity or no value */
          || ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] >= PAMPA_ENTT_SINGLE) && (passval == 1)) /* If sub-entity or single entity */
          || ((dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] < PAMPA_ENTT_SINGLE) && (passval == 0))) /* If super-entity */
        continue;


      for (; datasndcur < datasndsiz && dsndloctab[datasndcur].enttnum < enttlocnum; datasndcur ++);
      memset (dsndcnttab, 0, dmshptr->procglbnbr * sizeof (int));
      for (datasndidx = datasndcur; datasndidx < datasndsiz && dsndloctab[datasndidx].enttnum == enttlocnum; datasndidx ++)
        dsndcnttab[(int) dsndloctab[datasndidx].procnum] ++;

      dsnddsptab[0] = 0;
      for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr + 1; procngbnum ++) {
        dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
      }

      datasndnbr = dsnddsptab[dmshptr->procglbnbr];

      for (; datarcvcur < datarcvsiz && drcvloctab[datarcvcur].enttnum < enttlocnum; datarcvcur ++);
      memset (drcvcnttab, 0, dmshptr->procglbnbr * sizeof (int));
      for (datarcvidx = datarcvcur; datarcvidx < datarcvsiz && drcvloctab[datarcvidx].enttnum == enttlocnum; datarcvidx ++)
        drcvcnttab[(int) drcvloctab[datarcvidx].procnum] ++;

      drcvdsptab[0] = 0;
      for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr + 1; procngbnum ++) {
        drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
      }

      datarcvnbr = drcvdsptab[dmshptr->procglbnbr];

      /* For each value in this entity */
      for (valulocptr = valslocptr->evalloctak[enttlocnum]; valulocptr != NULL; valulocptr = valulocptr->next) {
        MPI_Aint            valuglbsiz;                 /* Extent of attribute datatype */
        MPI_Aint            dummy;
        byte *              valusndtab;
        byte *              valurcvtab;
        byte *              valutax;

        MPI_Type_get_extent (valulocptr->typeval, &dummy, &valuglbsiz);     /* Get type extent */

        if (memAllocGroup ((void **) (void *)
              &valusndtab, (size_t) (datasndnbr * valuglbsiz),
              &valurcvtab, (size_t) (datarcvnbr * valuglbsiz),
              NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, dmshptr->proccomm);

        for (procngbnum = 0 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
          dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
        }
        for (datasndidx = datasndcur; datasndidx < datasndsiz && dsndloctab[datasndidx].enttnum == enttlocnum; datasndidx ++) {
          Gnum procnum;
          Gnum vertlocnum;

          procnum = dsndloctab[datasndidx].procnum;
          vertlocnum = dsndloctab[datasndidx].vertnum;
          memCpy ((byte *) valusndtab + dsndcnttab[procnum] * valuglbsiz, (byte *) valulocptr->valuloctab + vertlocnum * valuglbsiz, valuglbsiz * sizeof (byte));
          dsndcnttab[procnum] ++;
        }
        for (procngbnum = 0 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
          dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
        }

        CHECK_VDBG (cheklocval, dmshptr->proccomm);
        /* Exchange value */
        if (commAlltoallv(valusndtab, dsndcnttab, dsnddsptab, valulocptr->typeval, valurcvtab, drcvcnttab, drcvdsptab, valulocptr->typeval, dmshptr->proccomm) != MPI_SUCCESS) {
          errorPrint ("communication error");
          return (1);
        }

        for (datarcvidx = datarcvcur, compnum = 0; compnum < meshlocnb2; compnum ++) {
          Gnum evrtlocnum;

          CHECK_FERR (meshValueLink(*meshloctab + compnum, (void **) &valutax, PAMPA_VALUE_NONE, NULL, NULL, valulocptr->typeval, enttlocnum, valulocptr->tagnum), dmshptr->proccomm);
          valutax -= baseval * valuglbsiz;

          /* Update value on the CC */
          for (evrtlocnum = baseval; datarcvidx < datarcvsiz && drcvloctab[datarcvidx].compidx == compnum && drcvloctab[datarcvidx].enttnum == enttlocnum; evrtlocnum ++, datarcvidx ++) {
            memCpy((byte *) valutax + evrtlocnum * valuglbsiz, (byte *) valurcvtab + drcvloctab[datarcvidx].indxnum * valuglbsiz, valuglbsiz * sizeof (byte));
          }
        }
        memFreeGroup (valusndtab);
      }
    }

    /* Update dsndloctab for sub-entities */
    for (enttlocnum = -1, datasndidx = 0; datasndidx < datasndsiz; datasndidx ++) {
      DmeshEntity * restrict enttlocptr;
      Gnum enttlocnm2;

      if (dsndloctab[datasndidx].enttnum != enttlocnum) {
        enttlocnum = dsndloctab[datasndidx].enttnum;
        enttlocptr = dmshptr->enttloctax[enttlocnum];
        if (dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
          enttlocnm2 = dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk];
          dsndloctab[datasndidx].enttnum = enttlocnm2;
          dsndloctab[datasndidx].vertnum = enttlocptr->svrtloctax[dsndloctab[datasndidx].vertnum];
        }
        else {
          for (; datasndidx < datasndsiz && enttlocnum == dsndloctab[datasndidx].enttnum; datasndidx ++);
          datasndidx --;
        }
      }
      else {
        dsndloctab[datasndidx].enttnum = enttlocnm2;
        dsndloctab[datasndidx].vertnum = enttlocptr->svrtloctax[dsndloctab[datasndidx].vertnum];
      }
    }

    /* Update drcvloctab for sub-entities */
    for (enttlocnum = -1, datarcvidx = 0; datarcvidx < datarcvsiz; datarcvidx ++) {
      Gnum enttlocnm2;

      if (drcvloctab[datarcvidx].enttnum != enttlocnum) {
        enttlocnum = drcvloctab[datarcvidx].enttnum;
        if (dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
          enttlocnm2 = dmshptr->esublocbax[enttlocnum & dmshptr->esublocmsk];
          drcvloctab[datarcvidx].enttnum = enttlocnm2;
          drcvloctab[datarcvidx].indxnum = drcvloctab[datarcvidx].indxnm2;
        }
        else {
          for (; datarcvidx < datarcvsiz && enttlocnum == drcvloctab[datarcvidx].enttnum; datarcvidx ++);
          datarcvidx --;
        }
      }
      else {
        drcvloctab[datarcvidx].enttnum = enttlocnm2;
        drcvloctab[datarcvidx].indxnum = drcvloctab[datarcvidx].indxnm2;
      }
    }

  }

  /* compute number of vertices in each CC. It will be used to attach value
   * PAMPA_TAG_PART on PAMPA_VIRT_PROC entity */
  memset (dsndcnttab, 0, dmshptr->procglbnbr * sizeof (int));
  for (compnum = 0; compnum < partnbr; compnum ++)
    if (vnbrloctab[compnum].vertnbr > 0)
      dsndcnttab[vnbrloctab[compnum].procnum] += 2;

  /* Compute displacement sending array */
  dsnddsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr + 1; procngbnum ++) {
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];
  }

  datasndnbr = dsnddsptab[dmshptr->procglbnbr];

  //* send dsndcnttab and receive drcvcnttab
  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, dmshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }

  /* Compute displacement receiving array */
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  drcvdsptab[dmshptr->procglbnbr] = drcvdsptab[dmshptr->procglbnbr - 1] + drcvcnttab[dmshptr->procglbnbr - 1];
  datarcvnbr = drcvdsptab[dmshptr->procglbnbr];
  dsndcnttab[0] = dsnddsptab[0];

  memFreeGroup (datasndtab);
  if (memAllocGroup ((void **) (void *)
        &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
        &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, dmshptr->proccomm);

  for (compnum = 0; compnum < partnbr; compnum ++)
    if (vnbrloctab[compnum].vertnbr > 0) {
      datasndtab[dsndcnttab[vnbrloctab[compnum].procnum] ++] = compnum;
      datasndtab[dsndcnttab[vnbrloctab[compnum].procnum] ++] = vnbrloctab[compnum].vertnbr;
    }

  for (procngbnum = 0 ; procngbnum < dmshptr->procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
  }

  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, dmshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    return (1);
  }

  for (compnum = 0; compnum < meshlocnb2; compnum ++) { /* compidx will be more appropriate */
    Gnum complocnum;
    Gnum * restrict vnbrtab;
#ifdef PAMPA_DEBUG_DMESH2
    Gnum verttmp;

    verttmp = 0;
#endif /* PAMPA_DEBUG_DMESH2 */

    complocnum = meshrcvtab[compnum];
    CHECK_FERR (meshValueLink(*meshloctab + compnum, (void **) &vnbrtab, PAMPA_VALUE_NONE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART), dmshptr->proccomm);
    memSet (vnbrtab, 0, dmshptr->procglbnbr * sizeof (Gnum));

    for (procngbnum = 0; procngbnum < dmshptr->procglbnbr; procngbnum ++) 
      if ((drcvdsptab[procngbnum] < drcvdsptab[procngbnum + 1]) && (datarcvtab[drcvdsptab[procngbnum]] == complocnum)) {
        vnbrtab[procngbnum] = datarcvtab[drcvdsptab[procngbnum] + 1];
#ifdef PAMPA_DEBUG_DMESH2
        verttmp += vnbrtab[procngbnum];
#endif /* PAMPA_DEBUG_DMESH2 */
        drcvdsptab[procngbnum] += 2;
      }
#ifdef PAMPA_DEBUG_DMESH2
    if (verttmp != (*meshloctab)[compnum].vertnbr) {
      errorPrint ("Vertex sum computed (%d) does not correspond to vertex number (%d)", verttmp, (*meshloctab)[compnum].vertnbr);
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, dmshptr->proccomm);
#endif /* PAMPA_DEBUG_DMESH2 */
  }

  memFreeGroup (vnumgsttax + baseval);
  memFreeGroup (datasndtab);
  memFreeGroup (meshrcvtab);
  for (compnum = 0; compnum < meshlocnb2; compnum ++)
    memFree (sortvrttab[compnum]);
  memFreeGroup ((void *) sortvrttab);
  memFreeGroup (vertloctax + baseval);

  /* Update vflgloctax on the distributed mesh */
  for (vertlocnum = baseval, vertlocnnd = dmshptr->vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
    if (vflgloctax[vertlocnum] < PAMPA_TAG_VERT_EXTERNAL)
      vflgloctax[vertlocnum] = PAMPA_TAG_VERT_INTERNAL;
    else if (vflgloctax[vertlocnum] > PAMPA_TAG_VERT_EXTERNAL)
      vflgloctax[vertlocnum] = PAMPA_TAG_VERT_FRONTIER;

  if (adptflg == 0)
    memFree (vflgloctax + baseval);


  CHECK_VDBG (cheklocval, dmshptr->proccomm);
  return (0);
}

