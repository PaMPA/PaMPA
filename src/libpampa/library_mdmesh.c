
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "mdmesh.h"
#include "pampa.h"

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* multi level distributed mesh         */
/* handling routines.                   */
/*                                      */
/****************************************/

//! \brief This routine initializes the opaque
//! multi level distributed mesh structure used to
//! handle distributed meshs in the
//! pampa library.
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_mdmeshInit (
PAMPA_Mdmesh * const        meshptr,              //!< Multi level distributed mesh to initialize
PAMPA_Num const             baseval,              //!< Base value for arrays (0 or 1)
PAMPA_Num      const        mdmhglbnbr,           //!< Communicator to be used for all communications
MPI_Comm                    proccomm)             //!< Communicator to be used for all communications
{
#ifdef PAMPA_PTHREAD                                                                                                      
  int                 thrdlvlval;
  MPI_Query_thread (&thrdlvlval);                                                                                          
  if (thrdlvlval < MPI_THREAD_MULTIPLE) {                                                                                  
    errorPrint ("PaMPA compiled with PAMPA_PTHREAD and program not launched with MPI_THREAD_MULTIPLE");
    return     (1);                                                                                                        
  }                                                                                                                        
#endif /* PAMPA_PTHREAD */                                                                                                
  if (sizeof (PAMPA_Num) != sizeof (Gnum)) {
    errorPrint ("internal error");
    return     (1);
  }
  if (sizeof (PAMPA_Mdmesh) < sizeof (Mdmesh)) {
    errorPrint ("internal error");
    return     (1);
  }

  return (mdmeshInit ((Mdmesh *) meshptr, baseval, mdmhglbnbr, proccomm));
}

//! \brief This routine frees the contents of the
//! given opaque mesh structure.
//!
//! \returns VOID  : in all cases.

void
PAMPA_mdmeshExit (
PAMPA_Mdmesh * const        meshptr)              //!< Distributed mesh structure to exit
{
  mdmeshExit ((Mdmesh *) meshptr);
}

//! \brief This routine frees the contents of the
//! given opaque mesh structure but does
//! not free its private data.
//!
//! \returns VOID  : in all cases.

void
PAMPA_mdmeshFree (
PAMPA_Mdmesh * const        meshptr)              //!< Distributed mesh structure to free
{
  mdmeshFree ((Mdmesh *) meshptr);
}


