
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "values.h"
#include "smesh.h"
#include <pampa.h>

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* distributed mesh handling routines.  */
/*                                      */
/****************************************/

//! \brief  This routine accesses smesh size data.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \returns VOID  : in all cases.

void
PAMPA_smeshSize (
const PAMPA_Smesh * const smshptr,               //!< Centralized smesh
PAMPA_Num * const         enttnbr,               //!< Number of entities
PAMPA_Num * const         vertptr)               //!< Number of vertices
{
  const Smesh *      srcsmshptr;
  Gnum enttnum, enttnnd;


  srcsmshptr = (Smesh *) smshptr;


  if (vertptr != NULL)
    *vertptr = (PAMPA_Num) (srcsmshptr->vertnbr);
  if (enttnbr != NULL)
    *enttnbr = (PAMPA_Num) (srcsmshptr->enttnbr);
}

//! \brief  This routine accesses smesh entity size data to a given entity number.
//! NULL pointers on input indicate unwanted
//! data.
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
PAMPA_smeshEnttSize (
const PAMPA_Smesh * const smshptr,               //!< Centralized smesh
PAMPA_Num const           enttnum,               //!< Entity number
PAMPA_Num * const         vertnbr)               //!< Number of vertices
{
  const Smesh *      srcsmshptr;

  srcsmshptr = (Smesh *) smshptr;

#ifdef PAMPA_DEBUG_MESH
  if ((enttnum < srcsmshptr->baseval) || (enttnum >= srcsmshptr->enttnbr + srcsmshptr->baseval)) {
    errorPrint("invalid entity number");
    return (1);
  }
#endif /* PAMPA_DEBUG_MESH */

  if (vertnbr != NULL)
    *vertnbr = srcsmshptr->vertnbr;
//    *vertnbr = (PAMPA_Num) ((srcsmshptr->entttax[enttnum] == NULL) ? (0) : (srcsmshptr->entttax[enttnum]->vertnbr)); /* TRICK: if srcmeshptr is a part of a distributed mesh, an entity could not be present (as boundary edges) */

  return (0);
}

//! \brief  This routine accesses all of the mesh data.
//! NULL pointers on input indicate unwanted
//! data. NULL pointers on output indicate
//! unexisting arrays.
//!
//! \returns VOID  : in all cases.

void
PAMPA_smeshData (
const PAMPA_Smesh * const  smshptr,              //!< Smesh structure to read
PAMPA_Num * const          baseptr,              //!< Base value            
PAMPA_Num * const          enttptr,              //!< Number of entities
PAMPA_Num * const          vertptr,              //!< Number of vertices
PAMPA_Num ** const         verttab,              //!< Index of start in edge tab
PAMPA_Num ** const         vendtab,              //!< Index of end in edge tab
PAMPA_Num * const          edgeptr,              //!< Number of links
PAMPA_Num * const          edgesiz,              //!< Size of edgetab
PAMPA_Num ** const         edgetab,              //!< Link array
PAMPA_Num ** const         venttab,              //!< Array of entity values for each vertex
PAMPA_Num * const          valuptr,              //!< Number of linked values
PAMPA_Num * const          valuptz)              //!< Maximum number of linked values
{
  const Smesh *      srcsmshptr;                 /* Pointer to source smesh structure */

  srcsmshptr = (const Smesh *) smshptr;

  if (baseptr != NULL)
    *baseptr = srcsmshptr->baseval;
  if (enttptr != NULL)
    *enttptr = srcsmshptr->enttnbr;
  if (vertptr != NULL)
    *vertptr = (PAMPA_Num) (srcsmshptr->vertnbr);
  if (verttab != NULL)
    *verttab = srcsmshptr->verttax + srcsmshptr->baseval;
  if (vendtab != NULL)
    *vendtab = srcsmshptr->vendtax + srcsmshptr->baseval;
  if (edgeptr != NULL)
    *edgeptr = srcsmshptr->edgenbr;
  if (edgesiz != NULL)
    *edgesiz = srcsmshptr->edgesiz;
  if (edgetab != NULL)
    *edgetab = srcsmshptr->edgetax + srcsmshptr->baseval;
  if (edgetab != NULL)
    *venttab = srcsmshptr->venttax + srcsmshptr->baseval;
  if (valuptr != NULL)
    *valuptr = srcsmshptr->valsptr->valunbr;
  if (valuptz != NULL)
    *valuptz = srcsmshptr->valsptr->valumax;
}

