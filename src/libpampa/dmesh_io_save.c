/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_io_save.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       These lines contains the input/output
//!                routines for distributed meshes.
//!
//!   \date        Version 1.0: from: 20 Jan 2015
//!                             to:   30 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines and includes.
*/

#define DMESH_IO_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dsmesh.h"
#include "dsmesh_dmesh.h"
#include "dmesh_allreduce.h"
#include "pampa.h"


static inline int valuePrint (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  if (!strcmp (format, "%d\t")) 
    fprintf (stream, format, *((int *) value));
  else if (!strcmp (format, "%.16e\t")) 
    fprintf (stream, "%.16e\t", *((double *) value)); 
    //fprintf (stream, format, *((double *) value));
  else if (!strcmp (format, "%lld\t")) 
    fprintf (stream, format, *((int64_t *) value));
  else {
    errorPrint ("Unrecognized format");
    return (1);
  }

  return (0);
}
    
/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
dmeshSaveValues (
Dmesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream)               /* One single centralized stream or distributed ones */
{
  Dvalues * restrict valslocptr;
  Gnum enttglbnum;
  Gnum enttglbnnd;
  int cheklocval;

  cheklocval = 0;
  valslocptr = meshptr->valslocptr;

  for (enttglbnum = PAMPA_ENTT_VIRT_PROC, enttglbnnd = meshptr->enttglbnbr + meshptr->baseval; enttglbnum < enttglbnnd; enttglbnum ++) {
    DmeshEntity * restrict enttlocptr;
    Dvalue * restrict valulocptr;
    Gnum valulocnbr;

    if (enttglbnum >= meshptr->baseval)
      enttlocptr = meshptr->enttloctax[enttglbnum];
    for (valulocnbr = 0, valulocptr = valslocptr->evalloctak[enttglbnum]; valulocptr != NULL; valulocptr = valulocptr->next, valulocnbr ++);
    fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", enttglbnum, valulocnbr);

    for (valulocptr = valslocptr->evalloctak[enttglbnum]; valulocptr != NULL; valulocptr = valulocptr->next) {
      int addnbr, intnbr, typenbr, combdat;
      MPI_Aint            valuglbsiz;                 /* Extent of attribute datatype */
      MPI_Aint            dummy;
      Gnum countnbr;
      Gnum countnum;
      Gnum procglbnum;
      Gnum vertlocnum;
      Gnum vertlocnnd;
      size_t elemsiz;
      char format[9];
      int intdat;
      byte * valuloctab;
      MPI_Datatype typedat;

      valuloctab = (byte *) valulocptr->valuloctab;
      MPI_Type_get_extent (valulocptr->typeval, &dummy, &valuglbsiz);     /* Get type extent */
      MPI_Type_get_envelope( valulocptr->typeval, &intnbr, &addnbr, &typenbr, &combdat);

      fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\t", valulocptr->tagnum, valulocptr->flagval);

      switch (combdat) {
        case MPI_COMBINER_NAMED:
          countnbr = 1;
          fprintf (stream, "1\t");
          if (valulocptr->typeval == MPI_INT) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 1);
            strcpy (format, "%d\t");
            elemsiz = sizeof(int);
          }
          else if (valulocptr->typeval == MPI_DOUBLE) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 2);
            strcpy (format, "%.16e\t");
            elemsiz = sizeof (double);
          }
          else if (valulocptr->typeval == MPI_LONG_LONG) {
            fprintf (stream, GNUMSTRING "\n", (Gnum) 3);
            strcpy (format, "%lld\t");
            elemsiz = sizeof (long long int);
          }
          else {
            errorPrint ("Type not implemented");
            cheklocval = 1;
          }
          break;
        case MPI_COMBINER_CONTIGUOUS:
          fprintf (stream, "2\t");
          MPI_Type_get_contents( valulocptr->typeval, 1, 0, 1, &intdat, NULL, &typedat); 
          countnbr = (Gnum) intdat;
          if (typedat == MPI_INT) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 1);
            strcpy (format, "%d\t");
            elemsiz = sizeof (int);
          }
          else if (typedat == MPI_DOUBLE) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 2);
            strcpy (format, "%.16e\t");
            elemsiz = sizeof (double);
          }
          else if (valulocptr->typeval == MPI_LONG_LONG) {
            fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\n", countnbr, (Gnum) 3);
            strcpy (format, "%lld\t");
            elemsiz = sizeof (long long int);
          }
          else {
            errorPrint ("Type not implemented");
            cheklocval = 1;
          }
          break;
        default:
          errorPrint ("case not implemented");
          cheklocval = 1;
      }
      CHECK_VERR (cheklocval, meshptr->proccomm);

      switch (enttglbnum) {
        case PAMPA_ENTT_VIRT_PROC:
          for (procglbnum = 0; procglbnum < meshptr->procglbnbr; procglbnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valuloctab + procglbnum * valuglbsiz + countnum * elemsiz);
          fprintf (stream, "\n");
          break;
        case PAMPA_ENTT_VIRT_VERT:
          for (vertlocnum = 0; vertlocnum < meshptr->vertlocnbr; vertlocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valuloctab + vertlocnum * valuglbsiz + countnum * elemsiz);
          fprintf (stream, "\n");
          break;
        default: /* real entity */

          for (vertlocnum = meshptr->baseval, vertlocnnd = enttlocptr->vertlocnbr + meshptr->baseval; vertlocnum < vertlocnnd; vertlocnum ++)
            for (countnum = 0; countnum < countnbr; countnum ++)
              valuePrint (stream, format, valuloctab + vertlocnum * valuglbsiz + countnum * elemsiz);
          fprintf (stream, "\n");
      }
    }
  }
  CHECK_VDBG(cheklocval, meshptr->proccomm);
  return (0);
}

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
dmeshSave (
Dmesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream)               /* One single centralized stream or distributed ones */
{
  Dvalues * restrict valslocptr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum edgelocnum;
  Gnum edgelocnnd;
  Gnum enttglbnum;
  Gnum enttglbnnd;
  int  procglbnum;
  Gnum reduloctab[2];
  Gnum reduglbtab[2];
  Dsmesh smshdat;
  int cheklocval;

  cheklocval = 0;
  valslocptr = meshptr->valslocptr;
  dsmeshInit(&smshdat, meshptr->proccomm);
  dmesh2dsmesh(meshptr, &smshdat);

  reduloctab[0] = meshptr->vertlocnbr;
  reduloctab[1] = meshptr->edgelocnbr;
  if (commAllreduce (reduloctab, reduglbtab, 2, GNUM_MPI, MPI_SUM, meshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR(cheklocval, meshptr->proccomm);

  cheklocval |= (fprintf(stream,
      "%d\t%d\t%d\n" /* version, parallel, Gnum size  */
      "%d\t%d\n" /* proclocnum, procglbnbr */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n"  /* enttglbnbr, vertglbnbr, edgeglbnbr */
      GNUMSTRING "\t" GNUMSTRING "\n"  /* vertlocnbr, edgelocnbr */
      GNUMSTRING "\t" GNUMSTRING "\n"  /* valuglbnbr, valuglbmax */
      GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\t" GNUMSTRING "\n" GNUMSTRING "\n", /* baseval, ovlpglbval, esubloctab, vlblloctax, edloloctax    */
      0, /* Version */
      2, /* Parallel XXX à changer */
      1, /* Gnumval XXX à changer */
      smshdat.proclocnum, smshdat.procglbnbr,
      smshdat.enttglbnbr, reduglbtab[0], reduglbtab[1], /* enttglbnbr, vertglbnbr, edgeglbnbr */
      smshdat.vertlocnbr, smshdat.edgelocnbr,
      valslocptr->valuglbnbr, valslocptr->valuglbmax,
      smshdat.baseval, smshdat.ovlpglbval, (Gnum) (smshdat.esubloctax == NULL ? 0 : 1),
      (Gnum) 0, /* vlblloctax not yet implemented */
      (Gnum) (smshdat.edloloctax == NULL ? 0 : 1)) == EOF);
  CHECK_VERR(cheklocval, smshdat.proccomm);

  if (smshdat.esubloctax != NULL) {
    for (enttglbnum = smshdat.baseval, enttglbnnd = smshdat.baseval + smshdat.enttglbnbr; enttglbnum < enttglbnnd; enttglbnum ++)
      cheklocval |= (fprintf (stream, GNUMSTRING "\t", smshdat.esubloctax[enttglbnum]) == EOF);
    cheklocval |= (fprintf (stream, "\n") == EOF);
  }

  for (vertlocnum = smshdat.baseval, vertlocnnd = smshdat.baseval + smshdat.vertlocnbr; vertlocnum < vertlocnnd; vertlocnum ++) {
    fprintf (stream, GNUMSTRING "\t" GNUMSTRING, smshdat.ventloctax[vertlocnum], smshdat.vendloctax[vertlocnum] - smshdat.vertloctax[vertlocnum]);
    for (edgelocnum = smshdat.vertloctax[vertlocnum], edgelocnnd = smshdat.vendloctax[vertlocnum]; edgelocnum < edgelocnnd; edgelocnum ++) {
      if (smshdat.edloloctax != NULL)
        fprintf (stream, "\t" GNUMSTRING, smshdat.edloloctax[edgelocnum]);
      fprintf (stream, "\t" GNUMSTRING, smshdat.edgeloctax[edgelocnum]);
    }
    fprintf (stream, "\n");
  }
  fprintf (stream, "\n");
  CHECK_VDBG(cheklocval, smshdat.proccomm);
  dsmeshExit (&smshdat);
  
  return (dmeshSaveValues (meshptr, stream));
}
    

/* This routine XXX
** It returns:
** - 0   : on success.
** - !0  : on error.
*/

int
dmeshSaveAll (
Dmesh * restrict const     meshptr,              /* Not const since halo may update structure         */
FILE * const               stream)               /* One single centralized stream or distributed ones */
{
  Gnum ventlocnum;
  Gnum ventlocnnd;
  Gnum edgelocnum;
  Gnum edgelocnnd;
  Gnum enttglbnum;
  Gnum enttglbnnd;
  int  procglbnum;
  Dvalues * restrict valslocptr;
  int cheklocval;

  cheklocval = 0;
  valslocptr = meshptr->valslocptr;

  cheklocval |= (fprintf(stream,
      "%d\t%d\t%d\n" /* version, parallel, Gnum size  */
      "%d\t" /* meshptr->flagval    */
      GNUMSTRING "\t" /* meshptr->baseval    */
      GNUMSTRING "\t" /* meshptr->ovlpglbval */
      GNUMSTRING "\t" /* meshptr->vertlocnbr */
      GNUMSTRING "\t" /* meshptr->vtrulocnbr */
      GNUMSTRING "\t" /* meshptr->vertlocmax */
      GNUMSTRING "\t" /* meshptr->vertglbnbr */
      GNUMSTRING "\t" /* meshptr->enttglbnbr */
      GNUMSTRING "\t" /* meshptr->esublocmsk */
      GNUMSTRING "\t" /* meshptr->degrlocmax */
      GNUMSTRING "\t" /* meshptr->edgelocnbr */
      GNUMSTRING "\t" /* meshptr->edgelocsiz */
      "%d\t"          /* meshptr->procglbnbr */
      "%d\t"          /* meshptr->proclocnum */
      "%d\t"          /* meshptr->procngbnbr */
      "%d\t"          /* meshptr->procngbmax */
      GNUMSTRING "\t", /* valuglbmax          */
      0, /* Version */
      3, /* All Parallel XXX à changer */
      1, /* Gnumval XXX à changer */
      meshptr->flagval,   
      meshptr->baseval,   
      meshptr->ovlpglbval,
      meshptr->vertlocnbr,
      meshptr->vtrulocnbr,
      meshptr->vertlocmax,
      meshptr->vertglbnbr,
      meshptr->enttglbnbr,
      meshptr->esublocmsk,
      meshptr->degrlocmax,
      meshptr->edgelocnbr,
      meshptr->edgelocsiz,
      meshptr->procglbnbr,
      meshptr->proclocnum,
      meshptr->procngbnbr,
      meshptr->procngbmax,
      meshptr->valslocptr->valuglbmax) == EOF);
  if (meshptr->esublocmsk != 0) {
    for (enttglbnum = meshptr->baseval, enttglbnnd = meshptr->baseval + meshptr->enttglbnbr; enttglbnum < enttglbnnd; enttglbnum ++)
      cheklocval |= (fprintf (stream, GNUMSTRING "\t", meshptr->esublocbax[enttglbnum]) == EOF);
    cheklocval |= (fprintf (stream, "\n") == EOF);
  }

  cheklocval |= (fprintf (stream, "%d\n", (meshptr->edloloctax == NULL ? 0 : 1)) == EOF);
  if (meshptr->edloloctax != NULL) {
    Gnum edlolocnum;
    for (edlolocnum = meshptr->baseval; edlolocnum < meshptr->edgelocsiz; edlolocnum ++)
      cheklocval |= (fprintf (stream, GNUMSTRING "\t", meshptr->edloloctax[edlolocnum]) == EOF);
    cheklocval |= (fprintf (stream, "\n") == EOF);
  }

  for (edgelocnum = meshptr->baseval; edgelocnum < meshptr->edgelocsiz; edgelocnum ++)
    cheklocval |= (fprintf (stream, GNUMSTRING "\t", meshptr->edgeloctax[edgelocnum]) == EOF);
  cheklocval |= (fprintf (stream, "\n") == EOF);

  for (procglbnum = 0; procglbnum <= meshptr->procglbnbr; procglbnum ++)
    cheklocval |= (fprintf (stream, GNUMSTRING "\t", meshptr->procvrttab[procglbnum]) == EOF);
  cheklocval |= (fprintf (stream, "\n") == EOF);

  for (procglbnum = 0; procglbnum < meshptr->procglbnbr; procglbnum ++)
    cheklocval |= (fprintf (stream, GNUMSTRING "\t", meshptr->proccnttab[procglbnum]) == EOF);
  cheklocval |= (fprintf (stream, "\n") == EOF);

  for (procglbnum = 0; procglbnum <= meshptr->procglbnbr; procglbnum ++)
    cheklocval |= (fprintf (stream, GNUMSTRING "\t", meshptr->procdsptab[procglbnum]) == EOF);
  cheklocval |= (fprintf (stream, "\n") == EOF);

  for (procglbnum = 0; procglbnum < meshptr->procngbnbr; procglbnum ++)
    cheklocval |= (fprintf (stream, "%d\t", meshptr->procngbtab[procglbnum]) == EOF);
  cheklocval |= (fprintf (stream, "\n") == EOF);

  for (ventlocnum = meshptr->baseval, ventlocnnd = meshptr->enttglbnbr + meshptr->baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
    DmeshEntity * restrict ventlocptr;
    Gnum vertlocnum;
    Gnum vertlocnnd;
    Gnum nentlocnum;
    Gnum nentlocnnd;
    int procsidnum;

    cheklocval |= (fprintf (stream, "%d\n", (meshptr->enttloctax[ventlocnum] == NULL ? 0 : 1)) == EOF);

    if (meshptr->enttloctax[ventlocnum] != NULL) {
      ventlocptr = meshptr->enttloctax[ventlocnum];

      cheklocval |= (fprintf (stream, GNUMSTRING "\n", ventlocptr->vgstlocnbr) == EOF);
      cheklocval |= (fprintf(stream,
            GNUMSTRING "\t" /* vfrnlocmin */
            GNUMSTRING "\t" /* vfrnlocmax */
            GNUMSTRING "\t" /* vertlocnbr */
            GNUMSTRING "\t" /* vintlocnbr */
            GNUMSTRING "\t" /* vovplocnbr */
            GNUMSTRING "\t", /* vertglbnbr */
            ventlocptr->vfrnlocmin,
            ventlocptr->vfrnlocmax,
            ventlocptr->vertlocnbr,
            ventlocptr->vintlocnbr,
            ventlocptr->vovplocnbr,
            ventlocptr->vertglbnbr) == EOF);
      cheklocval |= (fprintf (stream, "\n") == EOF);

      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { /* If ventlocnum is not a sub-entity */
        for (vertlocnum = meshptr->baseval, vertlocnnd = ventlocptr->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fprintf (stream, GNUMSTRING "\t", ventlocptr->mvrtloctax[vertlocnum]) == EOF);
        cheklocval |= (fprintf (stream, "\n") == EOF);
      }

      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] != PAMPA_ENTT_SINGLE) { /* If ventlocnum is not a single entity */
        for (vertlocnum = meshptr->baseval, vertlocnnd = ventlocptr->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fprintf (stream, GNUMSTRING "\t", ventlocptr->svrtloctax[vertlocnum]) == EOF);
        cheklocval |= (fprintf (stream, "\n") == EOF);
        for (vertlocnum = meshptr->baseval, vertlocnnd = ventlocptr->vgstlocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fprintf (stream, GNUMSTRING "\t", ventlocptr->ventloctax[vertlocnum]) == EOF);
        cheklocval |= (fprintf (stream, "\n") == EOF);
      }

      cheklocval |= (fprintf (stream, "%d\n", (ventlocptr->vprmloctax == NULL ? 0 : 1)) == EOF);
      if (ventlocptr->vprmloctax != NULL) {
        for (vertlocnum = meshptr->baseval, vertlocnnd = meshptr->baseval + ventlocptr->vovplocnbr; vertlocnum < vertlocnnd; vertlocnum ++)
          cheklocval |= (fprintf (stream, GNUMSTRING "\t", ventlocptr->vprmloctax[vertlocnum]) == EOF);
        cheklocval |= (fprintf (stream, "\n") == EOF);
      }

      cheklocval |= (fprintf (stream, "%d\n", (ventlocptr->vmatloctax == NULL ? 0 : 1)) == EOF);
      if (ventlocptr->vmatloctax != NULL)
        infoPrint ("Not yet implemented for dmeshMat");
    }
  }

  for (ventlocnum = meshptr->baseval, ventlocnnd = meshptr->enttglbnbr + meshptr->baseval; ventlocnum < ventlocnnd; ventlocnum ++) {
    DmeshEntity * restrict ventlocptr;
    Gnum vertlocsiz;
    Gnum vertlocnum;
    Gnum vertlocnnd;
    Gnum nentlocnum;
    Gnum nentlocnnd;
    int procsidnum;

    if (meshptr->enttloctax[ventlocnum] != NULL) {
      ventlocptr = meshptr->enttloctax[ventlocnum];
      if (meshptr->ovlpglbval > 0) 
        vertlocsiz = ventlocptr->vgstlocnbr;
      else 
        vertlocsiz = ventlocptr->vertlocnbr;


      if (meshptr->esublocbax[ventlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) /* If ventlocnum is not a sub-entity */
        for (nentlocnum = meshptr->baseval, nentlocnnd = meshptr->enttglbnbr + meshptr->baseval; nentlocnum < nentlocnnd; nentlocnum ++) {
          if ((meshptr->enttloctax[nentlocnum] != NULL) || (nentlocnum == meshptr->baseval) || (nentlocnum == nentlocnnd - 1)) {
            DmeshEnttNghb * restrict nghblocptr;

            cheklocval |= (fprintf (stream, "%d\n", (ventlocptr->nghbloctax[nentlocnum] == NULL ? 0 : 1)) == EOF);

            nghblocptr = ventlocptr->nghbloctax[nentlocnum];

            if (meshptr->esublocbax[nentlocnum & meshptr->esublocmsk] <= PAMPA_ENTT_SINGLE) { /* If nentlocnum is not a sub-entity */
              for (vertlocnum = meshptr->baseval; vertlocnum < vertlocsiz; vertlocnum ++)
                cheklocval |= (fprintf (stream, GNUMSTRING "\t" GNUMSTRING "\t", 
                      nghblocptr->vindloctax[vertlocnum].vertlocidx,
                      nghblocptr->vindloctax[vertlocnum].vendlocidx) == EOF);
              cheklocval |= (fprintf (stream, "\n") == EOF);
            }
          }
        }

      cheklocval |= (fprintf(stream,
            GNUMSTRING "\t"  /* procsndnbr */
            "%d\t"  /* procsidnbr */
            "%d\t", /* procngbnbr */
            ventlocptr->commlocdat.procsndnbr,
            ventlocptr->commlocdat.procsidnbr,
            ventlocptr->commlocdat.procngbnbr) == EOF);
      cheklocval |= (fprintf (stream, "\n") == EOF);

      for (procglbnum = 0; procglbnum < meshptr->procglbnbr; procglbnum ++)
        cheklocval |= (fprintf (stream, "%d\t", ventlocptr->commlocdat.procrcvtab[procglbnum]) == EOF);
      cheklocval |= (fprintf (stream, "\n") == EOF);

      for (procglbnum = 0; procglbnum < meshptr->procglbnbr; procglbnum ++)
        cheklocval |= (fprintf (stream, "%d\t", ventlocptr->commlocdat.procsndtab[procglbnum]) == EOF);
      cheklocval |= (fprintf (stream, "\n") == EOF);

      for (procsidnum = 0; procsidnum < ventlocptr->commlocdat.procsidnbr; procsidnum ++)
        cheklocval |= (fprintf (stream, "%d\t", ventlocptr->commlocdat.procsidtab[procsidnum]) == EOF);
      cheklocval |= (fprintf (stream, "\n") == EOF);

      for (procglbnum = 0; procglbnum < ventlocptr->commlocdat.procngbnbr; procglbnum ++)
        cheklocval |= (fprintf (stream, "%d\t", ventlocptr->commlocdat.procngbtab[procglbnum]) == EOF);
      cheklocval |= (fprintf (stream, "\n") == EOF);

    }
  }

  CHECK_VDBG(cheklocval, meshptr->proccomm);
  return (dmeshSaveValues (meshptr, stream));
}
