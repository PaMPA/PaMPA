/* Copyright 2004,2007,2008,2010,2012,2015,2016 IPB, Universite de Bordeaux, Inria & CNRS
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        common_memory.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       Part of a parallel direct block solver.
//!                This module handles memory allocations.
//!
//!   \date        Version 1.0: from: 18 Oct 2011
//!                             to:   26 Sep 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define COMMON_MEMORY

#ifndef COMMON_NOMODULE
#include "module.h"
#endif /* COMMON_NOMODULE */
#include "common.h"

#ifdef PAMPA_DEBUG_MEM
typedef struct pointers_ {
  int ptrnbr;
  void ** ptrtab;
} pointers;
#endif /* PAMPA_DEBUG_MEM */
#if(defined (PTSCOTCH) && defined (SCOTCH_MEMORY_TRACE))
#include "ptscotch.h"
#endif /* PTSCOTCH && SCOTCH_MEMORY_TRACE */

/*********************************/
/*                               */
/* The memory handling routines. */
/*                               */
/*********************************/

/* This routine keeps track of the amount of
** allocated memory, and keeps track of the
** maximum allowed.
*/

#ifdef COMMON_MEMORY_TRACE
#define COMMON_MEMORY_TRACE_ COMMON_MEMORY_TRACE
#undef COMMON_MEMORY_TRACE

#define COMMON_MEMORY_SKEW          (MAX ((sizeof (size_t)), (sizeof (double)))) /* For proper alignment */

static size_t           memorysiz = 0;            /*+ Number of allocated bytes        +*/
static size_t           memorymax = 0;            /*+ Maximum amount of allocated data +*/
static FILE *           memfile;
static int              id = 0;

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
static int              muteflag = 1;             /*+ Flag for mutex initialization +*/
static pthread_mutex_t  mutelocdat;               /*+ Local mutex for updates       +*/
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

#ifdef COMMON_MEMORY_USER_FILE
extern char PAMPA_memFilename[];
#endif /* COMMON_MEMORY_USER_FILE */

void *
memAllocRecord (
const char * const          function,
int                         line,
size_t                      newsiz)
{
  size_t              newadd;
  byte *              newptr;

  id ++;
  if (memorysiz == 0) {
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#ifdef COMMON_MEMORY_USER_FILE
	memfile = fopen(PAMPA_memFilename,"w");
#else /* COMMON_MEMORY_USER_FILE */
	char s[50];
	sprintf(s,"mem-%d", rank);
	memfile = fopen(s,"w");
#endif /* COMMON_MEMORY_USER_FILE */
#ifdef SCOTCH_MEMORY_TRACE
	fprintf(memfile, "id\t\tadd/remove\t\tsize\t\tmax\t\tscotch_size\t\tscotch_max\t\tfunction\t\tline\n");
#else /* SCOTCH_MEMORY_TRACE */
	fprintf(memfile, "id\t\tadd/remove\t\tsize\t\tmax\t\tfunction\t\tline\n");
#endif /* SCOTCH_MEMORY_TRACE */
  }
#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  if (muteflag != 0) {                            /* Unsafe code with respect to race conditions but should work as first allocs are sequential */
    muteflag = 0;
    pthread_mutex_init (&mutelocdat, NULL);       /* Initialize local mutex */
  }
  pthread_mutex_lock (&mutelocdat);               /* Lock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  if ((newptr = malloc (newsiz + COMMON_MEMORY_SKEW)) == NULL) /* Non-zero sizes will guarantee non-NULL pointers */
    newadd = (size_t) 0;
  else {
    newadd = newsiz;
    *((size_t *) newptr) = newsiz;                /* Record size for freeing                */
    newptr += COMMON_MEMORY_SKEW;                 /* Skew pointer while enforcing alignment */
  }

  memorysiz += newadd;
  if (memorymax < memorysiz)
    memorymax = memorysiz;
#ifdef SCOTCH_MEMORY_TRACE
  fprintf (memfile, "%d\t\t%zu\t\t%zu\t\t%zu\t\t%d\t\t%d\t\t%s\t\t%d\n", id, newadd, memorysiz, memorymax, SCOTCH_memCur(), SCOTCH_memMax(), function, line);
#else /* SCOTCH_MEMORY_TRACE */
  fprintf (memfile, "%d\t\t%zu\t\t%zu\t\t%zu\t\t%s\t\t%d\n", id, newadd, memorysiz, memorymax, function, line);
#endif /* SCOTCH_MEMORY_TRACE */

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_unlock (&mutelocdat);             /* Unlock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  return ((void *) newptr);                       /* Return skewed pointer or NULL */
}

void *
memReallocRecord (
const char * const          function,
int                         line,
void *                      oldptr,
size_t                      newsiz)
{
  byte *              tmpptr;
  byte *              newptr;
  size_t              oldsiz;
  size_t              newadd;

  id ++;
  if (oldptr != NULL) {
  	tmpptr = ((byte *) oldptr) - COMMON_MEMORY_SKEW;
  	oldsiz = *((size_t *) tmpptr);
  }
  else {
	tmpptr = NULL;
	oldsiz = 0;
  }

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_lock (&mutelocdat);               /* Lock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  if ((newptr = realloc (tmpptr, newsiz + COMMON_MEMORY_SKEW)) == NULL)
    newadd = (size_t) 0;
  else {
    newadd = newsiz - oldsiz;                     /* Add difference between the two sizes   */
    *((size_t *) newptr) = newsiz;                /* Record size for freeing                */
    newptr += COMMON_MEMORY_SKEW;                 /* Skew pointer while enforcing alignment */
  }

  memorysiz += newadd;
  if (memorymax < memorysiz)
    memorymax = memorysiz;
#ifdef SCOTCH_MEMORY_TRACE
  fprintf (memfile, "%d\t\t%zu\t\t%zu\t\t%zu\t\t%d\t\t%d\t\t%s\t\t%d\n", id, newadd, memorysiz, memorymax, SCOTCH_memCur(), SCOTCH_memMax(), function, line);
#else /* SCOTCH_MEMORY_TRACE */
  fprintf (memfile, "%d\t\t%zu\t\t%zu\t\t%zu\t\t%s\t\t%d\n", id, newadd, memorysiz, memorymax, function, line);
#endif /* SCOTCH_MEMORY_TRACE */

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_unlock (&mutelocdat);             /* Unlock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  return ((void *) newptr);                       /* Return skewed pointer or NULL */
}

void
memFreeCommonRecord (
const char * const          function,
int                         line,
void *                      oldptr)
{
  size_t              oldsiz;
  byte *              tmpptr;

  id ++;
  if (oldptr != NULL) {
  tmpptr = ((byte *) oldptr) - COMMON_MEMORY_SKEW;
  oldsiz = *((size_t *) tmpptr);
  }
  else {
	tmpptr = NULL;
	oldsiz = 0;
  }

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_lock (&mutelocdat);               /* Lock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  memorysiz -= oldsiz;
#ifdef SCOTCH_MEMORY_TRACE
  fprintf (memfile, "%d\t\t%zu\t\t%zu\t\t%zu\t\t%d\t\t%d\t\t%s\t\t%d\n", id, -oldsiz, memorysiz, memorymax, SCOTCH_memCur(), SCOTCH_memMax(), function, line);
#else /* SCOTCH_MEMORY_TRACE */
  fprintf (memfile, "%d\t\t%zu\t\t%zu\t\t%zu\t\t%s\t\t%d\n", id, -oldsiz, memorysiz, memorymax, function, line);
#endif /* SCOTCH_MEMORY_TRACE */

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_unlock (&mutelocdat);             /* Unlock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */
}

void
memFreeGroupRecord (
const char * const          function,
int                         line,
void *                      oldptr)
{

  memFreeCommonRecord (function, line, oldptr);

#ifdef PAMPA_DEBUG_MEM
  pointers *          ptrs;
  int                 ptrnum;
#endif /* PAMPA_DEBUG_MEM */

#ifndef PAMPA_DEBUG_MEM
  free (oldptr);
#else /* PAMPA_DEBUG_MEM */
  //ptrs->ptrtab = *((int *) ((int **) oldptr - 1));
  //ptrs = ((pointers *) oldptr) - 1;
  ptrs = *(((pointers **) oldptr) - 1);

  for (ptrnum = 0; ptrnum < ptrs->ptrnbr; ptrnum ++)
    free (ptrs->ptrtab[ptrnum]);
  free (ptrs->ptrtab);
  free (ptrs);
#endif /* PAMPA_DEBUG_MEM */
}

void
memFreeRecord (
const char * const          function,
int                         line,
void *                      oldptr)
{
  memFreeCommonRecord (function, line, oldptr);
}

size_t
memMax ()
{
  size_t              curmax;

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_lock (&mutelocdat);               /* Lock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  curmax = memorymax;

#if (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD))
  pthread_mutex_unlock (&mutelocdat);             /* Unlock local mutex */
#endif /* (defined (COMMON_PTHREAD) || defined (PAMPA_PTHREAD)) */

  return (curmax);
}

/* This routine allocates a set of arrays in
** a single memAlloc()'ed array, the address
** of which is placed in the first argument.
** Arrays to be allocated are described as
** a duplet of ..., &ptr, size, ...,
** terminated by a NULL pointer.
** It returns:
** - !NULL  : pointer to block, all arrays allocated.
** - NULL   : no array allocated.
*/

void *
memAllocGroupRecord (
const char * const          function,
int                         line,
void **                     memptr,               /*+ Pointer to first argument to allocate +*/
...)
{
  va_list             memlist;                    /* Argument list of the call              */
  byte **             memloc;                     /* Pointer to pointer of current argument */
  size_t              memoff;                     /* Offset value of argument               */
#ifndef PAMPA_DEBUG_MEM
  byte *              blkptr;                     /* Pointer to memory chunk                */

  memoff = 0;
  memloc = (byte **) memptr;                      /* Point to first memory argument */
  va_start (memlist, memptr);                     /* Start argument parsing         */
  while (memloc != NULL) {                        /* As long as not NULL pointer    */
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1));
    memoff += va_arg (memlist, size_t);
    memloc  = va_arg (memlist, byte **);
  }

  if ((blkptr = (byte *) memAllocRecord (function, line, memoff)) == NULL) { /* If cannot allocate   */
    *memptr = NULL;                               /* Set first pointer to NULL */
    va_end (memlist);
    return (NULL);
  }
#endif /* PAMPA_DEBUG_MEM */

  memoff = 0;
  memloc = (byte **) memptr;                      /* Point to first memory argument */
  va_start (memlist, memptr);                     /* Restart argument parsing       */
  while (memloc != NULL) {                        /* As long as not NULL pointer    */
#ifndef PAMPA_DEBUG_MEM
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1)); /* Pad  */
    *memloc = blkptr + memoff;                    /* Set argument address           */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes        */
#else /* PAMPA_DEBUG_MEM */
    memoff = va_arg (memlist, size_t);           /* Get padded sizes                */
    if ((*memloc = (void *) memAllocRecord (function, line, memoff)) == NULL) { /* If cannot allocate   */
      *memptr = NULL;                               /* Set first pointer to NULL */
      va_end (memlist);
      return (NULL);
    }
#endif /* PAMPA_DEBUG_MEM */
    memloc  = va_arg (memlist, void *);           /* Get next argument pointer      */
  }

  va_end (memlist);
#ifndef PAMPA_DEBUG_MEM
  return ((void *) blkptr);
#else /* PAMPA_DEBUG_MEM */
  return ((void *) *memptr);
#endif /* PAMPA_DEBUG_MEM */
}

/* This routine reallocates a set of arrays in
** a single memRealloc()'ed array passed as
** first argument, and the address of which
** is placed in the second argument.
** Arrays to be allocated are described as
** a duplet of ..., &ptr, size, ...,
** terminated by a NULL pointer.
** It returns:
** - !NULL  : pointer to block, all arrays allocated.
** - NULL   : no array allocated.
*/

void *
memReallocGroupRecord (
const char * const          function,
int                         line,
void *                      oldptr,               /*+ Pointer to block to reallocate +*/
...)
{
  va_list             memlist;                    /* Argument list of the call              */
  byte **             memloc;                     /* Pointer to pointer of current argument */
  size_t              memoff;                     /* Offset value of argument               */
#ifndef PAMPA_DEBUG_MEM
  byte *              blkptr;                     /* Pointer to memory chunk                */

  memoff = 0;
  va_start (memlist, oldptr);                     /* Start argument parsing */

  while ((memloc = va_arg (memlist, byte **)) != NULL) { /* As long as not NULL pointer */
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1)); /* Pad      */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes            */
  }

  if ((blkptr = (byte *) memReallocRecord (function, line, oldptr, memoff)) == NULL) { /* If cannot allocate block */
    va_end (memlist);
    return (NULL);
  }
#endif /* PAMPA_DEBUG_MEM */

  memoff = 0;
  va_start (memlist, oldptr);                     /* Restart argument parsing           */
  while ((memloc = va_arg (memlist, byte **)) != NULL) { /* As long as not NULL pointer */
#ifndef PAMPA_DEBUG_MEM
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1)); /* Pad      */
    *memloc = blkptr + memoff;                    /* Set argument address               */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes            */
#else /* PAMPA_DEBUG_MEM */
    memoff = va_arg (memlist, size_t);           /* Get padded sizes                    */
    if ((*memloc = (byte *) memReallocRecord (function, line, *memloc, memoff)) == NULL) { /* If cannot allocate block */
      va_end (memlist);
      return (NULL);
    }
#endif /* PAMPA_DEBUG_MEM */
  }

#ifndef PAMPA_DEBUG_MEM
  return ((void *) blkptr);
#else /* PAMPA_DEBUG_MEM */
  return ((void *) 0x1);
#endif /* PAMPA_DEBUG_MEM */
}
#else /* COMMON_MEMORY_TRACE */

/* This routine allocates a set of arrays in
** a single memAlloc()'ed array, the address
** of which is placed in the first argument.
** Arrays to be allocated are described as
** a duplet of ..., &ptr, size, ...,
** terminated by a NULL pointer.
** It returns:
** - !NULL  : pointer to block, all arrays allocated.
** - NULL   : no array allocated.
*/

void *
memAllocGroup (
void **                     memptr,               /*+ Pointer to first argument to allocate +*/
...)
{
  va_list             memlist;                    /* Argument list of the call              */
  byte **             memloc;                     /* Pointer to pointer of current argument */
  size_t              memoff;                     /* Offset value of argument               */
#ifndef PAMPA_DEBUG_MEM
  byte *              blkptr;                     /* Pointer to memory chunk                */
#else /* PAMPA_DEBUG_MEM */
  pointers *          ptrs;

  if ((ptrs = (pointers *) memAlloc (sizeof(pointers))) == NULL) { /* If cannot allocate   */
    *memptr = NULL;                               /* Set first pointer to NULL */
    return (NULL);
  }
  ptrs->ptrnbr = 0;
#endif /* PAMPA_DEBUG_MEM */

  memoff = 0;
  memloc = (byte **) memptr;                      /* Point to first memory argument */
  va_start (memlist, memptr);                     /* Start argument parsing         */
  while (memloc != NULL) {                        /* As long as not NULL pointer    */
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1));
    memoff += va_arg (memlist, size_t);
    memloc  = va_arg (memlist, byte **);
#ifdef PAMPA_DEBUG_MEM
    ptrs->ptrnbr ++;
#endif /* PAMPA_DEBUG_MEM */
  }

#ifndef PAMPA_DEBUG_MEM
  if ((blkptr = (byte *) memAlloc (memoff)) == NULL) { /* If cannot allocate   */
    *memptr = NULL;                               /* Set first pointer to NULL */
    va_end (memlist);
    return (NULL);
  }
#else /* PAMPA_DEBUG_MEM */
  if ((ptrs->ptrtab = (void **) memAlloc (ptrs->ptrnbr * sizeof (void *))) == NULL) { /* If cannot allocate   */
    memFree (ptrs);
    ptrs->ptrtab = NULL;                                                       /* Set first pointer to NULL */
    va_end (memlist);
    return (NULL);
  }
  ptrs->ptrnbr = 0;
#endif /* PAMPA_DEBUG_MEM */
  va_end (memlist);

  memoff = 0;
  memloc = (byte **) memptr;                      /* Point to first memory argument */
  va_start (memlist, memptr);                     /* Restart argument parsing       */
  while (memloc != NULL) {                        /* As long as not NULL pointer    */
#ifndef PAMPA_DEBUG_MEM
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1)); /* Pad  */
    *memloc = blkptr + memoff;                    /* Set argument address           */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes        */
#else /* PAMPA_DEBUG_MEM */
    memoff = va_arg (memlist, size_t);           /* Get padded sizes                */
    if (ptrs->ptrnbr == 0)
      memoff += sizeof (pointers *);
    if ((*memloc = (void *) memAlloc (memoff)) == NULL) { /* If cannot allocate   */
      *memptr = NULL;                               /* Set first pointer to NULL */
      va_end (memlist);
      return (NULL);
    }
    ptrs->ptrtab[ptrs->ptrnbr] = (void *) *memloc;
    if (ptrs->ptrnbr == 0) {
      *((pointers **) (*memloc)) = ptrs;
      *memloc += sizeof (pointers *);
    }
    ptrs->ptrnbr ++;
#endif /* PAMPA_DEBUG_MEM */
    memloc  = va_arg (memlist, void *);           /* Get next argument pointer      */
  }

#ifndef PAMPA_DEBUG_MEM
  return ((void *) blkptr);
#else /* PAMPA_DEBUG_MEM */
  return ((void *) *memptr);
#endif /* PAMPA_DEBUG_MEM */
}

void
memFreeGroup (
void *                      oldptr)
{
#ifdef PAMPA_DEBUG_MEM
  pointers *          ptrs;
  int                 ptrnum;
#endif /* PAMPA_DEBUG_MEM */

#ifndef PAMPA_DEBUG_MEM
  free (oldptr);
#else /* PAMPA_DEBUG_MEM */
  //ptrs->ptrtab = *((int *) ((int **) oldptr - 1));
  //ptrs = ((pointers *) oldptr) - 1;
  ptrs = *(((pointers **) oldptr) - 1);

  for (ptrnum = 0; ptrnum < ptrs->ptrnbr; ptrnum ++)
    free (ptrs->ptrtab[ptrnum]);
  free (ptrs->ptrtab);
  free (ptrs);
#endif /* PAMPA_DEBUG_MEM */
}

/* This routine reallocates a set of arrays in
** a single memRealloc()'ed array passed as
** first argument, and the address of which
** is placed in the second argument.
** Arrays to be allocated are described as
** a duplet of ..., &ptr, size, ...,
** terminated by a NULL pointer.
** It returns:
** - !NULL  : pointer to block, all arrays allocated.
** - NULL   : no array allocated.
*/

void *
memReallocGroup (
void *                      oldptr,               /*+ Pointer to block to reallocate +*/
...)
{
  va_list             memlist;                    /* Argument list of the call              */
  byte **             memloc;                     /* Pointer to pointer of current argument */
  size_t              memoff;                     /* Offset value of argument               */
#ifndef PAMPA_DEBUG_MEM
  byte *              blkptr;                     /* Pointer to memory chunk                */
#else /* PAMPA_DEBUG_MEM */
  pointers *          ptrs;
  int                 ptrnbr;

  if (oldptr != NULL) 
    ptrs = *(((pointers **) oldptr) - 1);
  else {
    if ((ptrs = (pointers *) memAlloc (sizeof(pointers))) == NULL) /* If cannot allocate   */
      return (NULL);
    ptrs->ptrnbr = 0;
  }
  ptrnbr = 0;
    //ptrtab = *((int *) ((int **) oldptr - 1));
#endif /* PAMPA_DEBUG_MEM */

  memoff = 0;
  va_start (memlist, oldptr);                     /* Start argument parsing */

  while ((memloc = va_arg (memlist, byte **)) != NULL) { /* As long as not NULL pointer */
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1)); /* Pad      */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes            */
#ifdef PAMPA_DEBUG_MEM
    ptrnbr ++;
#endif /* PAMPA_DEBUG_MEM */
  }

#ifndef PAMPA_DEBUG_MEM
  if ((blkptr = (byte *) memRealloc (oldptr, memoff)) == NULL) { /* If cannot allocate block */
    va_end (memlist);
    return (NULL);
  }
#else /* PAMPA_DEBUG_MEM */
  if ((oldptr == NULL) || ((ptrs->ptrnbr != 0) && (ptrs->ptrnbr != ptrnbr))) {
    ptrs->ptrnbr = ptrnbr;
    if (oldptr != NULL)
      free (ptrs->ptrtab);
    if ((ptrs->ptrtab = (void **) memAlloc (ptrs->ptrnbr * sizeof (void *))) == NULL) { /* If cannot allocate   */
      memFree (ptrs);
      ptrs->ptrtab = NULL;                                                       /* Set first pointer to NULL */
      va_end (memlist);
      return (NULL);
    }
  }
  ptrs->ptrnbr = 0;
#endif /* PAMPA_DEBUG_MEM */

  memoff = 0;
  va_start (memlist, oldptr);                     /* Restart argument parsing           */
  while ((memloc = va_arg (memlist, byte **)) != NULL) { /* As long as not NULL pointer */
#ifndef PAMPA_DEBUG_MEM
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1)); /* Pad      */
    *memloc = blkptr + memoff;                    /* Set argument address               */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes            */
#else /* PAMPA_DEBUG_MEM */
    memoff = va_arg (memlist, size_t);           /* Get padded sizes                    */
    if (ptrs->ptrnbr == 0) {
      if (oldptr != NULL)
        *memloc -= sizeof (int *);
      memoff += sizeof (int *);
    }
    if ((*memloc = (byte *) memRealloc (*memloc, memoff)) == NULL) { /* If cannot allocate block */
      memFree (*memloc);
      va_end (memlist);
      return (NULL);
    }
    ptrs->ptrtab[ptrs->ptrnbr] = (void *) *memloc;
    if (ptrs->ptrnbr == 0) {
      *((pointers **) (*memloc)) = ptrs;
      *memloc += sizeof (pointers *);
    }
    ptrs->ptrnbr ++;
#endif /* PAMPA_DEBUG_MEM */
  }

#ifndef PAMPA_DEBUG_MEM
  return ((void *) blkptr);
#else /* PAMPA_DEBUG_MEM */
  return ((void *) 0x1);
#endif /* PAMPA_DEBUG_MEM */
}
#endif /* COMMON_MEMORY_TRACE */

/* This routine computes the offsets of arrays
** of given sizes and types with respect to a
** given base address passed as first argument.
** Arrays the offsets of which are to be computed
** are described as a duplet of ..., &ptr, size, ...,
** terminated by a NULL pointer.
** It returns:
** - !NULL  : in all cases, pointer to the end of
**            the memory area.
*/

void *
memOffset (
void *                      memptr,               /*+ Pointer to base address of memory area +*/
...)
{
  va_list             memlist;                    /* Argument list of the call              */
  byte **             memloc;                     /* Pointer to pointer of current argument */
  size_t              memoff;                     /* Offset value of argument               */

  memoff = 0;
  va_start (memlist, memptr);                     /* Start argument parsing */

  while ((memloc = va_arg (memlist, byte **)) != NULL) { /* As long as not NULL pointer */
    memoff  = (memoff + (sizeof (double) - 1)) & (~ (sizeof (double) - 1));
    *memloc = (byte *) memptr + memoff;           /* Set argument address    */
    memoff += va_arg (memlist, size_t);           /* Accumulate padded sizes */
  }

  return ((void *) ((byte *) memptr + memoff));
}
