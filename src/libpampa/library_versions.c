/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_versions.c
//!
//!   \authors     Cedric Lachat
//!
//!   \brief       This module is the API of the libpampa
//!                library. 
//!
//!   \date        Version 1.0: from: 21 Jul 2015
//!                             to:   20 Jul 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
 ** The defines and includes.
 */

#define LIBRARY
#include "common.h"
#include "module.h"
#ifdef SCM_VERSIONS
#include "version.h"
#include "versions_ext.h"
#endif /* SCM_VERSIONS */
#include "pampa.h"
#ifdef PTSCOTCH
#include "scotch.h"
#endif /* PTSCOTCH */

//! \brief This routine prints the external library versions as Scotch and also the
//! PaMPA version

void
PAMPA_printVersions ()
{
#ifdef SCM_VERSIONS
  printf ("PAMPA: built at %s, %d.%d.%d under %s with revision %s in folder %s (%s files not commited)\n", PAMPA_DATE, PAMPA_VERSION, PAMPA_RELEASE, PAMPA_PATCHLEVEL, PAMPA_SCM, PAMPA_REV, PAMPA_PATH, PAMPA_NOTC);
#ifdef PTSCOTCH
  printf ("PTSCOTCH: %d.%d.%d under %s with revision %s in folder %s (%s files changed after last building, %s files not commited)\n", SCOTCH_VERSION, SCOTCH_RELEASE, SCOTCH_PATCHLEVEL, PTSCOTCH_SCM, PTSCOTCH_REV, PTSCOTCH_PATH, PTSCOTCH_CHGD, PTSCOTCH_NOTC);
#endif /* PTSCOTCH */
#else /* SCM_VERSIONS */
  printf ("No version is available\n");
#endif /* SCM_VERSIONS */
}
