
/*
 ** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
 */

#define MDMESH

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "dsmesh.h"
#include "mdmesh.h"
#include "mdmesh_mesh_levels.h"
#include "pampa.h"

/*************************************/
/*                                   */
/* These routines handle multi level */
/* distributed mesh meshs.           */
/*                                   */
/*************************************/

//! This routine XXX
//!
//! \returns 0   : on success.
//! \returns !0  : on error.

int
mdmeshMeshLevels (
    Dmesh * restrict const      meshcurptr,
    Dmesh * restrict const      mesholdptr,
    Gnum * restrict const       ventloctax,
    Gnum * restrict const       vnumgsttax,
    const Gnum * const          enttglbtax) /* Concordance table on entities between two levels */
{
  Gnum * restrict vprmcurloctax;
  //Gnum * restrict vprmoldloctax;
  //Gnum * restrict vnbrloctax;
  MdmeshMeshLvlsVertLink * restrict vertcuroldtab;
  Gnum vertcuroldnbr;
  DmeshEntity * restrict enttcurlocptr;
  DmeshEntity * restrict enttoldlocptr;
  Gnum baseval;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum voldlocmax;
  Gnum vcurlocmax;
  const Gnum vertlocbas = mesholdptr->procvrttab[mesholdptr->proclocnum] - mesholdptr->baseval;
  const Gnum vertlocmin = mesholdptr->procvrttab[mesholdptr->proclocnum];
  const Gnum vertlocmax = mesholdptr->procvrttab[mesholdptr->proclocnum + 1] - 1;
  int cheklocval;
  MPI_Datatype vprmventtyp;

  if (mesholdptr->ovlpglbval > 0) {
    MPI_Type_contiguous(2, GNUM_MPI, &vprmventtyp);
    MPI_Type_commit(&vprmventtyp);
  }

  baseval = mesholdptr->baseval;
  cheklocval = 0;

  if (mesholdptr->medgloctax == NULL) {
    if ((mesholdptr->medgloctax = (Gnum *) memAlloc (mesholdptr->vtrulocnbr * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, mesholdptr->proccomm);
    mesholdptr->medgloctax -= baseval;

    mesholdptr->medglocnbr = 0;
    mesholdptr->medglocsiz = mesholdptr->vtrulocnbr;
  }

  if (meshcurptr->medgloctax == NULL) {
    if ((meshcurptr->medgloctax = (Gnum *) memAlloc ((mesholdptr->vtrulocnbr + meshcurptr->vtrulocnbr) * sizeof (Gnum))) == NULL) {
      errorPrint  ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshcurptr->proccomm);
    meshcurptr->medgloctax -= baseval;

    meshcurptr->medglocnbr = 0;
    meshcurptr->medglocsiz = meshcurptr->vtrulocnbr + mesholdptr->vtrulocnbr;
  }
  else if (meshcurptr->medglocsiz < (mesholdptr->vtrulocnbr + meshcurptr->vtrulocnbr)) {
    meshcurptr->medglocsiz = meshcurptr->vtrulocnbr + mesholdptr->vtrulocnbr;
    if ((meshcurptr->medgloctax = (Gnum *) memRealloc(meshcurptr->medgloctax + baseval,
            meshcurptr->medglocsiz * sizeof(Gnum))) == NULL) {
      errorPrint ("out of memory");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, meshcurptr->proccomm);
    meshcurptr->medgloctax -= baseval;
  }

  for (voldlocmax = 0, enttnum = baseval, enttnnd = baseval + mesholdptr->enttglbnbr; enttnum < enttnnd; enttnum ++) {
    // * remplir avec vnumgsttax ngbuloctax et ngblloctax des deux niveaux de dmesh
    if ((mesholdptr->esublocbax[enttnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (mesholdptr->enttloctax[enttnum] == NULL)) /* If enttnum is a sub-entity and not empty */
      continue;

    voldlocmax += MAX (mesholdptr->enttloctax[enttnum]->vovplocnbr, mesholdptr->enttloctax[enttnum]->vertlocnbr);
  }

  for (vcurlocmax = 0, enttnum = baseval, enttnnd = baseval + meshcurptr->enttglbnbr; enttnum < enttnnd; enttnum ++) {
    // * remplir avec vnumgsttax ngbuloctax et ngblloctax des deux niveaux de dmesh
    if ((meshcurptr->esublocbax[enttnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE) || (meshcurptr->enttloctax[enttnum] == NULL)) /* If enttnum is a sub-entity and not empty */
      continue;

    //vcurlocmax += MAX (meshcurptr->enttloctax[enttnum]->vovplocnbr, meshcurptr->enttloctax[enttnum]->vertlocnbr);
    vcurlocmax = MAX (vcurlocmax, vertlocmax - vertlocmin + 1);
  }


  if (memAllocGroup ((void **) (void *)
        &vprmcurloctax, (size_t) (vcurlocmax * sizeof (Gnum)), // XXX pas bien, normalement vertlocmax doit etre sup à vertlocnbr et temporaire tant que vtrulocnbr existe
        //&vprmoldloctax, (size_t) (mesholdptr->vertlocnbr * sizeof (Gnum)),
        &vertcuroldtab, (size_t) (voldlocmax * sizeof (MdmeshMeshLvlsVertLink)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, mesholdptr->proccomm);

  vprmcurloctax -= baseval;
  //vprmoldloctax -= baseval;


    
  for (enttnum = baseval, enttnnd = baseval + mesholdptr->enttglbnbr; enttnum < enttnnd; enttnum ++) {
    Gnum enttcurnum;
    // * remplir avec vnumgsttax ngbuloctax et ngblloctax des deux niveaux de dmesh
    if (mesholdptr->esublocbax[enttnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) /* If enttnum is a sub-entity */
      continue;

    memSet (vprmcurloctax + baseval, ~0, vcurlocmax * sizeof (Gnum)); // XXX à quoi cela sert ? Pour un test en debug ?


    enttoldlocptr = mesholdptr->enttloctax[enttnum];

    if (enttoldlocptr == NULL)
      continue;

    CHECK_FERR (dmeshItBuild (mesholdptr, enttnum), mesholdptr->proccomm);
    enttcurnum = (enttglbtax == NULL) ? enttnum : enttglbtax[enttnum];

    if (enttcurnum == ~0)
      continue;

    enttcurlocptr = meshcurptr->enttloctax[enttcurnum];

    if (enttcurlocptr == NULL)
      continue;

    for (vertlocnum = baseval, vertlocnnd = enttcurlocptr->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
      Gnum bvrtlocnum;

      bvrtlocnum = ((meshcurptr->esublocbax[enttcurnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) ? enttcurlocptr->mvrtloctax[enttcurlocptr->svrtloctax[vertlocnum]] : enttcurlocptr->mvrtloctax[vertlocnum]) - vertlocbas;
      vprmcurloctax[bvrtlocnum] = vertlocnum; // XXX problème si PAMPA_LVL_PART_GLOBAL
    }

    //for (enttlocnum = baseval, enttlocnnd = mesholdptr->enttglbnbr + baseval; enttlocnum < enttlocnnd; enttlocnum ++) {
    //memSet (vprmoldloctax + baseval, ~0, mesholdptr->vertlocnbr * sizeof (Gnum));
    //if (mesholdptr->enttloctax[enttlocnum] != NULL) {
    for (vertcuroldnbr = 0, vertlocnum = baseval, vertlocnnd = enttoldlocptr->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) { //vertlocnbr ou vertgstnbr ??
      Gnum bvrtlocnum;

      bvrtlocnum = ((mesholdptr->esublocbax[enttcurnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) ? enttoldlocptr->mvrtloctax[enttoldlocptr->svrtloctax[vertlocnum]] : enttoldlocptr->mvrtloctax[vertlocnum]) - vertlocbas;
      if ((vnumgsttax[bvrtlocnum] == ~0) || (vnumgsttax[bvrtlocnum] < vertlocmin) || (vnumgsttax[bvrtlocnum] > vertlocmax))
        continue;
      //vprmoldloctax[bvrtlocnum] = vertlocnum;
      vertcuroldtab[vertcuroldnbr].vertcur = vprmcurloctax[vnumgsttax[bvrtlocnum] - vertlocbas];
      vertcuroldtab[vertcuroldnbr].enttold = ventloctax[vnumgsttax[bvrtlocnum] - vertlocbas]; // XXX Et pour les sommets du recouvrement ?
      vertcuroldtab[vertcuroldnbr ++].vertold = vertlocnum; // XXX problème si PAMPA_LVL_PART_GLOBAL
    }

    if (mesholdptr->ovlpglbval > 0) {
      PAMPA_Iterator it;
      PAMPA_dmeshItInitStart((PAMPA_Dmesh *) mesholdptr, enttcurnum, PAMPA_VERT_BOUNDARY, &it);
      MdmeshMeshLvlsVprmVent * vprmventloctax;

      CHECK_FERR (dmeshValueLink (mesholdptr, (void **) &vprmventloctax, 0, NULL, NULL, vprmventtyp, enttcurnum, -21), mesholdptr->proccomm); // XXX remplacer -21 par une macro définie
      vprmventloctax -= baseval;

      memSet (vprmventloctax + baseval, ~0, enttoldlocptr->vertlocnbr * 2 * sizeof (Gnum));
      while (PAMPA_itHasMore(&it)) {
        PAMPA_Num vertlocnum;
        PAMPA_Num bvrtlocnum;

        if (PAMPA_itIsSubEntt(&it))
          vertlocnum = PAMPA_itCurSubEnttVertNum(&it);
        else
          vertlocnum = PAMPA_itCurEnttVertNum(&it);
        bvrtlocnum = PAMPA_itCurMeshVertNum(&it) - vertlocbas;
        if ((vnumgsttax[bvrtlocnum] == ~0) || (vnumgsttax[bvrtlocnum] < vertlocmin) || (vnumgsttax[bvrtlocnum] > vertlocmax)) {
          PAMPA_itNext(&it);
          continue;
        }
        vprmventloctax[vertlocnum].vertnum = vprmcurloctax[vnumgsttax[bvrtlocnum] - vertlocbas];
        vprmventloctax[vertlocnum].enttnum = ventloctax[vnumgsttax[bvrtlocnum] - vertlocbas];
        PAMPA_itNext(&it);
      }

      CHECK_FERR (dmeshHaloSync (mesholdptr, mesholdptr->enttloctax[enttcurnum], vprmventloctax + baseval, vprmventtyp), mesholdptr->proccomm);

      for (vertlocnum = enttoldlocptr->vertlocnbr + baseval, vertlocnnd = enttoldlocptr->vovplocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) { //vertlocnbr ou vertgstnbr ??

        if (vprmventloctax[vertlocnum].vertnum != ~0) {
          vertcuroldtab[vertcuroldnbr].vertcur = vprmventloctax[vertlocnum].vertnum;
          vertcuroldtab[vertcuroldnbr].enttold = vprmventloctax[vertlocnum].enttnum;
          vertcuroldtab[vertcuroldnbr ++].vertold = vertlocnum; // XXX problème si PAMPA_LVL_PART_GLOBAL
        }
      }
    }

    intSort3asc1 (vertcuroldtab, vertcuroldnbr);

    //memSet (vnbrloctax + baseval, 0, mesholdptr->enttglbnbr * sizeof (Gnum));
    //if (baseval == 1)
    //  for (enttidx = baseval, enttnnd = mesholdptr->enttglbnbr + baseval; enttidx < enttnnd; enttidx ++)
    //    vnbrloctax[enttidx] = baseval;

    // XXX allouer ngblloctax et ngbuloctax, et la case pour l'entité en cours
    if (enttoldlocptr->ngbuloctax == NULL) {
      if ((enttoldlocptr->ngbuloctax = (DmeshEnttNghb **) memAlloc (mesholdptr->enttglbnbr * sizeof (DmeshEnttNghb *))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, mesholdptr->proccomm);

      enttoldlocptr->ngbuloctax -= baseval;
      memSet (enttoldlocptr->ngbuloctax + baseval, 0, mesholdptr->enttglbnbr * sizeof (DmeshEnttNghb *));
    }


    if (enttcurlocptr->ngblloctax == NULL) {
      if ((enttcurlocptr->ngblloctax = (DmeshEnttNghb **) memAlloc (meshcurptr->enttglbnbr * sizeof (DmeshEnttNghb *))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, mesholdptr->proccomm);

      enttcurlocptr->ngblloctax -= baseval;
      memSet (enttcurlocptr->ngblloctax + baseval, 0, meshcurptr->enttglbnbr * sizeof (DmeshEnttNghb *));
    }


    if (enttcurlocptr->ngblloctax[enttcurnum] == NULL) {
      if ((enttcurlocptr->ngblloctax[enttcurnum] = (DmeshEnttNghb *) memAlloc (sizeof (DmeshEnttNghb))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, mesholdptr->proccomm);

      if ((enttcurlocptr->ngblloctax[enttcurnum]->vindloctax = (DmeshVertBounds *) memAlloc (enttcurlocptr->vgstlocnbr * sizeof (DmeshVertBounds))) == NULL) { // XXX vertlocnbr ou vgstlocnbr ??
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, mesholdptr->proccomm);

      enttcurlocptr->ngblloctax[enttcurnum]->vindloctax -= baseval;

      memSet (enttcurlocptr->ngblloctax[enttcurnum]->vindloctax + baseval, ~0, enttcurlocptr->vgstlocnbr * sizeof (DmeshVertBounds));
    }

    if (enttoldlocptr->ngbuloctax[enttcurnum] == NULL) {
      if ((enttoldlocptr->ngbuloctax[enttcurnum] = (DmeshEnttNghb *) memAlloc (sizeof (DmeshEnttNghb))) == NULL) {
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, mesholdptr->proccomm);
      // XXX àjouter un drapeau dans les flags de meshcurptr

      if ((enttoldlocptr->ngbuloctax[enttcurnum]->vindloctax = (DmeshVertBounds *) memAlloc (enttoldlocptr->vgstlocnbr * sizeof (DmeshVertBounds))) == NULL) { // XXX vertlocnbr ou vgstlocnbr ??
        errorPrint  ("out of memory");
        cheklocval = 1;
      }
      CHECK_VERR (cheklocval, mesholdptr->proccomm);

      enttoldlocptr->ngbuloctax[enttcurnum]->vindloctax -= baseval;

      memSet (enttoldlocptr->ngbuloctax[enttcurnum]->vindloctax + baseval, ~0, enttoldlocptr->vgstlocnbr * sizeof (DmeshVertBounds));
    }


    if (vertcuroldnbr > 0) {
      Gnum vertcuroldidx;
      Gnum vertcuroldid2;
      Gnum medgoldlocidx;
      Gnum medgcurlocidx;

      enttcurlocptr->ngblloctax[vertcuroldtab[0].enttold]->vindloctax[vertcuroldtab[0].vertcur].vertlocidx = meshcurptr->medglocnbr + baseval;
      for (medgoldlocidx = mesholdptr->medglocnbr + baseval, medgcurlocidx = meshcurptr->medglocnbr + baseval, vertcuroldid2 = vertcuroldidx = 0;
          vertcuroldidx < vertcuroldnbr; vertcuroldidx ++) {
        Gnum ventlocnum;

        vertlocnum = vertcuroldtab[vertcuroldidx].vertcur;
        ventlocnum = vertcuroldtab[vertcuroldidx].enttold;
        enttoldlocptr = mesholdptr->enttloctax[ventlocnum];
        if (vertcuroldtab[vertcuroldid2].vertcur != vertcuroldtab[vertcuroldidx].vertcur) {
          enttcurlocptr->ngblloctax[vertcuroldtab[vertcuroldid2].enttold]->vindloctax[vertcuroldtab[vertcuroldid2].vertcur].vendlocidx = medgcurlocidx;
          enttcurlocptr->ngblloctax[ventlocnum]->vindloctax[vertlocnum].vertlocidx = medgcurlocidx;
          vertcuroldid2 = vertcuroldidx;
        }

#ifdef PAMPA_DEBUG_MDMESH2
        if (medgcurlocidx >= meshcurptr->medglocsiz) {
          errorPrint ("internal error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, mesholdptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH2 */
        meshcurptr->medgloctax[medgcurlocidx ++] = vertcuroldtab[vertcuroldidx].vertold;

        enttoldlocptr->ngbuloctax[enttcurnum]->vindloctax[vertcuroldtab[vertcuroldidx].vertold].vertlocidx = medgoldlocidx;
#ifdef PAMPA_DEBUG_MDMESH2
        if (medgoldlocidx >= mesholdptr->medglocsiz) {
          errorPrint ("internal error");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, mesholdptr->proccomm);
#endif /* PAMPA_DEBUG_MDMESH2 */
        mesholdptr->medgloctax[medgoldlocidx ++] = vertcuroldtab[vertcuroldidx].vertcur;
        enttoldlocptr->ngbuloctax[enttcurnum]->vindloctax[vertcuroldtab[vertcuroldidx].vertold].vendlocidx = medgoldlocidx;

      }

      enttcurlocptr->ngblloctax[vertcuroldtab[vertcuroldid2].enttold]->vindloctax[vertcuroldtab[vertcuroldid2].vertcur].vendlocidx = medgcurlocidx;
      meshcurptr->medglocnbr = medgcurlocidx - baseval;
      mesholdptr->medglocnbr = medgoldlocidx - baseval;
    }
  }

  if (mesholdptr->esublocbax != NULL) { /* If there are sub-entities */
 /* Redirect private neighbor data for each sub-entity of verctices or neighbors */
    Gnum ventlocnum;
    Gnum ventlocnnd;

    for (ventlocnum = baseval, ventlocnnd = mesholdptr->enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
      Gnum nentlocnum;
      Gnum nentlocnnd;

      if (mesholdptr->enttloctax[ventlocnum] == NULL)
        continue;

      if (mesholdptr->esublocbax[ventlocnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
        mesholdptr->enttloctax[ventlocnum]->ngbuloctax = mesholdptr->enttloctax[mesholdptr->esublocbax[ventlocnum & mesholdptr->esublocmsk]]->ngbuloctax;
      }

      DmeshEnttNghb * restrict * ngbuloctax;

      ngbuloctax = mesholdptr->enttloctax[ventlocnum]->ngbuloctax;

      if (ngbuloctax == NULL)
        continue;

      for (nentlocnum = baseval, nentlocnnd = mesholdptr->enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++) {
        if (((enttglbtax == NULL) && (ventlocnum != nentlocnum)) || ((enttglbtax != NULL) && (enttglbtax[ventlocnum] != nentlocnum)))
          continue;

        if ((mesholdptr->esublocbax[nentlocnum & mesholdptr->esublocmsk] > PAMPA_ENTT_SINGLE) && (ngbuloctax[mesholdptr->esublocbax[nentlocnum & mesholdptr->esublocmsk]] != NULL)) {
          if (ngbuloctax[nentlocnum] == NULL) {
            if ((ngbuloctax[nentlocnum] = (DmeshEnttNghb *) memAlloc (sizeof (DmeshEnttNghb))) == NULL) {
              errorPrint  ("out of memory");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, mesholdptr->proccomm);
          }

          ngbuloctax[nentlocnum]->vindloctax = ngbuloctax[mesholdptr->esublocbax[nentlocnum & mesholdptr->esublocmsk]]->vindloctax;
        }
      }
    }

    for (ventlocnum = baseval, ventlocnnd = meshcurptr->enttglbnbr + baseval ; ventlocnum < ventlocnnd ; ventlocnum ++) {
      Gnum nentlocnum;
      Gnum nentlocnnd;

      if (meshcurptr->enttloctax[ventlocnum] == NULL)
        continue;

      if (meshcurptr->esublocbax[ventlocnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE) {
        meshcurptr->enttloctax[ventlocnum]->ngblloctax = meshcurptr->enttloctax[meshcurptr->esublocbax[ventlocnum & meshcurptr->esublocmsk]]->ngblloctax;
      }

      DmeshEnttNghb * restrict * ngblloctax;

      ngblloctax = meshcurptr->enttloctax[ventlocnum]->ngblloctax;

      if (ngblloctax == NULL)
        continue;

      for (nentlocnum = baseval, nentlocnnd = meshcurptr->enttglbnbr + baseval ; nentlocnum < nentlocnnd ; nentlocnum ++) {
        if (((enttglbtax == NULL) && (ventlocnum != nentlocnum)) || ((enttglbtax != NULL) && (enttglbtax[nentlocnum] != ventlocnum)))
          continue;

        if ((meshcurptr->esublocbax[nentlocnum & meshcurptr->esublocmsk] > PAMPA_ENTT_SINGLE) && (ngblloctax[meshcurptr->esublocbax[nentlocnum & meshcurptr->esublocmsk]] != NULL)) {
          if (ngblloctax[nentlocnum] == NULL) {
            if ((ngblloctax[nentlocnum] = (DmeshEnttNghb *) memAlloc (sizeof (DmeshEnttNghb))) == NULL) {
              errorPrint  ("out of memory");
              cheklocval = 1;
            }
            CHECK_VERR (cheklocval, meshcurptr->proccomm);
          }

          ngblloctax[nentlocnum]->vindloctax = ngblloctax[meshcurptr->esublocbax[nentlocnum & meshcurptr->esublocmsk]]->vindloctax;
        }
      }
    }
  }


  //XXX on ne peut pas le supprimer car utilisé après...pas bien
  //if (mesholdptr->ovlpglbval > 0) {
  //  MPI_Type_free (&vprmventtyp);
  //}
  memFreeGroup (vprmcurloctax + baseval);
  meshcurptr->flagval |= DMESHFREELVL;
  mesholdptr->flagval |= DMESHFREELVL;

  return (0);
}
