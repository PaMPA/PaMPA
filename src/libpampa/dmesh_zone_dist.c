/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_zone_dist.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 16 May 2017
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_ADAPT
#ifdef PAMPA_TIME_CHECK2
#define PAMPA_TIME_CHECK
#endif /* PAMPA_TIME_CHECK2 */
//#define PAMPA_DEBUG_GRAPH
//#define PAMPA_DEBUG_DMESH_SAVE

#include "module.h"
#include "common.h"
#include "comm.h"
#include "dvalues.h"
#include "dmesh.h"
#include "pampa.h"
#include "pampa.h"
#include <math.h>

#include <ptscotch.h>
#include "dmesh_adapt.h"
#include "dmesh_adapt_common.h"

/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshZoneDist (
    Dmesh * const    meshptr,              /**< mesh */
    PAMPA_AdaptInfo * const infoptr,
    Gnum   const     bvrtlocnbr,
    Gnum * const     bnumgsttax,
    Gnum   const     cvrtlocnbr,
    Gnum   const     cvrtglbnbr,
    Gnum * const     cprcvrttab,
    Gnum * const     cvelloctab,
    Gnum *           parttax)
{
  Gnum baseval;
  Gnum bvrtlocnum;
  Gnum bvrtlocnnd;
  Gnum cvrtlocmin;
  Gnum cvrtlocmax;
  Gnum cvrtlocnum;
  Gnum cvrtlocnnd;
  Gnum cvrtlocbas;
  Gnum * restrict cvelglbtab;
  MPI_Comm proccomm;
  Gnum hashsiz;          /* Size of hash table    */
  Gnum hashnum;          /* Hash value            */
  Gnum hashmax;
  Gnum hashnbr;
  Gnum hashmsk;
  Gnum * restrict hashtab;
  int * restrict dsndcnttab;
  int * restrict dsnddsptab;
  int * restrict drcvcnttab;
  int * restrict drcvdsptab;
  int procngbnum;
  int procngbnnd;
  Gnum datasndnbr;
  Gnum * restrict datasndtab;
  Gnum datarcvnbr;
  Gnum * restrict datarcvtab;
  Gnum vertlocbas;
  Gnum vertlocnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum edgelocnbr;
  Gnum edgelocnum;
  Gnum pvrtlocnum;
  Gnum * restrict vertloctax;
  Gnum * restrict vlblloctax;
  Gnum * restrict veloloctax;
  Gnum * restrict edgeloctax;
  Gnum * restrict edloloctax;
  Gnum * restrict partloctax;
  Gnum * restrict parttx2;
  Gnum * restrict vertsidtab;
  SCOTCH_Dgraph dbgfdat; /* Distributed bigraph */
  int cheklocval;
  const Gnum colrdsp = 2; /* if value is changed, modify the intSort on datasndtab */

  cheklocval = 0;
  baseval = meshptr->baseval;
  proccomm = meshptr->proccomm;
  cvrtlocmin = cprcvrttab[meshptr->proclocnum];
  cvrtlocmax = cprcvrttab[meshptr->proclocnum + 1] - 1;

  hashnbr = cvrtglbnbr / meshptr->procglbnbr;

  for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */
  hashnbr = 0;

  hashmsk = hashsiz - 1;
  hashmax = hashsiz >> 2;

  if ((hashtab = (Gnum *) memAlloc (hashsiz * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  memSet (hashtab, ~0, hashsiz * sizeof (Gnum));

  if (memAllocGroup ((void **) (void *)
          &dsndcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
          &dsnddsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
          &drcvcnttab, (size_t) (meshptr->procglbnbr       * sizeof (int)),
          &drcvdsptab, (size_t) ((meshptr->procglbnbr + 1) * sizeof (int)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  memSet (dsndcnttab, 0, meshptr->procglbnbr * sizeof (int));

  for (procngbnum = meshptr->proclocnum, bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
    Gnum cvrtlocnum;

    cvrtlocnum = bnumgsttax[bvrtlocnum];

    for (hashnum = (cvrtlocnum * DMESHADAPTHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum] != cvrtlocnum; hashnum = (hashnum + 1) & hashmsk) ;

    if (hashtab[hashnum] == cvrtlocnum) /* If vertex already added */
      continue;

    if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
      if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
        int procngbmax;
        for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
            procngbmax - procngbnum > 1; ) {
          int                 procngbmed;

          procngbmed = (procngbmax + procngbnum) / 2;
          if (cprcvrttab[procngbmed] <= cvrtlocnum)
            procngbnum = procngbmed;
          else
            procngbmax = procngbmed;
        }
      }

      dsndcnttab[procngbnum] += colrdsp;
    }

    hashtab[hashnum] = cvrtlocnum;
    hashnbr ++;

    if (hashnbr >= hashmax) { /* If (*hashtab) is too much filled */
      cheklocval = dmeshAdaptVertResize (meshptr, &hashtab, &hashsiz, &hashmax, &hashmsk);
      CHECK_VERR (cheklocval, proccomm);
    }
  }

  /* Compute displacement sending array */
  dsnddsptab[0] = 0;
  for (procngbnum = 1, procngbnnd = meshptr->procglbnbr + 1; procngbnum < procngbnnd; procngbnum ++) 
    dsnddsptab[procngbnum] = dsnddsptab[procngbnum - 1] + dsndcnttab[procngbnum - 1];

  datasndnbr = dsnddsptab[meshptr->procglbnbr];

  //* envoyer dsndcnttab et recevoir drcvcnttab
  CHECK_VDBG (cheklocval, proccomm);
  if (commAlltoall(dsndcnttab, 1, MPI_INT, drcvcnttab, 1, MPI_INT, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);

  //* en déduire ddsp{snd,rcv}tab
  /* Compute displacement receiving array */
  drcvdsptab[0] = 0;
  for (procngbnum = 1 ; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    drcvdsptab[procngbnum] = drcvdsptab[procngbnum - 1] + drcvcnttab[procngbnum - 1];
    dsndcnttab[procngbnum] = dsnddsptab[procngbnum];
  }
  drcvdsptab[meshptr->procglbnbr] = drcvdsptab[meshptr->procglbnbr - 1] + drcvcnttab[meshptr->procglbnbr - 1];
  datarcvnbr = drcvdsptab[meshptr->procglbnbr];
  dsndcnttab[0] = dsnddsptab[0];

  vertlocnbr = cvrtlocnbr + 1; /* Adding processor as a vertex */
  edgelocnbr = hashnbr * 2 + datarcvnbr;
  //* allouer datasndtab et datarcvtab
  //* réinitialiser dsndcnttab à 0 pour l'utiliser comme marqueur d'arret
  if (memAllocGroup ((void **) (void *)
        &cvelglbtab, (size_t) (cvrtglbnbr * sizeof (Gnum)),
        &datasndtab, (size_t) (datasndnbr * sizeof (Gnum)),
        &datarcvtab, (size_t) (datarcvnbr * sizeof (Gnum)),
        &vertloctax, (size_t) ((vertlocnbr + 1) * sizeof (Gnum)),
        &vlblloctax, (size_t) (MAX(vertlocnbr, 1) * sizeof (Gnum)),
        &veloloctax, (size_t) (MAX(vertlocnbr, 1) * sizeof (Gnum)),
        &edgeloctax, (size_t) (MAX(edgelocnbr, 1) * sizeof (Gnum)),
        &edloloctax, (size_t) (MAX(edgelocnbr, 1) * sizeof (Gnum)), // Against Sctoch replacement by NULL if edloloctax == edgeloctax
        &partloctax, (size_t) (vertlocnbr * sizeof (Gnum)),
        &parttx2, (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
        &vertsidtab, (size_t) (meshptr->procglbnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  vertloctax -= baseval;
  vlblloctax -= baseval;
  veloloctax -= baseval;
  edgeloctax -= baseval;
  edloloctax -= baseval;
  partloctax -= baseval;
  parttx2 -= baseval;

  memSet (cvelglbtab, 0, cvrtglbnbr * sizeof (Gnum));
  memSet (parttx2 + baseval, 0, meshptr->procglbnbr * sizeof (Gnum));
  memSet (veloloctax + baseval, 0, (MAX(vertlocnbr, 1) * sizeof (Gnum)));
  memSet (edloloctax + baseval, 0, (MAX(edgelocnbr, 1) * sizeof (Gnum)));

  CHECK_FMPI (cheklocval, commAllreduce (cvelloctab, cvelglbtab, cvrtglbnbr, GNUM_MPI, MPI_SUM, meshptr->proccomm), meshptr->proccomm);

  for (cvrtlocbas = cprcvrttab[meshptr->proclocnum] + baseval, procngbnum = meshptr->proclocnum, hashnum = 0; hashnum < hashsiz; hashnum ++)
    if (hashtab[hashnum] != ~0) {
      Gnum cvrtlocnum;

      cvrtlocnum = hashtab[hashnum];
      //infoPrint("couleur: %d", cvrtlocnum);

      if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
        if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
          int procngbmax;
          for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              procngbmax - procngbnum > 1; ) {
            int                 procngbmed;

            procngbmed = (procngbmax + procngbnum) / 2;
            if (cprcvrttab[procngbmed] <= cvrtlocnum)
              procngbnum = procngbmed;
            else
              procngbmax = procngbmed;
          }
          cvrtlocbas = cprcvrttab[procngbnum] + baseval;
        }
        datasndtab[dsndcnttab[procngbnum] ++] = cvrtlocnum - cvrtlocbas;
        datasndtab[dsndcnttab[procngbnum] ++] = cvelloctab[cvrtlocnum];
      }
    }

  for (procngbnum = 0 ; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    dsndcnttab[procngbnum] -= dsnddsptab[procngbnum];
    intSort2asc1 (datasndtab + dsnddsptab[procngbnum], dsndcnttab[procngbnum] / colrdsp);
  }

  //* envoyer datasndtab et recevoir datarcvtab
  CHECK_VDBG (cheklocval, proccomm);
  if (commAlltoallv(datasndtab, dsndcnttab, dsnddsptab, GNUM_MPI, datarcvtab, drcvcnttab, drcvdsptab, GNUM_MPI, proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);


  for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++)
    vertsidtab[procngbnum] = drcvdsptab[procngbnum];

  /* Adding processor as a vertex and their neighbors */
  // XXX on pourrait se passer de baseval, vu que cvrtlocnum commence à 0
  vertlocnum = baseval;
  pvrtlocnum = baseval + cvrtglbnbr + meshptr->proclocnum; /* Vertex number of processor */
  vlblloctax[vertlocnum] = pvrtlocnum;
  veloloctax[vertlocnum] = 1;
  vertloctax[vertlocnum ++] = baseval;
  vertlocbas = meshptr->proclocnum - baseval;
  for (procngbnum = meshptr->proclocnum, edgelocnum = baseval, hashnum = 0; hashnum < hashsiz; hashnum ++)
    if (hashtab[hashnum] != ~0) {
      Gnum cvrtlocnum;
      cvrtlocnum = hashtab[hashnum];

      edloloctax[edgelocnum] = cvelloctab[cvrtlocnum];
      if ((cvrtlocnum < cvrtlocmin) || (cvrtlocnum > cvrtlocmax)) {
        if (!((cprcvrttab[procngbnum] <= cvrtlocnum) && (cprcvrttab[procngbnum+1] > cvrtlocnum))) {
          int procngbmax;
          for (procngbnum = 0, procngbmax = meshptr->procglbnbr;
              procngbmax - procngbnum > 1; ) {
            int                 procngbmed;

            procngbmed = (procngbmax + procngbnum) / 2;
            if (cprcvrttab[procngbmed] <= cvrtlocnum)
              procngbnum = procngbmed;
            else
              procngbmax = procngbmed;
          }
        }
        edgeloctax[edgelocnum ++] = cvrtlocnum + baseval;
      }
      else 
        edgeloctax[edgelocnum ++] = cvrtlocnum + baseval;
    }

  for (cvrtlocnum = 0, cvrtlocnnd = cvrtlocnbr; cvrtlocnum < cvrtlocnnd; cvrtlocnum ++) {
    Gnum cvrtglbnum;

    cvrtglbnum = cvrtlocnum + cprcvrttab[meshptr->proclocnum];
    veloloctax[vertlocnum] = cvelglbtab[cvrtglbnum];
    vlblloctax[vertlocnum] = cvrtglbnum;
    vertloctax[vertlocnum ++] = edgelocnum;

    for (hashnum = (cvrtglbnum * DMESHADAPTHASHPRIME) & hashmsk; hashtab[hashnum] != ~0 && hashtab[hashnum] != cvrtglbnum; hashnum = (hashnum + 1) & hashmsk) ;

    if (hashtab[hashnum] == cvrtglbnum) { /* If color added */
      edloloctax[edgelocnum] = cvelloctab[cvrtglbnum];
      edgeloctax[edgelocnum ++] = pvrtlocnum;
    }

    for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++)
      if ((procngbnum != meshptr->proclocnum) && (vertsidtab[procngbnum] < drcvdsptab[procngbnum + 1])
          && (datarcvtab[vertsidtab[procngbnum]] == cvrtlocnum)) {
        Gnum pvrtlocnm2;

        pvrtlocnm2 = baseval + cvrtglbnbr + procngbnum;
        edloloctax[edgelocnum] = datarcvtab[vertsidtab[procngbnum] + 1];
        //veloloctax[cvrtlocnum + 1] += edloloctax[edgelocnum];
        edgeloctax[edgelocnum ++] = pvrtlocnm2;
        vertsidtab[procngbnum] += colrdsp;
      }
  }
  vertloctax[vertlocnum] = edgelocnum;
  edgelocnbr = edgelocnum - baseval;


  CHECK_FERR (SCOTCH_dgraphInit (&dbgfdat, proccomm), proccomm);
  CHECK_FERR (SCOTCH_dgraphBuild (&dbgfdat, baseval, vertlocnbr, vertlocnbr, vertloctax + baseval, NULL, veloloctax + baseval, vlblloctax + baseval, edgelocnbr, edgelocnbr, edgeloctax + baseval, NULL, edloloctax + baseval), proccomm); // XXX doit-on mettre edloloctab ? Si oui, à quoi est-ce qu'il correspond ?

  memFree (hashtab);
#define PAMPA_DEBUG_ADAPT2
#ifdef PAMPA_DEBUG_ADAPT2
  CHECK_FERR (SCOTCH_dgraphCheck (&dbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
#undef PAMPA_DEBUG_ADAPT2
#ifdef PAMPA_DEBUG_DGRAPH
  {
    char s[30];
    FILE *dgraph_file;
    sprintf(s, "dbgf-adapt-%d-%d", infoptr->iitenum, meshptr->proclocnum);
    dgraph_file = fopen(s, "w");
    SCOTCH_dgraphSave (&dbgfdat, dgraph_file);
    fclose(dgraph_file);
  }
#endif /* PAMPA_DEBUG_DGRAPH */
  memSet (partloctax + baseval, ~0, vertlocnbr * sizeof (Gnum));
  partloctax[baseval] = meshptr->proclocnum;

  if (meshptr->proclocnum == 0) {
    Gnum vertnum;
    Gnum vertnm2;
    Gnum procnil;
    Gnum edgenum;
    Gnum vertnnd;
    Gnum edgennd;
    Gnum   vertnbr;
    Gnum   edgenbr;
    double loadglbmax;
    double loadglbmx2;
    Gnum * verttax;
    Gnum * vendtax;
    Gnum * velotax;
    Gnum * edgetax;
    Gnum * edlotax;
    Gnum * restrict vlbltax;
    SCOTCH_Graph  cbgfdat; /* Centralized bigraph */
    SCOTCH_Strat  strtdat; 

    CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, &cbgfdat), proccomm);
    // XXX temporaire remplace partitionnement 
    SCOTCH_graphData(&cbgfdat, NULL, &vertnbr, &verttax, &vendtax, &velotax, &vlbltax, &edgenbr, &edgetax, &edlotax);
    verttax -= baseval;
    vendtax -= baseval;
    velotax -= baseval;
    vlbltax -= baseval;
    edgetax -= baseval;
    edlotax -= baseval;
    // XXX fin temporaire
#ifdef PAMPA_DEBUG_ADAPT2
    CHECK_FERR (SCOTCH_graphCheck (&cbgfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

    //if ((veloglbtax = (VertLoad *) memAlloc (edgenbr * sizeof (VertLoad))) == NULL) {
    //  errorPrint  ("out of memory");
    //  cheklocval = 1;
    //}
    //CHECK_VERR (cheklocval, proccomm);
    //veloglbtax -= baseval;

    drcvdsptab[0] = 0;
    for (procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) {
      drcvcnttab[procngbnum] = (cprcvrttab[procngbnum + 1] - cprcvrttab[procngbnum]) + 1;
      drcvdsptab[procngbnum + 1] = drcvdsptab[procngbnum] + drcvcnttab[procngbnum];
    }

    if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
          parttax + baseval, drcvcnttab, drcvdsptab, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    //#define PAMPA_DEBUG_GRAPH
#ifdef PAMPA_DEBUG_GRAPH
    {
      char s[30];
      FILE *graph_file;
      sprintf(s, "cbgf-adapt-%d.grf", infoptr->iitenum);
      graph_file = fopen(s, "w");
      SCOTCH_graphSave (&cbgfdat, graph_file);
      fclose(graph_file);
      sprintf(s, "cbgf-adapt-%d.txt", infoptr->iitenum);
      graph_file = fopen(s, "w");
      for (vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++)
        fprintf (graph_file, "%d ", (int) parttax[vertnum]);
      fclose(graph_file);

    }
#endif /* PAMPA_DEBUG_GRAPH */

    CHECK_FDBG2 (SCOTCH_stratInit (&strtdat));
    CHECK_FDBG2 (SCOTCH_graphPartFixed (&cbgfdat, meshptr->procglbnbr, &strtdat, parttax + baseval));
    //for (vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++)
    //  printf ("parttax: %d ", (int) parttax[vertnum]);
    SCOTCH_stratExit (&strtdat);

    //for (loadglbmax = 0., vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
    //  if (vlbltax[vertnum] < (cvrtglbnbr + baseval)) 
    //    loadglbmax += velotax[vertnum];
    //  for (edgenum = verttax[vertnum], edgennd = vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
    //    veloglbtax[edgenum].vertnum = vertnum;
    //    veloglbtax[edgenum].loadval = edlotax[edgenum];
    //    veloglbtax[edgenum].edgenum = edgetax[edgenum];
    //  }
    //}
    //loadglbmax /= meshptr->procglbnbr;
    //loadglbmx2 = loadglbmax * 2;

    ////for (partlocidx = procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    ////  Gnum partlocid2;
    ////  Gnum partlocnnd;
    ////  for (partlocid2 = drcvdsptab[procngbnum] + 1, partlocnnd = drcvdsptab[procngbnum + 1];
    ////	  partlocid2 < partlocnnd; partlocid2 ++, partlocidx ++) {
    ////  	//parttax[partlocidx] = parttax[partlocid2];
    ////  	veloglbtax[partlocidx].loadval = velotax[partlocid2];
    ////        veloglbtax[partlocidx].vertnum = partlocidx + baseval;
    ////  }
    ////}
    //memSet (parttax + baseval, ~0, vertnbr * sizeof (Gnum));
    //intSort3asc2 (veloglbtax + baseval, edgenbr);
    //procnil = meshptr->procglbnbr;
    //for (vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
    //  if (vlbltax[vertnum] < (cvrtglbnbr + baseval)) {
    //    double loadtmp = loadglbmx2;
    //    Gnum   proctmp = ~0;

    //    cvrtlocnum = vlbltax[vertnum];

    //    for (edgenum = verttax[vertnum], edgennd = vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
    //      procngbnum = vlbltax[veloglbtax[edgenum].edgenum] - cvrtglbnbr;
    //      if ((parttx2[procngbnum] + veloglbtax[edgenum].loadval < loadglbmax) && (parttx2[procngbnum] < loadtmp)) {
    //        loadtmp = parttx2[procngbnum];
    //        proctmp = procngbnum;
    //      }
    //    }

    //    if (proctmp != ~0) {
    //      if (parttx2[proctmp] == 0)
    //        procnil --;
    //      else if (procnil != 0) {
    //        for (proctmp = 0; proctmp < meshptr->procglbnbr; proctmp ++) 
    //          if (parttx2[proctmp] == 0) {
    //            procnil --;
    //            parttax[cvrtlocnum] = proctmp;
    //            parttx2[proctmp] += velotax[vertnum];
    //            proctmp = ~0;
    //            break;
    //          }
    //      }
    //      if (proctmp != ~0) {
    //        parttax[cvrtlocnum] = proctmp;
    //        parttx2[proctmp] += velotax[vertnum];
    //      }
    //    }

    //    if (parttax[cvrtlocnum] == ~0) {
    //      for (edgenum = verttax[vertnum], edgennd = vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
    //        procngbnum = vlbltax[veloglbtax[edgenum].edgenum] - cvrtglbnbr;
    //        if ((parttx2[procngbnum] + veloglbtax[edgenum].loadval < loadglbmx2) && (parttx2[procngbnum] < loadtmp)) {
    //          loadtmp = parttx2[procngbnum];
    //          proctmp = procngbnum;
    //      }
    //    }

    //      if (proctmp != ~0) {
    //        if (parttx2[proctmp] == 0)
    //          procnil --;
    //        else if (procnil != 0) {
    //          for (proctmp = 0; proctmp < meshptr->procglbnbr; proctmp ++) 
    //            if (parttx2[proctmp] == 0) {
    //              procnil --;
    //              parttax[cvrtlocnum] = proctmp;
    //              parttx2[proctmp] += velotax[vertnum];
    //              proctmp = ~0;
    //              break;
    //            }
    //        }
    //        if (proctmp != ~0) {
    //          parttax[cvrtlocnum] = proctmp;
    //          parttx2[proctmp] += velotax[vertnum];
    //        }
    //      }
    //    }

    //    if (parttax[cvrtlocnum] == ~0) {
    //      Gnum procngbval;
    //      Gnum procngbind;
    //      for (procngbval = INTVALMAX, procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    //        if (parttx2[procngbnum] < procngbval) {
    //          procngbval = parttx2[procngbnum];
    //          procngbind = procngbnum;
    //        }
    //      }
    //      parttax[cvrtlocnum] = procngbind;
    //      parttx2[procngbind] += velotax[vertnum];
    //    }
    //  }

    //memFree (veloglbtax + baseval);

    //for (cvrtlocnum = cvrtglbnbr + baseval - 1, cvrtlocnnd = baseval - 1; cvrtlocnum > cvrtlocnnd; cvrtlocnum --) {
    //  Gnum procngbval;
    //  Gnum procngbind;

    //  for (procngbval = INTVALMAX, procngbnum = 0; procngbnum < meshptr->procglbnbr; procngbnum ++) {
    //    if (parttx2[procngbnum] < procngbval) {
    //      procngbval = parttx2[procngbnum];
    //      procngbind = procngbnum;
    //    }
    //    if (parttx2[procngbnum] == 0)
    //      break;
    //  }
    //  parttax[veloglbtax[cvrtlocnum].vertnum] = procngbind;
    //  parttx2[procngbind] += veloglbtax[cvrtlocnum].loadval;
    //}
#define PAMPA_INFO_ADAPT_ZONES
#ifdef PAMPA_INFO_ADAPT_ZONES
    {

      char s[30];
      sprintf(s, "zones-%d", infoptr->iitenum);
      FILE *graph_file;
      graph_file = fopen(s, "w");
      fprintf (graph_file, "digraph G {\n");
      fprintf (graph_file, "\tranksep=3;\n");
      fprintf (graph_file, "\tratio=auto;\n");
      //fprintf (graph_file, "\trankdir=LR\n");
      //fprintf (graph_file, "\tsubgraph cluster_1 {\n");
      for (vertnum = baseval, vertnnd = cvrtglbnbr + baseval; vertnum < vertnnd; vertnum ++)
        fprintf (graph_file, "\t\tnode [label = \"%d\"] \"%d\";\n", vertnum, vertnum);
      //fprintf (graph_file, "\tedge [style=\"invis\"];\n");
      //fprintf (graph_file, "\t%d", baseval);
      //for (vertnum = baseval + 1, vertnnd = cvrtglbnbr + baseval; vertnum < vertnnd; vertnum ++)
      //  fprintf (graph_file, " -> %d", vertnum);
      //fprintf (graph_file, " -> %d [style=\"invis\"];\n", baseval);
      //fprintf (graph_file, "\t}\n");

      //fprintf (graph_file, "\tsubgraph cluster_2 {\n");
      for (vertnum = 0, vertnnd = meshptr->procglbnbr; vertnum < vertnnd; vertnum ++)
        fprintf (graph_file, "\t\tnode [label = \"p%d\"] \"%d\";\n", vertnum, vertnum + cvrtglbnbr);
      //fprintf (graph_file, "\tedge [style=\"invis\"];\n");
      //fprintf (graph_file, "\t%d", cvrtglbnbr);
      //for (vertnum = 1, vertnnd = meshptr->procglbnbr; vertnum < vertnnd; vertnum ++)
      //  fprintf (graph_file, " -> %d", vertnum + cvrtglbnbr);
      //fprintf (graph_file, " -> %d [style=\"invis\"];\n", cvrtglbnbr);
      //fprintf (graph_file, "\t}\n");

      for (vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
        if (vlbltax[vertnum] < (cvrtglbnbr + baseval)) {
          for (edgenum = verttax[vertnum], edgennd = vendtax[vertnum]; edgenum < edgennd; edgenum ++) {
            if ((parttax[vertnum] + cvrtglbnbr) == vlbltax[edgetax[edgenum]]) {
              parttax[vertnum] = -parttax[vertnum] - 1;
            }
            fprintf(graph_file, "%d -> %d [penwidth=\"%f\"];\n", vlbltax[vertnum], vlbltax[edgetax[edgenum]], 5.0 * edlotax[edgenum] / velotax[vertnum]);
          }
        }
      //fprintf(graph_file, "edge;\n");
      for (vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
        if (vlbltax[vertnum] < (cvrtglbnbr + baseval)) {
          cvrtlocnum = vlbltax[vertnum];
          if (parttax[vertnum] < 0) {
            parttax[vertnum] = - parttax[vertnum] - 1;
            fprintf(graph_file, "%d -> %d [color=green];\n", cvrtlocnum, parttax[vertnum] + cvrtglbnbr);
          }
          else
            fprintf(graph_file, "%d -> %d [color=red];\n", cvrtlocnum, parttax[vertnum] + cvrtglbnbr);
        }
      fprintf(graph_file, "}\n");
      fclose(graph_file);
    }
#endif /* PAMPA_INFO_ADAPT_ZONES */

    for (vertnum = vertnm2 = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
      if (vlbltax[vertnum] < (cvrtglbnbr + baseval)) 
        parttax[vertnm2 ++] = parttax[vertnum];

    SCOTCH_graphExit (&cbgfdat);




    if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    //if (commScatterv (parttax + baseval, prcvcnttab, prcvdsptab, GNUM_MPI,
    //    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
    //	errorPrint ("communication error");
    //	return     (1);
    //}
  }
  else {
    CHECK_FERR (SCOTCH_dgraphGather (&dbgfdat, NULL), proccomm); /* For graphPartFixed */

    if (commGatherv (partloctax + baseval, vertlocnbr, GNUM_MPI,
          NULL, NULL, NULL, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);

    if (commBcast (parttax + baseval, cvrtglbnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
      cheklocval = 1;
    }
    CHECK_VERR (cheklocval, proccomm);
    //if (commScatterv (NULL, NULL, NULL, GNUM_MPI,
    //    	partloctax + baseval, vertlocnbr, GNUM_MPI, 0, proccomm) != MPI_SUCCESS) {
    //	errorPrint ("communication error");
    //	return     (1);
    //}
  }
  CHECK_VERR (cheklocval, proccomm);
  SCOTCH_dgraphExit (&dbgfdat);

  memFreeGroup (dsndcnttab);
  memFreeGroup (cvelglbtab);

  CHECK_VDBG (cheklocval, proccomm);
  return (0);
}
