/*  Copyright 2009-2016 Inria
**
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
** 
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_halo.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!   \brief       This file contains the data declarations
//!                for the halo exchange routine. 
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   13 Apr 2016
//!                Version 2.0: from:  7 Oct 2016
//!                             to:   12 May 2017
//!
/************************************************************/

/*
** The defines.
*/
// XXX temporaire à changer
//#define PAMPA_PTHREAD
// XXX fin temporaire

/* procsidtab-related values. */

#define DMESHGHSTSIDMAX            ((int) ((unsigned int) (1 << (sizeof (int) * 8 - 1)) - 2U)) //!< Maximum leap value for procsidtab entries

/*
** The type and structure definitions.
*/

/* Sort structure for ghost edges. */

//! \todo modifier la structure en fonction de ce qu'on aura besoin
typedef struct DmeshHaloRequest_ {
//#ifdef PAMPA_PTHREAD
  void *                    attrgsttab;           //!< Attribute array to share
  MPI_Datatype              attrglbtype;          //!< Attribute datatype
  Gnum                      enttnum;
  Gnum                      tagnum;               //!< Be careful, it's PaMPA tag NOT MPI tag
  DmeshEntity const *       enttlocptr;           //!< Pointer on entity private data
//#else /* PAMPA_PTHREAD */
  byte *                    attrsndtab;           //!< Group leader for memory freeing
  byte **                   attrsndptr;           //!< Group leader for memory freeing
//#endif /* PAMPA_PTHREAD */
} DmeshHaloRequest;

typedef struct DmeshHaloRequests_ {
  int                       flagval;
  int                       preqnbr;
  DmeshHaloRequest *        preqtab;              //!< PaMPA request array
  Dmesh *                   meshptr;              //!< Pointer to mesh data
  MPI_Request *             mreqtab;              //!< MPI asynchronous communication request array
  int                       mreqnbr;
#ifdef PAMPA_PTHREAD
  pthread_t                 thrdval;              //!< Data of asynchronous thread
#else /* PAMPA_PTHREAD */
#ifdef PAMPA_MPI_ASYNC_COLL
#endif /* PAMPA_MPI_ASYNC_COLL */
#endif /* PAMPA_PTHREAD */
} DmeshHaloRequests;

/*
** The function prototypes.
*/

void dmeshHaloMultAsync (
    Dmesh * restrict const       meshptr,
    Gnum const                   valunbr,
    Gnum const * restrict const  entttab,
    Gnum const * restrict const  tagtab,
    DmeshHaloRequests * restrict reqsptr);

int dmeshHaloMultSync (
    Dmesh * restrict const       meshptr,
    Gnum const                   valunbr,
    Gnum const * restrict const  entttab,
    Gnum const * restrict const  tagtab,
    DmeshHaloRequests * const    reqsptr);

#ifndef DMESH_HALO
#define static
#endif

#ifdef PAMPA_PTHREAD
static void * dmeshHaloAsync2 (
    DmeshHaloRequests * restrict         requptr);
#endif /* PAMPA_PTHREAD */

static
int
dmeshHaloSync3 (
    Dmesh * restrict const       meshptr,
	const DmeshEnttComm * restrict const commlocptr,
	const Gnum                    vertlocnbr,
    void * restrict const         attrgsttab, 
    const MPI_Datatype            attrglbtype,
    byte ** const                 attrsndptr, 
    int ** const                  senddspptr, 
    int ** const                  recvdspptr,
    MPI_Request ** const          requptr);

static int
dmeshHaloSync2 (
    Dmesh * restrict const       meshptr,
    DmeshEntity const * restrict const enttlocptr,
    byte **                       attrsndptr,
    void * restrict const         attrgsttab,
    const MPI_Datatype            attrglbtype,
    MPI_Request **                requptr);

int
dmeshHaloSyncComm ( // XXX doit-on garder celle-la en plus de dmeshHaloSync ???
Dmesh * restrict const       meshptr,
const DmeshEnttComm * restrict const commlocptr,
const Gnum                    vertlocnbr,
void * restrict const         attrgsttab,      
const MPI_Datatype            attrglbtype);

void dmeshHaloAsync (
    Dmesh * restrict const              meshptr,
    DmeshEntity * restrict const        enttlocptr,
    void * restrict const               attrgsttab,
    const MPI_Datatype                  attrglbtype,
    DmeshHaloRequests * restrict const   requptr);

int dmeshHaloWait (
    DmeshHaloRequests * restrict         requptr);

int dmeshHaloCheck (
    const Dmesh * restrict const        meshptr,
    const DmeshEnttComm * restrict const  commlocptr);

#undef static
