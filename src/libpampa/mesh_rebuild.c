/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_rebuild.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 28 Mar 2017
//!                             to:   28 Sep 2017
//!
/************************************************************/

#define MESH

#include "module.h"
#include "common.h"
#include "values.h"
#include "mesh.h"
#include "mesh_rebuild.h"
#include "pampa.h"

//! This routine rebuilds a mesh XXX
//! It returns:
//! - 0   : on success.
//! - !0  : on error.

int
meshRebuild (
    Mesh * restrict const       bmshptr,           //!< Boundary mesh
    Mesh * restrict const       imshptr,           //!< Internal mesh
    Mesh * restrict const       moutptr,           //!< Output mesh
    const Gnum                  erefnum,           //!< Reference entity number (XXX face)
    const Gnum                  trefnum,           //!< Tiny entity number (XXX node)
    const Gnum                  trefnbr,           //!< Number of vertex in ref. ent. number
    const Gnum                  trefsiz,           //!< Size of treftax
    Gnum * restrict * restrict const treftb2)           //!< Contains permutation of XXX

{
  // XXX temporaire, trouver comment on peut le mettre en arg (car int[][] ≠ int**)
    PAMPA_Num treftab[4][3] = {{0,1,2}, {0,1,3}, {0,2,3},{1,2,3}};
  Gnum enttnbr;
  Gnum enttnum;
  Gnum enttnnd;
  Gnum enttnm2;
  Gnum baseval;
  Gnum intvertmax;
  Gnum ** bvrttax;
  Gnum ** ivrttax;
  Gnum ** bprmtax;
  Gnum ** iprmtax;
  PAMPA_Iterator it;
  PAMPA_Iterator it_nghb;
  MeshFace * restrict hashfacetab;
  Gnum hashfacenbr;
  Gnum hashfacesiz;
  Gnum hashfacemax;
  Gnum hashfacemsk;
  Gnum hashfaceidx;
  MeshNode * restrict hashnodetab;
  Gnum hashnodenbr;
  Gnum hashnodesiz;
  Gnum hashnodemax;
  Gnum hashnodemsk;
  double * bcootax;
  double * icootax;
  Gnum curvertnum;
  Gnum vertnum;
  Gnum vertnnd;
  Gnum vertnbr;
  Gnum edgenbr;
  Gnum edgesiz;
  Gnum * verttax;
  Gnum * vendtax;
  Gnum * venttax;
  Gnum * edgetax;
  Gnum ventnum;
  Gnum ventnnd;


//* on garde les maillages sous format Mesh
//  * bverttax[enttnbr][vertnbr]
//  * iverttax[enttnbr][vertnbr]

  enttnbr = bmshptr->enttnbr;
  baseval = bmshptr->baseval;

#ifdef PAMPA_DEBUG_MESH
  {
    if (bmshptr->baseval != imshptr->baseval) {
      errorPrint ("Not yet implemented");
      return (1);
    }
  }
#endif /* PAMPA_DEBUG_MESH */


  if (memAllocGroup ((void **) (void *)
        &bvrttax, (size_t) (enttnbr * sizeof (Gnum *)),
        &ivrttax, (size_t) (enttnbr * sizeof (Gnum *)),
        &bprmtax, (size_t) (enttnbr * sizeof (Gnum *)),
        &iprmtax, (size_t) (enttnbr * sizeof (Gnum *)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }

  for (enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
    bvrttax[enttnum] = NULL;
    ivrttax[enttnum] = NULL;
    bprmtax[enttnum] = NULL;
    iprmtax[enttnum] = NULL;

    if (bmshptr->entttax[enttnum] != NULL) {
      if ((bvrttax[enttnum] = (Gnum *) memAlloc (bmshptr->entttax[enttnum]->vertnbr * sizeof (Gnum))) == NULL) {
        errorPrint ("out of memory");
        return     (1);
      }

      memSet (bvrttax[enttnum] + baseval, 0, bmshptr->entttax[enttnum]->vertnbr * sizeof (Gnum));

      if ((bprmtax[enttnum] = (Gnum *) memAlloc (bmshptr->entttax[enttnum]->vertnbr * sizeof (Gnum))) == NULL) {
        errorPrint ("out of memory");
        return     (1);
      }

      memSet (bprmtax[enttnum] + baseval, ~0, bmshptr->entttax[enttnum]->vertnbr * sizeof (Gnum));
    }
  

    if (imshptr->entttax[enttnum] != NULL) {
      if ((ivrttax[enttnum] = (Gnum *) memAlloc (imshptr->entttax[enttnum]->vertnbr * sizeof (Gnum))) == NULL) {
        errorPrint ("out of memory");
        return     (1);
      }

      memSet (ivrttax[enttnum] + baseval, 0, imshptr->entttax[enttnum]->vertnbr * sizeof (Gnum));

      if ((iprmtax[enttnum] = (Gnum *) memAlloc (imshptr->entttax[enttnum]->vertnbr * sizeof (Gnum))) == NULL) {
        errorPrint ("out of memory");
        return     (1);
      }

      memSet (iprmtax[enttnum] + baseval, ~0, imshptr->entttax[enttnum]->vertnbr * sizeof (Gnum));
    }
  }

//* sur le maillage de bord
//  * pour chaque élément, on « toggle » (si elle n'y est pas, on l'ajoute, sinon
//    on l'enlève)  chaque face dans la table de hachage
//    facehashtab (3 noeuds, l'élément dans le bmesh [maillage de bord], l'élément dans le
//    imesh [maillage interne], la face dans le bmesh, la face dans imesh)
//  * on parcourt facehashtab
//    * on ajoute chaque noeud dans nodehashtab (s'il n'y est pas déjà)
//      (coordonnées, le noeud dans bmesh, le noeud dans imesh)

  hashfacenbr = MAX (bmshptr->entttax[baseval]->vertnbr * 0.2, 10);
  for (hashfacesiz = 256; hashfacesiz < hashfacenbr; hashfacesiz <<= 1) ; /* Get upper power of two */
  hashfacenbr = 0;

  hashfacemsk = hashfacesiz - 1;
  hashfacemax = hashfacesiz >> 2;
  if ((hashfacetab = (MeshFace *) memAlloc (hashfacesiz * sizeof (MeshFace))) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }
  memSet(hashfacetab, ~0, hashfacesiz * sizeof (MeshFace));

  PAMPA_meshItInit((PAMPA_Mesh *) bmshptr, baseval, trefnum, &it_nghb);
  PAMPA_meshItInitStart((PAMPA_Mesh *) bmshptr, baseval, &it);
  while (PAMPA_itHasMore(&it)) {
    Gnum tetrnum;
    Gnum nodetab[4]; /* XXX attention à la supposition ! */
    Gnum facetab[3]; /* XXX attention autre supposition (att. supp.) */
    Gnum nodeidx;
    Gnum trefidx;

    tetrnum = PAMPA_itCurEnttVertNum(&it);
  
    PAMPA_itStart(&it_nghb, tetrnum);
    nodeidx = 0;
    while (PAMPA_itHasMore(&it_nghb)) {

      nodetab[nodeidx ++] = PAMPA_itCurEnttVertNum(&it_nghb);
      PAMPA_itNext(&it_nghb);
    }

    for (trefidx = 0; trefidx < trefsiz; trefidx ++) {
      for (Gnum i = 0; i < 3; i++) /* XXX att. supp. */
        facetab[i] = nodetab[treftab[trefidx][i]];

      CHECK_FDBG2 (meshRebldFaceToggle (&hashfacenbr, &hashfacesiz, &hashfacemax, &hashfacemsk, &hashfacetab, facetab, &hashfaceidx));
      if (hashfaceidx != ~0)
        hashfacetab[hashfaceidx].belmnum = tetrnum;
    }

    PAMPA_itNext(&it);
  }

  if (meshValueData (bmshptr, trefnum, PAMPA_TAG_GEOM, NULL, NULL, (void **) &bcootax) != 0) {
    errorPrint ("No geometry associated");
    return     (1);
  }
  bcootax -= 3 * baseval;

  if (meshValueData (imshptr, trefnum, PAMPA_TAG_GEOM, NULL, NULL, (void **) &icootax) != 0) {
    errorPrint ("No geometry associated");
    return     (1);
  }
  icootax -= 3 * baseval;

  hashnodenbr = MAX (hashfacenbr * 4, 10);
  for (hashnodesiz = 256; hashnodesiz < hashnodenbr; hashnodesiz <<= 1) ; /* Get upper power of two */
  hashnodenbr = 0;

  hashnodemsk = hashnodesiz - 1;
  hashnodemax = hashnodesiz >> 2;
  if ((hashnodetab = (MeshNode *) memAlloc (hashnodesiz * sizeof (MeshNode))) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }
  memSet(hashnodetab, ~0, hashnodesiz * sizeof (MeshNode));

  for (hashfaceidx = 0; hashfaceidx < hashfacesiz; hashfaceidx ++) {
    if (hashfacetab[hashfaceidx].nodesum != ~0) {
      Gnum nodenum;
      Gnum hashnum;

      for (nodenum = 0; nodenum < 3; nodenum ++) {
        Gnum coorsum;
        double coorsm2;
        double coortab[3];
        Gnum i;

        for (coorsm2 = i = 0; i < 3; i ++) {
          coorsm2 += bcootax[hashfacetab[hashfaceidx].nodetab[nodenum] * 3 + i];
          coortab[i] = bcootax[hashfacetab[hashfaceidx].nodetab[nodenum] * 3 + i];
        }
        if (fabs(coorsm2) != coorsm2)
          coorsm2 = - coorsm2;
        if (coorsm2 != 0)
          for (; coorsm2 < 100000; coorsm2 *= 10);
        coorsum = (Gnum) coorsm2;

        for (hashnum = (coorsum * MESHREBLDHASHPRIME) & hashnodemsk;
            hashnodetab[hashnum].coorsum != ~0 &&
            (hashnodetab[hashnum].coorsum != coorsum
             || hashnodetab[hashnum].coortab[0] != coortab[0]
             || hashnodetab[hashnum].coortab[1] != coortab[1]
             || hashnodetab[hashnum].coortab[2] != coortab[2]
            ); hashnum = (hashnum + 1) & hashnodemsk) ;

        if ((hashnodetab[hashnum].coorsum != coorsum)
            || (hashnodetab[hashnum].coortab[0] != coortab[0])
            || (hashnodetab[hashnum].coortab[1] != coortab[1])
            || (hashnodetab[hashnum].coortab[2] != coortab[2])) {  /* Node not found */
          hashnodenbr ++;
          hashnodetab[hashnum].coorsum = coorsum;
          memCpy (hashnodetab[hashnum].coortab, coortab, 3 * sizeof (double));
          hashnodetab[hashnum].bnodnum = hashfacetab[hashfaceidx].nodetab[nodenum];

          if (hashnodenbr >= hashnodemax) /* If hashnodetab is too much filled */
            CHECK_FDBG2 (meshRebldNodeResize (&hashnodetab, &hashnodesiz, &hashnodemax, &hashnodemsk));
        }
      }
    }
  }

//* sur le maillage interne
//  * pour chaque élément qui a moins de 4 éléments voisins (dépendant de la
//    topologie : ici tétraèdre)
//    * pour chaque face, on regarde si cette face n'est pas dans facehashtab en
//      retrouvant la correspondance de chaque noeud avec nodehashtab (si le noeud
//      y était déjà [numéro dans imesh], on compare, sinon on le cherche dans
//      nodehashtab)
//      * si la face est dans facehashtab
//        * on met le numéro de l'élément dans facehashtab ainsi que le num de la face (si elle existe)
//          * on incrémente btetrtax, itetrtax, bnodetax et inodetax
  PAMPA_meshItInit((PAMPA_Mesh *) imshptr, baseval, trefnum, &it_nghb);
  PAMPA_meshItInitStart((PAMPA_Mesh *) imshptr, baseval, &it);
  while (PAMPA_itHasMore(&it)) {
    Gnum tetrnum;
    Gnum nodetab[4]; /* XXX attention à la supposition ! */
    Gnum facetab[3]; /* XXX attention autre supposition (att. supp.) */
    Gnum nodeidx;
    Gnum nghbnbr;

    tetrnum = PAMPA_itCurEnttVertNum(&it);
    PAMPA_meshVertData ((PAMPA_Mesh *) imshptr, tetrnum, baseval, baseval, &nghbnbr, NULL);

    if (nghbnbr < 4) { /* XXX att. supp. */
      Gnum trefidx;

      PAMPA_itStart(&it_nghb, tetrnum);
      nodeidx = 0;
      while (PAMPA_itHasMore(&it_nghb)) {

        nodetab[nodeidx ++] = PAMPA_itCurEnttVertNum(&it_nghb);
        PAMPA_itNext(&it_nghb);
      }

      for (trefidx = 0; trefidx < trefsiz; trefidx ++) {
        Gnum hashfaceidx;

        for (Gnum i = 0; i < 3; i++) /* XXX att. supp. */
          facetab[i] = nodetab[treftab[trefidx][i]];

        CHECK_FDBG2 (meshRebldFaceInserted (
              &hashfacemsk, &hashfacetab, NULL, facetab,
              &hashnodemsk, &hashnodetab,
              icootax, &hashfaceidx));
        if (hashfaceidx != ~0) { /* Face found */
          Gnum nodenum;

          hashfacetab[hashfaceidx].ielmnum = tetrnum;
#ifdef PAMPA_DEBUG_MESH2
          if (hashfacetab[hashfaceidx].belmnum == ~0) {
            errorPrint ("internal error");
            return     (1);
          }
#endif /* PAMPA_DEBUG_MESH2 */
          bvrttax[baseval][hashfacetab[hashfaceidx].belmnum] ++;
          ivrttax[baseval][hashfacetab[hashfaceidx].ielmnum] ++;
          for (nodenum = 0; nodenum < 3; nodenum ++) { /* XXX att. supp. */
            bvrttax[trefnum][hashfacetab[hashfaceidx].nodetab[nodenum]] ++;
            ivrttax[trefnum][facetab[nodenum]] ++;
          }
        }
      }

    }
    PAMPA_itNext(&it);
  }


//* le nombre d'arête par sommet est déduit grâce au maillage et à [ib]...tax (sûrement [ib]verttax]
//* bpermtax[enttnbr][vertnbr]
//* ipermtax[enttnbr][vertnbr]
//  * initialisé avec mvrttax (obtenu avec les itérateurs)
//  * pour chaque face, si elle est stockée dans facehashtab,
//    * mais sans numéro de face dans le bmesh, on la vire ~0 dans iperm

  for (enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
    PAMPA_meshItInitStart((PAMPA_Mesh *) imshptr, enttnum, &it);
    while (PAMPA_itHasMore(&it)) {
      Gnum mvrtnum;
      Gnum vertnum;
      Gnum hashfaceidx;

      vertnum = PAMPA_itCurEnttVertNum(&it);
      mvrtnum = PAMPA_itCurMeshVertNum(&it);

      if (enttnum == erefnum) {
        Gnum facetab[3];
        Gnum nodeidx;

        PAMPA_meshItInit((PAMPA_Mesh *) imshptr, enttnum, trefnum, &it_nghb);
        PAMPA_itStart(&it_nghb, vertnum);
        nodeidx = 0;
        while (PAMPA_itHasMore(&it_nghb)) {
          facetab[nodeidx ++] = PAMPA_itCurEnttVertNum(&it_nghb);
          PAMPA_itNext(&it_nghb);
        }

        CHECK_FDBG2 (meshRebldFaceInserted (
              &hashfacemsk, &hashfacetab, NULL, facetab,
              &hashnodemsk, &hashnodetab,
              icootax, &hashfaceidx));
      }

      // XXX test inutile if ((enttnum != erefnum) || ((hashfaceidx != ~0) && (hashfacetab[hashfaceidx].bfacnum != ~0)))
      iprmtax[enttnum][vertnum] = mvrtnum;
      PAMPA_itNext(&it);
    }
  }
  curvertnum = imshptr->vertnbr + baseval;
  intvertmax = curvertnum - 1;



//* curvertnum vaut le vertnbr + baseval du maillage interne
//* on parcourt le maillage de bord
//  * pour chaque sommet
//    * on met à jour bpermtax en attribuant un nouveau numéro dans bpermtax
//    * sauf si c'est une face qui a un numéro de face pour le bmesh dans facehashtab avec un numéro dans le bmesh 
//      * auquel cas, on récupère le numéro dans ipermtax
  PAMPA_meshItInitStart((PAMPA_Mesh *) bmshptr, erefnum, &it);
  while (PAMPA_itHasMore(&it)) {
    Gnum vertnum;

    vertnum = PAMPA_itCurEnttVertNum(&it);

    if (enttnum == erefnum) {
      Gnum facetab[3];
      Gnum nodeidx;

      PAMPA_meshItInit((PAMPA_Mesh *) bmshptr, enttnum, trefnum, &it_nghb);
      PAMPA_itStart(&it_nghb, vertnum);
      nodeidx = 0;
      while (PAMPA_itHasMore(&it_nghb)) {
        facetab[nodeidx ++] = PAMPA_itCurEnttVertNum(&it_nghb);
        PAMPA_itNext(&it_nghb);
      }

      CHECK_FDBG2 (meshRebldFaceInserted (
            &hashfacemsk, &hashfacetab,facetab, NULL, 
            &hashnodemsk, &hashnodetab,
            icootax, &hashfaceidx));
    }

    if ((hashfaceidx != ~0) || (hashfacetab[hashfaceidx].bfacnum == ~0))
      bprmtax[erefnum][vertnum] = curvertnum ++;
    PAMPA_itNext(&it);
  }

  {
    PAMPA_Num hashidx;
    for (hashidx = 0; hashidx < hashfacesiz; hashidx ++)
      if ((hashfacetab[hashidx].nodesum != ~0) && (hashfacetab[hashidx].bfacnum != ~0) && (hashfacetab[hashidx].ifacnum != ~0))
        bprmtax[erefnum][hashfacetab[hashidx].bfacnum] = iprmtax[erefnum][hashfacetab[hashidx].ifacnum];

    for (hashidx = 0; hashidx < hashnodesiz; hashidx ++)
      if ((hashnodetab[hashidx].coorsum != ~0) && (hashnodetab[hashidx].bnodnum != ~0) && (hashnodetab[hashidx].inodnum != ~0))
        bprmtax[trefnum][hashnodetab[hashidx].bnodnum] = iprmtax[trefnum][hashnodetab[hashidx].inodnum];
  }

  for (enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
    //if ((enttnum == erefnum) || (enttnum == trefnum))
    //  continue;

    // XXX est-ce que cette partie est utilisée ? Dans quel cas ?
    PAMPA_meshItInitStart((PAMPA_Mesh *) bmshptr, enttnum, &it);
    while (PAMPA_itHasMore(&it)) {
      Gnum vertnum;

      vertnum = PAMPA_itCurEnttVertNum(&it);

      if (enttnum == erefnum) {
        Gnum facetab[3];
        Gnum nodeidx;

        PAMPA_meshItInit((PAMPA_Mesh *) bmshptr, enttnum, trefnum, &it_nghb);
        PAMPA_itStart(&it_nghb, vertnum);
        nodeidx = 0;
        while (PAMPA_itHasMore(&it_nghb)) {
          facetab[nodeidx ++] = PAMPA_itCurEnttVertNum(&it_nghb);
          PAMPA_itNext(&it_nghb);
        }

        CHECK_FDBG2 (meshRebldFaceInserted (
              &hashfacemsk, &hashfacetab,facetab, NULL, 
              &hashnodemsk, &hashnodetab,
              icootax, &hashfaceidx));
      }

      if (bprmtax[enttnum][vertnum] == ~0)
        bprmtax[enttnum][vertnum] = curvertnum ++;
//      else if ((enttnum == erefnum) && (hashfaceidx != ~0) && (hashfacetab[hashfaceidx].bfacnum != ~0)) {
//#ifdef PAMPA_DEBUG_MESH2
//        if (hashfacetab[hashfaceidx].ifacnum == ~0) {
//          errorPrint ("internal error");
//          return     (1);
//        }
//#endif /* PAMPA_DEBUG_MESH2 */
//        bprmtax[enttnum][vertnum] = iprmtax[enttnum][hashfacetab[hashfaceidx].ifacnum];
//      }
      PAMPA_itNext(&it);
    }
  }

  for (edgesiz = 0, enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
    if (bmshptr->entttax[enttnum] != NULL) 
      for (vertnum = baseval, vertnnd = baseval + bmshptr->entttax[enttnum]->vertnbr; vertnum < vertnnd; vertnum ++)
        edgesiz += bvrttax[enttnum][vertnum];

    if (imshptr->entttax[enttnum] != NULL) 
      for (vertnum = baseval, vertnnd = baseval + imshptr->entttax[enttnum]->vertnbr; vertnum < vertnnd; vertnum ++)
        edgesiz += ivrttax[enttnum][vertnum];
  }

  edgesiz += bmshptr->edgenbr + imshptr->edgenbr;
  vertnbr = curvertnum;

  if (memAllocGroup ((void **) (void *)
        &verttax, (size_t) ((vertnbr + 1) * sizeof (Gnum)),
        &vendtax, (size_t) (vertnbr       * sizeof (Gnum)),
        &venttax, (size_t) (vertnbr       * sizeof (Gnum)),
        &edgetax, (size_t) (edgesiz       * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }

  verttax -= baseval;
  vendtax -= baseval;
  venttax -= baseval;
  edgetax -= baseval;

  memSet (venttax + baseval, ~0, vertnbr * sizeof (Gnum));
#ifdef PAMPA_DEBUG_MESH2
  {
    memSet (verttax + baseval, ~0, (vertnbr + 1) * sizeof (Gnum));
    memSet (vendtax + baseval, ~0, vertnbr * sizeof (Gnum));
    memSet (edgetax + baseval, ~0, edgesiz * sizeof (Gnum));
  }
#endif /* PAMPA_DEBUG_MESH2 */

  {
    Gnum    hashidx;

    Mesh *  meshtab[2];
    Gnum ** mprmtax[2];
    Gnum ** mvrttax[2];
    Gnum    i;
    
    meshtab[0] = imshptr;
    meshtab[1] = bmshptr;

    mprmtax[0] = iprmtax;
    mprmtax[1] = bprmtax;

    mvrttax[0] = ivrttax;
    mvrttax[1] = bvrttax;

    for (vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++)
      verttax[vertnum] = baseval;

    verttax += 1;
    for (int i = 0; i < 2; i++) {
      for (enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
        if (meshtab[i]->entttax[enttnum] == NULL)
          continue;
        for (vertnum = baseval, vertnnd = baseval + meshtab[i]->entttax[enttnum]->vertnbr; vertnum < vertnnd; vertnum ++) {
          Gnum nghbnbr;

          PAMPA_meshVertData ((PAMPA_Mesh *) meshtab[i], vertnum, enttnum, -1, &nghbnbr, NULL);
          verttax[mprmtax[i][enttnum][vertnum]] += nghbnbr;
          verttax[mprmtax[i][enttnum][vertnum]] += mvrttax[i][enttnum][vertnum];
        }
      }
    }
    verttax -= 1;

    verttax[baseval] =
      vendtax[baseval] = baseval;
    for (vertnum = baseval + 1, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++) {
      verttax[vertnum] += verttax[vertnum - 1];
      vendtax[vertnum] = verttax[vertnum];
    }

    for (edgenbr = hashidx = 0; hashidx < hashfacesiz; hashidx ++)
      if ((hashfacetab[hashidx].nodesum != ~0) && (hashfacetab[hashidx].belmnum != ~0) && (hashfacetab[hashidx].ielmnum != ~0)) {
        Gnum belmnum;
        Gnum ielmnum;

        belmnum = hashfacetab[hashidx].belmnum;
        ielmnum = hashfacetab[hashidx].ielmnum;
        edgetax[vendtax[bprmtax[baseval][belmnum]] ++] = iprmtax[baseval][ielmnum];
        edgenbr ++;
        edgetax[vendtax[iprmtax[baseval][ielmnum]] ++] = bprmtax[baseval][belmnum];
        edgenbr ++;
      }

        

    for (i = 0; i < 2; i++) {

      for (ventnum = baseval, ventnnd = baseval + enttnbr; ventnum < ventnnd; ventnum ++) {
        Gnum nentnum;
        Gnum nentnnd;

        if (meshtab[i]->entttax[ventnum] == NULL)
          continue;
        for (nentnum = baseval, nentnnd = baseval + enttnbr; nentnum < nentnnd; nentnum ++) {
          if (meshtab[i]->entttax[nentnum] == NULL)
            continue;
          PAMPA_meshItInit((PAMPA_Mesh *) meshtab[i], ventnum, nentnum, &it_nghb);
          PAMPA_meshItInitStart((PAMPA_Mesh *) meshtab[i], ventnum, &it);
          while (PAMPA_itHasMore(&it)) {
            Gnum vertnum;
            Gnum newvertnum;

            vertnum = PAMPA_itCurEnttVertNum(&it);
            newvertnum = mprmtax[i][ventnum][vertnum];
            
            if (newvertnum != ~0) {

              venttax[newvertnum] = ventnum;
              PAMPA_itStart(&it_nghb, vertnum);
              while (PAMPA_itHasMore(&it_nghb)) {
                Gnum nghbnum;
                Gnum newnghbnum;

                nghbnum = PAMPA_itCurEnttVertNum(&it_nghb);
                newnghbnum = mprmtax[i][nentnum][nghbnum];

                if (newnghbnum != ~0) {
#ifdef PAMPA_DEBUG_MESH2
                  {
                    if (vendtax[newvertnum] == ~0) {
                      errorPrint ("internal error");
                      return     (1);
                    }
                  }
#endif /* PAMPA_DEBUG_MESH2 */
                  if ((i == 0) /* internal mesh */
                      || (newvertnum > intvertmax)
                      || (newnghbnum > intvertmax)) {
                    edgetax[vendtax[newvertnum] ++] = newnghbnum;
                    edgenbr ++;
                  }
                }

                PAMPA_itNext(&it_nghb);
              }
            }
            PAMPA_itNext(&it);
          }
        }
      }
    }
  }

  memFree (hashfacetab);
  memFree (hashnodetab);

  CHECK_FDBG2 (meshBuild( moutptr, ~0, baseval, vertnbr, verttax, vendtax,
        edgenbr, edgesiz, edgetax, NULL, enttnbr, venttax, NULL, 50, NULL, ~0, bmshptr->procglbnbr)); // XXX pourquoi 50 comme valeurs max ??
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (meshCheck2 (moutptr, PAMPA_CHECK_ALONE));
#endif /* PAMPA_DEBUG_MESH */

  { // XXX pour déboguer
    for (vertnum = baseval; vertnum < moutptr->entttax[0]->vertnbr; vertnum ++) {
      Gnum nghbnbr;
      PAMPA_meshVertData (moutptr, vertnum, 0, 2, &nghbnbr, NULL);
      if (nghbnbr != 4) {
        errorPrint ("error");
        return 1;
      }
    }
  } // XXX fin pour déboguer

  {
    Mesh *  meshtab[2];
    Gnum ** mprmtax[2];
    Gnum ** mvrttax[2];
    Gnum    i;

    meshtab[0] = imshptr;
    meshtab[1] = bmshptr;

    mprmtax[0] = iprmtax;
    mprmtax[1] = bprmtax;

    mvrttax[0] = ivrttax;
    mvrttax[1] = bvrttax;

    for (i = 0; i < 2; i ++) {
      for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++) {
        Value * valuptr;
        Gnum    vertmax;

        memSet (verttax + baseval, ~0, vertnbr * sizeof (Gnum));
        switch (enttnum) {
         case PAMPA_ENTT_DUMMY:
           break;
         case PAMPA_ENTT_VIRT_PROC:
           if (i == 0) /* we take only values from bmshptr */
             continue;
           break;
         case PAMPA_ENTT_VIRT_VERT:
           for (enttnm2 = baseval, enttnnd = baseval + enttnbr; enttnm2 < enttnnd; enttnm2 ++) {
             PAMPA_meshItInitStart((PAMPA_Mesh *) meshtab[i], enttnm2, &it);
             while (PAMPA_itHasMore(&it)) {
               Gnum vertnum;
               Gnum mvrtnum;

               vertnum = PAMPA_itCurEnttVertNum(&it);
               mvrtnum = PAMPA_itCurMeshVertNum(&it);            

               verttax[mvrtnum] = mprmtax[i][enttnm2][vertnum];
               PAMPA_itNext(&it);
             }
           }
           vertmax = meshtab[i]->vertnbr + baseval;
           break;
         default:
           if (moutptr->entttax[enttnum] == NULL)
             continue;

           PAMPA_meshItInitStart((PAMPA_Mesh *) moutptr, enttnum, &it);
           while (PAMPA_itHasMore(&it)) {
             Gnum vertnum;
             Gnum mvrtnum;

             vertnum = PAMPA_itCurEnttVertNum(&it);
             mvrtnum = PAMPA_itCurMeshVertNum(&it);            

             verttax[mvrtnum] = vertnum;
             PAMPA_itNext(&it);
           }
           vertmax = (meshtab[i]->entttax[enttnum] == NULL) ? (0) : (meshtab[i]->entttax[enttnum]->vertnbr + baseval);
        }
        CHECK_FDBG2 (meshRebldValues (meshtab[i], moutptr, (const Gnum **) mprmtax[i], enttnum, verttax, baseval, vertmax));
        if (enttnum >= baseval) {
          memFree (mprmtax[i][enttnum]);
          memFree (mvrttax[i][enttnum]);
        }
      }
    }
  }

  memFreeGroup (bvrttax + baseval);
  memFreeGroup (verttax + baseval);
  return (0);
}

int meshRebldValues (
    Mesh  * const srcmeshptr,
    Mesh  * const dstmeshptr,
    const Gnum * * mprmtax,
    const Gnum    enttnum,
    const Gnum *  verttax,
    const Gnum    vertmin,
    const Gnum    vertmax)
{
  Value * valuptr;

  for (valuptr = srcmeshptr->valsptr->evaltak[enttnum]; valuptr != NULL; valuptr = valuptr->next) {
    MPI_Aint            valusiz;                 /* Extent of attribute datatype */
    MPI_Aint            dummy;
    byte *              valutax;
    Gnum                srcvertnum;
    Gnum                srcvertnnd;
    Gnum                sizeval;
    int                 chekval;

    MPI_Type_get_extent (valuptr->typeval, &dummy, &valusiz);     /* Get type extent */

    chekval = meshValueData (dstmeshptr, enttnum, valuptr->tagnum, &sizeval, NULL, (void **) &valutax);
    if (chekval == 2) {
      CHECK_FDBG2 (meshValueLink(dstmeshptr, (void **) &valutax, valuptr->flagval & (!PAMPA_VALUE_ALLOCATED), &sizeval, NULL, valuptr->typeval, enttnum, valuptr->tagnum));
      memSet ((byte *) valutax, ~0, sizeval * valusiz * sizeof (byte));
    }
    else if (chekval != 0) {
      errorPrint ("internal error");
      return (1);
    }

    if (enttnum != PAMPA_ENTT_VIRT_PROC)
      valutax -= dstmeshptr->baseval * valusiz;

    if (enttnum == PAMPA_ENTT_VIRT_PROC)
      memCpy ((byte *) valutax, (byte *) valuptr->valutab, valusiz * sizeval * sizeof (byte));
    else 
      for (srcvertnum = vertmin, srcvertnnd = vertmax; srcvertnum < srcvertnnd; srcvertnum ++) {
        Gnum dstvertnum;

        if ((enttnum >= dstmeshptr->baseval) && (mprmtax[enttnum][srcvertnum] == ~0))
          continue;

        dstvertnum = (enttnum == PAMPA_ENTT_VIRT_VERT)
          ? (verttax[srcvertnum])
          : (verttax[mprmtax[enttnum][srcvertnum]]);
        if (dstvertnum != ~0)
          memCpy ((byte *) valutax + dstvertnum * valusiz, (byte *) valuptr->valutab + (srcvertnum - srcmeshptr->baseval) * valusiz, valusiz * sizeof (byte));
      }
  }
  return (0);
}

int meshRebldFaceToggle (
    Gnum * hashfacenbr, 
    Gnum * hashfacesiz, 
    Gnum * hashfacemax, 
    Gnum * hashfacemsk, 
    MeshFace * restrict * restrict hashfacetab,
    Gnum * nodetab,
    Gnum * hashfaceidx) 
{
  int  i;
  Gnum hashnum;
  Gnum nodesum;

  if (*hashfacenbr >= *hashfacemax) /* If hashfacetab is too much filled */
    CHECK_FDBG2 (meshRebldFaceResize (hashfacetab, hashfacesiz, hashfacemax, hashfacemsk));

  for (nodesum = i = 0; i < 3; i ++)
    nodesum += nodetab[i];

  intSort1asc1 (nodetab, 3);

  for (hashnum = (nodesum * MESHREBLDHASHPRIME) & (*hashfacemsk);
      (*hashfacetab)[hashnum].nodesum != ~0 &&
      ((*hashfacetab)[hashnum].nodesum != nodesum
       || (*hashfacetab)[hashnum].nodetab[0] != nodetab[0]
       || (*hashfacetab)[hashnum].nodetab[1] != nodetab[1]
       || (*hashfacetab)[hashnum].nodetab[2] != nodetab[2]
      ); hashnum = (hashnum + 1) & (*hashfacemsk)) ;

  if (((*hashfacetab)[hashnum].nodesum == nodesum)
      && ((*hashfacetab)[hashnum].nodetab[0] == nodetab[0])
      && ((*hashfacetab)[hashnum].nodetab[1] == nodetab[1])
      && ((*hashfacetab)[hashnum].nodetab[2] == nodetab[2])) {  /* Face found */
    PAMPA_Num hashtmp;
    PAMPA_Num hashidx;

    (*hashfacetab)[hashnum].nodesum = ~0;
    *hashfaceidx = ~0;
    (*hashfacenbr) --;

    //for (hashidx = (hashnum + 1) & (*hashfacemsk); ; hashidx = (hashidx + 1) & (*hashfacemsk));
    //hashtmp = hashnum;

    for (hashidx = (hashnum + 1) & (*hashfacemsk); (*hashfacetab)[hashidx].nodesum != ~0; hashidx = (hashidx + 1) & (*hashfacemsk)) {
      PAMPA_Num nodesum;
      double    nodetab[3];

      nodesum = (*hashfacetab)[hashidx].nodesum;
      for (int i = 0 ; i < 3 ; i ++)
        nodetab[i] = (*hashfacetab)[hashidx].nodetab[i];

      for (hashnum = (nodesum * MESHREBLDHASHPRIME) & (*hashfacemsk);
          (*hashfacetab)[hashnum].nodesum != ~0 &&
          ((*hashfacetab)[hashnum].nodesum != nodesum
           || (*hashfacetab)[hashnum].nodetab[0] != nodetab[0]
           || (*hashfacetab)[hashnum].nodetab[1] != nodetab[1]
           || (*hashfacetab)[hashnum].nodetab[2] != nodetab[2]
          ); hashnum = (hashnum + 1) & (*hashfacemsk)) ;

      if (hashnum == hashidx) // already at the good slot 
        continue;
      (*hashfacetab)[hashnum] = (*hashfacetab)[hashidx];
      (*hashfacetab)[hashidx].nodesum = ~0;
    }
  }
  else {                                                    /* Face not found */
    (*hashfacetab)[hashnum].nodesum = nodesum;
    (*hashfacetab)[hashnum].nodetab[0] = nodetab[0];
    (*hashfacetab)[hashnum].nodetab[1] = nodetab[1];
    (*hashfacetab)[hashnum].nodetab[2] = nodetab[2];
    *hashfaceidx = hashnum;
    (*hashfacenbr) ++;
  }
  return (0);
}

int meshRebldFaceInserted (
    Gnum * hashfacemsk,
    MeshFace * restrict * restrict hashfacetab,
    Gnum * bfactab,
    Gnum * ifactab,
    Gnum * hashnodemsk,
    MeshNode * restrict * restrict hashnodetab,
    double * coortax,
    Gnum * hashfaceidx)
{
  Gnum nodenum;
  Gnum nodetab[3];
  Gnum i;
  Gnum hashnum;
  Gnum coorsum;
  double coorsm2;
  Gnum nodesum;

  if (ifactab != NULL) {
    for (nodenum = 0; nodenum < 3; nodenum ++) {
      double coortab[3];

      for (coorsm2 = i = 0; i < 3; i ++) {
        coorsm2 += coortax[ifactab[nodenum] * 3 + i];
        coortab[i] = coortax[ifactab[nodenum] * 3 + i];
      }
      if (fabs(coorsm2) != coorsm2)
        coorsm2 = - coorsm2;
      if (coorsm2 != 0)
        for (; coorsm2 < 100000; coorsm2 *= 10);
      coorsum = (Gnum) coorsm2;

      for (hashnum = (coorsum * MESHREBLDHASHPRIME) & (*hashnodemsk);
          (*hashnodetab)[hashnum].coorsum != ~0 &&
          ((*hashnodetab)[hashnum].coorsum != coorsum
           || (*hashnodetab)[hashnum].coortab[0] != coortab[0]
           || (*hashnodetab)[hashnum].coortab[1] != coortab[1]
           || (*hashnodetab)[hashnum].coortab[2] != coortab[2]
          ); hashnum = (hashnum + 1) & (*hashnodemsk)) ;

      if (((*hashnodetab)[hashnum].coorsum == coorsum)
          && ((*hashnodetab)[hashnum].coortab[0] == coortab[0])
          && ((*hashnodetab)[hashnum].coortab[1] == coortab[1])
          && ((*hashnodetab)[hashnum].coortab[2] == coortab[2])) {  /* Node found */
        nodetab[nodenum] = (*hashnodetab)[hashnum].bnodnum;
#ifdef PAMPA_DEBUG_MESH2
        if (((*hashnodetab)[hashnum].inodnum != ~0) && ((*hashnodetab)[hashnum].inodnum != ifactab[nodenum])) {
          errorPrint ("internal error");
          return     (1);
        }
#endif /* PAMPA_DEBUG_MESH2 */
        if ((*hashnodetab)[hashnum].inodnum == ~0)
          (*hashnodetab)[hashnum].inodnum = ifactab[nodenum];
      }
      else {
        *hashfaceidx = ~0;
        return 0;
      }
//#ifdef PAMPA_DEBUG_MESH
//      else {
//        errorPrint ("internal error");
//        return     (1);
//      }
//#endif /*PAMPA_DEBUG_MESH */
    }
  }
  else /* bfacnum not NULL */
    memCpy (nodetab, bfactab, 3 * sizeof (Gnum));

  for (nodesum = i = 0; i < 3; i ++)
    nodesum += nodetab[i];

  intSort1asc1 (nodetab, 3);

  for (hashnum = (nodesum * MESHREBLDHASHPRIME) & (*hashfacemsk);
      (*hashfacetab)[hashnum].nodesum != ~0 &&
      ((*hashfacetab)[hashnum].nodesum != nodesum
       || (*hashfacetab)[hashnum].nodetab[0] != nodetab[0]
       || (*hashfacetab)[hashnum].nodetab[1] != nodetab[1]
       || (*hashfacetab)[hashnum].nodetab[2] != nodetab[2]
      ); hashnum = (hashnum + 1) & (*hashfacemsk)) ;

  if (((*hashfacetab)[hashnum].nodesum == nodesum)
      && ((*hashfacetab)[hashnum].nodetab[0] == nodetab[0])
      && ((*hashfacetab)[hashnum].nodetab[1] == nodetab[1])
      && ((*hashfacetab)[hashnum].nodetab[2] == nodetab[2])) /* Face found */
    *hashfaceidx = hashnum;
  else
    *hashfaceidx = ~0;
  return 0;
}

static
  int
meshRebldNodeResize (
    MeshNode * restrict * hashtabptr,
    PAMPA_Num * restrict const           hashsizptr,
    PAMPA_Num * restrict const           hashmaxptr,
    PAMPA_Num * restrict const           hashmskptr)
{
  PAMPA_Num                          oldhashsiz;          /* Size of hash table    */
  PAMPA_Num                          hashnum;          /* Hash value            */
  PAMPA_Num                          oldhashmsk;
  int                           cheklocval;
  PAMPA_Num hashidx;
  PAMPA_Num hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_MESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].coorsum == coorsum)
          && ((*hashtabptr)[hashnum].coortab[0] == coortab[0])
          && ((*hashtabptr)[hashnum].coortab[1] == coortab[1])
          && ((*hashtabptr)[hashnum].coortab[2] == coortab[2])
          )
        errorPrint ("duplicated item in MeshNodeTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_MESH */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (MeshNode *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (MeshNode)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VDBG2 (cheklocval);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (MeshNode)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].coorsum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (coorsum * MESHREBLDHASHPRIME) & (*hashmskptr);
        (*hashtabptr)[hashnum].coorsum != ~0 &&
        ((*hashtabptr)[hashnum].coorsum != coorsum
         || (*hashtabptr)[hashnum].coortab[0] != coortab[0]
         || (*hashtabptr)[hashnum].coortab[1] != coortab[1]
         || (*hashtabptr)[hashnum].coortab[2] != coortab[2]
        ); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].coorsum = ~0;
  }

#ifdef PAMPA_DEBUG_MESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].coorsum == coorsum)
          && ((*hashtabptr)[hashnum].coortab[0] == coortab[0])
          && ((*hashtabptr)[hashnum].coortab[1] == coortab[1])
          && ((*hashtabptr)[hashnum].coortab[2] == coortab[2])
          )
        errorPrint ("duplicated item in MeshNodeTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_MESH */
  return (0);
}

static
  int
meshRebldFaceResize (
    MeshFace * restrict * hashtabptr,
    PAMPA_Num * restrict const           hashsizptr,
    PAMPA_Num * restrict const           hashmaxptr,
    PAMPA_Num * restrict const           hashmskptr)
{
  PAMPA_Num                          oldhashsiz;          /* Size of hash table    */
  PAMPA_Num                          hashnum;          /* Hash value            */
  PAMPA_Num                          oldhashmsk;
  int                           cheklocval;
  PAMPA_Num hashidx;
  PAMPA_Num hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_MESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num nodesum;
    double    nodetab[3];

    nodesum = (*hashtabptr)[hashidx].nodesum;
    for (int i = 0 ; i < 3 ; i ++)
      nodetab[i] = (*hashtabptr)[hashidx].nodetab[i];

    if (nodesum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].nodesum == nodesum)
          && ((*hashtabptr)[hashnum].nodetab[0] == nodetab[0])
          && ((*hashtabptr)[hashnum].nodetab[1] == nodetab[1])
          && ((*hashtabptr)[hashnum].nodetab[2] == nodetab[2])
          )
        errorPrint ("duplicated item in MeshFaceTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_MESH */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (MeshFace *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (MeshFace)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VDBG2 (cheklocval);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (MeshFace)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].nodesum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num nodesum;
    double    nodetab[3];

    nodesum = (*hashtabptr)[hashidx].nodesum;
    for (int i = 0 ; i < 3 ; i ++)
      nodetab[i] = (*hashtabptr)[hashidx].nodetab[i];

    if (nodesum == ~0) // If empty slot
      continue;

    for (hashnum = (nodesum * MESHREBLDHASHPRIME) & (*hashmskptr);
        (*hashtabptr)[hashnum].nodesum != ~0 &&
        ((*hashtabptr)[hashnum].nodesum != nodesum
         || (*hashtabptr)[hashnum].nodetab[0] != nodetab[0]
         || (*hashtabptr)[hashnum].nodetab[1] != nodetab[1]
         || (*hashtabptr)[hashnum].nodetab[2] != nodetab[2]
        ); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].nodesum = ~0;
  }

#ifdef PAMPA_DEBUG_MESH
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num nodesum;
    double    nodetab[3];

    nodesum = (*hashtabptr)[hashidx].nodesum;
    for (int i = 0 ; i < 3 ; i ++)
      nodetab[i] = (*hashtabptr)[hashidx].nodetab[i];

    if (nodesum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].nodesum == nodesum)
          && ((*hashtabptr)[hashnum].nodetab[0] == nodetab[0])
          && ((*hashtabptr)[hashnum].nodetab[1] == nodetab[1])
          && ((*hashtabptr)[hashnum].nodetab[2] == nodetab[2])
          )
        errorPrint ("duplicated item in MeshFaceTab(2), hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_MESH */
  return (0);
}
