!*  Copyright 2009-2016 Inria
!*
!* This file is part of the PaMPA software package for parallel
!* mesh partitioning and adaptation.
!*
!* PaMPA is free software: you can redistribute it and/or modify
!* it under the terms of the GNU General Public License as published by
!* the Free Software Foundation, either version 3 of the License, or
!* any later version.
!* 
!* PaMPA is distributed in the hope that it will be useful,
!* but WITHOUT ANY WARRANTY; without even the implied warranty of
!* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!* GNU General Public License for more details.
!* 
!* In this respect, the user's attention is drawn to the risks associated
!* with loading, using, modifying and/or developing or reproducing the
!* software by the user in light of its specific status of free software,
!* that may mean that it is complicated to manipulate, and that also
!* therefore means that it is reserved for developers and experienced
!* professionals having in-depth computer knowledge. Users are therefore
!* encouraged to load and test the software's suitability as regards
!* their requirements in conditions enabling the security of their
!* systems and/or data to be ensured and, more generally, to use and
!* operate it in the same conditions as regards security.
!* 
!* The fact that you are presently reading this means that you have had
!* knowledge of the GPLv3 license and that you accept its terms.
!*
!************************************************************
!> 
!>    \file        libraryf.h
!> 
!>    \authors     Francois Pellegrini
!>                 Cedric Lachat
!> 
!>    \brief       This module is the API for the libPampa
!>                 library.
!> 
!>    \date        Version 1.0: from: 17 Jan 2011
!>                              to:   20 Jul 2016
!>                 Version 2.0: from:  7 Oct 2016
!>                              to:   12 May 2017
!> 
!************************************************************

! Size definitions for the PaMPA opaque
! structures. These structures must be
! allocated as arrays of DOUBLEPRECISION
! values for proper padding. The dummy
! sizes are computed at compile-time by
! program "dummysizes".




#ifndef PAMPAF_DEBUG_ITER

!! \attention 
!!      - to use PAMPAF_itHasMore, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itHasMore(iterptr)  \
    (iterptr%VERTNUM(1) < iterptr%VERTNND(1))

!! \attention 
!!      - to use PAMPAF_itNext, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
!! // XXX pourquoi la ligne ci-dessous est commentée alors qu'elle est mieux normalement ??
!#define PAMPAF_itNext(iterptr) do;iterptr%VERTNUM(1)=iterptr%VERTNUM(1)+1;if((iterptr%VERTNUM(1)>=iterptr%VERTNND(1)).or.(iterptr%NENTMSK==0))exit;if(iterptr%NENTBAS(iterptr%EDGETAB(iterptr%VERTNUM(1)))==iterptr%NESBNUM)exit;enddo
#define PAMPAF_itNext(iterptr) do;iterptr%VERTNUM(1)=iterptr%VERTNUM(1)+1;if(iterptr%VERTNUM(1)>=iterptr%VERTNND(1))exit;if(iterptr%NENTBAS(iand(iterptr%EDGETAB(iterptr%VERTNUM(1)),iterptr%NENTMSK))==iterptr%NESBNUM)exit;enddo
 
!! \attention 
!!      - to use PAMPAF_itHasSubEntt, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itHasSubEntt(iterptr) \
  (associated(iterptr%SNGBTAB))

!! \attention 
!!      - to use PAMPAF_itIsSubEntt, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itIsSubEntt(iterptr) \
  (iterptr%NESBNUM .ne. iterptr%NENTNUM)

!! \attention 
!!      - to use PAMPAF_itCurSubEnttVertNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itCurSubEnttVertNum(iterptr) \
  iterptr%SNGBTAB(iterptr%EDGETAB(iterptr%VERTNUM(1)))

!! \attention 
!!      - to use PAMPAF_itCurMeshVertNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itCurMeshVertNum(iterptr)   \
  iterptr%MNGBTAB(iterptr%EDGETAB(iterptr%VERTNUM(1)))

!! \attention 
!!      - to use PAMPAF_itCurEnttVertNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itCurEnttVertNum(iterptr)   \
  iterptr%EDGETAB(iterptr%VERTNUM(1))

!! \attention 
!!      - to use PAMPAF_itCurCurEnttNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itCurSubEnttNum(iterptr)   \
  iterptr%NENTTAB(iterptr%EDGETAB(iterptr%VERTNUM(1)))

!! \attention 
!!      - to use PAMPAF_itCurEnttNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itCurEnttNum(iterptr)   \
  iterptr%NENTNUM


!! \attention 
!!      - to use PAMPAF_itThreadsHasMore, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsHasMore(iterptr,thread)  \
    (iterptr%VERTNUM(thread) < iterptr%VERTNND(thread))

!! \attention 
!!      - to use PAMPAF_itThreadsNext, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
!! // XXX pourquoi la ligne ci-dessous est commentée alors qu'elle est mieux normalement ??
!#define PAMPAF_itThreadsNext(iterptr,thread) do;iterptr%VERTNUM(thread)=iterptr%VERTNUM(thread)+1;if((iterptr%VERTNUM(thread)>=iterptr%VERTNND(thread)).or.(iterptr%NENTMSK==0))exit;if(iterptr%NENTBAS(iterptr%EDGETAB(iterptr%VERTNUM(thread)))==iterptr%NESBNUM)exit;enddo
#define PAMPAF_itThreadsNext(iterptr,thread) do;iterptr%VERTNUM(thread)=iterptr%VERTNUM(thread)+1;if(iterptr%VERTNUM(thread)>=iterptr%VERTNND(thread))exit;if(iterptr%NENTBAS(iand(iterptr%EDGETAB(iterptr%VERTNUM(thread)),iterptr%NENTMSK))==iterptr%NESBNUM)exit;enddo
 
!! \attention 
!!      - to use PAMPAF_itThreadsHasSubEntt, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsHasSubEntt(iterptr,thread) \
  (associated(iterptr%SNGBTAB))

!! \attention 
!!      - to use PAMPAF_itThreadsIsSubEntt, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsIsSubEntt(iterptr,thread) \
  (iterptr%NESBNUM .ne. iterptr%NENTNUM)

!! \attention 
!!      - to use PAMPAF_itThreadsCurSubEnttVertNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsCurSubEnttVertNum(iterptr,thread) \
  iterptr%SNGBTAB(iterptr%EDGETAB(iterptr%VERTNUM(thread)))

!! \attention 
!!      - to use PAMPAF_itThreadsCurMeshVertNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsCurMeshVertNum(iterptr,thread)   \
  iterptr%MNGBTAB(iterptr%EDGETAB(iterptr%VERTNUM(thread)))

!! \attention 
!!      - to use PAMPAF_itThreadsCurEnttVertNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsCurEnttVertNum(iterptr,thread)   \
  iterptr%EDGETAB(iterptr%VERTNUM(thread))

!! \attention 
!!      - to use PAMPAF_itThreadsCurCurEnttNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsCurSubEnttNum(iterptr,thread)   \
  iterptr%NENTTAB(iterptr%EDGETAB(iterptr%VERTNUM(thread)))

!! \attention 
!!      - to use PAMPAF_itThreadsCurEnttNum, you have to put '\#include "pampaf.h"' instead of 'USE PAMPA' 
!!      - the name is case sensitive 
#define PAMPAF_itThreadsCurEnttNum(iterptr,thread)   \
  iterptr%NENTNUM

#else  /* PAMPA_DEBUG_ITER */

#define PAMPAF_itHasMore(iterptr) \
PAMPAF_itHasMoreDebug (iterptr)

#define PAMPAF_itNext(iterptr) \
CALL PAMPAF_itNextDebug (iterptr)

#define PAMPAF_itCurEnttVertNum(iterptr) \
PAMPAF_itCurEnttVertNumDebug (iterptr)

#define PAMPAF_itCurMeshVertNum(iterptr) \
PAMPAF_itCurMeshVertNumDebug (iterptr)

#define PAMPAF_itCurSubEnttVertNum(iterptr) \
PAMPAF_itCurSubEnttVertNumDebug (iterptr)

#define PAMPAF_itCurEnttNum(iterptr) \
PAMPAF_itCurEnttNumDebug (iterptr)

#define PAMPAF_itCurSubEnttNum(iterptr) \
PAMPAF_itCurSubEnttNumDebug (iterptr)

#define PAMPAF_itIsSubEntt(iterptr) \
PAMPAF_itIsSubEnttDebug (iterptr)

#define PAMPAF_itHasSubEntt(iterptr) \
PAMPAF_itHasSubEnttDebug (iterptr)

#endif /* PAMPAF_DEBUG_ITER */



#ifndef PAMPAF_BUILD_ITER
  USE PAMPA
#endif /* PAMPAF_BUILD_ITER */


