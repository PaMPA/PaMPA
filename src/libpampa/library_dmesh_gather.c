
/*
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/

#define LIBRARY

#include "module.h"
#include "common.h"
#include "comm.h"
#include "values.h"
#include "mesh.h"
#include "dvalues.h"
#include "dmesh.h"
#include <pampa.h>
#include "dmesh_gather_induce_multiple.h"

/************************************/
/*                                  */
/* These routines are the C API for */
/* the mesh handling routines.     */
/*                                  */
/************************************/

//! \brief This routine gathers the data of a
//! distributed mesh on a centralized mesh.
//! \attention This routine is not yet implemented
//!
//! \returns 0   : if the centralization succeeded.
//! \returns !0  : on error.

int
PAMPA_dmeshGather (
const PAMPA_Dmesh * const dmshptr,                                      //!< Distributed mesh to gather
PAMPA_Mesh * const        cmshptr)                                      //!< Centralized mesh which will contain the gathered mesh
{
  Dmesh * srcdmshptr;
  Gnum partloctab[1];
  Gnum partglbtab[1];
  Gnum * vnumloctax;
  Gnum meshlocnbr;
  Gnum baseval;
  int cheklocval;

  cheklocval = 0;

  srcdmshptr = (Dmesh *) dmshptr;
  baseval = srcdmshptr->baseval;

  if ((cmshptr != NULL) && ((void *) cmshptr != (void *) dmshptr)) {
	meshlocnbr = 1;
	partloctab[0] = srcdmshptr->proclocnum + 1;
  }
  else {
	meshlocnbr = 0;
  	partloctab[0] = 0;
  }


  CHECK_VDBG (cheklocval, srcdmshptr->proccomm);
  if (commAllreduce (partloctab, partglbtab, 1, GNUM_MPI, MPI_SUM, srcdmshptr->proccomm) != MPI_SUCCESS) {
    errorPrint ("communication error");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcdmshptr->proccomm);

  if (((cmshptr != NULL) && ((void *) cmshptr != (void *) dmshptr)) && (partloctab[0] != partglbtab[0])) {
	errorPrint ("Only one root is permitted\n");
	cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcdmshptr->proccomm);

  partloctab[0] = partglbtab[0] - 1;

  if ((vnumloctax = (Gnum *) memAlloc (srcdmshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum))) == NULL) {
    errorPrint  ("out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, srcdmshptr->proccomm);
  vnumloctax -= baseval;

  memSet (vnumloctax + baseval, 0, srcdmshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));

  if (cmshptr == NULL) {
  	CHECK_FDBG (dmeshGatherInduceMultiple (srcdmshptr, 0, 1, vnumloctax, partloctab, &meshlocnbr, NULL), srcdmshptr->proccomm);
  }
  else {
  	CHECK_FDBG (dmeshGatherInduceMultiple (srcdmshptr, 0, 1, vnumloctax, partloctab, &meshlocnbr, (Mesh **) &cmshptr), srcdmshptr->proccomm);
  }
  memFree (vnumloctax + baseval);
  return (0);
}
