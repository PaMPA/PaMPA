/*  Copyright 2012-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        dmesh_adapt_graph_grow.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 20 Dec 2012
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define DMESH
#define DMESH_ADAPT
#define PAMPA_TIME_CHECK

#include "module.h"
#include "common.h"
#include "dvalues.h"
#include "dmesh.h"
#include "values.h"
#include "mesh.h"
#include "smesh.h"
#include "pampa.h"
#include "pampa.h"
// FIXME ce n'est pas propre du tout
//#include "pampa-mmg3d.h"

#include <ptscotch.h>
#include "dmesh_adapt.h"
#include "dmesh_adapt_common.h"
#include "dmesh_dgraph.h"
#include "dmesh_gather_induce_multiple.h"
#include "dmesh_rebuild.h"

  
/** XXX
 ** It returns:
 ** - 0   : on success.
 ** - !0  : on error.
 */

  int
dmeshAdaptGraphGrow (
    Dmesh * const    srcmeshptr,              /**< mesh */
	const Gnum                bandval,              /**< Band value */
	const Gnum                ballsiz,
        int (*EXT_meshAdapt) (PAMPA_Mesh * const imshptr, PAMPA_Mesh * const omshptr, void * const dataptr, PAMPA_Num const flagval), 
        int (*EXT_dmeshCheck) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num const flagval), 
        int (*EXT_dmeshBand) (PAMPA_Dmesh * const meshptr, void * const dataptr, PAMPA_Num * rmshloctab, PAMPA_Num ebndval), 
        int (*EXT_dmeshMetCalc) (PAMPA_Dmesh * const dmshptr, void * const dataptr, PAMPA_Num const vertlocnbr, PAMPA_Num * const vertloctab, double * const veloloctab),
        int (*EXT_meshSave) (PAMPA_Mesh * const meshptr, PAMPA_Num const solflag, void * const dataptr, PAMPA_Num * const reftab, char * const fileval), 
	void * const              dataptr,
	Dmesh * const    dstmeshptr)
{
  SCOTCH_Dgraph dgrfdat;
  SCOTCH_Dgraph bgrfdat; /* Band graph */
  SCOTCH_Strat  strtdat; 
  Gnum * restrict rmshloctax;
  Gnum * restrict rmshloctx2;
  Gnum rmshlocnbr;
  Gnum rmshglbnbr;
  Gnum baseval;
  Gnum vertlocmax;
  Gnum distval;
  Gnum seedlocnbr;
  double velolocval;
  Gnum ballglbnbr;
  Gnum vertlocnum;
  Gnum vertlocnnd;
  Gnum dvrtlocnbr;
  int rateglbnbr;
  Gnum iternbr;
  int cheklocval;
  Gnum * restrict seedloctab;
  Gnum * restrict partloctax;
  Gnum * restrict paroloctax;
  MPI_Comm proccomm;
#ifdef PAMPA_DEBUG_DMESH_SAVE
    static Gnum wmshcnt = 0;
#endif /* PAMPA_DEBUG_DMESH_SAVE */

    infoPrint("*******************\n\nGraphGrow\n\n*******************\n");
  cheklocval = 0;

  rateglbnbr = 100;

  distval = sqrt(ballsiz / 3);
  baseval = srcmeshptr->baseval;
  proccomm = srcmeshptr->proccomm;

  CHECK_FERR (SCOTCH_stratInit (&strtdat), proccomm);
  CHECK_FERR (dmeshDgraphInit (srcmeshptr, &dgrfdat), proccomm);
  CHECK_FERR (SCOTCH_dgraphInit (&bgrfdat, srcmeshptr->proccomm), proccomm);
  CHECK_VERR (cheklocval, proccomm);

#ifdef PAMPA_INFO_ADAPT
  iternbr = 1;
#endif /* PAMPA_INFO_ADAPT */

  while (1) {
	Gnum dvrtlocbas;
	Gnum bvrtlocnbr;
	Gnum bvrtlocmax;
	Gnum bvrtgstnbr;
	Gnum bvrtlocnum;
	Gnum bvrtlocnnd;
        Gnum bedglocnum;
        Gnum bedglocnnd;
	Gnum meshlocnbr;
	Gnum meshlocnum;
	Gnum fronlocidx;
	int procngbnum;
	Gnum * fronloctax;
	Gnum * restrict vnumgsttax;
	Gnum * restrict bnumgsttax;
        Gnum * restrict vertloctax;
        double * restrict veloloctax;
	Gnum * restrict bvlbloctax;
	Gnum * restrict bvrtloctax;
	Gnum * restrict bvndloctax;
	Gnum * restrict bedggsttax;
	Gnum * restrict parttax;
	Mesh * imshloctab;
	Mesh * omshloctab;

	CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
	rmshloctax -= baseval;

	vertlocmax = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval - 1;
	for (rmshlocnbr = 0, vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctax[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		rmshlocnbr ++;

  	CHECK_VDBG (cheklocval, proccomm);
  	if (commAllreduce (&rmshlocnbr, &rmshglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
      errorPrint ("communication error");
	  cheklocval = 1;
  	}
	CHECK_VERR (cheklocval, proccomm);

        //errorPrint ("rmshglbnbr: %d, vertglbnbr: %d, rmshglbnbr/vertglbnbr: %lf, rateglbnbr: %lf",
        //    rmshglbnbr, srcmeshptr->vertglbnbr, 1.0 * rmshglbnbr / srcmeshptr->vertglbnbr, rateglbnbr);

	//if (rmshglbnbr == 0)
	if ((rmshglbnbr == 0) || (iternbr > 5))
	  break;

        rateglbnbr = 100 * rmshglbnbr / srcmeshptr->vertglbnbr;
#ifdef PAMPA_INFO_ADAPT
        infoPrint ("-- Iteration number: %d", iternbr);
        infoPrint ("Number of vertices to be remeshed before dgraphBand: %d", rmshglbnbr);
#endif /* PAMPA_INFO_ADAPT */

	
	// XXX ne devrait-on pas faire cette alloc une bonne fois pour toutes les
	// itérations ?
  	if (memAllocGroup ((void **) (void *)
		  &fronloctax, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum)),
		  &rmshloctx2, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
          NULL) == NULL) {
      errorPrint ("Out of memory");
      cheklocval = 1;
  	}
  	CHECK_VERR (cheklocval, proccomm);
	fronloctax -= baseval;
	rmshloctx2 -= baseval;

        memCpy (rmshloctx2 + baseval, rmshloctax + baseval, srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (Gnum));
        //if (iternbr > 1) 
          CHECK_FERR (EXT_dmeshBand ((PAMPA_Dmesh *) srcmeshptr, dataptr, rmshloctx2 + baseval, bandval), proccomm);
        //// XXX tmp
        //if (iternbr > 1)
        //  break;
        //// XXX fin tmp
	for (fronlocidx = vertlocnum = baseval; vertlocnum <= vertlocmax; vertlocnum ++)
	  if (rmshloctx2[vertlocnum] == PAMPA_TAG_VERT_REMESH)
		fronloctax[fronlocidx ++] = vertlocnum;
        rmshlocnbr = fronlocidx - baseval;

  	// 1) Identification des zones à remailler
  	// 	a) conversion du maillage en graphe uniquement pour les sommets qui ont un drapeau
  	dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_MAIN, ~0, NULL, NULL, NULL, &dgrfdat, &dvrtlocbas); // XXX pas de velo ni de edlo ??

#ifdef PAMPA_DEBUG_DGRAPH
  	char s[50], s_2[50];
//        Gnum * vnbrloctab; /* XXX temporaire à supprimer */
//  	sprintf(s, "dgrfdat-adapt-%d", srcmeshptr->proclocnum);
  	FILE *dgraph_file, *dgraph_file2;
//  	dgraph_file = fopen(s, "w");
//  	SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
//  	fclose(dgraph_file);
//
//        if (memAllocGroup ((void **) (void *)
//              &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
//              &parttax,    (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
//              &vnbrloctab,    (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
//              NULL) == NULL) {
//          errorPrint ("Out of memory");
//          cheklocval = 1;
//        }
//        CHECK_VERR (cheklocval, proccomm);
//        vnumgsttax -= baseval;
//        parttax -= baseval;
//
//        for (vertlocnum = baseval, vertlocnnd = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
//          vnumgsttax[vertlocnum] = srcmeshptr->proclocnum;
//        for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) 
//          parttax[procngbnum + baseval] = procngbnum;
//
//        imshloctab = NULL;
//        meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
//        PAMPA_dmeshHaloValue((PAMPA_Dmesh *) srcmeshptr, 1, PAMPA_TAG_GEOM);
//        CHECK_FERR (dmeshGatherInduceMultiple (srcmeshptr, 0, srcmeshptr->procglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), proccomm);
//        sprintf (s, "dmsh-adapt-%d.mesh", srcmeshptr->proclocnum);
//        if (meshlocnbr > 0)
//          CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, dataptr, NULL, s), proccomm);
//
//        meshExit (imshloctab);
//        memFree (imshloctab);
//
//
//
//        PAMPA_Iterator it, it_nghb;
//        Gnum tetentt, nodentt, vertlocbas;
//        double * coorloctax;
//        int i;
//
//        tetentt = 0;
//        nodentt = 1;
//        dmeshValueData(srcmeshptr, nodentt, PAMPA_TAG_GEOM, NULL, NULL, (void **) &coorloctax);
//        coorloctax -= 3 * baseval;
//  	sprintf(s, "dgrfdat-adapt.xyz");
//  	dgraph_file = fopen(s, "w");
//  	sprintf(s_2, "dgrfdat-adapt.map");
//  	dgraph_file2 = fopen(s_2, "w");
//        if (srcmeshptr->proclocnum == 0) {
//          fprintf (dgraph_file, "3\n%d\n", srcmeshptr->enttloctax[baseval]->vertglbnbr);
//          fprintf (dgraph_file2, "%d\n", srcmeshptr->enttloctax[baseval]->vertglbnbr);
//        }
//        fclose (dgraph_file);
//        fclose (dgraph_file2);
//
//        if (commAllgather (&srcmeshptr->enttloctax[baseval]->vertlocnbr, 1, GNUM_MPI,
//              vnbrloctab, 1, GNUM_MPI, srcmeshptr->proccomm) != MPI_SUCCESS) {
//          errorPrint ("communication error");
//          return     (1);
//        }
//
//        Gnum procnum;
//        Gnum procnnd;
//
//        vertlocbas = 0;
//        for (procngbnum = 0; procngbnum < srcmeshptr->proclocnum ; procngbnum ++)
//          vertlocbas += vnbrloctab[procngbnum];
//
//        for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) {
//          commBarrier (srcmeshptr->proccomm);
//          
//          if (procngbnum == srcmeshptr->proclocnum) {
//            dgraph_file = fopen(s, "a");
//            dgraph_file2 = fopen(s_2, "a");
//            PAMPA_dmeshItInitStart((PAMPA_Dmesh *) srcmeshptr, tetentt, PAMPA_VERT_LOCAL, &it);
//            PAMPA_dmeshItInit((PAMPA_Dmesh *) srcmeshptr, tetentt, nodentt, &it_nghb);
//            while (PAMPA_itHasMore(&it)) {
//              PAMPA_Num tetnum;
//              double coorval[3];
//
//              tetnum = PAMPA_itCurEnttVertNum(&it);
//              coorval[0] =
//                coorval[1] =
//                coorval[2] = 0.0;
//
//
//              PAMPA_itStart(&it_nghb, tetnum);
//
//              while (PAMPA_itHasMore(&it_nghb)) {
//                PAMPA_Num nodenum;
//
//                nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
//                for (i = 0; i < 3; i++)
//                  coorval[i] += coorloctax[nodenum * 3 + i];
//                PAMPA_itNext(&it_nghb);
//              }
//
//              fprintf(dgraph_file, "%d", vertlocbas + tetnum);
//              fprintf(dgraph_file2, "%d %d\n", vertlocbas + tetnum, srcmeshptr->proclocnum);
//              for (i = 0; i < 3; i++)
//                fprintf(dgraph_file, " %lf", coorval[i] / 4);
//              fprintf(dgraph_file, "\n");
//
//              PAMPA_itNext(&it);
//            }
//            fclose (dgraph_file); 
//            fclose (dgraph_file2); 
//          }
//        }
//
//        memFree (vnumgsttax);
//        commBarrier(MPI_COMM_WORLD);
//        exit (0);
#endif /* PAMPA_DEBUG_DGRAPH */

  	// 	a bis) élargissement des zones avec un dgraphBand ok
        //if (bandval > 0) {
	//  CHECK_FERR (SCOTCH_dgraphBand (&dgrfdat, rmshlocnbr, fronloctax + baseval, bandval, &bgrfdat), proccomm);
        //}
        //else {
          // 	b) sous-graphe induit des zones si pas de bande
          CHECK_FERR (SCOTCH_dgraphInducePart (&dgrfdat, rmshloctx2 + baseval, PAMPA_TAG_VERT_REMESH, rmshlocnbr, &bgrfdat), proccomm);
        //}

#ifdef PAMPA_DEBUG_DGRAPH
  	sprintf(s, "bgrfdat-adapt-%d", srcmeshptr->proclocnum);
  	dgraph_file = fopen(s, "w");
  	SCOTCH_dgraphSave (&dgrfdat, dgraph_file);
  	fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

#ifdef PAMPA_DEBUG_ADAPT2
        CHECK_FERR (SCOTCH_dgraphCheck (&bgrfdat), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */
        CHECK_FERR (SCOTCH_dgraphGhst (&bgrfdat), proccomm);
        //SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, &bvrtgstnbr, (SCOTCH_Num **) &bvrtloctax, (SCOTCH_Num **) &bvndloctax, NULL, (SCOTCH_Num **) NULL, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bedggsttax, NULL, NULL); 
        SCOTCH_dgraphData (&bgrfdat, NULL, NULL, &bvrtlocnbr, NULL, &bvrtgstnbr, (SCOTCH_Num **) &bvrtloctax, (SCOTCH_Num **) &bvndloctax, NULL, (SCOTCH_Num **) &bvlbloctax, NULL, NULL, NULL, NULL, (SCOTCH_Num **) &bedggsttax, NULL, NULL); 
        bvlbloctax -= baseval;
	bvrtloctax -= baseval;
	bvndloctax -= baseval;
	bedggsttax -= baseval;
        bvrtlocmax = bvrtlocnbr + baseval - 1;

  	// 	c) contraction du graphe des zones ok
        if (memAllocGroup ((void **) (void *)
              &bnumgsttax, (size_t) (bvrtgstnbr * sizeof (Gnum)),
              //&bvlbloctax, (size_t) (bvrtlocnbr * sizeof (Gnum)), 
              &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
              &vertloctax, (size_t) (bvrtlocnbr * sizeof (Gnum)),
              &veloloctax, (size_t) (srcmeshptr->enttloctax[baseval]->vertlocnbr * sizeof (double)), // XXX on est un peu large !!!
              &seedloctab, (size_t) (bvrtlocnbr * sizeof (Gnum)),
              &parttax,    (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
              NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
  	CHECK_VERR (cheklocval, proccomm);
  	bnumgsttax -= baseval;
  	vnumgsttax -= baseval;
        vertloctax -= baseval;
        veloloctax -= baseval;
        parttax -= baseval;
	//bvlbloctax -= baseval;
  	memSet (vnumgsttax + baseval, ~0, srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum));
  	memSet (bnumgsttax + baseval, ~0, bvrtgstnbr * sizeof (Gnum));

    if (rmshglbnbr > ballsiz) {
#ifdef PAMPA_INFO_ADAPT
        infoPrint ("-- Iteration number: %d", iternbr);
        infoPrint ("Number of vertices to be remeshed : %d", rmshglbnbr);
#endif /* PAMPA_INFO_ADAPT */

      Gnum vertlocidx;
      Gnum seedlocnum;
      //Gnum dvrtlocnum;
      //Gnum dvrtlocnnd;

      //dvrtlocnbr = vertlocmax - baseval + 1;
      //for (seedlocnbr = 0, dvrtlocnum = baseval, dvrtlocnnd = dvrtlocnbr + baseval; dvrtlocnum < dvrtlocnnd; dvrtlocnum ++) 
      //  if (rmshloctax[dvrtlocnum] == PAMPA_TAG_VERT_REMESH) {
      //    vnumgsttax[dvrtlocnum] = srcmeshptr->proclocnum;
      //    seedloctab[seedlocnbr ++] = dvrtlocnum;
      //    break;
      //  }

      //CHECK_FERR (SCOTCH_dgraphGrow (&dgrfdat, seedlocnbr, seedloctab, distval, vnumgsttax + baseval), proccomm);
      
      CHECK_FERR (dmeshValueData (srcmeshptr, baseval, PAMPA_TAG_REMESH, NULL, NULL, (void **) &rmshloctax), proccomm);
      rmshloctax -= baseval;
      for (vertlocidx = 0, bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++, vertlocidx ++) 
        vertloctax[vertlocidx] = bvlbloctax[bvrtlocnum] - dvrtlocbas;
      CHECK_FERR (EXT_dmeshMetCalc ((PAMPA_Dmesh *) srcmeshptr, dataptr, bvrtlocnbr, vertloctax + baseval, veloloctax + baseval), proccomm);

      for (velolocval = seedlocnbr = 0, bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {

        if ((rmshloctax[bvlbloctax[bvrtlocnum] - dvrtlocbas] == PAMPA_TAG_VERT_REMESH) && (veloloctax[bvlbloctax[bvrtlocnum] - dvrtlocbas] > velolocval)) {
          seedlocnum = bvrtlocnum;
          velolocval = veloloctax[bvrtlocnum];
        }
      }

      //errorPrint ("velolocval : %d, seedlocnum : %d", veloloctax[seedlocnum], seedlocnum);
      bnumgsttax[seedlocnum] = srcmeshptr->proclocnum;
      seedloctab[seedlocnbr ++] = seedlocnum;

#ifdef PAMPA_DEBUG_DGRAPH
      int i, n;
  	sprintf(s, "bgrfdat-adapt-extra-%d", srcmeshptr->proclocnum);
  	dgraph_file = fopen(s, "w");
        fprintf(dgraph_file, "%d\n", seedlocnbr);
        for (i = 0; i < seedlocnbr; i ++)
          fprintf(dgraph_file, "%d ", seedloctab[i]);
        fprintf(dgraph_file, "\n%d\n", bvrtgstnbr);
        for (i = baseval, n = baseval + bvrtgstnbr; i < n; i ++)
          fprintf(dgraph_file, "%d ", bnumgsttax[i]);
        fprintf(dgraph_file, "\n");
  	fclose(dgraph_file);
#endif /* PAMPA_DEBUG_DGRAPH */

      CHECK_FERR (SCOTCH_dgraphGrow (&bgrfdat, seedlocnbr, seedloctab, distval, bnumgsttax + baseval), proccomm);

      for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
            if (bnumgsttax[bvrtlocnum] != ~0)
              vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = bnumgsttax[bvrtlocnum];
	  ///* First pass to remove elements which have neighbors with different color */
  	  //for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) {
	  //	Gnum colrlocnum;
	  //	Gnum ucollocval; /* If undo coloring */

	  //	colrlocnum = bnumgsttax[bvrtlocnum];

	  //	for (ucollocval = 0, bedglocnum = bvrtloctax[bvrtlocnum], bedglocnnd = bvndloctax[bvrtlocnum]; bedglocnum < bedglocnnd; bedglocnum ++) /* Undo coloring on elements which are at the boundary */
	  //    if (bnumgsttax[bedggsttax[bedglocnum]] != colrlocnum) { /* If vertex and neighbor have different colors */
	  //    	ucollocval = 1;
	  //    	if (bedggsttax[bedglocnum] <= bvrtlocmax) 
	  //  	  vnumgsttax[bvlbloctax[bedggsttax[bedglocnum]] - dvrtlocbas] = ~0;
	  //    }
	  //	if (ucollocval == 1) { /* There are at least one neighbor with different color */
	  //    vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = ~0;
	  //	  // FIXME il faudrait diminuer le cvelloctax pour cette couleur
	  //	}
	  //}




          CHECK_VDBG (cheklocval, proccomm);
          if (commAllreduce (&seedlocnbr, &ballglbnbr, 1, GNUM_MPI, MPI_SUM, proccomm) != MPI_SUCCESS) {
            errorPrint ("communication error");
            cheklocval = 1;
          }
          CHECK_VERR (cheklocval, proccomm);

          for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) 
            parttax[procngbnum + baseval] = procngbnum;
        }
	else {
#ifdef PAMPA_INFO_ADAPT
		infoPrint ("-- Iteration number: %d", iternbr);
	  	infoPrint ("Number of vertices to be remeshed : %d", rmshglbnbr);
	  	infoPrint ("Last iteration");
#endif /* PAMPA_INFO_ADAPT */
	  memSet (bnumgsttax + baseval, 0, bvrtgstnbr * sizeof (Gnum));
  	  for (bvrtlocnum = baseval, bvrtlocnnd = bvrtlocnbr + baseval; bvrtlocnum < bvrtlocnnd; bvrtlocnum ++) 
	  	vnumgsttax[bvlbloctax[bvrtlocnum] - dvrtlocbas] = 0; /* Propagate colors on elements */
	  parttax[baseval] = 0; // FIXME pourquoi 0 ???
	  ballglbnbr = 1;
	}
        iternbr ++;
        SCOTCH_dgraphFree (&bgrfdat);
	memFree (fronloctax + baseval);

#ifdef PAMPA_DEBUG_DMESH_SAVE2
        char s1[50];
        sprintf (s1, "dmesh-adapt-avant-%d", wmshcnt);
        CHECK_FERR (dmeshAdaptGatherSave2 (srcmeshptr, vnumgsttax, EXT_meshSave, dataptr, s1), proccomm);
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

        // 2) Extraction des sous-maillages et envoie sur chaque proc ok
        meshlocnbr = -1;
        imshloctab = NULL;
        CHECK_FERR_STR (dmeshGatherInduceMultiple (srcmeshptr, 1, ballglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), "extraction", proccomm);

#ifdef PAMPA_INFO_ADAPT
	infoPrint ("Number of received meshes : %d", meshlocnbr);
	infoPrint ("Number of local vertices in source distributed mesh : %d", srcmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

        memFree (bnumgsttax + baseval);

        // 3) Remaillage des zones
        // 	a) conversion pampa -> mmg ok
        // 	b) remaillage ok
        // 	c) conversion mmg -> pampa quasi ok
        if ((omshloctab = (Mesh *) memAlloc (meshlocnbr * sizeof (Mesh))) == NULL) {
          errorPrint  ("out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);

        for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) { // XXX cette partie pourrait être parallélisé avec pthread
          Gnum * restrict inbrtab;
          Gnum * restrict onbrtab;

#ifdef PAMPA_DEBUG_MESH_SAVE
          char s2[50];
          sprintf (s2, "imsh-adapt-%d.mesh", imshloctab[meshlocnum].idnum);
          CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (imshloctab + meshlocnum), 1, dataptr, NULL, s2), proccomm);
#endif /* PAMPA_DEBUG_MESH_SAVE */

#ifdef PAMPA_DEBUG_ADAPT2
          CHECK_FERR (meshCheck (imshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

          meshInit (omshloctab + meshlocnum);

          CHECK_FERR_STR (EXT_meshAdapt ((PAMPA_Mesh *) (imshloctab + meshlocnum), (PAMPA_Mesh *) (omshloctab + meshlocnum), dataptr, 0), "remesh zone", proccomm); // FIXME remplacer le 0 par un drapeau
          omshloctab[meshlocnum].idnum = imshloctab[meshlocnum].idnum;
          omshloctab[meshlocnum].procglbnbr = imshloctab[meshlocnum].procglbnbr;
          CHECK_FERR (meshValueData (imshloctab + meshlocnum, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART, NULL, NULL, (void **) &inbrtab), srcmeshptr->proccomm);
          CHECK_FERR (meshValueLink(omshloctab + meshlocnum, (void **) &onbrtab, PAMPA_VALUE_NONE, NULL, NULL, GNUM_MPI, PAMPA_ENTT_VIRT_PROC, PAMPA_TAG_PART), srcmeshptr->proccomm);
          memCpy (onbrtab, inbrtab, srcmeshptr->procglbnbr * sizeof (Gnum));
          // FIXME : Et s'il y a d'autres valeurs associées, ne devrait-on pas
          // les recopier, sachant que :
          // 1) elles sont récupérées lors du rebuild
          // 2) pour les entt réelles, seule la routine PAMPA_remesh peut le faire

#ifdef PAMPA_DEBUG_MESH_SAVE
          sprintf (s2, "omsh-adapt-%d.mesh", omshloctab[meshlocnum].idnum);
          CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (omshloctab + meshlocnum), 1, dataptr, NULL, s2), proccomm);
#endif /* PAMPA_DEBUG_MESH_SAVE */

#ifdef PAMPA_DEBUG_ADAPT2
          CHECK_FERR (meshCheck (omshloctab + meshlocnum), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

          meshExit (imshloctab + meshlocnum);
        }
        memFree (imshloctab);
        CHECK_VDBG (cheklocval, proccomm);


        // 4) Reintegration des zones dans maillage distribué en cours
        // errorPrint ("à modifier pour le rebuild");
        // return (1);
	CHECK_FERR_STR (dmeshRebuild (srcmeshptr, meshlocnbr, omshloctab), "reintegration", proccomm);

#ifdef PAMPA_DEBUG_ADAPT2
        CHECK_FERR (dmeshCheck (srcmeshptr), proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

#ifdef PAMPA_INFO_ADAPT
	infoPrint ("Number of local vertices in destination distributed mesh : %d", srcmeshptr->vertlocnbr);
#endif /* PAMPA_INFO_ADAPT */

#ifdef PAMPA_DEBUG_DMESH_SAVE2
        sprintf (s1, "dmesh-adapt-apres-%d", wmshcnt ++);
        CHECK_FERR (dmeshAdaptGatherSave (srcmeshptr, NULL, EXT_meshSave, dataptr, s1), proccomm);
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */

        for (meshlocnum = 0; meshlocnum < meshlocnbr ; meshlocnum ++) 
          meshFree (omshloctab + meshlocnum);

        memFree (omshloctab);

#ifdef PAMPA_DEBUG_DMESH_SAVE
#ifndef PAMPA_DEBUG_DMESH_SAVE2
        char s1[50];
#endif /* PAMPA_DEBUG_DMESH_SAVE2 */
        //sprintf (s1, "dmesh-adapt-%d-%d", srcmeshptr->proclocnum, wmshcnt ++);
        //CHECK_FERR (dmeshAdaptGatherSave (srcmeshptr, NULL, dataptr, s1), proccomm);
        if (memAllocGroup ((void **) (void *)
              &vnumgsttax, (size_t) (srcmeshptr->enttloctax[baseval]->vgstlocnbr * sizeof (Gnum)),
              &parttax,    (size_t) (srcmeshptr->procglbnbr * sizeof (Gnum)),
              NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
        }
        CHECK_VERR (cheklocval, proccomm);
        vnumgsttax -= baseval;
        parttax -= baseval;

        for (vertlocnum = baseval, vertlocnnd = srcmeshptr->enttloctax[baseval]->vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
          vnumgsttax[vertlocnum] = srcmeshptr->proclocnum;
        for (procngbnum = 0; procngbnum < srcmeshptr->procglbnbr; procngbnum ++) 
          parttax[procngbnum + baseval] = procngbnum;

        imshloctab = NULL;
        meshlocnbr = -1; // FIXME l'allocation de imshloctab ne devrait pas être faite ici ?
        CHECK_FERR (dmeshGatherInduceMultiple (srcmeshptr, 0, srcmeshptr->procglbnbr, vnumgsttax, parttax, &meshlocnbr, &imshloctab), proccomm);
        sprintf (s1, "dmsh-adapt-%d-%d.mesh", srcmeshptr->proclocnum, wmshcnt ++);
        CHECK_FERR (EXT_meshSave((PAMPA_Mesh *) (imshloctab), 1, dataptr, NULL, s1), proccomm);

        meshExit (imshloctab);
        memFree (imshloctab);
        memFree (vnumgsttax);
#endif /* PAMPA_DEBUG_DMESH_SAVE */

    CHECK_FERR (dmeshDgraphFree (&dgrfdat), proccomm);
  }

  CHECK_FERR_STR (dmeshDgraphBuild (srcmeshptr, DMESH_ENTT_ANY, ~0, NULL, NULL, NULL, &dgrfdat, NULL), "graphe final", proccomm); // XXX pas de velo ni de edlo ??
  SCOTCH_dgraphSize (&dgrfdat, NULL, &dvrtlocnbr, NULL, NULL);

  if (memAllocGroup ((void **) (void *)
		&paroloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
		&partloctax, (size_t) (dvrtlocnbr * sizeof (Gnum)),
        NULL) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  paroloctax -= baseval;
  partloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = dvrtlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
   	paroloctax[vertlocnum] = srcmeshptr->proclocnum;

  //// 5) Repartitionnement
  //vmloloctab = NULL;
  //emraval = 1;
  //CHECK_FERR (SCOTCH_stratDgraphMap (&strtdat, "q{strat=m{asc=b{width=3,bnd=(d{pass=40,dif=1,rem=0}|)f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.01,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=0}}"), proccomm);
  //CHECK_FERR (SCOTCH_dgraphRepart (&dgrfdat, srcmeshptr->procglbnbr, paroloctax + baseval, emraval, vmloloctab, &strtdat, partloctax + baseval), proccomm);

  dmeshDgraphExit (&dgrfdat);
  SCOTCH_dgraphExit (&bgrfdat);
  SCOTCH_stratExit (&strtdat);
  // 5 bis) Redistribution ok
  //CHECK_FERR (dmeshRedist (srcmeshptr, partloctax, NULL, -1, -1, -1, dstmeshptr), proccomm); // XXX peut-être utiliser un dsmeshRedist qui serait moins gourmand en mémoire à mon avis
  memFree (paroloctax + baseval);
  CHECK_VDBG (cheklocval, proccomm);
  return (0);
} 

