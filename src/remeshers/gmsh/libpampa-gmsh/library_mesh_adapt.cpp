/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_adapt.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 May 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/
/****************************************/
/*                                      */
/* These routines are the CPP API for   */
/* the remesher handling routines.      */
/*                                      */
/****************************************/

#ifdef PAMPA_TIME_CHECK2
#define PAMPA_TIME_CHECK
#endif /* PAMPA_TIME_CHECK2 */

#include <mpi.h>
extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
#include "pampa.h"
//#include "../../common/library_mesh_induce_multiple.h"
//#include "../../common/library_mesh_rebuild.h"
}
#include <discreteRegion.h>
#include <MVertex.h>
#include <MTetrahedron.h>
#include <Gmsh.h>
#include "pampa-gmsh.h"
#include "rmesh_to_pmesh.h"
#include "pmesh_to_rmesh.h"
#include "types.h"

// XXX peut-être qu'on peut fusionner les fichiers meshAdapt de Tetgen, Gmsh (et Mmg) ?

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_GMSH_meshAdapt (
PAMPA_Mesh * const         imshptr,             //!< PaMPA centralized in mesh
PAMPA_Mesh * const         omshptr,             //!< PaMPA centralized out mesh
PAMPA_AdaptInfo * const    infoptr,             //!< Information used by the remesher
PAMPA_Num const            flagval)             //!< Flags used by the remesher
{

  GModel* rmshdat;
  PAMPA_GMSH_Data * data;
  double * restrict osoltax;
  double * restrict owghtax;
  PAMPA_Num* restrict oflgtax;
  PAMPA_Num* restrict ormstax;
  PAMPA_Num* iflgtax;
  PAMPA_Num chekval;
  PAMPA_Num baseval;
  PAMPA_Num nodenum;
  PAMPA_Num nodennd;
  PAMPA_Num vertnbr;
  PAMPA_Num tetrnbr;
  PAMPA_Num nodenbr;
  PAMPA_Num offset;
  int mbnd;
  int mint;
  int num;
  
  PAMPA_Mesh imintdat;
  PAMPA_Mesh omintdat;
  PAMPA_Mesh mbnddat;
  
  // XXX type soltax
  MPI_Datatype  metrtyp;
  
  char* arg;
  if ((arg = (char*)memAlloc(5)) == NULL) {
    errorPrint ("Out of memory");
    return 1;
  }
  snprintf (arg, 4, "gmsh");
  GmshInitialize(1, &arg);
  memFree (arg);
  
//  GmshInitialize();
  GmshSetOption("General", "Terminal", 1.);
  GmshSetOption("General", "Verbosity", 4.);
  GmshSetOption("Mesh", "CharacteristicLengthExtendFromBoundary", 1.);
  GmshSetOption("Mesh", "OldRefinement", 1.);
  GmshSetOption("Mesh", "CharacteristicLengthMin", 0.1);
  GmshSetOption("Mesh", "CharacteristicLengthMax", 0.1);

  GmshSetOption("Mesh", "Optimize", 0.); // not yet: need boundary!
  
  baseval = 0;

  data = (PAMPA_GMSH_Data *) infoptr->dataptr;
  rmshdat = new GModel();

  PAMPA_meshData (imshptr, NULL, NULL, &vertnbr, NULL, NULL, NULL, NULL);
  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &iflgtax));
  iflgtax -= baseval;
  mbnd = 0;
  mint = 0;
  for (num = baseval ; num < vertnbr && (mint == 0 || mbnd == 0) ; ++num) {
    if (iflgtax[num] != PAMPA_TAG_VERT_INTERNAL) {
      ++mbnd;
    }
    else {
      ++mint;
    }
  }
  
  if (mint == 0) { // There is no internal mesh
#ifdef PAMPA_DEBUG_ADAPT
    printf("DEBUG: There is no internal mesh\n");
#endif
    PAMPA_Smesh smshdat;
    PAMPA_smeshInit(&smshdat);
    PAMPA_mesh2smesh(imshptr, &smshdat);
    PAMPA_smesh2mesh(&smshdat, omshptr);
    PAMPA_smeshExit (&smshdat); // XXX mettre toute cette partie dans meshCopy en attendant de faire un meshCopy propre ;-)
    return (0);
  }
  
  if (mbnd != 0) { // There is internal and boundary meshes
#ifdef PAMPA_DEBUG_ADAPT
    printf("DEBUG: There is internal and boundary mesh\n");
#endif
    infoptr->tetrent = data->tetrent;
    infoptr->faceent = data->faceent;
    infoptr->nodeent = data->nodeent;
    infoptr->coortyp = data->coortyp;
    errorPrint ("modifier l'inducemultiple");
    return 1;
    //CHECK_FDBG2 (PAMPA_meshInduceMultiple(imshptr, &imintdat, &mbnddat, infoptr));
    //// XXX debut tmp
    //{
    //  FILE * meshout;
    //  meshout = fopen ("bmesh.pampa", "w");
    //  PAMPA_meshSaveAll (&mbnddat, meshout);
    //  fclose (meshout);
    //  meshout = fopen ("imesh.pampa", "w");
    //  PAMPA_meshSaveAll (&imintdat, meshout);
    //  fclose (meshout);
    //  exit (0);
    //}
    //// XXX fin tmp
    
    CHECK_FDBG2 (pmesh2rmesh (&imintdat, data, flagval | PAMPA_GMSH_ADAPT, rmshdat));
    for(GModel::riter it = rmshdat->firstRegion(); it != rmshdat->lastRegion(); ++it){
      discreteRegion *r = dynamic_cast<discreteRegion*>(*it);
      if(r){
        printf("found a discrete region (%d) with %d tetrahedra\n", r->tag(), r->tetrahedra.size());
        r->remesh();
        printf("after remeshing: %d tetrahedra\n", r->tetrahedra.size());
      }
    }
#ifdef PAMPA_DEBUG_ADAPT_SAVE
  {
  char s[100];
    sprintf (s, "apres_adapt-%d-%d-%d-%d.msh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    rmshdat->writeMSH(s);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE */
    PAMPA_meshExit (&imintdat);
    CHECK_FDBG2 (rmesh2pmesh (rmshdat, data, 0, flagval | PAMPA_GMSH_ADAPT, &omintdat));

/*    switch (outrmshdat.numberofpointmtrs) {
        MPI_Datatype  metrtyp;
      case 1 :
        PAMPA_meshValueLink (&omintdat, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
        // XXX
        memSet (osoltax, 0, nodenbr * sizeof (MPI_DOUBlE));
        break;
      case 6 :
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (&omintdat, (void **) &osoltax, PAMPA_VALUE_PUBLIC, metrtyp, data->nodeent, PAMPA_TAG_SOL_3DAI);
        // XXX
        memSet (osoltax, 0, nodenbr * sizeof (metryp));
        break;
      default :
        errorPrint ("unrecognized metric");
        return (1);
    }*/
//    osoltax -= outrmshdat.numberofpointmtrs * baseval;
    PAMPA_meshEnttSize (&omintdat, data->nodeent, &nodenbr);
    // XXX TODO
    chekval = PAMPA_meshValueData(&mbnddat, data->nodeent, PAMPA_TAG_SOL_3DI, (void **) &osoltax);
    if (chekval == 0) {  /* If there is a value with this tag */
      PAMPA_meshValueLink (&omintdat, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
      // XXX
//      memSet (osoltax, 0, nodenbr * sizeof (MPI_DOUBLE));
      osoltax -= baseval;
      for (num = baseval ; num < nodenbr + baseval ; ++num) {
        osoltax[num] = 0;
      }
    }
    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (chekval);
    }
    else { /* If there no value with this tag */
      chekval = PAMPA_meshValueData(&mbnddat, data->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &osoltax); 
      if (chekval == 0) {  /* If there is a value with this tag */
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (&omintdat, (void **) &osoltax, PAMPA_VALUE_PUBLIC, metrtyp, data->nodeent, PAMPA_TAG_SOL_3DAI);
        // XXX
//        memSet (osoltax, 0, nodenbr * sizeof (metrtyp));
        osoltax -= baseval;
        for (num = baseval ; num < nodenbr + baseval ; ++num) {
          osoltax[num] = 0;
        }
      }
      else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
        return (chekval);
      }
      else { /* If there no value with this tag */
        errorPrint ("No associated metric");
        return (chekval);
      }
    }
    
    PAMPA_meshData(&omintdat, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);
    PAMPA_meshEnttSize (&omintdat, data->tetrent, &tetrnbr);
    PAMPA_meshEnttSize (&omintdat, data->nodeent, &nodenbr);
    
    chekval = PAMPA_meshValueLink (&omintdat, (void **) &oflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS);
    if (chekval != 0)
      return chekval;
    oflgtax -= baseval;
    for (num = baseval ; num < vertnbr + baseval ; ++num) {
      oflgtax[num] = PAMPA_TAG_VERT_INTERNAL;
    }
    
    chekval = PAMPA_meshValueLink (&omintdat, (void **) &ormstax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, infoptr->tetrent, PAMPA_TAG_REMESH);
    if (chekval != 0)
      return chekval;
    memSet (ormstax, PAMPA_TAG_VERT_NOREMESH, tetrnbr * sizeof (PAMPA_Num));
    
    chekval = PAMPA_meshValueLink (&omintdat, (void **) &owghtax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, infoptr->tetrent, PAMPA_TAG_WEIGHT);
    if (chekval != 0)
      return chekval;
    owghtax -= baseval;
    for (num = baseval ; num < tetrnbr + baseval; ++num) {
      owghtax[num] = 0;
    }
    
    /*for (nodenum = outrmshdat.firstnumber, nodennd = outrmshdat.numberofpoints + outrmshdat.firstnumber; nodenum < nodennd; nodenum ++)
      for (offset = 0; offset < outrmshdat.numberofpointmtrs; offset ++)
        osoltax[nodenum * outrmshdat.numberofpointmtrs + offset] = outrmshdat.pointmtrlist[nodenum * outrmshdat.numberofpointmtrs + offset];*/
    
    CHECK_FDBG2 (PAMPA_meshRebuild(&mbnddat, &omintdat, omshptr, 1, 2, 3, 4, NULL));
    PAMPA_meshExit (&mbnddat);
    PAMPA_meshExit (&imintdat);
    PAMPA_meshExit (&omintdat);
  }
  else { // There is no boundary mesh
#ifdef PAMPA_DEBUG_ADAPT
    printf("DEGUB: No boundary mesh\n");
#endif
  
    CHECK_FDBG2 (pmesh2rmesh (imshptr, data, flagval | PAMPA_GMSH_ADAPT, rmshdat));
    for(GModel::riter it = rmshdat->firstRegion(); it != rmshdat->lastRegion(); ++it){
      discreteRegion *r = dynamic_cast<discreteRegion*>(*it);
      if(r){
        printf("found a discrete region (%d) with %d tetrahedra\n", r->tag(), r->tetrahedra.size());
        r->remesh();
        printf("after remeshing: %d tetrahedra\n", r->tetrahedra.size());
      }
    }
    CHECK_FDBG2 (rmesh2pmesh (rmshdat, data, 0, flagval | PAMPA_GMSH_ADAPT, omshptr));
    
    PAMPA_meshData(omshptr, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);
    PAMPA_meshEnttSize (omshptr, data->tetrent, &tetrnbr);
    PAMPA_meshEnttSize (omshptr, data->nodeent, &nodenbr);
    
/*    switch (outrmshdat.numberofpointmtrs) {
        MPI_Datatype  metrtyp;
      case 1 :
        PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
        // XXX
        memSet (osoltax, 0, nodenbr * sizeof (MPI_DOUBLE));
        break;
      case 6 :
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, metrtyp, data->nodeent, PAMPA_TAG_SOL_3DAI);
        // XXX
        memSet (osoltax, 0, nodenbr * sizeof (metrtyp));
        break;
      default :
        errorPrint ("unrecognized metric");
        return (1);
    }*/
//    osoltax -= outrmshdat.numberofpointmtrs * baseval;
    // XXX TODO
    chekval = PAMPA_meshValueData(imshptr, data->nodeent, PAMPA_TAG_SOL_3DI, (void **) &osoltax);
    if (chekval == 0) { /* If there is a value with this tag */
      PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
      // XXX
//      memSet (osoltax, 0, nodenbr * sizeof (MPI_DOUBLE));
      osoltax -= baseval;
      for (num = baseval ; num < nodenbr + baseval ; ++num) {
        osoltax[num] = 0;
      }
    }
    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (chekval);
    }
    else { /* If there no value with this tag */
      chekval = PAMPA_meshValueData(imshptr, data->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &osoltax); 
      if (chekval == 0) { /* If there is a value with this tag */
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, metrtyp, data->nodeent, PAMPA_TAG_SOL_3DAI);
        // XXX
//        memSet (osoltax, 0, nodenbr * sizeof (metrtyp));
        osoltax -= baseval;
        for (num = baseval ; num < nodenbr + baseval ; ++num) {
          osoltax[num] = 0;
        }
      }
      else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
        return (chekval);
      }
      else { /* If there no value with this tag */
        errorPrint ("No associated metric");
        return (chekval);
      }
    }
    
    chekval = PAMPA_meshValueLink (omshptr, (void **) &oflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS);
    if (chekval != 0)
      return chekval;
    oflgtax -= baseval;
    for (num = baseval ; num < vertnbr + baseval ; ++num) {
      oflgtax[num] = PAMPA_TAG_VERT_INTERNAL;
    }
    
    CHECK_FDBG2 (PAMPA_meshValueLink (omshptr, (void **) &ormstax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, data->tetrent, PAMPA_TAG_REMESH));
    ormstax -= baseval;

    memSet (ormstax + baseval, PAMPA_TAG_VERT_NOREMESH, tetrnbr * sizeof (PAMPA_Num));
    
    CHECK_FDBG2 (PAMPA_meshValueLink (omshptr, (void **) &owghtax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->tetrent, PAMPA_TAG_WEIGHT));
    owghtax -= baseval;

    memSet (owghtax, 0, tetrnbr * sizeof (MPI_DOUBLE));
    
    /*for (nodenum = outrmshdat.firstnumber, nodennd = outrmshdat.numberofpoints + outrmshdat.firstnumber; nodenum < nodennd; nodenum ++)
      for (offset = 0; offset < outrmshdat.numberofpointmtrs; offset ++)
        osoltax[nodenum * outrmshdat.numberofpointmtrs + offset] = outrmshdat.pointmtrlist[nodenum * outrmshdat.numberofpointmtrs + offset];*/
  }
  
#ifdef PAMPA_DEBUG_ADAPT
  PAMPA_Num ivertnbr;
  PAMPA_Num overtnbr;
  
  PAMPA_meshData (imshptr, NULL, NULL, &ivertnbr, NULL, NULL, NULL, NULL);
  PAMPA_meshData (omshptr, NULL, NULL, &overtnbr, NULL, NULL, NULL, NULL);
  
  if ((ivertnbr > 0) && (overtnbr == 0)) {
    errorPrint ("internal error, overtnbr == 0");
  }
#endif /* PAMPA_DEBUG_ADAPT */

  delete rmshdat;
  GmshFinalize();
  infoptr->cuntval ++;
  return (0);
}

