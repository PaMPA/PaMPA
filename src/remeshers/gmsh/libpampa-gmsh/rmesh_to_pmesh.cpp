/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        rmesh_to_pmesh.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 May 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/
/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

#include <mpi.h>
extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
#include "pampa.h"
}
//#include "rmesh_to_pmesh.h"
#include <discreteRegion.h>
#include <discreteFace.h>
#include <MVertex.h>
#include <MTetrahedron.h>
#include <MTriangle.h>
#include "pampa-gmsh.h"
#include "types.h"

#define FINISH 1
#if !FINISH // XXX en cours de création

//! \brief This routine initializes the
//! mesh structure of the tetgen remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
rmesh2pmesh (
GModel      * const         rmshptr,              //!< TETGEN mesh
PAMPA_GMSH_Data * const     dataptr,              //!< XXX
PAMPA_Num     const         baseval,              //!< PaMPA baseval
PAMPA_Num     const         flagval,              //!< If adaptation
PAMPA_Mesh  * const         pmshptr)              //!< PaMPA centralized mesh
{
  PAMPA_Num  vertnbr;
  PAMPA_Num* verttax;
  PAMPA_Num* vendtax;
  PAMPA_Num  edgenbr;
  PAMPA_Num  edgesiz;
  PAMPA_Num* edgetax;
  PAMPA_Num* venttax;
  PAMPA_Num  enttnbr;
  
  PAMPA_Num  nodenbr;
  PAMPA_Num  nodebas;
  PAMPA_Num* nnbrtax;
  PAMPA_Num  nodeind;
  PAMPA_Num  chekval;
  PAMPA_Num	ptetnum;
  
  PAMPA_Num* ereftax;
  PAMPA_Num* nreftax;
  double* geomtax;
  std::vector<MVertex*>::iterator nit;
  
  int i;
  
  // XXX
  enttnbr = 3;
  
  // XXX FIXME on suppose qu'il n'y a qu'une région
  for(GModel::riter it = rmshptr->firstRegion(); it != rmshptr->lastRegion(); ++it){
    discreteRegion *r = dynamic_cast<discreteRegion*>(*it);
        
    for (i = 0, nit = r->mesh_vertices.begin() ; nit != r->mesh_vertices.end() ; ++nit, ++i) {
      MVertex *node = dynamic_cast<MVertex*>(*nit);
      
      node->setIndex(i);
    }
    
    if (!r) {
      errorPrint("GMSH error");
      return (1);
    }
    vertnbr  = r->mesh_vertices.size();
    nodenbr  = vertnbr;
    vertnbr += r->tetrahedra.size();
    nodebas  = r->tetrahedra.size();
    
    if (memAllocGroup ((void **) (void *)
          &verttax,   (size_t) ((vertnbr + 1) * sizeof (PAMPA_Num)),
          &vendtax,   (size_t) (vertnbr       * sizeof (PAMPA_Num)),
          &venttax,   (size_t) (vertnbr       * sizeof (PAMPA_Num)),
          &nnbrtax,   (size_t) (nodenbr       * sizeof (int)),
          NULL) == NULL) {
      errorPrint("TETGEN_finalize: out of memory (1)");
      return (1);
    }
    
    memSet (nnbrtax,  0, nodenbr * sizeof (int));
    memSet (vendtax, ~0, vertnbr * sizeof (int));
    
    verttax += baseval;
    vendtax += baseval;
    venttax += baseval;
    nnbrtax += baseval;
    
    verttax[baseval] = baseval;
    vendtax[baseval] = baseval;
    venttax[baseval] = baseval;
    edgenbr = baseval;
    
    // Get the number of relations tetra <=> nodes
    ptetnum = baseval;
    for (std::vector<MTetrahedron*>::iterator tit = r->tetrahedra.begin() ; tit != r->tetrahedra.end() ; ++tit) {
      MTetrahedron *tetr = dynamic_cast<MTetrahedron*>(*tit);
      
      venttax[ptetnum] = dataptr->tetrent;
      verttax[ptetnum + 1] = verttax[ptetnum] + 4;
      edgenbr += 8;
      
      for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node
        PAMPA_Num pnodnum;

        pnodnum = (PAMPA_Num) (tetr->getVertex(nodeind))->getIndex();
        nnbrtax[pnodnum] ++; // 1 for tetra
      }
      ++ptetnum;
    }
    
    for (std::list<GFace *>::iterator fit = r->embedded_faces.begin() ; tit != r->embedded_faces.end() ; ++fit) {
      GFace *face = dynamic_cast<GFace *>(*fit);
      PAMPA_Num pfacnum;
      
      pfacnum = (PAMPA_Num) (r->embedded_faces.end() - fit);
      
      venttax[pfacnum] = dataptr->faceent;
      
      for ()
    }
    
    // Update verttax and vendtax for all nodes
    for (i = 0 , nodeind = nodebas ; nodeind < vertnbr ; ++i, ++nodeind) {
      verttax[nodeind + 1] = verttax[nodeind] + nnbrtax[i];
      vendtax[nodeind] = verttax[nodeind];
    }
    
    edgesiz = edgenbr;
    if ((edgetax = (PAMPA_Num *) memAlloc (edgesiz * sizeof (PAMPA_Num))) == NULL) {
      errorPrint  ("GMSH_initialize: out of memory (1)");
      return      (1);
    }
    edgetax -= baseval;
    
    // Fill edgetax
    ptetnum = baseval;
    for (std::vector<MTetrahedron*>::iterator tit = r->tetrahedra.begin() ; tit != r->tetrahedra.end() ; ++tit) {
      MTetrahedron *tetr = dynamic_cast<MTetrahedron*>(*tit);
      
      vendtax[ptetnum] = verttax[ptetnum];
      
      for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node
        PAMPA_Num pnodnum;

        pnodnum  = (PAMPA_Num) (tetr->getVertex(nodeind))->getIndex();
        pnodnum += nodebas;
        
        edgetax[vendtax[ptetnum]++] = pnodnum;
        edgetax[vendtax[pnodnum]++] = ptetnum;
        
        venttax[pnodnum] = dataptr->nodeent;
      }
      ++ptetnum;
    }
    
//    CHECK_FDBG2 (PAMPA_meshBuild( pmshptr, baseval, vertnbr, verttax + baseval, vendtax + baseval, edgenbr,
//        edgesiz, edgetax + baseval, NULL, enttnbr, venttax + baseval, NULL, NULL, 50)); // XXX pourquoi 50 comme valeurs max ??
  PAMPA_meshCreate (pmshptr, vertnbr, verttax, vendtax, edgenbr, edgetax, 3, venttax, baseval, 3);
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (PAMPA_meshCheck2 (pmshptr, PAMPA_CHECK_ALONE));
//  CHECK_FDBG2 (PAMPA_meshCheck (pmshptr));
#endif /* PAMPA_DEBUG_MESH */
    
    
    memFreeGroup (verttax + baseval);
    memFree (edgetax + baseval);
    
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &geomtax, PAMPA_VALUE_PUBLIC, dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM); // XXX baseval + 1 à changer
    if (chekval != 0)
      return chekval;
    geomtax -= 3 * baseval;
    
    for (i = baseval /*nodebas*/, nit = r->mesh_vertices.begin() ; nit != r->mesh_vertices.end() ; ++nit, ++i) {
      MVertex *node = dynamic_cast<MVertex*>(*nit);
      geomtax[i * 3]     = node->x();
      geomtax[i * 3 + 1] = node->y();
      geomtax[i * 3 + 2] = node->z();
    }
    
    // XXX temporaire
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &ereftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->tetrent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    ereftax -= baseval;
    for (i = baseval ; i < nodebas ; ++i) {
      ereftax[i] = 0;
    }
    
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &nreftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->nodeent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    nreftax -= baseval;
    for (i = baseval ; i < nodenbr ; ++i) {
      nreftax[i] = 0;
    }
    
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &freftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    freftax -= baseval;
    for (i = baseval ; i < /* TODO */ ; ++i) {
      freftax[i] = 0;
    }
    
  }
  
  return (0);
}

#else /* FINISH */ // XXX en cours de création

//! \brief This routine initializes the
//! mesh structure of the tetgen remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
rmesh2pmesh (
GModel      * const         rmshptr,              //!< TETGEN mesh
PAMPA_GMSH_Data * const     dataptr,              //!< XXX
PAMPA_Num     const         baseval,              //!< PaMPA baseval
PAMPA_Num     const         flagval,              //!< If adaptation
PAMPA_Mesh  * const         pmshptr)              //!< PaMPA centralized mesh
{
  PAMPA_Num  vertnbr;
  PAMPA_Num* verttax;
  PAMPA_Num* vendtax;
  PAMPA_Num  edgenbr;
  PAMPA_Num  edgesiz;
  PAMPA_Num* edgetax;
  PAMPA_Num* venttax;
  PAMPA_Num  enttnbr;
  
  PAMPA_Num  nodenbr;
  PAMPA_Num  nodebas;
  PAMPA_Num  nodeind;
  PAMPA_Num  chekval;
  PAMPA_Num	 ptetnum;
  PAMPA_Num	 pfacnum;
  PAMPA_Num  nodenum;
  
  PAMPA_Num* ereftax;
  PAMPA_Num* nreftax;
  PAMPA_Num* freftax;
  double* geomtax;
  
  PAMPA_Num pnodnum;
  
  std::vector<MVertex*>::iterator nit;
  std::vector<MTetrahedron*>::iterator tetrit;
  std::vector<MTriangle*>::iterator triait;
  std::map<int, int> tecotax; // Correlation between Gmsh and PaMPA elements numerotation (tetrahedron)
  std::map<int, int> fccotax; // Correlation between Gmsh and PaMPA elements numerotation (triangles)
  std::map<int, nodeMap> ndcotax; // Correlation between Gmsh and PaMPA nodes numerotation, first number : num, second : number of relations
  std::map<int, nodeMap>::iterator got;
  MVertex *node;
  
  int i, j;
  
  // XXX
  enttnbr = 3;
  
  /***
   * BEGIN: Get number of nodes, lines, faces, tetrahedrons, and get theire new numerotation
   */
  nodenum = baseval;
  vertnbr = baseval;
  nodenbr = baseval;
  nodebas = baseval;
  edgesiz = baseval;
  for(GModel::riter it = rmshptr->firstRegion(); it != rmshptr->lastRegion(); ++it){
    discreteRegion *r = dynamic_cast<discreteRegion*>(*it);
        
    if (!r) {
      errorPrint("GMSH error");
      return (1);
    }
    
    for (tetrit = r->tetrahedra.begin() ; tetrit != r->tetrahedra.end() ; ++tetrit) {
      MTetrahedron *tetr = dynamic_cast<MTetrahedron*>(*tetrit);
      if (tecotax.count(tetr->getNum()) == 0) {
        tecotax.insert(std::pair<int,int>(tetr->getNum(), vertnbr));
        ++vertnbr;
        edgesiz += 8;
        
        for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node
          pnodnum  = (PAMPA_Num) tetr->getVertex(nodeind)->getNum();
          if ((got=ndcotax.find(pnodnum)) == ndcotax.end()) {
            nodeMap ndmap;
            ndmap.id = nodenum;
            ndmap.nbr = 1;
            ndmap.geo1 = tetr->getVertex(nodeind)->x();
            ndmap.geo2 = tetr->getVertex(nodeind)->y();
            ndmap.geo3 = tetr->getVertex(nodeind)->z();
            ndcotax.insert(std::pair<int, nodeMap>(pnodnum, ndmap));
            ++nodenum;
          }
          else {
            got->second.nbr++;
          }
        }
      }
    }
  }
  for(GModel::fiter fit = rmshptr->firstFace(); fit != rmshptr->lastFace(); ++fit){
    discreteFace *f = dynamic_cast<discreteFace*>(*fit);
        
    if (!f) {
      errorPrint("GMSH error");
      return (1);
    }
    
    for (triait = f->triangles.begin() ; triait != f->triangles.end() ; ++triait) {
      MTriangle *tria = dynamic_cast<MTriangle*>(*triait);
      if (fccotax.count(tria->getNum()) == 0) {
        fccotax.insert(std::pair<int,int>(tria->getNum(), vertnbr));
        ++vertnbr;
        edgesiz += 6;
        
        for (nodeind = 0 ; nodeind < 3 ; nodeind++) { // nodeind is a node
          pnodnum  = (PAMPA_Num) tria->getVertex(nodeind)->getNum();
          if ((got=ndcotax.find(pnodnum)) == ndcotax.end()) {
            nodeMap ndmap;
            ndmap.id = nodenum;
            ndmap.nbr = 1;
            ndmap.geo1 = tria->getVertex(nodeind)->x();
            ndmap.geo2 = tria->getVertex(nodeind)->y();
            ndmap.geo3 = tria->getVertex(nodeind)->z();
            ndcotax.insert(std::pair<int, nodeMap>(pnodnum, ndmap));
            ++nodenum;
          }
          else {
            got->second.nbr++;
          }
        }
      }
      if ((flagval & PAMPA_GMSH_ADAPT) != 0) {
        errorPrint ("je passe la");
        exit (1);
        delete (tria);
      }
    }
  }
  // TODO idem hexaèdres, prismes..., quadrangles..., lines et vertex
  nodenbr  = nodenum;
  nodebas  = vertnbr;
  vertnbr += nodenbr;
  /***
   * END: Get number of nodes, lines, faces, tetrahedrons, and get theire new numerotation
   */
  
#ifdef PAMPA_DEBUG_MESH
/*printf("TEST DOUBLONS\n");
  std::map<int, nodeMap>::iterator got2;
  for (got = ndcotax.begin() ; got != ndcotax.end() ; ++got) {
    for (got2 = got ; got2 != ndcotax.end() ; ++got2) {
      if (got != got2) {
        if (got->second.geo1 == got2->second.geo1 &&
            got->second.geo2 == got2->second.geo2 &&
            got->second.geo3 == got2->second.geo3) {
          errorPrint ("Nodes %4d/%4d (%.2f;%.2f;%.2f) and %4d/%4d (%.2f;%.2f;%.2f) have the sames coordonates", got->second.id, got->first, got->second.geo1, got->second.geo2, got->second.geo3, got2->second.id, got2->first, got2->second.geo1, got2->second.geo2, got2->second.geo3);
        }
      }
    }
  }
  
printf("TEST DOUBLONS FIN\n");*/
#endif /* PAMPA_DEBUG_MESH */
  if (memAllocGroup ((void **) (void *)
        &verttax,   (size_t) ((vertnbr + 1) * sizeof (PAMPA_Num)),
        &vendtax,   (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &venttax,   (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &edgetax,   (size_t) (edgesiz       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("GMSH_finalize: out of memory (1)");
    return (1);
  }
  
  memSet (verttax,  0, vertnbr * sizeof (PAMPA_Num));
  memSet (vendtax,  0, vertnbr * sizeof (PAMPA_Num));
  memSet (edgetax, ~0, edgesiz * sizeof (PAMPA_Num));
  
  verttax += baseval;
  vendtax += baseval;
  venttax += baseval;
  edgetax += baseval;
  
  verttax[baseval] = baseval;
  vendtax[baseval] = baseval;
  venttax[baseval] = baseval;
  edgenbr = edgesiz;
  
  // Update verttax and vendtax for all nodes
  for (j = 0, nodeind = nodebas ; nodeind < vertnbr ; ++j, ++nodeind) {
    for (got = ndcotax.begin() ; got != ndcotax.end() && got->second.id != j ; ++got) {}
    verttax[nodeind + 1] = verttax[nodeind] + got->second.nbr;
    vendtax[nodeind] = verttax[nodeind];
  }
    
  /***
   * BEGIN: Fill edgetax
   */
  ptetnum = baseval;
  verttax[ptetnum] = verttax[vertnbr];
  for(GModel::riter it = rmshptr->firstRegion(); it != rmshptr->lastRegion(); ++it){
    discreteRegion *r = dynamic_cast<discreteRegion*>(*it);
        
    if (!r) {
      errorPrint("GMSH error");
      return (1);
    }
    
    for (tetrit = r->tetrahedra.begin() ; tetrit != r->tetrahedra.end() ; ++tetrit) {
      MTetrahedron *tetr = dynamic_cast<MTetrahedron*>(*tetrit);
      
      vendtax[ptetnum] = verttax[ptetnum];
      venttax[ptetnum] = dataptr->tetrent;
      
      for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node

        pnodnum  = (PAMPA_Num) ndcotax[(tetr->getVertex(nodeind))->getNum()].id;
        pnodnum += nodebas;
        
        edgetax[vendtax[ptetnum]] = pnodnum;
        edgetax[vendtax[pnodnum]] = ptetnum;
        ++vendtax[ptetnum];
        ++vendtax[pnodnum];
        
        venttax[pnodnum] = dataptr->nodeent;
      }
      if (ptetnum != nodebas-1)
        verttax[ptetnum+1] = vendtax[ptetnum];
      ++ptetnum;
    }
  }
  
  pfacnum = ptetnum;
  for(GModel::fiter it = rmshptr->firstFace(); it != rmshptr->lastFace(); ++it){
    discreteFace *f = dynamic_cast<discreteFace*>(*it);
        
      errorPrint ("je passe la2");
      exit (1);
    if (!f) {
      errorPrint("GMSH error");
      return (1);
    }
    
    for (triait = f->triangles.begin() ; triait != f->triangles.end() ; ++triait) {
      MTriangle *tria = dynamic_cast<MTriangle*>(*triait);
      
      vendtax[pfacnum] = verttax[pfacnum];
      venttax[pfacnum] = dataptr->faceent;
      
      for (nodeind = 0 ; nodeind < 3 ; nodeind++) { // nodeind is a node

        pnodnum  = (PAMPA_Num) ndcotax[(tria->getVertex(nodeind))->getNum()].id;
        pnodnum += nodebas;
        
        edgetax[vendtax[pfacnum]] = pnodnum;
        edgetax[vendtax[pnodnum]] = pfacnum;
        ++vendtax[pfacnum];
        ++vendtax[pnodnum];
        
        venttax[pnodnum] = dataptr->nodeent;
      }
      if (pfacnum != nodebas-1)
        verttax[pfacnum+1] = vendtax[pfacnum];
      ++pfacnum;
    }
    if ((flagval & PAMPA_GMSH_ADAPT) != 0) {
      errorPrint ("je passe la");
      exit (1);
      delete (f);
    }
  }
  // TODO idem hexaèdres, prismes..., quadrangles..., lignes et vertex
  /***
   * BEGIN: Fill edgetax
   */
  
#ifdef PAMPA_DEBUG_MESH
  PAMPA_Num num;
  PAMPA_Num nghbid;
  int test;
  for (num =  baseval ; num < vertnbr ; ++num) {
    for (nghbid = verttax[num] ; nghbid < vendtax[num] ; ++nghbid) {
      if (edgetax[nghbid] == num) {
        errorPrint ("Element %d linked to itself, nghbid : %d", num, nghbid);
      }
      test = 0;
      for (i = verttax[edgetax[nghbid]] ; i < vendtax[edgetax[nghbid]] ; ++i) {
        if (edgetax[i] == num) {
          ++test;
          break;
        }
      }
      if (test == 0) {
        errorPrint ("Link %d -> %d is missing", edgetax[nghbid], num);
      }
    }
  }
#endif /* PAMPA_DEBUG_MESH */
  
  PAMPA_meshCreate (pmshptr, vertnbr, verttax, vendtax, edgenbr, edgetax, 3, venttax, baseval, 3);
    
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (PAMPA_meshCheck2 (pmshptr, PAMPA_CHECK_ALONE));
#endif /* PAMPA_DEBUG_MESH */
    
  memFreeGroup (verttax + baseval);
    
  chekval = PAMPA_meshValueLink (pmshptr, (void **) &geomtax, PAMPA_VALUE_PUBLIC, dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM); // XXX baseval + 1 à changer
  if (chekval != 0)
    return chekval;
  geomtax -= 3 * baseval;
  
  for (j = 0, nodeind = nodebas ; nodeind < vertnbr ; ++j, ++nodeind) {
    for (got = ndcotax.begin() ; got != ndcotax.end() && got->second.id != j ; ++got) {}
    geomtax[j * 3]     = got->second.geo1;
    geomtax[j * 3 + 1] = got->second.geo2;
    geomtax[j * 3 + 2] = got->second.geo3;
  }
#ifdef PAMPA_DEBUG_MESH
/*  for (i=0 ; i<nodenbr ; ++i) {
    for (j=0 ; j<nodenbr ; ++j) {
      if (i!=j && geomtax[i*3]==geomtax[j*3] && geomtax[i*3+1]==geomtax[j*3+1] && geomtax[i*3+2]==geomtax[j*3+2]) {
        printf("nœuds %d et %d, coods (%f,%f,%f) (%f,%f,%f)\n", i, j, geomtax[i*3], geomtax[i*3+1], geomtax[i*3+2], geomtax[j*3], geomtax[j*3+1], geomtax[j*3+2]);
      }
    }
  }*/
#endif /* PAMPA_DEBUG_MESH */
  
  // XXX temporaire
  chekval = PAMPA_meshValueLink (pmshptr, (void **) &ereftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->tetrent, PAMPA_TAG_REF);
  if (chekval != 0)
    return chekval;
  ereftax -= baseval;
  for (i = baseval ; i < ptetnum ; ++i) {
    ereftax[i] = 0;
  }
  
  chekval = PAMPA_meshValueLink (pmshptr, (void **) &nreftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->nodeent, PAMPA_TAG_REF);
  if (chekval != 0)
    return chekval;
  nreftax -= baseval;
  for (i = baseval ; i < nodenbr ; ++i) {
    nreftax[i] = 0;
  }
  
  chekval = PAMPA_meshValueLink (pmshptr, (void **) &freftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_REF);
  if (chekval != 0)
    return chekval;
  freftax -= baseval;
  for (i = baseval ; i < nodebas - ptetnum/* TODO */ ; ++i) {
    freftax[i] = 0;
  }
  
  tecotax.clear();
  fccotax.clear();
  ndcotax.clear();
  
  return (0);
}

#endif // XXX en cours de création
#undef FINISH
