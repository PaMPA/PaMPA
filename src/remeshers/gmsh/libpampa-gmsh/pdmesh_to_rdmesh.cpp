/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pdmesh_to_rdmesh.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 14 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

/************************************************************/
//!                                                        
//!   \file        pdmesh_to_rdmesh.cpp           
//!                                                        
//!   \authors     Arnaud BARDOUX                     
//!                Cedric LACHAT                             
//!                                                        
//!   \brief       This module is the API for the distri-  
//!                buted source mesh handling routines of  
//!                the libpampa library.                   
//!                                                        
//!   \date        Version P2.0 : from : 24 may 2016     
//!                                                        
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

//! \brief This routine initializes the
//! mesh structure of the tetgen remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

#include <mpi.h>
extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
#include "pampa.h"
}
#include <discreteRegion.h>
#include <MVertex.h>
#include <MTetrahedron.h>
#include "pampa-gmsh.h"
#include "types.h"

#define FINISH 0
#if FINISH // XXX en cours de création
int
pdmesh2rdmesh (
    PAMPA_Dmesh  * const         pmshptr,              //!< PaMPA distributed mesh
    PAMPA_GMSH_Data * const     dataptr,              //!< XXX
    PAMPA_Num     const         flagval,              //!< XXX
    GModel *                    rmshptr)              //!< GMSH distributed mesh
{
  discreteRegion* reg;
  MVertex* node;
  MTetrahedron* tetr;
  
  PAMPA_Iterator it;
  PAMPA_Iterator it_nghb;
  
  PAMPA_Num tetrnum;
  PAMPA_Num nodenum;
  
//  double * soltax;
  double * geomtax;
  
  int nodes[4];
  int i;
  
//  PAMPA_Num * ereftax;
  
  CHECK_FDBG2 (PAMPA_dmeshValueData (pmshptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &geomtax));
  
  // XXX on suppose qu'il n'y a qu'une seule région !!
  reg = new discreteRegion (rmshptr, 0);
  rmshptr->add (reg);
  
  PAMPA_dmeshItInitStart(pmshptr, dataptr->nodeent, &it);
  while (PAMPA_itHasMore(&it)) {
    nodenum = PAMPA_itCurEnttVertNum(&it);
    
    node = new MVertex (geomtax[nodenum*3], geomtax[nodenum*3+1], geomtax[nodenum*3+2], reg);
    reg->mesh_vertices.push_back(node);
    
    PAMPA_itNext(&it);
  }
  
  PAMPA_dmeshItInitStart(pmshptr, dataptr->tetrent, &it);
  PAMPA_dmeshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  while (PAMPA_itHasMore(&it)) {
    i = 0;
    tetrnum = PAMPA_itCurEnttVertNum(&it);
    
    PAMPA_itStart(&it_nghb, tetrnum);
    while (PAMPA_itHasMore(&it_nghb)) {
      
      nodes[i++] = PAMPA_itCurEnttVertNum(&it_nghb);
      
      PAMPA_itNext(&it_nghb);
    }
    tetr = new MTetrahedron ( reg->mesh_vertices[nodes[0]],
                              reg->mesh_vertices[nodes[1]],
                              reg->mesh_vertices[nodes[2]],
                              reg->mesh_vertices[nodes[3]]);
    reg->tetrahedra.push_back(tetr);
    tetr->setPartition(/* TODO récupérer le num du proc */);
    
    if (/* TODO est fantôme sur d'autres proc */) {
      /* TODO ajouter l'élément dans _ghostCells, avec les num des procs sur lesquels l'élément est fantôme */
    }
    PAMPA_itNext(&it);
  }
  
  return (0);
}
#endif
#undef FINISH
