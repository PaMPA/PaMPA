/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pmesh_to_rmesh.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 May 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

/************************************************************/
//!                                                        
//!   \file        pmesh_to_rmesh.cpp           
//!                                                        
//!   \authors     Arnaud BARDOUX                     
//!                Cedric LACHAT                             
//!                                                        
//!   \brief       This module is the API for the distri-  
//!                buted source mesh handling routines of  
//!                the libpampa library.                   
//!                                                        
//!   \date        Version P2.0 : from : 24 may 2016     
//!                                                        
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

//! \brief This routine initializes the
//! mesh structure of the tetgen remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

#include <mpi.h>
extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
#include "pampa.h"
}
#include <discreteRegion.h>
//#include <..//meshPartitionObjects.h>
#include <discreteFace.h>
#include <MVertex.h>
#include <MTetrahedron.h>
#include <MTriangle.h>
#include "pampa-gmsh.h"
#include "types.h"

  int
pmesh2rmesh (
    PAMPA_Mesh  * const         pmshptr,              //!< PaMPA centralized mesh
    PAMPA_GMSH_Data * const     dataptr,              //!< XXX
    PAMPA_Num     const         flagval,              //!< XXX
    GModel *                    rmshptr)              //!< GMSH mesh
{
  discreteRegion* reg;
  GFace* face;
  MVertex* node;
  MTetrahedron* tetr;
  MTriangle* tria;
  
  PAMPA_Iterator it;
  PAMPA_Iterator it_nghb;
  
  PAMPA_Num tetrnum;
  PAMPA_Num facenum;
  PAMPA_Num nodenum;
  
  double * soltax;
  double * geomtax;
  
  int nodes[4];
  int i;
  
//  PAMPA_Num * ereftax;
  
  CHECK_FDBG2 (PAMPA_meshValueData (pmshptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &geomtax));
  
  // XXX on suppose qu'il n'y a qu'une seule région !!
  reg = new discreteRegion (rmshptr, 0);
  rmshptr->add (reg);
  
  /***
   * Add all nodes in the gmsh mesh
   */
  PAMPA_meshItInitStart(pmshptr, dataptr->nodeent, &it);
  while (PAMPA_itHasMore(&it)) {
    nodenum = PAMPA_itCurEnttVertNum(&it);
    
    node = new MVertex (geomtax[nodenum*3], geomtax[nodenum*3+1], geomtax[nodenum*3+2], reg);
    reg->mesh_vertices.push_back(node);
    
    PAMPA_itNext(&it);
  }
  
  /***
   * Add all tetrahedrons in the gmsh mesh
   */
  PAMPA_meshItInitStart(pmshptr, dataptr->tetrent, &it);
  PAMPA_meshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  while (PAMPA_itHasMore(&it)) {
    i = 0;
    tetrnum = PAMPA_itCurEnttVertNum(&it);
    
    PAMPA_itStart(&it_nghb, tetrnum);
    while (PAMPA_itHasMore(&it_nghb)) {
      
      nodes[i++] = PAMPA_itCurEnttVertNum(&it_nghb);
      
      PAMPA_itNext(&it_nghb);
    }
    tetr = new MTetrahedron ( reg->mesh_vertices[nodes[0]],
                              reg->mesh_vertices[nodes[1]],
                              reg->mesh_vertices[nodes[2]],
                              reg->mesh_vertices[nodes[3]]);
    reg->addTetrahedron(tetr);
    
    PAMPA_itNext(&it);
  }
  
  // XXX on suppose qu'il n'y a qu'une seule face !!
  face = new discreteFace(rmshptr, 0);
  reg->addEmbeddedFace (face);
  face->addRegion(reg);
  
  /***
   * Add all triangles in the gmsh mesh
   */
  PAMPA_meshItInitStart(pmshptr, dataptr->faceent, &it);
  PAMPA_meshItInit(pmshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
  while (PAMPA_itHasMore(&it)) {
    i = 0;
    facenum = PAMPA_itCurEnttVertNum(&it);
    
    PAMPA_itStart(&it_nghb, facenum);
    while (PAMPA_itHasMore(&it_nghb)) {
      
      nodes[i++] = PAMPA_itCurEnttVertNum(&it_nghb);
      
      PAMPA_itNext(&it_nghb);
    }
    tria = new MTriangle (reg->mesh_vertices[nodes[0]],
                          reg->mesh_vertices[nodes[1]],
                          reg->mesh_vertices[nodes[2]]);
    face->addTriangle(tria);
    
    PAMPA_itNext(&it);
  }
  
  //chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
  //if (chekval == 0) {  /* If there is a value with this tag */
  //  offset = 1;
  //}
  //else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
  //  return (chekval);
  //}
  //else { /* If there no value with this tag */
  //  chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
  //  if (chekval == 0) {  /* If there is a value with this tag */
  //    offset = 6;
  //  }
  //  else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
  //    return (chekval);
  //  }
  //  else { /* If there no value with this tag */
  //    solflg = 1;
  //    //errorPrint ("No associated metric");
  //    //return (chekval);
  //  }
  //}
  //soltax -= offset * baseval;

  //sollist = new PViewDataList (false);
  //for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++)


  //GModel::current()->getFields()->setBackgroundMesh(PView::list.size() - 1);
  return (0);
}
