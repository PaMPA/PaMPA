/*  Copyright 2015-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        rmesh_to_pmesh.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 18 Sep 2015
//!                             to:   22 Sep 2017
//!
/************************************************************/
/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

#include <mpi.h>
extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
#include "pampa.h"
}
#include <tetgen.h>
#include "pampa-tetgen.h"
#include "types.h"


//! \brief This routine initializes the
//! mesh structure of the tetgen remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
rmesh2pmesh (
tetgenio    * const         rmshptr,              //!< TETGEN mesh
PAMPA_TETGEN_Data  * const  dataptr,              //!< XXX
PAMPA_Num     const         baseval,              //!< PaMPA baseval
PAMPA_Num     const         flagval,              //!< If adaptation
PAMPA_Mesh  * const         pmshptr)              //!< PaMPA centralized mesh
{
  PAMPA_Num 	basedif;
  PAMPA_Num 	nodebas;
  PAMPA_Num 	tetrbas;
  PAMPA_Num 	facebas;
  PAMPA_Num     enttnbr;
  PAMPA_Num     tetrent;
  PAMPA_Num     faceent;
  PAMPA_Num     nodeent;
  TETGEN_int     rfacidx;
  TETGEN_int     rfacnum;
  TETGEN_int     rfacnnd;
  TETGEN_int    nodenbr;
  TETGEN_int    tetrnbr;
  TETGEN_int    trianbr;
  PAMPA_Num 	vertnum;
  PAMPA_Num 	vertnnd;
  PAMPA_Num 	vertnbr;
  PAMPA_Num 	edgenbr;
  PAMPA_Num 	edgesiz;
  PAMPA_Num * 	nnbrtax;
  PAMPA_Num * 	verttax;
  PAMPA_Num * 	vendtax;
  PAMPA_Num * 	venttax;
  PAMPA_Num * 	edgetax;
  PAMPA_Num * ereftax;
  PAMPA_Num * nreftax;
  PAMPA_Num * freftax;
  double *      geomtax;
  TETGEN_int 			nodeind;
  TETGEN_int 			nodennd;
  TETGEN_int 			rtetnum;
  TETGEN_int 			rtetnnd;
  int 			chekval;
  int dummy; // XXX temporaire

  basedif = baseval - rmshptr->firstnumber;

  nodenbr = rmshptr->numberofpoints;
  trianbr = rmshptr->numberoftrifaces;
  tetrnbr = rmshptr->numberoftetrahedra;

  edgenbr = baseval;

  tetrbas = basedif;
  facebas = tetrbas + (PAMPA_Num) tetrnbr;
  if ((flagval & PAMPA_TETGEN_FACES) != 0) {
    vertnbr = (PAMPA_Num) (nodenbr + tetrnbr + trianbr);
    nodebas = facebas + (PAMPA_Num) trianbr;
    nodeent = baseval + 2;
    enttnbr = 3;
  }
  else {
    vertnbr = (PAMPA_Num) (nodenbr + tetrnbr);
    nodebas = tetrbas + (PAMPA_Num) tetrnbr;
    nodeent = baseval + 1;
    enttnbr = 2;
  }

  tetrent = baseval;
  faceent = baseval + 1;

  if (memAllocGroup ((void **) (void *)
        &venttax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &verttax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &vendtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &nnbrtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("TETGEN_finalize: out of memory (1)");
    return (1);
  }
  venttax -= baseval;
  verttax -= baseval;
  vendtax -= baseval;
  nnbrtax -= baseval;

  memSet (nnbrtax + baseval, 0, vertnbr * sizeof (PAMPA_Num));
  memSet (venttax + baseval, ~0, vertnbr * sizeof (PAMPA_Num)); // XXX vérifier en débug que entt_dummy est bien ~0
  for (rtetnum = rmshptr->firstnumber, rtetnnd = tetrnbr + rmshptr->firstnumber ; rtetnum < rtetnnd ; rtetnum ++) {
    TETGEN_int *v;
    PAMPA_Num	ptetnum;
    TETGEN_int			nodeind;

    ptetnum = (PAMPA_Num) rtetnum + tetrbas;
    nnbrtax[ptetnum] += 8; /* 4 for nodes and 4 for elements or boundary faces */ // FIXME et si les faces frontières sont entre deux éléments de natures différentes ???
    v = &(rmshptr->tetrahedronlist[rtetnum * 4]); // XXX 4 ?

    for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node
      PAMPA_Num pnodnum;

      pnodnum = (PAMPA_Num) (v[nodeind]) + nodebas;
      nnbrtax[pnodnum] ++; // 1 for tetra
      if ((flagval & PAMPA_TETGEN_NODE2NODE) != 0)
        nnbrtax[pnodnum] += 3; // 3 for nodes
    }
  }

  if ((flagval & PAMPA_TETGEN_FACES) != 0) {
    for (rfacnum = rmshptr->firstnumber, rfacnnd = trianbr + rmshptr->firstnumber; rfacnum < rfacnnd; rfacnum ++) {
      TETGEN_int *v;
      PAMPA_Num pfacnum;
      TETGEN_int			nodeind;

      pfacnum = (PAMPA_Num) rfacnum + facebas;
      nnbrtax[pfacnum] += 4; // 3 for nodes and 1 for element // FIXME et si les faces frontières sont entre deux éléments de natures différentes ???
      v = &(rmshptr->trifacelist[rfacnum * 3]); // XXX 3 ?

      for (nodeind = 0 ; nodeind < 3 ; nodeind++) { // nodeind is a node
        PAMPA_Num pnodnum;

        pnodnum = (PAMPA_Num) (v[nodeind]) + nodebas;
        nnbrtax[pnodnum] ++;
      }
    }
  }

  // XXX faire une boucle sur ts les sommets pour mettre à jour verttax et
  // vendtax
  // en déduire edgesiz
  verttax[baseval] = baseval;
  vendtax[baseval] = baseval;
  for (vertnum = baseval + 1, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
    vendtax[vertnum] =
      verttax[vertnum] = verttax[vertnum - 1] + nnbrtax[vertnum - 1];

  edgesiz = vendtax[vertnum - 1] + nnbrtax[vertnum - 1];

  if ((edgetax = (PAMPA_Num *) memAlloc (edgesiz * sizeof (PAMPA_Num))) == NULL) {
    errorPrint  ("TETGEN_initialize: out of memory (1)");
    return      (1);
  }
  edgetax -= baseval;

  for (edgenbr = 0, rtetnum = baseval, rtetnnd = (PAMPA_Num) tetrnbr + baseval; rtetnum < rtetnnd; rtetnum ++) {
    PAMPA_Num	ptetnum;
    TETGEN_int *adja;
    TETGEN_int ind;

    // if (!rmshptr->tetra[rtetnum].v[0]) continue; /* If the tetra doesn't exist */

    ptetnum = (PAMPA_Num) rtetnum + tetrbas;

    venttax[ptetnum] = tetrent;

    adja = rmshptr->neighborlist + rtetnum * 4;

    for (ind = 0 ; ind < 4 ; ind ++) {
      TETGEN_int ngbnum; // ngbnum is a tetrahedron

      ngbnum = adja[ind];

      if (ngbnum != -1) { /* If the neighbor exists */
        edgetax[vendtax[ptetnum] ++] = (PAMPA_Num) ngbnum + tetrbas;
        edgenbr ++;
      }
    }

    for (nodeind = 0 ; nodeind < 4 ; nodeind ++) {
      TETGEN_int   pt;
      TETGEN_int   nodeind2;
      PAMPA_Num pnodnum;

      pt = rmshptr->tetrahedronlist[rtetnum * 4 + nodeind];
      pnodnum = (PAMPA_Num) (pt) + nodebas;

      venttax[pnodnum] = dataptr->nodeent;
      edgetax[vendtax[ptetnum] ++] = pnodnum;
      edgetax[vendtax[pnodnum] ++] = ptetnum;
      edgenbr += 2;


      // pour chaque nœud du tétra sauf le nœud lui-même
      if ((flagval & PAMPA_TETGEN_NODE2NODE) != 0)
        for (nodeind2 = 1 ; nodeind2 < 4 ; nodeind2++) {
          PAMPA_Num pnodnm2;
          PAMPA_Num edgeidx;
          PAMPA_Num edgennd;

          pt = rmshptr->tetrahedronlist[rtetnum * 4 + (nodeind + nodeind2) % 4];

          pnodnm2 = (PAMPA_Num) (pt) + nodebas;
          for (edgeidx = verttax[pnodnum], edgennd = vendtax[pnodnum]; edgeidx < edgennd && edgetax[edgeidx] != pnodnm2; edgeidx ++); /* If already added */
          if ((edgeidx == vendtax[pnodnum]) || (edgetax[edgeidx] != pnodnm2)) {
            edgetax[vendtax[pnodnum] ++] = pnodnm2;
            edgenbr ++;
          }
        }
    }
  }

  if ((flagval & PAMPA_TETGEN_FACES) != 0) {
    PAMPA_Num pfacnum;
    PAMPA_Num	ptetnum;
    for (pfacnum = facebas, rfacnum = rmshptr->firstnumber, rfacnnd = trianbr + rmshptr->firstnumber ; rfacnum < rfacnnd ; rfacnum ++) {

      if ((rmshptr->adjtetlist[rfacnum * 2] == -1) || (rmshptr->adjtetlist[rfacnum * 2 + 1] == -1)) { // XXX XXX on n'utilise pas adjtetlist, comme si la face n'était pas reliée à un élément, que fait le meshCheck ??? À RÉGLER URGENT

        venttax[pfacnum] = dataptr->faceent;

        for (nodeind = 0 ; nodeind < 3 ; nodeind ++) {
          TETGEN_int   pt;
          PAMPA_Num pnodnum;

          pt = rmshptr->trifacelist[rfacnum * 3 + nodeind];

          pnodnum = (PAMPA_Num) (pt) + nodebas;

          // XXX ATTENTION prévoir si la face est entre deux éléments de ref différentes
          edgetax[vendtax[pfacnum] ++] = pnodnum;
          edgetax[vendtax[pnodnum] ++] = pfacnum;
          edgenbr += 2;
        }
        if (rmshptr->adjtetlist[rfacnum * 2] != -1) {
          ptetnum = rmshptr->adjtetlist[rfacnum * 2] + tetrbas;
          edgetax[vendtax[pfacnum] ++] = ptetnum;
          edgetax[vendtax[ptetnum] ++] = pfacnum;
          edgenbr += 2;
        }
        else if (rmshptr->adjtetlist[rfacnum * 2 + 1] != -1) {
          ptetnum = rmshptr->adjtetlist[rfacnum * 2] + tetrbas;
          edgetax[vendtax[pfacnum] ++] = ptetnum;
          edgetax[vendtax[ptetnum] ++] = pfacnum;
          edgenbr += 2;
        }
        pfacnum ++;
      }
    }
  }

  CHECK_FDBG2 (PAMPA_meshBuild( pmshptr, baseval, vertnbr, verttax + baseval, vendtax + baseval, NULL, edgenbr,
      edgesiz, edgetax + baseval, NULL, enttnbr, venttax + baseval, NULL, NULL, 50)); // XXX pourquoi 50 comme valeurs max ??
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (PAMPA_meshCheck2 (pmshptr, PAMPA_CHECK_ALONE));
#endif /* PAMPA_DEBUG_MESH */

  memFreeGroup (venttax + baseval);
  memFree (edgetax + baseval);

  chekval = PAMPA_meshValueLink (pmshptr, (void **) &geomtax, PAMPA_VALUE_PUBLIC, dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM); // XXX baseval + 1 à changer
  if (chekval != 0)
    return chekval;
  geomtax -= 3 * baseval;

  chekval = PAMPA_meshValueLink (pmshptr, (void **) &ereftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->tetrent, PAMPA_TAG_REF);
  if (chekval != 0)
    return chekval;
  ereftax -= baseval;

  if ((flagval & PAMPA_TETGEN_ADAPT) == 0) {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &nreftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->nodeent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    nreftax -= baseval;
    //memSet (nreftax + baseval, 0, rmshptr->np * sizeof (PAMPA_Num)); // rmshptr->np ???

  }
  if ((flagval & PAMPA_TETGEN_FACES) != 0) {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &freftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    freftax -= baseval;
  }

  for (rtetnum = baseval, rtetnnd = rmshptr->numberoftetrahedra + baseval ; rtetnum < rtetnnd ; rtetnum ++) {
    PAMPA_Num ptetnum;
    ptetnum = (PAMPA_Num) rtetnum + tetrbas;
    ereftax[ptetnum] = rmshptr->tetrahedronattributelist[rtetnum];
  }

  for (nodeind = baseval, nodennd = rmshptr->numberofpoints + baseval ; nodeind < nodennd ; nodeind ++) {
    TETGEN_int i;

    if ((flagval & PAMPA_TETGEN_ADAPT) == 0)
      nreftax[nodeind + basedif] = (rmshptr->pointmarkerlist == NULL) ? 0 : rmshptr->pointmarkerlist[nodeind];


    // XXX utiliser plutot memCpy et optimiser pour le basedif
    for (i = 0 ; i < 3 ; i++)
      geomtax[(nodeind + basedif) * 3 + i] = rmshptr->pointlist[nodeind * 3 + i];
  }

  if ((flagval & PAMPA_TETGEN_FACES) != 0) {
    PAMPA_Num pfacnum;
    for (pfacnum = rfacnum = baseval, rfacnnd = trianbr + baseval; rfacnum < rfacnnd; rfacnum ++) {
      freftax[rfacnum + basedif] = rmshptr->trifacemarkerlist[rfacnum];
    }
  }


  return 0;
}
