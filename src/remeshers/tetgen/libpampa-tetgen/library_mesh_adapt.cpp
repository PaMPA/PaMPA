/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_adapt.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 29 Feb 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/
/****************************************/
/*                                      */
/* These routines are the CPP API for   */
/* the remesher handling routines.      */
/*                                      */
/****************************************/

#ifdef PAMPA_TIME_CHECK2
#define PAMPA_TIME_CHECK
#endif /* PAMPA_TIME_CHECK2 */

#include <mpi.h>
extern "C" {
#include <stdio.h>
#include <fcntl.h>
#include "common.h"
#include "module.h"
#include "comm.h"
#include "pampa.h"
#include "pampa.h"
//#include "pampaers.h"
}
#include <tetgen.h>
#include "pampa-tetgen.h"
#include "rmesh_to_pmesh.h"
#include "pmesh_to_rmesh.h"
#include "types.h"

// XXX peut-être qu'on peut fusionner les fichiers meshAdapt de Tetgen, Gmsh (et Mmg) ?

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

#ifdef __cplusplus
extern "C" {
#endif
int
PAMPA_TETGEN_meshAdapt (
PAMPA_Mesh * const         imshptr,             //!< PaMPA centralized in mesh
PAMPA_Mesh * const         omshptr,             //!< PaMPA centralized out mesh
PAMPA_AdaptInfo * const    infoptr,             //!< Information used by the remesher
PAMPA_Num const            flagval)             //!< Flags used by the remesher
{

  tetgenio inrmshdat;
  tetgenio outrmshdat;
  PAMPA_TETGEN_Data * data;
  double * restrict osoltax;
  double * restrict owghtax;
  PAMPA_Num * restrict oflgtax;
  PAMPA_Num * restrict ormstax;
  
  PAMPA_Num * iflgtax;
  PAMPA_Num * elmiflgtax;
  PAMPA_Num baseval;
  PAMPA_Num nodenum;
  PAMPA_Num nodennd;
  PAMPA_Num offset;
  PAMPA_Num vertnbr;
  PAMPA_Num tetrnum;
  PAMPA_Num tetrnbr;
  PAMPA_Num chekval;
  int cheklocval;
  int mbnd;
  int mint;
  int num;
  
  PAMPA_Mesh * imintptr;
  PAMPA_Mesh omintdat;
  PAMPA_Mesh * mbndptr;
  
  int newF;
  int bak;
  char s[100];
  
  cheklocval = 0;
  baseval = 0; // XXX what???

  data = (PAMPA_TETGEN_Data *) infoptr->dataptr;

  PAMPA_meshData (imshptr, NULL, NULL, &vertnbr, NULL, NULL, NULL, NULL);
  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &iflgtax));
  iflgtax -= baseval;

  CHECK_FDBG2 (PAMPA_meshValueLink (imshptr, (void **) &elmiflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, baseval, PAMPA_TAG_STATUS));
  elmiflgtax -= baseval;

  PAMPA_meshEnttSize (imshptr, data->tetrent, &tetrnbr);
  mbnd = 0;
  mint = 0;
  for (tetrnum = baseval; tetrnum < tetrnbr; tetrnum ++) {
    PAMPA_Num mvrtnum;

    CHECK_FDBG2 (PAMPA_meshVertData (imshptr, tetrnum, baseval, baseval, NULL, &mvrtnum));
    if (iflgtax[mvrtnum] != PAMPA_TAG_VERT_INTERNAL) {
      mbnd = 1;
      elmiflgtax[tetrnum] = 0;
    }
    else {
      mint = 1;
      elmiflgtax[tetrnum] = 1;
    }
  }
  
  if (mint == 0) { // There is no internal mesh
#ifdef PAMPA_DEBUG_ADAPT
    printf("DEBUG: There is no internal mesh\n");
#endif
    PAMPA_Smesh smshdat;
    PAMPA_smeshInit(&smshdat);
    PAMPA_mesh2smesh(imshptr, &smshdat);
    PAMPA_smesh2mesh(&smshdat, omshptr);
    PAMPA_smeshExit(&smshdat);
    return (0);
  }
  
  if (mbnd != 0) { // There is internal and boundary meshes
    PAMPA_Mesh omshtab[2];

    mbndptr = omshtab;
    imintptr = omshtab + 1;

    for (int i = 0; i < 2; i ++)
      PAMPA_meshInit(omshtab + i);
      
#ifdef PAMPA_DEBUG_ADAPT
    printf("(%d) DEBUG: There is internal and boundary mesh\n", infoptr->rank);
#endif
    infoptr->tetrent = data->tetrent;
    infoptr->faceent = data->faceent;
    infoptr->nodeent = data->nodeent;
    infoptr->coortyp = data->coortyp;
    CHECK_FDBG2 (PAMPA_meshInduceMultiple(imshptr, elmiflgtax + baseval, 2, omshtab));
    
    CHECK_FDBG2 (pmesh2rmesh (imintptr, data, infoptr->flagval, &inrmshdat));
#ifdef PAMPA_DEBUG_ADAPT_SAVE
  {
    FILE * meshstm;
    FILE * solstm;
    sprintf (s, "avant_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.mesh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    meshstm = fopen (s, "w");
    sprintf (s, "avant_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.sol", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    solstm = fopen (s, "w");
    PAMPA_meshMeshSave (imintptr, meshstm, solstm);
    fclose (meshstm);
    fclose (solstm);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE */

#ifdef PAMPA_INFO_REMESHER
  sprintf (s, "remesh-out-" GNUMSTRING "-" GNUMSTRING "-%d-%d", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
#else /* PAMPA_INFO_REMESHER */
  sprintf (s, "/dev/null"); // TODO mettre /dev/null comme macro définie par CMake, car NUL pour win et /dev/null pour unix ;)
#endif /* PAMPA_INFO_REMESHER */
  fflush (stdout);
  bak = dup (1);
  newF = open(s, O_CREAT|O_WRONLY|O_TRUNC, 0600);
  dup2 (newF, 1);
  close (newF);
    tetrahedralize(data->switval, &inrmshdat, &outrmshdat); 
  fflush (stdout);
  dup2 (bak, 1);
  close (bak);

    PAMPA_meshExit (imintptr);
    CHECK_FDBG2 (rmesh2pmesh (&outrmshdat, data, 0, infoptr->flagval, &omintdat));
    
#ifdef PAMPA_DEBUG_ADAPT_SAVE
  {
    FILE * meshstm;
    FILE * solstm;
    sprintf (s, "apres_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.mesh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    meshstm = fopen (s, "w");
    sprintf (s, "apres_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.sol", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    solstm = fopen (s, "w");
    PAMPA_meshMeshSave (&omintdat, meshstm, solstm);
    fclose (meshstm);
    fclose (solstm);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE */
    
    switch (outrmshdat.numberofpointmtrs) {
        MPI_Datatype  metrtyp;
      case 1 :
        PAMPA_meshValueLink (&omintdat, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
        break;
      case 6 :
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (&omintdat, (void **) &osoltax, PAMPA_VALUE_PUBLIC, metrtyp, data->nodeent, PAMPA_TAG_SOL_3DAI);
        break;
      default :
        errorPrint ("unrecognized metric");
        return (1);
    }
    osoltax -= outrmshdat.numberofpointmtrs * baseval;
    
    PAMPA_meshData(&omintdat, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);
    PAMPA_meshEnttSize (&omintdat, data->tetrent, &tetrnbr);
    
    CHECK_FDBG2 (PAMPA_meshValueLink (&omintdat, (void **) &oflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS));
    oflgtax -= baseval;
    for (num = baseval ; num < vertnbr + baseval ; ++num) {
      oflgtax[num] = PAMPA_TAG_VERT_INTERNAL;
    }
    
    CHECK_FDBG2 (PAMPA_meshValueLink (&omintdat, (void **) &ormstax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, infoptr->tetrent, PAMPA_TAG_REMESH));
    for (num = baseval ; num < tetrnbr + baseval ; ++num) {
      ormstax[num] = PAMPA_TAG_VERT_NOREMESH;
    }
    
    CHECK_FDBG2 (PAMPA_meshValueLink (&omintdat, (void **) &owghtax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, infoptr->tetrent, PAMPA_TAG_WEIGHT));
    owghtax -= baseval;
    for (num = baseval ; num < tetrnbr + baseval; ++num) {
      owghtax[num] = 0;
    }
    
    for (nodenum = outrmshdat.firstnumber, nodennd = outrmshdat.numberofpoints + outrmshdat.firstnumber; nodenum < nodennd; nodenum ++)
      for (offset = 0; offset < outrmshdat.numberofpointmtrs; offset ++)
        osoltax[nodenum * outrmshdat.numberofpointmtrs + offset] = outrmshdat.pointmtrlist[nodenum * outrmshdat.numberofpointmtrs + offset];
    
    CHECK_FDBG2 (PAMPA_meshRebuild(mbndptr, &omintdat, omshptr, 1, 2, 3, 4, NULL));
    PAMPA_meshExit (mbndptr);
    PAMPA_meshExit (&omintdat);
  }
  else { // There is no boundary mesh
#ifdef PAMPA_DEBUG_ADAPT
    printf("DEGUB: No boundary mesh\n");
#endif
    
    CHECK_FDBG2 (pmesh2rmesh (imshptr, data, infoptr->flagval, &inrmshdat));

#ifdef PAMPA_INFO_REMESHER
  sprintf (s, "remesh-out-" GNUMSTRING "-" GNUMSTRING "-%d-%d", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
#else /* PAMPA_INFO_REMESHER */
  sprintf (s, "/dev/null"); // TODO mettre /dev/null comme macro définie par CMake, car NUL pour win et /dev/null pour unix ;)
#endif /* PAMPA_INFO_REMESHER */
  fflush (stdout);
  bak = dup (1);
  newF = open(s, O_CREAT|O_WRONLY|O_TRUNC, 0600);
  dup2 (newF, 1);
  close (newF);
    tetrahedralize(data->switval, &inrmshdat, &outrmshdat); 
  fflush (stdout);
  dup2 (bak, 1);
  close (bak);

    CHECK_FDBG2 (rmesh2pmesh (&outrmshdat, data, 0, infoptr->flagval, omshptr));
    
    switch (outrmshdat.numberofpointmtrs) {
        MPI_Datatype  metrtyp;
      case 1 :
        PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
        break;
      case 6 :
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, metrtyp, data->nodeent, PAMPA_TAG_SOL_3DAI);
        break;
      default :
        errorPrint ("unrecognized metric");
        return (1);
    }
    osoltax -= outrmshdat.numberofpointmtrs * baseval;
    
    PAMPA_meshData(omshptr, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);
    PAMPA_meshEnttSize (omshptr, data->tetrent, &tetrnbr);
    
    CHECK_FDBG2 (PAMPA_meshValueLink (omshptr, (void **) &oflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS));
    oflgtax -= baseval;
    for (num = baseval ; num < vertnbr + baseval ; ++num) {
      oflgtax[num] = PAMPA_TAG_VERT_INTERNAL;
    }
    
    CHECK_FDBG2 (PAMPA_meshValueLink (omshptr, (void **) &ormstax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, data->tetrent, PAMPA_TAG_REMESH));
    for (num = baseval ; num < tetrnbr + baseval ; ++num) {
      ormstax[num] = PAMPA_TAG_VERT_NOREMESH;
    }
    
    CHECK_FDBG2 (PAMPA_meshValueLink (omshptr, (void **) &owghtax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->tetrent, PAMPA_TAG_WEIGHT));
    memSet (owghtax, 0, tetrnbr * sizeof (MPI_DOUBLE));
    
    for (nodenum = outrmshdat.firstnumber, nodennd = outrmshdat.numberofpoints + outrmshdat.firstnumber; nodenum < nodennd; nodenum ++)
      for (offset = 0; offset < outrmshdat.numberofpointmtrs; offset ++)
        osoltax[nodenum * outrmshdat.numberofpointmtrs + offset] = outrmshdat.pointmtrlist[nodenum * outrmshdat.numberofpointmtrs + offset];
  }

  //delete[] outrmshdat.pointlist;
  //outrmshdat.pointlist = NULL;
  //delete[] outrmshdat.trifacelist;
  //outrmshdat.trifacelist = NULL;
  //delete[] outrmshdat.trifacemarkerlist;
  //outrmshdat.trifacemarkerlist = NULL;
  //delete[] outrmshdat.tetrahedronlist;
  //outrmshdat.tetrahedronlist = NULL;
  //delete[] outrmshdat.tetrahedronattributelist;
  //outrmshdat.tetrahedronattributelist = NULL;
  //delete[] outrmshdat.pointmtrlist;
  //outrmshdat.pointmtrlist = NULL;


  CHECK_VDBG2 (cheklocval);
  infoptr->cuntval ++;
  return (0);
}
#ifdef __cplusplus
}
#endif
