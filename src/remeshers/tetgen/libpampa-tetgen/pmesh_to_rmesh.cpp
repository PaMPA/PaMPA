/*  Copyright 2015-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pmesh_to_rmesh.cpp
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 18 Sep 2015
//!                             to:   22 Sep 2017
//!
/************************************************************/

/************************************************************/
//!                                                        
//!   \file        pmesh_to_mmesh.c           
//!                                                        
//!   \authors     Francois PELLEGRINI                     
//!                Cedric LACHAT                             
//!                                                        
//!   \brief       This module is the API for the distri-  
//!                buted source mesh handling routines of  
//!                the libpampa library.                   
//!                                                        
//!   \date        Version P2.0 : from : 07 sep 2015     
//!                                                        
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

//! \brief This routine initializes the
//! mesh structure of the tetgen remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

#include <mpi.h>
extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
#include "pampa.h"
}
#include <tetgen.h>
#include "pampa-tetgen.h"
#include "types.h"

  int
pmesh2rmesh (
    PAMPA_Mesh  * const         pmshptr,              //!< PaMPA centralized mesh
    PAMPA_TETGEN_Data  * const  dataptr,              //!< XXX
    PAMPA_Num     const         flagval,              //!< XXX
    tetgenio *                  rmshptr)              //!< TETGEN mesh
{
  PAMPA_Num baseval;
  PAMPA_Num nodenum;
  PAMPA_Num nodennd;
  PAMPA_Num nodenbr;
  PAMPA_Num facenum;
  PAMPA_Num facennd;
  PAMPA_Num facenbr;
  PAMPA_Num tetrnum;
  PAMPA_Num tetrnnd;
  PAMPA_Num tetrnbr;
  PAMPA_Num offset;
  PAMPA_Num * restrict freftax;
  PAMPA_Num * restrict ereftax;
  double * restrict geomtax;
  double * restrict soltax;
  PAMPA_Iterator it_nghb;
  int chekval;

  chekval = 0;

  PAMPA_meshData(pmshptr, &baseval, NULL, NULL, NULL, NULL, NULL, NULL);
  rmshptr->firstnumber = baseval;

  /* Set nodes */
  PAMPA_meshEnttSize(pmshptr, dataptr->nodeent, &nodenbr);

  CHECK_FDBG2 (PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &geomtax));
  geomtax -= 3 * baseval;

  ///* Remove elements which are on the boundary */
  //CHECK_FDBG2 (PAMPA_meshValueData (imshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &iflgtax));
  //iflgtax -= baseval;

  //memSet (nodeflgtax + baseval, 0, nodenbr * sizeof (Gnum));

  //PAMPA_meshItInitStart(imshptr, data->tetrent, &it);
  //PAMPA_meshItInit(imshptr, data->tetrent, data->nodeent, &it_nghb);
  //while (PAMPA_itHasMore(&it)) {
  //  PAMPA_Num tetrnum;
  //  PAMPA_Num mtetnum; // FIXME mtetnum est ambigu : il peut dire par maillage comme ici ou pour MMG comme dans mmesh_to_pmesh

  //  mtetnum = PAMPA_itCurMeshVertNum(&it);
  //  tetrnum = PAMPA_itCurEnttVertNum(&it);

  //  if (iflgtax[mtetnum] == PAMPA_TAG_VERT_INTERNAL) 
  //    flagval = 1; // XXX macro a la place
  //  else { /* Element on the boundary */
  //    flagval = 2; // XXX macro a la place
  //    nodebndnbr += 4; /* max bound of boundary nodes */
  //  }
  //  
  //  PAMPA_itStart(&it_nghb, tetrnum);

  //  while (PAMPA_itHasMore(&it_nghb)) {
  //    PAMPA_Num nodenum;

  //    nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
  //    nodeflgtax[nodenum] |= flagval;
  //    PAMPA_itNext(&it_nghb);
  //  }
  //  PAMPA_itNext(&it);
  //}

  //if ((nodebndtab = (Node *) memAlloc (nodebndnbr * sizeof (Node))) == NULL) {
  //  errorPrint  ("out of memory");
  //  return      (1);
  //}

  //for (nodebndnbr = 0, nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++) {
  //  if ((nodeflgtax[nodenum] & 2) != 0) /* This node belongs to at least one boundary element */
  //    memCpy (nodebndtab[nodebndnbr].coortab, geomtax + (nodenum * 3), 3 * sizeof (double));
  //  if ((nodefgltax[nodenum] & 1) == 0)
  //    nodebndtab[nodebndnbr].addind 

  // algo pour supprimer les éléments de bord (avant remaillage)
  // * dans la continuite ou presque de ce qui est dessus
  // ** il manque le nombre de noeuds qui ne sont que sur des éléments de bord : nodeaddnbr (pas terrible comme nom)
  // ** il faut trier nodebndtab
  // * pour chaque élément de bord (qui n'est pas interne)
  // ** on parcourt les éléments voisins
  // *** si l'élément est interne
  // **** on cherche la face commune (on trie les sommets des deux éléments et
  //      on regarde si les sommets sont aux indices 0, 1, 2 ou 1, 2 et 3) 
  // **** puis on stocke dans tetrhashtab l'élément avec comme somme celle des faces
  // **** on ajoute comme valeur dans tetrhashtab elembndind
  // ** on incrémente elembndind
  // * via elembndind on a elembndnbr

  // algo pour remettre les éléments de bord supprimés (après remaillage)
  // * on alloue un tableau elembndtab de taille elembndnbr
  // * on alloue un tableau nodeaddtab de taille nodeaddtab
  // * on remplit les tableaux elembndnbr et nodeaddtab par les valeurs à la
  //   suite de celles du remailleur dans les deux catégories
  // * on remplit geomtax en conséquence avec nodebndtab (il manque la
  // correspondance d'indice entre nodeaddtab et nodebndtab !!! Peut-être avoir
  // un champ supplémentaire dans nodebndtab avec la valeur avant le tri)
  // * pour chaque face 
  // ** on parcourt nodebndtab et on remplit triatab par les indices qu'on a trouvé
  // ** on trie triatab
  // ** on cherche dans tetrhashtab la somme des faces, puis sur triatab à
  //    l'indice 0  de tetrhashtab[ ].nodetab ou à l'indice 1
  // ** avec adjtetlist et elembndtab on ajoute
  // *** le lien entre les deux éléments
  // *** et entre le nouvel élément et la face
  // *** et entre le nouvel élément et les noeuds
  // **** pour ceux qui sont communs, on les a via le (* pour chaque face)
  // **** pour le nouveau noeud, on l'a dans nodebndtab et nodeaddtab

  rmshptr->numberofpoints = nodenbr;
  rmshptr->pointlist = new REAL[nodenbr * 3];
  
  for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++) {
    rmshptr->pointlist[nodenum * 3] = geomtax[nodenum * 3];
    rmshptr->pointlist[nodenum * 3 + 1] = geomtax[nodenum * 3 + 1];
    rmshptr->pointlist[nodenum * 3 + 2] = geomtax[nodenum * 3 + 2];
  }

  /* Set faces */
  if ((flagval & PAMPA_TETGEN_FACES) != 0) {
    CHECK_FDBG2 (PAMPA_meshValueData(pmshptr, dataptr->faceent, PAMPA_TAG_REF, (void **) &freftax));
    freftax -= baseval;
  }

  PAMPA_meshEnttSize(pmshptr, dataptr->faceent, &facenbr);
  rmshptr->numberoftrifaces = facenbr;
  rmshptr->trifacelist = new int[facenbr * 3];
  rmshptr->trifacemarkerlist = new int[facenbr];

  PAMPA_meshItInit(pmshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
  for (facenum = baseval, facennd = baseval + facenbr; facenum < facennd; facenum ++) {
    PAMPA_Num offset;

    PAMPA_itStart(&it_nghb, facenum);

    offset = 0;
    while (PAMPA_itHasMore(&it_nghb)) {
      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
      rmshptr->trifacelist[facenum * 3 + offset] = nodenum;
      PAMPA_itNext(&it_nghb);
      offset ++;
    }
    if ((flagval & PAMPA_TETGEN_FACES) != 0) 
      rmshptr->trifacemarkerlist[facenum] = freftax[facenum];
    else
      rmshptr->trifacemarkerlist[facenum] = 0;
  }

  /* Set tetrahedra */
  CHECK_FDBG2 (PAMPA_meshValueData(pmshptr, dataptr->tetrent, PAMPA_TAG_REF, (void **) &ereftax));
  ereftax -= baseval;

  PAMPA_meshEnttSize(pmshptr, dataptr->tetrent, &tetrnbr);
  rmshptr->numberoftetrahedra = tetrnbr;
  rmshptr->numberofcorners = 4;
  rmshptr->numberoftetrahedronattributes = 1;
  rmshptr->tetrahedronlist = new int[tetrnbr * 4];
  rmshptr->tetrahedronattributelist = new REAL[tetrnbr];

  PAMPA_meshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  for (tetrnum = baseval, tetrnnd = baseval + tetrnbr; tetrnum < tetrnnd; tetrnum ++) {
    PAMPA_Num offset;

    PAMPA_itStart(&it_nghb, tetrnum);

    offset = 0;
    while (PAMPA_itHasMore(&it_nghb)) {
      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
      rmshptr->tetrahedronlist[tetrnum * 4 + offset] = nodenum;
      PAMPA_itNext(&it_nghb);
      offset ++;
    }
    rmshptr->tetrahedronattributelist[tetrnum] = ereftax[tetrnum];
  }

  chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
  if (chekval == 0)  /* If there is a value with this tag */
    rmshptr->numberofpointmtrs = 1;
  else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    return (chekval);
  }
  else { /* If there no value with this tag */
    chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
    if (chekval == 0)  /* If there is a value with this tag */
      rmshptr->numberofpointmtrs = 6;
    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (chekval);
    }
    else { /* If there no value with this tag */
      errorPrint ("No associated metric");
      return (chekval);
    }
  }
  soltax -= rmshptr->numberofpointmtrs * baseval;

  rmshptr->pointmtrlist = new REAL[rmshptr->numberofpoints * rmshptr->numberofpointmtrs];

  for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++) 
    for (offset = 0; offset < rmshptr->numberofpointmtrs; offset ++)
      rmshptr->pointmtrlist[nodenum * rmshptr->numberofpointmtrs + offset] = (REAL) soltax[nodenum * rmshptr->numberofpointmtrs + offset];

  return (0);
}
