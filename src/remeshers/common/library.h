/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

/*
** Macro to cast with the wright type
*/
#define DYN_CAST(type, outtax, outindex, intax, inindex)                            \
      {                                                                             \
        MPI_Aint dummy;                                                             \
        MPI_Aint typesiz;                                                           \
        if (MPI_Type_get_extent (type, &dummy, &typesiz)) {                         \
          errorPrint ("get size error");                                            \
          return (1);                                                               \
        }                                                                           \
        switch (typesiz) {                                                          \
          case sizeof(short):                                                       \
            ((short*)outtax)[outindex] = ((short*)intax)[inindex];                  \
            break;                                                                  \
          case sizeof(int):                                                         \
            ((int*)outtax)[outindex] = ((int*)intax)[inindex];                      \
            break;                                                                  \
          case sizeof(long int):                                                    \
            ((long int*)outtax)[outindex] = ((long int*)intax)[inindex];            \
            break;                                                                  \
          case sizeof(byte):                                                        \
          default:                                                                  \
            ((byte*)outtax)[outindex] = ((byte*)intax)[inindex];                    \
            break;                                                                  \
        }                                                                           \
      }
