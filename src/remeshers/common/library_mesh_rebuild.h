/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_rebuild.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 20 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the CPP API for   */
/* the remesher handling routines.      */
/*                                      */
/****************************************/

#define MESH_REBUILD_H
#define MESHREBLDHASHPRIME      17            //!< Prime number for hashing

typedef struct {
  PAMPA_Num   coorsum;        // Coordinate sum
  double      coortab[3];     // Coordinates of nodes XXX faire en sorte qu'on soit indépendant du 3 ;-)
  PAMPA_Num   id;       // Global num of the node
} NodeHash;


typedef struct {
  PAMPA_Num   nodesum;  // Node number sum
  PAMPA_Num   nodetab[3]; // Nodes of the face XXX faire en sorte qu'on soit indépendant du 3 ;-)
  PAMPA_Num   id;       // Global num of an internal frontier tetrahedron
  PAMPA_Num   val;      // Frontier face value
  PAMPA_Num   num;      // Num of the frontier face value in the boundaries mesh
  PAMPA_Num * index;    // Index of the last edge of an internal frontier tetrahedron
} FaceHash;

#define PAMPA_GEOM_1   0     // First coordinate of the node
#define PAMPA_GEOM_2   1     // Second coordinate of the node
#define PAMPA_GEOM_3   2     // Third coordinate of the node


//! \brief This routine merge two centralized
//! PaMPA meshes
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_meshRebuild (
PAMPA_Mesh  *               mintptr,    //!< Input internal mesh (will be free)
PAMPA_Mesh  *               mbndptr,    //!< Input boundaries mesh (will be free)
PAMPA_Mesh  * const         moutptr,    //!< Pointer to the final mesh
PAMPA_AdaptInfo  * const  dataptr);

//! \brief Test if a node if frontier
//!
//! \returns 1   : if the node is forntier.
//! \returns 0   : if not.

int
isNodeFrontier (
PAMPA_Num     const         nodenum,    //!< Id of the node
PAMPA_Num   * const         flagtab);   //!< Vector of frontier flags


//! \brief Test if a tetrahedron if frontier
//!
//! \returns 1   : if the tetrahedron is forntier.
//! \returns 0   : if not.

int
isTetrFrontier (
PAMPA_Num     const         tetrnum,    //!< Id of the tetrahedron
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the tetrahedron is
PAMPA_Num   * const         flagtab,    //!< Vector of frontier flags
PAMPA_AdaptInfo  * const  dataptr);


//! \brief Test if a face if frontier
//!
//! \returns 1   : if the face is forntier.
//! \returns 0   : if not.

int
isFaceFrontier (
PAMPA_Num     const         facenum,    //!< Id of the face
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num   * const         flagtab,    //!< Vector of frontier flags
PAMPA_AdaptInfo  * const  dataptr);


//! \brief Get the boundary num from the global
//!
//! \returns id   : on success.
//! \returns -1   : if the vertex is not in the correlation table.

int
getBndNum (
PAMPA_Num     const         num,        //!< Global num
double      * const         geomtab,    //!< Coordinates table
PAMPA_Num     const         hashmsk,
NodeHash    * const         hashtab);   //!< Hash table


//! \brief Test if a face is in the hash table
//!
//! \returns id   : index of the face in the table.
//! \returns -1   : if the face is not in the table.

int
isFaceInHash (
PAMPA_Num     const         num,          //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,      //!< Mesh in which the face is
PAMPA_Num     const         hashmsk, 
FaceHash    * const         hashtab,    //!< Hash table
int           const         origin,       //!< 1 if it's the internal mesh, else 2
double      * const         geomtab,      //!< Coordinates table (internal)
PAMPA_Num     const         hashnodemsk,
NodeHash    * const         hashndtab,    //!< Hash table (node hashtable)
PAMPA_AdaptInfo  * const  dataptr);


//! \brief Add a face is in the hash table
//!
//! \returns id   : index of the face in the table.
//! \returns -1   : on error.

int
addFaceInHash (
PAMPA_Num     const         num,        //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num   * const         hashnbr,    //!< Number of elements in the hash table
PAMPA_Num   * const         hashsiz,    //!< Size of the hash table
PAMPA_Num   * const         hashmax,    //!< Maximum size of the hash table
PAMPA_Num   * const         hashmsk,    //!< Mask of the hash table
FaceHash  * * const         hashtab,    //!< Hash table
PAMPA_AdaptInfo  * const  dataptr);


//! \brief Test if a tetrahedron's face is in the hash table
//!
//! \returns nbr  : number of faces in the table.

int
isTetrFaceInHash (
PAMPA_Num     const         num,        //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num     const         hashmsk,    //!< Mask of the hash table
FaceHash    * const         hashtab,    //!< Hash table
int           const         origin,       // 1 if it's the internal mesh, else 2
double      * const         geomtab,      //!< Coordinates table (internal)
PAMPA_Num     const         hashnodemsk,  //!< Value used to get the key of the element
NodeHash    * const         hashnodetab,    //!< Hash table (node hashtable)
PAMPA_Num   * const         vendtab,
PAMPA_Num   * const         edgetab,
PAMPA_Num     const         outnum,
PAMPA_AdaptInfo  * const  dataptr);


//! \brief Add a tetrahedron's face is in the hash table
//!
//! \returns nbr  : number of faces added in the table.

int
addTetrFaceInHash (
PAMPA_Num     const         num,        //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num   * const         hashnbr,    //!< Number of elements in the hash table
PAMPA_Num   * const         hashsiz,    //!< Size of the hash table
PAMPA_Num   * const         hashmax,    //!< Maximum size of the hash table
PAMPA_Num   * const         hashmsk,    //!< Value used to get the key of the element
FaceHash  * * const         hashtab,    //!< Hash table
PAMPA_Num   * const         flagtab,    //!< Vector of frontier flags
PAMPA_AdaptInfo  * const  dataptr);

static
  int
meshRebldNodeResize (
    NodeHash * restrict * hashtabptr,
    PAMPA_Num * restrict const           hashsizptr,
    PAMPA_Num * restrict const           hashmaxptr,
    PAMPA_Num * restrict const           hashmskptr);


static
  int
meshRebldFaceResize (
    FaceHash * restrict * hashtabptr,
    PAMPA_Num * restrict const           hashsizptr,
    PAMPA_Num * restrict const           hashmaxptr,
    PAMPA_Num * restrict const           hashmskptr);
