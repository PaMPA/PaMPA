/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_rebuild.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 20 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the CPP API for   */
/* the remesher handling routines.      */
/*                                      */
/****************************************/

#include <mpi.h>
//extern "C" {
#include "module.h"
#include "common.h"
#include "pampa.h"
//}
#include "library_mesh_rebuild.h"
#include "library.h"

#ifdef TETGEN_LIBRARY
#include "../tetgen/libpampa-tetgen/pampa-tetgen.h"
#include "../tetgen/libpampa-tetgen/types.h"
#endif

#ifdef GMSH_LIBRARY
#include "../gmsh/libpampa-gmsh/pampa-gmsh.h"
#include "../gmsh/libpampa-gmsh/types.h"
#endif



//! \brief This routine merge two centralized
//! PaMPA meshes
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_meshRebuild (
PAMPA_Mesh  *               mintptr,    //!< Input internal mesh (will be free)
PAMPA_Mesh  *               mbndptr,    //!< Input boundaries mesh (will be free)
PAMPA_Mesh  * const         moutptr,    //!< Pointer to the final mesh
PAMPA_AdaptInfo  * const  dataptr)
{
  
  PAMPA_Num baseval;
  
  // Datas from the internal mesh
  PAMPA_Smesh smintdat;
  
  PAMPA_Num  mintvertnbr;
  PAMPA_Num* mintverttax;
  PAMPA_Num* mintvendtax;
  PAMPA_Num  mintedgenbr;
  PAMPA_Num  mintedgesiz;
  PAMPA_Num* mintedgetax;
  PAMPA_Num  mintenttnbr;
  PAMPA_Num* mintventtax;
  PAMPA_Num  minttetrnbr;    // number of tetrahedrons in the internal mesh
  PAMPA_Num  mintfacenbr;    // number of face in the internal mesh
  
  double* mintgeomtax;
  
  // Datas from the boundaries mesh
  PAMPA_Smesh smbnddat;
  
  PAMPA_Num  mbndvertnbr;
  PAMPA_Num* mbndverttax;
  PAMPA_Num* mbndvendtax;
  PAMPA_Num  mbndedgenbr;
  PAMPA_Num  mbndedgesiz;
  PAMPA_Num* mbndedgetax;
  PAMPA_Num  mbndenttnbr;
  PAMPA_Num* mbndventtax;
  PAMPA_Num  mbndtetrnbr;    // number of tetrahedrons in the boundary mesh
  PAMPA_Num  mbndfacenbr;    // number of face in the boundary mesh
  
  double* mbndgeomtax;
  PAMPA_Num* mbndndfgtax;
  
  // Datas for the final mesh
  PAMPA_Num  vertnbr;
  PAMPA_Num* verttax;
  PAMPA_Num* vendtax;
  PAMPA_Num  edgenbr;
  PAMPA_Num  edgesiz;
  PAMPA_Num* edgetax;
  PAMPA_Num  enttnbr;
  PAMPA_Num* venttax;
  PAMPA_Num  nodenbr;
  
  
  
  PAMPA_Num* corrtax;       // Correlation table for old and new boundarie's numbering
  PAMPA_Num* ndcotax;       // Correlations tables for global numbering vs node numbering
  PAMPA_Num* mintcorrtax;   // Correlation table internal vertex num
  PAMPA_Num* mbndcorrtax;   // Correlation table extrenal vertex num
  
  PAMPA_Num * enttnum;
  PAMPA_Num mintndnum;      // Counter for internal node
  PAMPA_Num minttenum;      // Counter for internal tetra
  PAMPA_Num mintfcnum;      // Counter for internal face
  PAMPA_Num mbndndnum;      // Counter for external node
  PAMPA_Num mbndtenum;      // Counter for external tetra
  PAMPA_Num mbndfcnum;      // Counter for external face
  
  // For hashtables
  PAMPA_Num  mbndnodenbr;    // number of nodes in the boundary mesh
  PAMPA_Num  hashnodemsk;    // value use to get the key of en element
  PAMPA_Num  hashnodesiz;    // size of the hash table
  PAMPA_Num  hashnodemax;    // maximum size of the hash table
  PAMPA_Num  hashnodenbr;    // number of elements of the hash table
  NodeHash*  hashnodetab;    // hashtable
  
  PAMPA_Num  hashfacemsk;    // value use to get the key of en element
  PAMPA_Num  hashfacesiz;    // size of the hash table
  PAMPA_Num  hashfacemax;    // maximum size of the hash table
  PAMPA_Num  hashfacenbr;    // number of elements of the hash table
  FaceHash*  hashfacetab;    // hashtable
  
  
  PAMPA_Num num;
  PAMPA_Num outnum;
  PAMPA_Num maxnum;
  PAMPA_Num nghbnum;
  PAMPA_Num nghbid;
  PAMPA_Num end;
  PAMPA_Num id;
  PAMPA_Num tmp;
  PAMPA_Num next;
  PAMPA_Num last;
  
  int i, j, k, base;
  int chekval;
  
  /***
   * BEGIN: Everythings from internal mesh
   */
  PAMPA_smeshInit(&smintdat);
  PAMPA_mesh2smesh(mintptr, &smintdat);
  
  PAMPA_smeshData(&smintdat, &baseval, &mintenttnbr, &mintvertnbr,
                  &mintverttax, &mintvendtax, &mintedgenbr, &mintedgesiz,
                  &mintedgetax, &mintventtax, NULL, NULL);
  PAMPA_meshEnttSize(mintptr, dataptr->tetrent, &minttetrnbr);
  PAMPA_meshEnttSize(mintptr, dataptr->faceent, &mintfacenbr);
  mintverttax -= baseval;
  mintvendtax -= baseval;
  mintedgetax -= baseval;
  mintventtax -= baseval;
  
  CHECK_FDBG2 (PAMPA_meshValueData(mintptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &mintgeomtax));
  mintgeomtax -= 3 * baseval;
  
#ifdef PAMPA_DEBUG_ADAPT
  for (num = baseval ; num < mintvertnbr ; num++) {
    if (mintverttax[num] > (mintedgenbr + baseval)) {
      errorPrint ("mintverttax out of range");
      return (1);
    }
    if (mintverttax[num] < baseval) {
      errorPrint ("mintverttax under baseval");
      return (1);
    }
    if (mintvendtax[num] > (mintedgenbr + baseval)) {
      errorPrint ("mintvendtax out of range");
      return (1);
    }
    if (mintvendtax[num] < baseval) {
      errorPrint ("mintvendtax under baseval");
      return (1);
    }
  }
  for (num = baseval ; num < mintedgenbr ; num++) {
    if (mintedgetax[num] > (mintvertnbr + baseval)) {
      errorPrint ("mintedgetax out of range");
      return (1);
    }
    if (mintedgetax[num] < baseval) {
      errorPrint ("mintedgetax under baseval");
      return (1);
    }
  }
#endif /* PAMPA_DEBUG_ADAPT */
  /***
   * END: Everythings from internal mesh
   */
  
  
  /***
   * BEGIN: Everythings from bondary mesh
   */
  PAMPA_smeshInit(&smbnddat);
  PAMPA_mesh2smesh(mbndptr, &smbnddat);
  
  PAMPA_smeshData(&smbnddat, NULL, &mbndenttnbr, &mbndvertnbr,
                  &mbndverttax, &mbndvendtax, &mbndedgenbr, &mbndedgesiz,
                  &mbndedgetax, &mbndventtax, NULL, NULL);
  PAMPA_meshEnttSize(mbndptr, dataptr->tetrent, &mbndtetrnbr);
  PAMPA_meshEnttSize(mbndptr, dataptr->faceent, &mbndfacenbr);
  mbndverttax -= baseval;
  mbndvendtax -= baseval;
  mbndedgetax -= baseval;
  mbndventtax -= baseval;

  CHECK_FDBG2 (PAMPA_meshValueData(mbndptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &mbndgeomtax));
  
  PAMPA_meshValueData(mbndptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_FRONT, (void **) &mbndndfgtax);
  
  mbndgeomtax -= 3 * baseval;
  mbndndfgtax -= baseval;
  
#ifdef PAMPA_DEBUG_ADAPT
  for (num = baseval ; num < mbndvertnbr ; num++) {
    if (mbndverttax[num] > (mbndedgenbr + baseval)) {
      errorPrint ("mbndverttax out of range");
      return (1);
    }
    if (mbndverttax[num] < baseval) {
      errorPrint ("mbndverttax under baseval");
      return (1);
    }
    if (mbndvendtax[num] > (mbndedgenbr + baseval)) {
      errorPrint ("mbndvendtax out of range");
      return (1);
    }
    if (mbndvendtax[num] < baseval) {
      errorPrint ("mbndvendtax under baseval");
      return (1);
    }
  }
  for (num = baseval ; num < mbndedgenbr ; num++) {
    if (mbndedgetax[num] > (mbndvertnbr + baseval)) {
      errorPrint ("mbndedgetax out of range");
      return (1);
    }
    if (mbndedgetax[num] < baseval) {
      errorPrint ("mbndedgetax under baseval");
      return (1);
    }
  }
#endif /* PAMPA_DEBUG_ADAPT */
  /***
   * END: Everythings from bondary mesh
   */
  
  
  /***
   * BEGIN: nodes' hashtable creation
   */
  PAMPA_meshEnttSize(mbndptr, dataptr->nodeent, &mbndnodenbr);
  hashnodenbr = MAX (mbndnodenbr * 0.2, 10);
  for (hashnodesiz = 256; hashnodesiz < hashnodenbr; hashnodesiz <<= 1) ; /* Get upper power of two */
  hashnodenbr = 0;

  hashnodemsk = hashnodesiz - 1;
  hashnodemax = hashnodesiz >> 2;
  if ((hashnodetab = (NodeHash *) memAlloc (hashnodesiz * sizeof (NodeHash))) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }

  memSet(hashnodetab, ~0, hashnodesiz * sizeof (NodeHash));
  /***
   * END: nodes' hashtable creation
   */
  
  /***
   * BEGIN: faces' hashtable creation
   */
  hashfacenbr = MAX (mbndtetrnbr * 0.2, 10);
  for (hashfacesiz = 256; hashfacesiz < hashfacenbr; hashfacesiz <<= 1) ; /* Get upper power of two */
  hashfacenbr = 0;

  hashfacemsk = hashfacesiz - 1;
  hashfacemax = hashfacesiz >> 2;
  if ((hashfacetab = (FaceHash *) memAlloc (hashfacesiz * sizeof (FaceHash))) == NULL) {
    errorPrint ("out of memory");
    return     (1);
  }
  memSet(hashfacetab, ~0, hashfacesiz * sizeof (FaceHash));
  /***
   * END: faces' hashtable creation
   */
  
  /***
   * BEGIN: Get number of vertices and relations
   */
  vertnbr = mintvertnbr;
  edgenbr = mintedgenbr;
  
  PAMPA_meshEnttSize(mintptr, dataptr->nodeent, &nodenbr);
  
  if (memAllocGroup ((void **) (void *)
        &corrtax,     (size_t) (mbndvertnbr       * sizeof (PAMPA_Num)),
        &mintcorrtax, (size_t) (mintvertnbr       * sizeof (PAMPA_Num)),
        &mbndcorrtax, (size_t) (mbndvertnbr       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("TETGEN_finalize: out of memory (1)");
    return (1);
  }
  
  memSet (corrtax, ~0, mbndvertnbr * sizeof (PAMPA_Num));
  memSet (mbndcorrtax, ~0, mbndvertnbr * sizeof (PAMPA_Num));
  
  corrtax -= baseval;
  mintcorrtax -= baseval;
  mbndcorrtax -= baseval;
  
  outnum = mintvertnbr;
  mbndndnum = baseval;
  mbndtenum = baseval;
  mbndfcnum = baseval;
  
  for (num = baseval ; num < mbndvertnbr ; num++) {
    edgenbr += (mbndvendtax[num] - mbndverttax[num]);
    
    if (mbndventtax[num] == dataptr->tetrent) {
      vertnbr++;
      corrtax[num] = outnum++;
      
      if (isTetrFrontier(mbndtenum, mbndptr, mbndndfgtax, dataptr) == 1) { // Add the relation with his neighboor in the internal mesh
        tmp = addTetrFaceInHash(mbndtenum, mbndptr, &hashfacenbr, &hashfacesiz, &hashfacemax, &hashfacemsk, &hashfacetab, mbndndfgtax, dataptr);
        edgenbr += 2 * tmp;
      }
      mbndcorrtax[num] = mbndtenum++;
    }
    
    if (mbndventtax[num] == dataptr->faceent) {
      if (mbndcorrtax[num] == ~0) {
        mbndcorrtax[num] = mbndfcnum++;
      }
      if (isFaceFrontier(mbndcorrtax[num], mbndptr, mbndndfgtax, dataptr) == 1) { // Frontier vertices are already add with 'vertnbr = mintvertnbr;'
        
        vertnbr++;
        // Add the face in the hashtable, if it hasn't been done in the tetrahedron if
        if ((tmp=isFaceInHash(mbndcorrtax[num], mbndptr, hashfacemsk, hashfacetab, 2, NULL, hashnodemsk, NULL, dataptr)) == ~0) {
          tmp = addFaceInHash(mbndcorrtax[num], mbndptr, &hashfacenbr, &hashfacesiz, &hashfacemax, &hashfacemsk, &hashfacetab, dataptr);
        }
        if (tmp == ~0) {
          errorPrint("internal error: face get id");
          return (1);
        }

        hashfacetab[tmp].val = (mbndvendtax[num] - mbndverttax[num]);
        hashfacetab[tmp].num = num;
        
        for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mbndedgetax[nghbid];
          
          if (mbndventtax[nghbnum] == dataptr->faceent && mbndcorrtax[nghbnum] == ~0) {
            mbndcorrtax[nghbnum] = mbndfcnum++;
          }
          // At the frontier, face-face and face-node relations are already add in the internal mesh
          if ((mbndventtax[nghbnum] == dataptr->nodeent && isNodeFrontier(nghbnum, mbndndfgtax) == 1) ||
              (mbndventtax[nghbnum] == dataptr->faceent && isFaceFrontier(mbndcorrtax[nghbnum], mbndptr, mbndndfgtax, dataptr) == 1)) {
            edgenbr --;
          }
        }
        mbndfacenbr --;
      }
      else {
        vertnbr++;
        corrtax[num] = outnum++;
      }
    }
    
    if (mbndventtax[num] == dataptr->nodeent) {
      if (isNodeFrontier(num, mbndndfgtax) == 1) {
        PAMPA_Num coorsum;
        PAMPA_Num hashnum;

        mbndndfgtax[num] = mbndvendtax[num] - mbndverttax[num];
        
        for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; ++ nghbid) {
          nghbnum = mbndedgetax[nghbid];
          
          if (mbndventtax[nghbnum] == dataptr->faceent && mbndcorrtax[nghbnum] == ~0) {
            mbndcorrtax[nghbnum] = mbndfcnum++;
          }
          // At the frontier, node-node and node-face relations are already add in the internal mesh
          if ((mbndventtax[nghbnum] == dataptr->nodeent && isNodeFrontier(nghbnum, mbndndfgtax) == 1) ||
              (mbndventtax[nghbnum] == dataptr->faceent && isFaceFrontier(mbndcorrtax[nghbnum], mbndptr, mbndndfgtax, dataptr) == 1)) {
            edgenbr --;
          }
        }
        
        // Add the node in the hashtable
        coorsum = mbndgeomtax[mbndndnum * 3] + mbndgeomtax[mbndndnum * 3 + 1] + mbndgeomtax[mbndndnum * 3 + 2];
        for (hashnum = (coorsum * MESHREBLDHASHPRIME) & hashnodemsk;
            hashnodetab[hashnum].coorsum != ~0 &&
            (hashnodetab[hashnum].coorsum != coorsum
             || hashnodetab[hashnum].coortab[0] != mbndgeomtax[mbndndnum * 3]
             || hashnodetab[hashnum].coortab[1] != mbndgeomtax[mbndndnum * 3 + 1]
             || hashnodetab[hashnum].coortab[2] != mbndgeomtax[mbndndnum * 3 + 2]
            ); hashnum = (hashnum + 1) & hashnodemsk) ;
        //

        if ((hashnodetab[hashnum].coorsum != coorsum)
            || (hashnodetab[hashnum].coortab[0] != mbndgeomtax[mbndndnum * 3])
            || (hashnodetab[hashnum].coortab[1] != mbndgeomtax[mbndndnum * 3 + 1])
            || (hashnodetab[hashnum].coortab[2] != mbndgeomtax[mbndndnum * 3 + 2])) { /* Node not already added */
          hashnodenbr ++;
          hashnodetab[hashnum].coorsum  = coorsum;
          hashnodetab[hashnum].coortab[0] = mbndgeomtax[mbndndnum * 3];
          hashnodetab[hashnum].coortab[1] = mbndgeomtax[mbndndnum * 3 + 1];
          hashnodetab[hashnum].coortab[2] = mbndgeomtax[mbndndnum * 3 + 2];
          hashnodetab[hashnum].id   = num;

          if (hashnodenbr >= hashnodemax) /* If hashtab is too much filled */
            CHECK_FDBG2 (meshRebldNodeResize (&hashnodetab, &hashnodesiz, &hashnodemax, &hashnodemsk));
        }

      }
      else {
        vertnbr++;
        nodenbr++;
        corrtax[num] = outnum++;
      }
      mbndcorrtax[num] = mbndndnum++;
    }
  }
  edgesiz = (PAMPA_Num)(edgenbr*1.1);
  maxnum = outnum;
  /***
   * END: Get number of vertices and relations
   */
  
  
  /***
   * BEGIN: Allocate all tables
   */
  if (memAllocGroup ((void **) (void *)
        &venttax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &edgetax, (size_t) (edgesiz       * sizeof (PAMPA_Num)),
        &verttax, (size_t) ((vertnbr + 1) * sizeof (PAMPA_Num)),
        &vendtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("TETGEN_finalize: out of memory (1)");
    return (1);
  }
  memSet (verttax,  0, (vertnbr + 1) * sizeof (PAMPA_Num));
  memSet (vendtax,  0, vertnbr * sizeof (PAMPA_Num));
  memSet (edgetax, ~0, edgesiz * sizeof (PAMPA_Num));
  venttax -= baseval;
  edgetax -= baseval;
  verttax -= baseval;
  vendtax -= baseval;
  
  /***
   * END: Allocate all tables
   */
  
  /***
   * BEGIN: add edges from the internal mesh
   */
  verttax[baseval] = baseval;
  mintndnum = baseval;
  minttenum = baseval;
  mintfcnum = baseval;
  edgenbr = baseval;
  for (num = baseval ; num < mintvertnbr ; num++) { // Init internalt mesh correlation table
    if (mintventtax[num] == dataptr->tetrent) {
      mintcorrtax[num] = minttenum++;
    }
    if (mintventtax[num] == dataptr->faceent) {
      mintcorrtax[num] = mintfcnum++;
    }
    if (mintventtax[num] == dataptr->nodeent) {
      mintcorrtax[num] = mintndnum++;
    }
  }
  for (num = baseval ; num < mintvertnbr ; num++) {
    vendtax[num] = verttax[num];
    venttax[num] = mintventtax[num];
    
//printf("vendtax[68865] %d\tverttax[68866] %d\tvendtax[68866] %d\n", vendtax[68865], verttax[68866], vendtax[68866]);
//if (vendtax[68983]>edgesiz || vendtax[68866]>edgesiz) {printf("num :%d, venttax : %d\n", num, venttax[num]); exit(0);}
    if (mintventtax[num] == dataptr->tetrent) {
      for (nghbid = mintverttax[num], end = mintvendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = mintedgetax[nghbid];
        if (nghbnum >= num || venttax[nghbnum] != PAMPA_ENTT_DUMMY) { // If the neighbor is not a face to remove
          edgetax[vendtax[num]] = nghbnum;
          vendtax[num]++;
          edgenbr++;
        }
      }
      
      // The tetrahedron is forntier if at least one of it face is in the hashtable
      id = isTetrFaceInHash(mintcorrtax[num], mintptr, hashfacemsk, hashfacetab,
                            1, mintgeomtax, hashnodemsk, hashnodetab, &(vendtax[num]), NULL, ~0, dataptr);
      if (id == ~0)
        return (~0);
      vendtax[num] += id;
    }
    
    if (mintventtax[num] == dataptr->faceent) {
      if ((id = isFaceInHash(mintcorrtax[num], mintptr, hashfacemsk, hashfacetab,
                             1, mintgeomtax, hashnodemsk, hashnodetab, dataptr)) != ~0) { // The face is frontier if it's in the hashtable
        
        for (nghbid = mintverttax[num], end = mintvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mintedgetax[nghbid];
          tmp = mintcorrtax[nghbnum];
          
          if ((mintventtax[nghbnum] == dataptr->nodeent && getBndNum(tmp, mintgeomtax, hashnodemsk, hashnodetab) == ~0) ||
              (mintventtax[nghbnum] == dataptr->faceent && 
                  isFaceInHash(mintcorrtax[nghbnum], mintptr, hashfacemsk, hashfacetab,
                               1, mintgeomtax, hashnodemsk, hashnodetab, dataptr) == ~0) ||
              mintventtax[nghbnum] == dataptr->tetrent) { // The neighboor is not frontier
            
            if (nghbnum >= num || venttax[nghbnum] != PAMPA_ENTT_DUMMY) { // If the neighbor is not a face to remove
              edgetax[vendtax[num]] = nghbnum;
              vendtax[num]++;
              edgenbr++;
            }
          }
        }
        
        if (hashfacetab[id].num == ~0 || hashfacetab[id].val == ~0) { // The face did not exist in the boundary mesh, means it has to be removed
          mintventtax[num] = PAMPA_ENTT_DUMMY;
          
          edgenbr -= (vendtax[num] - verttax[num]);
          vendtax[num] = verttax[num];
          
          // Remove from neighboor adjacence
          for (nghbid = mintverttax[num], end = mintvendtax[num] ; nghbid < end ; nghbid++) {
            nghbnum = mintedgetax[nghbid];
            if (nghbnum < num) {
              for (i = verttax[nghbnum] ; i < vendtax[nghbnum] ; i++) {
                if (edgetax[i] == num) {
                  for (j = i + 1 ; j < vendtax[nghbnum] && edgetax[j] != ~0 ; j++) {}
                  if (venttax[edgetax[j]] != dataptr->nodeent) {
                    edgetax[i] = edgetax[j-1];
                    edgetax[j-1] = ~0;
                    edgenbr --;
                    vendtax[nghbnum] --;
                  }
                  else {
                    base = edgetax[j-1];
                    for (k = j - 2 ; k > i ; k --) {
                      if (venttax[edgetax[k]] == dataptr->nodeent) {
                        edgetax[j-1] = edgetax[k];
                        edgetax[k] = base;
                        base = edgetax[j-1];
                      }
                    }
                    edgetax[i] = base;
                    edgetax[j-1] = ~0;
                    edgenbr --;
                    vendtax[nghbnum] --;
                  }
                }
              }
            }
          }
        }
        else {
          corrtax[hashfacetab[id].num] = num;
          tmp = hashfacetab[id].val;
          hashfacetab[id].val = vendtax[num];
          vendtax[num] += tmp;
          vertnbr --;
        }
        
      }
      else { // The face is not frontier
        for (nghbid = mintverttax[num], end = mintvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mintedgetax[nghbid];
          if (nghbnum >= num || venttax[nghbnum] != PAMPA_ENTT_DUMMY) { // If the neighbor is not a face to remove
            edgetax[vendtax[num]] = nghbnum;
            vendtax[num]++;
            edgenbr++;
          }
        }
      }
    }
    
    if (mintventtax[num] == dataptr->nodeent) {
      mintndnum = mintcorrtax[num];
      
      
      id = getBndNum(mintndnum, mintgeomtax, hashnodemsk, hashnodetab);
      if (id != ~0) { // If num is in the hashtab, it's mean the node is frontier
        for (nghbid = mintverttax[num], end = mintvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mintedgetax[nghbid];
          tmp = mintcorrtax[nghbnum];
          if ((mintventtax[nghbnum] == dataptr->nodeent && getBndNum(tmp, mintgeomtax, hashnodemsk, hashnodetab) == ~0) ||
              (mintventtax[nghbnum] == dataptr->faceent && 
                  isFaceInHash(mintcorrtax[nghbnum], mintptr, hashfacemsk, hashfacetab,
                               1, mintgeomtax, hashnodemsk, hashnodetab, dataptr) == ~0) ||
              mintventtax[nghbnum] == dataptr->tetrent) { // The neighbor is not frontier
            if (nghbnum >= num || venttax[nghbnum] != PAMPA_ENTT_DUMMY) { // If the neighbor is not a face to remove
              edgetax[vendtax[num]] = nghbnum;
              vendtax[num]++;
              edgenbr++;
            }
          }
        }
        corrtax[id] = num;
        tmp = mbndndfgtax[id];
        mbndndfgtax[id] = vendtax[num];
        vendtax[num] += tmp;
      }
      else {
        for (nghbid = mintverttax[num], end = mintvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mintedgetax[nghbid];
          if (nghbnum >= num || venttax[nghbnum] != PAMPA_ENTT_DUMMY) { // If the neighbor is not a face to remove
            edgetax[vendtax[num]] = nghbnum;
            vendtax[num]++;
            edgenbr++;
          }
        }
      }
    }
    
//if (vendtax[num]>edgesiz) {printf("num : %d\tvendtax %d\tventtax %d\n", num, vendtax[num], venttax[num]); exit(0);}
    verttax[num+1] = vendtax[num];
    last = vendtax[num];
  }
  /***
   * END: add edges from the internal mesh
   */
  
  /***
   * BEGIN: add edges from the boundary mesh
   */
  for (num = baseval ; num < mbndvertnbr ; num++) {
    if (corrtax[num] < baseval) {
      corrtax[num] = maxnum++;
    }
    outnum = corrtax[num];
    
    if (outnum >= mintvertnbr) {
      verttax[outnum] = last;
    }
    
    venttax[outnum] = mbndventtax[num];
    
    if (mbndventtax[num] == dataptr->tetrent && isTetrFrontier(mbndcorrtax[num], mbndptr, mbndndfgtax, dataptr) == 1) {
      vendtax[outnum] = verttax[outnum];
      
      id = isTetrFaceInHash(mbndcorrtax[num], mbndptr, hashfacemsk, hashfacetab, 2, NULL, 0, NULL, vendtax, edgetax, outnum, dataptr);
      if (id == ~0)
        return (~0);
      edgenbr += 2 * id;
      
      for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = mbndedgetax[nghbid];
        if (corrtax[nghbnum] < baseval) {
          corrtax[nghbnum] = maxnum++;
        }
        edgetax[vendtax[outnum]] = corrtax[nghbnum];
        vendtax[outnum]++;
        edgenbr++;
      }
      if (verttax[outnum+1] == 0) {
        verttax[outnum+1] = vendtax[outnum];
      }
    }
    else if (mbndventtax[num] == dataptr->faceent && isFaceFrontier(mbndcorrtax[num], mbndptr, mbndndfgtax, dataptr) == 1) {
      if (outnum < mintvertnbr) {
        id = isFaceInHash(mbndcorrtax[num], mbndptr, hashfacemsk, hashfacetab, 2, NULL, 0, NULL, dataptr);
        for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mbndedgetax[nghbid];
          if (corrtax[nghbnum] < baseval) {
            corrtax[nghbnum] = maxnum++;
          }
          edgetax[hashfacetab[id].val] = corrtax[nghbnum];
          hashfacetab[id].val++;
          edgenbr++;
        }
      }
      else {
        for (i = num + 1 ; i < mbndvertnbr && corrtax[i] < mintvertnbr ; i++) {}
        next = corrtax[i];
//        verttax[outnum] = verttax[next];
        vendtax[outnum] = verttax[outnum];
        for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; nghbid++) {
          nghbnum = mbndedgetax[nghbid];
          if (corrtax[nghbnum] < baseval) {
            corrtax[nghbnum] = maxnum++;
          }
          edgetax[vendtax[outnum]] = corrtax[nghbnum];
          vendtax[outnum]++;
          edgenbr++;
        }
        verttax[next] = vendtax[outnum];
        
      }
    }
    else if (mbndventtax[num] == dataptr->nodeent && isNodeFrontier(num, mbndndfgtax) == 1) {
      for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = mbndedgetax[nghbid];
        if (corrtax[nghbnum] < baseval) {
          corrtax[nghbnum] = maxnum++;
        }
        edgetax[mbndndfgtax[num]] = corrtax[nghbnum];
        mbndndfgtax[num]++;
        edgenbr++;
      }
    }
    else { // vertex not on the frontier
      vendtax[outnum] = verttax[outnum];
      
      for (nghbid = mbndverttax[num], end = mbndvendtax[num] ; nghbid < end ; ++ nghbid) {
        nghbnum = mbndedgetax[nghbid];
        if (corrtax[nghbnum] < baseval) {
          corrtax[nghbnum] = maxnum++;
        }
        edgetax[vendtax[outnum]] = corrtax[nghbnum];
        vendtax[outnum]++;
        edgenbr++;
      }
      if (verttax[outnum+1]==0) {
        verttax[outnum+1] = vendtax[outnum];
      }
    }
    
    if (vendtax[outnum] > last) {
      last = vendtax[outnum];
    }
  }
  /***
   * END: add edges from the boundary mesh
   */
  
  /***
   * BEGIN: Remove DUMMY vertices
   */
  PAMPA_Num max;
  for (num = baseval ; num < mintvertnbr ; num++) {
    if (mintventtax[num] == PAMPA_ENTT_DUMMY) {
      for (j = mbndvertnbr - 1 + baseval ; j >= baseval && corrtax[j] != vertnbr - 1 + baseval ; j --) {}
      corrtax[j] = num;
      
      venttax[num] = venttax[vertnbr - 1 + baseval];
      verttax[num] = verttax[vertnbr - 1 + baseval];
      vendtax[num] = vendtax[vertnbr - 1 + baseval];
      
      // Update neigboors edges
      for (nghbid = verttax[num] ; nghbid < vendtax[num] ; nghbid++) {
        nghbnum = edgetax[nghbid];
        for (j = verttax[nghbnum] ; j < vendtax[nghbnum] ; j++) {
          if (edgetax[j] == (vertnbr - 1 + baseval)) {
            edgetax[j] = num;
          }
        }
      }
      vertnbr --;
    }
  }
  /***
   * END: Remove DUMMY vertices
   */
  
  /***
   * BEGIN: Update corelation table
   */
  enttnbr = mbndenttnbr;
  if (memAllocGroup ((void **) (void *)
        &ndcotax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &enttnum, (size_t) (enttnbr       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("PAMPA: out of memory (1)");
    return (1);
  }
  memSet (enttnum, 0, enttnbr * sizeof (PAMPA_Num));
  
  // BUG FIX
  for (num = baseval ; num < vertnbr ; num++) {
    if (verttax[num] > vendtax[num]) {
      max = edgesiz - 1;
      for (i = baseval ; i < vertnbr ; i++) {
        if (verttax[i] > verttax[num] && verttax[i] < max) {
          max = verttax[i];
        }
      }
      for (i = max ; i > verttax[num] && edgetax[i] == ~0; i --) {}
      vendtax[num] = i;
    }
  }
  
  ndcotax -= baseval;
  enttnum -= baseval;
  edgenbr = baseval;
  for (num = baseval ; num < vertnbr + baseval ; num++) {
    ndcotax[num] = enttnum[venttax[num]];
    enttnum[venttax[num]] ++;
    for (nghbid = vendtax[num] - 1 ; nghbid >=verttax[num] ; nghbid --) {
      nghbnum = edgetax[nghbid];
      if (nghbnum < baseval) {
        if (nghbid != (vendtax[num] - 1)) {
          edgetax[nghbid] = edgetax[vendtax[num]];
        }
        vendtax[num] --;
      }
      
      // Bug fix 2
/*      for (id = verttax[nghbnum] ; id < vendtax[nghbnum] ; id++) {
        if (edgetax[id] == num) break;
      }
      if (id == venttax[nghbnum]) {
        if (vendtax[nghbnum] != edgesiz && vendtax[nghbnum] != verttax[nghbnum+1]) {
          edgetax[vendtax[nghbnum]] = num;
          venttax++[nghbnum];
        }
        else {
          edgetax[nghbid] = vertnbr;
          nghbnum = vertnbr;
        }
      }*/
      
      // Gug fix 3
      if (nghbnum == num) {
        edgetax[nghbid] = vertnbr;
        nghbnum = vertnbr;
      }
      
      // Bug fix
      if (nghbnum >= vertnbr) {
        if (nghbid != vendtax[num]) {
          for (i = nghbid ; i < vendtax[num] - 1 ; i++) {
            edgetax[i] = edgetax[i+1];
          }
          edgetax[i] = ~0;
//          edgenbr --;
        }
        else {
          edgetax[nghbid] = ~0;
//          edgenbr --;
        }
        -- vendtax[num];
      }
    }
    edgenbr += (vendtax[num] - verttax[num]);
  }
  /***
   * END: Update corelation table
   */
  
//#ifdef PAMPA_DEBUG_MESH
//  for (num = baseval ; num < mintvertnbr ; num++) {
//    if (mintventtax[num] == dataptr->tetrent || mintventtax[num] == dataptr->faceent) {
//      base = 0;
//      for (i = mintverttax[num] ; i < mintvendtax[num] ; i++) {
//        nghbnum = mintedgetax[i];
//        if (mintventtax[nghbnum] == dataptr->nodeent) {
//          for (j = verttax[num] + base ; j < vendtax[num] ; j++, base++) {
//            if (venttax[edgetax[j]] == dataptr->nodeent) {
//              if (edgetax[j] != nghbnum) {
//                for (k = j+1 ; k < vendtax[num] ; k++) {
//                  if (venttax[edgetax[k]] == dataptr->nodeent && edgetax[k] == nghbnum) {
//                    tmp = edgetax[k];
//                    edgetax[k] = edgetax[j];
//                    edgetax[j] = tmp;
//                  }
//                }
////                errorPrint ("Bad orientation for element %d", num);
//              }
//              break;
//            }
//          }
//        }
//      }
//    }
//  }
//  
//  for (num = baseval ; num < mbndvertnbr ; num++) {
//    outnum = corrtax[num];
//    if (mbndventtax[num] == dataptr->tetrent || mbndventtax[num] == dataptr->faceent) {
//      base = 0;
//      for (i = mbndverttax[num] ; i < mbndvendtax[num] ; i++) {
//        nghbnum = mbndedgetax[i];
//        if (mbndventtax[nghbnum] == dataptr->nodeent) {
//          for (j = verttax[outnum] + base ; j < vendtax[outnum] ; j++, base++) {
//            if (venttax[edgetax[j]] == dataptr->nodeent) {
//              if (edgetax[j] != nghbnum) {
//                for (k = j+1 ; k < vendtax[outnum] ; k++) {
//                  if (venttax[edgetax[k]] == dataptr->nodeent && edgetax[k] == nghbnum) {
//                    tmp = edgetax[k];
//                    edgetax[k] = edgetax[j];
//                    edgetax[j] = tmp;
//                  }
//                }
////                errorPrint ("Bad orientation for element %d", outnum);
//              }
//              break;
//            }
//          }
//        }
//      }
//    }
//  }
//#endif /* PAMPA_DEBUG_MESH */
  
  /***
   * BEGIN: build final mesh and associate values
   */
  PAMPA_meshInit(moutptr);

  //// XXX debut test
  //{
  //  PAMPA_Num vertnum;
  //  PAMPA_Num vertnnd;

  //  for (vertnum = baseval, vertnnd = baseval + vertnbr; vertnum < vertnnd; vertnum ++)
  //    if (verttax[vertnum] == vendtax[vertnum])
  //      venttax[vertnum] = PAMPA_ENTT_DUMMY;

  //}
  //// XXX fin test
  CHECK_FDBG2 (PAMPA_meshBuild( moutptr, baseval, vertnbr, verttax + baseval, vendtax + baseval, NULL, // XXX ajouter vlbltab
        edgenbr,
      edgesiz, edgetax + baseval, NULL, enttnbr, venttax + baseval, NULL, NULL, 50)); // XXX pourquoi 50 comme valeurs max ??
  
#ifdef PAMPA_DEBUG_MESH
  for (num = baseval ; num < vertnbr ; num++) {
    if (verttax[num]>vendtax[num]) printf("end < start, num " PAMPA_NUMSTRING ", type " PAMPA_NUMSTRING "\n", num, venttax[num]);
    if (vendtax[num]>edgesiz+baseval) printf("end > size, num " PAMPA_NUMSTRING ", type " PAMPA_NUMSTRING ", end " PAMPA_NUMSTRING ", size " PAMPA_NUMSTRING ", nbr " PAMPA_NUMSTRING "\n", num, venttax[num], vendtax[num], edgesiz, edgenbr);
  }
#endif /* PAMPA_DEBUG_MESH */
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (PAMPA_meshCheck2 (moutptr, PAMPA_CHECK_ALONE));
#endif /* PAMPA_DEBUG_MESH */

  // XXX Gestion des sous-entitées
  void * minttax;
  void * mbndtax;
  void * mouttax;
  PAMPA_Num tag;
  MPI_Datatype type;
  PAMPA_AvIterator mbnditer;
  MPI_Aint            valusiz;                 /* Extent of attribute datatype */
  MPI_Aint            dummy;
  for (i = 0 ; i < enttnbr ; i++) {
    PAMPA_meshAvItInit (mbndptr, i, &mbnditer);
    
    while (PAMPA_avItHasMore(&mbnditer)) {
      
      tag = PAMPA_avItCurTagNum (&mbnditer);
      type = PAMPA_avItCurTypeVal (&mbnditer);
      
      chekval = PAMPA_meshValueData (mintptr, i, tag, (void **) &minttax);
      if (chekval != 0) {
        errorPrint ("Internale mesh does not have value with tag %d for entity %d", tag, i);
        PAMPA_avItNext(&mbnditer);
        continue;
      }
      
      mbndtax = PAMPA_avItCurValue (&mbnditer);
      
      PAMPA_meshValueLink (moutptr, (void**) &mouttax, PAMPA_VALUE_PUBLIC, type, i, tag);
      MPI_Type_get_extent (type, &dummy, &valusiz);     /* Get type extent */
      mouttax -= baseval;
      
      for (j = baseval ; j < (mintvertnbr + baseval) ; j++) {
        if (mintventtax[j] == i)
          memCpy ((byte *) mouttax + ndcotax[j] * valusiz, (byte *) minttax + mintcorrtax[j] * valusiz, valusiz * sizeof (byte));
      }
      for (j = baseval ; j < (mbndvertnbr + baseval) ; j++) {
        if (mbndventtax[j] == i)
          memCpy ((byte *) mouttax + ndcotax[corrtax[j]] * valusiz, (byte *) mbndtax + mbndcorrtax[j] * valusiz, valusiz * sizeof (byte));
      }
      PAMPA_avItNext(&mbnditer);
    }
  }
  
  PAMPA_Num * restrict mintoflgtax;
  PAMPA_Num * restrict mbndoflgtax;
  PAMPA_Num * restrict moutoflgtax;
  CHECK_FDBG2 (PAMPA_meshValueData (mintptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &mintoflgtax));
  CHECK_FDBG2 (PAMPA_meshValueData (mbndptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &mbndoflgtax));
  chekval = PAMPA_meshValueLink (moutptr, (void **) &moutoflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS);
  if (chekval != 0)
    return chekval;
  
  for (j = baseval ; j < (mintvertnbr + baseval) ; j++) {
    moutoflgtax[j] = mintoflgtax[j];
  }
  for (j = baseval ; j < (mbndvertnbr + baseval) ; j++) {
    moutoflgtax[corrtax[j]] = mbndoflgtax[j];
  }
  /***
   * END: build final mesh and associate values
   */
  
  PAMPA_meshExit(mintptr);
  PAMPA_meshExit(mbndptr);
  
  PAMPA_smeshExit(&smintdat);
  PAMPA_smeshExit(&smbnddat);
  
  memFreeGroup (venttax + baseval);
  memFreeGroup (corrtax + baseval);
  memFreeGroup (ndcotax + baseval);
  memFree (hashnodetab);
  memFree (hashfacetab);
  
  return 0;
}

//! \brief Test if a node if frontier
//!
//! \returns 1   : if the node is forntier.
//! \returns 0   : if not.

int
isNodeFrontier (
PAMPA_Num     const         nodenum,    //!< Id of the node
PAMPA_Num   * const         flagtab)    //!< Vector of frontier flags
{
  if (flagtab[nodenum] == ~0)
    return 0;
  else
    return 1;
}


//! \brief Test if a tetrahedron if frontier
//!
//! \returns 1   : if the tetrahedron is forntier.
//! \returns 0   : if not.

int
isTetrFrontier (
PAMPA_Num     const         tetrnum,    //!< Id of the tetrahedron
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the tetrahedron is
PAMPA_Num   * const         flagtab,    //!< Vector of frontier flags
PAMPA_AdaptInfo  * const  dataptr)
{
  PAMPA_Iterator it_nghb;
  int ret;
  
  PAMPA_meshItInit(meshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  
  ret=0;
  PAMPA_itStart(&it_nghb, tetrnum);
  while (PAMPA_itHasMore(&it_nghb)) {
    PAMPA_Num nodenum;
    
    nodenum = PAMPA_itCurMeshVertNum(&it_nghb);
    ret += isNodeFrontier(nodenum, flagtab);
    PAMPA_itNext(&it_nghb);
  }
  
  if (ret == 3)
    return 1;
  else
    return 0;
}


//! \brief Test if a face if frontier
//!
//! \returns 1   : if the face is forntier.
//! \returns 0   : if not.

int
isFaceFrontier (
PAMPA_Num     const         facenum,    //!< Id of the face
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num   * const         flagtab,    //!< Vector of frontier flags
PAMPA_AdaptInfo  * const  dataptr)
{
  PAMPA_Iterator it_nghb;
  int ret;
  
  PAMPA_meshItInit(meshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
  
  ret=0;
  PAMPA_itStart(&it_nghb, facenum);
  while (PAMPA_itHasMore(&it_nghb)) {
    PAMPA_Num nodenum;
    
    nodenum = PAMPA_itCurMeshVertNum(&it_nghb);
    ret += isNodeFrontier(nodenum, flagtab);
    PAMPA_itNext(&it_nghb);
  }
  
  if (ret == 3)
    return 1;
  else
    return 0;
}


//! \brief Get the boundary num from the global
//!
//! \returns id   : on success.
//! \returns ~0   : if the vertex is not in the correlation table.

int
getBndNum (
PAMPA_Num     const         num,        //!< Global num
double      * const         geomtab,    //!< Coordinates table
PAMPA_Num     const         hashmsk,    //!< Mask of the hashtable
NodeHash    * const         hashtab)    //!< Hash table
{
  PAMPA_Num coorsum;
  PAMPA_Num hashnum;

  coorsum = geomtab[num * 3] + geomtab[num * 3 + 1] + geomtab[num * 3 + 2];
  for (hashnum = (coorsum * MESHREBLDHASHPRIME) & hashmsk;
      hashtab[hashnum].coorsum != ~0 &&
      (hashtab[hashnum].coorsum != coorsum
       || hashtab[hashnum].coortab[0] != geomtab[num * 3]
       || hashtab[hashnum].coortab[1] != geomtab[num * 3 + 1]
       || hashtab[hashnum].coortab[2] != geomtab[num * 3 + 2]
      ); hashnum = (hashnum + 1) & hashmsk) ;

  if ((hashtab[hashnum].coorsum == coorsum)
      && (hashtab[hashnum].coortab[0] == geomtab[num * 3])
      && (hashtab[hashnum].coortab[1] == geomtab[num * 3 + 1])
      && (hashtab[hashnum].coortab[2] == geomtab[num * 3 + 2]))  /* Node found */
    return hashtab[hashnum].id;
  else
    return (~0);
}


//! \brief Test if a face is in the hash table
//!
//! \returns id   : index of the face in the table.
//! \returns ~0   : if the face is not in the table.

int
isFaceInHash (
PAMPA_Num     const         num,          //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,      //!< Mesh in which the face is
PAMPA_Num     const         hashfacemsk,  //!< Mask of the hashtable
FaceHash    * const         hashfacetab,  //!< Hash table
int           const         origin,       // 1 if it's the internal mesh, else 2
double      * const         geomtab,      //!< Coordinates table (internal)
PAMPA_Num     const         hashnodemsk,  //!< Mask of the hashtable
NodeHash    * const         hashnodetab,  //!< Hash table (node hashtable)
PAMPA_AdaptInfo  * const  dataptr)
{
  PAMPA_Num tmp;
  PAMPA_Num tmp2;
  PAMPA_Num hashnum;
  int i;
  PAMPA_Num tab[3];
  
  PAMPA_Iterator it_nghb;
  
  PAMPA_meshItInit(meshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
  
  tmp=0;
  i=0;
  PAMPA_itStart(&it_nghb, num);
  
  if (origin == 1) {
    while (PAMPA_itHasMore(&it_nghb)) {
      PAMPA_Num nodenum;
      
      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
      tmp2 = getBndNum (nodenum, geomtab, hashnodemsk, hashnodetab);
      if (tmp2 == ~0) {
        return ~0;
      }
      tmp += tmp2;
      tab[i++] = tmp2;
      PAMPA_itNext(&it_nghb);
    }
  }
  else {
    while (PAMPA_itHasMore(&it_nghb)) {
      PAMPA_Num nodenum;
      
      nodenum = PAMPA_itCurMeshVertNum(&it_nghb);
      tmp += nodenum;
      tab[i++] = nodenum;
      PAMPA_itNext(&it_nghb);
    }
  }
  
  intSort1asc1 (tab, 3);

  for (hashnum = (tmp * MESHREBLDHASHPRIME) & hashfacemsk;
      hashfacetab[hashnum].nodesum != ~0 &&
      (hashfacetab[hashnum].nodesum != tmp
       || hashfacetab[hashnum].nodetab[0] != tab[0]
       || hashfacetab[hashnum].nodetab[1] != tab[1]
       || hashfacetab[hashnum].nodetab[2] != tab[2]
      ); hashnum = (hashnum + 1) & hashfacemsk) ;

  if ((hashfacetab[hashnum].nodesum == tmp)
      && (hashfacetab[hashnum].nodetab[0] == tab[0])
      && (hashfacetab[hashnum].nodetab[1] == tab[1])
      && (hashfacetab[hashnum].nodetab[2] == tab[2]))  /* Face found */
    return hashnum;
  else
    return (~0);
}


//! \brief Add a face is in the hash table
//!
//! \returns id   : index of the face in the table.
//! \returns ~0   : on error.

int
addFaceInHash (
PAMPA_Num     const         num,        //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num   * const         hashnbr,    //!< Number of elements in the hash table
PAMPA_Num   * const         hashsiz,    //!< Size of the hash table
PAMPA_Num   * const         hashmax,    //!< Maximum size of the hash table
PAMPA_Num   * const         hashmsk,    //!< Mask of the hash table
FaceHash  * * const         hashtab,    //!< Hash table
PAMPA_AdaptInfo  * const  dataptr)
{
  PAMPA_Num tmp;
  PAMPA_Num hashnum;
  int i;
  PAMPA_Num tab[3];
  
  PAMPA_Iterator it_nghb;
  
  PAMPA_meshItInit(meshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
  
  tmp=0;
  i=0;
  PAMPA_itStart(&it_nghb, num);
  while (PAMPA_itHasMore(&it_nghb)) {
    PAMPA_Num nodenum;
    
    nodenum = PAMPA_itCurMeshVertNum(&it_nghb);
    tmp += nodenum;
    tab[i++] = nodenum;
    PAMPA_itNext(&it_nghb);
  }

  intSort1asc1 (tab, 3);

  for (hashnum = (tmp * MESHREBLDHASHPRIME) & (*hashmsk);
      (*hashtab)[hashnum].nodesum != ~0 &&
      ((*hashtab)[hashnum].nodesum != tmp
       || (*hashtab)[hashnum].nodetab[0] != tab[0]
       || (*hashtab)[hashnum].nodetab[1] != tab[1]
       || (*hashtab)[hashnum].nodetab[2] != tab[2]
      ); hashnum = (hashnum + 1) & (*hashmsk)) ;

#ifdef PAMPA_DEBUG_ADAPT
  if (((*hashtab)[hashnum].nodesum == tmp)
      && ((*hashtab)[hashnum].nodetab[0] == tab[0])
      && ((*hashtab)[hashnum].nodetab[1] == tab[1])
      && ((*hashtab)[hashnum].nodetab[2] == tab[2])) {  /* Face found */
    errorPrint ("internal error");
    return     (~0);
  }
#endif /* PAMPA_DEBUG_ADAPT */
  
  (*hashnbr) ++;
  (*hashtab)[hashnum].nodesum    = tmp;
  (*hashtab)[hashnum].nodetab[0] = tab[0];
  (*hashtab)[hashnum].nodetab[1] = tab[1];
  (*hashtab)[hashnum].nodetab[2] = tab[2];
  (*hashtab)[hashnum].index = NULL;
  (*hashtab)[hashnum].val = ~0;
  
  if (*hashnbr >= *hashmax) /* If hashtab is too much filled */
    CHECK_FDBG2 (meshRebldFaceResize (hashtab, hashsiz, hashmax, hashmsk));
  
  
  return hashnum;
}


//! \brief Test if a tetrahedron's face is in the hash table
//!
//! \returns nbr  : number of faces in the table.

int
isTetrFaceInHash (
PAMPA_Num     const         num,        //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num     const         hashmsk,    //!< Mask of the hash table
FaceHash    * const         hashtab,    //!< Hash table
int           const         origin,       // 1 if it's the internal mesh, else 2
double      * const         geomtab,      //!< Coordinates table (internal)
PAMPA_Num     const         hashnodemsk,  //!< Value used to get the key of the element
NodeHash    * const         hashnodetab,    //!< Hash table (node hashtable)
PAMPA_Num   * const         vendtab,
PAMPA_Num   * const         edgetab,
PAMPA_Num     const         outnum,
PAMPA_AdaptInfo  * const  dataptr)
{
  PAMPA_Num tmp;
  PAMPA_Num nodetab[4];
  PAMPA_Num sum[4] = {0,0,0,0};
  PAMPA_Num face[4][3];
  PAMPA_Num const faceidx[4][3] = {{0,1,2},{0,1,3},{0,2,3},{1,2,3}};
  int i;
  int j;
  int k;
  int nbr;
  
  PAMPA_Iterator it_nghb;
  
  PAMPA_meshItInit(meshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  
  i=0;
  PAMPA_itStart(&it_nghb, num);
  while (PAMPA_itHasMore(&it_nghb)) {
    PAMPA_Num nodenum;
    
    if (origin == 1) {
      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
      nodenum = getBndNum (nodenum, geomtab, hashnodemsk, hashnodetab);
    }
    else
      nodenum = PAMPA_itCurMeshVertNum(&it_nghb);
    //
    sum[faceidx[i][0]] += nodenum;
    sum[faceidx[i][1]] += nodenum;
    sum[faceidx[i][2]] += nodenum;
    nodetab[i] = nodenum; // XXX attention le i d'ici et de la ligne au-dessus ne sont pas les mêmes (resp. |noeuds| et |faces| !!!

    i++;
    PAMPA_itNext(&it_nghb);
  }
  
  nbr = 0;
  for (j=0 ; j<4 ; j++) {
    PAMPA_Num hashnum;

    for (int i = 0 ; i < 3 ; i ++) {
      face[j][i] = nodetab[faceidx[j][i]];
      if (face[j][i] < 0) {
        sum[j] = ~0;
        break;
      }
    }

    if (sum[j] < 0) continue;

    intSort1asc1 (face[j], 3);

    for (hashnum = (sum[j] * MESHREBLDHASHPRIME) & hashmsk;
        hashtab[hashnum].nodesum != ~0 &&
        (hashtab[hashnum].nodesum != sum[j]
         || hashtab[hashnum].nodetab[0] != face[j][0]
         || hashtab[hashnum].nodetab[1] != face[j][1]
         || hashtab[hashnum].nodetab[2] != face[j][2]
        ); hashnum = (hashnum + 1) & hashmsk) ;

//#ifdef PAMPA_DEBUG_ADAPT
//    if ((hashtab[hashnum].nodesum != sum[j])
//        || (hashtab[hashnum].nodetab[0] != face[j][0])
//        || (hashtab[hashnum].nodetab[1] != face[j][1])
//        || (hashtab[hashnum].nodetab[2] != face[j][2])) {  /* Face not found */
//      errorPrint ("internal error");
//      return     (~0);
//    }
//#endif /* PAMPA_DEBUG_ADAPT */

    if ((hashtab[hashnum].nodesum == sum[j])
        && (hashtab[hashnum].nodetab[0] == face[j][0])
        && (hashtab[hashnum].nodetab[1] == face[j][1])
        && (hashtab[hashnum].nodetab[2] == face[j][2])) {  /* Face found */
      if (origin == 1) {
        nbr++;
        hashtab[hashnum].index = vendtab;
        hashtab[hashnum].id = num;
      }
      else {
        if (hashtab[hashnum].index == NULL)
          continue;

        nbr++;
        for (k = *(hashtab[hashnum].index) - 1 ; edgetab[k] == ~0 ; k --) ;

        edgetab[k+1] = outnum;
        edgetab[vendtab[outnum]] = hashtab[hashnum].id;
        vendtab[outnum] ++;
      }
    }
  }
  
  return nbr;
}

//! \brief Add a tetrahedron's face is in the hash table
//!
//! \returns nbr  : number of faces added in the table.

int
addTetrFaceInHash (
PAMPA_Num     const         num,        //!< Num in the mesh
PAMPA_Mesh  * const         meshptr,    //!< Mesh in which the face is
PAMPA_Num   * const         hashnbr,    //!< Number of elements in the hash table
PAMPA_Num   * const         hashsiz,    //!< Size of the hash table
PAMPA_Num   * const         hashmax,    //!< Maximum size of the hash table
PAMPA_Num   * const         hashmsk,    //!< Value used to get the key of the element
FaceHash  * * const         hashtab,    //!< Hash table
PAMPA_Num   * const         flagtab,    //!< Vector of frontier flags
PAMPA_AdaptInfo  * const  dataptr)
{
  PAMPA_Num ret;
  PAMPA_Num tmp;
  int i, j, k;
  PAMPA_Num face[4][3];
  PAMPA_Num nodetab[4];
  PAMPA_Num const faceidx[4][3] = {{0,1,2},{0,1,3},{0,2,3},{1,2,3}};
  
  PAMPA_Iterator it_nghb;
  
  PAMPA_meshItInit(meshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  
  tmp=0;
  i=0;
  PAMPA_itStart(&it_nghb, num);
  while (PAMPA_itHasMore(&it_nghb)) {
    PAMPA_Num nodenum;

    nodenum = PAMPA_itCurMeshVertNum(&it_nghb);
    nodetab[i] = nodenum; // XXX attention le i d'ici et de la ligne au-dessus ne sont pas les mêmes (resp. |noeuds| et |faces| !!!

    i++;
    PAMPA_itNext(&it_nghb);
  }
  
  ret = 0;
  for (i = 0 ; i < 4 ; i++) {
    tmp = 0;
    k = 0;

    face[i][0] = nodetab[faceidx[i][0]];
    face[i][1] = nodetab[faceidx[i][1]];
    face[i][2] = nodetab[faceidx[i][2]];

    intSort1asc1 (face[i], 3);
    
    for (j = 0 ; j < 3 ; j++) {
      if (isNodeFrontier(face[i][j], flagtab) == 1) {
        tmp += face[i][j];
        k++;
      }
    }
    
    // The face i is frontier
    if (k == 3) {
      PAMPA_Num hashnum;

      k = 0;
      
      //
      for (hashnum = (tmp * MESHREBLDHASHPRIME) & (*hashmsk);
          (*hashtab)[hashnum].nodesum != ~0 &&
          ((*hashtab)[hashnum].nodesum != tmp
           || (*hashtab)[hashnum].nodetab[0] != face[i][0]
           || (*hashtab)[hashnum].nodetab[1] != face[i][1]
           || (*hashtab)[hashnum].nodetab[2] != face[i][2]
          ); hashnum = (hashnum + 1) & (*hashmsk)) ;

      if (((*hashtab)[hashnum].nodesum == tmp)
          && ((*hashtab)[hashnum].nodetab[0] == face[i][0])
          && ((*hashtab)[hashnum].nodetab[1] == face[i][1])
          && ((*hashtab)[hashnum].nodetab[2] == face[i][2])) {  /* Face found */
        k = 1; // XXX pourquoi 1 ?
      }


      // The face is not in the table
      if (k != 1) {
        ret++;
        (*hashtab)[hashnum].nodesum = tmp;
        (*hashtab)[hashnum].nodetab[0] = face[i][0];
        (*hashtab)[hashnum].nodetab[1] = face[i][1];
        (*hashtab)[hashnum].nodetab[2] = face[i][2];
        (*hashtab)[hashnum].index = NULL;
        (*hashtab)[hashnum].val = ~0;
        (*hashnbr) ++;

        if (*hashnbr >= *hashmax) /* If hashtab is too much filled */
          CHECK_FDBG2 (meshRebldFaceResize (hashtab, hashsiz, hashmax, hashmsk));
      }

    }
  }
  
  return ret;
}


static
  int
meshRebldNodeResize (
    NodeHash * restrict * hashtabptr,
    PAMPA_Num * restrict const           hashsizptr,
    PAMPA_Num * restrict const           hashmaxptr,
    PAMPA_Num * restrict const           hashmskptr)
{
  PAMPA_Num                          oldhashsiz;          /* Size of hash table    */
  PAMPA_Num                          hashnum;          /* Hash value            */
  PAMPA_Num                          oldhashmsk;
  int                           cheklocval;
  PAMPA_Num hashidx;
  PAMPA_Num hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_ADAPT
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].coorsum == coorsum)
          && ((*hashtabptr)[hashnum].coortab[0] == coortab[0])
          && ((*hashtabptr)[hashnum].coortab[1] == coortab[1])
          && ((*hashtabptr)[hashnum].coortab[2] == coortab[2])
          )
        errorPrint ("duplicated item in NodeHashTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_ADAPT */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (NodeHash *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (NodeHash)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VDBG2 (cheklocval);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (NodeHash)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].coorsum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (coorsum * MESHREBLDHASHPRIME) & (*hashmskptr);
        (*hashtabptr)[hashnum].coorsum != ~0 &&
        ((*hashtabptr)[hashnum].coorsum != coorsum
         || (*hashtabptr)[hashnum].coortab[0] != coortab[0]
         || (*hashtabptr)[hashnum].coortab[1] != coortab[1]
         || (*hashtabptr)[hashnum].coortab[2] != coortab[2]
        ); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].coorsum = ~0;
  }

#ifdef PAMPA_DEBUG_ADAPT
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].coorsum == coorsum)
          && ((*hashtabptr)[hashnum].coortab[0] == coortab[0])
          && ((*hashtabptr)[hashnum].coortab[1] == coortab[1])
          && ((*hashtabptr)[hashnum].coortab[2] == coortab[2])
          )
        errorPrint ("duplicated item in NodeHashTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_ADAPT */
  return (0);
}

static
  int
meshRebldFaceResize (
    FaceHash * restrict * hashtabptr,
    PAMPA_Num * restrict const           hashsizptr,
    PAMPA_Num * restrict const           hashmaxptr,
    PAMPA_Num * restrict const           hashmskptr)
{
  PAMPA_Num                          oldhashsiz;          /* Size of hash table    */
  PAMPA_Num                          hashnum;          /* Hash value            */
  PAMPA_Num                          oldhashmsk;
  int                           cheklocval;
  PAMPA_Num hashidx;
  PAMPA_Num hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_ADAPT
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num nodesum;
    double    nodetab[3];

    nodesum = (*hashtabptr)[hashidx].nodesum;
    for (int i = 0 ; i < 3 ; i ++)
      nodetab[i] = (*hashtabptr)[hashidx].nodetab[i];

    if (nodesum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].nodesum == nodesum)
          && ((*hashtabptr)[hashnum].nodetab[0] == nodetab[0])
          && ((*hashtabptr)[hashnum].nodetab[1] == nodetab[1])
          && ((*hashtabptr)[hashnum].nodetab[2] == nodetab[2])
          )
        errorPrint ("duplicated item in FaceHashTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_ADAPT */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (FaceHash *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (FaceHash)))) == NULL) {
    errorPrint ("Out of memory");
    cheklocval = 1;
  }
  CHECK_VDBG2 (cheklocval);

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (FaceHash)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].nodesum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num nodesum;
    double    nodetab[3];

    nodesum = (*hashtabptr)[hashidx].nodesum;
    for (int i = 0 ; i < 3 ; i ++)
      nodetab[i] = (*hashtabptr)[hashidx].nodetab[i];

    if (nodesum == ~0) // If empty slot
      continue;

    for (hashnum = (nodesum * MESHREBLDHASHPRIME) & (*hashmskptr);
        (*hashtabptr)[hashnum].nodesum != ~0 &&
        ((*hashtabptr)[hashnum].nodesum != nodesum
         || (*hashtabptr)[hashnum].nodetab[0] != nodetab[0]
         || (*hashtabptr)[hashnum].nodetab[1] != nodetab[1]
         || (*hashtabptr)[hashnum].nodetab[2] != nodetab[2]
        ); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].nodesum = ~0;
  }

#ifdef PAMPA_DEBUG_ADAPT
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num nodesum;
    double    nodetab[3];

    nodesum = (*hashtabptr)[hashidx].nodesum;
    for (int i = 0 ; i < 3 ; i ++)
      nodetab[i] = (*hashtabptr)[hashidx].nodetab[i];

    if (nodesum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].nodesum == nodesum)
          && ((*hashtabptr)[hashnum].nodetab[0] == nodetab[0])
          && ((*hashtabptr)[hashnum].nodetab[1] == nodetab[1])
          && ((*hashtabptr)[hashnum].nodetab[2] == nodetab[2])
          )
        errorPrint ("duplicated item in FaceHashTab, hashidx: %d, hashnum: %d", hashidx, hashnum);
  }
#endif /* PAMPA_DEBUG_ADAPT */
  return (0);
}
