/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_induce_multiple.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 20 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the CPP API for   */
/* the remesher handling routines.      */
/*                                      */
/****************************************/

#include <mpi.h>
//extern "C" {
#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
//}
#include "library_mesh_induce_multiple.h"
#include "library.h"

#ifdef TETGEN_LIBRARY
#include "../tetgen/libpampa-tetgen/pampa-tetgen.h"
#include "../tetgen/libpampa-tetgen/types.h"
#endif

#ifdef GMSH_LIBRARY
#include "../gmsh/libpampa-gmsh/pampa-gmsh.h"
#include "../gmsh/libpampa-gmsh/types.h"
#endif




//! \brief This routine extract the skin of mesh
//! and create two meshes
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_meshInduceMultiple (
PAMPA_Mesh  * const         pminptr,    //!< Input mesh (will be free)
PAMPA_Mesh  *               mintptr,    //!< Pointer to the internal mesh
PAMPA_Mesh  *               mbndptr,    //!< Pointer to the boundaries mesh
PAMPA_AdaptInfo  * const  dataptr)              //!< XXX
{
  
  PAMPA_Smesh smshdat;
  
  PAMPA_Num baseval;
  
  // Datas from the input mesh
  PAMPA_Num  vertnbr;
  PAMPA_Num* verttax;
  PAMPA_Num* vendtax;
  PAMPA_Num  edgenbr;
  PAMPA_Num  edgesiz;
  PAMPA_Num* edgetax;
  PAMPA_Num  enttnbr;
  PAMPA_Num* venttax;
    
  PAMPA_Num* iflgtax;
  double* geomtax;
  
  
  // Datas for the internal mesh
  PAMPA_Num  mintvertnbr;
  PAMPA_Num* mintverttax;
  PAMPA_Num* mintvendtax;
  PAMPA_Num  mintedgenbr;
  PAMPA_Num  mintedgesiz;
  PAMPA_Num* mintedgetax;
  PAMPA_Num  mintenttnbr;
  PAMPA_Num* mintventtax;
  
  PAMPA_Num  mintnodenbr;
  PAMPA_Num  minttetrnbr;
  PAMPA_Num  mintfacenbr;
  double* mintgeomtax;
  
  
  // Datas for the boundaries mesh
  PAMPA_Num  mbndvertnbr;
  PAMPA_Num* mbndverttax;
  PAMPA_Num* mbndvendtax;
  PAMPA_Num  mbndedgenbr;
  PAMPA_Num  mbndedgesiz;
  PAMPA_Num* mbndedgetax;
  PAMPA_Num  mbndenttnbr;
  PAMPA_Num* mbndventtax;
  
  PAMPA_Num  mbndnodenbr;
  PAMPA_Num  mbndtetrnbr;
  PAMPA_Num  mbndfacenbr;
  double* mbndgeomtax;
  PAMPA_Num* mbndndfgtax;
  
  
  PAMPA_Num* flagtax;
  PAMPA_Num* corrtax;       // Correlation table for the initial mesh
  PAMPA_Num* mintcorrtax;   // Correlation table internal num vs initial mesh
  PAMPA_Num* mintvecotax;   // Correlation table internal vertex num
  PAMPA_Num* mbndcorrtax;   // Correlation table extrenal num
  PAMPA_Num* mbndvecotax;   // Correlation table external vertex num
  
  PAMPA_Num nodenum;        // Counter for global nodes
  PAMPA_Num tetrnum;        // Counter for global tetra
  PAMPA_Num facenum;        // Counter for global faces
  PAMPA_Num mintnum;        // Counter for internal
  PAMPA_Num mbndnum;        // Counter for external
  PAMPA_Num mintndnum;      // Counter for internal node
  PAMPA_Num mbndndnum;      // Counter for external node
  PAMPA_Num minttenum;      // Counter for internal tetra
  PAMPA_Num mbndtenum;      // Counter for external tetra
  PAMPA_Num mintfcnum;      // Counter for internal face
  PAMPA_Num mbndfcnum;      // Counter for external face
  
  PAMPA_Num num;
  PAMPA_Num nghbnum;
  PAMPA_Num nghbid;
  PAMPA_Num end;
  
  int front;
  int chekval;
  
  CHECK_FDBG2 (PAMPA_meshValueData(pminptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &iflgtax));
  CHECK_FDBG2 (PAMPA_meshValueData(pminptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &geomtax));
  
  PAMPA_smeshInit(&smshdat);
  PAMPA_mesh2smesh(pminptr, &smshdat);
  
  PAMPA_smeshData(&smshdat, &baseval, &enttnbr, &vertnbr, &verttax, &vendtax, &edgenbr, &edgesiz, &edgetax, &venttax, NULL, NULL);
  verttax -= baseval;
  vendtax -= baseval;
  edgetax -= baseval;
  venttax -= baseval;
  
  iflgtax -= baseval;
  geomtax -= 3 * baseval;
  
  
  mintvertnbr = 0;
  mintnodenbr = 0;
  minttetrnbr = 0;
  mintfacenbr = 0;
  mintedgenbr = 0;
  mintenttnbr = enttnbr;
  
  mbndvertnbr = 0;
  mbndnodenbr = 0;
  mbndtetrnbr = 0;
  mbndfacenbr = 0;
  mbndedgenbr = 0;
  mbndenttnbr = enttnbr;
  
#ifdef PAMPA_DEBUG_ADAPT
  for (num = baseval ; num < vertnbr ; num++) {
    if (verttax[num] > (edgenbr + baseval)) {
      errorPrint ("verttax out of range");
      return (1);
    }
    if (verttax[num] < baseval) {
      errorPrint ("verttax under baseval");
      return (1);
    }
    if (vendtax[num] > (edgenbr + baseval)) {
      errorPrint ("vendtax out of range");
      return (1);
    }
    if (vendtax[num] < baseval) {
      errorPrint ("vendtax under baseval");
      return (1);
    }
  }
  for (num = baseval ; num < edgesiz ; num++) {
    if (edgetax[num] > (vertnbr + baseval)) {
      errorPrint ("edgetax out of range");
      return (1);
    }
    if (edgetax[num] < baseval) {
      errorPrint ("edgetax under baseval");
      return (1);
    }
  }
#endif /* PAMPA_DEBUG_ADAPT */
  
  /***
   * BEGIN: update flags PAMPA_TAG_VERT_ to FRONTIER or EXTERNAL and get the vertices numbers of each sub-meshes
   */
  if ((flagtax = (PAMPA_Num *) memAlloc (vertnbr * sizeof (PAMPA_Num))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  flagtax -= baseval;
  
  for (num = baseval ; num < vertnbr ; num++) {
    front = 0;
    
    if (iflgtax[num] != PAMPA_TAG_VERT_INTERNAL) {
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        if (iflgtax[nghbnum] == PAMPA_TAG_VERT_INTERNAL) {
          if (venttax[num] == dataptr->nodeent) {
            mintnodenbr++;
            mbndnodenbr++;
          }
          if (venttax[num] == dataptr->tetrent) {
            minttetrnbr++;
            mbndtetrnbr++;
          }
          if (venttax[num] == dataptr->faceent) {
            mintfacenbr++;
            mbndfacenbr++;
          }
          flagtax[num] = PAMPA_TAG_VERT_FRONTIER;
          
          if (venttax[num] != dataptr->tetrent) {
            mintvertnbr++;
          }
          mbndvertnbr++;
          
          front = 1;
          break;
        }
      }
      
      if (front == 0) {
        if (venttax[num] == dataptr->nodeent) {
          mbndnodenbr++;
        }
        if (venttax[num] == dataptr->tetrent) {
          mbndtetrnbr++;
        }
        if (venttax[num] == dataptr->faceent) {
          mbndfacenbr++;
        }
        flagtax[num] = PAMPA_TAG_VERT_EXTERNAL;
        mbndvertnbr++;
      }
    }
    else {
      if (venttax[num] == dataptr->nodeent) {
        mintnodenbr++;
      }
      if (venttax[num] == dataptr->tetrent) {
        minttetrnbr++;
      }
      if (venttax[num] == dataptr->faceent) {
        mintfacenbr++;
      }
      flagtax[num] = PAMPA_TAG_VERT_INTERNAL;
      mintvertnbr++;
    }
  }
  /***
   * END: update flags PAMPA_TAG_VERT_ to FRONTIER or EXTERNAL and get the vertices numbers of each sub-meshes
   */
  
  if (mintvertnbr == 0) {
    errorPrint ("nothing to remesh");
    return (1);
  }
  if (mbndvertnbr==0) {
    errorPrint ("no boundary elements");
    return (1);
  }
  
  /***
   * BEGIN: Allocate all tables
   */
  if (memAllocGroup ((void **) (void *)
        &corrtax,     (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &mintcorrtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &mintvecotax, (size_t) (mintvertnbr   * sizeof (PAMPA_Num)),
        &mbndcorrtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &mbndvecotax, (size_t) (mbndvertnbr   * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }
  corrtax -= baseval;
  mintcorrtax -= baseval;
  mintvecotax -= baseval;
  mbndcorrtax -= baseval;
  mbndvecotax -= baseval;
  
  if ((mintgeomtax = (double *) memAlloc (3 * mintnodenbr * sizeof (double))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  mintgeomtax -= 3 * baseval;
  
  
  if ((mbndgeomtax = (double *) memAlloc (3 * mbndnodenbr * sizeof (double))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  mbndgeomtax -= 3 * baseval;
  
  
  if ((mbndndfgtax = (PAMPA_Num *) memAlloc (mbndvertnbr * sizeof (PAMPA_Num))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  mbndndfgtax -= baseval;
  /***
   * END: Allocate all tables
   */
  
  /***
   * BEGIN: get the edges numbers of each sub-meshes
   */
  nodenum = baseval;
  tetrnum = baseval;
  facenum = baseval;
  mintnum = baseval;
  mintndnum = baseval;
  minttenum = baseval;
  mintfcnum = baseval;
  mbndnum = baseval;
  mbndndnum = baseval;
  mbndtenum = baseval;
  mbndfcnum = baseval;
  for (num = baseval ; num < vertnbr ; num++) {
    if (flagtax[num] == PAMPA_TAG_VERT_INTERNAL) {
      if (venttax[num] == dataptr->nodeent) {
        corrtax[num] = nodenum++;
        mintvecotax[mintnum] = mintndnum++;
      }
      if (venttax[num] == dataptr->tetrent) {
        corrtax[num] = tetrnum++;
        mintvecotax[mintnum] = minttenum++;
      }
      if (venttax[num] == dataptr->faceent) {
        corrtax[num] = facenum++;
        mintvecotax[mintnum] = mintfcnum++;
      }
      mintcorrtax[num] = mintnum++;
      
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        if (flagtax[nghbnum] != PAMPA_TAG_VERT_FRONTIER || venttax[nghbnum] != dataptr->tetrent) {
          mintedgenbr++;
        }
      }
    }
    
    if (flagtax[num] == PAMPA_TAG_VERT_EXTERNAL) {
      if (venttax[num] == dataptr->nodeent) {
        mbndndfgtax[mbndnum] = -1;
        corrtax[num] = nodenum++;
        mbndvecotax[mbndnum] = mbndndnum++;
      }
      if (venttax[num] == dataptr->tetrent) {
        mbndndfgtax[mbndnum] = 2;
        corrtax[num] = tetrnum++;
        mbndvecotax[mbndnum] = mbndtenum++;
      }
      if (venttax[num] == dataptr->faceent) {
        mbndndfgtax[mbndnum] = 2;
        corrtax[num] = facenum++;
        mbndvecotax[mbndnum] = mbndfcnum++;
      }
      mbndcorrtax[num] = mbndnum++;
      mbndedgenbr += (vendtax[num] - verttax[num]);
    }
    
    if (flagtax[num] == PAMPA_TAG_VERT_FRONTIER && venttax[num] == dataptr->tetrent) {
      mbndndfgtax[mbndnum] = 2;
      corrtax[num] = tetrnum++;
      mbndvecotax[mbndnum] = mbndtenum++;
      mbndcorrtax[num] = mbndnum++;
      mbndedgenbr += (vendtax[num] - verttax[num] - 1); // We don't take into account the relation with the internal tetrahedron
    }
    
    if (flagtax[num] == PAMPA_TAG_VERT_FRONTIER && venttax[num] != dataptr->tetrent) {
      if (venttax[num] == dataptr->nodeent) {
        mbndndfgtax[mbndnum] = 1;
        corrtax[num] = nodenum++;
        mintvecotax[mintnum] = mintndnum++;
        mbndvecotax[mbndnum] = mbndndnum++;
      }
      if (venttax[num] == dataptr->faceent) {
        mbndndfgtax[mbndnum] = 2;
        corrtax[num] = facenum++;
        mintvecotax[mintnum] = mintfcnum++;
        mbndvecotax[mbndnum] = mbndfcnum++;
      }
      mintcorrtax[num] = mintnum++;
      mbndcorrtax[num] = mbndnum++;
      
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        if (flagtax[nghbnum] == PAMPA_TAG_VERT_INTERNAL ||
            (flagtax[nghbnum] == PAMPA_TAG_VERT_FRONTIER && venttax[nghbnum] != dataptr->tetrent)) {
          mintedgenbr++;
        }
        if (flagtax[nghbnum] == PAMPA_TAG_VERT_EXTERNAL || flagtax[nghbnum] == PAMPA_TAG_VERT_FRONTIER) {
          mbndedgenbr++;
        }
      }
    }
  }
  
  mintedgesiz = mintedgenbr;
  mbndedgesiz = mbndedgenbr;
  /***
   * end: get the edges numbers of each sub-meshes
   */
  
  if (memAllocGroup ((void **) (void *)
        &mintventtax, (size_t) (mintvertnbr       * sizeof (PAMPA_Num)),
        &mintedgetax, (size_t) (mintedgesiz       * sizeof (PAMPA_Num)),
        &mintverttax, (size_t) ((mintvertnbr + 1) * sizeof (PAMPA_Num)),
        &mintvendtax, (size_t) (mintvertnbr       * sizeof (PAMPA_Num)),
        &mbndventtax, (size_t) (mbndvertnbr       * sizeof (PAMPA_Num)),
        &mbndedgetax, (size_t) (mbndedgesiz       * sizeof (PAMPA_Num)),
        &mbndverttax, (size_t) ((mbndvertnbr + 1) * sizeof (PAMPA_Num)),
        &mbndvendtax, (size_t) (mbndvertnbr       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }
  mintventtax -= baseval;
  mintedgetax -= baseval;
  mintverttax -= baseval;
  mintvendtax -= baseval;
  mbndventtax -= baseval;
  mbndedgetax -= baseval;
  mbndvendtax -= baseval;
  
  mbndedgenbr = mintedgenbr = 0;

  memSet (mintverttax, ~0, (mintvertnbr + 1) * sizeof (PAMPA_Num));
  memSet (mintventtax, ~0, mintvertnbr       * sizeof (PAMPA_Num));

  /***
   * BEGIN: add edge in each sub-meshes
   */
  mintverttax[baseval] = baseval;
  mbndverttax[baseval] = baseval;
  mintnum = baseval;
  mbndnum = baseval;
  for (num = baseval ; num < vertnbr ; num++) {
    if (flagtax[num] == PAMPA_TAG_VERT_INTERNAL) {
      mintvendtax[mintnum] = mintverttax[mintnum];
      
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        if (flagtax[nghbnum] != PAMPA_TAG_VERT_FRONTIER || venttax[nghbnum] != dataptr->tetrent) {
          mintedgetax[mintvendtax[mintnum]++] = mintcorrtax[nghbnum];
          mintedgenbr ++;
        }
      }
      
      mintventtax[mintnum] = venttax[num];
      mintverttax[mintnum+1] = mintvendtax[mintnum];
      mintnum++;
      
      if (venttax[num] == dataptr->tetrent) {
        tetrnum = corrtax[num];
        minttenum = mintvecotax[mintcorrtax[num]];
        
      }
      if (venttax[num] == dataptr->faceent) {
        facenum = corrtax[num];
        mintfcnum = mintvecotax[mintcorrtax[num]];
      }
    }
    
    if (flagtax[num] == PAMPA_TAG_VERT_EXTERNAL) {
      mbndvendtax[mbndnum] = mbndverttax[mbndnum];
      
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        mbndedgetax[mbndvendtax[mbndnum]++] = mbndcorrtax[nghbnum];
        mbndedgenbr ++;
      }
      
      mbndventtax[mbndnum] = venttax[num];
      mbndverttax[mbndnum+1] = mbndvendtax[mbndnum];
      mbndnum++;
      
      if (venttax[num] == dataptr->tetrent) {
        tetrnum = corrtax[num];
        mbndtenum = mbndvecotax[mbndcorrtax[num]];
      }
      if (venttax[num] == dataptr->faceent) {
        facenum = corrtax[num];
        mbndfcnum = mbndvecotax[mbndcorrtax[num]];
      }
    }
    
    if (flagtax[num] == PAMPA_TAG_VERT_FRONTIER && venttax[num] == dataptr->tetrent) {
      mbndvendtax[mbndnum] = mbndverttax[mbndnum];
      
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        if (flagtax[nghbnum] != PAMPA_TAG_VERT_INTERNAL) {
          mbndedgetax[mbndvendtax[mbndnum]++] = mbndcorrtax[nghbnum];
          mbndedgenbr ++;
        }
      }
      
      mbndventtax[mbndnum] = venttax[num];
      mbndverttax[mbndnum+1] = mbndvendtax[mbndnum];
      mbndnum++;
      
      if (venttax[num] == dataptr->tetrent) {
        tetrnum = corrtax[num];
        mbndtenum = mbndvecotax[mbndcorrtax[num]];
      }
    }
    
    if (flagtax[num] == PAMPA_TAG_VERT_FRONTIER && venttax[num] != dataptr->tetrent) {
      mintvendtax[mintnum] = mintverttax[mintnum];
      mbndvendtax[mbndnum] = mbndverttax[mbndnum];
      
      for (nghbid = verttax[num], end = vendtax[num] ; nghbid < end ; nghbid++) {
        nghbnum = edgetax[nghbid];
        if (flagtax[nghbnum] == PAMPA_TAG_VERT_INTERNAL ||
            (flagtax[nghbnum] == PAMPA_TAG_VERT_FRONTIER && venttax[nghbnum] != dataptr->tetrent)) {
          mintedgetax[mintvendtax[mintnum]++] = mintcorrtax[nghbnum];
          mintedgenbr ++;
        }
        if (flagtax[nghbnum] == PAMPA_TAG_VERT_EXTERNAL || flagtax[nghbnum] ==PAMPA_TAG_VERT_FRONTIER) {
          mbndedgetax[mbndvendtax[mbndnum]++] = mbndcorrtax[nghbnum];
          mbndedgenbr ++;
        }
      }
      mintventtax[mintnum] = venttax[num];
      mbndventtax[mbndnum] = venttax[num];
      mintverttax[mintnum+1] = mintvendtax[mintnum];
      mbndverttax[mbndnum+1] = mbndvendtax[mbndnum];
      mintnum++;
      mbndnum++;
      
      
      if (venttax[num] == dataptr->faceent) {
        facenum = corrtax[num];
        mintfcnum = mintvecotax[mintcorrtax[num]];
        mbndfcnum = mbndvecotax[mbndcorrtax[num]];
      }
    }
    
    /** BEGIN: Fill geomtax and soltax arrays **/
    if (venttax[num] == dataptr->nodeent) {
      switch (flagtax[num]) {
        case PAMPA_TAG_VERT_INTERNAL:
          nodenum = corrtax[num];
          mintndnum = mintvecotax[mintcorrtax[num]];

          mintgeomtax[mintndnum*3] = geomtax[nodenum*3];
          mintgeomtax[mintndnum*3+1] = geomtax[nodenum*3+1];
          mintgeomtax[mintndnum*3+2] = geomtax[nodenum*3+2];
          break;
        case PAMPA_TAG_VERT_EXTERNAL:
          nodenum = corrtax[num];
          mbndndnum = mbndvecotax[mbndcorrtax[num]];

          mbndgeomtax[mbndndnum*3] = geomtax[nodenum*3];
          mbndgeomtax[mbndndnum*3+1] = geomtax[nodenum*3+1];
          mbndgeomtax[mbndndnum*3+2] = geomtax[nodenum*3+2];
          break;
        case PAMPA_TAG_VERT_FRONTIER:
          nodenum = corrtax[num];
          mintndnum = mintvecotax[mintcorrtax[num]];
          mbndndnum = mbndvecotax[mbndcorrtax[num]];

          mintgeomtax[mintndnum*3] = geomtax[nodenum*3];
          mintgeomtax[mintndnum*3+1] = geomtax[nodenum*3+1];
          mintgeomtax[mintndnum*3+2] = geomtax[nodenum*3+2];

          mbndgeomtax[mbndndnum*3] = geomtax[nodenum*3];
          mbndgeomtax[mbndndnum*3+1] = geomtax[nodenum*3+1];
          mbndgeomtax[mbndndnum*3+2] = geomtax[nodenum*3+2];
          break;
        default:
          errorPrint ("internal error");
          return     (1);
      }
    }
    /** END: Fill geomtax and soltax arrays **/
  }
  /***
   * END: add edge in each sub-meshes
   */
  
#ifdef PAMPA_DEBUG_ADAPT
    {
      if ((mbndedgenbr > mbndedgesiz) || (mintedgenbr > mintedgesiz)) {
        errorPrint ("internal error");
        return     (1);
      }
    }
#endif /* PAMPA_DEBUG_ADAPT */
  
  /***
   * BEGIN: build internal mesh
   */
  PAMPA_meshInit(mintptr);
  CHECK_FDBG2 (PAMPA_meshBuild( mintptr, baseval, mintvertnbr, mintverttax + baseval, mintvendtax + baseval, NULL, // XXX prévoir vlbltab comme dans meshCreate ;-)
      mintedgenbr,
      mintedgesiz, mintedgetax + baseval, NULL, mintenttnbr, mintventtax + baseval, NULL, NULL, 50)); // XXX pourquoi 50 comme valeurs max ??
  
  mintgeomtax += 3 * baseval;
    chekval = PAMPA_meshValueLink (mintptr, (void **) &mintgeomtax, PAMPA_VALUE_ALLOCATED,
              dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM); // XXX baseval + 1 à changer
  if (chekval != 0)
    return chekval;
  /***
   * END: build internal mesh
   */
  
  /***
   * BEGIN: build boundarie mesh
   */
  PAMPA_meshInit(mbndptr);
  CHECK_FDBG2 (PAMPA_meshBuild( mbndptr, baseval, mbndvertnbr, mbndverttax + baseval, mbndvendtax + baseval, NULL, // XXX ajouter vlbltab
        mbndedgenbr,
      mbndedgesiz, mbndedgetax + baseval, NULL, mbndenttnbr, mbndventtax + baseval, NULL, NULL, 50)); // XXX pourquoi 50 comme valeurs max ??
  
  mbndgeomtax += 3 * baseval;
  chekval = PAMPA_meshValueLink (mbndptr, (void **) &mbndgeomtax, PAMPA_VALUE_ALLOCATED,
              dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM); // XXX baseval + 1 à changer
  if (chekval != 0)
    return chekval;
  
  mbndndfgtax += baseval;
  chekval = PAMPA_meshValueLink (mbndptr, (void **) &mbndndfgtax, PAMPA_VALUE_ALLOCATED,
              PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_FRONT);
  /***
   * END: build boundarie mesh
   */
  
  /***
   * BEGIN: Associate values
   */
  // XXX Gestion des sous-entitées
  void * mintax;
  void * minttax;
  void * mbndtax;
  int i, j;
  PAMPA_Num tag;
  MPI_Datatype type;
  PAMPA_AvIterator vaiter;
  MPI_Aint            valusiz;                 /* Extent of attribute datatype */
  MPI_Aint            dummy;
  for (i = 0 ; i < enttnbr ; i++) {
    PAMPA_meshAvItInit (pminptr, i, &vaiter);
    
    while (PAMPA_avItHasMore(&vaiter)) {
      
      tag = PAMPA_avItCurTagNum (&vaiter);
      type = PAMPA_avItCurTypeVal (&vaiter);
      
      if (tag == PAMPA_TAG_GEOM) {
        PAMPA_avItNext(&vaiter);
        continue;
      }
      
      CHECK_FDBG2 (PAMPA_meshValueLink (mbndptr, (void**) &mbndtax, PAMPA_VALUE_PUBLIC, type, i, tag));
      CHECK_FDBG2 (PAMPA_meshValueLink (mintptr, (void**) &minttax, PAMPA_VALUE_PUBLIC, type, i, tag));
      MPI_Type_get_extent (type, &dummy, &valusiz);     /* Get type extent */
      
      mintax = PAMPA_avItCurValue (&vaiter);
      
      // XXX parcours numérotation des entités
      for (j = baseval ; j < vertnbr + baseval ; j++) {
        if (venttax[j] == i) {
          if (flagtax[j] == PAMPA_TAG_VERT_INTERNAL) {
            memCpy ((byte *) minttax + mintvecotax[mintcorrtax[j]] * valusiz, (byte *) mintax + corrtax[j] * valusiz, valusiz * sizeof (byte));
          }
          else if (flagtax[j] == PAMPA_TAG_VERT_EXTERNAL || (flagtax[j] == PAMPA_TAG_VERT_FRONTIER && venttax[j] == dataptr->tetrent)) {
            memCpy ((byte *) mbndtax + mbndvecotax[mbndcorrtax[j]] * valusiz, (byte *) mintax + corrtax[j] * valusiz, valusiz * sizeof (byte));
          }
          else if (flagtax[j] == PAMPA_TAG_VERT_FRONTIER && venttax[j] != dataptr->tetrent) {
            memCpy ((byte *) minttax + mintvecotax[mintcorrtax[j]] * valusiz, (byte *) mintax + corrtax[j] * valusiz, valusiz * sizeof (byte));
            memCpy ((byte *) mbndtax + mbndvecotax[mbndcorrtax[j]] * valusiz, (byte *) mintax + corrtax[j] * valusiz, valusiz * sizeof (byte));
          }
        }
      }
      
      PAMPA_avItNext(&vaiter);
    }
  }
  PAMPA_Num * restrict minoflgtax;
  PAMPA_Num * restrict mintoflgtax;
  PAMPA_Num * restrict mbndoflgtax;
  CHECK_FDBG2 (PAMPA_meshValueData (pminptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &minoflgtax));
  CHECK_FDBG2 (PAMPA_meshValueLink (mbndptr, (void**) &mbndoflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS));
  CHECK_FDBG2 (PAMPA_meshValueLink (mintptr, (void**) &mintoflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS));
  
  for (j = baseval ; j < vertnbr + baseval ; j++) {
    if (flagtax[j] == PAMPA_TAG_VERT_INTERNAL) {
      mintoflgtax[mintcorrtax[j]] = minoflgtax[j];
    }
    else if (flagtax[j] == PAMPA_TAG_VERT_EXTERNAL || (flagtax[j] == PAMPA_TAG_VERT_FRONTIER && venttax[j] == dataptr->tetrent)) {
      mbndoflgtax[mbndcorrtax[j]] = minoflgtax[j];
    }
    else if (flagtax[j] == PAMPA_TAG_VERT_FRONTIER && venttax[j] != dataptr->tetrent) {
      mintoflgtax[mintcorrtax[j]] = minoflgtax[j];
      mbndoflgtax[mbndcorrtax[j]] = minoflgtax[j];
    }
  }
  /***
   * BEGIN: Associate values
   */
  
#ifdef PAMPA_DEBUG_MESH
  CHECK_FDBG2 (PAMPA_meshCheck2 (mintptr, PAMPA_CHECK_ALONE));
  CHECK_FDBG2 (PAMPA_meshCheck2 (mbndptr, PAMPA_CHECK_ALONE));
#endif /* PAMPA_DEBUG_MESH */
  
  PAMPA_smeshExit(&smshdat);
  
  memFreeGroup (mintventtax + baseval);
  memFreeGroup (corrtax + baseval);
  
  memFree(flagtax);
  
  return 0;
}
