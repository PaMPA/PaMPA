/*  Copyright 2014-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_adapt.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 25 Jan 2014
//!                             to:   22 Sep 2017
//!
/************************************************************/

// TODO: common code between MMG3D4 and MMG3D5, maybe the code could be
// factorized
#define MESHADAPTHASHPRIME     17                 //!< Prime number for hashing

typedef struct MeshAdaptElem_                     //!< Data structure to store tetrahedra
{
  PAMPA_Num    nodesum;                           //!< Sum of node numbers
  PAMPA_Num    nodetab[4];                        //!< Node numbers which composed the tetrahedra
  PAMPA_Num    vertnum;                           //!< Tetrahedra number
} MeshAdaptElem;                                  //!< Data structure to store tetrahedra

typedef struct MeshAdaptFace_                     //!< Data structure to store faces
{
  PAMPA_Num    nodesum;                           //!< Sum of node numbers
  PAMPA_Num    nodetab[3];                        //!< Node numbers which composed the faces
  PAMPA_Num    vertnum;                           //!< faces number
} MeshAdaptFace;                                  //!< Data structure to store faces

static
int
meshAdaptElemResize (
	MeshAdaptElem * restrict * tetrhashtabptr,
	PAMPA_Num * restrict const           tetrhashsizptr,
	PAMPA_Num * restrict const           tetrhashmaxptr,
	PAMPA_Num * restrict const           tetrhashmskptr);

static
int
meshAdaptFaceResize (
	MeshAdaptFace * restrict * facehashtabptr,
	PAMPA_Num * restrict const           facehashsizptr,
	PAMPA_Num * restrict const           facehashmaxptr,
	PAMPA_Num * restrict const           facehashmskptr);
