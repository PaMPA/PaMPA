/*  Copyright 2014-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_load.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 25 Jan 2014
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include "common.h"
#include "module.h"
#include <libmmg3d4.h>
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d4.h"
#include "rmesh_to_pmesh.h"

//! \brief This routine calls the remesher to read the mesh and convert remesher
//! mesh to PaMPA mesh
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D4_meshLoad (
PAMPA_Mesh	* const			meshptr,              //!< PaMPA centralized in mesh
char		* const         fileval,             //!< Filename value
PAMPA_MMG3D4_Data   * const         dataptr,
PAMPA_Num const             flagval)
{
  MMG_Mesh    rmshdat;
  MMG_Sol    msoldat;
  PAMPA_Num baseval;
  PAMPA_Num basedif;
  PAMPA_Num nodenbr;
  PAMPA_Num * data;
  PAMPA_Iterator it;
  double * soltax;

  memSet (&rmshdat, 0, sizeof (MMG_Mesh));
  rmshdat.info.memory = -1;
  MMG_loadMesh (&rmshdat, fileval);
  MMG_inputdata(&rmshdat, NULL);

  if ((flagval & PAMPA_MMG3D4_FORTRAN) != 0)
	baseval = 1;
  else
    baseval = 0;
  basedif = 1 - baseval;
  rmesh2pmesh (&rmshdat, &msoldat, dataptr, baseval, flagval, meshptr);
  if ((flagval & PAMPA_MMG3D4_FORTRAN) == 0) { // FIXME test barbare pour ne pas charger la sol avec le proto du laplacien
    MMG_solinit(&msoldat, &rmshdat, 0, 0);
    MMG_loadSol (&msoldat, fileval, rmshdat.npmax);
    //renumbering(500, &rmshdat, &msoldat);

    if ( !MMG_hashTetra(&rmshdat) )    return(1);


    switch (msoldat.offset) {
      MPI_Datatype  metrtyp;
      case 1 :
        PAMPA_meshValueLink (meshptr, (void **) &soltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, dataptr->nodeent, PAMPA_TAG_SOL_3DI);
        break;
      case 6 :
        MPI_Type_contiguous(6, MPI_DOUBLE, &metrtyp);
        MPI_Type_commit(&metrtyp);
        PAMPA_meshValueLink (meshptr, (void **) &soltax, PAMPA_VALUE_PUBLIC, metrtyp, dataptr->nodeent, PAMPA_TAG_SOL_3DAI);
        break;
      default :
        errorPrint ("unrecognized metric");
        return (1);
    }

    soltax -= msoldat.offset * baseval;

    PAMPA_meshEnttSize (meshptr, dataptr->nodeent, &nodenbr);
    PAMPA_meshItInitStart(meshptr, dataptr->nodeent, &it);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num nodenum;

      nodenum = PAMPA_itCurEnttVertNum(&it);

      //soltax[nodenum] = msoldat.met[nodenum + basedif];
      memCpy (soltax + nodenum * msoldat.offset, msoldat.met + (nodenum + basedif - 1) * msoldat.offset + 1, msoldat.offset * sizeof (double));
      PAMPA_itNext(&it);
    }
    MMG_solexit (&msoldat);
  }
  MMG_meshexit (&rmshdat);
  return (0);
}
