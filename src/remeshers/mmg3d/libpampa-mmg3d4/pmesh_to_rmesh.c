/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pmesh_to_rmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

//! \brief This routine initializes the
//! mesh structure of the mmg3d4 remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

#include <stdio.h>
#include <mpi.h>
#include <libmmg3d4.h>
#include "common.h"
#include "module.h"
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d4.h"
#include "types.h"

#define M_REQUIRED (1 << 5) // FIXME à supprimer une fois qu'il sera visible de MMG
#define M_BDRY     (1 << 1) 
int
pmesh2rmesh (
PAMPA_Mesh  * const         pmshptr,              //!< PaMPA centralized mesh
PAMPA_MMG3D4_Data  * const         dataptr,              //!< XXX
PAMPA_Num     const         flagval,              //!< XXX
MMG_pMesh     const         mmshptr,              //!< MMG3D4 mesh
MMG_pSol      const         msolptr)              //!< MMG3D4 solution
{

  PAMPA_Num      baseval;
  PAMPA_Num      enttnbr;
  PAMPA_Num      nodenbr;
  PAMPA_Num      tetrnbr;
  PAMPA_Num      facenbr;
  PAMPA_Num      nodenum;
  PAMPA_Num      nodennd;
  PAMPA_Num      tetrnum;
  PAMPA_Num      basedif;
  PAMPA_Num    * restrict vreftax;
  PAMPA_Num    * restrict ereftax;
  PAMPA_Num    * restrict freftax;
  double       * restrict geomtax;
  double       * restrict soltax;
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it;
  int chekval;

  mmshptr->np = mmshptr->nt = mmshptr->ne = mmshptr->ncor = 0; 
  mmshptr->ver = 2;
  // XXX on suppose que les nœuds du bord de la bulle soient numérotés en
  // premier pour que nous puissions les renuméroter d'abord lors du passage
  // MMG3D4 à PaMPA
  // XXX la remarque ci-dessus est fausse, du coup il faut modifier le code
  // FIXME
  PAMPA_meshData(pmshptr, &baseval, &enttnbr, NULL, NULL, NULL, NULL, NULL);

  PAMPA_meshValueData(pmshptr, dataptr->tetrent, PAMPA_TAG_REF, (void **) &ereftax);
  ereftax -= baseval;

  if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
    PAMPA_meshValueData(pmshptr, dataptr->faceent, PAMPA_TAG_REF, (void **) &freftax);
    freftax -= baseval;
  }

  PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &geomtax);
  geomtax -= 3 * baseval;

  if (msolptr != NULL) {
    chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
    if (chekval == 0)  /* If there is a value with this tag */
      msolptr->offset = 1;
    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (chekval);
    }
    else { /* If there no value with this tag */
      chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
      if (chekval == 0)  /* If there is a value with this tag */
        msolptr->offset = 6;
      else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
        return (chekval);
      }
      else { /* If there no value with this tag */
        errorPrint ("No associated metric");
        return (chekval);
      }
    }
    soltax -= msolptr->offset * baseval;
  }

  if ((flagval & PAMPA_MMG3D4_ADAPT) != 0) {
  	PAMPA_meshValueData(pmshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &vreftax);
  	vreftax -= baseval;
  }
  PAMPA_meshEnttSize(pmshptr, dataptr->nodeent, &nodenbr);
  PAMPA_meshEnttSize(pmshptr, dataptr->tetrent, &tetrnbr);
  if ((flagval & PAMPA_MMG3D4_FACES) != 0) 
    PAMPA_meshEnttSize(pmshptr, dataptr->faceent, &facenbr);
  else
    facenbr = 0;


  MMG_meshinit(mmshptr, (MMG3D4_int) nodenbr, (MMG3D4_int) tetrnbr, (MMG3D4_int) facenbr); 
  mmshptr->npfixe = mmshptr->np;
  mmshptr->nefixe = mmshptr->ne;
  mmshptr->ntfixe = mmshptr->nt;


  basedif = 1 - baseval;
  for(nodenum = baseval, nodennd = baseval + mmshptr->np ; nodenum < nodennd ; nodenum ++) {

    memCpy (mmshptr->point + ((MMG3D4_int) (nodenum + basedif)), geomtax + nodenum * 3, 3 * sizeof (double)); // XXX on ne pointe pas sur c dans point, pas bien
  }

  PAMPA_meshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  PAMPA_meshItInitStart(pmshptr, dataptr->tetrent, &it);
  while (PAMPA_itHasMore(&it)) {
    MMG3D4_int i;
    PAMPA_Num mtetnum;

    i = 0;
    tetrnum = PAMPA_itCurEnttVertNum(&it);
    mtetnum = PAMPA_itCurMeshVertNum(&it);

    if (((flagval & PAMPA_MMG3D4_ADAPT) != 0) && (vreftax[mtetnum] != PAMPA_TAG_VERT_INTERNAL)) /* if at the frontier */
      mmshptr->tetra[(MMG3D4_int) (tetrnum + basedif)].tag |= M_REQUIRED;
    mmshptr->tetra[(MMG3D4_int) (tetrnum + basedif)].ref = ereftax[tetrnum];
    PAMPA_itStart(&it_nghb, tetrnum);

    while (PAMPA_itHasMore(&it_nghb)) {
	  PAMPA_Num mnodnum;

      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
	  mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
	  if (((flagval & PAMPA_MMG3D4_ADAPT) != 0) && (vreftax[mnodnum] != PAMPA_TAG_VERT_INTERNAL)) { /* if at the frontier */
		mmshptr->point[(MMG3D4_int) (nodenum + basedif)].tag |= M_BDRY;
		mmshptr->point[(MMG3D4_int) (nodenum + basedif)].ref = mnodnum + 1; /* TRICK: + 1 to have 0 as default value */
	  }
	  else
		mmshptr->point[(MMG3D4_int) (nodenum + basedif)].ref = 0;
      mmshptr->tetra[(MMG3D4_int) (tetrnum + basedif)].v[i ++] = (MMG3D4_int) (nodenum + basedif);
      PAMPA_itNext(&it_nghb);
    }
    PAMPA_itNext(&it);
#ifdef PAMPA_DEBUG_ADAPT
    if (i != 4) {
      errorPrint ("Element %d doesn't have 4 nodes", mtetnum);
      return (1);
    }
#endif /* PAMPA_DEBUG_ADAPT */
  }

  if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
    PAMPA_meshItInit(pmshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
    PAMPA_meshItInitStart(pmshptr, dataptr->faceent, &it);
    while (PAMPA_itHasMore(&it)) {
      MMG3D4_int i;
      PAMPA_Num mtrinum;
      PAMPA_Num trianum;

      i = 0;
      trianum = PAMPA_itCurEnttVertNum(&it);
      mtrinum = PAMPA_itCurMeshVertNum(&it);

      mmshptr->tria[(MMG3D4_int) (trianum + basedif)].ref = freftax[trianum] + 11;
      PAMPA_itStart(&it_nghb, trianum);

      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num mnodnum;

        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
        mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
        mmshptr->tria[(MMG3D4_int) (trianum + basedif)].v[i ++] = (MMG3D4_int) (nodenum + basedif);
        PAMPA_itNext(&it_nghb);
      }
      PAMPA_itNext(&it);
#ifdef PAMPA_DEBUG_ADAPT
      if (i != 3) {
        errorPrint ("Triangle %d doesn't have 3 nodes", mtrinum);
        return (1);
      }
#endif /* PAMPA_DEBUG_ADAPT */
    }
  }

  if (msolptr != NULL) {
    MMG_solinit(msolptr, mmshptr, msolptr->offset, 1);
    for(nodenum = baseval, nodennd = baseval + mmshptr->np ; nodenum < nodennd ; nodenum ++) {
      memCpy (msolptr->met + (nodenum + basedif - 1) * msolptr->offset + 1, soltax + nodenum * msolptr->offset, msolptr->offset * sizeof (double));
    }
  }

  // XXX peut-être appeler MMG_hashTetra pour construire adja
  chekval = MMG_hashTetra(mmshptr);
  if (chekval == 0) {
    errorPrint ("MMG3D4_finalize: MMG_hashTetra error");
    return chekval;
  }



  return (0);
}
