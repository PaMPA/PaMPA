/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_metric_smooth.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 May 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#include "module.h"
#include "common.h"
#include "comm.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d4.h>
#include "pampa-mmg3d4.h"
#include "types.h"
#include <math.h>

// FIXME pour débogue
extern unsigned char MMG_iare[6][2];

//! \brief XXX
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
meshMetricSmooth (
PAMPA_Mesh * const      pmshptr,
PAMPA_MMG3D4_Data * const dataptr)
{
//  PAMPA_Iterator it_t;
//  PAMPA_Iterator it_t_n;
//  PAMPA_Iterator it_n_t;
//  PAMPA_Num nodeidx;
//  PAMPA_Num edgenum;
//  PAMPA_Num nodetab[4];
//  double eflgtax;
//  PAMPA_Num * restrict vflgtax;
//  double * restrict tflgtax;
//  PAMPA_Num * restrict tlsttab;
//  double * restrict coortax;
//  double * restrict soltax;
//  double * restrict edletax; // edge length array for each node
//  PAMPA_Num * restrict ednbtax; // edge number array for each node (counter of edges for each node)
//  PAMPA_Num baseval;
//  PAMPA_Num tlstnbr;
//  PAMPA_Num tlstidx;
//  PAMPA_Num tetrnbr;
//  PAMPA_Num tetrnum;
//  PAMPA_Num tetrnnd;
//  PAMPA_Num nodenbr;
//  PAMPA_Num nodenum;
//  PAMPA_Num nodennd;
//  MMG3D4_int offset;
//  int chekval;
//
//  chekval = 0;
//
//  // algo pour lisser la métrique sur le maillage
//  // - eflgtax tableau de taille |elem| et initialisé à ~0 contenant soit ~0, soit
//  //   le rapport entre 0 et 1 de la taille actuelle de la métrique à garder
//  // - nflgtax tableau de taille |noeud| et initialisé à ~0 contenant soit ~0, soit 1 si le noeud a déjà été traité
//  // - tlsttab tableau de taille |elem| et contenant une liste d'éléments
//  //
//  // - on parcourt chaque élément « e »
//  //   - si e est contraint, on l'ajoute à tlsttab
//
//  PAMPA_meshData (pmshptr, &baseval, NULL, NULL, NULL, NULL, NULL, NULL);
//
//  CHECK_FDBG2 (PAMPA_meshValueData (pmshptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &coortax));
//  coortax -= 3 * baseval;
//
//  PAMPA_meshEnttSize (pmshptr, dataptr->tetrent, &tetrnbr);
//  PAMPA_meshEnttSize (pmshptr, dataptr->nodeent, &nodenbr);
//
//  if (memAllocGroup ((void **) (void *)
//        &tlsttab, (size_t) (tetrnbr * sizeof (PAMPA_Num)),
//        &tflgtax, (size_t) (tetrnbr * sizeof (double)),
//        &edletax, (size_t) (nodenbr * sizeof (double)),
//        &ednbtax, (size_t) (nodenbr * sizeof (PAMPA_Num)),
//        NULL) == NULL) {
//    errorPrint("out of memory");
//    return (1);
//  }
//  tflgtax -= baseval;
//  edletax -= baseval;
//  ednbtax -= baseval;
//  
//  for (tetrnum = baseval, tetrnnd = baseval + tetrnbr; tetrnum < tetrnnd; tetrnum ++)
//    tflgtax[tetrnum] = -1.0;
//  memSet (edletax + baseval, 0, nodenbr * sizeof(double));
//  memSet (ednbtax + baseval, 0, nodenbr * sizeof(PAMPA_Num));
//  CHECK_FDBG2 (PAMPA_meshValueData (pmshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &vflgtax));
//  vflgtax -= baseval;
//
//  //memSet (oreftax + baseval, 0, nodenbr * sizeof (PAMPA_Num));
//  tlstnbr =
//    tlstidx = 0;
//  PAMPA_meshItInitStart(pmshptr, dataptr->tetrent, &it_t);
//  while (PAMPA_itHasMore(&it_t)) {
//    PAMPA_Num mtetnum;
//    PAMPA_Num tetrnum;
//    PAMPA_Num nghbnbr;
//
//    mtetnum = PAMPA_itCurMeshVertNum(&it_t);
//    tetrnum = PAMPA_itCurEnttVertNum(&it_t);
//
//    // XXX test actuel
//    CHECK_FDBG2 (PAMPA_meshVertData (pmshptr, tetrnum, baseval, baseval, &nghbnbr, NULL));
//    if (nghbnbr == 3) {
//      tlsttab[tlstnbr ++] = tetrnum;
//      tflgtax[tetrnum] = 1.0;
//    }
//    // XXX fin test actuel
//    
//    //// XXX futur test
//    //if (vflgtax[mtetnum] != PAMPA_TAG_VERT_INTERNAL) {
//    //  tlsttab[tlstnbr ++] = tetrnum;
//    //  tflgtax[tetrnum] = 1;
//    //}
//    //// XXX fin futur test
//    PAMPA_itNext(&it_t);
//  }
//
//  chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
//  if (chekval == 0)  /* If there is a value with this tag */
//    offset = 1;
//  else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
//    return (chekval);
//  }
//  else { /* If there no value with this tag */
//    errorPrint ("Not yet implemented"); // XXX due to metrval which is one double, instead of ???
//    return (1);
//
//    chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
//    if (chekval == 0)  /* If there is a value with this tag */
//      offset = 6;
//    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
//      return (chekval);
//    }
//    else { /* If there no value with this tag */
//      errorPrint ("No associated metric");
//      return (chekval);
//    }
//  }
//  soltax -= offset * baseval;
//
//  //
//  // - tant qu'il y a des éléments dans tlsttab
//  PAMPA_meshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_t_n);
//  PAMPA_meshItInit(pmshptr, dataptr->nodeent, dataptr->tetrent, &it_n_t);
//  while (tlstidx < tlstnbr) {
//    PAMPA_Num tetrnum;
//    double tflgval;
//    
//    tetrnum = tlsttab[tlstidx ++];
//    tflgval = tflgtax[tetrnum];
//
//    nodeidx = 0;
//    PAMPA_itStart(&it_t_n, tetrnum);
//    while (PAMPA_itHasMore(&it_t_n)) {
//      PAMPA_Num nodenum;
//
//      nodenum = PAMPA_itCurEnttVertNum(&it_t_n);
//      nodetab[nodeidx ++] = nodenum;
//      PAMPA_itNext(&it_t_n);
//    }
//
//    for (edgenum = 0; edgenum < 6; edgenum ++) {
//      PAMPA_Num nodetb2[2];
//      MMG3D4_int p1;
//      MMG3D4_int p2;
//      double lenval;
//
//      nodetb2[0] = nodetab[MMG_iare[edgenum][0]];
//      nodetb2[1] = nodetab[MMG_iare[edgenum][1]];
//      lenval = sqrt (
//          ((coortax[3 * nodetb2[0]] - coortax[3 * nodetb2[1]])
//           * (coortax[3 * nodetb2[0]] - coortax[3 * nodetb2[1]]))
//          + ((coortax[3 * nodetb2[0] + 1] - coortax[3 * nodetb2[1] + 1])
//            * (coortax[3 * nodetb2[0] + 1] - coortax[3 * nodetb2[1] + 1]))
//          + ((coortax[3 * nodetb2[0] + 2] - coortax[3 * nodetb2[1] + 2])
//            * (coortax[3 * nodetb2[0] + 2] - coortax[3 * nodetb2[1] + 2])));
//
//      //printf ("metval: %lf\n", lenval);
//
//      for (int i = 0; i < 2; i++) {
//
//        //printf ("rapport: %lf\n", (lenval / soltax[nodetb2[i]]));
//        if (((lenval / soltax[nodetb2[i]]) > 5)) { // XXX  && (tflgval != 0)) {
//          nodenum = nodetb2[i];
//
//          if (edletax[nodenum] == 0) {
//            edletax[nodenum] += soltax[nodenum] * tflgval + lenval * (1 - tflgval);
//            ednbtax[nodenum] ++;
//
//            PAMPA_itStart(&it_n_t, nodenum);
//            while (PAMPA_itHasMore(&it_n_t)) {
//              PAMPA_Num tetrnum;
//
//              tetrnum = PAMPA_itCurEnttVertNum(&it_n_t);
//              if (tflgtax[tetrnum] < 0) {
//                tlsttab[tlstnbr ++] = tetrnum;
//                tflgtax[tetrnum] = MAX (0, tflgval - 0.7); // XXX tflgval - 0.5;
//              }
//              PAMPA_itNext(&it_n_t);
//            }
//          }
//        }
//      }
//      //printf ("tetrnbr: " GNUMSTRING ", tlstnbr: " GNUMSTRING "\n", tetrnbr, tlstnbr);
//
//
//      //if (offset == 1)
//      //  solval = MMG_long_iso (&coortax[3*p1], &coortax[3*p2], &soltax[p1], &soltax[p2]);
//      //else
//      //  solval = MMG_long_ani (&coortax[3*p1], &coortax[3*p2], &soltax[p1 * offset], &soltax[p2 * offset]);
//      
//    } 
//  }
//
//  for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++) 
//    if (edletax[nodenum] != 0)
//      edletax[nodenum] /= ednbtax[nodenum];
//    else
//      edletax[nodenum] = soltax[nodenum];
//
//  memCpy (soltax + baseval, edletax + baseval, nodenbr * sizeof (double));
  return (0);
}

