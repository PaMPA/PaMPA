/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        rmesh_to_pmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d4.h"
#include "types.h"
#include <libmmg3d4.h>
#include "rmesh_to_pmesh.h"

extern unsigned char MMG_idir[4][3];


//! \brief This routine initializes the
//! mesh structure of the mmg3d4 remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
rmesh2pmesh (
MMG_pMesh     const         mmshptr,              //!< MMG3D4 mesh
MMG_pSol      const         msolptr,              //!< MMG3D4 solution
PAMPA_MMG3D4_Data  * const         dataptr,              //!< XXX
PAMPA_Num     const         baseval,              //!< PaMPA baseval
PAMPA_Num     const         flagval,              //!< If adaptation
PAMPA_Mesh  * const         pmshptr)              //!< PaMPA centralized mesh
{
  PAMPA_Num  basedif;
  PAMPA_Num  nodebas;
  PAMPA_Num  tetrbas;
  PAMPA_Num  facebas;
  PAMPA_Num  enttnbr;
  PAMPA_Num  mfacidx;
  PAMPA_Num  mfacnum;
  PAMPA_Num  mfacnm2;
  PAMPA_Num  mfacnnd;
  PAMPA_Num  vertnum;
  PAMPA_Num  vertnnd;
  PAMPA_Num  vertnbr;
  PAMPA_Num  edgenbr;
  PAMPA_Num  edgesiz;
  PAMPA_Num  facenbr;
  PAMPA_Num * restrict nnbrtax;
  PAMPA_Num * restrict verttax;
  PAMPA_Num * restrict vendtax;
  PAMPA_Num * restrict venttax;
  PAMPA_Num * restrict edgetax;
  PAMPA_Num * restrict ereftax;
  PAMPA_Num * restrict nreftax;
  PAMPA_Num * restrict freftax;
  PAMPA_Num * restrict vflgtax;
  double *    restrict   geomtax;
  faces *     restrict facetab;
  MMG3D4_int    nodeind;
  MMG3D4_int    nodennd;
  MMG3D4_int    mtetnum;
  MMG3D4_int    mtetnnd;
  int    chekval;

  basedif = baseval - 1;

  if ((flagval & PAMPA_MMG3D4_ADAPT) == 0) {
    for (nodeind = 1, nodennd = mmshptr->np + 1 ; nodeind < nodennd ; nodeind ++) {
      mmshptr->point[nodeind].tag = ~M_UNUSED;
    }

    // XXX peut-être appeler MMG_hashTetra pour construire adja
    chekval = MMG_hashTetra(mmshptr);
    if (chekval == 0) {
      errorPrint ("MMG3D4_finalize: MMG_hashTetra error");
      return chekval;
    }

    //chekval = MMG_markBdry(mmshptr);
    //if (chekval == 0) {
    //  errorPrint ("MMG3D4_finalize: MMG_hashTetra error");
    //  return chekval;
    //}
  }

  //  for (nodeind = 1, nodennd = mmshptr->np + 1 ; nodeind < nodennd ; nodeind ++) 
  // if ((mmshptr->point[nodeind].tag & M_BDRY) != 0)
  //   printf ("nœud sur l'enveloppe : %d\n", nodeind);

  //return(-1);
  //edgesiz = 2 * mmshptr->np + 4 * mmshptr->ne;
  edgenbr = baseval;

  tetrbas = basedif;
  facebas = tetrbas + (PAMPA_Num) mmshptr->ne;
  if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
    for (facenbr = mfacnum = 1, mfacnnd = (PAMPA_Num) (mmshptr->nt + 1); mfacnum < mfacnnd; mfacnum ++) 
      //if ((mmshptr->tria[mfacnum].ref != 0) && (mmshptr->tria[mfacnum].ref != 10)) { /* 10 c'est temporaire */
    if (((flagval & PAMPA_MMG3D4_ADAPT) == 0) || (mmshptr->tria[mfacnum].ref > 10)) {
        facenbr ++;
      }
    facenbr --;

    vertnbr = (PAMPA_Num) (mmshptr->np + mmshptr->ne + facenbr);
    nodebas = facebas + (PAMPA_Num) facenbr;
    enttnbr = 3;
  }
  else {
    vertnbr = (PAMPA_Num) (mmshptr->np + mmshptr->ne);
    nodebas = tetrbas + (PAMPA_Num) mmshptr->ne;
    enttnbr = 2;
  }

  if (memAllocGroup ((void **) (void *)
        &venttax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &verttax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &vendtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        &nnbrtax, (size_t) (vertnbr       * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("MMG3D4_finalize: out of memory (1)");
    return (1);
  }
  venttax -= baseval;
  verttax -= baseval;
  vendtax -= baseval;
  nnbrtax -= baseval;

  //memSet (venttax + baseval, PAMPA_ENTT_DUMMY, vertnbr * sizeof (PAMPA_Num));
  memSet (nnbrtax + baseval, 0, vertnbr * sizeof (PAMPA_Num));
  for (mtetnum = 1, mtetnnd = mmshptr->ne + 1 ; mtetnum < mtetnnd ; mtetnum ++) {
    PAMPA_Num ptetnum;
    MMG3D4_int   nodeind;

    // if (!mmshptr->tetra[mtetnum].v[0]) continue; /* If the tetra doesn't exist */

    ptetnum = (PAMPA_Num) mtetnum + tetrbas;
    nnbrtax[ptetnum] += 8; /* 4 for nodes and 4 for elements */
    if ((flagval & PAMPA_MMG3D4_FACES) != 0) 
      nnbrtax[ptetnum] += 4; /* 4 for boundary faces */

    for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node
      MMG3D4_int   pt;
      PAMPA_Num pnodnum;

      pt = mmshptr->tetra[mtetnum].v[nodeind];

      pnodnum = (PAMPA_Num) (pt) + nodebas;
      nnbrtax[pnodnum] ++; // 1 for tetra
      if ((flagval & PAMPA_MMG3D4_NODE2NODE) != 0)
        nnbrtax[pnodnum] += 3; // 3 for nodes XXX un même nœud peut être compté plusieurs fois, pb mémoire si trop gros maillage ?
    }
  }

  if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
    if ((facetab = (faces *) memAlloc ((facenbr + 1) * sizeof (faces))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }

    for (mfacnm2 = mfacnum = 1, mfacnnd = (PAMPA_Num) (mmshptr->nt + 1); mfacnum < mfacnnd; mfacnum ++) {
      PAMPA_Num pfacnum;
      MMG3D4_int   nodeind;

      //if ((mmshptr->tria[mfacnum].ref != 0) && (mmshptr->tria[mfacnum].ref != 10)) { /* 10 c'est temporaire */
      if (((flagval & PAMPA_MMG3D4_ADAPT) == 0) || (mmshptr->tria[mfacnum].ref > 10)) {
        pfacnum = (PAMPA_Num) mfacnm2 + facebas;
        facetab[mfacnm2].facenum = mfacnum;
        facetab[mfacnm2].refval = (PAMPA_Num) mmshptr->tria[mfacnum].ref;
        nnbrtax[pfacnum] += 5; /* 3 for nodes and 2 for elements  */

        for (nodeind = 0 ; nodeind < 3 ; nodeind++) { // nodeind is a node
          MMG3D4_int   pt;
          PAMPA_Num pnodnum;

          pt = mmshptr->tria[mfacnum].v[nodeind];
          facetab[mfacnm2].v[nodeind] = (PAMPA_Num) pt;

          pnodnum = (PAMPA_Num) (pt) + nodebas;
          nnbrtax[pnodnum] ++;
        }
        _PAMPAintSort1asc1 (facetab[mfacnm2].v, 3);
        mfacnm2 ++;
      }
    }


    /* Sorting faces */
    _PAMPAintSort5asc3 ((PAMPA_Num *) (facetab + 1), facenbr);
  }

  // XXX faire une boucle sur ts les sommets pour mettre à jour verttax et
  // vendtax
  // en déduire edgesiz
  verttax[baseval] = baseval;
  vendtax[baseval] = baseval;
  for (vertnum = baseval + 1, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
    vendtax[vertnum] =
      verttax[vertnum] = verttax[vertnum - 1] + nnbrtax[vertnum - 1];

  edgesiz = vendtax[vertnum - 1] + nnbrtax[vertnum - 1];

  if ((edgetax = (PAMPA_Num *) memAlloc (edgesiz * sizeof (PAMPA_Num))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  edgetax -= baseval;

  for (edgenbr = 0, mtetnum = 1, mtetnnd = (PAMPA_Num) mmshptr->ne + 1 ; mtetnum < mtetnnd ; mtetnum ++) {
    PAMPA_Num ptetnum;
    MMG3D4_int *  adja;
    MMG3D4_int   ind;

    // if (!mmshptr->tetra[mtetnum].v[0]) continue; /* If the tetra doesn't exist */

    ptetnum = (PAMPA_Num) mtetnum + tetrbas;

    venttax[ptetnum] = dataptr->tetrent;

    adja = &mmshptr->adja[4*(mtetnum - 1) + 1];

    for (ind = 0 ; ind < 4 ; ind ++) {
      MMG3D4_int ngbnum; // ngbnum is a tetrahedron

      ngbnum = adja[ind] >> 2;

//#ifdef PAMPA_NOT_REF_ONE
//      if ((ngbnum == 0) || ((mmshptr->tetra[mtetnum].ref != mmshptr->tetra[ngbnum].ref) && (mmshptr->tetra[mtetnum].ref != 1) && (mmshptr->tetra[ngbnum].ref != 1))) { /* If the neighbor doesn't exist */
//#else /* PAMPA_NOT_REF_ONE */
      if ((ngbnum == 0) || (mmshptr->tetra[mtetnum].ref != mmshptr->tetra[ngbnum].ref)) { /* If the neighbor doesn't exist */
//#endif /* PAMPA_NOT_REF_ONE */
        if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
          PAMPA_Num pfacnum;
          MMG3D4_int mfacidx;
          MMG3D4_int mfacmax;
          PAMPA_Num v[3];

          v[0] = (PAMPA_Num) mmshptr->tetra[mtetnum].v[MMG_idir[ind][0]];
          v[1] = (PAMPA_Num) mmshptr->tetra[mtetnum].v[MMG_idir[ind][1]];
          v[2] = (PAMPA_Num) mmshptr->tetra[mtetnum].v[MMG_idir[ind][2]];
          _PAMPAintSort1asc1 (v, 3);

          /* Search the processor which have current neighbor */
          for (mfacidx = 1, mfacmax = facenbr + 1;
              mfacmax - mfacidx > 1; ) {
            MMG3D4_int                 mfacmed;

            mfacmed = (mfacmax + mfacidx) / 2;
            if ((facetab[mfacmed].v[0] < v[0]) || 
                ((facetab[mfacmed].v[0] == v[0]) && (facetab[mfacmed].v[1] < v[1])) ||
                ((facetab[mfacmed].v[0] == v[0]) && (facetab[mfacmed].v[1] == v[1]) && (facetab[mfacmed].v[2] <= v[2])))
              mfacidx = mfacmed;
            else
              mfacmax = mfacmed;
          }
//#if (defined PAMPA_DEBUG_ADAPT) && (! defined PAMPA_NOT_REF_ONE)
//          if ((facetab[mfacidx].v[0] != v[0]) || (facetab[mfacidx].v[1] != v[1]) || (facetab[mfacidx].v[2] != v[2])) {
//            errorPrint ("face (%d,%d,%d) not found", v[0], v[1], v[2]);
//            return (1);
//          }
//#endif /* (defined PAMPA_DEBUG_ADAPT) && (! defined PAMPA_NOT_REF_ONE) */
//#ifdef PAMPA_NOT_REF_ONE
          if ((mfacidx <= facenbr) && (facetab[mfacidx].v[0] == v[0]) && (facetab[mfacidx].v[1] == v[1]) && (facetab[mfacidx].v[2] == v[2])) {
//#endif /* PAMPA_NOT_REF_ONE */
            mfacnum = facetab[mfacidx].facenum;
            pfacnum = (PAMPA_Num) mfacidx + facebas;
            edgetax[vendtax[ptetnum] ++] = pfacnum;
            edgetax[vendtax[pfacnum] ++] = ptetnum;
            edgenbr += 2;
//#ifdef PAMPA_NOT_REF_ONE
          }
//#endif /* PAMPA_NOT_REF_ONE */
        }
      }
      else {
        edgetax[vendtax[ptetnum] ++] = ngbnum + tetrbas;
        edgenbr ++;
      }
    }

    for (nodeind = 0 ; nodeind < 4 ; nodeind ++) {
      MMG3D4_int   pt;
      MMG3D4_int   nodeind2;
      PAMPA_Num pnodnum;

      pt = mmshptr->tetra[mtetnum].v[nodeind];

      pnodnum = (PAMPA_Num) (pt) + nodebas;

      venttax[pnodnum] = dataptr->nodeent;
      edgetax[vendtax[ptetnum] ++] = pnodnum;
      edgetax[vendtax[pnodnum] ++] = ptetnum;
      edgenbr += 2;


      // pour chaque nœud du tétra sauf le nœud lui-même
      if ((flagval & PAMPA_MMG3D4_NODE2NODE) != 0)
        for (nodeind2 = 1 ; nodeind2 < 4 ; nodeind2++) {
          PAMPA_Num pnodnm2;
          PAMPA_Num edgeidx;
          PAMPA_Num edgennd;

          pt = mmshptr->tetra[mtetnum].v[(nodeind + nodeind2) % 4];

          pnodnm2 = (PAMPA_Num) (pt) + nodebas;
          for (edgeidx = verttax[pnodnum], edgennd = vendtax[pnodnum]; edgeidx < edgennd && edgetax[edgeidx] != pnodnm2; edgeidx ++); /* If already added */
          if ((edgeidx == vendtax[pnodnum]) || (edgetax[edgeidx] != pnodnm2)) {
            edgetax[vendtax[pnodnum] ++] = pnodnm2;
            edgenbr ++;
          }
        }
    }
  }

  if ((flagval & PAMPA_MMG3D4_FACES) != 0)
    for (mfacidx = 1, mfacnnd = facenbr + 1 ; mfacidx < mfacnnd ; mfacidx ++) {
      PAMPA_Num pfacnum;

      mfacnum = facetab[mfacidx].facenum;
      pfacnum = (PAMPA_Num) mfacidx + facebas;

      venttax[pfacnum] = dataptr->faceent;


      for (nodeind = 0 ; nodeind < 3 ; nodeind ++) {
        MMG3D4_int   pt;
        PAMPA_Num pnodnum;

        pt = facetab[mfacidx].v[nodeind];

        pnodnum = (PAMPA_Num) (pt) + nodebas;

        // XXX ATTENTION prévoir si la face est entre deux éléments de ref différentes
        edgetax[vendtax[pfacnum] ++] = pnodnum;
        edgetax[vendtax[pnodnum] ++] = pfacnum;
        edgenbr += 2;


      }
    }

  chekval = PAMPA_meshBuild( pmshptr, baseval, vertnbr, verttax + baseval, vendtax + baseval, NULL, edgenbr,
      edgesiz, edgetax + baseval, NULL, enttnbr, venttax + baseval, NULL, NULL, 50); // XXX pourquoi 50 comme valeurs max ??
  if (chekval != 0)
    return chekval;

  memFreeGroup (venttax + baseval);
  memFree (edgetax + baseval);

  if (chekval != 0)
    return chekval;
  chekval = PAMPA_meshValueLink (pmshptr, (void **) &geomtax, PAMPA_VALUE_PUBLIC, dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM); // XXX baseval + 1 à changer
  if (chekval != 0)
    return chekval;
  geomtax -= 3 * baseval;

  chekval = PAMPA_meshValueLink (pmshptr, (void **) &ereftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->tetrent, PAMPA_TAG_REF);
  if (chekval != 0)
    return chekval;
  ereftax -= baseval;

  if ((flagval & PAMPA_MMG3D4_ADAPT) == 0) {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &nreftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->nodeent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    nreftax -= baseval;
    //memSet (nreftax + baseval, 0, mmshptr->np * sizeof (PAMPA_Num)); // mmshptr->np ???

  }
  else {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &vflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS);
    if (chekval != 0)
      return chekval;
    vflgtax -= baseval;

  }
  if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &freftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    freftax -= baseval;
  }

  for (mtetnum = 1, mtetnnd = mmshptr->ne + 1 ; mtetnum < mtetnnd ; mtetnum ++) {
    PAMPA_Num ptetnum;
    ptetnum = (PAMPA_Num) mtetnum + tetrbas;
    ereftax[ptetnum] = mmshptr->tetra[mtetnum].ref;
  }

  for (nodeind = 1, nodennd = mmshptr->np + 1 ; nodeind < nodennd ; nodeind ++) {
    MMG_Point pt;
    MMG3D4_int i;

    pt = mmshptr->point[nodeind];
    if (pt.tag & M_UNUSED) continue;

    if ((flagval & PAMPA_MMG3D4_ADAPT) == 0)
      nreftax[nodeind + basedif] = pt.ref;
    else
      vflgtax[nodeind + nodebas] = (pt.ref == 0) ? PAMPA_TAG_VERT_INTERNAL : pt.ref - 1; /* TRICK: - 1 to have 0 as default value */


    // XXX utiliser plutot memCpy et optimiser pour le basedif
    for (i = 0 ; i < 3 ; i++)
      geomtax[(nodeind + basedif) * 3 + i] = mmshptr->point[nodeind].c[i];
  }

  if ((flagval & PAMPA_MMG3D4_FACES) != 0) {
    for (mfacidx = 1, mfacnnd = facenbr + 1; mfacidx < mfacnnd; mfacidx ++) {
      mfacnum = facetab[mfacidx].facenum;
      freftax[mfacidx + basedif] = facetab[mfacidx].refval;
      if ((flagval & PAMPA_MMG3D4_ADAPT) != 0)
        freftax[mfacidx + basedif] -= 11;
    }
    memFree (facetab);
  }


  return 0;
}
