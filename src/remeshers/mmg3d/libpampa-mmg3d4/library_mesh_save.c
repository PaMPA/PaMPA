/*  Copyright 2014-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_save.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 25 Jan 2014
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include "common.h"
#include "module.h"
#include <libmmg3d4.h>
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d4.h"
#include "pmesh_to_rmesh.h"

//! \brief This routine calls the remesher to read the mesh and convert remesher
//! mesh to PaMPA mesh
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D4_meshSave (
PAMPA_Mesh	* const			meshptr,              //!< PaMPA centralized in mesh
PAMPA_Num         const                 solflag,
void       * const         dataptr,
PAMPA_Num   * const         reftab,
char		* const         fileval)             //!< Filename value
{
  MMG_Mesh    rmshdat;
  MMG_Sol     msoldat;
  PAMPA_Num   tetrnum;
  PAMPA_Num   tetrnnd;
  PAMPA_MMG3D4_Data * data;

  data = (PAMPA_MMG3D4_Data *) dataptr;

  if (solflag != 0) {
    CHECK_FDBG2 (pmesh2rmesh (meshptr, data, 0, &rmshdat, &msoldat));
  }
  else {
    CHECK_FDBG2 (pmesh2rmesh (meshptr, data, 0, &rmshdat, NULL));
  }
  if (reftab != NULL)
    for(tetrnum = 1, tetrnnd = rmshdat.ne + 1 ; tetrnum < tetrnnd ; tetrnum ++) 
      rmshdat.tetra[tetrnum].ref = reftab[tetrnum - 1] + 2; /* If value is -2 */

  rmshdat.info.imprim = 0; //10;
  if ((solflag != 0) && (data->infoprt != 0)) {
    MMG_outqua(&rmshdat, &msoldat);
    MMG_prilen(&rmshdat, &msoldat);
  }
  MMG_saveMesh (&rmshdat, fileval);
  if (solflag != 0) {
    MMG_saveSol (&rmshdat, &msoldat, fileval);
    MMG_solexit (&msoldat);
  }
  MMG_meshexit (&rmshdat);
  return (0);
}
