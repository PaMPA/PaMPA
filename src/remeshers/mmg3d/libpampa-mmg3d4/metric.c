/*  Copyright 2014-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        metric.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 25 Jan 2014
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d4.h"
#include "types.h"
#include <libmmg3d4.h>
#include <math.h>

// FIXME pour débogue
static int countval = 0;
extern unsigned char MMG_iare[6][2];

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

inline int
PAMPA_MMG3D4_metCalc (
PAMPA_Iterator * const     iterptr,             //!< Iterator
PAMPA_Num   const          vertlocnum,          //!< Element number for which metric will be computed
double * const          coorloctax,
double * const          metrloctax,
PAMPA_Num const         soloffset,
double    * const          veloloctax)
{
  PAMPA_Num nodeidx;
  PAMPA_Num edgenum;
  PAMPA_Num nodetab[4];
  double veloval;

  nodeidx = 0;
  PAMPA_itStart(iterptr, vertlocnum);
  while (PAMPA_itHasMore(iterptr)) {
    PAMPA_Num nodenum;

    nodenum = PAMPA_itCurEnttVertNum(iterptr);
    nodetab[nodeidx ++] = nodenum;
    PAMPA_itNext(iterptr);
  }

  for (veloval = edgenum = 0; edgenum < 6; edgenum ++) {
    MMG3D4_int p1;
    MMG3D4_int p2;
    double solval;

    p1 = (MMG3D4_int) nodetab[MMG_iare[edgenum][0]];
    p2 = (MMG3D4_int) nodetab[MMG_iare[edgenum][1]];
    if (soloffset == 1)
      solval = MMG_long_iso (&coorloctax[3*p1], &coorloctax[3*p2], &metrloctax[p1], &metrloctax[p2]);
    else
      solval = MMG_long_ani (&coorloctax[3*p1], &coorloctax[3*p2], &metrloctax[p1 * soloffset], &metrloctax[p2 * soloffset]);
    // XXX ensuite convertir le double en nb de sommets 
    if (solval < 0.6)
      veloval += 1;
    else if (solval > 2)
      veloval += 2 * ((int) solval - 1);
    else if (solval > 1.5)
      veloval += 1;
  } 

  veloloctax[vertlocnum] = veloval;
  //veloloctax[vertlocnum] = veloval / 6;

  return (0);
}

