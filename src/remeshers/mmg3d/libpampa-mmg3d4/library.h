/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/

/*
**  The type and structure definitions.
*/

#define PAMPA_MMG3D4_NOFLAG        0x0000
#define PAMPA_MMG3D4_ADAPT         0x0002
#define PAMPA_MMG3D4_FACES         0x0004
#define PAMPA_MMG3D4_FORTRAN       0x0008
#define PAMPA_MMG3D4_NODE2NODE     0x0010

typedef struct {
  PAMPA_Num tetrent;
  PAMPA_Num nodeent;
  PAMPA_Num faceent;
  PAMPA_Num infoprt;
  double    edgemin;
  double    edgemax;
  double    qualmax;
  int       opt[10]; // TODO: normally MMG3D4_int opt[10]
  MPI_Datatype coortyp;
} PAMPA_MMG3D4_Data;

int PAMPA_MMG3D4_meshAdapt (
    PAMPA_Mesh * const           imshptr,
    PAMPA_Mesh * const           omshptr,
    PAMPA_AdaptInfo * const      infoptr,
    PAMPA_Num    const           flagval);

int PAMPA_MMG3D4_meshLoad (
    PAMPA_Mesh        * const    meshptr,
    char              * const    fileval,
    PAMPA_MMG3D4_Data * const    dataptr,
    PAMPA_Num    const           flagval);

int PAMPA_MMG3D4_dmeshBand (
    PAMPA_Dmesh * const          dmshptr,
    void        * const          dataptr,
    PAMPA_Num   * const          vertgsttab,
    PAMPA_Num                    bandval);

int PAMPA_MMG3D4_dmeshCheck (
    PAMPA_Dmesh * const          dmshptr,
    void        * const          dataptr,
    PAMPA_Num const              flagval);

int PAMPA_MMG3D4_dmeshMetricCompute (
    PAMPA_Dmesh * const        dmshptr,
    void        * const        dataptr,
    double                     alphval);

int PAMPA_MMG3D4_meshSave (
    PAMPA_Mesh * const           meshptr,
    PAMPA_Num    const           solflag,
    void       * const           dataptr,
    PAMPA_Num  * const           reftab,
    char       * const           fileval);

int
PAMPA_MMG3D4_dmeshWeightCompute (
PAMPA_Dmesh * const        dmshptr,             //!< PaMPA distributed mesh
void        * const        dataptr,
double      * const        veloloctab,
PAMPA_Num   *              vnodglbnbr);
