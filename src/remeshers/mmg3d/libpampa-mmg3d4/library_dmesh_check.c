/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_check.c
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/

#ifdef PAMPA_TIME_CHECK2
#define PAMPA_TIME_CHECK
#endif /* PAMPA_TIME_CHECK2 */

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#include "module.h"
#include "common.h"
#include "comm.h"
#include "types.h"
#include <pampa.h>
#include <pampa.h>
#include <pampa-mmg3d4.h>
#include <libmmg3d4.h>
#include "pmesh_to_rmesh.h"
#include "rmesh_to_pmesh.h"
//#include <unistd.h>
#include <fcntl.h>

// FIXME pour débogue
static int countval = 0;

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D4_dmeshCheck (
PAMPA_Dmesh * const        dmshptr,               //!< PaMPA distributed in mesh
void        * const   dataptr,                    //!< Opaque data structure used by the remesher
PAMPA_Num const            flagval)               //!< flags used by the remesher
{
  PAMPA_MMG3D4_Data * data;
  PAMPA_Mesh * cmshptr;
  MMG_Mesh    rmshdat;
  MMG_Sol     msoldat;
  MMG3D4_int         opt[10];
  PAMPA_Num   baseval;
  PAMPA_Num   basedif;
  PAMPA_Num   tetrlocnbr;
  PAMPA_Num   tetrgstnbr;
  PAMPA_Num tetrnbr;
  PAMPA_Num   vertnbr;
  PAMPA_Num   vertnum;
  PAMPA_Num   meshlocnbr;
  MMG3D4_int   vnumnbr;
  PAMPA_Iterator it;
  PAMPA_Num * restrict iflgtax;
  PAMPA_Num * restrict erefloctax;
  PAMPA_Num * restrict rmshloctax;
  MMG3D4_int * restrict rmshloctx2;
  PAMPA_Num * restrict vnumgsttax;
  PAMPA_Num * restrict parttax;
  PAMPA_Num * restrict vnumloctax; // XXX est-ce le bon nom ?
  PAMPA_Num * restrict vnumtax;
  PAMPA_Num       vertlocnum;
  PAMPA_Num       vertlocnnd;
  MPI_Comm        proccomm;
  int new;
  int bak;
  int cheklocval;
  int mmgval;
  int proclocnum;
  int procngbnum;
  int procglbnbr;
  char s[50];

  data = (PAMPA_MMG3D4_Data *) dataptr;
  cheklocval = 0;

  opt[0]=1; // 4; //splitting
  opt[1]=0; //debug
  opt[2]=64; //par default 64
  opt[3]=0;//noswap
  opt[4]=0;//noinsert
  opt[5]=0;//nomove
  opt[6]=5; //imprim
  opt[7]=0; //7;  //renum
  opt[8]=0; //500; //renum
  //opt[0]=0; // 4; //splitting
  //opt[1]=0; //debug
  //opt[2]=64; //par default 64
  //opt[3]=1;//noswap
  //opt[4]=1;//noinsert
  //opt[5]=1;//nomove
  //opt[6]=5; //imprim
  //opt[7]=3;  //renum
  //opt[8]=1; //renum
  opt[9]=0; // 0 pour etre dans le cas normal, 1 pour optim les 
  
  // XXX remplacer le calloc

  PAMPA_dmeshData (dmshptr, &baseval, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &proccomm); 
  MPI_Comm_size(proccomm, &procglbnbr) ;
  MPI_Comm_rank(proccomm, &proclocnum) ;

  basedif = 1 - baseval;
  CHECK_FERR(PAMPA_dmeshValueData(dmshptr, data->tetrent, PAMPA_TAG_REF, (void **) &erefloctax), proccomm);
  erefloctax -= baseval;
  CHECK_FERR(PAMPA_dmeshValueData(dmshptr, data->tetrent, PAMPA_TAG_REMESH, (void **) &rmshloctax), proccomm);
  rmshloctax -= baseval;
  PAMPA_dmeshEnttSize (dmshptr, data->tetrent, &tetrlocnbr, PAMPA_VERT_LOCAL, &tetrgstnbr, PAMPA_VERT_ANY, NULL);

  CHECK_FERR (PAMPA_dmeshValueLink(dmshptr, (void **) &vnumloctax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, data->tetrent, PAMPA_TAG_PERM), proccomm);
  vnumloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = tetrlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) 
    vnumloctax[vertlocnum] = vertlocnum;

#ifdef PAMPA_DEBUG_ADAPT2
  if (PAMPA_TAG_VERT_NOREMESH != 0) {
    errorPrint ("Tag PAMPA_TAG_VERT_NOREMESH is not 0");
    cheklocval = 1;
  }
  CHECK_VDBG (cheklocval, proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

  if (memAllocGroup ((void **) (void *)
                          &vnumgsttax, (size_t) (tetrgstnbr * sizeof (PAMPA_Num)),
                          &parttax,    (size_t) (procglbnbr * sizeof (PAMPA_Num)),
                          &rmshloctx2, (size_t) (tetrlocnbr * sizeof (MMG3D4_int)),
                          &cmshptr,    (size_t) (             sizeof (PAMPA_Mesh)),
                          NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  vnumgsttax -= baseval;
  parttax -= baseval;
  rmshloctx2 -= baseval;

  memSet (rmshloctx2 + baseval, 0, tetrlocnbr * sizeof (MMG3D4_int));
  memSet (vnumgsttax + baseval, ~0, tetrgstnbr * sizeof (PAMPA_Num)); // XXX pas ~0, mais le define

  PAMPA_meshInit (cmshptr);
  for (vertlocnum = baseval, vertlocnnd = tetrlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
#ifdef PAMPA_NOT_REF_ONE
    if (erefloctax[vertlocnum] != PAMPA_REF_IS)
#endif /* PAMPA_NOT_REF_ONE */
      vnumgsttax[vertlocnum] = proclocnum;
    //else
    //  printf ("je passe au moins là une fois\n");
  for (procngbnum = 0; procngbnum < procglbnbr; procngbnum ++) 
    parttax[procngbnum + baseval] = procngbnum;

  memSet (rmshloctax + baseval, PAMPA_TAG_VERT_NOREMESH, tetrlocnbr * sizeof (PAMPA_Num));

  meshlocnbr = -1;
  CHECK_FERR (PAMPA_dmeshGatherInduceMultiple (dmshptr, procglbnbr, vnumgsttax + baseval, parttax + baseval, &meshlocnbr, &cmshptr), proccomm);


  if (meshlocnbr == 1) {
#ifdef PAMPA_ADAPT_TRACE
    char s2[50];
    FILE * ballfile;
    PAMPA_meshEnttSize (cmshptr, data->tetrent, &tetrnbr);
    sprintf (s2, "ballsiz-%d", getpid());
    ballfile = fopen(s2, "a");
    fprintf(ballfile, "%d\n", tetrnbr);
    fclose (ballfile);
#endif /* PAMPA_ADAPT_TRACE */
    PAMPA_meshData (cmshptr, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);

    // XXX tester le retour des fonctions
    CHECK_FERR (pmesh2rmesh(cmshptr, data, flagval, &rmshdat, &msoldat), proccomm);
    //#ifdef PAMPA_DEBUG_ADAPT2
    //  {
    //    PAMPA_Num * iflgtx2;
    //    //printf ("%s: pid %d, line %d", __FUNCTION__, getpid(), __LINE__);
    //    //sprintf (s, "apres_adapt-mmg3d4-%d-%d.mesh", getpid(), countval);
    //    //MMG_saveMesh (&rmshdat, s);
    //    //MMG_saveSol (&rmshdat, &msoldat, s);
    //    sprintf (s, "avant_adapt-pampa1-%d-%d.mesh", getpid(), countval);
    //    PAMPA_MMG3D4_meshSave (cmshptr, 0, data, irmstax + baseval, s);
    //    PAMPA_meshEnttSize (cmshptr, data->tetrent, &tetrnbr);
    //    if ((iflgtx2 = (PAMPA_Num *) memAlloc (tetrnbr * sizeof (PAMPA_Num))) == NULL) {
    //      errorPrint  ("out of memory");
    //      return (1);
    //    }
    //    iflgtx2 -= baseval;
    //
    //    PAMPA_meshItInitStart(cmshptr, data->tetrent, &it);
    //    while (PAMPA_itHasMore(&it)) {
    //      PAMPA_Num mtetnum;
    //      PAMPA_Num tetnum;
    //
    //      mtetnum = PAMPA_itCurMeshVertNum(&it);
    //      tetnum = PAMPA_itCurEnttVertNum(&it);
    //
    //      iflgtx2[tetnum] = iflgtax[mtetnum];
    //      PAMPA_itNext(&it);
    //    }
    //    sprintf (s, "avant_adapt-pampa2-%d-%d.mesh", getpid(), countval);
    //    PAMPA_MMG3D4_meshSave (cmshptr, 0, data, iflgtx2 + baseval, s);
    //    memFree (iflgtx2 + baseval);
    //  }
    //#endif /* PAMPA_DEBUG_ADAPT2 */
    //  printf ("%s: pid %d, line %d", __FUNCTION__, getpid(), __LINE__);
    //  sprintf (s, "avant_adapt-mmg3d4-%d-%d.mesh", getpid(), countval);
    //  MMG_saveMesh (&rmshdat, s);
    //  MMG_saveSol (&rmshdat, &msoldat, s);
    //#endif /* PAMPA_DEBUG_ADAPT2 */
    // FIXME le retoru de mmg3dlib peut être un warning pour la valeur 1
    // voir s'il y a d'autres valeurs et lesquelles sont des erreurs
    //CHECK_FERR (MMG_mmg3dlib(opt, &rmshdat, &msoldat));
#undef PAMPA_INFO_REMESHER /* XXX temporaire */
#ifdef PAMPA_INFO_REMESHER
    sprintf (s, "check-out-%d-%d", getpid(), countval ++);
#else /* PAMPA_INFO_REMESHER */
    sprintf (s, "/dev/null"); // TODO mettre /dev/null comme macro définie par CMake, car NUL pour win et /dev/null pour unix ;)
#endif /* PAMPA_INFO_REMESHER */
    fflush (stdout);
    bak = dup (1);
    new = open(s, O_CREAT|O_WRONLY|O_TRUNC, 0600);
    dup2 (new, 1);
    close (new);
    fprintf (stdout, "oui c'est bien moi, s:%s\n\n\n\n\n",s);
    mmgval = MMG_mmg3dcheck(opt, &rmshdat, &msoldat, data->qualmax, data->edgemin, data->edgemax, rmshloctx2 - basedif);
    //fprintf (stderr, "retour de mmg : %d\n", cheklocval);
    fflush (stdout);
    dup2 (bak, 1);
    close (bak);

    CHECK_FERR(PAMPA_meshValueData(cmshptr, data->tetrent, PAMPA_TAG_PERM, (void **) &vnumtax), proccomm); // XXX attention data->tetrent vaut-il toujours baseval ?
    vnumtax -= baseval;
    PAMPA_meshEnttSize (cmshptr, data->tetrent, &tetrnbr);

    for (vertlocnum = baseval, vertlocnnd = tetrnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
      if (rmshloctx2[vertlocnum] == 1)
        rmshloctax[vnumtax[vertlocnum]] = PAMPA_TAG_VERT_REMESH;

    MMG_meshexit (&rmshdat);
    MMG_solexit (&msoldat);

    PAMPA_meshExit (cmshptr);
  }
  memFreeGroup (vnumgsttax + baseval);
  CHECK_FERR (PAMPA_dmeshValueUnlink(dmshptr, data->tetrent, PAMPA_TAG_PERM), proccomm);

  CHECK_VDBG (cheklocval, proccomm);
  return (0);
}

