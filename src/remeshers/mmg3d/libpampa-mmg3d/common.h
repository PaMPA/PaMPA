/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        common.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define COMMON_H

/*
** The includes.
*/

#include            <ctype.h>
#include            <math.h>
#include            <memory.h>
#include            <stdio.h>
#include            <stdarg.h>
#include            <stdlib.h>
#ifdef HAVE_MALLOC_H
#include            <malloc.h>                    /* Deprecated, but required on some old systems */
#endif /* HAVE_MALLOC_H */
#include            <string.h>
#include            <time.h>                      /* For the effective calls to clock () */
#include            <limits.h>
#include            <float.h>
#include            <sys/types.h>
#include            <sys/time.h>
#ifdef COMMON_TIMING_OLD
#include            <sys/resource.h>
#endif /* COMMON_TIMING_OLD */
#include            <unistd.h>

#ifndef __cplusplus
#include            <mpi.h>
#endif /* __cplusplus */

#if ((defined COMMON_PTHREAD) || (defined SCOTCH_PTHREAD))
#include            <pthread.h>
#endif /* ((defined COMMON_PTHREAD) || (defined SCOTCH_PTHREAD)) */

/*
**  Working definitions.
*/

#ifdef PAMPA_DEBUG_MEM
#define MEM_ALIGN(s) (s == 0) ? (1) : (s)
#else /* PAMPA_DEBUG_MEM */
#define MEM_ALIGN(s) ((s) | 8)
#endif /* PAMPA_DEBUG_MEM */

#ifdef COMMON_MEMORY_TRACE
#ifdef PAMPA_RENAME
#define memAllocGroup(ptr,...)      _PAMPAmemAllocGroupRecord (__FUNCTION__, __LINE__, (ptr), ##__VA_ARGS__)
#define memReallocGroup(ptr,...)    _PAMPAmemReallocGroupRecord (__FUNCTION__, __LINE__, (ptr), ##__VA_ARGS__)
#define memAlloc(size)              _PAMPAmemAllocRecord (__FUNCTION__, __LINE__, MEM_ALIGN(size))
#define memRealloc(ptr,size)        _PAMPAmemReallocRecord (__FUNCTION__, __LINE__, (ptr), MEM_ALIGN(size))
#define memCalloc(nmemb,size)       _PAMPAmemCallocRecord (__FUNCTION__, __LINE__, (nmemb), MEM_ALIGN(size))
#define memFree(ptr)                (_PAMPAmemFreeRecord (__FUNCTION__, __LINE__, (void *) (ptr)), 0)
#else /* PAMPA_RENAME */
#define memAllocGroupRecord(ptr,...)      memAllocGroupRecord (__FUNCTION__, __LINE__, (ptr), ##__VA_ARGS__)
#define memReallocGroupRecord(ptr,...)    memReallocGroupRecord (__FUNCTION__, __LINE__, (ptr), ##__VA_ARGS__)
#define memAlloc(size)              memAllocRecord (__FUNCTION__, __LINE__, MEM_ALIGN(size))
#define memRealloc(ptr,size)        memReallocRecord (__FUNCTION__, __LINE__, (ptr), MEM_ALIGN(size))
#define memCalloc(nmemb,size)       memCallocRecord (__FUNCTION__, __LINE__, (nmemb), MEM_ALIGN(size))
#define memFree(ptr)                (memFreeRecord (__FUNCTION__, __LINE__, (void *) (ptr)), 0)
#endif /* PAMPA_RENAME */
#else /* COMMON_MEMORY_TRACE */
#define memAlloc(size)              malloc(MEM_ALIGN(size)) /* For platforms which return NULL for malloc(0) */
#define memRealloc(ptr,size)        realloc((ptr),MEM_ALIGN(size))
#define memCalloc(nmemb,size)       calloc ((nmemb), MEM_ALIGN(size))
#define memFree(ptr)                (free ((char *) (ptr)), 0)
#endif /* COMMON_MEMORY_TRACE */


#define memSet(ptr,val,siz)         memset((ptr),(val),(siz))
#define memCpy(dst,src,siz)         memcpy((dst),(src),(siz))
#define memMov(dst,src,siz)         memmove((dst),(src),(siz))

#define MIN(x,y)                    (((x) < (y)) ? (x) : (y))
#define MAX(x,y)                    (((x) < (y)) ? (y) : (x))
#define ABS(x)                      MAX ((x), -(x))
#define SIGN(x)                     (((x) < 0) ? -1 : 1)

/* linked values flags. */




/*
**  Handling of generic types.
*/

#ifndef INT                                       /* If type not externally overriden */
#ifdef INTSIZE32
#define INT                         int32_t
#define UINT                        u_int32_t
#define COMM_INT                    MPI_INTEGER4
#define INTSTRING                   "%d"
#else /* INTSIZE32 */
#ifdef INTSIZE64
#define INT                         int64_t
#define UINT                        u_int64_t
#define COMM_INT                    MPI_LONG_LONG
#define INTSTRING                   "%lld"
#else /* INTSIZE64 */
#ifdef LONG                                       /* Better not use it */
#define INT                         long          /* Long integer type */
#define UINT                        unsigned long
#define COMM_INT                    MPI_LONG
#define INTSTRING                   "%ld"
#else /* LONG */
#define INT                         int           /* Default integer type */
#define UINT                        unsigned int
#define COMM_INT                    MPI_INT       /* Generic MPI integer type */
#define INTSTRING                   "%d"
#endif /* LONG      */
#endif /* INTSIZE64 */
#endif /* INTSIZE32 */
#endif /* INT       */

#ifndef INTSIZEBITS
#define INTSIZEBITS                 (sizeof (INT) << 3)
#endif /* INTSIZEBITS */

#define INTVALMAX                   ((INT) (((UINT) 1 << (INTSIZEBITS - 1)) - 1))

#define byte unsigned char                        /* Byte type */
#ifndef BYTE
#define BYTE                        byte
#endif /* BYTE */
#ifndef COMM_BYTE
#define COMM_BYTE                   MPI_BYTE
#endif /* COMM_BYTE */
#define COMM_PART                   COMM_BYTE

/*
**  Handling of timers.
*/

/** The clock type. **/

typedef struct Clock_ {
  double                    time[2];              /*+ The start and accumulated times +*/
} Clock;

/*
**  Handling of files.
*/

/** The file structure. **/

typedef struct File_ {
  char *                    name;                 /*+ File name    +*/
  FILE *                    pntr;                 /*+ File pointer +*/
  char *                    mode;                 /*+ Opening mode +*/
} File;

/*
**  Function prototypes.
*/

#ifdef COMMON_MEMORY_TRACE
void *                      memAllocRecord      (const char * const, int, size_t);
void *                      memReallocRecord    (const char * const, int, void * const, size_t);
void                        memFreeRecord       (const char * const, int, void * const);
size_t                      memMax              ();
void *                      memAllocGroupRecord (const char * const, int, void **, ...);
void *                      memReallocGroupRecord     (const char * const, int, void *, ...);
void *                      memOffset           (void *, ...);
#else /* COMMON_MEMORY_TRACE */
void *                      memAllocGroup       (void **, ...);
void *                      memReallocGroup     (void *, ...);
void                        memFreeGroup        (void *);
void *                      memOffset           (void *, ...);
#endif /* COMMON_MEMORY_TRACE */

void                        usagePrint          (FILE * const, const char (* []));

int                         fileBlockOpen       (File * const, const int);
int                         fileBlockOpenDist   (File * const, const int, const int, const int, const int);
void                        fileBlockClose      (File * const, const int);
FILE *                      fileCompress        (FILE * const, const int);
int                         fileCompressType    (const char * const);
FILE *                      fileUncompress      (FILE * const, const int);
int                         fileUncompressType  (const char * const);
int                         fileNameDistExpand  (char ** const, const int, const int, const int);

void                        errorProg           (const char * const);
void                        errorPrintW         (const char * const, ...);
void                        PAMPA_errorPrint    (const char * const, ...);

int                         intLoad             (FILE * const, INT * const);
int                         intSave             (FILE * const, const INT);
void                        intAscn             (INT * const, const INT, const INT);
void                        intPerm             (INT * const, const INT);
void                        intRandReset        (void);
void                        intRandInit         (void);
INT                         intRandVal          (INT);
void                        intSort1asc1        (void * const, const INT);
void                        intSort2asc1        (void * const, const INT);
void                        intSort2asc2        (void * const, const INT);
void                        intSort3asc1        (void * const, const INT);
void                        intSort3asc2        (void * const, const INT);
void                        intSort3asc3        (void * const, const INT);
void                        intSort4asc2        (void * const, const INT);
void                        intSort4asc4        (void * const, const INT);
void                        intSort5asc3        (void * const, const INT);
INT                         intSearchDicho      (const INT * const, const INT, const INT, const INT);

void                        clockInit           (Clock * const);
void                        clockStart          (Clock * const);
void                        clockStop           (Clock * const);
double                      clockVal            (Clock * const);
double                      clockGet            (void);

/*
**  Macro definitions.
*/

#define assert_scotch(f,m)          if ((f) != 0) { errorPrint(m); return (1);}
#define clockInit(clk)              ((clk)->time[0]  = (clk)->time[1] = 0)
#define clockStart(clk)             ((clk)->time[0]  = clockGet ())
#define clockStop(clk)              ((clk)->time[1] += (clockGet () - (clk)->time[0]))
#define clockVal(clk)               ((clk)->time[1])

#ifdef COMMON_RANDOM_RAND
#define intRandVal(ival)            ((INT) (((UINT) rand ()) % ((UINT) (ival))))
#else /* COMMON_RANDOM_RAND */
#define intRandVal(ival)            ((INT) (((UINT) random ()) % ((UINT) (ival))))
#endif /* COMMON_RANDOM_RAND */

#define DATASIZE(n,p,i)             ((INT) (((n) + ((p) - 1 - (i))) / (p)))

#define FORTRAN(nu,nl,pl,pc)        FORTRAN2(FORTRAN3(nu),FORTRAN3(nl),pl,pc)
#define FORTRAN2(nu,nl,pl,pc)                    \
void nu pl;                                      \
void nl pl                                       \
{ nu pc; }                                       \
void FORTRAN4(nl,_) pl	                         \
{ nu pc; }                                       \
void FORTRAN4(nl,__) pl                          \
{ nu pc; }                                       \
void nu pl
#define FORTRAN3(s)                 s
#define FORTRAN4(p,s)               p##s
