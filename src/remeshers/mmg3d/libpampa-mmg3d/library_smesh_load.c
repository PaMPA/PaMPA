/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_smesh_load.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 14 Nov 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include "common.h"
#include "module.h"
#include <libmmg3d.h>
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d.h"
#include "rmesh_to_psmesh.h"
#include "types.h"

//! \brief This routine calls the remesher to read the mesh and convert remesher
//! mesh to PaMPA mesh
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D_smeshLoad (
PAMPA_Smesh	* const			meshptr,              //!< PaMPA centralized in mesh
char		* const         fileval,             //!< Filename value
PAMPA_MMG3D_Data   * const         dataptr,
PAMPA_Num const             flagval,
PAMPA_Num const             procnbr,
PAMPA_Num * const           procvrttab)
{
  MMG5_pMesh    mmshptr;
  MMG5_pSol    msolptr;
  MMG3D_int dummy;
  PAMPA_Num baseval;
  PAMPA_Num basedif;
  PAMPA_Num nodenbr;
  PAMPA_Num nodenum;
  PAMPA_Num nodennd;
  PAMPA_Num * data;
  PAMPA_Num * restrict nodepermtax;
  PAMPA_Iterator it;
  double * soltax;
  const MMG3D_int offset = 1;

  mmshptr = NULL;
  msolptr = NULL;
  MMG3D_Init_mesh (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);
  CHECK_FDBG2 (MMG3D_Set_inputMeshName (mmshptr, fileval) != 1);
  CHECK_FDBG2 (MMG3D_Set_inputSolName (mmshptr, msolptr, fileval) != 1);
  CHECK_FDBG2 (MMG3D_loadMesh (mmshptr, fileval) != 1);

  if ((flagval & PAMPA_MMG3D_FORTRAN) != 0)
	baseval = 1;
  else
    baseval = 0;
  basedif = 1 - baseval;
  CHECK_FDBG2 (rmesh2psmesh (mmshptr, msolptr, dataptr, baseval, flagval | PAMPA_MMG3D_SEQPART, meshptr, procnbr, procvrttab, &nodenbr));
  CHECK_FDBG2 (PAMPA_smeshValueData (meshptr, dataptr->nodeent, -142, (void **) &nodepermtax));
  nodepermtax -= baseval;

  if ((flagval & PAMPA_MMG3D_FORTRAN) == 0) { // FIXME test barbare pour ne pas charger la sol avec le proto du laplacien
    MMG3D_loadSol (mmshptr, msolptr, fileval);
    // XXX temporaire
    dataptr->infoval.hmin = mmshptr->info.hmin;
    dataptr->infoval.hmax = mmshptr->info.hmax;
    // XXX fin temporaire
    //renumbering(500, mmshptr, msolptr);

    //if ( !MMG5_hashTetra(mmshptr) )    return(1);


    // XXX uniquement métrique scalaire
    if ((soltax = (double *) memAlloc (nodenbr * sizeof (double))) == NULL) { // XXX pas bon le 3
      errorPrint  ("out of memory");
      return      (1);
    }
    PAMPA_smeshValueLink (meshptr, (void **) &soltax, PAMPA_VALUE_ALLOCATED, MPI_DOUBLE, dataptr->nodeent, PAMPA_TAG_SOL_3DI); 
    soltax -= offset * baseval;

    // XXX pour que get_scalarsol ne renvoie pas d'erreur
    CHECK_FDBG2 (MMG3D_Get_solSize(mmshptr, msolptr, &dummy, &dummy, &dummy) != 1);

    for (nodenum = baseval, nodennd = nodenbr + baseval; nodenum < nodennd; nodenum ++) {
      CHECK_FDBG2 (MMG3D_Get_scalarSol (msolptr, &soltax[nodepermtax[nodenum]]) != 1);
    }
  }
  PAMPA_smeshValueUnlink (meshptr, dataptr->nodeent, -142); 

  MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);
  return (0);
}
