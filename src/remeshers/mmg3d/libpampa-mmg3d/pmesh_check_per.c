/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pmesh_check_per.c
//!
//!   \authors     Cedric Lachat
//!
//!
/************************************************************/


/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include "pampa-mmg3d.h"
#include "types.h"
#include "pmesh_check_per.h"
#include <scotch.h>

//! \brief This routine XXX
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

  int
pmeshCheckPer (
    PAMPA_Mesh  * const         imshptr,              //!< PaMPA centralized input mesh
    PAMPA_MMG3D_Data  * const   dataptr,              //!< XXX
    PAMPA_Mesh  * const         omshptr)              //!< PaMPA centralized output mesh
{
  PAMPA_Num            baseval;
  PAMPA_Num            tetrnbr;
  PAMPA_Num            facenbr;
  PAMPA_Num            nodenbr;
  PAMPA_Num * restrict permpertax;
  double    * restrict coorpertax;
  PAMPA_Num            tetrlismax;
  PAMPA_Num            tetrlisnbr;
  PAMPA_Num            tetrlisnum;
  DoubleNum * restrict tetrlistab;
  PAMPA_Num * restrict eflgtax;
  PAMPA_Num * restrict nflgtax;
  double    * restrict coortax;
  double    * restrict coorlistab;
  double    * restrict newcoortax;
  PAMPA_Num            elemnum;
  PAMPA_Num            elemnnd;
  PAMPA_Num            elemnb2;
  PAMPA_Iterator       it_face;
  PAMPA_Iterator       it_e2f;
  PAMPA_Iterator       it_e2n;
  PAMPA_Iterator       it_e2e;
  PAMPA_Num const      planoritab[DIMNBR * 4 + 1] = {
    0x0100, /* 0100000000: original plane 3          */
    0x0080, /* 0010000000: original plane 3          */
    0x0040, /* 0001000000: original plane 3          */
    0x0024, /* 0000100100: mask for planes 3 or -3   */
    0x0012, /* 0000010010: mask for planes 2 or -2   */
    0x0009, /* 0000001001: mask for planes 1 or -1   */
    0x0004, /* 0000000100: plane 3                   */
    0x0002, /* 0000000010: plane 2                   */
    0x0001, /* 0000000001: plane 1                   */
    0x0200, /* 1000000000: flag which means not kept */
    0x0008, /* 0000001000: plane -1                  */
    0x0010, /* 0000010000: plane -2                  */
    0x0020, /* 0000100000: plane -3                  */
  };
  PAMPA_Num * const  plantab = planoritab + DIMNBR;
  PAMPA_Num const    origidx = 2 * DIMNBR;
  PAMPA_Num const    maskidx =     DIMNBR;
    
  
  PAMPA_meshData (imshptr, &baseval, NULL, NULL, NULL, NULL, NULL, NULL);

  PAMPA_meshEnttSize (imshptr, dataptr->tetrent, &tetrnbr);
  PAMPA_meshEnttSize (imshptr, dataptr->nodeent, &nodenbr);
  PAMPA_meshEnttSize (imshptr, dataptr->faceent, &facenbr);

  // on a deux valeurs associées par face
  // 1) les coordonnées à ajouter (ou à soustraire) pour l'élément voisin par périodicité
  // 2) le plan (0, 1, 2, 4), la face périodique et les paires de numéros de nœuds périodiques (p,f,((n1,n1'),(n2,n2'),(n3,n3'))
  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, dataptr->nodeent, PAMPA_TAG_PER_COOR, (void **) &coorpertax));
  coorpertax -= baseval;
  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, dataptr->faceent, PAMPA_TAG_PER_PERM,   (void **) &permpertax));
  permpertax -= baseval;

  // cette fonction doit
  // 1) vérifier qu'on ne passe pas plus qu'une fois par un plan périodique
  // 2) modifier les coordonnées des nœuds en fonction des plans périodiques
  //
  // en fait dès qu'on passe par un plan périodique, on ne peut pas passer par
  // un autre pour un même élément.
  // il faut une variable qui stocke en binaire chaque plan périodique
  //
  // on met le premier élément du maillage dans la liste «l» et on le marque

  // XXX début nouvelle partie

  if (memAllocGroup ((void **) (void *)
        &tetrlistab, (size_t) (tetrnbr * 3          * sizeof (DoubleNum)),
        &eflgtax,    (size_t) (tetrnbr              * sizeof (PAMPA_Num)),
        &nflgtax,    (size_t) (nodenbr              * sizeof (PAMPA_Num)),
        &coorlistab, (size_t) (tetrnbr * 3 * DIMNBR * sizeof (double)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }
  eflgtax -= baseval;
  nflgtax -= baseval;

  tetrlisnum = 
    tetrlisnbr = 0;
  tetrlismax = tetrnbr * 3;

  memSet (eflgtax + baseval, 0, tetrnbr * sizeof (PAMPA_Num));
  memSet (nflgtax + baseval, 0, nodenbr * sizeof (PAMPA_Num));
  memCpy (newcoortax, coortax, nodenbr * DIMNBR * sizeof (double));

  PAMPA_meshItInitStart(imshptr, dataptr->faceent, &it_face);
  PAMPA_meshItInit(imshptr, dataptr->faceent, dataptr->tetrent, &it_e2f);
  PAMPA_meshItInit(imshptr, dataptr->faceent, dataptr->nodeent, &it_e2n);

  // * pour chaque face f
  while (PAMPA_itHasMore(&it_face)) {
    PAMPA_Num facenum;
    PAMPA_Num plannum;

    facenum = PAMPA_itCurEnttVertNum(&it_face);

    // ** si elle a une face périodique (permpertax[f] != ~0 ou l'avoir sous forme d'une structure : permpertax[f].facenum != ~0)
    if ((plannum = permpertax[facenum]) != 0) {
      PAMPA_Iterator it_e2f;
      PAMPA_Num      cuntnum;

      PAMPA_itStart(&it_e2f, facenum);
      cuntnum = 1;

      while (PAMPA_itHasMore(&it_e2f)) {
        elemnum = PAMPA_itCurEnttVertNum(&it_e2f);

        // *** on flague le premier ET le second élément (dans eflgtax)
        // **** avec le numéro de plan de la face pour le premier
        // **** avec l'opposé du numéro de plan de la face pour le second
        eflgtax[elemnum] = plantab[cuntnum * plannum];
        // ***** et pour tous les deux la valeur « élément original »
        eflgtax[elemnum] |= plantab[origidx + plannum];
        // *** on ajoute les deux éléments de la face dans tetrlistab et on copie coorpertax[3*f] dans coorlistab 
        memCpy (coorlistab + (tetrlisnbr % tetrlismax) * DIMNBR, coorpertax + facenum * DIMNBR, DIMNBR * sizeof (double));
        tetrlistab[tetrlisnbr % tetrlismax].plannum = plannum;
        tetrlistab[tetrlisnbr % tetrlismax].elemnum = elemnum;
        tetrlisnbr = (tetrlisnbr + 1) % tetrlismax;
        cuntnum = -cuntnum;
        PAMPA_itNext(&it_e2f);
      }

      PAMPA_itStart(&it_e2n, facenum);
      cuntnum = 1;

      // *** pour chaque nœud de la face
      while (PAMPA_itHasMore(&it_e2n)) {
        PAMPA_Num nodenum;

        nodenum = PAMPA_itCurEnttVertNum(&it_e2n);

        // *** on flague le nœud dans nflgtax par le numéro de plan (ou l'opposé comme pour les éléments, pas besoin du 8)
        nflgtax[nodenum] = plantab[cuntnum * plannum];
        cuntnum = -cuntnum;
        PAMPA_itNext(&it_e2n);
      }
    }
    PAMPA_itNext(&it_face);
  }


  PAMPA_meshItInit(imshptr, dataptr->tetrent, dataptr->tetrent, &it_e2e);
  PAMPA_meshItInit(imshptr, dataptr->tetrent, dataptr->nodeent, &it_e2n);

  // * pour chaque élément «e» de tetrlistab
  for (tetrlisnum = 0; tetrlisnum != tetrlisnbr; tetrlisnum = (tetrlisnum + 1) % tetrlismax) {
    PAMPA_Num      elemnum;
    PAMPA_Num      plannum;
    PAMPA_Num      tettmptab[TETNBR];
    PAMPA_Num      tettmpnbr;
    PAMPA_Num      tettmpnum;

    plannum = tetrlistab[tetrlisnum % tetrlismax].plannum;
    elemnum = tetrlistab[tetrlisnum % tetrlismax].elemnum;

    tettmpnbr = 0;
    PAMPA_itStart(&it_e2e, elemnum);
    // ** pour chaque élément voisin v
    while (PAMPA_itHasMore(&it_e2e)) {
      PAMPA_Num nghbnum;

      nghbnum = PAMPA_itCurEnttVertNum(&it_e2e);
      // *** si l'élément n'a pas (dans eflgtax) la valeur opposée (op. bit) ou qu'ils sont tous les deux originaux (op. bit sur le 8)
      /* neighbor doesn't contain -plannum or plannum */
      if ((eflgtax[nghbnum] & plantab[maskidx + plannum] & plantab[0]) == 0)
        // **** alors on stocke l'élément dans tettmptab
        tettmptab[tettmpnbr ++] = nghbnum;
      // *** sinon on vérifie que l'élément actuel est compatible avec ses voisins
      else if (/* neighbor is the opposite of plannum */
          ((plantab[maskidx + plannum] & (eflgtax[nghbnum] | eflgtax[elemnum])) == plantab[maskidx + plannum])
          /* neighbor and current elements don't share a periodic face */
          && ((plantab[origidx + plannum] & eflgtax[nghbnum]) != (plantab[origidx + plannum] & eflgtax[elemnum]))) {
        // **** on arrête la boucle sur les «v» et on passe à l'élément suivant dans eflgtax
        tettmpnbr = -1;
        break;
      }
      PAMPA_itNext(&it_e2e);
    }

    /* si l'élément actuel reforme une boucle dans le maillage */
    if (tettmpnbr == -1) {
      eflgtax[elemnum] |= plantab[0]; /* Element not kept */
      continue;
    }

    // *** pour chaque élément dans tettmptab
    for (tettmpnum = 0; tettmpnum < tettmpnbr; tettmpnum ++) {
      PAMPA_Num nghbnum;

      // **** on le marque par par la même valeur de plan que e
      nghbnum = tettmptab[tettmpnum];

      // **** on l'ajoute dans tetrlistab
      eflgtax[nghbnum] |= plannum;
      tetrlistab[tetrlisnbr % tetrlismax].plannum = plannum;
      tetrlistab[tetrlisnbr % tetrlismax].elemnum = elemnum;
      memCpy (coorlistab + (tetrlisnbr % tetrlismax) * DIMNBR, coorlistab + (tetrlisnum % tetrlismax) * DIMNBR, DIMNBR * sizeof (double));
      tetrlisnbr = (tetrlisnbr + 1) % tetrlismax;
    }

    PAMPA_itStart(&it_e2n, elemnum);
    // *** pour chaque nœud
    while (PAMPA_itHasMore(&it_e2n)) {
      PAMPA_Num nodenum;
      PAMPA_Num dimnum;

      nodenum = PAMPA_itCurEnttVertNum(&it_e2n);
      // **** si nflgtax ne contient pas le plan de e
      if ((nflgtax[nodenum] & plantab[maskidx + plannum]) == 0) {
        // **** on calcule la nouvelle coordonnée avec coorpertax et on met à jour les coordonnées 
        for (dimnum = 0; dimnum < DIMNBR; dimnum ++)
          newcoortax[nodenum * DIMNBR + dimnum] += coorlistab[tetrlisnum % tetrlismax];
      }
      PAMPA_itNext(&it_e2n);
    }
  }
  // XXX fin nouvelle partie

  // * Il reste à construire le nouveau maillage en supprimant les trous,
  // autrement dit les tétraèdres KEPT
  for (elemnb2 = elemnum = baseval, elemnnd = baseval + tetrnbr; elemnum < elemnnd; elemnum ++)
    if ((eflgtax[elemnum] & plantab[0]) == 0)
      eflgtax[elemnum] = 0;
    else
      eflgtax[elemnum] = ~0;

  /* on extrait le sous-maillage, cela pourrait très bien être externalisé dans
   * un meshInduce ou est-ce déjà fait plus haut ;-)
   */
  CHECK_FDBG2 (PAMPA_meshInduceMultiple (imshptr, eflgtax + baseval, 1, omshptr));
  return (0);
}





