/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pmesh_to_rmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Jun 2016
//!                             to:   25 Sep 2017
//!
/************************************************************/

//! \brief This routine initializes the
//! mesh structure of the mmg3d remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

#include <stdio.h>
#include <mpi.h>
#include <libmmg3d.h>
#include "common.h"
#include "module.h"
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d.h"
#include "types.h"
#ifdef PAMPA_BUG_REF
#include <limits.h>
#endif /* PAMPA_BUG_REF */

#define M_REQUIRED (1 << 5) // FIXME à supprimer une fois qu'il sera visible de MMG
#define M_BDRY     (1 << 1) 

#define DIMNBR     3
  int
pmesh2rmesh (
    PAMPA_Mesh  * const         pmshptr,              //!< PaMPA centralized mesh
    PAMPA_MMG3D_Data  * const         dataptr,              //!< XXX
    PAMPA_Num     const         flagval,              //!< XXX
    MMG5_pMesh *                 mmshptr,              //!< MMG3D mesh
    MMG5_pSol *                  msolptr)              //!< MMG3D solution
{

  PAMPA_Num      baseval;
  PAMPA_Num      enttnbr;
  PAMPA_Num      nodenbr;
  PAMPA_Num      tetrnbr;
  PAMPA_Num      facenbr;
  PAMPA_Num      nodenum;
  PAMPA_Num      nodennd;
  PAMPA_Num      tetnum;
  PAMPA_Num      basedif;
  PAMPA_Num      solflg;
  PAMPA_Num    * restrict vreftax;
  PAMPA_Num    * restrict ereftax;
  PAMPA_Num    * restrict freftax;
  PAMPA_Num    * restrict freqtax;
  double       * restrict geomtax;
  double       * restrict soltax;
  PAMPA_Iterator it_nghb;
  PAMPA_Iterator it;
  MMG3D_int i;
  MMG5_pSol msoldat;
  int chekval;
  static MMG3D_int offset = 1;

  solflg = 0;
  // XXX on suppose que les nœuds du bord de la bulle soient numérotés en
  // premier pour que nous puissions les renuméroter d'abord lors du passage
  // MMG3D à PaMPA
  // XXX la remarque ci-dessus est fausse, du coup il faut modifier le code
  // FIXME
  PAMPA_meshData(pmshptr, &baseval, &enttnbr, NULL, NULL, NULL, NULL, NULL);

  PAMPA_meshValueData(pmshptr, dataptr->tetrent, PAMPA_TAG_REF, (void **) &ereftax);
  ereftax -= baseval;

  if ((flagval & PAMPA_MMG3D_FACES) != 0) {
    PAMPA_meshValueData(pmshptr, dataptr->faceent, PAMPA_TAG_REF, (void **) &freftax);
    freftax -= baseval;

    chekval = PAMPA_meshValueData(pmshptr, dataptr->faceent, PAMPA_TAG_REF, (void **) &freqtax);
    if (chekval == 0) /* If there is a value with this tag */
      freqtax -= baseval;
    else
      freqtax = NULL;
  }

  PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_GEOM, (void **) &geomtax);
  geomtax -= DIMNBR * baseval;

  PAMPA_meshEnttSize(pmshptr, dataptr->nodeent, &nodenbr);
  PAMPA_meshEnttSize(pmshptr, dataptr->tetrent, &tetrnbr);



  msoldat = NULL;
  *mmshptr = NULL;
  if (msolptr != NULL) 
    *msolptr = NULL;
  else 
    msolptr = &msoldat;
  MMG3D_Init_mesh (MMG5_ARG_start, MMG5_ARG_ppMesh, mmshptr, MMG5_ARG_ppMet, msolptr, MMG5_ARG_end);
  CHECK_FDBG2 (MMG3D_stockOptions (*mmshptr, &dataptr->infoval) != 1);
  PAMPA_meshEnttSize(pmshptr, dataptr->nodeent, &nodenbr);
  PAMPA_meshEnttSize(pmshptr, dataptr->tetrent, &tetrnbr);

  if ((flagval & PAMPA_MMG3D_FACES) != 0) 
    PAMPA_meshEnttSize(pmshptr, dataptr->faceent, &facenbr);
  else
    facenbr = 0;

  CHECK_FDBG2 (MMG3D_Set_meshSize (*mmshptr, nodenbr, tetrnbr, 0, facenbr, 0, 0) != 1);

#ifdef MSHINT 
  {
    // XXX only 3DI at the moment
    PAMPA_meshValueLink (pmshptr, (void **) &intsoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_INTSOL);
    intsoltax -= baseval;
  }
#endif /* MSHINT */

  chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
  if (chekval == 0) {  /* If there is a value with this tag */
    CHECK_FDBG2 (MMG3D_Set_solSize (*mmshptr, *msolptr, MMG5_Vertex, nodenbr, MMG5_Scalar) != 1);
    offset = 1;
  }
  else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    return (chekval);
  }
  else { /* If there no value with this tag */
    chekval = PAMPA_meshValueData(pmshptr, dataptr->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
    if (chekval == 0) {  /* If there is a value with this tag */
      CHECK_FDBG2 (MMG3D_Set_solSize (*mmshptr, *msolptr, MMG5_Vertex, nodenbr, MMG5_Scalar) != 1);
      offset = 6;
    }
    else if (chekval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      return (chekval);
    }
    else { /* If there no value with this tag */
      solflg = 1;
      //errorPrint ("No associated metric");
      //return (chekval);
    }
  }
  soltax -= offset * baseval;

#ifdef MSHINT
  {
    PAMPA_meshEnttSize (imshptr, data->nodeent, &nodenbr);
    intsoltax -= offset * baseval;
    
    // XXX pour que get_scalarsol ne renvoie pas d'erreur
    CHECK_FDBG2 (MMG5_Get_solSize(mmshptr, msolptr, &dummy, &dummy, &dummy) != 1);
    PAMPA_meshEnttSize (omshptr, data->nodeent, &nodenbr);
    
    for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++)
      CHECK_FDBG2 (MMG5_Get_scalarSol (msolptr, &intsoltax[nodenum * offset]) != 1);
    PAMPA_Num tagptr = PAMPA_TAG_INTSOL;
    CHECK_FDBG2 (infoptr->EXT_meshInt (imshptr, omshptr, infoptr, 1, &tagptr, 0)); // XXX mshint remplacer 0 par drapeau pour l'interpolateur qui devrait être stocké dans infoptr
    //{
    //  double * soltax;
    //  CHECK_FDBG2 (PAMPA_meshValueData (omshptr, data->nodeent, PAMPA_TAG_INTSOL, (void **) &soltax));
    //  soltax -= baseval;

    //  PAMPA_meshEnttSize (omshptr, data->nodeent, &nodenbr);
    //  for (int v = 0; v < nodenbr; v ++)
    //    infoPrint ("reel: %3.6f, calcule: %3.6f", msoldat.met[v+1], soltax[v]);
    //}
  }
#endif /* MSHINT */

  MMG3D_setfunc (*mmshptr, *msolptr);
  //MMG5_pampa_setfunc (*mmshptr, *msolptr); // XXX cette fonction définissait les pointeurs des fonctions lenedgeCoor, hashTetra and saveMesh.

  if ((flagval & PAMPA_MMG3D_ADAPT) != 0) {
    PAMPA_meshValueData(pmshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &vreftax);
    vreftax -= baseval;
  }


  basedif = 1 - baseval;

  PAMPA_meshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
  PAMPA_meshItInitStart(pmshptr, dataptr->tetrent, &it);
  i = 4;
  while (PAMPA_itHasMore(&it)) {
    MMG3D_int v[4];

    PAMPA_Num mtetnum;
    tetnum = PAMPA_itCurEnttVertNum(&it);
    mtetnum = PAMPA_itCurMeshVertNum(&it);

    if (((flagval & PAMPA_MMG3D_ADAPT) != 0) && (vreftax[mtetnum] != PAMPA_TAG_VERT_INTERNAL)) { /* if at the frontier */
      CHECK_FDBG2 (MMG3D_Set_requiredTetrahedron (*mmshptr, tetnum + basedif) != 1);
    }
    i = 0;
    PAMPA_itStart(&it_nghb, tetnum);

    while (PAMPA_itHasMore(&it_nghb)) {
      PAMPA_Num mnodnum;

      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
      mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
      if (((flagval & PAMPA_MMG3D_ADAPT) != 0) && (vreftax[mnodnum] != PAMPA_TAG_VERT_INTERNAL)) { /* if at the frontier */
#ifdef PAMPA_DEBUG_ADAPT2
        if ((mnodnum + 1) < 0) {
          errorPrint ("Invalid ref: %d\n", mnodnum + 1);
          return (1);
        }
#endif /* PAMPA_DEBUG_ADAPT2 */
#define PAMPA_BUG_REF /* en attendant que les noeuds soient gérés directement dans PaMPA car pour le moment, les noeuds dans mmg sont ajoutés avec une ref qui est dépendante des refs des triangles (ref + 10) */
#ifdef PAMPA_BUG_REF
        mnodnum += PAMPA_BUG_REF_VAL;
#endif /* PAMPA_BUG_REF */
        CHECK_FDBG2 (MMG3D_Set_vertex (*mmshptr, geomtax[nodenum * DIMNBR], geomtax[nodenum * DIMNBR + 1], geomtax[nodenum * DIMNBR + 2], mnodnum + 1, nodenum + basedif) != 1); /* TRICK: mnodnum + 1 to have 0 as default value */
        CHECK_FDBG2 (MMG3D_Set_requiredVertex (*mmshptr, nodenum + basedif) != 1);
      }
      else {
        CHECK_FDBG2 (MMG3D_Set_vertex (*mmshptr, geomtax[nodenum * DIMNBR], geomtax[nodenum * DIMNBR + 1], geomtax[nodenum * DIMNBR + 2], 0, nodenum + basedif) != 1);
      }
      PAMPA_itNext(&it_nghb);
      v[i ++] = (MMG3D_int) (nodenum + basedif);
    }
#ifdef PAMPA_DEBUG_ADAPT
    if (i != 4) {
      errorPrint ("Element %d doesn't have 4 nodes", mtetnum);
      return (1);
    }
    if ((v[0] == 0) && (v[1] == 0) && (v[2] == 0) && (v[3] == 0)) {
      errorPrint ("Internal error");
      return (1);
    }
    if ((v[0] == v[1] == v[2] == v[3])) {
      errorPrint ("Internal error");
      return (1);
    }
#endif /* PAMPA_DEBUG_ADAPT */
    CHECK_FDBG2 (MMG3D_Set_tetrahedron (*mmshptr, v[0], v[1], v[2], v[3], (int) ereftax[tetnum], tetnum + basedif) != 1); // XXX ref = 0 ?
    PAMPA_itNext(&it);
  }


  if ((flagval & PAMPA_MMG3D_FACES) != 0) {
    PAMPA_meshItInit(pmshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
    PAMPA_meshItInitStart(pmshptr, dataptr->faceent, &it);
    while (PAMPA_itHasMore(&it)) {
      MMG3D_int v[3];
      MMG3D_int i;
      PAMPA_Num mtrinum;
      PAMPA_Num trianum;

      i = 0;
      trianum = PAMPA_itCurEnttVertNum(&it);
      mtrinum = PAMPA_itCurMeshVertNum(&it);

      PAMPA_itStart(&it_nghb, trianum);

      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num mnodnum;

        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
        mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
        v[i ++] = (MMG3D_int) (nodenum + basedif);
        PAMPA_itNext(&it_nghb);
      }
      PAMPA_itNext(&it);
#ifdef PAMPA_DEBUG_ADAPT
      if (i != 3) {
        errorPrint ("Triangle %d doesn't have 3 nodes", mtrinum);
        return (1);
      }
#endif /* PAMPA_DEBUG_ADAPT */
      if ((flagval & PAMPA_MMG3D_ADAPT) != 0) {
        CHECK_FDBG2 (MMG3D_Set_triangle (*mmshptr, v[0], v[1], v[2], (int) freftax[trianum] + 11, trianum + basedif) != 1); // XXX ref = 0 ?
      }
      else {
        CHECK_FDBG2 (MMG3D_Set_triangle (*mmshptr, v[0], v[1], v[2], (int) freftax[trianum], trianum + basedif) != 1); // XXX ref = 0 ?
      }
      if (((flagval & PAMPA_MMG3D_ADAPT) != 0) && (vreftax[mtrinum] != PAMPA_TAG_VERT_INTERNAL)) { /* if at the frontier */
        CHECK_FDBG2 (MMG3D_Set_requiredTriangle (*mmshptr, trianum + basedif) != 1);
      }

      if ((freqtax != NULL) && (freqtax[trianum] != 0)) {
        CHECK_FDBG2 (MMG3D_Set_requiredTriangle (*mmshptr, trianum + basedif) != 1);
      }
    }
  }

  if ((*msolptr != NULL) && (solflg == 0)) {
    for(nodenum = baseval, nodennd = baseval + nodenbr ; nodenum < nodennd ; nodenum ++) {
      if (offset == 1) {
        CHECK_FDBG2 (MMG3D_Set_scalarSol (*msolptr, soltax[nodenum * offset], nodenum + basedif) != 1);
      }
      else { /* offset = 6 */
        errorPrint ("Not yet implemented");
        return (1);
      }
    }
  }

  return (0);
}
