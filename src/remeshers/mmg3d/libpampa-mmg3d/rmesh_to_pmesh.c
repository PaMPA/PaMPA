/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        rmesh_to_pmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include "pampa-mmg3d.h"
#include "types.h"
#include "rmesh_to_pmesh.h"
#include <scotch.h>

static const unsigned char MMG5_idir[4][3] = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1} }; // XXX à changer temporaire (on fait des suppositions sur le remailleur, pas bien)


//! \brief This routine initializes the
//! mesh structure of the mmg3d remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
rmesh2pmesh (
MMG5_pMesh     const         mmshptr,              //!< MMG3D mesh
MMG5_pSol      const         msolptr,              //!< MMG3D solution
PAMPA_MMG3D_Data  * const         dataptr,              //!< XXX
PAMPA_Num     const         baseval,              //!< PaMPA baseval
PAMPA_Num     const         flagval,              //!< If adaptation
PAMPA_Mesh  * const         pmshptr)              //!< PaMPA centralized mesh
{
  PAMPA_Num 	basedif;
  PAMPA_Num 	nodebas;
  PAMPA_Num 	tetrbas;
  PAMPA_Num 	facebas;
  PAMPA_Num     enttnbr;
  PAMPA_Num     mfacidx;
  PAMPA_Num  mfacnm2;
  PAMPA_Num     mfacnnd;
  MMG3D_int    nodenbr;
  MMG3D_int    tetrnbr;
  MMG3D_int    trianbr;
  PAMPA_Num 	vertnum;
  PAMPA_Num 	vertnnd;
  PAMPA_Num 	vertnbr;
  PAMPA_Num 	edgenbr;
  PAMPA_Num 	edgesiz;
  PAMPA_Num  facenbr;
  PAMPA_Num * restrict nnbrtax;
  PAMPA_Num * restrict verttax;
  PAMPA_Num * restrict vendtax;
  PAMPA_Num * restrict venttax;
  PAMPA_Num * restrict edgetax;
  PAMPA_Num * restrict ereftax;
  PAMPA_Num * restrict nreftax;
  PAMPA_Num * restrict freftax;
  PAMPA_Num * restrict freqtax;
  PAMPA_Num * restrict vflgtax;
  double *    restrict geomtax;
  faces *     restrict facetab;
  MMG3D_int 			nodeind;
  MMG3D_int 			nodennd;
  MMG3D_int 			mtetnum;
  MMG3D_int 			mtetnnd;
  int 			chekval = 0;
  int dummy; // XXX temporaire

  basedif = baseval - 1;

  CHECK_FDBG2 (MMG3D_Get_meshSize (mmshptr, &nodenbr, &tetrnbr, NULL, &trianbr, NULL, NULL) != 1);

  //  for (nodeind = 1, nodennd = mmshptr->np + 1 ; nodeind < nodennd ; nodeind ++) 
  //	if ((mmshptr->point[nodeind].tag & M_BDRY) != 0)
  //	  printf ("nœud sur l'enveloppe : %d\n", nodeind);

  //return(-1);
  //edgesiz = 2 * mmshptr->np + 4 * mmshptr->ne;
  edgenbr = baseval;

  tetrbas = basedif;
  facebas = tetrbas + (PAMPA_Num) tetrnbr;
  if ((flagval & PAMPA_MMG3D_FACES) != 0) {
    for (facenbr = mfacidx = 1, mfacnnd = (PAMPA_Num) (trianbr + 1); mfacidx < mfacnnd; mfacidx ++) {
      MMG3D_int   ref;
      MMG3D_int v[3];
      //if ((mmshptr->tria[mfacidx].ref != 0) && (mmshptr->tria[mfacidx].ref != 10)) { /* 10 c'est temporaire */
      CHECK_FDBG2 (MMG3D_Get_triangle (mmshptr, v, v + 1, v + 2, &ref, NULL) != 1);
      if (((flagval & PAMPA_MMG3D_ADAPT) == 0) || (ref > 10)) {
        facenbr ++;
      }
    }
    facenbr --;

    vertnbr = (PAMPA_Num) (nodenbr + tetrnbr + facenbr);
    nodebas = facebas + (PAMPA_Num) facenbr;
    enttnbr = 3;

  }
  else {
    vertnbr = (PAMPA_Num) (nodenbr + tetrnbr);
    nodebas = tetrbas + (PAMPA_Num) tetrnbr;
    enttnbr = 2;
  }


  if (memAllocGroup ((void **) (void *)
        &venttax,     (size_t) (vertnbr * sizeof (PAMPA_Num)),
        &verttax,     (size_t) (vertnbr * sizeof (PAMPA_Num)),
        &vendtax,     (size_t) (vertnbr * sizeof (PAMPA_Num)),
        &nnbrtax,     (size_t) (vertnbr * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }
  venttax -= baseval;
  verttax -= baseval;
  vendtax -= baseval;
  nnbrtax -= baseval;

  //memSet (venttax + baseval, PAMPA_ENTT_DUMMY, vertnbr * sizeof (PAMPA_Num));
  memSet (nnbrtax + baseval, 0, vertnbr * sizeof (PAMPA_Num));
  // XXX la ligne suivante permettrait (si elle est couplée à une vérif a priori
  // non présente dans meshBuild) de vérifier que chaque sommet ajouté est
  // bien utilisé. Pour le moment, les nœuds sont mis à jour via les éléments.
  // Du coup, si un nœud n'est relié à aucun élément, il n'aura pas de numéro
  // d'entité.
  //memSet (venttax + baseval, ~0, vertnbr * sizeof (PAMPA_Num)); // XXX vérifier en débug que entt_dummy est bien ~0
  CHECK_FDBG2 (MMG3D_Get_meshSize (mmshptr, &nodenbr, &tetrnbr, NULL, &trianbr, NULL, NULL) != 1);
  for (mtetnum = 1, mtetnnd = tetrnbr + 1 ; mtetnum < mtetnnd ; mtetnum ++) {
    MMG3D_int v[4];
    PAMPA_Num	ptetnum;
    MMG3D_int			nodeind;

    // if (!mmshptr->tetra[mtetnum].v[0]) continue; /* If the tetra doesn't exist */

    ptetnum = (PAMPA_Num) mtetnum + tetrbas;
    nnbrtax[ptetnum] += 8; /* 4 for nodes and 4 for elements or boundary faces */ // FIXME et si les faces frontières sont entre deux éléments de natures différentes ???
    CHECK_FDBG2 (MMG3D_Get_tetrahedron (mmshptr, v, v + 1, v + 2, v + 3, &dummy, &dummy) != 1);

    for (nodeind = 0 ; nodeind < 4 ; nodeind++) { // nodeind is a node
      PAMPA_Num pnodnum;

      pnodnum = (PAMPA_Num) (v[nodeind]) + nodebas;
#ifdef PAMPA_DEBUG_ADAPT
      if ((pnodnum < baseval) || (pnodnum >= vertnbr)) {
        errorPrint ("internal error");
        return (1);
      }
#endif /* PAMPA_DEBUG_ADAPT */
      nnbrtax[pnodnum] ++; // 1 for tetra
      if ((flagval & PAMPA_MMG3D_NODE2NODE) != 0)
        nnbrtax[pnodnum] += 3; // 3 for nodes
    }
  }

  if ((flagval & PAMPA_MMG3D_FACES) != 0) {
    // XXX pour que get_triangle ne renvoie pas d'erreur
    CHECK_FDBG2 (MMG3D_Get_meshSize (mmshptr, NULL, NULL, NULL, NULL, NULL, NULL) != 1);
    if ((facetab = (faces *) memAlloc ((facenbr + 1) * sizeof (faces))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }
    //memSet (facetab, ~0, (facenbr + 1) * sizeof (faces));

    for (mfacnm2 = mfacidx = 1, mfacnnd = (PAMPA_Num) (mmshptr->nt + 1); mfacidx < mfacnnd; mfacidx ++) {
      PAMPA_Num pfacnum;
      MMG3D_int   nodeind;
      MMG3D_int   v[3];
      MMG3D_int   ref;
      MMG3D_int   requflg;

      CHECK_FDBG2 (MMG3D_Get_triangle (mmshptr, v, v + 1, v + 2, &ref, &requflg) != 1);
      //if ((mmshptr->tria[mfacidx].ref != 0) && (mmshptr->tria[mfacidx].ref != 10)) { /* 10 c'est temporaire */
      if (((flagval & PAMPA_MMG3D_ADAPT) == 0) || (ref > 10)) {
        pfacnum = (PAMPA_Num) mfacnm2 + facebas;
        facetab[mfacnm2].refval = (PAMPA_Num) ref;
        facetab[mfacnm2].requflg = (PAMPA_Num) requflg;
        nnbrtax[pfacnum] += 5; /* 3 for nodes and 2 for elements  */

        for (nodeind = 0 ; nodeind < 3 ; nodeind++) { // nodeind is a node
          MMG3D_int   pt;
          PAMPA_Num pnodnum;

          pt = mmshptr->tria[mfacidx].v[nodeind];
          facetab[mfacnm2].v[nodeind] = (PAMPA_Num) pt;

          pnodnum = (PAMPA_Num) (pt) + nodebas;
          nnbrtax[pnodnum] ++;
        }
        _PAMPAintSort1asc1 (facetab[mfacnm2].v, 3);
        mfacnm2 ++;
      }
    }


    /* Sorting faces */
    _PAMPAintSort5asc3 ((PAMPA_Num *) (facetab + 1), facenbr); // XX pourquoi + 1 ?
  }

  // XXX faire une boucle sur ts les sommets pour mettre à jour verttax et
  // vendtax
  // en déduire edgesiz
  verttax[baseval] = baseval;
  vendtax[baseval] = baseval;
  for (vertnum = baseval + 1, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++)
    vendtax[vertnum] =
      verttax[vertnum] = verttax[vertnum - 1] + nnbrtax[vertnum - 1];

  edgesiz = vendtax[vertnum - 1] + nnbrtax[vertnum - 1];

  if ((edgetax = (PAMPA_Num *) memAlloc (edgesiz * sizeof (PAMPA_Num))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }
  edgetax -= baseval;

  // XXX pour que get_tetra ne renvoie pas d'erreur
  CHECK_FDBG2 (MMG3D_Get_meshSize (mmshptr, NULL, NULL, NULL, NULL, NULL, NULL) != 1);

  for (edgenbr = 0, mtetnum = 1, mtetnnd = (PAMPA_Num) tetrnbr + 1 ; mtetnum < mtetnnd ; mtetnum ++) {
    PAMPA_Num	ptetnum;
    MMG3D_int v[4];
    MMG3D_int adja[4];
    MMG3D_int			ind;

    // if (!mmshptr->tetra[mtetnum].v[0]) continue; /* If the tetra doesn't exist */

    ptetnum = (PAMPA_Num) mtetnum + tetrbas;

    venttax[ptetnum] = dataptr->tetrent;

    CHECK_FDBG2 (MMG3D_Get_adjaTet (mmshptr, mtetnum, adja) != 1);

    for (ind = 0 ; ind < 4 ; ind ++) {
      MMG3D_int	ngbnum; // ngbnum is a tetrahedron

      ngbnum = adja[ind];

//#ifdef PAMPA_NOT_REF_ONE
//      if ((ngbnum == 0) || ((mmshptr->tetra[mtetnum].ref != mmshptr->tetra[ngbnum].ref) && (mmshptr->tetra[mtetnum].ref != 1) && (mmshptr->tetra[ngbnum].ref != 1))) { /* If the neighbor doesn't exist */
//#else /* PAMPA_NOT_REF_ONE */
      if ((ngbnum == 0) || (mmshptr->tetra[mtetnum].ref != mmshptr->tetra[ngbnum].ref)) { /* If the neighbor doesn't exist */
//#endif /* PAMPA_NOT_REF_ONE */
        if ((flagval & PAMPA_MMG3D_FACES) != 0) {
          PAMPA_Num pfacnum;
          MMG3D_int mfacidx;
          MMG3D_int mfacmax;
          PAMPA_Num v[3];

          v[0] = (PAMPA_Num) mmshptr->tetra[mtetnum].v[MMG5_idir[ind][0]];
          v[1] = (PAMPA_Num) mmshptr->tetra[mtetnum].v[MMG5_idir[ind][1]];
          v[2] = (PAMPA_Num) mmshptr->tetra[mtetnum].v[MMG5_idir[ind][2]];
          _PAMPAintSort1asc1 (v, 3);

          /* Search the processor which have current neighbor */
          for (mfacidx = 1, mfacmax = facenbr + 1;
              mfacmax - mfacidx > 1; ) {
            MMG3D_int                 mfacmed;

            mfacmed = (mfacmax + mfacidx) / 2;
            if ((facetab[mfacmed].v[0] < v[0]) || 
                ((facetab[mfacmed].v[0] == v[0]) && (facetab[mfacmed].v[1] < v[1])) ||
                ((facetab[mfacmed].v[0] == v[0]) && (facetab[mfacmed].v[1] == v[1]) && (facetab[mfacmed].v[2] <= v[2])))
              mfacidx = mfacmed;
            else
              mfacmax = mfacmed;
          }
//#if (defined PAMPA_DEBUG_ADAPT) && (! defined PAMPA_NOT_REF_ONE)
//          if ((facetab[mfacidx].v[0] != v[0]) || (facetab[mfacidx].v[1] != v[1]) || (facetab[mfacidx].v[2] != v[2])) {
//            errorPrint ("face (%d,%d,%d) not found", v[0], v[1], v[2]);
//            return (1);
//          }
//#endif /* (defined PAMPA_DEBUG_ADAPT) && (! defined PAMPA_NOT_REF_ONE) */
//#ifdef PAMPA_NOT_REF_ONE
          if ((mfacidx <= facenbr) && (facetab[mfacidx].v[0] == v[0]) && (facetab[mfacidx].v[1] == v[1]) && (facetab[mfacidx].v[2] == v[2])) {
//#endif /* PAMPA_NOT_REF_ONE */
            pfacnum = (PAMPA_Num) mfacidx + facebas;
            edgetax[vendtax[ptetnum] ++] = pfacnum;
            edgetax[vendtax[pfacnum] ++] = ptetnum;
            edgenbr += 2;
//#ifdef PAMPA_NOT_REF_ONE
          }
//#endif /* PAMPA_NOT_REF_ONE */
        }
      }
      if (ngbnum != 0) { /* If the neighbor doesn't exist */
        edgetax[vendtax[ptetnum] ++] = ngbnum + tetrbas;
        edgenbr ++;
      }
    }

    CHECK_FDBG2 (MMG3D_Get_tetrahedron (mmshptr, v, v + 1, v + 2, v + 3, &dummy, &dummy) != 1);

    for (nodeind = 0 ; nodeind < 4 ; nodeind ++) {
      MMG3D_int 		pt;
      MMG3D_int 		nodeind2;
      PAMPA_Num pnodnum;

      pt = v[nodeind];

      pnodnum = (PAMPA_Num) (pt) + nodebas;

      venttax[pnodnum] = dataptr->nodeent; // XXX on peut le faire plusieurs fois pour un seul nœud, inutile TODO
      edgetax[vendtax[ptetnum] ++] = pnodnum;
      edgetax[vendtax[pnodnum] ++] = ptetnum;
      edgenbr += 2;


      // pour chaque nœud du tétra sauf le nœud lui-même
      if ((flagval & PAMPA_MMG3D_NODE2NODE) != 0)
        for (nodeind2 = 1 ; nodeind2 < 4 ; nodeind2++) {
          PAMPA_Num pnodnm2;
          PAMPA_Num edgeidx;
          PAMPA_Num edgennd;

          pt = v[(nodeind + nodeind2) % 4];

          pnodnm2 = (PAMPA_Num) (pt) + nodebas;
          for (edgeidx = verttax[pnodnum], edgennd = vendtax[pnodnum]; edgeidx < edgennd && edgetax[edgeidx] != pnodnm2; edgeidx ++); /* If already added */
          if ((edgeidx == vendtax[pnodnum]) || (edgetax[edgeidx] != pnodnm2)) {
            edgetax[vendtax[pnodnum] ++] = pnodnm2;
            edgenbr ++;
          }
        }
    }
  }

  if ((flagval & PAMPA_MMG3D_FACES) != 0)
    for (mfacidx = 1, mfacnnd = facenbr + 1 ; mfacidx < mfacnnd ; mfacidx ++) {
      PAMPA_Num pfacnum;

      pfacnum = (PAMPA_Num) mfacidx + facebas;

      venttax[pfacnum] = dataptr->faceent;


      for (nodeind = 0 ; nodeind < 3 ; nodeind ++) {
        MMG3D_int   pt;
        PAMPA_Num pnodnum;

        pt = facetab[mfacidx].v[nodeind];

        pnodnum = (PAMPA_Num) (pt) + nodebas;

        // XXX ATTENTION prévoir si la face est entre deux éléments de ref différentes
        edgetax[vendtax[pfacnum] ++] = pnodnum;
        edgetax[vendtax[pnodnum] ++] = pfacnum;
        edgenbr += 2;


      }
    }

  CHECK_FDBG2 (PAMPA_meshBuild( pmshptr, baseval, vertnbr, verttax + baseval, vendtax + baseval, NULL, edgenbr,
        edgesiz, edgetax + baseval, NULL, enttnbr, venttax + baseval, NULL, NULL, 50)); // XXX pourquoi 50 ?



  memFreeGroup (venttax + baseval);
  memFree (edgetax + baseval);

  if (chekval != 0)
    return chekval;
  chekval = PAMPA_meshValueLink (pmshptr, (void **) &geomtax, PAMPA_VALUE_PUBLIC, dataptr->coortyp, dataptr->nodeent, PAMPA_TAG_GEOM);
  if (chekval != 0)
    return chekval;
  geomtax -= 3 * baseval;

  chekval = PAMPA_meshValueLink (pmshptr, (void **) &ereftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->tetrent, PAMPA_TAG_REF);
  if (chekval != 0)
    return chekval;
  ereftax -= baseval;

  if ((flagval & PAMPA_MMG3D_ADAPT) == 0) {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &nreftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->nodeent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    nreftax -= baseval;
    //memSet (nreftax + baseval, 0, nodenbr * sizeof (PAMPA_Num)); // nodenbr ???

  }
  else {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &vflgtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS);
    if (chekval != 0)
      return chekval;
    vflgtax -= baseval;

  }

  if ((flagval & PAMPA_MMG3D_FACES) != 0) {
    chekval = PAMPA_meshValueLink (pmshptr, (void **) &freftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_REF);
    if (chekval != 0)
      return chekval;
    freftax -= baseval;

    chekval = PAMPA_meshValueLink (pmshptr, (void **) &freqtax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_REQU);
    if (chekval != 0)
      return chekval;
    freqtax -= baseval;
    memSet (freqtax + baseval, 0, facenbr * sizeof (PAMPA_Num));
  }


  // XXX pour que get_tetra ne renvoie pas d'erreur
  CHECK_FDBG2 (MMG3D_Get_meshSize (mmshptr, NULL, NULL, NULL, NULL, NULL, NULL) != 1);

  for (mtetnum = 1, mtetnnd = tetrnbr + 1 ; mtetnum < mtetnnd ; mtetnum ++) {
    MMG3D_int ref;
    MMG3D_int v[4];
    PAMPA_Num	ptetnum;

    // if (!mmshptr->tetra[mtetnum].v[0]) continue; /* If the tetra doesn't exist */

    ptetnum = (PAMPA_Num) mtetnum + tetrbas;
    CHECK_FDBG2 (MMG3D_Get_tetrahedron (mmshptr, v, v + 1, v + 2, v + 3, &ref, &dummy) != 1);
    ereftax[ptetnum] = ref;
  }

  for (nodeind = 1, nodennd = nodenbr + 1 ; nodeind < nodennd ; nodeind ++) {
    double c[3];
    MMG5_Point pt;
    MMG3D_int tag;
    MMG3D_int ref;
    MMG3D_int i;

    CHECK_FDBG2 (MMG3D_Get_vertex (mmshptr, c, c + 1, c + 2, &ref, &dummy, &tag) != 1);

    if ((flagval & PAMPA_MMG3D_ADAPT) == 0)
      nreftax[(PAMPA_Num) (nodeind) + basedif] = ref;
    else {
#ifdef PAMPA_DEBUG_ADAPT2
      if (ref < 0) {
        errorPrint ("invalid ref: %d", ref);
        return (1);
      }
#endif /* PAMPA_DEBUG_ADAPT2 */
      vflgtax[nodeind + nodebas] = (ref == 0) ? PAMPA_TAG_VERT_INTERNAL : ref - 1; /* TRICK: - 1 to have 0 as default value */
#define PAMPA_BUG_REF /* en attendant que les noeuds soient gérés directement dans PaMPA car pour le moment, les noeuds dans mmg sont ajoutés avec une ref qui est dépendante des refs des triangles (ref + 10) */
#ifdef PAMPA_BUG_REF
      if (vflgtax[nodeind + nodebas] >= PAMPA_BUG_REF_VAL)
        vflgtax[nodeind + nodebas] = ref - PAMPA_BUG_REF_VAL - 1;
      else
        vflgtax[nodeind + nodebas] = PAMPA_TAG_VERT_INTERNAL;
#endif /* PAMPA_BUG_REF */
    }


    // XXX utiliser plutot memCpy et optimiser pour le basedif
    for (i = 0 ; i < 3 ; i++)
      geomtax[((PAMPA_Num) (nodeind) + basedif) * 3 + i] = c[i];
  }

  if ((flagval & PAMPA_MMG3D_FACES) != 0) {
    for (mfacidx = 1, mfacnnd = facenbr + 1; mfacidx < mfacnnd; mfacidx ++) {
      freftax[(PAMPA_Num) (mfacidx) + basedif] = facetab[mfacidx].refval;
      if ((flagval & PAMPA_MMG3D_ADAPT) == 0) {
        freqtax[(PAMPA_Num) (mfacidx) + basedif] = facetab[mfacidx].requflg;
      }
      else
        freftax[mfacidx + basedif] -= 11;
    }
    memFree (facetab);
  }


#ifdef PAMPA_DEBUG_ADAPT2
  {
    PAMPA_Iterator it_nghb;
    PAMPA_Iterator it;
    MMG3D_int i;
    PAMPA_meshItInit(pmshptr, dataptr->tetrent, dataptr->nodeent, &it_nghb);
    PAMPA_meshItInitStart(pmshptr, dataptr->tetrent, &it);
    i = 4;
    while (PAMPA_itHasMore(&it)) {

      PAMPA_Num mtetnum;
      PAMPA_Num tetnum;
      tetnum = PAMPA_itCurEnttVertNum(&it);
      mtetnum = PAMPA_itCurMeshVertNum(&it);

      i = 0;
      PAMPA_itStart(&it_nghb, tetnum);

      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num mnodnum;
        PAMPA_Num nodnum;
        nodnum = PAMPA_itCurEnttVertNum(&it_nghb);
        mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);

        i ++;
        PAMPA_itNext(&it_nghb);
      }
      // FIXME test debug
      if (i != 4) {
        errorPrint ("Element %d doesn't have 4 nodes", mtetnum);
        return (1);
      }
      PAMPA_itNext(&it);
    }
  }
#endif /* PAMPA_DEBUG_ADAPT2 */

  // XXX pas très clair ce qu'il faut faire
  //if ((flagval & PAMPA_MMG3D_PERIODIC) != 0) {
  //  meshVert * hashtab;
  //  PAMPA_Num  hashsiz;          /* Size of hash table    */
  //  PAMPA_Num  hashmax;
  //  PAMPA_Num  hashnbr;
  //  PAMPA_Num  hashmsk;
  //  PAMPA_Num * restrict * restrict nodeflagtax;
  //  PAMPA_Num * restrict * restrict faceflagtax;
  //  PAMPA_Iterator it;
  //  PAMPA_Iterator it_nghb;

  //  const double coorval[3][2] = {{0,0.2},{0.1,0.4},{-0.1,0.1}};
  //  //const double xcoorval[2] = {0,0.2};
  //  //const double ycoorval[2] = {0.1,0.4};
  //  //const double zcoorval[2] = {-0.1,0.1};

  //  // XXX début nouvel algo
  //  // * est-on au bon endroit pour modifier pmshptr ?
  //  // * ne faut-il pas le faire avant le meshbuild ?
  //  // * ou même dans mesh_adapt.c
  //  // * pour chaque face 
  //  // XXX fin nouvel algo
  //  if (memAllocGroup ((void **) (void *)
  //        &nodeflagtax, (size_t) (3 * sizeof (PAMPA_Num *)),
  //        &faceflagtax, (size_t) (3 * sizeof (PAMPA_Num *)),
  //        NULL) == NULL) {
  //    errorPrint("out of memory");
  //    return (1);
  //  }

  //  hashnbr = MAX (nodenbr / 10, 10);
  //  for (hashsiz = 256; hashsiz < hashnbr; hashsiz <<= 1) ; /* Get upper power of two */
  //  hashnbr = 0;
  //  hashmsk = hashsiz - 1;
  //  hashmax = hashsiz >> 2;

  //  if ((hashtab = (meshVert *) memAlloc (hashsiz * sizeof (meshVert))) == NULL) {
  //    errorPrint  ("out of memory");
  //    return      (1);
  //  }
  //  
  //  for (int i = 0; i < 3; i++) {
  //    CHECK_FDBG2 (PAMPA_meshValueLink (pmshptr, (void **) nodeflagtax + i, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->nodeent, PAMPA_TAG_PER_1 - i));
  //    nodeflagtax[i] -= baseval;
  //    memSet (nodeflagtax[i] + baseval, ~0, nodenbr * sizeof (PAMPA_Num));

  //    CHECK_FDBG2 (PAMPA_meshValueLink (pmshptr, (void **) faceflagtax + i, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, dataptr->faceent, PAMPA_TAG_PER_1 - i));
  //    faceflagtax[i] -= baseval;
  //    memSet (faceflagtax[i] + baseval, ~0, facenbr * sizeof (PAMPA_Num));
  //  }

  //  PAMPA_meshItInitStart(pmshptr, dataptr->nodeent, &it);
  //  while (PAMPA_itHasMore(&it)) {
  //    PAMPA_Num nodenum;
  //    PAMPA_Num nodenm2;
  //    double    coortab[3];

  //    nodenum = PAMPA_itCurEnttVertNum(&it);

  //    for (int i = 0; i < 3; i ++) {
  //      if ((geomtax[nodenum * 3 + i] == coorval[i][0]) || (geomtax[nodenum * 3 + i] == coorval[i][1])) {
  //        memCpy (coortab, geomtax + (nodenum * 3), 3 * sizeof (double)); // XXX 3 ?
  //        chekval = rmeshHashtableToggleElem (&hashtab, &hashsiz, &hashmax, &hashnbr, &hashmsk, coortab, nodenum, &nodenm2);
  //        if (chekval == 1) { /* already inserted */
  //          nodeflagtax[i][nodenum] = nodenm2;
  //          nodeflagtax[i][nodenm2] = nodenum;
  //        }
  //        else if (chekval != 0) { /* error */
  //          return (chekval);
  //        }
  //      }
  //    }
  //  }

  //  // XXX il faut vider la table
  //  memSet (hashtab, ~0, hashsiz * sizeof (meshVert));
  //  hashnbr = 0;

  //  PAMPA_meshItInitStart(pmshptr, dataptr->faceent, &it);
  //  PAMPA_meshItInit(pmshptr, dataptr->faceent, dataptr->nodeent, &it_nghb);
  //  while (PAMPA_itHasMore(&it)) {
  //    PAMPA_Num facenum;
  //    PAMPA_Num facenm2;
  //    double    coortab[3]; // XXX pourquoi 3 ?

  //    facenum = PAMPA_itCurEnttVertNum(&it);

  //    coortab[0] =
  //    coortab[1] =
  //    coortab[2] = 0.0;

  //    PAMPA_itStart(&it_nghb, facenum);

  //    while (PAMPA_itHasMore(&it_nghb)) {
  //      PAMPA_Num nodenum;
  //      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);

  //      coortab[0] += geomtax[nodenum * 3];
  //      coortab[1] += geomtax[nodenum * 3 + 1];
  //      coortab[2] += geomtax[nodenum * 3 + 2];
  //      PAMPA_itNext(&it_nghb);
  //    }
  //    coortab[0] /= 3;
  //    coortab[1] /= 3;
  //    coortab[2] /= 3;

  //    for (int i = 0; i < 3; i ++) {
  //      if ((coortab[i] == coorval[i][0]) || (coortab[i] == coorval[i][1])) {
  //        chekval = rmeshHashtableToggleElem (&hashtab, &hashsiz, &hashmax, &hashnbr, &hashmsk, coortab, facenum, &facenm2);
  //        if (chekval == 1) { /* already inserted */
  //          faceflagtax[i][facenum] = facenm2;
  //          faceflagtax[i][facenm2] = facenum;
  //        }
  //        else if (chekval != 0) { /* error */
  //          return (chekval);
  //        }
  //      }
  //    }

  //  }
  //}

  return 0;
}

int
rmeshHashtableToggleElem (
meshVert ** hashtab,
PAMPA_Num * hashsiz,
PAMPA_Num * hashmax,
PAMPA_Num * hashnbr,
PAMPA_Num * hashmsk,
double    * coortab,
PAMPA_Num   vertnum,
PAMPA_Num * vertnm2)
{
  PAMPA_Num hashnum;
  PAMPA_Num coorsum;
  double    coorsm2;
  int       i;

  for (coorsm2 = i = 0; i < 3; i ++) 
    coorsm2 += coortab[i];
  if (fabs(coorsm2) != coorsm2)
    coorsm2 = - coorsm2;
  if (coorsm2 != 0)
    for (; coorsm2 < 100000; coorsm2 *= 10);
  coorsum = (PAMPA_Num) coorsm2;

  for (hashnum = (coorsum * MESHVERTHASHPRIME) & (*hashmsk); (*hashtab)[hashnum].coorsum != ~0 && ((*hashtab)[hashnum].coorsum != coorsum || (*hashtab)[hashnum].coortab[0] != coortab[0] || (*hashtab)[hashnum].coortab[1] != coortab[1] || (*hashtab)[hashnum].coortab[2] != coortab[2]); hashnum = (hashnum + 1) & (*hashmsk)) ;

  if (((*hashtab)[hashnum].coorsum != coorsum) || ((*hashtab)[hashnum].coortab[0] != coortab[0]) || ((*hashtab)[hashnum].coortab[1] != coortab[1]) || ((*hashtab)[hashnum].coortab[2] != coortab[2])) { /* Vertex not already added */

    (*hashtab)[hashnum].coorsum    = coorsum;
    (*hashtab)[hashnum].coortab[0] = coortab[0];
    (*hashtab)[hashnum].coortab[1] = coortab[1];
    (*hashtab)[hashnum].coortab[2] = coortab[2];
    (*hashtab)[hashnum].vertnum    = vertnum;
    hashnbr ++;

    if (hashnbr >= hashmax) /* If hashtab is too much filled */
      CHECK_FDBG2 (meshVertResize (&hashtab, &hashsiz, &hashmax, &hashmsk));
  }
  else { /* Vertex already added */
    PAMPA_Num hashidx;

    *vertnm2 = (*hashtab)[hashnum].vertnum;
    (*hashtab)[hashnum].coorsum = ~0;
    // XXX TODO IMPORTANT décaler les éléments qui suivent le supprimé
    hashnbr --;

    for (hashidx = (hashnum + 1) & (*hashmsk); (*hashtab)[hashidx].coorsum != ~0; hashidx = (hashidx + 1) & (*hashmsk)) {
      PAMPA_Num coorsum;
      double    coortab[3];

      coorsum = (*hashtab)[hashidx].coorsum;
      for (int i = 0 ; i < 3 ; i ++)
        coortab[i] = (*hashtab)[hashidx].coortab[i];

      for (hashnum = (coorsum * MESHVERTHASHPRIME) & (*hashmsk);
          (*hashtab)[hashnum].coorsum != ~0 &&
          ((*hashtab)[hashnum].coorsum != coorsum
           || (*hashtab)[hashnum].coortab[0] != coortab[0]
           || (*hashtab)[hashnum].coortab[1] != coortab[1]
           || (*hashtab)[hashnum].coortab[2] != coortab[2]
          ); hashnum = (hashnum + 1) & (*hashmsk)) ;

      if (hashnum == hashidx) // already at the good slot 
        continue;
      (*hashtab)[hashnum] = (*hashtab)[hashidx];
      (*hashtab)[hashidx].coorsum = ~0;
    }
  }
  return (0);
}

static
  int
meshVertResize (
    meshVert * restrict * hashtabptr,
    PAMPA_Num * restrict const hashsizptr,
    PAMPA_Num * restrict const hashmaxptr,
    PAMPA_Num * restrict const hashmskptr)
{
  PAMPA_Num                          oldhashsiz;          /* Size of hash table    */
  PAMPA_Num                          hashnum;          /* Hash value            */
  PAMPA_Num                          oldhashmsk;
  int                           cheklocval;
  PAMPA_Num hashidx;
  PAMPA_Num hashtmp;

  cheklocval = 0;
#ifdef PAMPA_DEBUG_ADAPT
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].coorsum == coorsum) && ((*hashtabptr)[hashnum].coortab[0] == coortab[0]) && ((*hashtabptr)[hashnum].coortab[1] == coortab[1]) && ((*hashtabptr)[hashnum].coortab[2] == coortab[2])) {
        errorPrint ("vertices with same coordinates are already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
        return     (1);
      }
  }
#endif /* PAMPA_DEBUG_ADAPT */

  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (meshVert *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (meshVert)))) == NULL) {
    errorPrint ("Out of memory");
    return     (1);
  }

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (meshVert)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].vertnum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (coorsum * MESHVERTHASHPRIME) & (*hashmskptr); (*hashtabptr)[hashnum].coorsum != ~0 && ((*hashtabptr)[hashnum].coorsum != coorsum || (*hashtabptr)[hashnum].coortab[0] != coortab[0] || (*hashtabptr)[hashnum].coortab[1] != coortab[1] || (*hashtabptr)[hashnum].coortab[2] != coortab[2]); hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].coorsum = ~0;
  }

#ifdef PAMPA_DEBUG_ADAPT
  for (hashidx = 0; hashidx < *hashsizptr; hashidx ++) {
    PAMPA_Num coorsum;
    double    coortab[3];

    coorsum = (*hashtabptr)[hashidx].coorsum;
    for (int i = 0 ; i < 3 ; i ++)
      coortab[i] = (*hashtabptr)[hashidx].coortab[i];

    if (coorsum == ~0) // If empty slot
      continue;

    for (hashnum = (hashidx + 1) & (*hashmskptr); hashnum != hashidx ; hashnum = (hashnum + 1) & (*hashmskptr)) 
      if (((*hashtabptr)[hashnum].coorsum == coorsum) && ((*hashtabptr)[hashnum].coortab[0] == coortab[0]) && ((*hashtabptr)[hashnum].coortab[1] == coortab[1]) && ((*hashtabptr)[hashnum].coortab[2] == coortab[2])) {
        errorPrint ("vertices with same coordinates are already in hashtab, hashidx: %d, hashnum: %d", hashidx, hashnum);
        return     (1);
      }
  }
#endif /* PAMPA_DEBUG_ADAPT */
  return (0);
}
