/*  Copyright 2015-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_metric.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 18 May 2015
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#undef PAMPA_TIME_CHECK
#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include "pampa-mmg3d.h"
#include "metric.h"

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D_dmeshMetricCompute (
PAMPA_Dmesh * const        dmshptr,             //!< PaMPA distributed mesh
void        * const        dataptr,
double                     alphval)
{
  PAMPA_MMG3D_Data * data;
  PAMPA_Num baseval;
  PAMPA_Num vertlocnbr;
  PAMPA_Num vertlocnum;
  PAMPA_Num vertlocnnd;
  double *  metrloctax;
  int cheklocval;
  PAMPA_Num soloffset;
  PAMPA_Num soloffidx;
  MPI_Comm proccomm;

  data = (PAMPA_MMG3D_Data *) dataptr;
  cheklocval = 0;

  PAMPA_dmeshData (dmshptr, &baseval, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &proccomm); 
  cheklocval = PAMPA_dmeshValueData (dmshptr, data->nodeent, PAMPA_TAG_SOL_3DI, (void **) &metrloctax);
  if (cheklocval == 0)  /* If there is a value with this tag */
    soloffset = 1;
  else if (cheklocval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
    CHECK_VERR (cheklocval, proccomm);
  }
  else { /* If there no value with this tag */
    cheklocval = PAMPA_dmeshValueData (dmshptr, data->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &metrloctax);
    if (cheklocval == 0)  /* If there is a value with this tag */
      soloffset = 6;
    else if (cheklocval != 2) { /* If error code is different than "No value with this tag" */ // XXX ne doit-on pas mettre un define pour le code d'erreur comme pour les erreurs de meshCheck…
      CHECK_VERR (cheklocval, proccomm);
    }
    else { /* If there no value with this tag */
      errorPrint ("No associated metric");
      CHECK_VERR (cheklocval, proccomm);
    }
  }

  metrloctax -= soloffset * baseval;
  PAMPA_dmeshEnttSize(dmshptr, data->nodeent, &vertlocnbr, PAMPA_VERT_LOCAL, NULL);

  for (vertlocnum = baseval, vertlocnnd = vertlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
    for (soloffidx = 0; soloffidx < soloffset; soloffidx ++) {
      metrloctax[soloffset * vertlocnum + soloffidx] *= alphval;
      //if (vertlocnum == baseval)
      //  infoPrint ("metrlocval: %f, alphval: %f", metrloctax[soloffset * vertlocnum + soloffidx], alphval); 
    }

  CHECK_VDBG (cheklocval, proccomm);
  return (0);
}

