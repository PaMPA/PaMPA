/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        comm.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 Mar 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define COMM_H

/*
**  The type and structure definitions.
*/

#ifndef GNUMMAX                                   /** If dmesh.h not included     */
typedef INT                   Gnum;               /** Vertex and edge numbers     */
typedef UINT                  Gunum;              /** Unsigned type of same width */
#define GNUMMAX                     (INTVALMAX)   /** Maximum signed Gnum value   */
#define GNUMSTRING                  INTSTRING     /** String to printf a Gnum     */
#endif /* GNUMMAX */

/*
**  The function prototypes.
*/

#ifndef COMM
#define static
#endif

int                         commAllgatherv      (void * const, const Gnum, MPI_Datatype, void * const, const Gnum * const, const Gnum * const, MPI_Datatype, MPI_Comm);
int                         commGatherv         (void * const, const Gnum, MPI_Datatype, void * const, const Gnum * const, const Gnum * const, MPI_Datatype, const int, MPI_Comm);
int                         commScatterv        (void * const, const Gnum * const, const Gnum * const, MPI_Datatype, void * const, const Gnum, MPI_Datatype, const int, MPI_Comm);

#undef static

/*
**  The macro definitions.
*/

#ifndef COMM
#ifndef INTSIZE64
#ifdef PAMPA_RENAME
#define _PAMPAcommAllgatherv        MPI_Allgatherv
#define _PAMPAcommGatherv           MPI_Gatherv
#define _PAMPAcommScatterv          MPI_Scatterv
#else /* PAMPA_RENAME */
#define commAllgatherv              MPI_Allgatherv
#define commGatherv                 MPI_Gatherv
#define commScatterv                MPI_Scatterv
#endif /* PAMPA_RENAME */
#endif /* INTSIZE64 */
#endif /* COMM      */
