/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        rmesh_to_pmesh.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 30 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#define MESHVERTHASHPRIME       17            //!< Prime number for hashing

// XXX commentaire en gardant bien les v[3] en début
typedef struct faces_ {
  PAMPA_Num v[3];
  PAMPA_Num refval;
  PAMPA_Num requflg;
} faces;

typedef struct meshVert_ {
  PAMPA_Num coorsum; // XXX est-ce utile ?
  double    coortab[3];
  PAMPA_Num vertnum;
} meshVert;

//! \brief This routine initializes the
//! mesh structure of the mmg3d remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
rmesh2pmesh (
MMG5_pMesh     const         mmshptr,
MMG5_pSol      const         msolptr,
PAMPA_MMG3D_Data  * const   dataptr,
PAMPA_Num     const         baseval,
PAMPA_Num     const         adptval,
PAMPA_Mesh  * const         pmshptr);


int
rmeshHashtableToggleElem (
    meshVert ** hashtab,
    PAMPA_Num * hashsiz,
    PAMPA_Num * hashmax,
    PAMPA_Num * hashnbr,
    PAMPA_Num * hashmsk,
    double    * coortab,
    PAMPA_Num   vertnum,
    PAMPA_Num * vertnm2);

static
int
meshVertResize (
    meshVert * restrict * hashtabptr,
    PAMPA_Num * restrict const hashsizptr,
    PAMPA_Num * restrict const hashmaxptr,
    PAMPA_Num * restrict const hashmskptr);
