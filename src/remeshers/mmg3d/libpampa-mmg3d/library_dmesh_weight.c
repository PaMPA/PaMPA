/*  Copyright 2015-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_dmesh_weight.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 18 May 2015
//!                             to:   25 Sep 2017
//!
/************************************************************/

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#undef PAMPA_TIME_CHECK
#include "module.h"
#include "common.h"
#include "types.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include "pampa-mmg3d.h"
#include <fcntl.h>




//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D_dmeshWeightCompute (
PAMPA_Dmesh * const        dmshptr,             //!< PaMPA distributed mesh
void        * const        dataptr,             //!< Data used by the remesher
double      * const        veloloctax,          // TODO: change tax by tab, because API
PAMPA_Num   *              vnodlocnbr)          //!< Estimated number of nodes
{
  PAMPA_MMG3D_Data * data;
  PAMPA_Mesh * cmshptr;
  MMG5_pMesh    mmshptr;
  MMG5_pSol     msolptr;
  MMG3D_int         opt[10];
  PAMPA_Num   baseval;
  PAMPA_Num   basedif;
  PAMPA_Num   tetrlocnbr;
  PAMPA_Num   tetrgstnbr;
  PAMPA_Num tetrnbr;
  PAMPA_Num   vertnbr;
  PAMPA_Num   vertnum;
  PAMPA_Num   meshlocnbr;
  MMG3D_int   vnumnbr;
  PAMPA_Iterator it;
  PAMPA_Num * restrict iflgtax;
  PAMPA_Num * restrict erefloctax;
  PAMPA_Num * restrict rmshloctax;
  MMG3D_int * restrict rmshloctx2;
  PAMPA_Num * restrict vnumgsttax;
  PAMPA_Num * restrict parttax;
  PAMPA_Num * restrict vnumloctax; // XXX est-ce le bon nom ?
  PAMPA_Num * restrict vnumtax;
  double * restrict veloloctx2;
  PAMPA_Num       vertlocnum;
  PAMPA_Num       vertlocnnd;
  MPI_Comm        proccomm;
  int new;
  int bak;
  int cheklocval;
  int mmgval;
  int proclocnum;
  int procngbnum;
  int procglbnbr;
  int solflg;
  double    * restrict soltax;
  long vnodlocnb2;
  char s[50];

  data = (PAMPA_MMG3D_Data *) dataptr;
  cheklocval = 0;

  
  // XXX remplacer le calloc

  PAMPA_dmeshData (dmshptr, &baseval, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &proccomm); 
  MPI_Comm_size(proccomm, &procglbnbr) ;
  MPI_Comm_rank(proccomm, &proclocnum) ;

  basedif = 1 - baseval;
  CHECK_FERR(PAMPA_dmeshValueData(dmshptr, data->tetrent, PAMPA_TAG_REF, (void **) &erefloctax), proccomm); // XXX attention data->tetrent vaut-il toujours baseval ?
  erefloctax -= baseval;
  CHECK_FERR(PAMPA_dmeshValueData(dmshptr, data->tetrent, PAMPA_TAG_REMESH, (void **) &rmshloctax), proccomm);
  rmshloctax -= baseval;
  PAMPA_dmeshEnttSize (dmshptr, data->tetrent, &tetrlocnbr, PAMPA_VERT_LOCAL, &tetrgstnbr, PAMPA_VERT_ANY, NULL);

  CHECK_FERR (PAMPA_dmeshValueLink(dmshptr, (void **) &vnumloctax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, data->tetrent, PAMPA_TAG_PERM), proccomm);
  vnumloctax -= baseval;

  for (vertlocnum = baseval, vertlocnnd = tetrlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) 
    vnumloctax[vertlocnum] = vertlocnum;

#ifdef PAMPA_DEBUG_ADAPT2
  if (PAMPA_TAG_VERT_NOREMESH != 0) {
    errorPrint ("Tag PAMPA_TAG_VERT_NOREMESH is not 0");
    cheklocval = 1;
  }
  CHECK_VDBG (cheklocval, proccomm);
#endif /* PAMPA_DEBUG_ADAPT2 */

  if (memAllocGroup ((void **) (void *)
                          &vnumgsttax, (size_t) (tetrgstnbr * sizeof (PAMPA_Num)),
                          &parttax,    (size_t) (procglbnbr * sizeof (PAMPA_Num)),
                          &rmshloctx2, (size_t) (tetrlocnbr * sizeof (MMG3D_int)),
                          &veloloctx2, (size_t) (tetrlocnbr * sizeof (double)),
                          &cmshptr,    (size_t) (             sizeof (PAMPA_Mesh)),
                          NULL) == NULL) {
          errorPrint ("Out of memory");
          cheklocval = 1;
  }
  CHECK_VERR (cheklocval, proccomm);
  vnumgsttax -= baseval;
  parttax -= baseval;
  rmshloctx2 -= baseval;
  veloloctx2 -= baseval;

  memSet (rmshloctx2 + baseval, 0, tetrlocnbr * sizeof (MMG3D_int));
  memSet (vnumgsttax + baseval, ~0, tetrgstnbr * sizeof (PAMPA_Num)); // XXX pas ~0, mais le define

  PAMPA_meshInit (cmshptr);
  for (vertlocnum = baseval, vertlocnnd = tetrlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) {
#ifdef PAMPA_NOT_REF_ONE
    if (erefloctax[vertlocnum] != PAMPA_REF_IS)
#endif /* PAMPA_NOT_REF_ONE */
      vnumgsttax[vertlocnum] = proclocnum;
  }

  for (procngbnum = 0; procngbnum < procglbnbr; procngbnum ++) 
    parttax[procngbnum + baseval] = procngbnum;

  memSet (veloloctax + baseval, 0, tetrlocnbr * sizeof (double));

  meshlocnbr = -1;
  CHECK_FERR (PAMPA_dmeshGatherInduceMultiple (dmshptr, procglbnbr, vnumgsttax + baseval, parttax + baseval, &meshlocnbr, &cmshptr), proccomm);

  if (meshlocnbr == 1) {

    PAMPA_meshData (cmshptr, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);

    // XXX tester le retour des fonctions
    solflg = PAMPA_meshValueData(cmshptr, data->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
    if (solflg != 0) {  /* If there is a value with this tag */
      solflg = PAMPA_meshValueData(cmshptr, data->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
    }
    if (solflg == 0) {
      CHECK_FERR (pmesh2rmesh(cmshptr, data, 0, &mmshptr, &msolptr), proccomm); // XXX 0?
      MMG3D_hashTetra(mmshptr, 0);

#ifdef PAMPA_INFO_REMESHER
      sprintf (s, "weight-out-%d-%d", getpid(), 0);
#else /* PAMPA_INFO_REMESHER */
      sprintf (s, "/dev/null"); // TODO mettre /dev/null comme macro définie par CMake, car NUL pour win et /dev/null pour unix ;)
#endif /* PAMPA_INFO_REMESHER */
      fflush (stdout);
      bak = dup (1);
      new = open(s, O_CREAT|O_WRONLY|O_TRUNC, 0600);
      dup2 (new, 1);
      close (new);
      if (veloloctax == NULL)
        mmgval = _MMG5_countelt(mmshptr, msolptr, NULL, &vnodlocnb2); // XXX verifier la valeur de retour
      else
        mmgval = _MMG5_countelt(mmshptr, msolptr, veloloctx2 - basedif, &vnodlocnb2); // XXX verifier la valeur de retour
      //fprintf (stderr, "retour de mmg : %d\n", cheklocval);
      fflush (stdout);
      dup2 (bak, 1);
      close (bak);

      // XXX si dépassement
      // modifier la taille de l'entier
      if (vnodlocnb2 < 0)
        vnodlocnb2 = INT_MAX;
      if (vnodlocnbr != NULL)
        *vnodlocnbr = (PAMPA_Num) vnodlocnb2;
    }
    else /* (solflg == 1) */
      PAMPA_meshEnttSize(cmshptr, data->nodeent, vnodlocnbr);
    CHECK_FERR(PAMPA_meshValueData(cmshptr, data->tetrent, PAMPA_TAG_PERM, (void **) &vnumtax), proccomm); // XXX attention data->tetrent vaut-il toujours baseval ?
    vnumtax -= baseval;
    PAMPA_meshEnttSize (cmshptr, data->tetrent, &tetrnbr);
    for (vertlocnum = baseval, vertlocnnd = tetrnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++) 
      veloloctax[vnumtax[vertlocnum]] = (solflg == 0) ? (veloloctx2[vertlocnum]) : (1);



    if (solflg == 0)
      MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);
    PAMPA_meshExit (cmshptr);
  }
  memFreeGroup (vnumgsttax + baseval);
  CHECK_FERR (PAMPA_dmeshValueUnlink(dmshptr, data->tetrent, PAMPA_TAG_PERM), proccomm);

  CHECK_VDBG (cheklocval, proccomm);
  return (0);
}

