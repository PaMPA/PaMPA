/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_rmesh_to_pmesh.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from:  1 Jul 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include <stdio.h>
#include <mpi.h>
#include <libmmg3d.h>
#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d.h"
#include "rmesh_to_pmesh.h"
#include "types.h"

//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D_rmesh2pmesh (
MMG5_pMesh * const         mmshptr,
MMG5_pSol  * const         msolptr,
void       * const         dataptr,
PAMPA_Num    const         flagval,             
PAMPA_Mesh * const         pmshptr)             //!< PaMPA centralized mesh
{
  double * soltax;
  const MMG3D_int offset = 1;
  PAMPA_Num baseval;
  PAMPA_Iterator it;
  MMG3D_int dummy;
  PAMPA_Num nodenbr;

  baseval = 0; // XXX à changer
  CHECK_FDBG2 (rmesh2pmesh (*mmshptr, *msolptr, (PAMPA_MMG3D_Data *) dataptr, 0, flagval, pmshptr));

  // XXX uniquement métrique scalaire
  PAMPA_meshValueLink (pmshptr, (void **) &soltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, ((PAMPA_MMG3D_Data *) dataptr)->nodeent, PAMPA_TAG_SOL_3DI); 
  soltax -= offset * baseval;

  // XXX pour que get_scalarsol ne renvoie pas d'erreur
  CHECK_FDBG2 (MMG3D_Get_solSize(*mmshptr, *msolptr, &dummy, &dummy, &dummy) != 1);

  PAMPA_meshEnttSize (pmshptr, ((PAMPA_MMG3D_Data *) dataptr)->nodeent, &nodenbr);
  PAMPA_meshItInitStart(pmshptr, ((PAMPA_MMG3D_Data *) dataptr)->nodeent, &it);
  while (PAMPA_itHasMore(&it)) {
    PAMPA_Num nodenum;

    nodenum = PAMPA_itCurEnttVertNum(&it);

    CHECK_FDBG2 (MMG3D_Get_scalarSol (*msolptr, &soltax[nodenum]) != 1);
    PAMPA_itNext(&it);
  }

  return (0);
}

