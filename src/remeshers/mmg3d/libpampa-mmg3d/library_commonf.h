/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_commonf.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from:  7 Jun 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/
!// Size definitions for the PaMPA opaque
!// structures. These structures must be
!// allocated as arrays of DOUBLEPRECISION
!// values for proper padding. The dummy
!// sizes are computed at compile-time by
!// program "dummysizes".

        !/*+ Flags for vertices +*/
        INTEGER(KIND = PAMPAF_NUM) PAMPAF_MMG3D_NOFLAG
        INTEGER(KIND = PAMPAF_NUM) PAMPAF_MMG3D_ADAPT
        INTEGER(KIND = PAMPAF_NUM) PAMPAF_MMG3D_FACES
        INTEGER(KIND = PAMPAF_NUM) PAMPAF_MMG3D_FORTRAN
        INTEGER(KIND = PAMPAF_NUM) PAMPAF_MMG3D_NODE2NODE
!// FIXME ce n'est pas bon par rapport au C, à corriger !! utiliser peut-être dummysizes
        PARAMETER (PAMPAF_MMG3D_NOFLAG      = 0) 
        PARAMETER (PAMPAF_MMG3D_ADAPT       = 2)
        PARAMETER (PAMPAF_MMG3D_FACES       = 4)
        PARAMETER (PAMPAF_MMG3D_FORTRAN     = 8)
        PARAMETER (PAMPAF_MMG3D_NODE2NODE   = 16)
