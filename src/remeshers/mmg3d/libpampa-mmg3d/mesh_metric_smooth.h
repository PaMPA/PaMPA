/*  Copyright 2016-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mesh_metric_smooth.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 23 Jun 2016
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include "pampa-mmg3d.h"
#include "types.h"
#include <math.h>

// FIXME pour débogue
extern unsigned char _MMG5_iare[6][2];

//! \brief XXX
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
meshMetricSmooth (
PAMPA_Mesh * const      pmshptr,
PAMPA_Num const            flagval)             //!< Flags used by the remesher
{
  PAMPA_Iterator * iterptr;
  PAMPA_Num nodeidx;
  PAMPA_Num edgenum;
  PAMPA_Num nodetab[4];
  double eflgtax;
  PAMPA_Num * restrict nflgtax;
  PAMPA_Num * restrict tetrtab;
  double * restrict coortax;
  double * restrict metrtax;
  PAMPA_Num soloffset;

  // algo pour lisser la métrique sur le maillage
  // - eflgtax tableau de taille |elem| et initialisé à ~0 contenant soit ~0, soit
  //   le rapport entre 0 et 1 de la taille actuelle de la métrique à garder
  // - nflgtax tableau de taille |noeud| et initialisé à ~0 contenant soit ~0, soit 1 si le noeud a déjà été traité
  // - tetrtab tableau de taille |elem| et contenant une liste d'éléments
  //
  // - on parcourt chaque élément « e »
  //   - si e est contraint, on l'ajoute à tetrtab

  CHECK_FDBG2 (PAMPA_meshValueData (pmshptr, data->nodeent, PAMPA_TAG_GEOM, (void **) &coortax));
  coortax -= 3 * baseval;

  PAMPA_meshEnttSize (pmshptr, data->tetrent, &tetrnbr);

  if (memAllocGroup ((void **) (void *)
        &tetrtab, (size_t) (tetrnbr * sizeof (PAMPA_Num)),
        NULL) == NULL) {
    errorPrint("out of memory");
    return (1);
  }
  
  //memSet (oreftax + baseval, 0, nodenbr * sizeof (PAMPA_Num));
  tetrnbr =
    tetridx = 0;
  PAMPA_meshItInitStart(pmshptr, data->tetrent, &it);
  while (PAMPA_itHasMore(&it)) {
    PAMPA_Num mtetnum;
    PAMPA_Num tetenum;

    mtetnum = PAMPA_itCurMeshVertNum(&it);
    tetrnum = PAMPA_itCurEnttVertNum(&it);

    if (vflgtax[mtetnum] != PAMPA_TAG_VERT_INTERNAL)
      tetrtab[tetrnbr ++] = tetrnum;
    PAMPA_itNext(&it);
  }

  //
  // - tant qu'il y a des éléments dans tetrtab
  while (tetridx < tetrnbr) {
    tetrnum = tetrtab[tetridx ++];

    nodeidx = 0;
    PAMPA_itStart(iterptr, tetrnum);
    while (PAMPA_itHasMore(iterptr)) {
      PAMPA_Num nodenum;

      nodenum = PAMPA_itCurEnttVertNum(iterptr);
      nodetab[nodeidx ++] = nodenum;
      PAMPA_itNext(iterptr);
    }

    for (edgenum = 0; edgenum < 6; edgenum ++) {
      MMG3D_int p1;
      MMG3D_int p2;
      double metval;

      p1 = (MMG3D_int) nodetab[_MMG5_iare[edgenum][0]];
      p2 = (MMG3D_int) nodetab[_MMG5_iare[edgenum][1]];
      if (metoffset == 1)
        metval = MMG5_lenedgCoor (&coortax[3*p1], &coortax[3*p2], &metrtax[p1], &metrtax[p2]);
      else {
        errorPrint ("metric not defined");
        return (1);
      }
      printf ("metval: %lf\n", metval);
    } 
  }

  return (0);
}

