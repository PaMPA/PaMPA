/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pmesh_check_per.h
//!
//!   \authors     Cedric Lachat
//!
//!
/************************************************************/

/****************************************/
/*                                      */
/* These routines are the C API for the */
/* remesher handling routines.          */
/*                                      */
/****************************************/

#define DIMNBR  3
#define TETNBR  3 // maximum number of neighboring tetrahedra of a tetrahedron
#define ORIGVAL 0x0008

typedef struct DoubleNum_ {
  PAMPA_Num plannum;
  PAMPA_Num elemnum;
} DoubleNum;

  int
pmeshCheckPer (
    PAMPA_Mesh  * const         imshptr,
    PAMPA_MMG3D_Data  * const   dataptr,
    PAMPA_Mesh  * const         omshptr);
