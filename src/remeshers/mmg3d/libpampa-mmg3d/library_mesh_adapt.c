/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_adapt.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 28 Nov 2011
//!                             to:   25 Sep 2017
//!
/************************************************************/

#ifdef PAMPA_TIME_CHECK2
#define PAMPA_TIME_CHECK
#endif /* PAMPA_TIME_CHECK2 */

#include <stdio.h>
#include <mpi.h>
//#include <libmmg3d.h>
#include "module.h"
#include "common.h"
#include "comm.h"
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include "pampa-mmg3d.h"
#include "types.h"
#include "pmesh_check_per.h"
#include "pmesh_to_rmesh.h"
#include "rmesh_to_pmesh.h"
#include "library_mesh_adapt.h"
//#include <unistd.h>
#include <fcntl.h>


//! \brief This routine calls the remesher
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

// XXX cette fonction doit être juste un appel à meshAdapt (a creer) et déplacer meshAdaptElemResize
// pareil pour les autres remailleurs et toutes les autres fonctions si elles
// sont un peu longues
int
PAMPA_MMG3D_meshAdapt (
PAMPA_Mesh * const         imshptr,             //!< PaMPA centralized in mesh
PAMPA_Mesh * const         omshptr,             //!< PaMPA centralized out mesh
PAMPA_AdaptInfo * const    infoptr,             //!< Information used by the remesher
PAMPA_Num const            flagval)             //!< Flags used by the remesher // XXX not used :(
{
  MMG5_pMesh    mmshptr;
  MMG5_pSol     msolptr;
  PAMPA_Num   baseval;
  PAMPA_Num   basedif;
  PAMPA_Num tetrnbr;
  PAMPA_Num   nodenbr;
  PAMPA_Num   vertnbr;
  PAMPA_Num   vertnnd;
  PAMPA_Num   vertnum;
  PAMPA_Iterator it;
  PAMPA_Iterator it_nghb;
  PAMPA_MMG3D_Data * data;
  PAMPA_Num * restrict iflgtax;
  PAMPA_Num * restrict oflgtax;
  double * restrict osoltax;
  PAMPA_Num * restrict ireftax;
  PAMPA_Num * restrict ireftx2;
  PAMPA_Num * restrict oreftax;
  PAMPA_Num * restrict irmstax;
  PAMPA_Num * restrict ormstax;
  PAMPA_Num * restrict ereftax;
  PAMPA_Num * restrict ireqtax;
  PAMPA_Num * restrict oreqtax;
  double * restrict iwghtax;
  double * restrict owghtax;
  PAMPA_Num       tetrhashnum;
  PAMPA_Num       tetrhashnbr;
  PAMPA_Num       tetrhashmax;
  PAMPA_Num       tetrhashsiz;
  PAMPA_Num       tetrhashmsk;
  MeshAdaptElem * restrict tetrhashtab;
  PAMPA_Num       facehashnum;
  PAMPA_Num       facehashnbr;
  PAMPA_Num       facehashmax;
  PAMPA_Num       facehashsiz;
  PAMPA_Num       facehashmsk;
  MeshAdaptFace * restrict facehashtab;
  MMG3D_int new;
  MMG3D_int bak;
  int chekval;
  MMG3D_int mmgval;
  MMG3D_int dummy;
  int solflg;
  double    * restrict soltax;
  char s[50];
  const MMG3D_int offset = 1;

  chekval = 0;
  data = (PAMPA_MMG3D_Data *) infoptr->dataptr;

#ifdef PAMPA_DEBUG_ADAPT_SAVE3
  {
    FILE * stream;
    char s1[200];
    sprintf (s1, "pampa_mesh-" GNUMSTRING "-" GNUMSTRING "-%d-%d.pmsh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    stream = fopen (s1, "w");
    CHECK_FDBG2 (PAMPA_meshSaveAll (imshptr, stream));
    fclose (stream);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE3 */

  
  // XXX remplacer le calloc

#ifdef PAMPA_ADAPT_TRACE
  char s2[50];
  FILE * ballfile;
  PAMPA_meshEnttSize (imshptr, data->tetrent, &tetrnbr);
  sprintf (s2, "ballsiz-%d", infoptr->rank);
  ballfile = fopen(s2, "a");
  fprintf(ballfile, "%d\n", tetrnbr);
  fclose (ballfile);
#endif /* PAMPA_ADAPT_TRACE */
  PAMPA_meshData (imshptr, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);
  basedif = 1 - baseval;

  // XXX on suppose que nous avons qu'une seule entité avec les refs : celle
  // correspondant aux nœuds
  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &iflgtax));
  iflgtax -= baseval;

  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, data->tetrent, PAMPA_TAG_REMESH, (void **) &irmstax)); // XXX normalement MPI_GNUM
  irmstax -= baseval;

  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, data->tetrent, PAMPA_TAG_WEIGHT, (void **) &iwghtax));
  iwghtax -= baseval;

  CHECK_FDBG2 (PAMPA_meshValueData (imshptr, data->nodeent, PAMPA_TAG_REF, (void **) &ireftax));
  ireftax -= baseval;

  chekval = PAMPA_meshValueData(imshptr, data->faceent, PAMPA_TAG_REQU, (void **) &ireqtax);
  if (chekval == 0) /* If there is a value with this tag */
    ireqtax -= baseval;
  else {
    ireqtax = NULL;
    chekval = 0;
  }

  if ((ireftx2 = (PAMPA_Num *) memAlloc (vertnbr * sizeof (PAMPA_Num))) == NULL) {
    PAMPA_errorPrint  ("out of memory");
    return (1);
  }
  ireftx2 -= baseval;

  PAMPA_meshItInitStart(imshptr, data->nodeent, &it);
  while (PAMPA_itHasMore(&it)) {
	PAMPA_Num nodenum;
	PAMPA_Num mnodnum;

	mnodnum = PAMPA_itCurMeshVertNum(&it);
	nodenum = PAMPA_itCurEnttVertNum(&it);

	ireftx2[mnodnum] = ireftax[nodenum];
    PAMPA_itNext(&it);
  }

  tetrhashnbr = vertnbr / 10;
  for (tetrhashsiz = 256; tetrhashsiz < tetrhashnbr; tetrhashsiz <<= 1) ; /* Get upper power of two */
  tetrhashmsk = tetrhashsiz - 1;
  tetrhashmax = tetrhashsiz >> 2;

  if ((tetrhashtab = (MeshAdaptElem *) memAlloc (tetrhashsiz * sizeof (MeshAdaptElem))) == NULL) {
    errorPrint  ("out of memory");
    return (1);
  }
  memSet (tetrhashtab, ~0, tetrhashsiz * sizeof (MeshAdaptElem));

  PAMPA_meshItInitStart(imshptr, data->tetrent, &it);
  PAMPA_meshItInit(imshptr, data->tetrent, data->nodeent, &it_nghb);
  while (PAMPA_itHasMore(&it)) {
    PAMPA_Num tetnum;
    PAMPA_Num mtetnum; // FIXME mtetnum est ambigu : il peut dire par maillage comme ici ou pour MMG comme dans rmesh_to_pmesh

    mtetnum = PAMPA_itCurMeshVertNum(&it);
    tetnum = PAMPA_itCurEnttVertNum(&it);

    if (iflgtax[mtetnum] != PAMPA_TAG_VERT_INTERNAL) {
      PAMPA_Num nodesum = 0;
      PAMPA_Num nodetab[4];
      PAMPA_Num nodeidx = 0;

      PAMPA_itStart(&it_nghb, tetnum);

      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num nodenum;
        PAMPA_Num mnodnum;

        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
        mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
        nodetab[nodeidx ++] = mnodnum;
        nodesum += mnodnum;
        PAMPA_itNext(&it_nghb);
      }

	  _PAMPAintSort1asc1 (nodetab, 4);

      for (tetrhashnum = (nodesum * MESHADAPTHASHPRIME) & tetrhashmsk;
          tetrhashtab[tetrhashnum].nodesum != ~0 &&
          (tetrhashtab[tetrhashnum].nodesum != nodesum
           || tetrhashtab[tetrhashnum].nodetab[0] != nodetab[0]
           || tetrhashtab[tetrhashnum].nodetab[1] != nodetab[1]
           || tetrhashtab[tetrhashnum].nodetab[2] != nodetab[2]
           || tetrhashtab[tetrhashnum].nodetab[3] != nodetab[3]);
          tetrhashnum = (tetrhashnum + 1) & tetrhashmsk) ;

      if ((tetrhashtab[tetrhashnum].nodesum != nodesum)
          || (tetrhashtab[tetrhashnum].nodetab[0] != nodetab[0])
          || (tetrhashtab[tetrhashnum].nodetab[1] != nodetab[1])
          || (tetrhashtab[tetrhashnum].nodetab[2] != nodetab[2])
          || (tetrhashtab[tetrhashnum].nodetab[3] != nodetab[3])) { /* Vertex not already added */
        tetrhashtab[tetrhashnum].nodesum = nodesum;
        for (nodeidx = 0; nodeidx < 4; nodeidx ++)
          tetrhashtab[tetrhashnum].nodetab[nodeidx] = nodetab[nodeidx];
        tetrhashtab[tetrhashnum].vertnum = tetnum;
        tetrhashnbr ++;

        if (tetrhashnbr >= tetrhashmax)  /* If tetrhashtab is too much filled */
          CHECK_FDBG2 (meshAdaptElemResize (&tetrhashtab, &tetrhashsiz, &tetrhashmax, &tetrhashmsk));
      }
    }

    PAMPA_itNext(&it);
  }

  if ((infoptr->flagval & PAMPA_MMG3D_FACES) != 0) {
    PAMPA_Num facenbr;

    PAMPA_meshEnttSize (imshptr, data->faceent, &facenbr);
    facehashnbr = MAX(5, facenbr / 10);
    for (facehashsiz = 256; facehashsiz < facehashnbr; facehashsiz <<= 1) ; /* Get upper power of two */
    facehashnbr = 0;
    facehashmsk = facehashsiz - 1;
    facehashmax = facehashsiz >> 2;

    if ((facehashtab = (MeshAdaptFace *) memAlloc (facehashsiz * sizeof (MeshAdaptFace))) == NULL) {
      errorPrint  ("out of memory");
      return (1);
    }
    memSet (facehashtab, ~0, facehashsiz * sizeof (MeshAdaptFace));

    PAMPA_meshItInitStart(imshptr, data->faceent, &it);
    PAMPA_meshItInit(imshptr, data->faceent, data->nodeent, &it_nghb);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num facenum;
      PAMPA_Num mfacnum; // FIXME mfacnum est ambigu : il peut dire par maillage comme ici ou pour MMG comme dans rmesh_to_pmesh

      mfacnum = PAMPA_itCurMeshVertNum(&it);
      facenum = PAMPA_itCurEnttVertNum(&it);

      if (iflgtax[mfacnum] != PAMPA_TAG_VERT_INTERNAL) {
        PAMPA_Num nodesum = 0;
        PAMPA_Num nodetab[3];
        PAMPA_Num nodeidx = 0;

        PAMPA_itStart(&it_nghb, facenum);

        while (PAMPA_itHasMore(&it_nghb)) {
          PAMPA_Num nodenum;
          PAMPA_Num mnodnum;

          nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
          mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
          nodetab[nodeidx ++] = mnodnum;
          nodesum += mnodnum;
          PAMPA_itNext(&it_nghb);
        }

        _PAMPAintSort1asc1 (nodetab, 3);

        for (facehashnum = (nodesum * MESHADAPTHASHPRIME) & facehashmsk;
            facehashtab[facehashnum].nodesum != ~0 &&
            (facehashtab[facehashnum].nodesum != nodesum
             || facehashtab[facehashnum].nodetab[0] != nodetab[0]
             || facehashtab[facehashnum].nodetab[1] != nodetab[1]
             || facehashtab[facehashnum].nodetab[2] != nodetab[2]);
            facehashnum = (facehashnum + 1) & facehashmsk) ;

        if ((facehashtab[facehashnum].nodesum != nodesum)
            || (facehashtab[facehashnum].nodetab[0] != nodetab[0])
            || (facehashtab[facehashnum].nodetab[1] != nodetab[1])
            || (facehashtab[facehashnum].nodetab[2] != nodetab[2])) { /* Vertex not already added */
          facehashtab[facehashnum].nodesum = nodesum;
          for (nodeidx = 0; nodeidx < 3; nodeidx ++) // XXX utiliser plutôt memCpy
            facehashtab[facehashnum].nodetab[nodeidx] = nodetab[nodeidx];
          facehashtab[facehashnum].vertnum = facenum;
          facehashnbr ++;

          if (facehashnbr >= facehashmax)  /* If facehashtab is too much filled */
            CHECK_FDBG2 (meshAdaptFaceResize (&facehashtab, &facehashsiz, &facehashmax, &facehashmsk));
        }
      }

      PAMPA_itNext(&it);
    }
  }

#ifdef PAMPA_DEBUG_ADAPT_SAVE
  {
    // pour éviter que mmg écrase par la suite les fichiers avant_adapt-*
    char s1[200], s2[200];
    FILE *mshfile, *solfile;

    sprintf (s1, "avant_adapt_pre-" GNUMSTRING "-" GNUMSTRING "-%d-%d.mesh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    sprintf (s2, "avant_adapt_pre-" GNUMSTRING "-" GNUMSTRING "-%d-%d.sol", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    mshfile = fopen (s1, "w");
    solfile = fopen (s2, "w");
    CHECK_FDBG2 (PAMPA_meshMeshSave (imshptr, mshfile, solfile));
    fclose (mshfile);
    fclose (solfile);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE */
#ifdef MSHINT
  CHECK_FDBG2 (meshMetricSmooth (imshptr, data));
#endif /* MSHINT */

  // XXX pour le périodique
  if ((infoptr->flagval & PAMPA_MMG3D_PERIODIC) != 0) {
    PAMPA_Mesh imshda2;

    CHECK_FDBG2 (PAMPA_meshInit (&imshda2));
    CHECK_FDBG2 (pmeshCheckPer (imshptr, data, &imshda2));
    CHECK_FDBG2 (pmesh2rmesh(&imshda2, data, infoptr->flagval | PAMPA_MMG3D_ADAPT, &mmshptr, &msolptr));
  }
  else {

    CHECK_FDBG2 (pmesh2rmesh(imshptr, data, infoptr->flagval | PAMPA_MMG3D_ADAPT, &mmshptr, &msolptr));
  }
  solflg = PAMPA_meshValueData(imshptr, data->nodeent, PAMPA_TAG_SOL_3DI, (void **) &soltax); 
  if (solflg != 0) {  /* If there is a value with this tag */
    solflg = PAMPA_meshValueData(imshptr, data->nodeent, PAMPA_TAG_SOL_3DAI, (void **) &soltax); 
  }
  if (solflg != 0) 
    msolptr = NULL;

//#ifdef MSHINT
//  {
//    MMG5_pTetra pt;
//
//    PAMPA_meshItInitStart(omshptr, data->tetrent, &it);
//    PAMPA_meshItInit(omshptr, data->tetrent, data->nodeent, &it_nghb);
//    while (PAMPA_itHasMore(&it)) {
//      PAMPA_Num tetrnum;
//      PAMPA_Num edgenum;
//
//      tetrnum = PAMPA_itCurEnttVertNum(&it);
//
//      for (edgenum = 0; edgenum < 6; edgenum ++) {
//        double solval;
//
//        pt = tetrnum + basedif;
//        solval = _MMG5_lenedg_iso (mmshptr, msolptr, edgenum, pt);
//        printf ("solval : %lf\n", solval);
//      }
//    }
//  }
//#endif /* MSHINT */


  //// XXX debut temporaire
  //PAMPA_meshItInitStart(imshptr, data->tetrent, &it);
  //while (PAMPA_itHasMore(&it)) {
  //  PAMPA_Num tetnum;
  //  PAMPA_Num nghbnbr;

  //  tetnum = PAMPA_itCurEnttVertNum(&it);

  //  PAMPA_meshVertData (imshptr, tetnum, data->tetrent, data->tetrent, &nghbnbr, NULL);
  //  if (nghbnbr != 4) {
  //    CHECK_FDBG2 (MMG5_Set_requiredTetrahedra (mmshptr, tetnum + basedif) != 1);
  //  }

  //  PAMPA_itNext(&it);
  //}
  //// XXX fin temporaire


//#undef PAMPA_DEBUG_ADAPT_SAVE // FIXME si on enlève cette ligne, cela provoque une erreur plus loin sur rmshdat.point alors que le tableau n'est pas modifié. Il doit y avoir une fuite mémoire en amont qui n'est pas détectée par valgrind. MALLOC_CHECK_ ne dit rien non plus :(
#ifdef PAMPA_DEBUG_ADAPT_SAVE
  {
    // pour éviter que mmg écrase par la suite les fichiers avant_adapt-*
    char s1[200], s2[200];
    sprintf (s1, "avant_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.mesh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    //CHECK_FDBG2 (MMG3D_Set_outputMeshName(mmshptr, s1) != 1);
    sprintf (s2, "avant_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.sol", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    //CHECK_FDBG2 (MMG3D_Set_outputSolName(mmshptr, msolptr, s2) != 1);
    MMG3D_saveMesh (mmshptr, s1);
    MMG3D_saveSol (mmshptr, msolptr, s2);


    //PAMPA_Num * iflgtx2;
    //printf ("%s: pid %d, line %d", __FUNCTION__, infoptr->rank, __LINE__);
    //sprintf (s, "apres_adapt-mmg3d-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
    //MMG5_saveMesh (mmshptr, s);
    //MMG5_saveSol (mmshptr, msolptr, s);
    //sprintf (s, "avant_adapt-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
    //PAMPA_MMG3D_meshSave (imshptr, 0, data, irmstax + baseval, s);
    //PAMPA_MMG3D_meshSave (imshptr, 1, data, NULL, s);
    //PAMPA_meshEnttSize (imshptr, data->tetrent, &tetrnbr);
    //if ((iflgtx2 = (PAMPA_Num *) memAlloc (tetrnbr * sizeof (PAMPA_Num))) == NULL) {
    //  errorPrint  ("out of memory");
    //  return (1);
    //}
    //iflgtx2 -= baseval;

    //PAMPA_meshItInitStart(imshptr, data->tetrent, &it);
    //while (PAMPA_itHasMore(&it)) {
    //  PAMPA_Num mtetnum;
    //  PAMPA_Num tetnum;

    //  mtetnum = PAMPA_itCurMeshVertNum(&it);
    //  tetnum = PAMPA_itCurEnttVertNum(&it);

    //  iflgtx2[tetnum] = iflgtax[mtetnum];
    //  PAMPA_itNext(&it);
    //}
    //sprintf (s, "avant_adapt-pampa2-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
    //PAMPA_MMG3D_meshSave (imshptr, 0, data, iflgtx2 + baseval, s);
    //memFree (iflgtx2 + baseval);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE */
//  printf ("%s: pid %d, line %d", __FUNCTION__, infoptr->rank, __LINE__);
//  sprintf (s, "avant_adapt-mmg3d-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
//  MMG5_saveMesh (mmshptr, s);
//  MMG5_saveSol (mmshptr, msolptr, s);
//#endif /* PAMPA_DEBUG_ADAPT_SAVE */
  // FIXME le retoru de mmg3dlib peut être un warning pour la valeur 1
  // voir s'il y a d'autres valeurs et lesquelles sont des erreurs
  //CHECK_FDBG2 (MMG5_mmg3dlib(infoptr, mmshptr, msolptr));
#ifdef PAMPA_INFO_REMESHER
  sprintf (s, "remesh-out-" GNUMSTRING "-" GNUMSTRING "-%d-%d", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
#else /* PAMPA_INFO_REMESHER */
  sprintf (s, "/dev/null"); // TODO mettre /dev/null comme macro définie par CMake, car NUL pour win et /dev/null pour unix ;)
#endif /* PAMPA_INFO_REMESHER */
  fflush (stdout);
  bak = dup (1);
  new = open(s, O_CREAT|O_WRONLY|O_TRUNC, 0600);
  dup2 (new, 1);
  close (new);
  if (solflg == 0) {
    CHECK_FDBG2 (MMG3D_Chk_meshData(mmshptr, msolptr) != 1);
  }
  else {
    //CHECK_FDBG2 (MMG3D_Chk_meshData(mmshptr, NULL) != 1);
  }
  infoPrint ("before meshing,nt: %d", mmshptr->nt);
  mmgval = MMG3D_mmg3dlib(mmshptr, msolptr); // ! TODO tester la valeur de retour
  if (solflg == 0) {
    mmgval = MMG3D_mmg3dlib(mmshptr, msolptr); // ! TODO tester la valeur de retour
  }
  else {
    mmgval = MMG3D_mmg3dlib(mmshptr, NULL); // ! TODO tester la valeur de retour
  }
  infoPrint ("after meshing,nt: %d", mmshptr->nt);
  if (mmgval == MMG5_STRONGFAILURE) {
    errorPrint ("fatal error");
    return (1);
  }
#ifdef PAMPA_DEBUG_ADAPT_SAVE
  {
    char s1[200], s2[200];
    sprintf (s1, "apres_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.mesh", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    //CHECK_FDBG2 (MMG3D_Set_outputMeshName(mmshptr, s1) != 1);
    sprintf (s2, "apres_adapt-" GNUMSTRING "-" GNUMSTRING "-%d-%d.sol", infoptr->eitenum, infoptr->iitenum, infoptr->rank, infoptr->cuntval);
    //CHECK_FDBG2 (MMG3D_Set_outputSolName(mmshptr, msolptr, s2) != 1);
    MMG3D_saveMesh (mmshptr, s1);
    MMG3D_saveSol (mmshptr, msolptr, s2);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE */
  //fprintf (stderr, "retour de mmg : %d\n", chekval);
  fflush (stdout);
  dup2 (bak, 1);
  close (bak);
  CHECK_FDBG2 (rmesh2pmesh(mmshptr, msolptr, data, baseval, infoptr->flagval | PAMPA_MMG3D_ADAPT, omshptr));

//#ifdef MSHINT
//  {
//    PAMPA_meshEnttSize (imshptr, data->nodeent, &nodenbr);
//    intsoltax -= offset * baseval;
//    
//    // XXX pour que get_scalarsol ne renvoie pas d'erreur
//    CHECK_FDBG2 (MMG5_Get_solSize(mmshptr, msolptr, &dummy, &dummy, &dummy) != 1);
//    PAMPA_meshEnttSize (omshptr, data->nodeent, &nodenbr);
//    
//    for (nodenum = baseval, nodennd = baseval + nodenbr; nodenum < nodennd; nodenum ++)
//      CHECK_FDBG2 (MMG5_Get_scalarSol (msolptr, &intsoltax[nodenum * offset]) != 1);
//    PAMPA_Num tagptr = PAMPA_TAG_INTSOL;
//    CHECK_FDBG2 (infoptr->EXT_meshInt (imshptr, omshptr, infoptr, 1, &tagptr, 0)); // XXX mshint remplacer 0 par drapeau pour l'interpolateur qui devrait être stocké dans infoptr
//    {
//      double * soltax;
//      CHECK_FDBG2 (PAMPA_meshValueData (omshptr, data->nodeent, PAMPA_TAG_INTSOL, (void **) &soltax));
//      soltax -= baseval;
//
//      PAMPA_meshEnttSize (omshptr, data->nodeent, &nodenbr);
//      for (int v = 0; v < nodenbr; v ++)
//        infoPrint ("reel: %3.6f, calcule: %3.6f", msoldat.met[v+1], soltax[v]);
//    }
//  }
//#endif /* MSHINT */


  PAMPA_meshValueData (omshptr, PAMPA_ENTT_VIRT_VERT, PAMPA_TAG_STATUS, (void **) &oflgtax);
  oflgtax -= baseval;

  PAMPA_meshValueData (omshptr, data->tetrent, PAMPA_TAG_REF, (void **) &ereftax);
  ereftax -= baseval;

  if (ireqtax != NULL) {
    chekval = PAMPA_meshValueData(omshptr, data->faceent, PAMPA_TAG_REQU, (void **) &oreqtax);
    if (chekval == 0) /* If there is a value with this tag */
      oreqtax -= baseval;
    else {
      oreqtax = NULL;
      chekval = 0;
    }
  }

  PAMPA_meshValueLink (omshptr, (void **) &ormstax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, data->tetrent, PAMPA_TAG_REMESH);
  ormstax -= baseval;

  PAMPA_meshValueLink (omshptr, (void **) &owghtax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->tetrent, PAMPA_TAG_WEIGHT);
  owghtax -= baseval;

  // TODO et pour les autres types de métriques qui seront développés ?
  PAMPA_meshValueLink (omshptr, (void **) &osoltax, PAMPA_VALUE_PUBLIC, MPI_DOUBLE, data->nodeent, PAMPA_TAG_SOL_3DI);
  osoltax -= offset * baseval;

  PAMPA_meshValueLink (omshptr, (void **) &oreftax, PAMPA_VALUE_PUBLIC, PAMPA_MPI_NUM, data->nodeent, PAMPA_TAG_REF);
  oreftax -= baseval;

  // XXX pour que get_scalarsol ne renvoie pas d'erreur
  CHECK_FDBG2 (MMG3D_Get_solSize(mmshptr, msolptr, &dummy, &dummy, &dummy) != 1);

  PAMPA_meshEnttSize (omshptr, data->nodeent, &nodenbr);
  memSet (oreftax + baseval, 0, nodenbr * sizeof (PAMPA_Num)); // XXX 0 pour pas de ref, à changer ???
  PAMPA_meshItInitStart(omshptr, data->nodeent, &it);
  while (PAMPA_itHasMore(&it)) {
    PAMPA_Num mnodnum;
    PAMPA_Num nodenum;

    mnodnum = PAMPA_itCurMeshVertNum(&it);
    nodenum = PAMPA_itCurEnttVertNum(&it);

    CHECK_FDBG2 (MMG3D_Get_scalarSol (msolptr, &osoltax[nodenum * offset]) != 1);
    if (oflgtax[mnodnum] != PAMPA_TAG_VERT_INTERNAL)
      oreftax[nodenum] = ireftx2[oflgtax[mnodnum]];
    PAMPA_itNext(&it);
  }
  memFree (ireftx2 + baseval);

#ifdef PAMPA_DEBUG_PRESERV
  if (PAMPA_TAG_VERT_NOREMESH != 0) {
    errorPrint ("Tag PAMPA_TAG_VERT_NOREMESH is not 0");
    chekval = 1;
  }
  CHECK_VDBG2 (chekval);
#endif /* PAMPA_DEBUG_PRESERV */
  PAMPA_meshEnttSize (omshptr, data->tetrent, &tetrnbr);
  memSet (ormstax + baseval, PAMPA_TAG_VERT_NOREMESH, tetrnbr * sizeof (PAMPA_Num));
  memSet (owghtax + baseval, 0, tetrnbr * sizeof (double));
  PAMPA_meshItInitStart(omshptr, data->tetrent, &it);
  PAMPA_meshItInit(omshptr, data->tetrent, data->nodeent, &it_nghb);
  while (PAMPA_itHasMore(&it)) {
    PAMPA_Num tetnum;
    PAMPA_Num mtetnum;
    PAMPA_Num tetnm2;
    PAMPA_Num mtetnm2;
    PAMPA_Num nodetab[4];
    PAMPA_Num nodeidx;
    PAMPA_Num nodesum;
    PAMPA_Num edgenum;
    //const double solmin = 0.68;
    //const double solmax = 1.41;
    const double solmin = 0.71;
    const double solmax = 1.41;
    //const double solmin = 0.5;
    //const double solmax = 1.5;
    //const double solmin = 1.0*sqrt(2)/2.0 - 0.0001;
    //const double solmax = 1.0*sqrt(2) + 0.0001;

    tetnum = PAMPA_itCurEnttVertNum(&it);
    mtetnum = PAMPA_itCurMeshVertNum(&it);

    // XXX tester si mmgval vaut MMG5_STRONGFAILURE ou MMG5_LOWFAILURE
    // si MMG5_STRONGFAILURE renvoyer l'ancien maillage avec tous les tags et
    // renvoyer une valeur de retour spécifique
    //if (mmgval != 0) {
    //  mmgval = MMG5_mmg3dcheck(mmshptr, msolptr, data->qualmax, data->edgemin, data->edgemax, ormstax - basedif);
    //  if (mmgval != 0) {
    //    errorPrint ("internal error");
    //    return (1);
    //  }
    //  //for (edgenum = 0; edgenum < 6; edgenum ++) {
    //  //  MMG3D_int p1;
    //  //  MMG3D_int p2;
    //  //  double solval;

    //  //  p1 = rmshdat.tetra[tetnum + basedif].v[MMG5_iare[edgenum][0]];
    //  //  p2 = rmshdat.tetra[tetnum + basedif].v[MMG5_iare[edgenum][1]];
    //  //  solval = MMG5_long_iso (rmshdat.point[p1].c, rmshdat.point[p2].c, msolptr.m[p1], msolptr.m[p2]);

    //  //  if ((solval < solmin) || (solval > solmax)) {
    //  //    //printf("tetnum : %d, solval : %lf (%lf, %lf)\n", tetnum, solval, solmin, solmax);
    //  //    ormstax[tetnum] = PAMPA_TAG_VERT_REMESH;
    //  //    break;
    //  //  }
    //  //  else
    //  //    ormstax[tetnum] = PAMPA_TAG_VERT_NOREMESH;
    //  //}
    //}

    PAMPA_itStart(&it_nghb, tetnum);

    nodesum =
      nodeidx = 0;
    while (PAMPA_itHasMore(&it_nghb)) {
      PAMPA_Num mnodnum;
      PAMPA_Num nodenum;

      nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
      mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
      if (oflgtax[mnodnum] == PAMPA_TAG_VERT_INTERNAL) {
      	//ormstax[tetnum] = PAMPA_TAG_VERT_NOREMESH;
        break;
      }
      nodetab[nodeidx ++] = oflgtax[mnodnum];
      nodesum += oflgtax[mnodnum];
      PAMPA_itNext(&it_nghb);
    }

    if (nodeidx != 4) { /* All of the nodes are not on the boundary */
      if (mmgval == 0)
        ormstax[tetnum] = PAMPA_TAG_VERT_NOREMESH;
      oflgtax[mtetnum] = PAMPA_TAG_VERT_INTERNAL;
      PAMPA_itNext(&it);
      continue;
    }

    _PAMPAintSort1asc1 (nodetab, 4);

    for (tetrhashnum = (nodesum * MESHADAPTHASHPRIME) & tetrhashmsk;
        tetrhashtab[tetrhashnum].nodesum != ~0 &&
        (tetrhashtab[tetrhashnum].nodesum != nodesum
         || tetrhashtab[tetrhashnum].nodetab[0] != nodetab[0]
         || tetrhashtab[tetrhashnum].nodetab[1] != nodetab[1]
         || tetrhashtab[tetrhashnum].nodetab[2] != nodetab[2]
         || tetrhashtab[tetrhashnum].nodetab[3] != nodetab[3]);
        tetrhashnum = (tetrhashnum + 1) & tetrhashmsk) ;

    if ((tetrhashtab[tetrhashnum].nodesum != nodesum)
        || (tetrhashtab[tetrhashnum].nodetab[0] != nodetab[0])
        || (tetrhashtab[tetrhashnum].nodetab[1] != nodetab[1])
        || (tetrhashtab[tetrhashnum].nodetab[2] != nodetab[2])
        || (tetrhashtab[tetrhashnum].nodetab[3] != nodetab[3])) { /* Vertex not already added */
      ormstax[tetnum] = PAMPA_TAG_VERT_NOREMESH;
      oflgtax[mtetnum] = PAMPA_TAG_VERT_INTERNAL;
      PAMPA_itNext(&it);
      continue;
    }

    tetnm2 = tetrhashtab[tetrhashnum].vertnum;
    CHECK_FDBG2_NOTIM (PAMPA_meshVertData (imshptr, tetnm2, baseval, -1, NULL, &mtetnm2));
    oflgtax[mtetnum] = mtetnm2;
#ifdef PAMPA_DEBUG_PRESERV
    tetrhashtab[tetrhashnum].nodesum = 0;
#endif /* PAMPA_DEBUG_PRESERV */
#ifdef PAMPA_NOT_REF_ONE
    if ((irmstax[tetnm2] == PAMPA_TAG_VERT_REMESH) && (ereftax[tetnum] != PAMPA_REF_IS)) {
#else  /* PAMPA_NOT_REF_ONE */
    if (irmstax[tetnm2] == PAMPA_TAG_VERT_REMESH) {
#endif /* PAMPA_NOT_REF_ONE */
        ormstax[tetnum] = PAMPA_TAG_VERT_REMESH;
        owghtax[tetnum] = iwghtax[tetnm2];
    }
    else
        ormstax[tetnum] = PAMPA_TAG_VERT_NOREMESH;

    PAMPA_itNext(&it);
  }
#ifdef PAMPA_DEBUG_PRESERV
  for (tetrhashnum = 0; tetrhashnum < tetrhashsiz; tetrhashnum ++)
    if (tetrhashtab[tetrhashnum].nodesum > 0) {
      errorPrint ("element with tetrhashnum = " PAMPA_NUMSTRING " (" PAMPA_NUMSTRING ", " PAMPA_NUMSTRING ", " PAMPA_NUMSTRING ", " PAMPA_NUMSTRING "), not found on adapted mesh (pid: %d)", tetrhashnum,
          tetrhashtab[tetrhashnum].nodetab[0],
          tetrhashtab[tetrhashnum].nodetab[1],
          tetrhashtab[tetrhashnum].nodetab[2],
          tetrhashtab[tetrhashnum].nodetab[3],
          infoptr->rank);
      return (1);
    }
#endif /* PAMPA_DEBUG_PRESERV */
  memFree (tetrhashtab);

  if ((infoptr->flagval & PAMPA_MMG3D_FACES) != 0) {
    PAMPA_meshItInitStart(omshptr, data->faceent, &it);
    PAMPA_meshItInit(omshptr, data->faceent, data->nodeent, &it_nghb);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num facenum;
      PAMPA_Num mfacnum;
      PAMPA_Num facenm2;
      PAMPA_Num mfacnm2;
      PAMPA_Num nodetab[3];
      PAMPA_Num nodeidx;
      PAMPA_Num nodesum;

      facenum = PAMPA_itCurEnttVertNum(&it);
      mfacnum = PAMPA_itCurMeshVertNum(&it);

      PAMPA_itStart(&it_nghb, facenum);

      nodesum =
        nodeidx = 0;
      while (PAMPA_itHasMore(&it_nghb)) {
        PAMPA_Num mnodnum;
        PAMPA_Num nodenum;

        nodenum = PAMPA_itCurEnttVertNum(&it_nghb);
        mnodnum = PAMPA_itCurMeshVertNum(&it_nghb);
        if (oflgtax[mnodnum] == PAMPA_TAG_VERT_INTERNAL) {
          break;
        }
        nodetab[nodeidx ++] = oflgtax[mnodnum];
        nodesum += oflgtax[mnodnum];
        PAMPA_itNext(&it_nghb);
      }

      if (nodeidx != 3) { /* All of the nodes are not on the boundary */
        oflgtax[mfacnum] = PAMPA_TAG_VERT_INTERNAL;
        PAMPA_itNext(&it);
        continue;
      }

      _PAMPAintSort1asc1 (nodetab, 3);

      for (facehashnum = (nodesum * MESHADAPTHASHPRIME) & facehashmsk;
          facehashtab[facehashnum].nodesum != ~0 &&
          (facehashtab[facehashnum].nodesum != nodesum
           || facehashtab[facehashnum].nodetab[0] != nodetab[0]
           || facehashtab[facehashnum].nodetab[1] != nodetab[1]
           || facehashtab[facehashnum].nodetab[2] != nodetab[2]);
          facehashnum = (facehashnum + 1) & facehashmsk) ;

      if ((facehashtab[facehashnum].nodesum != nodesum)
          || (facehashtab[facehashnum].nodetab[0] != nodetab[0])
          || (facehashtab[facehashnum].nodetab[1] != nodetab[1])
          || (facehashtab[facehashnum].nodetab[2] != nodetab[2])) { /* Vertex not already added */
        oflgtax[mfacnum] = PAMPA_TAG_VERT_INTERNAL;
        PAMPA_itNext(&it);
        continue;
      }

      facenm2 = facehashtab[facehashnum].vertnum;
      CHECK_FDBG2_NOTIM (PAMPA_meshVertData (imshptr, facenm2, data->faceent, -1, NULL, &mfacnm2));
      if (ireqtax != NULL)
        oreqtax[facenum] = ireqtax[facenm2];
      oflgtax[mfacnum] = mfacnm2;
#ifdef PAMPA_DEBUG_PRESERV
      facehashtab[facehashnum].nodesum = 0;
#endif /* PAMPA_DEBUG_PRESERV */
      PAMPA_itNext(&it);
    }
#ifdef PAMPA_DEBUG_PRESERV
    for (facehashnum = 0; facehashnum < facehashsiz; facehashnum ++)
      if (facehashtab[facehashnum].nodesum > 0) {
        errorPrint ("face with facenum = %d, not found on adapted mesh (rank: %d)", facehashnum, infoptr->rank);
        return (1);
      }
#endif /* PAMPA_DEBUG_PRESERV */
    memFree (facehashtab);
  }

  PAMPA_meshData (omshptr, &baseval, NULL, &vertnbr, NULL, NULL, NULL, NULL);

  for (vertnum = baseval, vertnnd = vertnbr + baseval; vertnum < vertnnd; vertnum ++) {
	if (oflgtax[vertnum] != PAMPA_TAG_VERT_INTERNAL)
	  oflgtax[vertnum] = iflgtax[oflgtax[vertnum]];
  }
#ifdef PAMPA_DEBUG_ADAPT_SAVE2
  {
    PAMPA_Num * oflgtx2;
    //printf ("%s: pid %d, line %d", __FUNCTION__, infoptr->rank, __LINE__);
    //sprintf (s, "apres_adapt-mmg3d-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
    //MMG5_saveMesh (mmshptr, s);
    //MMG5_saveSol (mmshptr, msolptr, s);
    sprintf (s, "apres_adapt-pampa2-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
    CHECK_FDBG2 (PAMPA_MMG3D_meshSave (omshptr, 0, data, ormstax + baseval, s));
    if ((oflgtx2 = (PAMPA_Num *) memAlloc (tetrnbr * sizeof (PAMPA_Num))) == NULL) {
      errorPrint  ("out of memory");
      return (1);
    }
    oflgtx2 -= baseval;

    PAMPA_meshItInitStart(omshptr, data->tetrent, &it);
    while (PAMPA_itHasMore(&it)) {
      PAMPA_Num mtetnum;
      PAMPA_Num tetnum;

      mtetnum = PAMPA_itCurMeshVertNum(&it);
      tetnum = PAMPA_itCurEnttVertNum(&it);

      oflgtx2[tetnum] = oflgtax[mtetnum];
      PAMPA_itNext(&it);
    }
    sprintf (s, "apres_adapt-pampa3-%d-%d.mesh", infoptr->rank, infoptr->cuntval);
    PAMPA_MMG3D_meshSave (omshptr, 0, data, oflgtx2 + baseval, s);
    memFree (oflgtx2 + baseval);
  }
#endif /* PAMPA_DEBUG_ADAPT_SAVE2 */
  // XXX 2 lignes suivantes ont été déplacées pour le débogage (voir a314daea5b2)
  MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);

  infoptr->cuntval ++;

  return (0);
}

static
  int
meshAdaptElemResize (
	MeshAdaptElem * restrict * hashtabptr,
	PAMPA_Num * restrict const           hashsizptr,
	PAMPA_Num * restrict const           hashmaxptr,
	PAMPA_Num * restrict const           hashmskptr)
{
  PAMPA_Num                          oldhashsiz;          /* Size of hash table    */
  PAMPA_Num                          hashnum;          /* Hash value            */
  PAMPA_Num                          oldhashmsk;
  int                           cheklocval;
  PAMPA_Num hashidx;
  PAMPA_Num hashtmp;

  cheklocval = 0;
  oldhashmsk = *hashmskptr;
  oldhashsiz = *hashsizptr;
  *hashmaxptr <<= 1;
  *hashsizptr <<= 1;
  *hashmskptr = *hashsizptr - 1;

  if ((*hashtabptr = (MeshAdaptElem *) memRealloc (*hashtabptr, (*hashsizptr * sizeof (MeshAdaptElem)))) == NULL) {
    errorPrint ("Out of memory");
    return (1);
  }

  memSet (*hashtabptr + oldhashsiz, ~0, oldhashsiz * sizeof (MeshAdaptElem)); // TRICK: *hashsizptr = oldhashsiz * 2

  for (hashidx = oldhashsiz - 1; (*hashtabptr)[hashidx].nodesum != ~0; hashidx --); // Stop at last empty slot

  hashtmp = hashidx;

  for (hashidx = (hashtmp + 1) & oldhashmsk; hashidx != hashtmp ; hashidx = (hashidx + 1) & oldhashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num nodeidx;
    PAMPA_Num nodesum;
    PAMPA_Num nodetab[4];

    nodesum = (*hashtabptr)[hashidx].nodesum;

    if (nodesum == ~0) // If empty slot
      continue;

    for (nodeidx = 0; nodeidx < 4; nodeidx ++)
      nodetab[nodeidx] = (*hashtabptr)[hashidx].nodetab[nodeidx];
    
    for (hashnum = (nodesum * MESHADAPTHASHPRIME) & (*hashmskptr);
        (*hashtabptr)[hashnum].nodesum != ~0 &&
        ((*hashtabptr)[hashnum].nodesum != nodesum
         || (*hashtabptr)[hashnum].nodetab[0] != nodetab[0]
         || (*hashtabptr)[hashnum].nodetab[1] != nodetab[1]
         || (*hashtabptr)[hashnum].nodetab[2] != nodetab[2]
         || (*hashtabptr)[hashnum].nodetab[3] != nodetab[3]);
        hashnum = (hashnum + 1) & (*hashmskptr)) ;

    if (hashnum == hashidx) // already at the good slot 
      continue;
    (*hashtabptr)[hashnum] = (*hashtabptr)[hashidx];
    (*hashtabptr)[hashidx].nodesum = ~0;
  }

  CHECK_VDBG2 (cheklocval);
  return (0);
}

static
  int
meshAdaptFaceResize (
	MeshAdaptFace * restrict * facehashtabptr,
	PAMPA_Num * restrict const           facehashsizptr,
	PAMPA_Num * restrict const           facehashmaxptr,
	PAMPA_Num * restrict const           facehashmskptr)
{
  PAMPA_Num                          oldfacehashsiz;          /* Size of facehash table    */
  PAMPA_Num                          facehashnum;          /* faceHash value            */
  PAMPA_Num                          oldfacehashmsk;
  int                           cheklocval;
  PAMPA_Num facehashidx;
  PAMPA_Num facehashtmp;

  cheklocval = 0;
  oldfacehashmsk = *facehashmskptr;
  oldfacehashsiz = *facehashsizptr;
  *facehashmaxptr <<= 1;
  *facehashsizptr <<= 1;
  *facehashmskptr = *facehashsizptr - 1;

  if ((*facehashtabptr = (MeshAdaptFace *) memRealloc (*facehashtabptr, (*facehashsizptr * sizeof (MeshAdaptFace)))) == NULL) {
    errorPrint ("Out of memory");
    return (1);
  }

  memSet (*facehashtabptr + oldfacehashsiz, ~0, oldfacehashsiz * sizeof (MeshAdaptFace)); // TRICK: *facehashsizptr = oldfacehashsiz * 2

  for (facehashidx = oldfacehashsiz - 1; (*facehashtabptr)[facehashidx].nodesum != ~0; facehashidx --); // Stop at last empty slot

  facehashtmp = facehashidx;

  for (facehashidx = (facehashtmp + 1) & oldfacehashmsk; facehashidx != facehashtmp ; facehashidx = (facehashidx + 1) & oldfacehashmsk) { // Start 1 slot after the last empty and end on it
    PAMPA_Num nodeidx;
    PAMPA_Num nodesum;
    PAMPA_Num nodetab[3];

    nodesum = (*facehashtabptr)[facehashidx].nodesum;

    if (nodesum == ~0) // If empty slot
      continue;

    for (nodeidx = 0; nodeidx < 3; nodeidx ++)
      nodetab[nodeidx] = (*facehashtabptr)[facehashidx].nodetab[nodeidx];
    
    for (facehashnum = (nodesum * MESHADAPTHASHPRIME) & (*facehashmskptr);
        (*facehashtabptr)[facehashnum].nodesum != ~0 &&
        ((*facehashtabptr)[facehashnum].nodesum != nodesum
         || (*facehashtabptr)[facehashnum].nodetab[0] != nodetab[0]
         || (*facehashtabptr)[facehashnum].nodetab[1] != nodetab[1]
         || (*facehashtabptr)[facehashnum].nodetab[2] != nodetab[2]);
        facehashnum = (facehashnum + 1) & (*facehashmskptr)) ;

    if (facehashnum == facehashidx) // already at the good slot 
      continue;
    (*facehashtabptr)[facehashnum] = (*facehashtabptr)[facehashidx];
    (*facehashtabptr)[facehashidx].nodesum = ~0;
  }

  CHECK_VDBG2 (cheklocval);
  return (0);
}

