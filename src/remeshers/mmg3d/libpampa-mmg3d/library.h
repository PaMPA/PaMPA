/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library.h
//!
//!   \authors     Francois Pellegrini
//!                Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 17 Jan 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/

/*
**  The type and structure definitions.
*/

#define PAMPA_MMG3D_NOFLAG        0x0000
#define PAMPA_MMG3D_ADAPT         0x0002
#define PAMPA_MMG3D_FACES         0x0004
#define PAMPA_MMG3D_FORTRAN       0x0008
#define PAMPA_MMG3D_NODE2NODE     0x0010
#define PAMPA_MMG3D_SEQPART       0x0020
#define PAMPA_MMG3D_PERIODIC      0x0040

#define PAMPA_BUG_REF_VAL 200
typedef struct {
  PAMPA_Num tetrent;
  PAMPA_Num faceent;
  PAMPA_Num nodeent;
  PAMPA_Num infoprt;
  double    edgemin;
  double    edgemax;
  double    qualmin;
  MMG5_Info infoval;
  MPI_Datatype coortyp;
} PAMPA_MMG3D_Data;

int PAMPA_MMG3D_meshAdapt (
    PAMPA_Mesh * const           imshptr,
    PAMPA_Mesh * const           omshptr,
    PAMPA_AdaptInfo * const      infoptr,
    PAMPA_Num    const           flagval);

int PAMPA_MMG3D_meshLoad (
    PAMPA_Mesh        * const    meshptr,
    char              * const    fileval,
    PAMPA_MMG3D_Data * const    dataptr,
    PAMPA_Num    const           flagval);

int PAMPA_MMG3D_dmeshBand (
    PAMPA_Dmesh * const          dmshptr,
    void        * const          dataptr,
    PAMPA_Num   * const          vertgsttab,
    PAMPA_Num                    bandval);

int PAMPA_MMG3D_dmeshCheck (
    PAMPA_Dmesh * const          dmshptr,
    void        * const          dataptr,
    PAMPA_Num const              flagval);

int PAMPA_MMG3D_dmeshMetricCompute (
    PAMPA_Dmesh * const        dmshptr,
    void        * const        dataptr,
    double                     alphval);

int PAMPA_MMG3D_meshSave (
    PAMPA_Mesh * const           meshptr,
    PAMPA_Num    const           solflag,
    void       * const           dataptr,
    PAMPA_Num  * const           reftab,
    char       * const           fileval);

int PAMPA_MMG3D_dmeshWeightCompute (
    PAMPA_Dmesh * const          dmshptr,
    void        * const          dataptr,
    double      * const          veloloctax,
    PAMPA_Num   *                vnodlocnbr);

int PAMPA_MMG3D_pmesh2rmesh (
    PAMPA_Mesh * const         pmshptr,
    void       * const         dataptr,
    PAMPA_Num    const         flagval,             
    MMG5_pMesh * const         mmshptr,
    MMG5_pSol  * const         msolptr);

int PAMPA_MMG3D_rmesh2pmesh (
    MMG5_pMesh * const         mmshptr,
    MMG5_pSol  * const         msolptr,
    void       * const         dataptr,
    PAMPA_Num    const         flagval,             
    PAMPA_Mesh * const         pmshptr);

int PAMPA_MMG3D_smeshLoad (
PAMPA_Smesh	* const			meshptr,
char		* const         fileval,
PAMPA_MMG3D_Data   * const         dataptr,
PAMPA_Num const             flagval,
PAMPA_Num const             procnbr,
PAMPA_Num * const           procvrttab);
