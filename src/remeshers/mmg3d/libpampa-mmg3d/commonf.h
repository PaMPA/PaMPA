/*  Copyright 2011-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        commonf.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 21 Mar 2011
//!                             to:   22 Sep 2017
//!
/************************************************************/
#ifndef INT                                       /* If type not externally overriden */
#ifdef INTSIZE32
#define PAMPA_C_INT                 C_INT32_T
#define MPI_C_INT_IMP               , C_INT
#else /* INTSIZE32 */
#ifdef INTSIZE64
#define PAMPA_C_INT                 C_INT64_T
#define MPI_C_INT_IMP               , C_INT
#else /* INTSIZE64 */
#define PAMPA_C_INT                 C_INT
/* pour éviter les warning d'import quand PAMPA_C_INT = C_INT */
#define MPI_C_INT_IMP
#endif /* INTSIZE64 */
#endif /* INTSIZE32 */
#endif /* INT       */

#define PAMPA_C_DOUBLE C_DOUBLE
#define MPI_C_INT C_INT


#define TYPE_INTERNAL -1


#ifdef PAMPA_RENAME
#define dmeshItData_name        '_PAMPAdmeshItData'
#define meshItData_name         '_PAMPAmeshItData'
#define typeInit_name         '_PAMPAtypeInit'
#else /* PAMPA_RENAME */
#define typeInit_name         'typeInit'
#define meshItData_name         'meshItData'
#define dmeshItData_name        'dmeshItData'
#endif /* PAMPA_RENAME */
