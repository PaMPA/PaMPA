/*  Copyright 2015-2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        library_mesh_save.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 18 May 2015
//!                             to:   22 Sep 2017
//!
/************************************************************/

#include "common.h"
#include "module.h"
#include <libmmg3d.h>
#include <pampa.h>
#include <pampa.h>
#include "pampa-mmg3d.h"
#include "pmesh_to_rmesh.h"
#include "types.h"

//! \brief This routine calls the remesher to read the mesh and convert remesher
//! mesh to PaMPA mesh
//!
//! \returns 0   : if the initialization succeeded.
//! \returns !0  : on error.

int
PAMPA_MMG3D_meshSave (
PAMPA_Mesh	* const			meshptr,              //!< PaMPA centralized in mesh
PAMPA_Num         const                 solflag,
void       * const         dataptr,
PAMPA_Num   * const         reftab,
char		* const         fileval)             //!< Filename value
{
  MMG5_pMesh    mmshptr;
  MMG5_pSol     msolptr;
  PAMPA_Num   tetrnum;
  PAMPA_Num   tetrnnd;
  MMG3D_int  tetrnbr;
  PAMPA_MMG3D_Data * data;
  char s1[100], s2[100];

  msolptr = NULL;
  data = (PAMPA_MMG3D_Data *) dataptr;

  if (solflag != 0) {
    CHECK_FDBG2 (pmesh2rmesh (meshptr, data, PAMPA_MMG3D_FACES, &mmshptr, &msolptr)); // XXX temporaire pour le flag en dur
  }
  else {
    CHECK_FDBG2 (pmesh2rmesh (meshptr, data, 0, &mmshptr, NULL));
  }

  sprintf (s1, "%s.mesh", fileval);
  CHECK_FDBG2 (MMG5_Set_outputMeshName(mmshptr, s1) != 1);
  if (solflag != 0) {
    sprintf (s2, "%s.sol", fileval);
    CHECK_FDBG2 (MMG5_Set_outputSolName(mmshptr, msolptr, s2) != 1);
  }
  if (reftab != NULL) {
    MMG3D_int v[4];
    CHECK_FDBG2 (MMG3D_Get_meshSize (mmshptr, NULL, &tetrnbr, NULL, NULL, NULL, NULL) != 1);
    for(tetrnum = 1, tetrnnd = tetrnbr + 1 ; tetrnum < tetrnnd ; tetrnum ++) {
      CHECK_FDBG2 (MMG3D_Get_tetrahedron (mmshptr, &v[0], &v[1], &v[2], &v[3], NULL, NULL) != 1);
      CHECK_FDBG2 (MMG3D_Set_tetrahedron (mmshptr, v[0], v[1], v[2], v[3], reftab[tetrnum - 1] + 2, tetrnum) != 1); /* If value is -2 */
    }
  }

  MMG3D_hashTetra(mmshptr, 0);
  if ((solflag != 0) && (data->infoprt != 0)) {
    _MMG3D_outqua(mmshptr, msolptr);
    _MMG3D_prilen(mmshptr, msolptr);
  }
  MMG3D_saveMesh (mmshptr, s1);
  if (solflag != 0) {
    MMG3D_saveSol (mmshptr, msolptr, s2);
  }
  MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);
  return (0);
}
