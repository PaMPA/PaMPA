/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pampa-mmg3d-bin2.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 23 May 2017
//!                             to:   22 Sep 2017
//!
/************************************************************/
typedef struct GlbData_ {
  PAMPA_Num extloop;
  char * cmin;
  char * cmout;
  PAMPA_MMG3D_Data data;
} GlbData;

void usagePrint (FILE * const stream, const char ** const data);
int parsopt (char **argv, int argc, MPI_Comm comm, int rank, int size, GlbData *data) ;
void printopt (GlbData *data) ;
int loadmesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) ;
int savemesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) ;
