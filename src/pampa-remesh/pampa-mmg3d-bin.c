/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pampa-mmg3d-bin.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 23 May 2017
//!                             to:   22 Sep 2017
//!
/************************************************************/
#include <mpi.h>
#include <common.h>
#include <module.h>
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include <pampa-mmg3d.h>
#ifdef MSHINT
#include <pampa-mshint.h>
#endif /* MSHINT */
#include <pampa-mmg3d-bin.h>
#include <fcntl.h>
#include <ptscotch.h>

static const char *         C_usageList[] = {     /* Usage */
  "pampa-mmg3d-bin [PaMPA options] + [mmg3d options]",
  "",
  "PaMPA options are:",
  " mesh infile (ext .mesh) is required as:",
  "  -i, --cm-in <file>       : centralized mesh infile",
  "  -l, --dp-in <file>       : distributed PaMPA infile",
  "",
  " mesh outfile (ext .mesh) is required as:",
  "  -o, --cm-out <file>      : centralized mesh outfile",
  "  -O, --dm-out <file>      : distributed mesh outfile [with @p replaced by",
  "                             the number of processors and @r by the rank]",
  "  -s, --dp-out <file>      : distributed PaMPA outfile",
  "",
  " mesh options are:",
  "  -a, --alpha <valuee>     : metric is multiplied by alpha value",
  "  -B, --e-band <value>     : band for external loop or at the beginning if",
  "                             no external loop (default is 3)",
  "  -c, --compute            : compute the number of zones for the first",
  "                             iteration",
  "  -C, --bounding-box       : specify the bounding box if the zone parallel",
  "                             identifying is mbGeompart. The format is",
  "                             x:<min>,<max>,y:<min>,<max>,z:<min>,<max>",
  "  -d, --debug <file>       : save as PaMPA file the distributed mesh at each",
  "                             iteration [with @p replaced by",
  "                             the number of processors and @r by the rank,",
  "                             @i and @e are required, with @e before @i]",
  "  -e, --emin <value>       : edge length minimum (default is 0.6)",
  "  -E, --emax <value>       : edge length maximum (default is 1.5)",
  "  -b, --i-band <value>     : band for internal loop (default is 3)",
  "  -m, --ident-meth <value> : method for zone parallel identifying (default",
  "                             is mbpart, other choices are mbGeompart,",
  "                             mbMeshGrow, gbCoarsen, gbCoarsenGrow,",
  "                             gbGraphGrow)",
  "  -p, --part-meth <value>  : method for mesh partitioning (default is",
  "                             partElem, other choice is partVert)",
  "  -q, --qmin <value>       : element quality minimum (default is 0.9)",
  "  -r, --rmin <value>       : minimum ratio of elements which need to be",
  "                             remeshed to start another iteration on external",
  "                             loop only (default is 0.1)",
  "  -w, --zweight <value>    : weight for zone (default is 600 000)",
  "  -x, --ext-loop           : with external loop in order to check",
  "                             user-defined criteria (by default this option is",
  "                             not included)",
  "  -z, --zones <value>      : number of zones (by default the number is",
  "                             computed)",
  "",
  " other options are:",
  "  -h, --help               : print this menu",
  "  -v, --print-opt          : print option values",
  "  -f, --log <file>         : each processor will print output in the file",
  "                             [with @p replaced by the number of processors",
  "                             and @r by the rank]",
  "  -t, --print-scm          : print version of each used program",
  "",
  NULL };

#define FILL_DBL(s,d)    \
  if (! strcmp (s, "*")) \
    d = (double) atof (s);


#define CHECK_ARG(s,i,c)  { \
  if (((i) >= (c)) || ((s)[i][0] == '-')) { \
    errorPrint ("missing or invalid argument for option %s", (s[i - 1])); \
    errorPrint ("Try option --help for more information"); \
    return (1); \
  } \
}

#define UNK_ARG(s)  { \
  errorPrint ("unknown option %s", (s)); \
  errorPrint ("Try option --help for more information"); \
  return (1); \
}

  void
usagePrint (
    FILE * const                stream,
    const char ** const         data)
{
  const char **       cptr;

  fprintf (stream, "\nUsage is:\n");
  for (cptr = data; *cptr != NULL; cptr ++)
    fprintf (stream, "  %s\n", *cptr);
}

static int parsstr (char *src, char ** dst, int rank, int size) {
  char * ppos, * rpos;

  *dst = malloc ((strlen (src) + 1 + (int) log10(size)) * sizeof (char));
  ppos = strstr(src, "@p");
  if (ppos != NULL) {
    ppos[0] = '%';
    ppos[1] = 'd';
  }
  rpos = strstr(src, "@r");
  if (rpos != NULL) {
    rpos[0] = '%';
    rpos[1] = 'd';
  }
  if ((ppos != NULL) && (rpos != NULL)) {
    if (ppos < rpos) 
      sprintf(*dst, src, size, rank);
    else
      sprintf(*dst, src, rank, size);
  }
  else if (ppos != NULL) 
    sprintf(*dst, src, size);
  else if (rpos != NULL) 
    sprintf(*dst, src, rank);
  else
    sprintf(*dst, src);
  return (0);
}

int parsopt (char **argv, int argc, MPI_Comm comm, int rank, int size, GlbData *data) {
  MMG5_pMesh mesh;
  MMG5_pSol sol;
  int retval;
  int i;
  int print;
  int argp;
  int optbin[1];

  print = 0;
  data->log = 
    data->cmin =
    data->cmout =
    data->dmout =
    data->dpin =
    data->dpout =
    data->debug = NULL;
  data->alpha = 1.0;
  data->iband = 3;
  data->zweight = 600000;
  data->eband = 3;
  data->extloop = 0;
  data->data.edgemin = 0.6;
  data->data.edgemax = 1.5;
  data->data.qualmin = 0.9;
  data->rmin = 0.1;
  data->comp = 0;
  data->scm = 0;
  data->part = "partElem";
  data->ident = "mbPart";
  data->bndfg = 0;
  data->minx = DBL_MAX;
  data->miny = DBL_MAX;
  data->minz = DBL_MAX;
  data->maxx = - DBL_MAX;
  data->maxy = - DBL_MAX;
  data->maxz = - DBL_MAX;
  data->zones = ~0;

  for (argp = -1, i=1; (i < argc) && (argp == -1); i ++) 
    if (!strcmp(argv[i], "+"))
      argp = i;
    else if ((!strcmp(argv[i], "--help")) || (!strcmp(argv[i], "-h"))) {
      usagePrint (stdout, C_usageList);
      MMG3D_usage ("mmg3d");
      exit (0);
    }

  if (argp == -1) {
    errorPrint ("'+' is missing");
    exit (-1);
  }

  for (i=1; i < argp; i ++) {
    char * ipos, * epos;
    char minxstr[20], maxxstr[20], minystr[20], maxystr[20], minzstr[20], maxzstr[20];

    if ((strlen(argv[i]) > 2) && (argv[i][0] == '-') && (argv[i][1] == '-')) 
      switch (argv[i][2]) {
        case 'a' :
          if (!strcmp(argv[i], "--alpha")) {
            CHECK_ARG(argv, i + 1, argc);
            data->alpha = (double) atof(argv[++ i]);
          }
          break;
        case 'b' :
          if (!strcmp(argv[i], "--bounding-box")) {
            CHECK_ARG(argv, i + 1, argc);
            data->bndfg = 0;
            sscanf (argv[++ i], "\"x:%s,%s;y:%s,%s;z:%s,%s\"", minxstr, maxxstr, minystr, maxystr, minzstr, maxzstr);
            FILL_DBL (minxstr, data->minx);
            FILL_DBL (maxxstr, data->maxx);
            FILL_DBL (minystr, data->miny);
            FILL_DBL (maxystr, data->maxy);
            FILL_DBL (minzstr, data->minz);
            FILL_DBL (maxzstr, data->maxz);
          }
          break;
        case 'c' :
          if (!strcmp(argv[i], "--cm-in")) {
            CHECK_ARG(argv, i + 1, argc);
            data->cmin = argv[++ i];
          }
          else if (!strcmp(argv[i], "--cm-out")) {
            CHECK_ARG(argv, i + 1, argc);
            data->cmout = argv[++ i];
          }
          else if (!strcmp(argv[i], "--compute")) {
            data->comp = 1;
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'd' :
          if (!strcmp(argv[i], "--dp-in")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->dpin, rank, size);
          }
          else if (!strcmp(argv[i], "--dp-out")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->dpout, rank, size);
          }
          else if (!strcmp(argv[i], "--debug")) {

            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->debug, rank, size);
            ipos = strstr(data->debug, "@i");
            epos = strstr(data->debug, "@e");
            if (ipos == NULL) {
              errorPrint ("@i is missing for iteration number (inner loop)");
              return (1);
            }
            if (epos == NULL) {
              errorPrint ("@e is missing for iteration number (outer loop)");
              return (1);
            }
            if (ipos < epos) {
              errorPrint ("@e must be before @i");
              return (1);
            }
            ipos[0] = '%';
            ipos[1] = 'd';
            epos[0] = '%';
            epos[1] = 'd';
          }
          else if (!strcmp(argv[i], "--dm-out")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->dmout, rank, size);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'e' :
          if (!strcmp(argv[i], "--ext-loop")) {
            data->extloop = 1;
          }
          else if (!strcmp(argv[i], "--e-band")) {
            CHECK_ARG(argv, i + 1, argc);
            data->eband = (PAMPA_Num) atoi(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--emin")) {
            CHECK_ARG(argv, i + 1, argc);
            data->data.edgemin = atof(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--emax")) {
            CHECK_ARG(argv, i + 1, argc);
            data->data.edgemax = atof(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'i' :
          if (!strcmp(argv[i], "--i-band")) {
            CHECK_ARG(argv, i + 1, argc);
            data->iband = (PAMPA_Num) atoi(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--ident-meth")) {
            CHECK_ARG(argv, i + 1, argc);
            data->ident = argv[++ i];
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'l' :
          if (!strcmp(argv[i], "--log")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->log, rank, size);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'p' :
          if (!strcmp(argv[i], "--print-opt")) {
            print = 1;
          }
          else if (!strcmp(argv[i], "--print-scm")) {
            data->scm = 1;
          }
          else if (!strcmp(argv[i], "--part-meth")) {
            CHECK_ARG(argv, i + 1, argc);
            data->part = argv[++ i];
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'q' :
          if (!strcmp(argv[i], "--qmin")) {
            CHECK_ARG(argv, i + 1, argc);
            data->data.qualmin = atof(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'r' :
          if (!strcmp(argv[i], "--rmin")) {
            CHECK_ARG(argv, i + 1, argc);
            data->rmin = atof(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'z' :
          if (!strcmp(argv[i], "--zweight")) {
            CHECK_ARG(argv, i + 1, argc);
            data->zweight = (PAMPA_Num) atoi(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--zones")) {
            CHECK_ARG(argv, i + 1, argc);
            data->zones = (PAMPA_Num) atoi(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        default:
          UNK_ARG(argv[i]);
          break;

      }
    else if ((strlen(argv[i]) == 2) && (argv[i][0] == '-')) 
      switch (argv[i][1]) {
        case 'a' :
          CHECK_ARG(argv, i + 1, argc);
          data->alpha = (double) atof(argv[++ i]);
          break;
        case 'b' :
          CHECK_ARG(argv, i + 1, argc);
          data->iband = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'c' :
          data->comp = 1;
          break;
        case 'C' :
          CHECK_ARG(argv, i + 1, argc);
          data->bndfg = 1;
          sscanf (argv[++ i], "x:%lf,%lf,y:%lf,%lf,z:%lf,%lf", &data->minx, &data->maxx, &data->miny, &data->maxy, &data->minz, &data->maxz);
          //FILL_DBL (minxstr, data->minx);
          //FILL_DBL (maxxstr, data->maxx);
          //FILL_DBL (minystr, data->miny);
          //FILL_DBL (maxystr, data->maxy);
          //FILL_DBL (minzstr, data->minz);
          //FILL_DBL (maxzstr, data->maxz);
          break;
        case 'd' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->debug, rank, size);
          ipos = strstr(data->debug, "@i");
          epos = strstr(data->debug, "@e");
          if (ipos == NULL) {
            errorPrint ("@i is missing for iteration number (inner loop)");
            return (1);
          }
          if (epos == NULL) {
            errorPrint ("@e is missing for iteration number (outer loop)");
            return (1);
          }
          if (ipos < epos) {
            errorPrint ("@e must be before @i");
            return (1);
          }
          ipos[0] = '%';
          ipos[1] = 'd';
          epos[0] = '%';
          epos[1] = 'd';
          break;
        case 'e' :
          CHECK_ARG(argv, i + 1, argc);
          data->data.edgemin = atof(argv[++ i]);
          break;
        case 'f' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->log, rank, size);
          break;
        case 'i' :
          CHECK_ARG(argv, i + 1, argc);
          data->cmin = argv[++ i];
          break;
        case 'l' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->dpin, rank, size);
          break;
        case 'm' :
          CHECK_ARG(argv, i + 1, argc);
          data->ident = argv[++ i];
          break;
        case 'o' :
          CHECK_ARG(argv, i + 1, argc);
          data->cmout = argv[++ i];
          break;
        case 'p' :
          CHECK_ARG(argv, i + 1, argc);
          data->part = argv[++ i];
          break;
        case 'q' :
          CHECK_ARG(argv, i + 1, argc);
          data->data.qualmin = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'r' :
          CHECK_ARG(argv, i + 1, argc);
          data->rmin = atof(argv[++ i]);
          break;
        case 's' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->dpout, rank, size);
          break;
        case 't' :
          data->scm = 1;
          break;
        case 'v' :
          print = 1;
          break;
        case 'w' :
          CHECK_ARG(argv, i + 1, argc);
          data->zweight = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'x' :
          data->extloop = 1;
          break;
        case 'z' :
          CHECK_ARG(argv, i + 1, argc);
          data->zones = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'B' :
          CHECK_ARG(argv, i + 1, argc);
          data->eband = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'E' :
          CHECK_ARG(argv, i + 1, argc);
          data->data.edgemax = atof(argv[++ i]);
          break;
        case 'O' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->dmout, rank, size);
          break;
        default:
          UNK_ARG(argv[i]);
          break;
      }
    else
      UNK_ARG(argv[i]);
  }

  mesh = NULL;
  sol = NULL;
  MMG3D_Init_mesh (MMG5_ARG_start, MMG5_ARG_ppMesh, &mesh, MMG5_ARG_ppMet, &sol, MMG5_ARG_end);
  //CHECK_FERR (MMG5_Set_inputMeshName(mesh, argv[0]), comm);
  //CHECK_FERR (MMG5_Set_inputSolName(mesh, sol, argv[0]), comm);
  retval = MMG3D_parsar(argc - argp, argv + argp, mesh, sol) ; // XXX tester la valeur de retour
  MMG3D_destockOptions(mesh, &data->data.infoval) ;
  MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mesh, MMG5_ARG_ppMet, &sol, MMG5_ARG_end);

  if (((data->dpin == NULL) && (data->cmin == NULL)) || ((data->comp == 0) && (data->dpout == NULL) && (data->cmout == NULL) && (data->dmout == NULL))) {
    errorPrint ("Mesh infile or mesh outfile is missing");
    usagePrint (stdout, C_usageList);
    return (1);
  }
  else if ((data->cmin != NULL) && ((data->dpin != NULL))) {
    errorPrint ("Too many mesh infiles");
    usagePrint (stdout, C_usageList);
    return (1);
  }
  else if (print != 0)
    printopt(data);
  
  return (1 - retval);
}

void printopt (GlbData *data) {
  infoPrint("options:\n\tcm-in: %s\n\tcm-out: %s\n\tdm-out: %s\n\n\talpha: %lf\n\te-band: %d\n\temin: %f\n\temax: %f\n\text-loop: %d\n\ti-band: %d\n\tident-meth: %s\n\tpart-meth: %s\n\tqmin: %lf\n\trmin: %f\n\tzweight: %d\n\n\tlog: %s\n\tprint-scm: %d\n\tbounding-box: %d\n\tminx: %lf\n\tmaxx: %lf\n\tminy: %lf\n\tmaxy: %lf\n\tminz: %lf\n\tmaxz: %lf\n", 
      data->cmin,
      data->cmout,
      data->dmout,
      data->alpha,
      data->eband,
      data->data.edgemin,
      data->data.edgemax,
      data->extloop,
      data->iband,
      data->ident,
      data->part,
      data->data.qualmin,
      data->rmin,
      data->zweight,
      data->log,
      data->scm,
      data->bndfg,
      data->minx,
      data->maxx,
      data->miny,
      data->maxy,
      data->minz,
      data->maxz);
}

int loadmesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) {
  PAMPA_Mesh m;
  PAMPA_Dmesh dm;
  PAMPA_Strat strat;
  PAMPA_Num vertlocnbr, *partloctab, ovlpglbval, typenbr;
  PAMPA_Num * entttab, * tagtab;
  MPI_Datatype * typetab;

  ovlpglbval = 0;
  data->data.tetrent = 0; // entity number for main entity
  data->data.faceent = 1; // entity number for faces
  if (data->data.faceent == 1)
    data->data.nodeent = 2; // entity number for nodes
  else
    data->data.nodeent = 1; // entity number for nodes

  data->data.infoprt = 0;

  // XXX pas au bon endroit ces tableaux
  // Fill in arrays for attached values
  typenbr = 9;
  entttab = malloc (sizeof (PAMPA_Num) * typenbr);
  tagtab = malloc (sizeof (PAMPA_Num) * typenbr);
  typetab = malloc (sizeof (MPI_Datatype) * typenbr);

  entttab[0] = 
    entttab[1] =
    entttab[2] =  data->data.nodeent;
  entttab[3] =  data->data.tetrent;
  entttab[4] =  data->data.nodeent;
  entttab[5] =  PAMPA_ENTT_VIRT_VERT;
  entttab[6] =  data->data.tetrent;
  entttab[7] =  data->data.faceent;
  entttab[8] =  data->data.faceent;
  tagtab[0] = PAMPA_TAG_GEOM;
  tagtab[1] = PAMPA_TAG_REF;
  tagtab[2] = PAMPA_TAG_SOL_3DI;
  tagtab[3] = PAMPA_TAG_REMESH;
  tagtab[4] = PAMPA_TAG_SOL_3DAI;
  tagtab[5] = 33; /* partitioning, FIXME mettre un define */
  tagtab[6] = PAMPA_TAG_REF;
  tagtab[7] = PAMPA_TAG_REF;
  tagtab[8] = PAMPA_TAG_REQU;
  MPI_Type_contiguous(3, MPI_DOUBLE, &typetab[0]);
  MPI_Type_commit(&typetab[0]);
  typetab[1] = PAMPA_MPI_NUM;
  typetab[2] = MPI_DOUBLE;
  typetab[3] = PAMPA_MPI_NUM;
  MPI_Type_contiguous(6, MPI_DOUBLE, &typetab[4]);
  MPI_Type_commit(&typetab[4]);
  typetab[5] = PAMPA_MPI_NUM;
  typetab[6] = PAMPA_MPI_NUM;
  typetab[7] = PAMPA_MPI_NUM;
  typetab[8] = PAMPA_MPI_NUM;

  data->data.coortyp = typetab[0];

  // Initialisation of PaMPA dmesh structure
  CHECK_FERR(PAMPA_dmeshInit(&dm, MPI_COMM_WORLD), comm);

  if (data->cmin != NULL) {
  if (rank == 0) {
    // Initialisation of PaMPA mesh structure
    CHECK_FERR(PAMPA_meshInit(&m), comm);

    // Build PaMPA mesh
    CHECK_FDBG2(PAMPA_MMG3D_meshLoad(&m, data->cmin, &data->data, (data->data.faceent == ~0) ? (0) : (PAMPA_MMG3D_FACES)));

    //// XXX début temporaire
    //{
    //        File f;
    //        f.name = data->cmout; // XXX temporaire
    //        f.mode = "w";
    //        f.pntr = stdout;
    //        CHECK_FERR (fileBlockOpen (&f, 1), comm);
    //        CHECK_FERR (PAMPA_meshSave (&m, f.pntr), comm);
    //        CHECK_FERR (fileBlockClose (&f, 1), comm);
    //}
    //// XXX fin temporaire
    
    //// XXX début temporaire
    //  {
    //    FILE *f1, *f2;
    //    char s[100];
    //    sprintf (s, "%s.mesh", data->cmout);
    //    f1 = fopen (s, "w");
    //    sprintf (s, "%s.sol", data->cmout);
    //    f2 = fopen (s, "w");
    //    PAMPA_meshMeshSave (&m, f1, f2);
    //    fclose (f1);
    //    fclose (f2);
    //  }
    //// XXX fin temporaire
    //// XXX debut temporaire
    //PAMPA_Num tetrnbr = 0;
    //PAMPA_Iterator it;
    //PAMPA_meshItInitStart(&m, data->data.tetrent, &it);
    //while (PAMPA_itHasMore(&it)) {
    //  PAMPA_Num tetnum;
    //  PAMPA_Num nghbnbr;

    //  tetnum = PAMPA_itCurEnttVertNum(&it);

    //  PAMPA_meshVertData (&m, tetnum, data->data.tetrent, data->data.tetrent, &nghbnbr, NULL);
    //  if (nghbnbr != 4) {
    //    if (nghbnbr < 3)
    //      infoPrint ("tet %d has %d neighbors", tetnum, nghbnbr);
    //    //else
    //      tetrnbr ++;
    //  }

    //  PAMPA_itNext(&it);
    //}
    //infoPrint ("total of boundary elements: %d", tetrnbr);
    //// XXX fin temporaire




    // Scatter PaMPA mesh
    CHECK_FERR(PAMPA_dmeshScatter2(&dm, &m, typenbr, entttab, tagtab, typetab, ovlpglbval), comm);

    // Finalisation of PaMPA mesh structure
    PAMPA_meshExit(&m);
  }
  else {
    // Scatter PaMPA mesh
    CHECK_FERR(PAMPA_dmeshScatter2(&dm, NULL, typenbr, entttab, tagtab, typetab, ovlpglbval), comm);
  }

  //MPI_Finalize() ;

  //exit(EXIT_SUCCESS);
  }
  else if (data->dpin != NULL) {
    PAMPA_Num baseval,vertglbnbr,vertlocnbr;
    File f;
    f.name = data->dpin;
    f.mode = "r";
    f.pntr = stdin;
    CHECK_FERR (fileBlockOpen (&f, 1), comm);
    CHECK_FERR(PAMPA_dmeshInit(dmesh, MPI_COMM_WORLD), comm);
    CHECK_FERR (PAMPA_dmeshLoad (dmesh, f.pntr, -1, 0), comm);
    //PAMPA_dmeshData ( &dm, &baseval, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    //PAMPA_dmeshSize ( dmesh, NULL, &vertlocnbr, NULL, &vertglbnbr);
    //infoPrint ("vertglb: " PAMPA_NUMSTRING ", vertloc: " PAMPA_NUMSTRING, vertglbnbr, vertlocnbr);
    //PAMPA_dmeshEnttSize ( dmesh, 2, &vertlocnbr, PAMPA_VERT_LOCAL, NULL);
    //{
    //        double * geomtab;
    //        PAMPA_Num vertlocnum;
    //        PAMPA_dmeshValueData (dmesh, 2, PAMPA_TAG_GEOM, (void **) &geomtab);
    //        for (vertlocnum = 0; vertlocnum < vertlocnbr; vertlocnum ++) {
    //    	    infoPrint ("coor[" PAMPA_NUMSTRING "] = (%lf,%lf,%lf)", vertlocnum, geomtab[vertlocnum * 3 + 0], geomtab[vertlocnum * 3 + 1], geomtab[vertlocnum * 3 + 2]);
    //        }
    //}
    //MPI_Allreduce (&vertlocnbr, &vertglbnbr, 1, PAMPA_MPI_NUM, MPI_SUM, MPI_COMM_WORLD);
    //infoPrint ("vertglb: " PAMPA_NUMSTRING ", vertloc: " PAMPA_NUMSTRING, vertglbnbr, vertlocnbr);
    CHECK_FERR (fileBlockClose (&f, 1), comm);
    // XXX tester avant si les données sont associées
    //CHECK_FERR (PAMPA_dmeshValueUnlink(&dm, baseval, PAMPA_TAG_REMESH), comm);
    //CHECK_FERR (PAMPA_dmeshValueUnlink(&dm, baseval, PAMPA_TAG_WEIGHT), comm);

  }
  else {
    errorPrint ("Invalid read");
    return (1);
  }

  MPI_Type_free (&typetab[4]);
  free (entttab);
  free (tagtab);
  free (typetab);

  if (data->dpin == NULL) {
#ifdef PAMPA_DEBUG_DMESH
    CHECK_FERR (PAMPA_dmeshCheck (&dm), comm);
#endif /* PAMPA_DEBUG_DMESH */

    // Initialisation of PaMPA strat structure
    CHECK_FERR(PAMPA_stratInit(&strat), comm);

    PAMPA_dmeshSize(&dm, NULL, &vertlocnbr, NULL, NULL);

    partloctab = malloc (sizeof (PAMPA_Num) * vertlocnbr);

    // Partitioning PaMPA Dmesh
    CHECK_FERR(PAMPA_dmeshPart(&dm, (PAMPA_Num) size, &strat, partloctab), comm);
    PAMPA_stratExit(&strat);



    // Initialisation of PaMPA dmesh structure
    CHECK_FERR(PAMPA_dmeshInit(dmesh, MPI_COMM_WORLD), comm);

    // Redistribute PaMPA Dmesh
    CHECK_FERR(PAMPA_dmeshRedist(&dm, partloctab, NULL, -1, -1, -1, dmesh), comm);
    free (partloctab);
    PAMPA_dmeshExit(&dm);
  }

#ifdef PAMPA_DEBUG_DMESH
  CHECK_FERR (PAMPA_dmeshCheck (dmesh), comm);
#endif /* PAMPA_DEBUG_DMESH */

  return (0);
}

int savemesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) {

  if (data->dmout != NULL)
    CHECK_FERR(PAMPA_dmeshSave2 (dmesh, data->dmout, PAMPA_MMG3D_meshSave, (void * const) &data->data), comm);

  if (data->cmout != NULL) {
    File meshstm = { "a" };
    File solstm = { "a" };
    char s1[100];
    char s2[100];

    fileBlockInit (&meshstm, 1);
    fileBlockInit (&solstm, 1);
    sprintf (s1, "%s.mesh", data->cmout);
    meshstm.name = s1;
    sprintf (s2, "%s.sol", data->cmout);
    solstm.name = s2;
    PAMPA_dmeshMeshSave (dmesh, &meshstm, &solstm);
  }
  if (data->dpout != NULL) {
    File f;
    f.name = data->dpout;
    f.mode = "w";
    f.pntr = stdout;
    CHECK_FERR (fileBlockOpen (&f, 1), comm);
    CHECK_FERR (PAMPA_dmeshSave (dmesh, f.pntr), comm);
    CHECK_FERR (fileBlockClose (&f, 1), comm);
  }

  return (0);
}

int freemem (GlbData * data) {
  MPI_Type_free(&data->data.coortyp);
  if (data->dpout != NULL)
    free (data->dpout);
  if (data->dpin != NULL)
    free (data->dpin);
  if (data->debug != NULL)
    free (data->debug);
  if (data->dmout != NULL)
    free (data->dmout);
  if (data->log != NULL)
    free (data->log);

  return (0);
}

int main(int argc, char** argv){
  PAMPA_Dmesh dmesh, dmesh2;
  PAMPA_AdaptInfo infoptr;
  PAMPA_Num vertglbnbr;
  GlbData data;
  int rank, retval, size, bak, new;
  MPI_Comm comm;
  int  namelen;
  char procnam[MPI_MAX_PROCESSOR_NAME] ;

  comm = MPI_COMM_WORLD;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &retval);
  // XXX tester retval
  MPI_Comm_rank(comm, &rank) ;
  MPI_Comm_size(comm, &size) ;
  MPI_Get_processor_name(procnam,&namelen);

  CHECK_FERR (parsopt(argv, argc, comm, rank, size, &data), comm);

  if (data.log != NULL) {
    fflush (stdout);
    bak = dup (1);
    new = open(data.log, O_CREAT|O_WRONLY|O_TRUNC, 0600);
    dup2 (new, 1);
  }
  printf ("Processus %d, host: %s, with MPI rank %d on %d\n", getpid(), procnam, rank, size);

  CHECK_FERR (loadmesh (&data, &dmesh, comm, rank, size), comm);
  if (rank == 0) {
    PAMPA_printVersions ();
    PAMPA_dmeshEnttSize(&dmesh, data.data.tetrent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("At the beginning\n\tNumber of tetrahedra: %d\n", vertglbnbr);
    PAMPA_dmeshEnttSize(&dmesh, data.data.nodeent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("\tNumber of nodes: %d\n", vertglbnbr);
  }

  infoptr.rank = rank;
  infoptr.alpha = data.alpha;
  infoptr.flagval = (data.data.faceent == ~0) ? (0) : (PAMPA_MMG3D_FACES);
  infoptr.flagval |= PAMPA_MMG3D_PERIODIC;
  infoptr.zonenb2 = data.zones;
  infoptr.zonenbr = ~0;
  infoptr.estmflg = data.comp;
  infoptr.itdebug = data.debug;
  infoptr.methval = data.ident;
  infoptr.ibndval = data.iband;
  infoptr.ballsiz = data.zweight;
  infoptr.ebndval = data.eband;
  infoptr.loopval = data.extloop;
  infoptr.ratemin = data.rmin;
  infoptr.bndgflg = data.bndfg;
  infoptr.minxval = data.minx;
  infoptr.maxxval = data.maxx;
  infoptr.minyval = data.miny;
  infoptr.maxyval = data.maxy;
  infoptr.minzval = data.minz;
  infoptr.maxzval = data.maxz;
  infoptr.EXT_meshAdapt = &PAMPA_MMG3D_meshAdapt;
#ifdef MSHINT 
  infoptr.EXT_meshInt = &PAMPA_MSHINT_meshInt;
#endif /* MSHINT */
  infoptr.EXT_dmeshCheck = &PAMPA_MMG3D_dmeshCheck;
  infoptr.EXT_dmeshBand = &PAMPA_MMG3D_dmeshBand;
  infoptr.EXT_dmeshWeightCompute = &PAMPA_MMG3D_dmeshWeightCompute;
  infoptr.EXT_dmeshMetricCompute = &PAMPA_MMG3D_dmeshMetricCompute;
  infoptr.EXT_meshSave = &PAMPA_MMG3D_meshSave;
  infoptr.dataptr = &data.data;
  CHECK_FERR (PAMPA_dmeshAdapt (&dmesh, &infoptr, &dmesh2), comm);
  if (data.comp == 0) {
  if (rank == 0) {
    PAMPA_dmeshEnttSize(&dmesh, data.data.tetrent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("At the end\n\tNumber of tetrahedra: %d\n", vertglbnbr);
    PAMPA_dmeshEnttSize(&dmesh, data.data.nodeent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("\tNumber of nodes: %d\n", vertglbnbr);
  }
  CHECK_FERR (savemesh (&data, &dmesh, comm, rank, size), comm);
  }
  //{
  //  SCOTCH_Dgraph dgdat;
  //  char s[100];

  //  CHECK_FERR (SCOTCH_dgraphInit (&dgdat, comm), comm);
  //  PAMPA_dmeshDgraph (&dmesh, &dgdat);
  //    PAMPA_dmeshExit(&dmesh);
  //  sprintf (s, "dg-%d-%d.grf.bz2", size, rank);
  //  File f;
  //  f.name = s;
  //  f.mode = "w";
  //  f.pntr = stdin;
  //  CHECK_FERR (fileBlockOpen (&f, 1), comm);
  //  CHECK_FERR (SCOTCH_dgraphCheck (&dgdat), comm);
  //  CHECK_FERR (SCOTCH_dgraphSave (&dgdat, f.pntr), comm);
  //  CHECK_FERR (fileBlockClose (&f, 1), comm);
  //      SCOTCH_dgraphExit (&dgdat);
  //}

  //PAMPA_dmeshExit(&dmesh);
  CHECK_FERR (freemem (&data), comm);

  if (data.log != NULL) {
    if (rank == 0)
      printf("fin du prog\n");
    fflush (stdout);
    dup2 (bak, 1);
    close (bak);
  }

  if (rank == 0)
    printf("fin du prog\n");

  MPI_Finalize() ;

  return EXIT_SUCCESS;
}
