/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mscat.c
//!
//!   \authors     Cedric Lachat
//!                Francois Pellegrini
//!
//!
//!   \date        Version 1.0: from: 23 May 2017
//!                             to:   25 Sep 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define MSCAT

#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include "mscat.h"

/*
**  The static variables.
*/

static int                  C_paraNum = 0;        /* Number of parameters       */
static int                  C_fileNum = 0;        /* Number of file in arg list */
static File                 C_fileTab[C_FILENBR] = { /* File array              */
                              { "r" },
                              { "w" } };

static const char *         C_usageList[] = {     /* Usage */
  "mscat <nparts> <input source file> <output target file pattern> <options>",
  "  -h  : Display this help",
  "  -V  : Print program version and copyright",
  NULL };

// XXX debut partie redondante avec dmesh_io_save.c
static inline int valueScan (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  int chekval;

  chekval = 0;

  if (!strcmp (format, "%d\t")) 
    chekval |= (fscanf (stream, format, ((int *) value)) == EOF);
  else if (!strcmp (format, "%.16e\t")) 
    chekval |= (fscanf (stream, "%lf", ((double *) value)) == EOF);
  else if (!strcmp (format, "%lld\t")) 
    chekval |= (fscanf (stream, format, ((int64_t *) value)) == EOF);
  else {
    errorPrint ("Unrecognized format: %s", format);
    return (1);
  }

  return (chekval);
}
 
static inline int valuePrint (
    FILE * const stream,
    char *       format,
    byte *       value)
{
  if (!strcmp (format, "%d\t")) 
    fprintf (stream, format, *((int *) value));
  else if (!strcmp (format, "%.16e\t")) 
    fprintf (stream, format, *((double *) value));
  else if (!strcmp (format, "%lld\t")) 
    fprintf (stream, format, *((int64_t *) value));
  else {
    errorPrint ("Unrecognized format: %s", format);
    return (1);
  }

  return (0);
}
// XXX fin partie redondante avec dmesh_io_save.c


/******************************/
/*                            */
/* This is the main function. */
/*                            */
/******************************/

int
main (
int                         argc,
char *                      argv[])
{
  PAMPA_Num          p[1] = { 1 };               /* Number of parts */
  int                 i;
  PAMPA_Num * pnbrtab;

  MPI_Init (&argc, &argv);

  errorProg ("mscat");

  if ((argc >= 2) && (argv[1][0] == '?')) {       /* If need for help */
    //usagePrint (stdout, C_usageList);
    errorPrint ("XXX pas bien temporaire");
    return     (0);
  }

  fileBlockInit (C_fileTab, C_FILENBR);           /* Set default stream pointers */

  for (i = 1; i < argc; i ++) {                   /* Loop for all option codes                        */
    if ((argv[i][0] != '-') || (argv[i][1] == '\0') || (argv[i][1] == '.')) { /* If found a file name */
      if (C_paraNum < 1) {                        /* If number of parameters not reached              */
        if ((p[C_paraNum ++] = atoi (argv[i])) < 1) { /* Get number of parts                          */
          errorPrint ("invalid number of parts '%s'", argv[i]);
          return     (1);
        }
        continue;                                 /* Process the other parameters */
      }
      if (C_fileNum < C_FILEARGNBR)               /* A file name has been given */
        fileBlockName (C_fileTab, C_fileNum ++) = argv[i];
      else {
	      // XXX tmp
        //errorPrint ("too many file names given");
        //return     (1);
      }
    }
    else {                                        /* If found an option name */
      switch (argv[i][1]) {
        case 'H' :                                /* Give the usage message */
        case 'h' :
          //usagePrint (stdout, C_usageList);
          errorPrint ("XXX pas bien temporaire");
          return     (0);
        case 'V' :
          fprintf (stderr, "mscat, version " PAMPA_VERSION_STRING "\n");
          fprintf (stderr, "Copyright XXX\n");
          fprintf (stderr, "This software is XXX\n");
          return  (0);
        default :
          errorPrint ("unprocessed option '%s'", argv[i]);
          return     (1);
      }
    }
  }

  if (argc == 5) {
      FILE * fileptr;
      PAMPA_Num partnum;

      fileptr = fopen ("parttab", "r");

      if ((pnbrtab = (PAMPA_Num *) memAlloc (p[0] * sizeof (PAMPA_Num))) == NULL) {
	errorPrint  ("out of memory");
	return      (1);
      }
      for (partnum = 0; partnum < p[0]; partnum ++) {
        intLoad (fileptr, pnbrtab + partnum);
      }
      fclose (fileptr);
  }
  else
    pnbrtab = NULL;

  fileBlockOpen (C_fileTab, 1);                   /* Open input mesh file */

  C_meshScat (C_filepntrsrcinp, p[0], C_filenamesrcout, pnbrtab);

  fileBlockClose (C_fileTab, 1);                  /* Always close explicitely to end eventual (un)compression tasks */

#ifdef COMMON_PTHREAD
  pthread_exit ((void *) 0);                      /* Allow potential (un)compression tasks to complete */
#endif /* COMMON_PTHREAD */
  MPI_Finalize ();
  return (0);
}

static int btyp2format (
int                         typeval,
MPI_Datatype *              typeptr,
char *                      format,
size_t *                    elemsiz)
{

  if (typeval == 1) { /* MPI_INT */
    *typeptr = MPI_INT;
    strcpy (format, "%d\t");
  }
  else if (typeval == 2) { /* MPI_DOUBLE */
    *typeptr = MPI_DOUBLE;
    strcpy (format, "%.16e\t");
  }
  else if (typeval == 3) { /* MPI_LONG_LONG */
    *typeptr = MPI_LONG_LONG;
    strcpy (format, "%lld\t");
  }
  else {
    errorPrint ("Type not implemented");
    return     (1);
  }

  return (0);
}

static
int
C_meshScat (
FILE * const                stream,
PAMPA_Num                  procnbr,
char * const                nameptr,
PAMPA_Num *                 pnbrtab)
{
  PAMPA_Num           versval;
  PAMPA_Num           cenval;
  PAMPA_Num           gnumval;
  PAMPA_Num           dummy;
  PAMPA_Num           esubflg;
  char                proptab[4];
  PAMPA_Num           flagtab[2];
  PAMPA_Num           baseval;
  PAMPA_Num           vertglbnbr;
  PAMPA_Num           edgeglbnbr;
  PAMPA_Num           procnum;
  PAMPA_Num           enttnbr;
  PAMPA_Num           enttnum;
  PAMPA_Num           enttnnd;
  PAMPA_Num           valunbr;
  PAMPA_Num           valumax;
  PAMPA_Num           ovlpval;
  PAMPA_Num * restrict esubtax;
  PAMPA_Num * restrict * restrict vnbrprctab;
  PAMPA_Num * restrict            vnbrmaxtax;
  long                valupos;

  if (intLoad (stream, &versval) != 1) {          /* Read version number */
    errorPrint ("bad input");
    printf ("line %d\n", __LINE__);
    return     (1);
  }
  //if (versval < 1) {                             /* If version not zero */
  //  errorPrint ("Version 0 not supported");
  //  return     (1);
  //}

  if (intLoad (stream, &cenval) != 1) {          /* Read cenion number */
    errorPrint ("only centralized meshes supported");
    return     (1);
  }
  if (cenval != 1) {                             /* If cenion not zero */
    errorPrint ("mesh format with all data is not supported");
    return     (1);
  }

  if ((intLoad (stream, &gnumval)    != 1) ||     /* Read rest of header */
      (intLoad (stream, &dummy)      != 1) || // XXX procnum pourquoi ?
      (intLoad (stream, &dummy)      != 1) || // XXX procnbr pourquoi ?
      (intLoad (stream, &enttnbr)    != 1) ||
      (intLoad (stream, &vertglbnbr) != 1) ||
      (intLoad (stream, &edgeglbnbr) != 1) ||
      (intLoad (stream, &valunbr)    != 1) ||
      (intLoad (stream, &valumax)    != 1) ||
      (intLoad (stream, &baseval)    != 1) ||
      (intLoad (stream, &esubflg)    != 1) ||
      (intLoad (stream, flagtab)     != 1)) {     /* Edge weights flag */
    errorPrint ("bad input");
    printf ("line %d\n", __LINE__);
    return     (1);
  }

  // XXX début temporaire
  if (esubflg != 0) {
    errorPrint ("Not yet implemented");
    return     (1);
  }
  esubtax = NULL;
  // XXX fin temporaire
  
  if ((vnbrprctab =
        (PAMPA_Num * restrict * restrict)
        memAlloc ((procnbr + 2) * sizeof (PAMPA_Num*))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }

  for (procnum  = 0; procnum < procnbr + 2; procnum ++) {
    if ((vnbrprctab[procnum] = 
          (PAMPA_Num * restrict) memAlloc ((enttnbr - PAMPA_ENTT_VIRT_PROC + baseval) * sizeof (PAMPA_Num))) == NULL) {
      errorPrint  ("out of memory");
      return      (1);
    }
    memSet (vnbrprctab[procnum], 0, (enttnbr - PAMPA_ENTT_VIRT_PROC + baseval) * sizeof (PAMPA_Num));
    vnbrprctab[procnum] -= PAMPA_ENTT_VIRT_PROC;
  }
  vnbrprctab +=2;
  vnbrmaxtax = vnbrprctab[-2];

  for (procnum = 0; procnum < procnbr; procnum ++) {
    char *              nametmp;
    FILE *              ostream;
    PAMPA_Num           vertlocnbr;
    PAMPA_Num           vertlocnum;
    PAMPA_Num           edgelocnbr;
    PAMPA_Num * restrict vnbrtax;

    vnbrtax = vnbrprctab[procnum];

    nametmp = nameptr;
    ostream = NULL;
    if (fileNameDistExpand (&nametmp, procnbr, procnum, -1) == 0) {
      ostream = fopen (nametmp, "w+");
      memFree (nametmp);                            /* Expanded name no longer needed anyway */
    }
    if (ostream == NULL) {
      errorPrint ("cannot open file");
      return     (1);
    }

    if (pnbrtab == NULL)
      vertlocnbr = DATASIZE (vertglbnbr, procnbr, procnum);
    else
      vertlocnbr = pnbrtab[procnum];
    //infoPrint ("vertlocnbr" PAMPA_NUMSTRING " for " PAMPA_NUMSTRING, vertlocnbr, procnum);
    ovlpval = 0; // XXX temporaire, peut-être mettre en argument (optionnel)
    if (fprintf(ostream,
          "%d\t%d\t%d\n" /* version, parallel, PAMPA_Num size  */
          "%d\t%d\n" /* proclocnum, procglbnbr */
          PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* enttglbnbr, vertglbnbr, edgeglbnbr */
          PAMPA_NUMSTRING "\t%015d\n"  /* vertlocnbr, edgelocnbr */
          PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* valuglbnbr, valuglbmax */
          PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n", /* baseval, ovlpval, esubloctab, flagval    */
          0, /* Version */
          2, /* Parallel XXX à changer */
          1, /* PAMPA_Numval XXX à changer */
          (int) procnum, (int) procnbr,
          enttnbr, vertglbnbr, edgeglbnbr, /* enttnbr, vertglbnbr, edgeglbnbr */
          vertlocnbr,
          0,                               /* Number of edges not yet known */
          valunbr, valumax,
          baseval, ovlpval, (PAMPA_Num) (esubtax == NULL ? 0 : 1), flagtab[0]) == EOF) {
      errorPrint ("bad output");
      return     (1);
    }

    if (esubtax != NULL) {
      for (enttnum = baseval, enttnnd = baseval + enttnbr; enttnum < enttnnd; enttnum ++)
        intSave (ostream, esubtax[enttnum]);
      putc ('\n', ostream);
    }

    for (vertlocnum = edgelocnbr = 0; vertlocnum < vertlocnbr; vertlocnum ++) {
      PAMPA_Num          degrval;

      if (intLoad (stream, &enttnum) != 1) {      /* Read vertex degree */
        errorPrint ("bad input");
	printf ("line %d\n", __LINE__);
        return     (1);
      }
      vnbrtax[enttnum] ++;

      intSave (ostream, enttnum);
      putc ('\t', ostream);

      if (intLoad (stream, &degrval) != 1) {      /* Read vertex degree */
        errorPrint ("bad input");
	printf ("line %d\n", __LINE__);
        return     (1);
      }
      intSave (ostream, degrval);

      edgelocnbr += degrval;

      for ( ; degrval > 0; degrval --) {
        PAMPA_Num          edgeval;              /* Value where to read edge end */

        if (flagtab[0] != 0) {                    /* If must read edge load        */
          PAMPA_Num          edloval;            /* Value where to read edge load */

          if (intLoad (stream, &edloval) != 1) {  /* Read edge load data    */
            errorPrint ("bad input");
	    printf ("line %d\n", __LINE__);
            return     (1);
          }
          putc ('\t', ostream);
          intSave (ostream, edloval);
        }

        if (intLoad (stream, &edgeval) != 1) {    /* Read edge data */
          errorPrint ("bad input");
	  printf ("line %d\n, vertlocnum" PAMPA_NUMSTRING ", flag: " PAMPA_NUMSTRING ", deg: " PAMPA_NUMSTRING, __LINE__, vertlocnum, flagtab[0], degrval);
          return     (1);
        }
        putc ('\t', ostream);
        intSave (ostream, edgeval);
      }
      putc ('\n', ostream);
    }

    rewind (ostream);

    if (fprintf(ostream,
          "%d\t%d\t%d\n" /* version, parallel, PAMPA_Num size  */
          "%d\t%d\n" /* proclocnum, procglbnbr */
          PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\t" PAMPA_NUMSTRING "\n"  /* enttglbnbr, vertglbnbr, edgeglbnbr */
          PAMPA_NUMSTRING "\t%015lld\n",  /* vertlocnbr, edgelocnbr */
          0, /* Version */
          2, /* Parallel XXX à changer */
          1, /* PAMPA_Numval XXX à changer */
          (int) procnum,(int)  procnbr,
          enttnbr, vertglbnbr, edgeglbnbr, /* enttnbr, vertglbnbr, edgeglbnbr */
          vertlocnbr,
          (long long) edgelocnbr) == EOF) { /* Now we know the exact number of edges */
      errorPrint ("bad output");
      return     (1);
    }

    fclose (ostream);

    for (enttnum = baseval, enttnnd = enttnbr + baseval; enttnum < enttnnd; enttnum ++) 
      vnbrmaxtax[enttnum] += vnbrtax[enttnum];
  }

  if ((valupos = ftell (stream)) < 0) {
    errorPrint ("bad position");
    return     (1);
  }

  // XXX il manque la partie pour les sous-entités
  // autrement dit remplir vnbrprctab pour les entités mères avec esubtax
  for (procnum = 0; procnum < procnbr; procnum ++) {
    char *              nametmp;
    FILE *              ostream;
    PAMPA_Num * restrict vnbrcurtax;
    PAMPA_Num * restrict vnbrprvtax;

    vnbrcurtax = vnbrprctab[procnum] - baseval;
    vnbrprvtax = vnbrprctab[procnum - 1] - baseval;

    for (enttnum = baseval, enttnnd = enttnbr + baseval; enttnum < enttnnd; enttnum ++) 
      vnbrcurtax[PAMPA_ENTT_VIRT_VERT] += vnbrcurtax[enttnum];

    nametmp = nameptr;
    ostream = NULL;
    if (fileNameDistExpand (&nametmp, procnbr, procnum, -1) == 0) {
      ostream = fopen (nametmp, "a+");
      memFree (nametmp);                            /* Expanded name no longer needed anyway */
    }
    if (ostream == NULL) {
      errorPrint ("cannot open file");
      return     (1);
    }

    if (fseek(stream, valupos, SEEK_SET) != 0) {
      errorPrint ("bad position");
      return     (1);
    }

    for (enttnum = PAMPA_ENTT_VIRT_PROC, enttnnd = enttnbr + baseval; enttnum < enttnnd; enttnum ++) {
      PAMPA_Num         enttval;
      PAMPA_Num         valunum;

      if ((intLoad (stream, &enttval) != 1) 
          || (enttval != enttnum)) {    /* Read entity number */
        errorPrint ("bad input");
	printf ("line %d\n", __LINE__);
        return     (1);
      }
      intSave (ostream, enttval);
      putc ('\t', ostream);

      if (intLoad (stream, &valunbr) != 1) {    /* Read number of values */
        errorPrint ("bad input");
	printf ("line %d\n", __LINE__);
        return     (1);
      }
      intSave (ostream, valunbr);
      putc ('\n', ostream);

      for (valunum = 0; valunum < valunbr; valunum ++) {
        MPI_Aint        valusiz;                 /* Extent of attribute datatype */
        PAMPA_Num       combdat; // XXX il faut mettre « int » dans les *_io_*.c
        PAMPA_Num       typeval;
        PAMPA_Num       countnum;
        PAMPA_Num       countnbr;
        PAMPA_Num       tagnum;
        PAMPA_Num       flagval;
        PAMPA_Num       vertnum;
        MPI_Datatype    typedat;
        MPI_Datatype    typedt2;
        char            format[9];
        byte            valuitm[VMAXSIZ];
        int             typfflg;                /* set if type must be freed */

        if (intLoad (stream, &tagnum) != 1) {
          errorPrint ("bad input");
	  printf ("line %d\n", __LINE__);
          return     (1);
        }
        intSave (ostream, tagnum);
        putc ('\t', ostream);

        flagval = 0; // XXX temporaire
        intSave (ostream, flagval);
        putc ('\t', ostream);

        if (intLoad (stream, &combdat) != 1) {
          errorPrint ("bad input");
	  printf ("line %d\n", __LINE__);
          return     (1);
        }
        intSave (ostream, combdat);
        putc ('\t', ostream);

        typfflg = 0;
        switch (combdat) {
          case 1: /* MPI_COMBINER_NAMED */
            countnbr = 1;
            if (intLoad (stream, &typeval) != 1) {
              errorPrint ("bad input");
	      printf ("line %d\n", __LINE__);
              return     (1);
            }
            intSave (ostream, typeval);
            putc ('\n', ostream);

            if (btyp2format (typeval, &typedat, format, NULL) != 0) {
              return (1);
            }
            break;
          case 2: /* MPI_COMBINER_CONTIGUOUS */
            if (intLoad (stream, &countnbr) != 1) {
              errorPrint ("bad input");
	      printf ("line %d\n", __LINE__);
              return     (1);
            }
            intSave (ostream, countnbr);
            putc ('\t', ostream);

            if (intLoad (stream, &typeval) != 1) {
              errorPrint ("bad input");
	      printf ("line %d\n", __LINE__);
              return     (1);
            }
            intSave (ostream, typeval);
            putc ('\n', ostream);

            btyp2format (typeval, &typedt2, format, NULL);

            MPI_Type_contiguous(countnbr, typedt2, &typedat);
            MPI_Type_commit(&typedat);
            typfflg = 1;

            break;
          default:
            errorPrint ("case not implemented");
            return     (1);
        }

        MPI_Type_get_extent (typedat, &dummy, &valusiz);     /* Get type extent */
        if (valusiz > VMAXSIZ) {
          errorPrint ("Value is larger than max allowed");
          return     (1);
        }

        for (vertnum = 0; vertnum < vnbrprvtax[enttnum]; vertnum ++)
          for (countnum = 0; countnum < countnbr; countnum ++) {
            if (valueScan (stream, format, valuitm) != 0) {
              errorPrint ("bad input");
	      printf ("line %d\n", __LINE__);
              return     (1);
            }
          }

        for (vertnum = 0; vertnum < vnbrcurtax[enttnum]; vertnum ++)
          for (countnum = 0; countnum < countnbr; countnum ++) {
            if ((valueScan (stream, format, valuitm) != 0)
                || (valuePrint (ostream, format, valuitm) != 0)) {
              errorPrint ("bad I/O");
              return     (1);
            }
          }

        if ((vnbrcurtax[enttnum] + vnbrprvtax[enttnum]) == 0)
          getc (stream);
        if ((vnbrcurtax[enttnum] + vnbrprvtax[enttnum]) != vnbrmaxtax[enttnum])
          while (getc (stream) != '\n');
        putc ('\n', ostream);
        
        if (typfflg == 1)
          MPI_Type_free (&typedat);
      }
    }

    for (enttnum = baseval, enttnnd = enttnbr + baseval; enttnum < enttnnd; enttnum ++) 
      vnbrcurtax[enttnum] += vnbrprvtax[enttnum];

    fclose (ostream);
  }

  vnbrprctab -= 2;
  for (procnum = 0; procnum < procnbr + 2; procnum ++) 
    memFree (vnbrprctab[procnum] + PAMPA_ENTT_VIRT_PROC);
  memFree (vnbrprctab);

    


  return (0);
}
