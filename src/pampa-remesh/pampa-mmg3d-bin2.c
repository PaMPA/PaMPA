/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pampa-mmg3d-bin2.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 23 May 2017
//!                             to:   22 Sep 2017
//!
/************************************************************/
#include <mpi.h>
#include <common.h>
#include <module.h>
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d.h>
#include <pampa-mmg3d.h>
#ifdef MSHINT
#include <pampa-mshint.h>
#endif /* MSHINT */
#include <pampa-mmg3d-bin.h>
#include <fcntl.h>

static const char *         C_usageList[] = {     /* Usage */
  "pampa-mmg3d-bin <mesh infile> <mesh outfile> [mmg3d options]",
  "",
  "PaMPA options are:",
  " mesh infile (ext .mesh) is required as:",
  "  -i, --cm-in <file>       : centralized mesh infile",
  "",
  " mesh outfile (ext .mesh) is required as:",
  "  -o, --cm-out <file>      : centralized mesh outfile",
  "",
  NULL };

#define FILL_DBL(s,d)    \
  if (! strcmp (s, "*")) \
    d = (double) atof (s);


#define CHECK_ARG(s,i,c)  { \
  if (((i) >= (c)) || ((s)[i][0] == '-')) { \
    errorPrint ("missing or invalid argument for option %s", (s[i - 1])); \
    errorPrint ("Try option --help for more information"); \
    return (1); \
  } \
}

#define UNK_ARG(s)  { \
  errorPrint ("unknown option %s", (s)); \
  errorPrint ("Try option --help for more information"); \
  return (1); \
}

  void
usagePrint (
    FILE * const                stream,
    const char ** const         data)
{
  const char **       cptr;

  fprintf (stream, "\nUsage is:\n");
  for (cptr = data; *cptr != NULL; cptr ++)
    fprintf (stream, "  %s\n", *cptr);
}

static int parsstr (char *src, char ** dst, int rank, int size) {
  char * ppos, * rpos;

  *dst = malloc ((strlen (src) + 1 + (int) log10(size)) * sizeof (char));
  ppos = strstr(src, "@p");
  if (ppos != NULL) {
    ppos[0] = '%';
    ppos[1] = 'd';
  }
  rpos = strstr(src, "@r");
  if (rpos != NULL) {
    rpos[0] = '%';
    rpos[1] = 'd';
  }
  if ((ppos != NULL) && (rpos != NULL)) {
    if (ppos < rpos) 
      sprintf(*dst, src, size, rank);
    else
      sprintf(*dst, src, rank, size);
  }
  else if (ppos != NULL) 
    sprintf(*dst, src, size);
  else if (rpos != NULL) 
    sprintf(*dst, src, rank);
  else
    sprintf(*dst, src);
  return (0);
}

int parsopt (char **argv, int argc, MPI_Comm comm, int rank, int size, GlbData *data) {
  MMG5_pMesh mesh;
  MMG5_pSol sol;
  int retval;

  if (argc < 3) {
    usagePrint (stdout, C_usageList);
    return (1);
  }

  CHECK_ARG(argv, 1, argc);
  data->cmin = argv[1];
  CHECK_ARG(argv, 2, argc);
  data->cmout = argv[2];

  mesh = NULL;
  sol = NULL;
  MMG3D_Init_mesh (MMG5_ARG_start, MMG5_ARG_ppMesh, &mesh, MMG5_ARG_ppMet, &sol, MMG5_ARG_end);
  retval = MMG3D_parsar(argc - 2, argv + 2, mesh, sol) ; // XXX tester la valeur de retour
  MMG3D_destockOptions(mesh, &data->data.infoval) ;
  MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mesh, MMG5_ARG_ppMet, &sol, MMG5_ARG_end);

  return (1 - retval);
}

int mmg3d2pampa (MMG5_pMesh * mesh, MMG5_pSol * sol, GlbData * data, PAMPA_Num flagval, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) {
  PAMPA_Mesh m;
  PAMPA_Dmesh dm;
  PAMPA_Strat strat;
  PAMPA_Num vertlocnbr, *partloctab, ovlpglbval, typenbr;
  PAMPA_Num * entttab, * tagtab;
  MPI_Datatype * typetab;

  ovlpglbval = 0;
  data->data.tetrent = 0; // entity number for main entity
  data->data.faceent = 1; // entity number for faces
  data->data.nodeent = 2; // entity number for nodes
  data->data.infoprt = 0;

  // XXX pas au bon endroit ces tableaux
  // Fill in arrays for attached values
  typenbr = 8;
  entttab = malloc (sizeof (PAMPA_Num) * typenbr);
  tagtab = malloc (sizeof (PAMPA_Num) * typenbr);
  typetab = malloc (sizeof (MPI_Datatype) * typenbr);

  entttab[0] = 
    entttab[1] =
    entttab[2] =  data->data.nodeent;
  entttab[3] =  data->data.tetrent;
  entttab[4] =  data->data.nodeent;
  entttab[5] =  PAMPA_ENTT_VIRT_VERT;
  entttab[6] =  data->data.tetrent;
  entttab[7] =  data->data.faceent;
  tagtab[0] = PAMPA_TAG_GEOM;
  tagtab[1] = PAMPA_TAG_REF;
  tagtab[2] = PAMPA_TAG_SOL_3DI;
  tagtab[3] = PAMPA_TAG_REMESH;
  tagtab[4] = PAMPA_TAG_SOL_3DAI;
  tagtab[5] = 33; /* partitioning, FIXME mettre un define */
  tagtab[6] = PAMPA_TAG_REF;
  tagtab[7] = PAMPA_TAG_REF;
  MPI_Type_contiguous(3, MPI_DOUBLE, &typetab[0]);
  MPI_Type_commit(&typetab[0]);
  typetab[1] = PAMPA_MPI_NUM;
  typetab[2] = MPI_DOUBLE;
  typetab[3] = PAMPA_MPI_NUM;
  typetab[5] = PAMPA_MPI_NUM;
  typetab[6] = PAMPA_MPI_NUM;
  typetab[7] = PAMPA_MPI_NUM;

  MPI_Type_contiguous(6, MPI_DOUBLE, &typetab[4]);
  MPI_Type_commit(&typetab[4]);

  data->data.coortyp = typetab[0];

  // Initialisation of PaMPA dmesh structure
  CHECK_FERR(PAMPA_dmeshInit(&dm, MPI_COMM_WORLD), comm);

  if (rank == 0) {
    // Initialisation of PaMPA mesh structure
    CHECK_FERR(PAMPA_meshInit(&m), comm);

    // Convert to PaMPA mesh
    CHECK_FERR (PAMPA_MMG3D_rmesh2pmesh (mesh, sol, &data->data, flagval, &m), comm);

    // Scatter PaMPA mesh
    CHECK_FERR(PAMPA_dmeshScatter2(&dm, &m, typenbr, entttab, tagtab, typetab, ovlpglbval), comm);

    // Finalisation of PaMPA mesh structure
    PAMPA_meshExit(&m);
  }
  else {
    // Scatter PaMPA mesh
    CHECK_FERR(PAMPA_dmeshScatter2(&dm, NULL, typenbr, entttab, tagtab, typetab, ovlpglbval), comm);
  }

  MPI_Type_free (&typetab[4]);
  free (entttab);
  free (tagtab);
  free (typetab);

#ifdef PAMPA_DEBUG_DMESH
  CHECK_FERR (PAMPA_dmeshCheck (&dm), comm);
#endif /* PAMPA_DEBUG_DMESH */

  // Initialisation of PaMPA strat structure
  CHECK_FERR(PAMPA_stratInit(&strat), comm);

  PAMPA_dmeshSize(&dm, NULL, &vertlocnbr, NULL, NULL);

  partloctab = malloc (sizeof (PAMPA_Num) * vertlocnbr);

  // Partitioning PaMPA Dmesh
  CHECK_FERR(PAMPA_dmeshPart(&dm, (PAMPA_Num) size, &strat, partloctab), comm);
  PAMPA_stratExit(&strat);



  // Initialisation of PaMPA dmesh structure
  CHECK_FERR(PAMPA_dmeshInit(dmesh, MPI_COMM_WORLD), comm);

  // Redistribute PaMPA Dmesh
  CHECK_FERR(PAMPA_dmeshRedist(&dm, partloctab, NULL, -1, -1, -1, dmesh), comm);
  free (partloctab);
  PAMPA_dmeshExit(&dm);

#ifdef PAMPA_DEBUG_DMESH
  CHECK_FERR (PAMPA_dmeshCheck (dmesh), comm);
#endif /* PAMPA_DEBUG_DMESH */

  return (0);
}

int pampa2mmg3d (PAMPA_Dmesh * dmesh, GlbData * data, PAMPA_Num flagval, MMG5_pMesh * mesh, MMG5_pSol * sol, MPI_Comm comm, int rank, int size) {

  if (rank == 0) {
    PAMPA_Mesh m;
    // Initialisation of PaMPA mesh structure
    CHECK_FERR(PAMPA_meshInit(&m), comm);

    errorPrint ("Not yet implemented");
    return (1);
    //CHECK_FERR(PAMPA_dmeshGather (dmesh, &m), comm);
    data->data.infoprt = 1;
    CHECK_FDBG2(PAMPA_MMG3D_pmesh2rmesh(&m, &data->data, flagval, mesh, sol));

    // Finalisation of PaMPA mesh structure
    PAMPA_meshExit(&m);
  }
  else {
    // Gather PaMPA mesh
    errorPrint ("Not yet implemented");
    return (1);
    //CHECK_FERR(PAMPA_dmeshGather (dmesh, NULL), comm);
  }

  return (0);
}

int freemem (GlbData * data) {
  MPI_Type_free(&data->data.coortyp);

  return (0);
}

int main(int argc, char** argv){
  MMG5_pMesh    mmshptr;
  MMG5_pSol    msolptr;
  PAMPA_Num basedif;
  PAMPA_Num nodenbr;
  PAMPA_Dmesh dmesh;
  PAMPA_AdaptInfo infoptr;
  PAMPA_Num vertglbnbr;
  PAMPA_Num flagval;
  GlbData data;
  int rank, retval, size, bak, new;
  MPI_Comm comm;
  int  namelen;
  char procnam[MPI_MAX_PROCESSOR_NAME] ;
  char s1[100];
  char s2[100];

  comm = MPI_COMM_WORLD;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &retval);
  // XXX tester retval
  MPI_Comm_rank(comm, &rank) ;
  MPI_Comm_size(comm, &size) ;
  MPI_Get_processor_name(procnam,&namelen);

  CHECK_FERR (parsopt(argv, argc, comm, rank, size, &data), comm);

  printf ("Processus %d, host: %s, with MPI rank %d on %d\n", getpid(), procnam, rank, size);

  if (rank == 0) {
    mmshptr = NULL;
    msolptr = NULL;
    MMG3D_Init_mesh (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);
    CHECK_FDBG2 (MMG3D_Set_inputMeshName (mmshptr, data.cmin) != 1);
    CHECK_FDBG2 (MMG3D_Set_inputSolName (mmshptr, msolptr, data.cmin) != 1);
    CHECK_FDBG2 (MMG3D_loadMesh (mmshptr, data.cmin) != 1);
    CHECK_FDBG2 (MMG3D_loadSol (mmshptr, msolptr, data.cmin) != 1);
    data.data.infoval.hmin = mmshptr->info.hmin;
    data.data.infoval.hmax = mmshptr->info.hmax;
  }

  // XXX begin necessary
  MPI_Bcast (&data.data.infoval.hmin, 1, MPI_DOUBLE, 0, comm);
  MPI_Bcast (&data.data.infoval.hmax, 1, MPI_DOUBLE, 0, comm);
  infoptr.flagval =
    flagval = PAMPA_MMG3D_FACES;

  CHECK_FERR (mmg3d2pampa (&mmshptr, &msolptr, &data, flagval, &dmesh, comm, rank, size), comm);
  if (rank == 0) {
    MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);

    PAMPA_printVersions ();
    PAMPA_dmeshEnttSize(&dmesh, data.data.tetrent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("At the beginning\n\tNumber of tetrahedra: %d\n", vertglbnbr);
    PAMPA_dmeshEnttSize(&dmesh, data.data.nodeent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("\tNumber of nodes: %d\n", vertglbnbr);
  }

  // -e 0.6 -E 3 -q 30 -B 0 [sur Curie]
  data.data.edgemin = 0.6;
  data.data.edgemax = 3;
  data.data.qualmin = 0.9;
  infoptr.rank = rank;
  infoptr.alpha = 1.0;
  infoptr.zonenb2 = ~0;
  infoptr.zonenbr = ~0;
  infoptr.estmflg = 0;
  infoptr.itdebug = NULL;
  infoptr.methval = "mbPart";
  infoptr.ibndval = 3;
  infoptr.ballsiz = 600000;
  infoptr.ebndval = 0;
  infoptr.loopval = 0;
  infoptr.ratemin = 0.1;
  infoptr.bndgflg = 0;
  infoptr.minxval = DBL_MAX;
  infoptr.maxxval = - DBL_MAX;
  infoptr.minyval = DBL_MAX;
  infoptr.maxyval = - DBL_MAX;
  infoptr.minzval = DBL_MAX;
  infoptr.maxzval = - DBL_MAX;
  infoptr.EXT_meshAdapt = &PAMPA_MMG3D_meshAdapt;
#ifdef MSHINT 
  infoptr.EXT_meshInt = &PAMPA_MSHINT_meshInt;
#endif /* MSHINT */
  infoptr.EXT_dmeshCheck = &PAMPA_MMG3D_dmeshCheck;
  infoptr.EXT_dmeshBand = &PAMPA_MMG3D_dmeshBand;
  infoptr.EXT_dmeshWeightCompute = &PAMPA_MMG3D_dmeshWeightCompute;
  infoptr.EXT_dmeshMetricCompute = &PAMPA_MMG3D_dmeshMetricCompute;
  infoptr.EXT_meshSave = &PAMPA_MMG3D_meshSave;
  infoptr.dataptr = &data.data;
  CHECK_FERR (PAMPA_dmeshAdapt (&dmesh, &infoptr, NULL), comm);
  if (rank == 0) {
    PAMPA_dmeshEnttSize(&dmesh, data.data.tetrent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("At the end\n\tNumber of tetrahedra: %d\n", vertglbnbr);
    PAMPA_dmeshEnttSize(&dmesh, data.data.nodeent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("\tNumber of nodes: %d\n", vertglbnbr);
  }
  sprintf (s1, "%s.mesh", data.cmout);

  mmshptr = NULL;
  msolptr = NULL;
  CHECK_FERR (pampa2mmg3d (&dmesh, &data, flagval, &mmshptr, &msolptr, comm, rank, size), comm);
  PAMPA_dmeshExit(&dmesh);
  CHECK_FERR (freemem (&data), comm);
  // XXX end necessary

  if (rank == 0) {
    CHECK_FDBG2 (MMG5_Set_outputMeshName(mmshptr, s1) != 1);
    sprintf (s2, "%s.sol", data.cmout);
    CHECK_FDBG2 (MMG5_Set_outputSolName(mmshptr, msolptr, s2) != 1);
    MMG5_saveMesh (mmshptr);
    MMG5_saveMet (mmshptr, msolptr);
    MMG3D_Free_all (MMG5_ARG_start, MMG5_ARG_ppMesh, &mmshptr, MMG5_ARG_ppMet, &msolptr, MMG5_ARG_end);
    printf("fin du prog\n");
  }

  MPI_Finalize() ;

  return EXIT_SUCCESS;
}
