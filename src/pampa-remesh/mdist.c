/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        mdist.c
//!
//!   \authors     Cedric Lachat
//!                Francois Pellegrini
//!
//!
//!   \date        Version 1.0: from: 23 May 2017
//!                             to:   25 Sep 2017
//!
/************************************************************/

/*
**  The defines and includes.
*/

#define mdist

#include "module.h"
#include "common.h"
#include <pampa.h>
#include <pampa.h>
#include "mdist.h"
#include "libmmg3d.h"
#include "pampa-mmg3d.h"

/*
**  The static variables.
*/

static int                  C_paraNum = 0;        /* Number of parameters       */
static int                  C_fileNum = 0;        /* Number of file in arg list */
static File                 C_fileTab[C_FILENBR] = { /* File array              */
                              { "r" },
                              { "w" } };

static const char *         C_usageList[] = {     /* Usage */
  "mdist <nparts> <input source file> <output target file pattern> <options>",
  "  -h  : Display this help",
  "  -V  : Print program version and copyright",
  NULL };



/******************************/
/*                            */
/* This is the main function. */
/*                            */
/******************************/

int
main (
int                         argc,
char *                      argv[])
{
  PAMPA_Smesh        smshdat;
  PAMPA_MMG3D_Data   datadat;
  PAMPA_Num          flagval;
  PAMPA_Num          p[1] = { 1 };               /* Number of parts */
  PAMPA_Num        * procvrttab;
  int                 i;
  int                size;

  MPI_Init (&argc, &argv);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  if (size > 1) {
    errorPrint ("Invalid number of processors");
    return     (1);
  }

  errorProg ("mdist");

  if ((argc >= 2) && (argv[1][0] == '?')) {       /* If need for help */
    //usagePrint (stdout, C_usageList);
    errorPrint ("XXX pas bien temporaire");
    return     (0);
  }

  fileBlockInit (C_fileTab, C_FILENBR);           /* Set default stream pointers */

  for (i = 1; i < argc; i ++) {                   /* Loop for all option codes                        */
    if ((argv[i][0] != '-') || (argv[i][1] == '\0') || (argv[i][1] == '.')) { /* If found a file name */
      if (C_paraNum < 1) {                        /* If number of parameters not reached              */
        if ((p[C_paraNum ++] = atoi (argv[i])) < 1) { /* Get number of parts                          */
          errorPrint ("invalid number of parts '%s'", argv[i]);
          return     (1);
        }
        continue;                                 /* Process the other parameters */
      }
      if (C_fileNum < C_FILEARGNBR)               /* A file name has been given */
        fileBlockName (C_fileTab, C_fileNum ++) = argv[i];
      else {
	      // XXX tmp
        //errorPrint ("too many file names given");
        //return     (1);
      }
    }
    else {                                        /* If found an option name */
      switch (argv[i][1]) {
        case 'H' :                                /* Give the usage message */
        case 'h' :
          //usagePrint (stdout, C_usageList);
          errorPrint ("XXX pas bien temporaire");
          return     (0);
        case 'V' :
          fprintf (stderr, "mdist, version " PAMPA_VERSION_STRING "\n");
          fprintf (stderr, "Copyright XXX\n");
          fprintf (stderr, "This software is XXX\n");
          return  (0);
        default :
          errorPrint ("unprocessed option '%s'", argv[i]);
          return     (1);
      }
    }
  }


  //fileBlockOpen (C_fileTab, 1);                   /* Open input mesh file */

  CHECK_FDBG2 (PAMPA_smeshInit (&smshdat));

  if ((procvrttab = (PAMPA_Num *) memAlloc ((p[0] + 1) * sizeof (PAMPA_Num))) == NULL) {
    errorPrint  ("out of memory");
    return      (1);
  }

  flagval = PAMPA_MMG3D_FACES;
  datadat.tetrent = 0;
  datadat.faceent = 1;
  datadat.nodeent = 2;
  MPI_Type_contiguous(3, MPI_DOUBLE, &datadat.coortyp);
  MPI_Type_commit(&datadat.coortyp);
  CHECK_FDBG2 (PAMPA_MMG3D_smeshLoad (&smshdat, C_filenamesrcinp, &datadat, flagval, p[0], procvrttab));
  CHECK_FDBG2 (PAMPA_smeshSaveByName (&smshdat, C_filenamesrcout, p[0], procvrttab));
  PAMPA_smeshExit (&smshdat);
  memFree (procvrttab);
  // XXX fixer les autres fuites mémoires qui ne sont pas gênantes, mais pas
  // propres :-(

  //fileBlockClose (C_fileTab, 1);                  /* Always close explicitely to end eventual (un)compression tasks */

#ifdef COMMON_PTHREAD
  pthread_exit ((void *) 0);                      /* Allow potential (un)compression tasks to complete */
#endif /* COMMON_PTHREAD */
  MPI_Finalize ();
  return (0);
}

