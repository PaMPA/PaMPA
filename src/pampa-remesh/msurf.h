/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        msurf.h
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 26 Sep 2017
//!                             to:   26 Sep 2017
//!
/************************************************************/

/*
**  The defines.
*/

/*+ File name aliases. +*/

#define C_FILENBR                   3             /* Number of files in list                */
#define C_FILEARGNBR                3             /* Number of files which can be arguments */

#define C_filenamesrcinp            fileBlockName (C_fileTab, 0) /* Centralized source mesh input file name  */
#define C_filenamemshout            fileBlockName (C_fileTab, 1) /* Centralized source mesh output file name */
#define C_filenamemetout            fileBlockName (C_fileTab, 2) /* Centralized source mesh output file name */

#define C_filepntrsrcinp            fileBlockFile (C_fileTab, 0) /* Centralized source mesh input file  */
#define C_filepntrmshout            fileBlockFile (C_fileTab, 1) /* Centralized source mesh output file */
#define C_filepntrmetout            fileBlockFile (C_fileTab, 2) /* Centralized source mesh output file */

#define VMAXSIZ                     100
/*
**  The function prototypes.
*/

