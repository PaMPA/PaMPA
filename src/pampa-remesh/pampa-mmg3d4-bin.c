/*  Copyright 2017 Inria
** This file is part of the PaMPA software package for parallel
** mesh partitioning and adaptation.
**
** PaMPA is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** PaMPA is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the GPLv3 license and that you accept its terms.
*/
/************************************************************/
//!
//!   \file        pampa-mmg3d4-bin.c
//!
//!   \authors     Cedric Lachat
//!
//!
//!   \date        Version 1.0: from: 23 May 2017
//!                             to:   22 Sep 2017
//!
/************************************************************/
#include <mpi.h>
#include <common.h>
#define PAMPA_TIME_CHECK
#include "comm.h"
#include <module.h>
#include <pampa.h>
#include <pampa.h>
#include <libmmg3d4.h>
#include <pampa-mmg3d4.h>
#ifdef MSHINT
#include <pampa-mshint.h>
#endif /* MSHINT */
#include <pampa-mmg3d4-bin.h>
#include <fcntl.h>
#include <ptscotch.h>

static const char *         C_usageList[] = {     /* Usage */
  "pampa-mmg3d4-bin [PaMPA options] + [MMG3D4 options]",
  "",
  "PaMPA options are:",
  " mesh infile (ext .mesh) is required as:",
  "  -i, --cm-in <file>       : centralized mesh infile",
  "  -l, --dp-in <file>       : distributed PaMPA infile",
  "",
  " mesh outfile (ext .mesh) is required as:",
  "  -o, --cm-out <file>      : centralized mesh outfile",
  "  -O, --dm-out <file>      : distributed mesh outfile [with @p replaced by",
  "                             the number of processors and @r by the rank]",
  "  -s, --dp-out <file>      : distributed PaMPA outfile",
  "",
  " mesh options are:",
  "  -a, --alpha <valuee>     : metric is multiplied by alpha value",
  "  -B, --e-band <value>     : band for external loop or at the beginning if",
  "                             no external loop (default is 3)",
  "  -c, --compute            : compute the number of zones for the first",
  "                             iteration",
  "  -C, --bounding-box       : specify the bounding box if the zone parallel",
  "                             identifying is mbGeompart. The format is",
  "                             x:<min>,<max>,y:<min>,<max>,z:<min>,<max>",
  "  -d, --debug <file>       : save as PaMPA file the distributed mesh at each",
  "                             iteration [with @p replaced by",
  "                             the number of processors and @r by the rank,",
  "                             @i and @e are required, with @e before @i]",
  "  -e, --emin <value>       : edge length minimum (default is 0.6)",
  "  -E, --emax <value>       : edge length maximum (default is 1.5)",
  "  -b, --i-band <value>     : band for internal loop (default is 3)",
  "  -m, --ident-meth <value> : method for zone parallel identifying (default",
  "                             is mbGeompart, other choices are mbPart,",
  "                             mbMeshGrow, gbCoarsen, gbCoarsenGrow,",
  "                             gbGraphGrow)",
  "  -p, --part-meth <value>  : method for mesh partitioning (default is",
  "                             partElem, other choice is partVert)",
  "  -q, --qmax <value>       : element quality maximum (default is 20)",
  "  -r, --rmin <value>       : minimum ratio of elements which need to be",
  "                             remeshed to start another iteration on external",
  "                             loop only (default is 0.1)",
  "  -w, --zweight <value>    : weight for zone (default is 600 000)",
  "  -x, --ext-loop           : with external loop in order to check",
  "                             user-defined criteria (by default this option is",
  "                             not included)",
  "  -z, --zones <value>      : number of zones (by default the number is",
  "                             computed)",
  "",
  " other options are:",
  "  -h, --help               : print this menu",
  "  -v, --print-opt          : print option values",
  "  -f, --log <file>         : each processor will print output in the file",
  "                             [with @p replaced by the number of processors",
  "                             and @r by the rank]",
  "  -t, --print-scm          : print version of each used program",
  "",
  NULL };

#define FILL_DBL(s,d)    \
  if (! strcmp (s, "*")) \
    d = (double) atof (s);


#define CHECK_ARG(s,i,c)  { \
  if (((i) >= (c)) || ((s)[i][0] == '-')) { \
    errorPrint ("missing or invalid argument for option %s", (s[i - 1])); \
    errorPrint ("Try option --help for more information"); \
    return (1); \
  } \
}

#define UNK_ARG(s)  { \
  errorPrint ("unknown option %s", (s)); \
  errorPrint ("Try option --help for more information"); \
  return (1); \
}

  void
usagePrint (
    FILE * const                stream,
    const char ** const         data)
{
  const char **       cptr;

  fprintf (stream, "\nUsage is:\n");
  for (cptr = data; *cptr != NULL; cptr ++)
    fprintf (stream, "  %s\n", *cptr);
}

static int parsstr (char *src, char ** dst, int rank, int size) {
  char * ppos, * rpos;

  *dst = malloc ((strlen (src) + 1 + (int) log10(size)) * sizeof (char));
  ppos = strstr(src, "@p");
  if (ppos != NULL) {
    ppos[0] = '%';
    ppos[1] = 'd';
  }
  rpos = strstr(src, "@r");
  if (rpos != NULL) {
    rpos[0] = '%';
    rpos[1] = 'd';
  }
  if ((ppos != NULL) && (rpos != NULL)) {
    if (ppos < rpos) 
      sprintf(*dst, src, size, rank);
    else
      sprintf(*dst, src, rank, size);
  }
  else if (ppos != NULL) 
    sprintf(*dst, src, size);
  else if (rpos != NULL) 
    sprintf(*dst, src, rank);
  else
    sprintf(*dst, src);
  return (0);
}

int parsopt (char **argv, int argc, int rank, int size, GlbData *data) {
  MMG_Mesh mesh;
  MMG_Sol sol;
  int retval;
  int i;
  int print;
  int argp;
  int optbin[1];

  print = 0;
  data->log = 
    data->cmin =
    data->cmout =
    data->dmout =
    data->dpin =
    data->dpout =
    data->debug = NULL;
  data->alpha = 1.0;
  data->iband = 3;
  data->zweight = 600000;
  data->eband = 3;
  data->extloop = 0;
  data->data.edgemin = 0.6;
  data->data.edgemax = 1.5;
  data->data.qualmax = 20;
  data->rmin = 0.1;
  data->comp = 0;
  data->scm = 0;
  data->part = "partElem";
  data->ident = "mbGeompart";
  data->bndfg = 0;
  data->minx = DBL_MAX;
  data->miny = DBL_MAX;
  data->minz = DBL_MAX;
  data->maxx = - DBL_MAX;
  data->maxy = - DBL_MAX;
  data->maxz = - DBL_MAX;
  data->zones = ~0;

  data->data.opt[0]=1; // 4; //splitting
  data->data.opt[1]=0; //debug
  data->data.opt[2]=64; //par default 64
  data->data.opt[3]=0;//noswap
  data->data.opt[4]=0;//noinsert
  data->data.opt[5]=0;//nomove
  data->data.opt[6]=5; //imprim
  data->data.opt[7]=0; //3;  //renum
  data->data.opt[8]=500; //500; //renum
  //data->data.opt[0]=0; // 4; //splitting
  //data->data.opt[1]=0; //debug
  //data->data.opt[2]=64; //par default 64
  //data->data.opt[3]=1;//noswap
  //data->data.opt[4]=1;//noinsert
  //data->data.opt[5]=1;//nomove
  //data->data.opt[6]=5; //imprim
  //data->data.opt[7]=3;  //renum
  //data->data.opt[8]=1; //renum
  data->data.opt[9]=0; // 0 pour etre dans le cas normal, 1 pour optim les 
  for (argp = -1, i=1; (i < argc) && (argp == -1); i ++) 
    if (!strcmp(argv[i], "+"))
      argp = i;
    else if ((!strcmp(argv[i], "--help")) || (!strcmp(argv[i], "-h"))) {
      usagePrint (stdout, C_usageList);
      usage ("MMG3D4");
      exit (0);
    }

  if (argp == -1) {
    errorPrint ("'+' is missing");
    exit (-1);
  }

  for (i=1; i < argp; i ++) {
    char * ipos, * epos;
    char minxstr[20], maxxstr[20], minystr[20], maxystr[20], minzstr[20], maxzstr[20];

    if ((strlen(argv[i]) > 2) && (argv[i][0] == '-') && (argv[i][1] == '-')) 
      switch (argv[i][2]) {
        case 'a' :
          if (!strcmp(argv[i], "--alpha")) {
            CHECK_ARG(argv, i + 1, argc);
            data->alpha = (double) atof(argv[++ i]);
          }
          break;
        case 'b' :
          if (!strcmp(argv[i], "--bounding-box")) {
            CHECK_ARG(argv, i + 1, argc);
            data->bndfg = 0;
            sscanf (argv[++ i], "\"x:%s,%s;y:%s,%s;z:%s,%s\"", minxstr, maxxstr, minystr, maxystr, minzstr, maxzstr);
            FILL_DBL (minxstr, data->minx);
            FILL_DBL (maxxstr, data->maxx);
            FILL_DBL (minystr, data->miny);
            FILL_DBL (maxystr, data->maxy);
            FILL_DBL (minzstr, data->minz);
            FILL_DBL (maxzstr, data->maxz);
          }
          break;
        case 'c' :
          if (!strcmp(argv[i], "--cm-in")) {
            CHECK_ARG(argv, i + 1, argc);
            data->cmin = argv[++ i];
          }
          else if (!strcmp(argv[i], "--cm-out")) {
            CHECK_ARG(argv, i + 1, argc);
            data->cmout = argv[++ i];
          }
          else if (!strcmp(argv[i], "--compute")) {
            data->comp = 1;
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'd' :
          if (!strcmp(argv[i], "--dp-in")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->dpin, rank, size);
          }
          else if (!strcmp(argv[i], "--dp-out")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->dpout, rank, size);
          }
          else if (!strcmp(argv[i], "--debug")) {

            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->debug, rank, size);
            ipos = strstr(data->debug, "@i");
            epos = strstr(data->debug, "@e");
            if (ipos == NULL) {
              errorPrint ("@i is missing for iteration number (inner loop)");
              return (1);
            }
            if (epos == NULL) {
              errorPrint ("@e is missing for iteration number (outer loop)");
              return (1);
            }
            if (ipos < epos) {
              errorPrint ("@e must be before @i");
              return (1);
            }
            ipos[0] = '%';
            ipos[1] = 'd';
            epos[0] = '%';
            epos[1] = 'd';
          }
          else if (!strcmp(argv[i], "--dm-out")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->dmout, rank, size);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'e' :
          if (!strcmp(argv[i], "--ext-loop")) {
            data->extloop = 1;
          }
          else if (!strcmp(argv[i], "--e-band")) {
            CHECK_ARG(argv, i + 1, argc);
            data->eband = (PAMPA_Num) atoi(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--emin")) {
            CHECK_ARG(argv, i + 1, argc);
            data->data.edgemin = atof(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--emax")) {
            CHECK_ARG(argv, i + 1, argc);
            data->data.edgemax = atof(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'i' :
          if (!strcmp(argv[i], "--i-band")) {
            CHECK_ARG(argv, i + 1, argc);
            data->iband = (PAMPA_Num) atoi(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--ident-meth")) {
            CHECK_ARG(argv, i + 1, argc);
            data->ident = argv[++ i];
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'l' :
          if (!strcmp(argv[i], "--log")) {
            CHECK_ARG(argv, i + 1, argc);
            parsstr(argv[++ i], &data->log, rank, size);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'p' :
          if (!strcmp(argv[i], "--print-opt")) {
            print = 1;
          }
          else if (!strcmp(argv[i], "--print-scm")) {
            data->scm = 1;
          }
          else if (!strcmp(argv[i], "--part-meth")) {
            CHECK_ARG(argv, i + 1, argc);
            data->part = argv[++ i];
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'q' :
          if (!strcmp(argv[i], "--qmax")) {
            CHECK_ARG(argv, i + 1, argc);
            data->data.qualmax = atof(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'r' :
          if (!strcmp(argv[i], "--rmin")) {
            CHECK_ARG(argv, i + 1, argc);
            data->rmin = atof(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        case 'z' :
          if (!strcmp(argv[i], "--zweight")) {
            CHECK_ARG(argv, i + 1, argc);
            data->zweight = (PAMPA_Num) atoi(argv[++ i]);
          }
          else if (!strcmp(argv[i], "--zones")) {
            CHECK_ARG(argv, i + 1, argc);
            data->zones = (PAMPA_Num) atoi(argv[++ i]);
          }
          else {
            UNK_ARG(argv[i]);
          }
          break;
        default:
          UNK_ARG(argv[i]);
          break;

      }
    else if ((strlen(argv[i]) == 2) && (argv[i][0] == '-')) 
      switch (argv[i][1]) {
        case 'a' :
          CHECK_ARG(argv, i + 1, argc);
          data->alpha = (double) atof(argv[++ i]);
          break;
        case 'b' :
          CHECK_ARG(argv, i + 1, argc);
          data->iband = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'c' :
          data->comp = 1;
          break;
        case 'C' :
          CHECK_ARG(argv, i + 1, argc);
          data->bndfg = 1;
          sscanf (argv[++ i], "x:%lf,%lf,y:%lf,%lf,z:%lf,%lf", &data->minx, &data->maxx, &data->miny, &data->maxy, &data->minz, &data->maxz);
          //FILL_DBL (minxstr, data->minx);
          //FILL_DBL (maxxstr, data->maxx);
          //FILL_DBL (minystr, data->miny);
          //FILL_DBL (maxystr, data->maxy);
          //FILL_DBL (minzstr, data->minz);
          //FILL_DBL (maxzstr, data->maxz);
          break;
        case 'd' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->debug, rank, size);
          ipos = strstr(data->debug, "@i");
          epos = strstr(data->debug, "@e");
          if (ipos == NULL) {
            errorPrint ("@i is missing for iteration number (inner loop)");
            return (1);
          }
          if (epos == NULL) {
            errorPrint ("@e is missing for iteration number (outer loop)");
            return (1);
          }
          if (ipos < epos) {
            errorPrint ("@e must be before @i");
            return (1);
          }
          ipos[0] = '%';
          ipos[1] = 'd';
          epos[0] = '%';
          epos[1] = 'd';
          break;
        case 'e' :
          CHECK_ARG(argv, i + 1, argc);
          data->data.edgemin = atof(argv[++ i]);
          break;
        case 'f' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->log, rank, size);
          break;
        case 'i' :
          CHECK_ARG(argv, i + 1, argc);
          data->cmin = argv[++ i];
          break;
        case 'l' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->dpin, rank, size);
          break;
        case 'm' :
          CHECK_ARG(argv, i + 1, argc);
          data->ident = argv[++ i];
          break;
        case 'o' :
          CHECK_ARG(argv, i + 1, argc);
          data->cmout = argv[++ i];
          break;
        case 'p' :
          CHECK_ARG(argv, i + 1, argc);
          data->part = argv[++ i];
          break;
        case 'q' :
          CHECK_ARG(argv, i + 1, argc);
          data->data.qualmax = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'r' :
          CHECK_ARG(argv, i + 1, argc);
          data->rmin = atof(argv[++ i]);
          break;
        case 's' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->dpout, rank, size);
          break;
        case 't' :
          data->scm = 1;
          break;
        case 'v' :
          print = 1;
          break;
        case 'w' :
          CHECK_ARG(argv, i + 1, argc);
          data->zweight = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'x' :
          data->extloop = 1;
          break;
        case 'z' :
          CHECK_ARG(argv, i + 1, argc);
          data->zones = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'B' :
          CHECK_ARG(argv, i + 1, argc);
          data->eband = (PAMPA_Num) atoi(argv[++ i]);
          break;
        case 'E' :
          CHECK_ARG(argv, i + 1, argc);
          data->data.edgemax = atof(argv[++ i]);
          break;
        case 'O' :
          CHECK_ARG(argv, i + 1, argc);
          parsstr(argv[++ i], &data->dmout, rank, size);
          break;
        default:
          UNK_ARG(argv[i]);
          break;
      }
    else
      UNK_ARG(argv[i]);
  }

  mesh.name =
    sol.name =
    mesh.outf = argv[0];
  retval = parsar(argc - argp, argv + argp, &mesh, &sol, data->data.opt, optbin) ;

  if (((data->dpin == NULL) && (data->cmin == NULL)) || ((data->comp == 0) && (data->dpout == NULL) && (data->cmout == NULL) && (data->dmout == NULL))) {
    errorPrint ("Mesh infile or mesh outfile is missing");
    usagePrint (stdout, C_usageList);
    return (1);
  }
  else if ((data->cmin != NULL) && ((data->dpin != NULL))) {
    errorPrint ("Too many mesh infiles");
    usagePrint (stdout, C_usageList);
    return (1);
  }
  else if (print != 0)
    printopt(data);
  
  return (1 - retval);
}

void printopt (GlbData *data) {
  infoPrint("options:\n\tcm-in: %s\n\tcm-out: %s\n\tdm-out: %s\n\n\talpha: %lf\n\te-band: %d\n\temin: %f\n\temax: %f\n\text-loop: %d\n\ti-band: %d\n\tident-meth: %s\n\tpart-meth: %s\n\tqmax: %lf\n\trmin: %f\n\tzweight: %d\n\n\tlog: %s\n\tprint-scm: %d\n\tbounding-box: %d\n\tminx: %lf\n\tmaxx: %lf\n\tminy: %lf\n\tmaxy: %lf\n\tminz: %lf\n\tmaxz: %lf\n", 
      data->cmin,
      data->cmout,
      data->dmout,
      data->alpha,
      data->eband,
      data->data.edgemin,
      data->data.edgemax,
      data->extloop,
      data->iband,
      data->ident,
      data->part,
      data->data.qualmax,
      data->rmin,
      data->zweight,
      data->log,
      data->scm,
      data->bndfg,
      data->minx,
      data->maxx,
      data->miny,
      data->maxy,
      data->minz,
      data->maxz);
}

int loadmesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) {
  PAMPA_Mesh m;
  PAMPA_Dmesh dm;
  PAMPA_Strat strat;
  PAMPA_Num vertlocnbr, *partloctab, ovlpglbval, typenbr;
  PAMPA_Num * entttab, * tagtab;
  MPI_Datatype * typetab;

  ovlpglbval = 0;
  data->data.tetrent = 0; // entity number for main entity
  data->data.faceent = ~0; // entity number for faces
  //data->data.faceent = 1; // entity number for faces
  if (data->data.faceent == 1)
    data->data.nodeent = 2; // entity number for nodes
  else
    data->data.nodeent = 1; // entity number for nodes

  data->data.infoprt = 0;

  // XXX pas au bon endroit ces tableaux
  // Fill in arrays for attached values
  typenbr = 8;
  entttab = malloc (sizeof (PAMPA_Num) * typenbr);
  tagtab = malloc (sizeof (PAMPA_Num) * typenbr);
  typetab = malloc (sizeof (MPI_Datatype) * typenbr);

  entttab[0] = 
    entttab[1] =
    entttab[2] =  data->data.nodeent;
  entttab[3] =  data->data.tetrent;
  entttab[4] =  data->data.nodeent;
  entttab[5] =  PAMPA_ENTT_VIRT_VERT;
  entttab[6] =  data->data.tetrent;
  entttab[7] =  data->data.faceent;
  tagtab[0] = PAMPA_TAG_GEOM;
  tagtab[1] = PAMPA_TAG_REF;
  tagtab[2] = PAMPA_TAG_SOL_3DI;
  tagtab[3] = PAMPA_TAG_REMESH;
  tagtab[4] = PAMPA_TAG_SOL_3DAI;
  tagtab[5] = 33; /* partitioning, FIXME mettre un define */
  tagtab[6] = PAMPA_TAG_REF;
  tagtab[7] = PAMPA_TAG_REF;
  MPI_Type_contiguous(3, MPI_DOUBLE, &typetab[0]);
  MPI_Type_commit(&typetab[0]);
  typetab[1] = PAMPA_MPI_NUM;
  typetab[2] = MPI_DOUBLE;
  typetab[3] = PAMPA_MPI_NUM;
  MPI_Type_contiguous(6, MPI_DOUBLE, &typetab[4]);
  MPI_Type_commit(&typetab[4]);
  typetab[5] = PAMPA_MPI_NUM;
  typetab[6] = PAMPA_MPI_NUM;
  typetab[7] = PAMPA_MPI_NUM;

  data->data.coortyp = typetab[0];

  // Initialisation of PaMPA dmesh structure
  CHECK_FERR(PAMPA_dmeshInit(&dm, MPI_COMM_WORLD), comm);

  if (data->cmin != NULL) {
    if (rank == 0) {
      // Initialisation of PaMPA mesh structure
      CHECK_FERR(PAMPA_meshInit(&m), comm);

      // Build PaMPA mesh
      CHECK_FDBG2(PAMPA_MMG3D4_meshLoad(&m, data->cmin, &data->data, (data->data.faceent == ~0) ? (0) : (PAMPA_MMG3D4_FACES)));

      CHECK_FDBG2(PAMPA_meshCheck(&m));


      // Scatter PaMPA mesh
      CHECK_FERR(PAMPA_dmeshScatter2(&dm, &m, typenbr, entttab, tagtab, typetab, ovlpglbval), comm);

      // Finalisation of PaMPA mesh structure
      PAMPA_meshExit(&m);
    }
    else {
      // Scatter PaMPA mesh
      CHECK_FERR(PAMPA_dmeshScatter2(&dm, NULL, typenbr, entttab, tagtab, typetab, ovlpglbval), comm);
    }
  }
  else if (data->dpin != NULL) {
    PAMPA_Num baseval;
    File f;
    f.name = data->dpin;
    f.mode = "r";
    f.pntr = stdin;
    CHECK_FERR (fileBlockOpen (&f, 1), comm);
    CHECK_FERR(PAMPA_dmeshInit(&dm, MPI_COMM_WORLD), comm);
    CHECK_FERR (PAMPA_dmeshLoad (&dm, f.pntr, -1, 0), comm);
    PAMPA_dmeshData ( &dm, &baseval, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    CHECK_FERR (fileBlockClose (&f, 1), comm);
    // XXX tester avant si les données sont associées
    CHECK_FERR (PAMPA_dmeshValueUnlink(&dm, baseval, PAMPA_TAG_REMESH), comm);
    CHECK_FERR (PAMPA_dmeshValueUnlink(&dm, baseval, PAMPA_TAG_WEIGHT), comm);

  }
  else {
    errorPrint ("Invalid read");
    return (1);
  }

  // XXX on ne peut pas libérer les types suivants car ils sont utilisés dans
  // dmesh_scatter, à corriger…
  //MPI_Type_free (&typetab[4]);
  //free (entttab);
  //free (tagtab);
  //free (typetab);

#ifdef PAMPA_DEBUG_DMESH
  CHECK_FERR (PAMPA_dmeshCheck (&dm), comm);
#endif /* PAMPA_DEBUG_DMESH */

  // Initialisation of PaMPA strat structure
  CHECK_FERR(PAMPA_stratInit(&strat), comm);

  PAMPA_dmeshSize(&dm, NULL, &vertlocnbr, NULL, NULL);

  partloctab = malloc (sizeof (PAMPA_Num) * vertlocnbr);

  // Partitioning PaMPA Dmesh
#ifdef NETLOC
  {
    if (rank == 0)
    infoPrint ("with netloc");
    FILE *arch_file;
    SCOTCH_Arch archdat;
    CHECK_FERR(SCOTCH_archInit (&archdat), comm);
    arch_file = fopen ("arch", "r");
    CHECK_FERR(SCOTCH_archLoad (&archdat, arch_file), comm);
    CHECK_FERR(PAMPA_dmeshMap(&dm, &archdat, &strat, partloctab), comm);
  }
#else /* NETLOC */
  CHECK_FERR(PAMPA_dmeshPart(&dm, (PAMPA_Num) size, &strat, partloctab), comm);
#endif /* NETLOC */
  PAMPA_stratExit(&strat);



  // Initialisation of PaMPA dmesh structure
  CHECK_FERR(PAMPA_dmeshInit(dmesh, MPI_COMM_WORLD), comm);

  // Redistribute PaMPA Dmesh
  CHECK_FERR(PAMPA_dmeshRedist(&dm, partloctab, NULL, -1, -1, -1, dmesh), comm);
  free (partloctab);
  PAMPA_dmeshExit(&dm);

#ifdef PAMPA_DEBUG_DMESH
  CHECK_FERR (PAMPA_dmeshCheck (dmesh), comm);
#endif /* PAMPA_DEBUG_DMESH */

  return (0);
}

//int qualmesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) {
//  PAMPA_Mesh * cmshptr;
//  MMG_Qual qual;
//  MMG_Mesh mesh;
//  MMG_Sol  sol;
//  PAMPA_Num tetrlocnbr;
//  PAMPA_Num tetrgstnbr;
//  PAMPA_Num vertlocnum;
//  PAMPA_Num vertlocnnd;
//  PAMPA_Num baseval;
//  PAMPA_Num meshlocnbr;
//  int procngbnum;
//  int iredloctab[54];
//  int * iredglbtab;
//  double dredloctab[7];
//  double * dredglbtab;
//  PAMPA_Num * vnumgsttax;
//  PAMPA_Num * parttax;
//  int cheklocval;
//
//  cheklocval = 0;
//  baseval = 0; // XXX à modifier
//
//  PAMPA_dmeshEnttSize (dmesh, data->data.tetrent, &tetrlocnbr, PAMPA_VERT_LOCAL, &tetrgstnbr, PAMPA_VERT_ANY, NULL);
//
//  if (memAllocGroup ((void **) (void *)
//                          &vnumgsttax, (size_t) (tetrgstnbr * sizeof (PAMPA_Num)),
//                          &parttax,    (size_t) (size       * sizeof (PAMPA_Num)),
//                          &cmshptr,    (size_t) (             sizeof (PAMPA_Mesh)),
//                          NULL) == NULL) {
//          errorPrint ("Out of memory");
//          cheklocval = 1;
//  }
//  CHECK_VERR (cheklocval, comm);
//  vnumgsttax -= baseval;
//  parttax -= baseval;
//
//  PAMPA_meshInit (cmshptr);
//  for (vertlocnum = baseval, vertlocnnd = tetrlocnbr + baseval; vertlocnum < vertlocnnd; vertlocnum ++)
//    vnumgsttax[vertlocnum] = rank;
//  for (procngbnum = 0; procngbnum < size; procngbnum ++) 
//    parttax[procngbnum + baseval] = procngbnum;
//
//  meshlocnbr = 1;
//  CHECK_FERR (PAMPA_dmeshGatherInduceMultiple (dmesh, size, vnumgsttax + baseval, parttax + baseval, &meshlocnbr, &cmshptr), comm);
//
//  if (rank == 0) {
//    if (memAllocGroup ((void **) (void *)
//          &iredglbtab, (size_t) (MAX(size * 12, 54) * sizeof (int)),
//          &dredglbtab, (size_t) (size * 7 * sizeof (double)),
//          NULL) == NULL) {
//      errorPrint ("out of memory");
//      cheklocval = 1;
//    }
//    CHECK_VERR (cheklocval, comm);
//  }
//  else {
//    iredglbtab = NULL;
//    dredglbtab = NULL;
//  }
//
//  CHECK_FERR (PAMPA_MMG3D4_pmesh2mmesh (cmshptr, &data->data, &mesh, &sol), comm);
//  PAMPA_meshExit (cmshptr);
//  if ( !MMG_hashTetra(&mesh) )    return(1);
//  qual.info = mesh.info; // XXX récupérer plutôt celui du parsopt
//  MMG_calcqua (&mesh, &sol, &qual);
//  MMG_calclen (&mesh, &sol, &qual);
//
//  MMG_meshexit (&mesh);
//  MMG_solexit (&sol);
//
//  iredloctab[0] = qual.iamin;
//  iredloctab[1] = qual.ibmin;
//  iredloctab[2] = qual.iamax;
//  iredloctab[3] = qual.ibmax;
//  iredloctab[4] = qual.iel;
//  iredloctab[5] = qual.ielreal;
//  iredloctab[6] = qual.v[0];
//  iredloctab[7] = qual.v[1];
//  iredloctab[8] = qual.v[2];
//  iredloctab[9] = qual.v[3];
//  iredloctab[10] = qual.rapnum;
//  iredloctab[11] = qual.navg;
//
//  CHECK_FMPI (cheklocval, MPI_Gather (iredloctab, 12, MPI_INT, iredglbtab, 12, MPI_INT, 0, comm), comm);
//
//  dredloctab[0] = qual.lavg;
//  dredloctab[1] = qual.lmin;
//  dredloctab[2] = qual.lmax;
//  dredloctab[3] = qual.som;
//  dredloctab[4] = qual.rapmin;
//  dredloctab[5] = qual.rapmax;
//  dredloctab[6] = qual.rapavg;
//
//  CHECK_FMPI (cheklocval, MPI_Gather (dredloctab, 7, MPI_DOUBLE, dredglbtab, 7, MPI_DOUBLE, 0, comm), comm);
//
//  if (rank == 0) {
//    int proclocnum;
//
//    qual.ielreal = 0;
//    for (proclocnum = 1; proclocnum < size; proclocnum ++) {
//      qual.rapnum += iredglbtab[12 * proclocnum + 10];
//      qual.lavg += dredglbtab[7 * proclocnum + 0];
//      qual.navg += iredglbtab[12 * proclocnum + 11];
//      qual.som += dredglbtab[7 * proclocnum + 3];
//      qual.rapavg += dredglbtab[7 * proclocnum + 6];
//
//      if (dredglbtab[7 * proclocnum + 1] < qual.lmin) {
//        qual.lmin = dredglbtab[7 * proclocnum + 1];
//        qual.iamin = iredglbtab[12 * proclocnum + 0];
//        qual.ibmin = iredglbtab[12 * proclocnum + 1];
//      }
//      if (dredglbtab[7 * proclocnum + 2] > qual.lmax) {
//        qual.lmax = dredglbtab[7 * proclocnum + 2];
//        qual.iamax = iredglbtab[12 * proclocnum + 2];
//        qual.ibmax = iredglbtab[12 * proclocnum + 3];
//      }
//      if (dredglbtab[7 * proclocnum + 5] > qual.rapmax) {
//        qual.rapmax = dredglbtab[7 * proclocnum + 5];
//        qual.iel = iredglbtab[12 * proclocnum + 4];
//        qual.ielreal = proclocnum;
//        qual.v[0] = iredglbtab[12 * proclocnum + 6];
//        qual.v[1] = iredglbtab[12 * proclocnum + 7];
//        qual.v[2] = iredglbtab[12 * proclocnum + 8];
//        qual.v[3] = iredglbtab[12 * proclocnum + 9];
//      }
//      if (dredglbtab[7 * proclocnum + 4] < qual.rapmin) {
//        qual.rapmin = dredglbtab[7 * proclocnum + 4];
//      }
//    }
//  }
//
//  memcpy ( iredloctab,qual.hl, 10 * sizeof (int));
//  memcpy ( iredloctab + 10,qual.his10, 11 * sizeof (int));
//  memcpy ( iredloctab + 21,qual.his01, 33 * sizeof (int));
//
//  CHECK_FMPI (cheklocval, MPI_Reduce (iredloctab, iredglbtab, 54, MPI_INT, MPI_SUM, 0, comm), comm);
//
//  if (rank == 0) {
//    memcpy (qual.hl, iredglbtab, 10 * sizeof (int));
//    memcpy (qual.his10, iredglbtab + 10, 11 * sizeof (int));
//    memcpy (qual.his01, iredglbtab + 21, 33 * sizeof (int));
//
//    MMG_priqua (&qual);
//    MMG_prilen2 (&qual);
//
//    memFreeGroup (iredglbtab);
//  }
//
//  memFreeGroup (vnumgsttax + baseval);
//  return (0);
//}

int savemesh (GlbData * data, PAMPA_Dmesh * dmesh, MPI_Comm comm, int rank, int size) {

  // XXX temporaire
#ifdef PAMPA_NOT_REF_ONE
  CHECK_FERR(PAMPA_dmeshSave2 (dmesh, "part_remesh.mesh", PAMPA_MMG3D4_meshSave, (void * const) &data->data), comm);
#endif /* PAMPA_NOT_REF_ONE */

  if (data->dmout != NULL)
    CHECK_FERR(PAMPA_dmeshSave2 (dmesh, data->dmout, PAMPA_MMG3D4_meshSave, (void * const) &data->data), comm);

  if (data->cmout != NULL) {
    if (rank == 0) {
      PAMPA_Mesh mesh;
      // Initialisation of PaMPA mesh structure
      CHECK_FERR(PAMPA_meshInit(&mesh), comm);

      CHECK_FERR(PAMPA_dmeshGather (dmesh, &mesh), comm);
      data->data.infoprt = 1;
      //CHECK_FERR(PAMPA_MMG3D4_meshSave(&mesh, 1, &data->data, NULL, data->cmout), comm);
      {
        FILE *f1, *f2;
        char s[100];
        sprintf (s, "%s.mesh", data->cmout);
        f1 = fopen (s, "w");
        sprintf (s, "%s.sol", data->cmout);
        f2 = fopen (s, "w");
        PAMPA_meshMeshSave (&mesh, f1, f2);
        fclose (f1);
        fclose (f2);
      }
      // Finalisation of PaMPA mesh structure
      PAMPA_meshExit(&mesh);
    }
    else {
      // Gather PaMPA mesh
      CHECK_FERR(PAMPA_dmeshGather (dmesh, NULL), comm);
    }
  }
  if (data->dpout != NULL) {
    File f;
    f.name = data->dpout;
    f.mode = "w";
    f.pntr = stdout;
    CHECK_FERR (fileBlockOpen (&f, 1), comm);
    CHECK_FERR (PAMPA_dmeshSave (dmesh, f.pntr), comm);
    CHECK_FERR (fileBlockClose (&f, 1), comm);
  }

  return (0);
}

int freemem (GlbData * data) {
  MPI_Type_free(&data->data.coortyp);
  if (data->dpout != NULL)
    free (data->dpout);
  if (data->dpin != NULL)
    free (data->dpin);
  if (data->debug != NULL)
    free (data->debug);
  if (data->dmout != NULL)
    free (data->dmout);
  if (data->log != NULL)
    free (data->log);

  return (0);
}

int main(int argc, char** argv){
  PAMPA_Dmesh dmesh, dmesh2;
  PAMPA_AdaptInfo infoptr;
  PAMPA_Num vertglbnbr;
  GlbData data;
  //int bak2;
  int rank, retval, size, bak, new;
  MPI_Comm comm;
  int  namelen;
  char procnam[MPI_MAX_PROCESSOR_NAME] ;


  comm = MPI_COMM_WORLD;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &retval);
  // XXX tester retval
  MPI_Comm_rank(comm, &rank) ;
  MPI_Comm_size(comm, &size) ;
  MPI_Get_processor_name(procnam,&namelen);
  printf ("Processus %d, host: %s, with MPI rank %d on %d\n", getpid(), procnam, rank, size);

  CHECK_FERR (parsopt(argv, argc, rank, size, &data), comm);

  if (data.log != NULL) {
    fflush (stdout);
    //fflush (stderr);
    bak = dup (1);
    //bak2 = dup (2);
    //if (rank != 0)
    //  sprintf (data.log, "/dev/null");
    new = open(data.log, O_CREAT|O_WRONLY|O_TRUNC, 0600);
    dup2 (new, 1);
    //dup2 (1, 2);
  }
  printf ("Processus %d, host: %s, with MPI rank %d on %d\n", getpid(), procnam, rank, size);
  if (rank == 0) {
	int i;
	for (i = 0; i < argc; i ++)
	  printf ("%s ", argv[i]);
	printf ("\n");
  }


  CHECK_FERR (loadmesh (&data, &dmesh, comm, rank, size), comm);

  if (rank == 0) {
    PAMPA_printVersions ();
    PAMPA_dmeshEnttSize(&dmesh, data.data.tetrent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("At the beginning\n\tNumber of tetrahedra: " PAMPA_NUMSTRING "\n", vertglbnbr);
    PAMPA_dmeshEnttSize(&dmesh, data.data.nodeent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
    printf ("\tNumber of nodes: " PAMPA_NUMSTRING "\n", vertglbnbr);
  }

  infoptr.rank = rank;
  infoptr.alpha = data.alpha;
  infoptr.flagval = (data.data.faceent == ~0) ? (0) : (PAMPA_MMG3D4_FACES);
  infoptr.zonenb2 = data.zones;
  infoptr.estmflg = data.comp;
  infoptr.itdebug = data.debug;
  infoptr.methval = data.ident;
  infoptr.ibndval = data.iband;
  infoptr.ballsiz = data.zweight;
  infoptr.ebndval = data.eband;
  infoptr.loopval = data.extloop;
  infoptr.ratemin = data.rmin;
  infoptr.bndgflg = data.bndfg;
  infoptr.minxval = data.minx;
  infoptr.maxxval = data.maxx;
  infoptr.minyval = data.miny;
  infoptr.maxyval = data.maxy;
  infoptr.minzval = data.minz;
  infoptr.maxzval = data.maxz;
  infoptr.EXT_meshAdapt = &PAMPA_MMG3D4_meshAdapt;
#ifdef MSHINT 
  infoptr.EXT_meshInt = &PAMPA_MSHINT_meshInt;
#endif /* MSHINT */
  infoptr.EXT_dmeshCheck = &PAMPA_MMG3D4_dmeshCheck;
  infoptr.EXT_dmeshBand = &PAMPA_MMG3D4_dmeshBand;
  infoptr.EXT_dmeshWeightCompute = &PAMPA_MMG3D4_dmeshWeightCompute;
  infoptr.EXT_dmeshMetricCompute = &PAMPA_MMG3D4_dmeshMetricCompute;
  infoptr.EXT_meshSave = &PAMPA_MMG3D4_meshSave;
  infoptr.dataptr = &data.data;
#define PAMPA_TIME_CHECK
  CHECK_FERR (PAMPA_dmeshAdapt (&dmesh, &infoptr, &dmesh2), comm);
#undef PAMPA_TIME_CHECK

  if (data.comp == 0) {
    if (rank == 0) {
      PAMPA_dmeshEnttSize(&dmesh, data.data.tetrent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
      printf ("At the end\n\tNumber of tetrahedra: " PAMPA_NUMSTRING "\n", vertglbnbr);
      PAMPA_dmeshEnttSize(&dmesh, data.data.nodeent, &vertglbnbr, PAMPA_VERT_GLOBAL, NULL);
      printf ("\tNumber of nodes: " PAMPA_NUMSTRING "\n", vertglbnbr);
    }



    //CHECK_FERR (qualmesh (&data, &dmesh, comm, rank, size), comm);
    CHECK_FERR (savemesh (&data, &dmesh, comm, rank, size), comm);
  }

  //{
  //  SCOTCH_Dgraph dgdat;
  //  char s[100];

  //  CHECK_FERR (SCOTCH_dgraphInit (&dgdat, comm), comm);
  //  PAMPA_dmeshDgraph (&dmesh, &dgdat);
  //    PAMPA_dmeshExit(&dmesh);
  //  sprintf (s, "dg-%d-%d.grf.bz2", size, rank);
  //  File f;
  //  f.name = s;
  //  f.mode = "w";
  //  f.pntr = stdin;
  //  CHECK_FERR (fileBlockOpen (&f, 1), comm);
  //  CHECK_FERR (SCOTCH_dgraphSave (&dgdat, f.pntr), comm);
  //  CHECK_FERR (fileBlockClose (&f, 1), comm);
  //      SCOTCH_dgraphExit (&dgdat);
  //}


  CHECK_FERR (freemem (&data), comm);

  if (data.log != NULL) {
    if (rank == 0)
      printf("fin du prog\n");
    fflush (stdout);
    //fflush (stderr);
    dup2 (bak, 1);
    //dup2 (bak2, 2);
    close (bak);
    //close (bak2);
  }

  if (rank == 0)
    printf("fin du prog\n");
  printf("fin du prog, rank %d\n", rank);

  MPI_Finalize() ;

  return EXIT_SUCCESS;
}
