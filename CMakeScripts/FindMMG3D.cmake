##  Copyright 2011-2017 Inria  ##
#############################################################
##
##    \file        FindMMG3D.cmake
##
##    \authors     Cedric Lachat
##
##
##    \date        Version 1.0: from: 28 Nov 2011
##                              to:   22 Sep 2017
##
#############################################################
SET(MMG3D_PATH ${MMG3D_HOME})

FIND_PATH(MMG3D_INCLUDE_DIR libmmg3d.h PATHS ${MMG3D_PATH} PATH_SUFFIXES include)

FIND_LIBRARY(MMG3D_LIBRARY NAMES libmmg3d.a PATHS ${MMG3D_PATH} PATH_SUFFIXES lib)

IF (MMG3D_INCLUDE_DIR AND MMG3D_LIBRARY)
  SET(MMG3D_FOUND TRUE)
ENDIF (MMG3D_INCLUDE_DIR AND MMG3D_LIBRARY)


IF (MMG3D_FOUND)
  IF (NOT MMG3D_FIND_QUIETLY)
    MESSAGE(STATUS "Found MMG3D : ${MMG3D_LIBRARY}")
  ENDIF (NOT MMG3D_FIND_QUIETLY)
ELSE (MMG3D_FOUND)
  IF (MMG3D_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find MMG3D. You have to define variable MMG3D_HOME")
  ENDIF (MMG3D_FIND_REQUIRED)
ENDIF (MMG3D_FOUND)

MARK_AS_ADVANCED(MMG3D_INCLUDE_DIR MMG3D_LIBRARY)
