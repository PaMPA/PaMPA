##  Copyright 2009-2016 Inria
##
## This file is part of the PaMPA software package for parallel
## mesh partitioning and adaptation.
##
## PaMPA is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## any later version.
## 
## PaMPA is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## In this respect, the user's attention is drawn to the risks associated
## with loading, using, modifying and/or developing or reproducing the
## software by the user in light of its specific status of free software,
## that may mean that it is complicated to manipulate, and that also
## therefore means that it is reserved for developers and experienced
## professionals having in-depth computer knowledge. Users are therefore
## encouraged to load and test the software's suitability as regards
## their requirements in conditions enabling the security of their
## systems and/or data to be ensured and, more generally, to use and
## operate it in the same conditions as regards security.
## 
## The fact that you are presently reading this means that you have had
## knowledge of the GPLv3 license and that you accept its terms.
##
#############################################################
##
##    \file        software_version.sh
##
##    \authors     Cedric Lachat
##
##    \brief       This file is a part of the configuration
##                 to build PaMPA with CMake
##
##    \date        Version 1.0: from:  7 Jun 2013
##                              to:    5 Oct 2016
##
#############################################################
#!/bin/bash

opath=$( pwd )
echo "/* Generated file by $0 */" > $opath/versions_ext.h
echo -e "Searching versions for external tools..."
for i in $*; do
  prgm=$( echo $i|awk -F: '{print $1}' )
  echo -e "\tExternal tool:$prgm"
  path=$( echo $i|awk -F: '{print $2}' )
  files=$( echo $i|awk -F: '{print $3}'|sed -e 's/,/ /g' )
  cd $path
  res=$(git rev-parse 2>/dev/null)
  if [ $? == 0 ];then
    scm="git"
    rev=$( git log --pretty=oneline --abbrev-commit --abbrev=10 -1|awk '{print $1}' )
    # not commited files
    notc=$( git diff HEAD --name-only|wc -l )
  else
    res=$(svn status 2>&1 |grep -v "not a working copy")
    if [ $? == 0 ]; then
      scm="svn"
      #rev=$( svn log -l 1|grep "^r"|awk '{print $1}' )
      rev=$( svn info|grep Revision|awk '{print $2}' )
      # not commited files
      notc=$( svn st|grep "^M"|wc -l )
    else
      scm="none"
      rev="none"
      notc="infinity"
    fi
  fi
  echo -e "\t\tFind modified files after last building..."
  cmd="find $path -type f"
  for file in $files; do
    cmd="$cmd -a -cnewer $file"
  done
  echo -e "\t\tDone"
  chgd=$( $cmd | wc -l )
  echo "#define ${prgm}_PATH \"$path\"" >> $opath/versions_ext.h
  echo "#define ${prgm}_SCM \"$scm\"" >> $opath/versions_ext.h
  echo "#define ${prgm}_REV \"$rev\"" >> $opath/versions_ext.h
  echo "/* Number of files changed after last building */" >> $opath/versions_ext.h
  echo "#define ${prgm}_CHGD \"$chgd\"" >> $opath/versions_ext.h
  echo "/* Not commited files */" >> $opath/versions_ext.h
  echo "#define ${prgm}_NOTC \"$notc\"" >> $opath/versions_ext.h
  echo -e "\tDone"
done
