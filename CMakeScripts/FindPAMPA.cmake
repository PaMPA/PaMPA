##  Copyright 2017 Inria  ##
#############################################################
##
##    \file        FindPAMPA.cmake
##
##    \authors     Cedric Lachat
##
##
##    \date        Version 1.0: from: 19 May 2017
##                              to:   25 Sep 2017
##
#############################################################
## Copyright 2009-2017 Inria

if (NOT ${PAMPA_SRC_PATH} STREQUAL ${PAMPA_SRC_HOME})
  set (PAMPA_SRC_LIBRARY "NOTFOUND")
  set (PAMPA_SRC_EXTRA_LIBRARY "NOTFOUND")
  set (PAMPA_SRC_INCLUDE_DIR "NOTFOUND")
endif ()
set (PAMPA_SRC_PATH ${PAMPA_SRC_HOME} CACHE INTERNAL "")

find_path (PAMPA_SRC_INCLUDE_DIR pampaf.h HINTS ${PAMPA_SRC_PATH} PATH_SUFFIXES include)

find_library (PAMPA_SRC_LIBRARY NAMES pampa HINTS ${PAMPA_SRC_PATH} PATH_SUFFIXES lib)
find_library (PAMPA_SRC_EXTRA_LIBRARY NAMES pampaerr HINTS ${PAMPA_SRC_PATH} PATH_SUFFIXES lib)
if (PAMPA_SRC_INCLUDE_DIR AND PAMPA_SRC_LIBRARY AND PAMPA_SRC_EXTRA_LIBRARY)
  set (PAMPA_SRC_FOUND TRUE)
endif (PAMPA_SRC_INCLUDE_DIR AND PAMPA_SRC_LIBRARY AND
  PAMPA_SRC_EXTRA_LIBRARY)

if (PAMPA_SRC_FOUND)
  if (NOT PAMPA_SRC_FIND_QUIETLY)
    message (STATUS "Found PaMPA : ${PAMPA_SRC_LIBRARY}")
  endif (NOT PAMPA_SRC_FIND_QUIETLY)
else (PAMPA_SRC_FOUND)
  if (PAMPA_SRC_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find PaMPA. You have to define variable PAMPA_SRC_HOME")
  endif (PAMPA_SRC_FIND_REQUIRED)
endif (PAMPA_SRC_FOUND)

mark_as_advanced (PAMPA_SRC_INCLUDE_DIR PAMPA_SRC_LIBRARY PAMPA_SRC_EXTRA_LIBRARY)
