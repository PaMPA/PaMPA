##  Copyright 2017 Inria  ##
#############################################################
##
##    \file        FindPAMPA-CORE-SRC.cmake
##
##    \authors     Cedric Lachat
##
##
##    \date        Version 1.0: from: 23 May 2017
##                              to:   25 Sep 2017
##
#############################################################
## Copyright 2009-2017 Inria

if (NOT ${PAMPA-CORE-SRC_PATH} STREQUAL ${PAMPA-CORE-SRC_HOME})
  set (PAMPA-CORE-SRC_LIBRARY "NOTFOUND")
  set (PAMPA-CORE-SRC_EXTRA_LIBRARY "NOTFOUND")
  set (PAMPA-CORE-SRC_INCLUDE_DIR "NOTFOUND")
endif ()
set (PAMPA-CORE-SRC_PATH ${PAMPA-CORE-SRC_HOME} CACHE INTERNAL "")

find_path (PAMPA-CORE-SRC_INCLUDE_DIR NAMES library.h mesh.h dmesh.h HINTS ${PAMPA-CORE-SRC_PATH} PATH_SUFFIXES src/libpampa)

if (PAMPA-CORE-SRC_INCLUDE_DIR)
  set (PAMPA-CORE-SRC_FOUND TRUE)
endif (PAMPA-CORE-SRC_INCLUDE_DIR)

if (PAMPA-CORE-SRC_FOUND)
  if (NOT PAMPA-CORE-SRC_FIND_QUIETLY)
    message (STATUS "Found PaMPA : ${PAMPA-CORE-SRC_LIBRARY}")
  endif (NOT PAMPA-CORE-SRC_FIND_QUIETLY)
else (PAMPA-CORE-SRC_FOUND)
  if (PAMPA-CORE-SRC_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find PaMPA source directory. You have to define variable PAMPA-CORE-SRC_HOME")
  endif (PAMPA-CORE-SRC_FIND_REQUIRED)
endif (PAMPA-CORE-SRC_FOUND)

mark_as_advanced (PAMPA-CORE-SRC_INCLUDE_DIR)
