##  Copyright 2017 Inria  ##
#############################################################
##
##    \file        FindPAMPA-CORE.cmake
##
##    \authors     Cedric Lachat
##
##
##    \date        Version 1.0: from: 23 May 2017
##                              to:   25 Sep 2017
##
#############################################################
## Copyright 2009-2017 Inria

if (NOT ${PAMPA-CORE_PATH} STREQUAL ${PAMPA-CORE_HOME})
  set (PAMPA-CORE_LIBRARY "NOTFOUND")
  set (PAMPA-CORE_EXTRA_LIBRARY "NOTFOUND")
  set (PAMPA-CORE_INCLUDE_DIR "NOTFOUND")
endif ()
set (PAMPA-CORE_PATH ${PAMPA-CORE_HOME} CACHE INTERNAL "")

find_path (PAMPA-CORE_INCLUDE_DIR pampa.h HINTS ${PAMPA-CORE_PATH} PATH_SUFFIXES include)

find_library (PAMPA-CORE_LIBRARY NAMES pampa HINTS ${PAMPA-CORE_PATH} PATH_SUFFIXES lib)
find_library (PAMPA-CORE_EXTRA_LIBRARY NAMES pampaerr HINTS ${PAMPA-CORE_PATH} PATH_SUFFIXES lib)
if (PAMPA-CORE_INCLUDE_DIR AND PAMPA-CORE_LIBRARY AND PAMPA-CORE_EXTRA_LIBRARY)
  set (PAMPA-CORE_FOUND TRUE)
endif (PAMPA-CORE_INCLUDE_DIR AND PAMPA-CORE_LIBRARY AND
  PAMPA-CORE_EXTRA_LIBRARY)

if (PAMPA-CORE_FOUND)
  if (NOT PAMPA-CORE_FIND_QUIETLY)
    message (STATUS "Found PaMPA : ${PAMPA-CORE_LIBRARY}")
  endif (NOT PAMPA-CORE_FIND_QUIETLY)
else (PAMPA-CORE_FOUND)
  if (PAMPA-CORE_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find PaMPA. You have to define variable PAMPA-CORE_HOME")
  endif (PAMPA-CORE_FIND_REQUIRED)
endif (PAMPA-CORE_FOUND)

mark_as_advanced (PAMPA-CORE_INCLUDE_DIR PAMPA-CORE_LIBRARY PAMPA-CORE_EXTRA_LIBRARY)
