/*!

\page install Installation
\tableofcontents
     - The installation directory for PaMPA is defined by the CMAKE_INSTALL_PREFIX environment variable. By default, it is set to "/usr/local", but can be modified using ccmake or cmake-gui.

*/
