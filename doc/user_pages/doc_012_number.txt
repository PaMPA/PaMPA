/*!

\page number Numbering

Software for distributed-memory architectures have to handle their local memory independently on each processor. Consequently, all data structures are accessed according to a local addressing scheme: pointers refer to a local addressing space, and array indices are numbered locally. These measures are essential to achieve memory efficiency, thanks to array compacity: in the local address space of every processor, array sizes are bounded by the number of entities managed by this processor and all array cells are meant to be used.

Because distributed meshes are defined in PaMPA using an unambiguous, global numbering, and data arrays have to be accesses through local indexing, PaMPA uses simultaneously both types of numberings.

\section number_glb Global numbering

The global numbering allows the user to identify every vertex, independently from the distribution of mesh entities across processors. This numbering is used to build the distributed enriched graph with PAMPA_dmeshBuild (see \ref quick_c_build).
See following figure for an example of global numering.

\image html figs/maillage_dist_num2.png
\image latex figs/maillage_dist_num2.pdf


\section number_loc Local numbering

As seen above, local indices are mandatory to access local data arrays. Hence, PaMPA maintains a set of local indices and provides service routines that help the user to create local arrays associated with local mesh entities, and to iterate over them. Local numberings are provided for each mesh entity. and comprise halo vertices as well.
For all local vertices on every subdomain, vertices are indexed per entity.
Local indices are extended to index halo vertices as well, e.g. with respect to processor P1 on following figure.

\image html figs/maillage_dist_p1.png
\image latex figs/maillage_dist_p1.pdf

\section number_lbl Label numbering

The user will also be soon able to provide her/his own global numbering of the distributed mesh vertices.1.0. This amounts to attaching a unique, numeric label to every vertex. Doing so may slightly slow down the building of distributed meshes in PaMPA, since a global, compact numbering must always be created internally.

*/
