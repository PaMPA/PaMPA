/*!

\page quick_c Howto in C
\tableofcontents

\section quick_c_intro Introduction
Array indices start from 0.

\section quick_c_init Initialize meshes
The first step is to initialize a mesh with PAMPA_meshInit() in sequential, or PAMPA_dmeshInit() in parallel.

\section quick_c_build Building meshes

Once the appropriate mesh data structure is initialized, a distributed or centralized mesh may be built using PAMPA_meshBuild() or PAMPA_dmeshBuild(), respectively. The following images show different ways to represent a mesh made up of one quadrangle and two triangles.
\image html meshes/C/17-vert_1.png
\image html meshes/C/17-vert_2.png
\image latex meshes/C/17-vert_1.pdf
\image latex meshes/C/17-vert_2.pdf

If a centralized mesh is used, PAMPA_dmeshScatter() may distribute it according to its global numbering.
The following examples show you how to build and check distributed meshes:
 - build_check.c

PAMPA_meshCheck() and PAMPA_dmeshCheck() check the consistency of a centralized or a distributed mesh, respectively.

\section quick_c_iter Iterators

Some examples show how to use iterators:
 - seq_neighbors.c: in the sequential case;
 - neighbors.c: in the parallel case.

All of them include tools/seq_it.c or tools/it.c

For example, when neighbors.c is coupled with 17-vert.c and it.c and runs on 3 processors, we obtain on output the lines written in file tests/neighbors_11-vert_2-proc_it_c.
The second line of this file is:
\verbatim
[0] neighbor of 0 with entity 1 : 1(sub: 4) 1(ent: 3) 3(glb)
\endverbatim
It means that quadrangle (entity 1) 0 (with entity numbering) owns boundary edge (entity 4) 1 (numbered in the sub-entity), also known as edge (entity 3) 1 (numbered in the entity), finally known as vertex 3 (global numbering).

We can see on the following figure, which shows the distributed mesh, that vertex 4 (previous quadrangle) on processor 0 is linked to vertex 3 (boundary edge 1 or edge 1, according to the numbering on this processor).

\image html meshes/C/17-vert_3.png
\image latex meshes/C/17-vert_3.pdf

\section quick_c_map Partitioning

Partitioning is illustrated with this example:
 - part.c

It is based on the PAMPA_dmeshPart() function. In the sequential case, static mapping is performed with PAMPA_meshMap() instead of PAMPA_meshPart().
The value tagged PAMPA_TAG_PART associated on elements is used (if defined) by these functions. It depicts the load of each element. The load on edges is given directly to PAMPA_dmeshBuild().

\section quick_c_ovlp Overlap

The following example shows how to use the overlap feature, with a thickness of 1.
 - overlap_1.c

This test case also uses iterators and performs data exchange across mesh entities.

\section quick_c_redist Redistribution

Two examples illustrate the redistribution feature of PaMPA:
 - redist.c
 - redist2.c

\section quick_c_comm Communications

Data can be exchanged across processors using the following routines:
  - PAMPA_dmeshHaloValue(): for a single value borne by some entity;
  - PAMPA_dmeshHaloEntt(): for all values borne by some entity;
  - PAMPA_dmeshHalo(): for all values.

The previous features use synchronous communication. Asynchronous data exchange can be performed by using routines that exhibit the "Async" suffix. For instance, PAMPA_dmeshHaloValueAsync() initializes the asynchronous communication of a single value, and PAMPA_dmeshHaloWait() finalizes it.

*/
