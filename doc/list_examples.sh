#!/bin/bash

usage()
{
  cat << EOF
  usage: $0 options

  This script recursively searches c and F90 source files in arg1 directory and
  put all files in arg2 file

  OPTIONS:
  -h      Show this message
EOF
}

if [ $# != 2 ]; then
  usage
  exit 1
fi

while getopts "h" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    ?)
      usage
      exit
      ;;
  esac
done



find $1/C/{include,meshes,tools,components} $1/FORTRAN \( -name "*.c" -o -name "*.F90" \) -execdir ls {} \; | awk -F '/' '{print "//! \\example", $2}'|grep -v -e mdmesh >> $2


