PaMPA performs dynamic parallel remeshing and redistribution of very
large unstructured meshes.

Two formats of documentation are available,
[HTML](https://pampa.gitlabpages.inria.fr/PaMPA/user_2.0/html/) and
[PDF](https://pampa.gitlabpages.inria.fr/PaMPA/user_2.0/pampa_v2.0.pdf).
